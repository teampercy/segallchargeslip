// -----------------------------------------------------------------------
// <copyright file="ChargeSlip.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing ChargeSlip
/// </summary>
namespace SegallChargeSlipBll
{
    [Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
    public class ChargeSlip
    {
        #region Basic Functionality

        #region Variable Declaration

        /// <summary>
        /// Variable to store Database object to interact with database.
        /// </summary>
        private Database db;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ChargeSlip class.
        /// </summary>
        public ChargeSlip()
        {
            this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        }

        /// <summary>
        /// Initializes a new instance of the ChargeSlip class.
        /// </summary>
        /// <param name="chargeSlipID">Sets the value of ChargeSlipID.</param>
        public ChargeSlip(int chargeSlipID)
        {
            this.db = DatabaseFactory.CreateDatabase();
            this.ChargeSlipID = chargeSlipID;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets ChargeSlipID
        /// </summary>
        public int ChargeSlipID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DealName
        /// </summary>
        public string DealName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LocationID
        /// </summary>
        public int LocationID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets MapLocationID
        /// </summary>
        public int MapLocationID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ChargeDate
        /// </summary>
        public DateTime ChargeDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CompanyID
        /// </summary>
        public int CompanyID
        {
            get;
            set;
        }

        ///// <summary>
        ///// Gets or sets DealTypeID
        ///// </summary>
        //public int DealTypeID
        //{
        //    get;
        //    set;
        //}

        ///// <summary>
        ///// Gets or sets DealSubTypeID
        ///// </summary>
        //public int DealSubTypeID
        //{
        //    get;
        //    set;
        //}

        ///// <summary>
        ///// Gets or sets DealTransactionId
        ///// </summary>
        //public int DealTransactionId
        //{
        //    get;
        //    set;
        //}

        /// <summary>
        /// Gets or sets CreatedBy
        /// </summary>
        public int CreatedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedOn
        /// </summary>
        public DateTime CreatedOn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LastModifiedOn
        /// </summary>
        public DateTime LastModifiedOn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Status
        /// </summary>
        public byte Status
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets DealStatusId
        /// </summary>
        public int DealStatusId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LocShoppingCenter
        /// </summary>
        public string LocShoppingCenter
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LocAddress1
        /// </summary>
        public string LocAddress1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LocAddress2
        /// </summary>
        public string LocAddress2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LocCity
        /// </summary>
        public string LocCity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LocState
        /// </summary>
        public string LocState
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LocZipCode
        /// </summary>
        public string LocZipCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LocCounty
        /// </summary>
        public string LocCounty
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LocLatitude
        /// </summary>
        public decimal LocLatitude
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LocLongitude
        /// </summary>
        public decimal LocLongitude
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Company
        /// </summary>
        public string Company
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CTAddress1
        /// </summary>
        public string CTAddress1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CTAddress2
        /// </summary>
        public string CTAddress2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CTCity
        /// </summary>
        public string CTCity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CTState
        /// </summary>
        public string CTState
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CTZipCode
        /// </summary>
        public string CTZipCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CTName
        /// </summary>
        public string CTName
        {
            get;
            set;
        }
        public DealTransactions DealTransactionsProperty
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CTOwnershipEntity
        /// </summary>
        public string CTOwnershipEntity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CTEmail
        /// </summary>
        public string CTEmail
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CTCounty
        /// </summary>
        public string CTCounty
        {
            get;
            set;

        }
        /// Gets or sets TotAnnualRent
        /// </summary>
        public decimal TotAnnualRent
        {
            get;
            set;
        }


        /// Gets or sets PropertyId
        /// </summary>
        public int PropertyId
        {
            get;
            set;
        }

        /// Gets or sets PropertyId
        /// </summary>
        public string UserIds
        {
            get;
            set;
        }
        public string DeleteReason
        {
            get;
            set;
        }
        public int DealDesignationTypesId
        {
            get;
            set;
        }

        /// <summary>
        /// Report file name
        /// </summary>
        public string Report
        {
            get;
            set;
        }

        /// <summary>
        /// Report file name edited
        /// </summary>
        public string ReportEdited
        {
            get;
            set;
        }
        #endregion

        #region Actions

        /// <summary>
        /// Loads the details for ChargeSlip.
        /// </summary>
        /// <returns>True if Load operation is successful; Else False.</returns>
        public bool Load()
        {
            try
            {
                //if (DateTime.Now <= DateTime.Parse("2014/09/03"))
                //{
                    if (this.ChargeSlipID != 0)
                    {
                        DbCommand com = this.db.GetStoredProcCommand("usp_ChargeSlipGetDetails");
                        this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
                        DataSet ds = this.db.ExecuteDataSet(com);
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            DataTable dt = ds.Tables[0];
                            this.ChargeSlipID = Convert.ToInt32(dt.Rows[0]["ChargeSlipID"]);
                            this.DealName = Convert.ToString(dt.Rows[0]["DealName"]);
                            this.LocationID = Convert.ToInt32(dt.Rows[0]["LocationID"]);
                            this.MapLocationID = Convert.ToInt32(dt.Rows[0]["MapLocationID"]);
                            this.ChargeDate = Convert.ToDateTime(dt.Rows[0]["ChargeDate"]);
                            this.CompanyID = Convert.ToInt32(dt.Rows[0]["CompanyID"]);
                            this.CreatedBy = Convert.ToInt32(dt.Rows[0]["CreatedBy"]);
                            this.CreatedOn = Convert.ToDateTime(dt.Rows[0]["CreatedOn"]);
                            this.LastModifiedBy = Convert.ToInt32(dt.Rows[0]["LastModifiedBy"]);
                            this.LastModifiedOn = Convert.ToDateTime(dt.Rows[0]["LastModifiedOn"]);
                            this.Status = Convert.ToByte(dt.Rows[0]["Status"]);
                            this.DealStatusId = Convert.ToInt32(dt.Rows[0]["DealStatusId"]);
                            this.LocShoppingCenter = Convert.ToString(dt.Rows[0]["LocShoppingCenter"]);
                            this.LocAddress1 = Convert.ToString(dt.Rows[0]["LocAddress1"]);
                            this.LocAddress2 = Convert.ToString(dt.Rows[0]["LocAddress2"]);
                            this.LocCity = Convert.ToString(dt.Rows[0]["LocCity"]);
                            this.LocState = Convert.ToString(dt.Rows[0]["LocState"]);
                            this.LocZipCode = Convert.ToString(dt.Rows[0]["LocZipCode"]);
                            this.LocCounty = Convert.ToString(dt.Rows[0]["LocCounty"]);
                            this.LocLatitude = Convert.ToDecimal(dt.Rows[0]["LocLatitude"]);
                            this.LocLongitude = Convert.ToDecimal(dt.Rows[0]["LocLongitude"]);
                            this.Company = Convert.ToString(dt.Rows[0]["Company"]);
                            this.CTAddress1 = Convert.ToString(dt.Rows[0]["CTAddress1"]);
                            this.CTAddress2 = Convert.ToString(dt.Rows[0]["CTAddress2"]);
                            this.CTCity = Convert.ToString(dt.Rows[0]["CTCity"]);
                            this.CTState = Convert.ToString(dt.Rows[0]["CTState"]);
                            this.CTZipCode = Convert.ToString(dt.Rows[0]["CTZipCode"]);
                            this.CTName = Convert.ToString(dt.Rows[0]["CTName"]);
                            this.CTOwnershipEntity = Convert.ToString(dt.Rows[0]["CTOwnershipEntity"]);
                            this.CTEmail = Convert.ToString(dt.Rows[0]["CTEmail"]);
                            this.CTCounty = Convert.ToString(dt.Rows[0]["CTCounty"]);
                            this.DealDesignationTypesId = Convert.ToInt32(dt.Rows[0]["DealDesignationTypesId"]);
                            this.Report = Convert.ToString(dt.Rows[0]["Report"]);
                            this.ReportEdited = Convert.ToString(dt.Rows[0]["ReportEdited"]);
                            return true;
                        }
                    }
                //}

                return false;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }
        }

        /// <summary>
        /// Inserts details for ChargeSlip if ChargeSlipID = 0.
        /// Else updates details for ChargeSlip.
        /// </summary>
        /// <returns>True if Save operation is successful; Else False.</returns>
        public bool Save()
        {
            if (this.ChargeSlipID == 0)
            {
                return this.Insert();
            }
            else
            {
                if (this.ChargeSlipID > 0)
                {
                    return this.Update();
                }
                else
                {
                    this.ChargeSlipID = 0;
                    return false;
                }
            }
        }

        /// <summary>
        /// Inserts details for ChargeSlip.
        /// Saves newly created Id in ChargeSlipID.
        /// </summary>
        /// <returns>True if Insert operation is successful; Else False.</returns>
        private bool Insert()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_ChargeSlipInsert");
                this.db.AddOutParameter(com, "ChargeSlipID", DbType.Int32, 1024);
                if (!String.IsNullOrEmpty(this.DealName))
                {
                    this.db.AddInParameter(com, "DealName", DbType.String, this.DealName);
                }
                else
                {
                    this.db.AddInParameter(com, "DealName", DbType.String, DBNull.Value);
                }
                if (this.LocationID > 0)
                {
                    this.db.AddInParameter(com, "LocationID", DbType.Int32, this.LocationID);
                }
                else
                {
                    this.db.AddInParameter(com, "LocationID", DbType.Int32, DBNull.Value);
                }
                if (this.MapLocationID > 0)
                {
                    this.db.AddInParameter(com, "MapLocationID", DbType.Int32, this.MapLocationID);
                }
                else
                {
                    this.db.AddInParameter(com, "MapLocationID", DbType.Int32, 0);
                }
                if (this.ChargeDate > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "ChargeDate", DbType.DateTime, this.ChargeDate);
                }
                else
                {
                    this.db.AddInParameter(com, "ChargeDate", DbType.DateTime, DBNull.Value);
                }
                if (this.CompanyID > 0)
                {
                    this.db.AddInParameter(com, "CompanyID", DbType.Int32, this.CompanyID);
                }
                else
                {
                    this.db.AddInParameter(com, "CompanyID", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedBy > 0)
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
                }
                if (this.LastModifiedBy > 0)
                {
                    this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, this.LastModifiedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, DBNull.Value);
                }
                if (this.LastModifiedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, this.LastModifiedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, DBNull.Value);
                }
                //if (!String.IsNullOrEmpty(Status))
                //{
                    this.db.AddInParameter(com, "Status", DbType.Byte, this.Status);
               // }
                //else
              //  {
                   // this.db.AddInParameter(com, "Status", DbType.String, DBNull.Value);
               // }
                if (this.DealStatusId > 0)
                {
                    this.db.AddInParameter(com, "DealStatusId", DbType.Int32, this.DealStatusId);
                }
                else
                {
                    this.db.AddInParameter(com, "DealStatusId", DbType.Int32, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocShoppingCenter))
                {
                    this.db.AddInParameter(com, "LocShoppingCenter", DbType.String, this.LocShoppingCenter);
                }
                else
                {
                    this.db.AddInParameter(com, "LocShoppingCenter", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocAddress1))
                {
                    this.db.AddInParameter(com, "LocAddress1", DbType.String, this.LocAddress1);
                }
                else
                {
                    this.db.AddInParameter(com, "LocAddress1", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocAddress2))
                {
                    this.db.AddInParameter(com, "LocAddress2", DbType.String, this.LocAddress2);
                }
                else
                {
                    this.db.AddInParameter(com, "LocAddress2", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocCity))
                {
                    this.db.AddInParameter(com, "LocCity", DbType.String, this.LocCity);
                }
                else
                {
                    this.db.AddInParameter(com, "LocCity", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocState))
                {
                    this.db.AddInParameter(com, "LocState", DbType.String, this.LocState);
                }
                else
                {
                    this.db.AddInParameter(com, "LocState", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocZipCode))
                {
                    this.db.AddInParameter(com, "LocZipCode", DbType.String, this.LocZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "LocZipCode", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocCounty))
                {
                    this.db.AddInParameter(com, "LocCounty", DbType.String, this.LocCounty);
                }
                else
                {
                    this.db.AddInParameter(com, "LocCounty", DbType.String, DBNull.Value);
                }
                if (this.LocLatitude != 0)
                {
                    this.db.AddInParameter(com, "LocLatitude", DbType.Decimal, this.LocLatitude);
                }
                else
                {
                    this.db.AddInParameter(com, "LocLatitude", DbType.Double, DBNull.Value);
                }
                if (this.LocLongitude != 0)
                {
                    this.db.AddInParameter(com, "LocLongitude", DbType.Decimal, this.LocLongitude);
                }
                else
                {
                    this.db.AddInParameter(com, "LocLongitude", DbType.Double, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Company))
                {
                    this.db.AddInParameter(com, "Company", DbType.String, this.Company);
                }
                else
                {
                    this.db.AddInParameter(com, "Company", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTAddress1))
                {
                    this.db.AddInParameter(com, "CTAddress1", DbType.String, this.CTAddress1);
                }
                else
                {
                    this.db.AddInParameter(com, "CTAddress1", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTAddress2))
                {
                    this.db.AddInParameter(com, "CTAddress2", DbType.String, this.CTAddress2);
                }
                else
                {
                    this.db.AddInParameter(com, "CTAddress2", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTCity))
                {
                    this.db.AddInParameter(com, "CTCity", DbType.String, this.CTCity);
                }
                else
                {
                    this.db.AddInParameter(com, "CTCity", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTState))
                {
                    this.db.AddInParameter(com, "CTState", DbType.String, this.CTState);
                }
                else
                {
                    this.db.AddInParameter(com, "CTState", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTZipCode))
                {
                    this.db.AddInParameter(com, "CTZipCode", DbType.String, this.CTZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "CTZipCode", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTName))
                {
                    this.db.AddInParameter(com, "CTName", DbType.String, this.CTName);
                }
                else
                {
                    this.db.AddInParameter(com, "CTName", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTOwnershipEntity))
                {
                    this.db.AddInParameter(com, "CTOwnershipEntity", DbType.String, this.CTOwnershipEntity);
                }
                else
                {
                    this.db.AddInParameter(com, "CTOwnershipEntity", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTEmail))
                {
                    this.db.AddInParameter(com, "CTEmail", DbType.String, this.CTEmail);
                }
                else
                {
                    this.db.AddInParameter(com, "CTEmail", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTCounty))
                {
                    this.db.AddInParameter(com, "CTCounty", DbType.String, this.CTCounty);
                }
                else
                {
                    this.db.AddInParameter(com, "CTCounty", DbType.String, DBNull.Value);
                }
                if (this.DealDesignationTypesId > 0)
                {
                    this.db.AddInParameter(com, "DealDesignationTypesId", DbType.Int32, this.DealDesignationTypesId);
                }
                else
                {
                    this.db.AddInParameter(com, "DealDesignationTypesId", DbType.Int32, DBNull.Value);
                }
                this.db.ExecuteNonQuery(com);
                this.ChargeSlipID = Convert.ToInt32(this.db.GetParameterValue(com, "ChargeSlipID"));      // Read in the output parameter value
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return this.ChargeSlipID > 0; // Return whether ID was returned
        }

        /// <summary>
        /// Updates details for ChargeSlip.
        /// </summary>
        /// <returns>True if Update operation is successful; Else False.</returns>
        private bool Update()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_ChargeSlipUpdate");
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
                if (!String.IsNullOrEmpty(this.DealName))
                {
                    this.db.AddInParameter(com, "DealName", DbType.String, this.DealName);
                }
                else
                {
                    this.db.AddInParameter(com, "DealName", DbType.String, DBNull.Value);
                }
                if (this.LocationID > 0)
                {
                    this.db.AddInParameter(com, "LocationID", DbType.Int32, this.LocationID);
                }
                else
                {
                    this.db.AddInParameter(com, "LocationID", DbType.Int32, DBNull.Value);
                }
                if (this.MapLocationID > 0)
                {
                    this.db.AddInParameter(com, "MapLocationID", DbType.Int32, this.MapLocationID);
                }
                else
                {
                    this.db.AddInParameter(com, "MapLocationID", DbType.Int32, 0);
                }
                if (this.ChargeDate > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "ChargeDate", DbType.DateTime, this.ChargeDate);
                }
                else
                {
                    this.db.AddInParameter(com, "ChargeDate", DbType.DateTime, DBNull.Value);
                }
                if (this.CompanyID > 0)
                {
                    this.db.AddInParameter(com, "CompanyID", DbType.Int32, this.CompanyID);
                }
                else
                {
                    this.db.AddInParameter(com, "CompanyID", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedBy > 0)
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
                }
                if (this.LastModifiedBy > 0)
                {
                    this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, this.LastModifiedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, DBNull.Value);
                }
                if (this.LastModifiedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, this.LastModifiedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, DBNull.Value);
                }
                //if (!String.IsNullOrEmpty(Status))
                //{
                    this.db.AddInParameter(com, "Status", DbType.Byte, this.Status);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "Status", DbType.String, DBNull.Value);
                //}
                if (this.DealStatusId > 0)
                {
                    this.db.AddInParameter(com, "DealStatusId", DbType.Int32, this.DealStatusId);
                }
                else
                {
                    this.db.AddInParameter(com, "DealStatusId", DbType.Int32, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocShoppingCenter))
                {
                    this.db.AddInParameter(com, "LocShoppingCenter", DbType.String, this.LocShoppingCenter);
                }
                else
                {
                    this.db.AddInParameter(com, "LocShoppingCenter", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocAddress1))
                {
                    this.db.AddInParameter(com, "LocAddress1", DbType.String, this.LocAddress1);
                }
                else
                {
                    this.db.AddInParameter(com, "LocAddress1", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocAddress2))
                {
                    this.db.AddInParameter(com, "LocAddress2", DbType.String, this.LocAddress2);
                }
                else
                {
                    this.db.AddInParameter(com, "LocAddress2", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocCity))
                {
                    this.db.AddInParameter(com, "LocCity", DbType.String, this.LocCity);
                }
                else
                {
                    this.db.AddInParameter(com, "LocCity", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocState))
                {
                    this.db.AddInParameter(com, "LocState", DbType.String, this.LocState);
                }
                else
                {
                    this.db.AddInParameter(com, "LocState", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocZipCode))
                {
                    this.db.AddInParameter(com, "LocZipCode", DbType.String, this.LocZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "LocZipCode", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LocCounty))
                {
                    this.db.AddInParameter(com, "LocCounty", DbType.String, this.LocCounty);
                }
                else
                {
                    this.db.AddInParameter(com, "LocCounty", DbType.String, DBNull.Value);
                }
                if (this.LocLatitude != 0)
                {
                    this.db.AddInParameter(com, "LocLatitude", DbType.Decimal, this.LocLatitude);
                }
                else
                {
                    this.db.AddInParameter(com, "LocLatitude", DbType.Double, DBNull.Value);
                }
                if (this.LocLongitude != 0)
                {
                    this.db.AddInParameter(com, "LocLongitude", DbType.Decimal, this.LocLongitude);
                }
                else
                {
                    this.db.AddInParameter(com, "LocLongitude", DbType.Double, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Company))
                {
                    this.db.AddInParameter(com, "Company", DbType.String, this.Company);
                }
                else
                {
                    this.db.AddInParameter(com, "Company", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTAddress1))
                {
                    this.db.AddInParameter(com, "CTAddress1", DbType.String, this.CTAddress1);
                }
                else
                {
                    this.db.AddInParameter(com, "CTAddress1", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTAddress2))
                {
                    this.db.AddInParameter(com, "CTAddress2", DbType.String, this.CTAddress2);
                }
                else
                {
                    this.db.AddInParameter(com, "CTAddress2", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTCity))
                {
                    this.db.AddInParameter(com, "CTCity", DbType.String, this.CTCity);
                }
                else
                {
                    this.db.AddInParameter(com, "CTCity", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTState))
                {
                    this.db.AddInParameter(com, "CTState", DbType.String, this.CTState);
                }
                else
                {
                    this.db.AddInParameter(com, "CTState", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTZipCode))
                {
                    this.db.AddInParameter(com, "CTZipCode", DbType.String, this.CTZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "CTZipCode", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTName))
                {
                    this.db.AddInParameter(com, "CTName", DbType.String, this.CTName);
                }
                else
                {
                    this.db.AddInParameter(com, "CTName", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTOwnershipEntity))
                {
                    this.db.AddInParameter(com, "CTOwnershipEntity", DbType.String, this.CTOwnershipEntity);
                }
                else
                {
                    this.db.AddInParameter(com, "CTOwnershipEntity", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTEmail))
                {
                    this.db.AddInParameter(com, "CTEmail", DbType.String, this.CTEmail);
                }
                else
                {
                    this.db.AddInParameter(com, "CTEmail", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CTCounty))
                {
                    this.db.AddInParameter(com, "CTCounty", DbType.String, this.CTCounty);
                }
                else
                {
                    this.db.AddInParameter(com, "CTCounty", DbType.String, DBNull.Value);
                }
                if (this.DealDesignationTypesId > 0)
                {
                    this.db.AddInParameter(com, "DealDesignationTypesId", DbType.Int32, this.DealDesignationTypesId);
                }
                else
                {
                    this.db.AddInParameter(com, "DealDesignationTypesId", DbType.Int32, DBNull.Value);
                }
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes details of ChargeSlip for provided ChargeSlipID.
        /// </summary>
        /// <returns>True if Delete operation is successful; Else False.</returns>
        public bool Delete()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_ChargeSlipDelete");
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get list of ChargeSlip for provided parameters.
        /// </summary>
        /// <returns>DataSet of result</returns>
        /// <remarks></remarks>
        public DataSet GetList()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_ChargeSlipGetList");
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }
        public DataSet GetListStatusCompleted()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_ChargeSlipGetListStatusCompleted");
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }
        public bool UpdateEditedChargeslip(bool boolupdate)
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_UpdateEditedChargeslipName");
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
                this.db.AddInParameter(com, "boolupdate", DbType.Boolean, boolupdate);
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }
        #region CustomActions
        public DataSet GetChargeSlipPagingListByStatus(int PageNo, int PageSize, int User)
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_GetDraftChargeSlipPagingList");
                this.db.AddInParameter(com, "@PageNo", DbType.String, PageNo);
                this.db.AddInParameter(com, "@PageSize", DbType.String, PageSize);
                this.db.AddInParameter(com, "@Status", DbType.Byte, this.Status);
                this.db.AddInParameter(com, "@User", DbType.String, User);
                ds = db.ExecuteDataSet(com);

            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }


        public Boolean DeleteChargeSlipRecords(string IDList)
        {
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_DeleteMultipleChargeSlips");
                this.db.AddInParameter(com, "@ChargeSlipIds", DbType.String, IDList);
                this.db.ExecuteNonQuery(com);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
        public Boolean PushDataToGeoApp()
        {
            try
            {
                if (this.ChargeSlipID > 0)
                {
                    DbCommand com = this.db.GetStoredProcCommand("usp_PropertyBasicAndPropertyDealsInsert");
                    this.db.AddInParameter(com, "PropertyId", DbType.Int32, 1024);
                    this.db.AddInParameter(com, "ChargeSlipID", DbType.Decimal, this.ChargeSlipID);
                    if (this.TotAnnualRent > 0)
                    {
                        this.db.AddInParameter(com, "TotAnnualRent", DbType.Decimal, this.TotAnnualRent);
                    }
                    else
                    {
                        this.db.AddInParameter(com, "TotAnnualRent", DbType.Decimal, 0);
                    }
                    if (this.UserIds !="")
                    {
                        this.db.AddInParameter(com, "UserIds", DbType.String, this.UserIds);
                    }
                    else
                    {
                        this.db.AddInParameter(com, "UserIds", DbType.String, DBNull.Value);
                    }
                    this.db.ExecuteNonQuery(com);
                    this.PropertyId = Convert.ToInt32(this.db.GetParameterValue(com, "PropertyId"));  
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                // To Do: Handle Exception 
                return false;
            }
        }

        public DataTable GetGeoAppUsers()
        {
            DataSet ds;
            DataTable dt = null;

            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_GetGeoAppUsers");
                ds = db.ExecuteDataSet(com);
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                return dt;
                throw;
            }
        }

        //// Insert Users From Segall Geo App--------------------------------
        //public Boolean PushDataToGeoApp()
        //{
 
        //}

        /// <summary>
        /// Get list of ChargeSlip for provided parameters optimized for custom paging technique.
        /// </summary>
        /// <param name="filter">Provide expression for filter result</param>
        /// <param name="sort">Provide expression to sort result</param>
        /// <param name="pageSize">Provide page size for custom paging.</param>
        /// <param name="pageNumber">Provide page number for custom paging</param>
        /// <param name="rowCount">Get total count of rows in result </param>
        /// <returns>DataSet of result</returns>
        /// <remarks></remarks>
        public DataSet GetPagingList(string filter, string sort, int pageSize, int pageNumber, int rowCount)
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_ChargeSlipGetPagingList");
                db.AddInParameter(com, "Filter", DbType.String, filter);
                db.AddInParameter(com, "Sort", DbType.String, sort);
                db.AddInParameter(com, "PageNo", DbType.Int32, pageSize);
                db.AddInParameter(com, "PageSize", DbType.Int32, pageNumber);
                db.AddInParameter(com, "RowCount", DbType.Int32, rowCount);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }

        /// <summary>
        /// Get list of ChargeSlip for provided parameters optimized for custom paging technique.
        /// </summary>
        /// <param name="expression">Provide expression for filter or sort result</param>
        /// <param name="isFilterExpression">If provided expression is filter expression then True; Else if sort expression then false.</param>
        /// <returns>DataSet of result</returns>
        /// <remarks></remarks>
        public DataSet GetPagingList(string expression, bool isFilterExpression)
        {
            if (isFilterExpression == true)
            {
                return GetPagingList(expression, "", 0, 0, 0);
            }
            else
            {
                return GetPagingList("", expression, 0, 0, 0);
            }
        }

        /// <summary>
        /// Get list of ChargeSlip for provided parameters optimized for custom paging technique.
        /// </summary>
        /// <param name="pageSize">Provide page size for custom paging.</param>
        /// <param name="pageNumber">Provide page number for custom paging</param>
        /// <param name="rowCount">Get total count of rows in result </param>
        /// <returns>DataSet of result</returns>
        /// <remarks></remarks>
        public DataSet GetPagingList(int pageSize, int pageNumber, int rowCount)
        {
            return GetPagingList("", "", pageSize, pageNumber, rowCount);
        }
        public bool SetResendChargeSlip()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_ChargeSlipSetResendEmail");
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }
        public bool SetDeleteChargeSlip(int loggedinuser)
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_ChargeSlipSetDelete");
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
                this.db.AddInParameter(com, "DeleteReason", DbType.String, this.DeleteReason);
                this.db.AddInParameter(com, "UserId", DbType.Int32, loggedinuser);
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }
            
        #endregion

        #endregion
    }
}
