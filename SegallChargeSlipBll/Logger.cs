﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Net;

public class CreateLogFiles
{
    private string sLogFormat;

    private string sErrorTime;
    public CreateLogFiles()
    {
        //sLogFormat used to create log files format :
        // dd/mm/yyyy hh:mm:ss AM/PM ==> Log Message
        sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";

        //this variable used to create log filename format "
        //for example filename : ErrorLogYYYYMMDD
        string sYear = DateTime.Now.Year.ToString();
        string sMonth = DateTime.Now.Month.ToString();
        string sDay = DateTime.Now.Day.ToString();
        sErrorTime = sYear + sMonth + sDay;
    }

    public void ErrorLog(string sPathName, string sErrMsg)
    {
        StreamWriter sw = new StreamWriter(sPathName + sErrorTime + ".txt", true);
        sw.WriteLine(sLogFormat + sErrMsg);
        sw.Flush();
        sw.Close();
    }

    //Commented  by Shishir 03-05-2016
    //public void WriteLog(string StrMessage, string MethodName = "")
    //{
    //    CreateLogFiles Err = new CreateLogFiles();
    //    string reportPath =System.Configuration.ConfigurationManager.AppSettings["ReportPath"];
    //    if (MethodName != "")
    //        Err.ErrorLog(reportPath + "\\log\\", StrMessage + " Occured in " + MethodName);
    //    else
    //        Err.ErrorLog(reportPath + "\\log\\", StrMessage);

    //    //Msg.Visible = True
    //    //Msg.Text = ">"  StrMessage +"  " + DateTime.Now() 
    //}

    //Modified by Shishir 03-05-2016
    public void WriteLog(string StrMessage,string ReportPath="", string MethodName = "")
    {
        CreateLogFiles Err = new CreateLogFiles();
        //string reportPath = System.Configuration.ConfigurationManager.AppSettings["ReportPath"];
        string reportPath = ReportPath;
        if (MethodName != "")
            Err.ErrorLog(reportPath , StrMessage + " Occured in " + MethodName);
        else
            Err.ErrorLog(reportPath , StrMessage);

        //Msg.Visible = True
        //Msg.Text = ">"  StrMessage +"  " + DateTime.Now() 
    }

}

public class SendMail
{
    public void SendErrorMail(string Message, string SendTo, string[] ClientList, string SenderName)
    {
        MailMessage email_msg = new MailMessage();
        string str_from_address = "support2009@traxsales.com";     //The From address (Email ID)    
        string str_name = SenderName;         //The Display Name  
        string str_to_address = SendTo;// "support@traxsales.com";     //The To address (Email ID)
        email_msg.From = new MailAddress(str_from_address, str_name);
        email_msg.Sender = new MailAddress(str_from_address, str_name);
        email_msg.ReplyTo = new MailAddress(str_from_address, str_name);
        email_msg.To.Add(new MailAddress(str_to_address, str_name));
        email_msg.Subject = "Proccess faild for date:" + DateTime.Now.AddDays(-1).ToShortDateString() + " at " + DateTime.Now.ToString();
        email_msg.Body = "<b> Hi, </b> <br/><br/> Proccess faild for date:" + DateTime.Now.AddDays(-1).ToShortDateString() + " ran at " + DateTime.Now.ToString() + " <br/><br/>  Error reason : <br/> '" + Message + "' <br/><br/>Thnaks, <br/> Traxsales.com ";
        email_msg.IsBodyHtml = true;
        foreach (string client in ClientList)
        {
            if (client.Trim() != "")
                email_msg.CC.Add(client);
        }

        email_msg.Priority = MailPriority.High;
        // email_msg.Attachments.Add(new Attachment(file));
        //Create an object for SmtpClient class
        SmtpClient mail_client = new SmtpClient();

        //Providing Credentials (Username & password)
        NetworkCredential network_cdr = new NetworkCredential();
        //network_cdr.UserName = str_from_address;
        //network_cdr.Password = "trax2009";

        //mail_client.Host = "smtp.Traxsales.com"; //SMTP host   

        network_cdr.UserName = "jay.pandya@rhealtech.com";
        network_cdr.Password = "jay1234";
        mail_client.Host = "smtp.rhealtech.com"; //SMTP host   
        mail_client.Port = 25;
        mail_client.UseDefaultCredentials = false;
        mail_client.Credentials = network_cdr;

        //Now Send the message
        mail_client.Send(email_msg);
    }
}

