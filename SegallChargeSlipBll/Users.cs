// -----------------------------------------------------------------------
// <copyright file="Users.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing Users
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class Users
{
    #region Basic Functionality

    #region Variable Declaration

    /// <summary>
    /// Variable to store Database object to interact with database.
    /// </summary>
    private Database db;
    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the Users class.
    /// </summary>
    public Users()
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
    }

    /// <summary>
    /// Initializes a new instance of the Users class.
    /// </summary>
    /// <param name="userID">Sets the value of UserID.</param>
    public Users(int userID)
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        this.UserID = userID;
    }
    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets UserID
    /// </summary>
    /// 
    public int UserType
    {
        get;
        set;

    }
    public int CurrentPageNo
    {
        get;
        set;

    }
    public int PageSize
    {
        get;
        set;
    }
    public int TotalRecords
    {
        get;
        set;
    }
    public int UserID
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets FirstName
    /// </summary>
    public string FirstName
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets MiddleName
    /// </summary>
    public string MiddleName
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets LastName
    /// </summary>
    public string LastName
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Username
    /// </summary>
    public string Username
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Password
    /// </summary>
    public string Password
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Email
    /// </summary>
    public string Email
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Mobile
    /// </summary>
    public string Mobile
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Address1
    /// </summary>
    public string Address1
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Address2
    /// </summary>
    public string Address2
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets City
    /// </summary>
    public string City
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets State
    /// </summary>
    public string State
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Country
    /// </summary>
    public string Country
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Zip
    /// </summary>
    public string Zip
    {
        get;
        set;
    }
    public string UserNotes
    {
        get;
        set;
    }
    public DateTime? LastModifiedNotesOn
         {
        get;
        set;
    }
    /// <summary>
    /// Gets or sets IsActive
    /// </summary>
    public bool IsActive
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets CreatedBy
    /// </summary>
    public int CreatedBy
    {
        get;
        set;
    }
    public int BrokerTypeId
    {
        get;
        set;
    }
    public Int64 LastModifiedNotesBy
    {
        get;
        set;
    }
    /// <summary>
    /// Gets or sets CreatedOn
    /// </summary>
    public DateTime CreatedOn
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets LastModifiedBy
    /// </summary>
    public int LastModifiedBy
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets LastModifiedOn
    /// </summary>
    public DateTime LastModifiedOn
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets UserTypeID
    /// </summary>
    public int UserTypeID
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets CompanyID
    /// </summary>
    public int CompanyID
    {
        get;
        set;
    }
    public string CompanyName
    {
        get;
        set;
    }
    public string EIN
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets SharePercent
    /// </summary>
    public double SharePercent
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets SetAmount
    /// </summary>
    public decimal SetAmount
    {
        get;
        set;
    }

    #endregion

    #region Actions

    /// <summary>
    /// Loads the details for Users.
    /// </summary>
    /// <returns>True if Load operation is successful; Else False.</returns>
    public bool Load()
    {
        try
        {
            if (this.UserID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_UsersGetDetails");
                this.db.AddInParameter(com, "UserID", DbType.Int32, this.UserID);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    this.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    this.FirstName = Convert.ToString(dt.Rows[0]["FirstName"]);
                    this.MiddleName = Convert.ToString(dt.Rows[0]["MiddleName"]);
                    this.LastName = Convert.ToString(dt.Rows[0]["LastName"]);
                    this.Username = Convert.ToString(dt.Rows[0]["Username"]);
                    this.Password = Convert.ToString(dt.Rows[0]["Password"]);
                    this.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    this.Mobile = Convert.ToString(dt.Rows[0]["Mobile"]);
                    this.Address1 = Convert.ToString(dt.Rows[0]["Address1"]);
                    this.Address2 = Convert.ToString(dt.Rows[0]["Address2"]);
                    this.City = Convert.ToString(dt.Rows[0]["City"]);
                    this.State = Convert.ToString(dt.Rows[0]["State"]);
                    this.Country = Convert.ToString(dt.Rows[0]["Country"]);
                    this.Zip = Convert.ToString(dt.Rows[0]["Zip"]);
                    this.IsActive = Convert.ToBoolean(dt.Rows[0]["IsActive"]);
                    this.CreatedBy = Convert.ToInt32(dt.Rows[0]["CreatedBy"]);
                    this.CreatedOn = Convert.ToDateTime(dt.Rows[0]["CreatedOn"]);
                    this.LastModifiedBy = Convert.ToInt32(dt.Rows[0]["LastModifiedBy"]);
                    this.LastModifiedOn = Convert.ToDateTime(dt.Rows[0]["LastModifiedOn"]);
                    this.UserTypeID = Convert.ToInt32(dt.Rows[0]["UserTypeID"]);
                    this.CompanyName = Convert.ToString(dt.Rows[0]["Company"]);
                    this.CompanyID = Convert.ToInt32(Convert.ToString(dt.Rows[0]["CompanyID"]));
                    this.EIN = Convert.ToString(dt.Rows[0]["EIN"]);
                    this.SharePercent = Convert.ToDouble(dt.Rows[0]["SharePercent"]);
                    this.SetAmount = Convert.ToDecimal(dt.Rows[0]["SetAmount"]);
                    this.BrokerTypeId = Convert.ToInt32(dt.Rows[0]["BrokerTypeId"]);
                    this.LastModifiedNotesBy = Convert.ToInt64(dt.Rows[0]["LastModifiedNotesBy"]);
                    if (!string.IsNullOrEmpty(dt.Rows[0]["LastModifiedNotesOn"].ToString()))
                    {
                        this.LastModifiedNotesOn = Convert.ToDateTime(dt.Rows[0]["LastModifiedNotesOn"]);
                    }
                    
                    this.UserNotes = Convert.ToString(dt.Rows[0]["UserNotes"]);
                    return true;
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }
    }

    /// <summary>
    /// Inserts details for Users if UserID = 0.
    /// Else updates details for Users.
    /// </summary>
    /// <returns>True if Save operation is successful; Else False.</returns>
    public bool Save()
    {
        if (this.UserID == 0)
        {
            this.CreatedOn = DateTime.Now;
            return this.Insert();
        }
        else
        {
            if (this.UserID > 0)
            {
                return this.Update();
            }
            else
            {
                this.UserID = 0;
                return false;
            }
        }
    }
    public void UserPasswordChange(int AdminUserid)
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_UserPasswordChange");
            if (AdminUserid > 0)
            {
                this.db.AddInParameter(com, "AdminUserid", DbType.Int32, AdminUserid);
            }
            else
            {
                this.db.AddInParameter(com, "AdminUserid", DbType.Int32, DBNull.Value);
            }
            if (this.UserID > 0)
            {
                this.db.AddInParameter(com, "UserID", DbType.Int32, this.UserID);
            }
            else
            {
                this.db.AddInParameter(com, "UserID", DbType.Int32, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Password))
            {
                this.db.AddInParameter(com, "UserPassword", DbType.String, this.Password);
            }
            else
            {
                this.db.AddInParameter(com, "UserPassword", DbType.String, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            ErrorLog.WriteLog("UserDetails", "UserThresoldExist", ex.Message);
        }
    }
    /// <summary>
    /// Inserts details for Users.
    /// Saves newly created Id in UserID.
    /// </summary>
    /// <returns>True if Insert operation is successful; Else False.</returns>
    private bool Insert()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_UsersInsert");
            this.db.AddOutParameter(com, "UserID", DbType.Int32, 1024);
            if (!String.IsNullOrEmpty(this.FirstName))
            {
                this.db.AddInParameter(com, "FirstName", DbType.String, this.FirstName);
            }
            else
            {
                this.db.AddInParameter(com, "FirstName", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.MiddleName))
            {
                this.db.AddInParameter(com, "MiddleName", DbType.String, this.MiddleName);
            }
            else
            {
                this.db.AddInParameter(com, "MiddleName", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.LastName))
            {
                this.db.AddInParameter(com, "LastName", DbType.String, this.LastName);
            }
            else
            {
                this.db.AddInParameter(com, "LastName", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Username))
            {
                this.db.AddInParameter(com, "Username", DbType.String, this.Username);
            }
            else
            {
                this.db.AddInParameter(com, "Username", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Password))
            {
                this.db.AddInParameter(com, "Password", DbType.String, this.Password);
            }
            else
            {
                this.db.AddInParameter(com, "Password", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Email))
            {
                this.db.AddInParameter(com, "Email", DbType.String, this.Email);
            }
            else
            {
                this.db.AddInParameter(com, "Email", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Mobile))
            {
                this.db.AddInParameter(com, "Mobile", DbType.String, this.Mobile);
            }
            else
            {
                this.db.AddInParameter(com, "Mobile", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Address1))
            {
                this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
            }
            else
            {
                this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Address2))
            {
                this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
            }
            else
            {
                this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.City))
            {
                this.db.AddInParameter(com, "City", DbType.String, this.City);
            }
            else
            {
                this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.State))
            {
                this.db.AddInParameter(com, "State", DbType.String, this.State);
            }
            else
            {
                this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Country))
            {
                this.db.AddInParameter(com, "Country", DbType.String, this.Country);
            }
            else
            {
                this.db.AddInParameter(com, "Country", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Zip))
            {
                this.db.AddInParameter(com, "Zip", DbType.String, this.Zip);
            }
            else
            {
                this.db.AddInParameter(com, "Zip", DbType.String, DBNull.Value);
            }
            this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
            if (this.CreatedBy > 0)
            {
                this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
            }
            else
            {
                this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
            }
            if (this.CreatedOn > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
            }
            else
            {
                this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
            }
            if (this.LastModifiedBy > 0)
            {
                this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, this.LastModifiedBy);
            }
            else
            {
                this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, DBNull.Value);
            }
            if (this.LastModifiedOn > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, this.LastModifiedOn);
            }
            else
            {
                this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, DBNull.Value);
            }
            if (this.UserTypeID > 0)
            {
                this.db.AddInParameter(com, "UserTypeID", DbType.Int32, this.UserTypeID);
            }
            else
            {
                this.db.AddInParameter(com, "UserTypeID", DbType.Int32, DBNull.Value);
            }
            if (this.CompanyID > 0)
            {
                this.db.AddInParameter(com, "CompanyID", DbType.Int32, this.CompanyID);
            }
            else
            {
                this.db.AddInParameter(com, "CompanyID", DbType.Int32, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.EIN))
            {
                this.db.AddInParameter(com, "EIN", DbType.String, this.EIN);
            }
            else
            {
                this.db.AddInParameter(com, "EIN", DbType.String, DBNull.Value);
            }
            if (this.SharePercent > 0)
            {
                this.db.AddInParameter(com, "SharePercent", DbType.Double, this.SharePercent);
            }
            else
            {
                this.db.AddInParameter(com, "SharePercent", DbType.Double, DBNull.Value);
            }
            if (this.SetAmount > 0)
            {
                this.db.AddInParameter(com, "SetAmount", DbType.Decimal, this.SetAmount);
            }
            else
            {
                this.db.AddInParameter(com, "SetAmount", DbType.Double, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.CompanyName))
            {
                this.db.AddInParameter(com, "CompanyName", DbType.String, this.CompanyName);
            }
            else
            {
                this.db.AddInParameter(com, "CompanyName", DbType.String, DBNull.Value);
            }
            if (this.BrokerTypeId > 0)
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, this.BrokerTypeId);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, DBNull.Value);
            }
            
            this.db.ExecuteNonQuery(com);
            this.UserID = Convert.ToInt32(this.db.GetParameterValue(com, "UserID"));      // Read in the output parameter value
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return this.UserID > 0; // Return whether ID was returned
    }

    /// <summary>
    /// Updates details for Users.
    /// </summary>
    /// <returns>True if Update operation is successful; Else False.</returns>
    private bool Update()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_UsersUpdate");
            this.db.AddInParameter(com, "UserID", DbType.Int32, this.UserID);
            if (!String.IsNullOrEmpty(this.FirstName))
            {
                this.db.AddInParameter(com, "FirstName", DbType.String, this.FirstName);
            }
            else
            {
                this.db.AddInParameter(com, "FirstName", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.MiddleName))
            {
                this.db.AddInParameter(com, "MiddleName", DbType.String, this.MiddleName);
            }
            else
            {
                this.db.AddInParameter(com, "MiddleName", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.LastName))
            {
                this.db.AddInParameter(com, "LastName", DbType.String, this.LastName);
            }
            else
            {
                this.db.AddInParameter(com, "LastName", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Username))
            {
                this.db.AddInParameter(com, "Username", DbType.String, this.Username);
            }
            else
            {
                this.db.AddInParameter(com, "Username", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Password))
            {
                this.db.AddInParameter(com, "Password", DbType.String, this.Password);
            }
            else
            {
                this.db.AddInParameter(com, "Password", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Email))
            {
                this.db.AddInParameter(com, "Email", DbType.String, this.Email);
            }
            else
            {
                this.db.AddInParameter(com, "Email", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Mobile))
            {
                this.db.AddInParameter(com, "Mobile", DbType.String, this.Mobile);
            }
            else
            {
                this.db.AddInParameter(com, "Mobile", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Address1))
            {
                this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
            }
            else
            {
                this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Address2))
            {
                this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
            }
            else
            {
                this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.City))
            {
                this.db.AddInParameter(com, "City", DbType.String, this.City);
            }
            else
            {
                this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.State))
            {
                this.db.AddInParameter(com, "State", DbType.String, this.State);
            }
            else
            {
                this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Country))
            {
                this.db.AddInParameter(com, "Country", DbType.String, this.Country);
            }
            else
            {
                this.db.AddInParameter(com, "Country", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Zip))
            {
                this.db.AddInParameter(com, "Zip", DbType.String, this.Zip);
            }
            else
            {
                this.db.AddInParameter(com, "Zip", DbType.String, DBNull.Value);
            }
            this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
            if (this.CreatedBy > 0)
            {
                this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
            }
            else
            {
                this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
            }
            if (this.CreatedOn > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
            }
            else
            {
                this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
            }
            if (this.LastModifiedBy > 0)
            {
                this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, this.LastModifiedBy);
            }
            else
            {
                this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, DBNull.Value);
            }
            if (this.LastModifiedOn > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, this.LastModifiedOn);
            }
            else
            {
                this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, DBNull.Value);
            }
            if (this.UserTypeID > 0)
            {
                this.db.AddInParameter(com, "UserTypeID", DbType.Int32, this.UserTypeID);
            }
            else
            {
                this.db.AddInParameter(com, "UserTypeID", DbType.Int32, DBNull.Value);
            }
            if (this.CompanyID > 0)
            {
                this.db.AddInParameter(com, "CompanyID", DbType.Int32, this.CompanyID);
            }
            else
            {
                this.db.AddInParameter(com, "CompanyID", DbType.Int32, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.EIN))
            {
                this.db.AddInParameter(com, "EIN", DbType.String, this.EIN);
            }
            else
            {
                this.db.AddInParameter(com, "EIN", DbType.String, DBNull.Value);
            }
            if (this.SharePercent > 0)
            {
                this.db.AddInParameter(com, "SharePercent", DbType.Double, this.SharePercent);
            }
            else
            {
                this.db.AddInParameter(com, "SharePercent", DbType.Double, DBNull.Value);
            }
            if (this.SetAmount > 0)
            {
                this.db.AddInParameter(com, "SetAmount", DbType.Decimal, this.SetAmount);
            }
            else
            {
                this.db.AddInParameter(com, "SetAmount", DbType.Double, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.CompanyName))
            {
                this.db.AddInParameter(com, "CompanyName", DbType.String, this.CompanyName);
            }
            else
            {
                this.db.AddInParameter(com, "CompanyName", DbType.String, DBNull.Value);
            }
            if (this.BrokerTypeId > 0)
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, this.BrokerTypeId);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, DBNull.Value);
            }
            if (this.LastModifiedNotesBy > 0)
            {
                this.db.AddInParameter(com, "LastModifiedNotesBy", DbType.Int32, this.LastModifiedNotesBy);
            }
            else
            {
                this.db.AddInParameter(com, "LastModifiedNotesBy", DbType.Int32, DBNull.Value);
            }
            if (this.LastModifiedNotesOn > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "LastModifiedNotesOn", DbType.DateTime, this.LastModifiedNotesOn);
            }
            else
            {
                this.db.AddInParameter(com, "LastModifiedNotesOn", DbType.DateTime, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.UserNotes))
            {
                this.db.AddInParameter(com, "UserNotes", DbType.String, this.UserNotes);
            }
            else
            {
                this.db.AddInParameter(com, "UserNotes", DbType.String, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Deletes details of Users for provided UserID.
    /// </summary>
    /// <returns>True if Delete operation is successful; Else False.</returns>
    public bool Delete()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_UsersDelete");
            this.db.AddInParameter(com, "UserID", DbType.Int32, this.UserID);
            //this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Get list of Users for provided parameters.
    /// </summary>
    /// <returns>DataSet of result</returns>
    /// <remarks></remarks>
    public DataSet GetList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_UsersGetList");
            this.db.AddOutParameter(com, "@TotalRecords", DbType.Int32, 0);
            if (!String.IsNullOrEmpty(this.Username))
            {
                this.db.AddInParameter(com, "Username", DbType.String, this.Username);
            }
            else
            {
                this.db.AddInParameter(com, "Username", DbType.String, DBNull.Value);
            }
            if (CurrentPageNo > 0)
            {
                this.db.AddInParameter(com, "@CurrentPageNo", DbType.Int32, this.CurrentPageNo);
            }
            else
            {
                this.db.AddInParameter(com, "@CurrentPageNo", DbType.Int32, DBNull.Value);
            }
            if (PageSize > 0)
            {
                this.db.AddInParameter(com, "@PageSize", DbType.Int32, this.PageSize);
            }
            else
            {
                this.db.AddInParameter(com, "@PageSize", DbType.Int32, DBNull.Value);
            }

            ds = db.ExecuteDataSet(com);
            this.TotalRecords = Convert.ToInt32(this.db.GetParameterValue(com, "@TotalRecords"));
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    public DataSet GetList(int pageNumber, int pageSize)
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_VendorGetList");
            if (!string.IsNullOrEmpty(this.CompanyName))
            {
                db.AddInParameter(com, "Company", DbType.String, this.CompanyName);
            }
            else
            {
                db.AddInParameter(com, "Company", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.City))
            {
                this.db.AddInParameter(com, "City", DbType.String, this.City);
            }
            else
            {
                this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.State))
            {
                this.db.AddInParameter(com, "State", DbType.String, this.State);
            }
            else
            {
                this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Zip))
            {
                this.db.AddInParameter(com, "ZipCode", DbType.String, this.Zip);
            }
            else
            {
                this.db.AddInParameter(com, "ZipCode", DbType.String, DBNull.Value);
            }
            if (pageNumber > 0)
                db.AddInParameter(com, "PageNo", DbType.Int32, pageNumber);
            else
                db.AddInParameter(com, "PageNo", DbType.Int32, 1);
            if (pageSize > 0)
                db.AddInParameter(com, "PageSize", DbType.Int32, pageSize);
            else
                db.AddInParameter(com, "PageSize", DbType.Int32, 20);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    #endregion
    public DataTable LoadUserDetails()
    {
        try
        {
            DataTable dt = new DataTable();
            if (this.UserID != 0)
            {

                DbCommand com = this.db.GetStoredProcCommand("usp_UserDetailsGetDetails");
                this.db.AddInParameter(com, "@UserID", DbType.Int32, this.UserID);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                    return dt;
                }
            }
            return dt;

        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return null;
        }
    }
    #region CustomAction
    public DataSet GetVendorInfoList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_VendorInfoList");
            this.db.AddInParameter(com, "UserID", DbType.Int32, this.UserID);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    public DataSet GetDealInfoListByUserId()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_DealInfoListByUserId");
            this.db.AddInParameter(com, "userid", DbType.Int32, this.UserID);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    public Users UserAunthentication(string username, string password)
    {
        DbCommand com = this.db.GetStoredProcCommand("usp_UserAunthenticationGetList");
        this.db.AddInParameter(com, "Username", DbType.String, username);

        DataSet ds = this.db.ExecuteDataSet(com);
        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        {
            DataTable dt = ds.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string Password = Convert.ToString(dt.Rows[i]["Password"]);
                string Username = Convert.ToString(dt.Rows[i]["Username"]);
                if (Password == password && username == Username)
                {
                    this.UserID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    this.Username = Convert.ToString(dt.Rows[i]["Username"]);
                    this.UserType = Convert.ToInt32(dt.Rows[i]["UserType"]);
                    this.FirstName = Convert.ToString(dt.Rows[i]["FirstName"]);
                    this.LastName = Convert.ToString(dt.Rows[i]["LastName"]);
                    return this;
                }
            }
            return null;
            //else
            //    return null;
        }
        else
            return null;
    }
    public DataSet GetCompanySearchList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_UserCompanyGetList");
            this.db.AddInParameter(com, "Company", DbType.String, this.CompanyName);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    public DataTable GetBrokerListByUsersCompany()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_GetBrokerListByUsersCompany");
            this.db.AddInParameter(com, "UserID", DbType.String, this.UserID);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds.Tables[0];
    }
    public DataSet CheckUsersEmailAlreadyExist(string Emailid, Int64 userid)
    {
        DataSet ds = new DataSet();
        ds = null;
        try
        {

            DbCommand com = this.db.GetStoredProcCommand("usp_UsersEmailIdAlreadyExist");
            this.db.AddInParameter(com, "EmailId", DbType.String, Emailid);
            this.db.AddInParameter(com, "UserId", DbType.Int64, userid);
            ds = this.db.ExecuteDataSet(com);
            return ds;
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return ds;
        }

    }
    public DataTable LoadUser()
    {
        try
        {
            DataTable dt = new DataTable();
            if (this.UserID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_UsersExist");
                this.db.AddInParameter(com, "UserID", DbType.Int32, this.UserID);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                    return dt;
                }
            }
            return dt;


        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return null;
        }
    }
    public DataTable GetUserAndAdminEmail()
    {

        try
        {
            DataTable dt = new DataTable();
            if (this.UserID != 0)
            {

                DbCommand com = this.db.GetStoredProcCommand("usp_UsersGetAdminEmail");
                this.db.AddInParameter(com, "@UserID", DbType.Int32, this.UserID);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                    return dt;
                }
            }


        }
        catch (Exception ex)
        {
            // To Do: Handle Exception

        }
        return null;
    }
   
   
    #endregion
    #endregion
}
