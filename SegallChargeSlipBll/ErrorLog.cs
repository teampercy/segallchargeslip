// -----------------------------------------------------------------------
// <copyright file="ErrorLog.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;

/// <summary>
/// Business class representing ErrorLog
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class ErrorLog
{
#region Basic Functionality

#region Variable Declaration

   /// <summary>
   /// Variable to store Database object to interact with database.
   /// </summary>
   private Database db;
#endregion

#region Constructors

   /// <summary>
   /// Initializes a new instance of the ErrorLog class.
   /// </summary>
   public ErrorLog()
   {
       this.db = DatabaseFactory.CreateDatabase("ConnectionString");
   }

   /// <summary>
   /// Initializes a new instance of the ErrorLog class.
   /// </summary>
   /// <param name="errorID">Sets the value of ErrorID.</param>
   public ErrorLog(int errorID)
   {
      this.db = DatabaseFactory.CreateDatabase();
      this.ErrorID = errorID;
   }
#endregion

#region Properties

   /// <summary>
   /// Gets or sets ErrorID
   /// </summary>
   public int ErrorID
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets ErrorSourcePage
   /// </summary>
   public string ErrorSourcePage
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets ErrorSourcePoint
   /// </summary>
   public string ErrorSourcePoint
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets Description
   /// </summary>
   public string Description
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets ErrorDateTime
   /// </summary>
   public DateTime ErrorDateTime
   {
      get; set;
   }
#endregion

#region Actions

   /// <summary>
   /// Loads the details for ErrorLog.
   /// </summary>
   /// <returns>True if Load operation is successful; Else False.</returns>
   public bool Load()
   {
      try
      {
         if (this.ErrorID != 0)
         {
            DbCommand com = this.db.GetStoredProcCommand("usp_ErrorLogGetDetails");
            this.db.AddInParameter(com, "ErrorID", DbType.Int32, this.ErrorID);
            DataSet ds = this.db.ExecuteDataSet(com);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
               DataTable dt = ds.Tables[0];
               this.ErrorID = Convert.ToInt32(dt.Rows[0]["ErrorID"]);
               this.ErrorSourcePage = Convert.ToString(dt.Rows[0]["ErrorSourcePage"]);
               this.ErrorSourcePoint = Convert.ToString(dt.Rows[0]["ErrorSourcePoint"]);
               this.Description = Convert.ToString(dt.Rows[0]["Description"]);
               this.ErrorDateTime = Convert.ToDateTime(dt.Rows[0]["ErrorDateTime"]);
               return true;
            }
         }

      return false;
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }
   }

   /// <summary>
   /// Inserts details for ErrorLog if ErrorID = 0.
   /// Else updates details for ErrorLog.
   /// </summary>
   /// <returns>True if Save operation is successful; Else False.</returns>
   public bool Save()
   {
      if (this.ErrorID == 0)
      {
         return this.Insert();
      }
      else
      {
         if (this.ErrorID > 0)
         {
            return this.Update();
         }
         else
         {
            this.ErrorID = 0;
            return false;
         }
      }
   }

   /// <summary>
   /// Inserts details for ErrorLog.
   /// Saves newly created Id in ErrorID.
   /// </summary>
   /// <returns>True if Insert operation is successful; Else False.</returns>
   private bool Insert()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_ErrorLogInsert");
         this.db.AddOutParameter(com, "ErrorID", DbType.Int32, 1024);
         if (!String.IsNullOrEmpty(this.ErrorSourcePage))
         {
            this.db.AddInParameter(com, "ErrorSourcePage", DbType.String, this.ErrorSourcePage);
         }
         else
         {
            this.db.AddInParameter(com, "ErrorSourcePage", DbType.String, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.ErrorSourcePoint))
         {
            this.db.AddInParameter(com, "ErrorSourcePoint", DbType.String, this.ErrorSourcePoint);
         }
         else
         {
            this.db.AddInParameter(com, "ErrorSourcePoint", DbType.String, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.Description))
         {
            this.db.AddInParameter(com, "Description", DbType.String, this.Description);
         }
         else
         {
            this.db.AddInParameter(com, "Description", DbType.String, DBNull.Value);
         }
         if (this.ErrorDateTime > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "ErrorDateTime", DbType.DateTime, this.ErrorDateTime);
         }
         else
         {
            this.db.AddInParameter(com, "ErrorDateTime", DbType.DateTime, DBNull.Value);
         }
         this.db.ExecuteNonQuery(com);
         this.ErrorID = Convert.ToInt32(this.db.GetParameterValue(com, "ErrorID"));      // Read in the output parameter value
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return this.ErrorID > 0; // Return whether ID was returned
   }

   /// <summary>
   /// Updates details for ErrorLog.
   /// </summary>
   /// <returns>True if Update operation is successful; Else False.</returns>
   private bool Update()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_ErrorLogUpdate");
         this.db.AddInParameter(com, "ErrorID", DbType.Int32, this.ErrorID);
         if (!String.IsNullOrEmpty(this.ErrorSourcePage))
         {
            this.db.AddInParameter(com, "ErrorSourcePage", DbType.String, this.ErrorSourcePage);
         }
         else
         {
            this.db.AddInParameter(com, "ErrorSourcePage", DbType.String, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.ErrorSourcePoint))
         {
            this.db.AddInParameter(com, "ErrorSourcePoint", DbType.String, this.ErrorSourcePoint);
         }
         else
         {
            this.db.AddInParameter(com, "ErrorSourcePoint", DbType.String, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.Description))
         {
            this.db.AddInParameter(com, "Description", DbType.String, this.Description);
         }
         else
         {
            this.db.AddInParameter(com, "Description", DbType.String, DBNull.Value);
         }
         if (this.ErrorDateTime > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "ErrorDateTime", DbType.DateTime, this.ErrorDateTime);
         }
         else
         {
            this.db.AddInParameter(com, "ErrorDateTime", DbType.DateTime, DBNull.Value);
         }
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Deletes details of ErrorLog for provided ErrorID.
   /// </summary>
   /// <returns>True if Delete operation is successful; Else False.</returns>
   public bool Delete()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_ErrorLogDelete");
         this.db.AddInParameter(com, "ErrorID", DbType.Int32, this.ErrorID);
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Get list of ErrorLog for provided parameters.
   /// </summary>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetList()
   {
      DataSet ds = null;
      try
      {
         DbCommand com = db.GetStoredProcCommand("usp_ErrorLogGetList");
         ds = db.ExecuteDataSet(com);
      }
      catch (Exception ex)
      {
         //To Do: Handle Exception
      }

      return ds;
   }
#endregion

#endregion

    #region Custom Actions

   public static void WriteLog(string loc, string src, string desc)
   {
       ErrorLog log = new ErrorLog();
       log.ErrorSourcePage = loc;
       log.ErrorSourcePoint = src;
       log.Description = desc;
       log.ErrorDateTime = DateTime.Now;
       log.Save();
      
   }

    #endregion
}
