// -----------------------------------------------------------------------
// <copyright file="CSSentToEmails.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing CSSentToEmails
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class CSSentToEmails
{
    #region Basic Functionality

    #region Variable Declaration

    /// <summary>
    /// Variable to store Database object to interact with database.
    /// </summary>
    private Database db;
    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the CSSentToEmails class.
    /// </summary>
    public CSSentToEmails()
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
    }

    /// <summary>
    /// Initializes a new instance of the CSSentToEmails class.
    /// </summary>
    /// <param name="cSSentToEmailId">Sets the value of CSSentToEmailId.</param>
    public CSSentToEmails(int cSSentToEmailId)
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        this.CSSentToEmailId = cSSentToEmailId;
    }
    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets CSSentToEmailId
    /// </summary>
    public int CSSentToEmailId
    {
        get; set;
    }

    /// <summary>
    /// Gets or sets SentToType
    /// </summary>
    public string SentToType
    {
        get; set;
    }

    /// <summary>
    /// Gets or sets EmailId
    /// </summary>
    public string EmailId
    {
        get; set;
    }
    #endregion

    #region Actions

    /// <summary>
    /// Loads the details for CSSentToEmails.
    /// </summary>
    /// <returns>True if Load operation is successful; Else False.</returns>
    public bool Load()
    {
        try
        {
            if (this.CSSentToEmailId != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_CSSentToEmailsGetDetails");
                this.db.AddInParameter(com, "CSSentToEmailId", DbType.Int32, this.CSSentToEmailId);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    this.CSSentToEmailId = Convert.ToInt32(dt.Rows[0]["CSSentToEmailId"]);
                    this.SentToType = Convert.ToString(dt.Rows[0]["SentToType"]);
                    this.EmailId = Convert.ToString(dt.Rows[0]["EmailId"]);
                    return true;
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }
    }

    /// <summary>
    /// Inserts details for CSSentToEmails if CSSentToEmailId = 0.
    /// Else updates details for CSSentToEmails.
    /// </summary>
    /// <returns>True if Save operation is successful; Else False.</returns>
    public bool Save()
    {
        if (this.CSSentToEmailId == 0)
        {
            return this.Insert();
        }
        else
        {
            if (this.CSSentToEmailId > 0)
            {
                return this.Update();
            }
            else
            {
                this.CSSentToEmailId = 0;
                return false;
            }
        }
    }

    /// <summary>
    /// Inserts details for CSSentToEmails.
    /// Saves newly created Id in CSSentToEmailId.
    /// </summary>
    /// <returns>True if Insert operation is successful; Else False.</returns>
    public void InsertEmail(string Accounting, string Accounting2, string Admin1, string Admin2, string NewsAdminEmail, string NewsAdminEmail1, string NewsAdminEmail2, string NewsAdminEmail3, string NewsAdminEmail4)
    {
        DbCommand com = this.db.GetStoredProcCommand("usp_CSSentToEmailsInsert");
        if (!String.IsNullOrEmpty(Accounting))
        {
            this.db.AddInParameter(com, "Accounting", DbType.String, Accounting);
        }
        else
        {
            this.db.AddInParameter(com, "Accounting", DbType.String, DBNull.Value);
        }
        if (!String.IsNullOrEmpty(Accounting2))
        {
            this.db.AddInParameter(com, "Accounting1", DbType.String, Accounting2);
        }
        else
        {
            this.db.AddInParameter(com, "Accounting1", DbType.String, DBNull.Value);
        }
        if (!String.IsNullOrEmpty(Admin1))
        {
            this.db.AddInParameter(com, "Admin1", DbType.String, Admin1);
        }
        else
        {
            this.db.AddInParameter(com, "Admin1", DbType.String, DBNull.Value);
        }
        if (!String.IsNullOrEmpty(Admin2))
        {
            this.db.AddInParameter(com, "Admin2", DbType.String, Admin2);
        }
        else
        {
            this.db.AddInParameter(com, "Admin2", DbType.String, DBNull.Value);
        }
        if (!String.IsNullOrEmpty(NewsAdminEmail))
        {
            this.db.AddInParameter(com, "NewsAdminEmail", DbType.String, NewsAdminEmail);
        }
        else
        {
            this.db.AddInParameter(com, "NewsAdminEmail", DbType.String, DBNull.Value);
        }
        if (!String.IsNullOrEmpty(NewsAdminEmail1))
        {
            this.db.AddInParameter(com, "NewsAdminEmail1", DbType.String, NewsAdminEmail1);
        }
        else
        {
            this.db.AddInParameter(com, "NewsAdminEmail1", DbType.String, DBNull.Value);
        }
        if (!String.IsNullOrEmpty(NewsAdminEmail2))
        {
            this.db.AddInParameter(com, "NewsAdminEmail2", DbType.String, NewsAdminEmail2);
        }
        else
        {
            this.db.AddInParameter(com, "NewsAdminEmail2", DbType.String, DBNull.Value);
        }
        if (!String.IsNullOrEmpty(NewsAdminEmail3))
        {
            this.db.AddInParameter(com, "NewsAdminEmail3", DbType.String, NewsAdminEmail3);
        }
        else
        {
            this.db.AddInParameter(com, "NewsAdminEmail3", DbType.String, DBNull.Value);
        }
        if (!String.IsNullOrEmpty(NewsAdminEmail4))
        {
            this.db.AddInParameter(com, "NewsAdminEmail4", DbType.String, NewsAdminEmail4);
        }
        else
        {
            this.db.AddInParameter(com, "NewsAdminEmail4", DbType.String, DBNull.Value);
        }
        this.db.ExecuteNonQuery(com);
    }
    private bool Insert()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_CSSentToEmailsInsert");
            this.db.AddOutParameter(com, "CSSentToEmailId", DbType.Int32, 1024);
            if (!String.IsNullOrEmpty(this.SentToType))
            {
                this.db.AddInParameter(com, "SentToType", DbType.String, this.SentToType);
            }
            else
            {
                this.db.AddInParameter(com, "SentToType", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.EmailId))
            {
                this.db.AddInParameter(com, "EmailId", DbType.String, this.EmailId);
            }
            else
            {
                this.db.AddInParameter(com, "EmailId", DbType.String, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
            this.CSSentToEmailId = Convert.ToInt32(this.db.GetParameterValue(com, "CSSentToEmailId"));      // Read in the output parameter value
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return this.CSSentToEmailId > 0; // Return whether ID was returned
    }

    /// <summary>
    /// Updates details for CSSentToEmails.
    /// </summary>
    /// <returns>True if Update operation is successful; Else False.</returns>
    private bool Update()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_CSSentToEmailsUpdate");
            this.db.AddInParameter(com, "CSSentToEmailId", DbType.Int32, this.CSSentToEmailId);
            if (!String.IsNullOrEmpty(this.SentToType))
            {
                this.db.AddInParameter(com, "SentToType", DbType.String, this.SentToType);
            }
            else
            {
                this.db.AddInParameter(com, "SentToType", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.EmailId))
            {
                this.db.AddInParameter(com, "EmailId", DbType.String, this.EmailId);
            }
            else
            {
                this.db.AddInParameter(com, "EmailId", DbType.String, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Deletes details of CSSentToEmails for provided CSSentToEmailId.
    /// </summary>
    /// <returns>True if Delete operation is successful; Else False.</returns>
    public bool Delete()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_CSSentToEmailsDelete");
            this.db.AddInParameter(com, "CSSentToEmailId", DbType.Int32, this.CSSentToEmailId);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Get list of CSSentToEmails for provided parameters.
    /// </summary>
    /// <returns>DataSet of result</returns>
    /// <remarks></remarks>
    public DataSet GetList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_CSSentToEmailsGetList");
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    public DataSet GetListBySentToType()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_CSSentToEmailsGetListBySentToType");
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    #endregion

    #endregion
}
