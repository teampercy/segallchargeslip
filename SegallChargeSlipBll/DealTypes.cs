﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class DealTypes
{
    #region Basic Functionality

    #region Variable Declaration

    /// <summary>
    /// Variable to store Database object to interact with database.
    /// </summary>
    private Database db;
    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the DealParties class.
    /// </summary>
    public DealTypes()
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
    }

    /// <summary>
    /// Initializes a new instance of the DealParties class.
    /// </summary>
    /// <param name="DealTypeID">Sets the value of DealTypeID.</param>
    public DealTypes(int DealTypeID)
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        this.DealTypeID = DealTypeID;
    }
    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets DealTypeID
    /// </summary>
    public int DealTypeID
    {
        get;
        set;
    }
    public bool IsActive
    {
        get;
        set;
    }
    /// <summary>
    /// Gets or sets DealType
    /// </summary>
    public string DealType
    {
        get;
        set;
    }

    public int CurrentPageNo
    {
        get;
        set;

    }
    public int PageSize
    {
        get;
        set;
    }
    public int TotalRecords
    {
        get;
        set;
    }
    #endregion

    #region Actions

    /// <summary>
    /// Loads the details for DealParties.
    /// </summary>
    /// <returns>True if Load operation is successful; Else False.</returns>
    public bool Load()
    {
        try
        {
            if (this.DealTypeID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_DealTypesGetDetails");
                this.db.AddInParameter(com, "DealTypeID", DbType.Int32, this.DealTypeID);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    this.DealTypeID = Convert.ToInt32(dt.Rows[0]["DealTypeID"]);
                    this.DealType = Convert.ToString(dt.Rows[0]["DealType"]);
                    this.IsActive = Convert.ToBoolean(dt.Rows[0]["IsActive"]);
           
                    return true;
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }
    }

    /// <summary>
    /// Inserts details for DealParties if DealTypeID = 0.
    /// Else updates details for DealParties.
    /// </summary>
    /// <returns>True if Save operation is successful; Else False.</returns>
    public bool Save()
    {
        if (this.DealTypeID == 0)
        {
            return this.Insert();
        }
        else
        {
            if (this.DealTypeID > 0)
            {
                return this.Update();
            }
            else
            {
                this.DealTypeID = 0;
                return false;
            }
        }
    }

    /// <summary>
    /// Inserts details for DealParties.
    /// Saves newly created Id in DealTypeID.
    /// </summary>
    /// <returns>True if Insert operation is successful; Else False.</returns>
    private bool Insert()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_DealTypesInsert");
            this.db.AddOutParameter(com, "DealTypeID", DbType.Int32, 1024);
            if (!String.IsNullOrEmpty(this.DealType))
            {
                this.db.AddInParameter(com, "DealType", DbType.String, this.DealType);
            }
            else
            {
                this.db.AddInParameter(com, "DealType", DbType.String, DBNull.Value);
            }
            this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
            this.db.ExecuteNonQuery(com);
            this.DealTypeID = Convert.ToInt32(this.db.GetParameterValue(com, "DealTypeID"));      // Read in the output parameter value
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return this.DealTypeID > 0; // Return whether ID was returned
    }

    /// <summary>
    /// Updates details for DealParties.
    /// </summary>
    /// <returns>True if Update operation is successful; Else False.</returns>
    private bool Update()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_DealTypesUpdate");
            this.db.AddInParameter(com, "DealTypeID", DbType.Int32, this.DealTypeID);
            if (!String.IsNullOrEmpty(this.DealType))
            {
                this.db.AddInParameter(com, "DealType", DbType.String, this.DealType);
            }
            else
            {
                this.db.AddInParameter(com, "DealType", DbType.String, DBNull.Value);
            }
            this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Deletes details of DealParties for provided DealTypeID.
    /// </summary>
    /// <returns>True if Delete operation is successful; Else False.</returns>
    public bool Delete()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_DealTypesDelete");
            this.db.AddInParameter(com, "DealTypeID", DbType.Int32, this.DealTypeID);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Get list of DealParties for provided parameters.
    /// </summary>
    /// <returns>DataSet of result</returns>
    /// <remarks></remarks>
    public DataSet GetList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_DealTypesGetList");
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }

    #endregion
    #region [CUSTOM ACTION]
    public DataTable LoadDealTransaction()
    {
        try
        {
            DataTable dt = new DataTable();
            if (this.DealTypeID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_DealTypeExist");
                this.db.AddInParameter(com, "DealTypeID", DbType.Int32, this.DealTypeID);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                    return dt;
                }
            }
            return dt;

            
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return null;
        }
    }
    #endregion
    #endregion
}
