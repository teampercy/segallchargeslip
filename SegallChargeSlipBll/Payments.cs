// -----------------------------------------------------------------------
// <copyright file="Payments.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing Payments
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class Payments
{
#region Basic Functionality

#region Variable Declaration

   /// <summary>
   /// Variable to store Database object to interact with database.
   /// </summary>
   private Database db;
#endregion

#region Constructors

   /// <summary>
   /// Initializes a new instance of the Payments class.
   /// </summary>
   public Payments()
   {
       this.db = DatabaseFactory.CreateDatabase("ConnectionString");
   }

   /// <summary>
   /// Initializes a new instance of the Payments class.
   /// </summary>
   /// <param name="paymentId">Sets the value of PaymentId.</param>
   public Payments(int paymentId)
   {
       this.db = DatabaseFactory.CreateDatabase("ConnectionString");
      this.PaymentId = paymentId;
   }
#endregion

#region Properties

   /// <summary>
   /// Gets or sets PaymentId
   /// </summary>
   public int PaymentId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets ChargeSlipId
   /// </summary>
   public int ChargeSlipId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets DealTransactionId
   /// </summary>
   public int DealTransactionId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets ReceivedFrom
   /// </summary>
   public int ReceivedFrom
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets PaymentAmount
   /// </summary>
   public decimal PaymentAmount
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets PaymentMethod
   /// </summary>
   public int PaymentMethod
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets BalanceAmount
   /// </summary>
   public decimal BalanceAmount
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets PaymentDate
   /// </summary>
   public DateTime PaymentDate
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets CheckNo
   /// </summary>
   public string CheckNo
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets PaymentStatus
   /// </summary>
   public int PaymentStatus
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets PaymentAppliedBy
   /// </summary>
   public int PaymentAppliedBy
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets CommissionDueId
   /// </summary>
   public int CommissionDueId
   {
      get; set;
   }
#endregion

#region Actions

   /// <summary>
   /// Loads the details for Payments.
   /// </summary>
   /// <returns>True if Load operation is successful; Else False.</returns>
   public bool Load()
   {
      try
      {
          //if (DateTime.Now <= DateTime.Parse("2014/09/03"))
          //{
              if (this.PaymentId != 0)
              {
                  DbCommand com = this.db.GetStoredProcCommand("usp_upPaymentsGetDetails");
                  this.db.AddInParameter(com, "PaymentId", DbType.Int32, this.PaymentId);
                  DataSet ds = this.db.ExecuteDataSet(com);
                  if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                  {
                      DataTable dt = ds.Tables[0];
                      this.PaymentId = Convert.ToInt32(dt.Rows[0]["PaymentId"]);
                      this.ChargeSlipId = Convert.ToInt32(dt.Rows[0]["ChargeSlipId"]);
                      this.DealTransactionId = Convert.ToInt32(dt.Rows[0]["DealTransactionId"]);
                      this.ReceivedFrom = Convert.ToInt32(dt.Rows[0]["ReceivedFrom"]);
                      this.PaymentAmount = Convert.ToDecimal(dt.Rows[0]["PaymentAmount"]);
                      this.PaymentMethod = Convert.ToInt32(dt.Rows[0]["PaymentMethod"]);
                      this.BalanceAmount = Convert.ToDecimal(dt.Rows[0]["BalanceAmount"]);
                      this.PaymentDate = Convert.ToDateTime(dt.Rows[0]["PaymentDate"]);
                      this.CheckNo = Convert.ToString(dt.Rows[0]["CheckNo"]);
                      this.PaymentStatus = Convert.ToInt32(dt.Rows[0]["PaymentStatus"]);
                      this.PaymentAppliedBy = Convert.ToInt32(dt.Rows[0]["PaymentAppliedBy"]);
                      this.CommissionDueId = Convert.ToInt32(dt.Rows[0]["CommissionDueId"]);
                      return true;
                  }
            //  }
          }

      return false;
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }
   }

   /// <summary>
   /// Inserts details for Payments if PaymentId = 0.
   /// Else updates details for Payments.
   /// </summary>
   /// <returns>True if Save operation is successful; Else False.</returns>
   public bool Save()
   {
      if (this.PaymentId == 0)
      {
         return this.Insert();
      }
      else
      {
         if (this.PaymentId > 0)
         {
            return this.Update();
         }
         else
         {
            this.PaymentId = 0;
            return false;
         }
      }
   }

   /// <summary>
   /// Inserts details for Payments.
   /// Saves newly created Id in PaymentId.
   /// </summary>
   /// <returns>True if Insert operation is successful; Else False.</returns>
   private bool Insert()
   {
      try
      {
          DbCommand com = this.db.GetStoredProcCommand("usp_PaymentsInsert");
         this.db.AddOutParameter(com, "PaymentId", DbType.Int32, 1024);
         if (this.ChargeSlipId > 0)
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
         }
         else
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
         }
         if (this.DealTransactionId > 0)
         {
            this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, this.DealTransactionId);
         }
         else
         {
            this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, DBNull.Value);
         }
         if (this.ReceivedFrom > 0)
         {
            this.db.AddInParameter(com, "ReceivedFrom", DbType.Int32, this.ReceivedFrom);
         }
         else
         {
            this.db.AddInParameter(com, "ReceivedFrom", DbType.Int32, DBNull.Value);
         }
         if (this.PaymentAmount > 0)
         {
            this.db.AddInParameter(com, "PaymentAmount", DbType.Decimal, this.PaymentAmount);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentAmount", DbType.Double, DBNull.Value);
         }
         if (this.PaymentMethod > 0)
         {
            this.db.AddInParameter(com, "PaymentMethod", DbType.Int32, this.PaymentMethod);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentMethod", DbType.Int32, DBNull.Value);
         }
         //if (this.BalanceAmount > 0)
         //{
         //   this.db.AddInParameter(com, "BalanceAmount", DbType.Decimal, this.BalanceAmount);
         //}
         //else
         //{
         //   this.db.AddInParameter(com, "BalanceAmount", DbType.Double, DBNull.Value);
         //}
         if (this.PaymentDate > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "PaymentDate", DbType.DateTime, this.PaymentDate);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentDate", DbType.DateTime, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.CheckNo))
         {
            this.db.AddInParameter(com, "CheckNo", DbType.String, this.CheckNo);
         }
         else
         {
            this.db.AddInParameter(com, "CheckNo", DbType.String, DBNull.Value);
         }
         //if (this.PaymentStatus > 0)
         //{
         //   this.db.AddInParameter(com, "PaymentStatus", DbType.Int32, this.PaymentStatus);
         //}
         //else
         //{
         //   this.db.AddInParameter(com, "PaymentStatus", DbType.Int32, DBNull.Value);
         //}
         if (this.PaymentAppliedBy > 0)
         {
            this.db.AddInParameter(com, "PaymentAppliedBy", DbType.Int32, this.PaymentAppliedBy);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentAppliedBy", DbType.Int32, DBNull.Value);
         }
         //if (this.CommissionDueId > 0)
         //{
         //   this.db.AddInParameter(com, "CommissionDueId", DbType.Int32, this.CommissionDueId);
         //}
         //else
         //{
         //   this.db.AddInParameter(com, "CommissionDueId", DbType.Int32, DBNull.Value);
         //}
         this.db.ExecuteNonQuery(com);
         this.PaymentId = Convert.ToInt32(this.db.GetParameterValue(com, "PaymentId"));      // Read in the output parameter value
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return this.PaymentId > 0; // Return whether ID was returned
   }

   /// <summary>
   /// Updates details for Payments.
   /// </summary>
   /// <returns>True if Update operation is successful; Else False.</returns>
   private bool Update()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_upPaymentsUpdate");
         this.db.AddInParameter(com, "PaymentId", DbType.Int32, this.PaymentId);
         if (this.ChargeSlipId > 0)
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
         }
         else
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
         }
         if (this.DealTransactionId > 0)
         {
            this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, this.DealTransactionId);
         }
         else
         {
            this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, DBNull.Value);
         }
         if (this.ReceivedFrom > 0)
         {
            this.db.AddInParameter(com, "ReceivedFrom", DbType.Int32, this.ReceivedFrom);
         }
         else
         {
            this.db.AddInParameter(com, "ReceivedFrom", DbType.Int32, DBNull.Value);
         }
         if (this.PaymentAmount > 0)
         {
            this.db.AddInParameter(com, "PaymentAmount", DbType.Decimal, this.PaymentAmount);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentAmount", DbType.Double, DBNull.Value);
         }
         if (this.PaymentMethod > 0)
         {
            this.db.AddInParameter(com, "PaymentMethod", DbType.Int32, this.PaymentMethod);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentMethod", DbType.Int32, DBNull.Value);
         }
         if (this.BalanceAmount > 0)
         {
            this.db.AddInParameter(com, "BalanceAmount", DbType.Decimal, this.BalanceAmount);
         }
         else
         {
            this.db.AddInParameter(com, "BalanceAmount", DbType.Double, DBNull.Value);
         }
         if (this.PaymentDate > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "PaymentDate", DbType.DateTime, this.PaymentDate);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentDate", DbType.DateTime, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.CheckNo))
         {
            this.db.AddInParameter(com, "CheckNo", DbType.String, this.CheckNo);
         }
         else
         {
            this.db.AddInParameter(com, "CheckNo", DbType.String, DBNull.Value);
         }
         if (this.PaymentStatus > 0)
         {
            this.db.AddInParameter(com, "PaymentStatus", DbType.Int32, this.PaymentStatus);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentStatus", DbType.Int32, DBNull.Value);
         }
         if (this.PaymentAppliedBy > 0)
         {
            this.db.AddInParameter(com, "PaymentAppliedBy", DbType.Int32, this.PaymentAppliedBy);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentAppliedBy", DbType.Int32, DBNull.Value);
         }
         if (this.CommissionDueId > 0)
         {
            this.db.AddInParameter(com, "CommissionDueId", DbType.Int32, this.CommissionDueId);
         }
         else
         {
            this.db.AddInParameter(com, "CommissionDueId", DbType.Int32, DBNull.Value);
         }
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Deletes details of Payments for provided PaymentId.
   /// </summary>
   /// <returns>True if Delete operation is successful; Else False.</returns>
   public bool Delete()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_upPaymentsDelete");
         this.db.AddInParameter(com, "PaymentId", DbType.Int32, this.PaymentId);
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }
   
   /// <summary>
   /// Get list of Payments for provided parameters.
   /// </summary>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetList()
   {
      DataSet ds = null;
      try
      {
         DbCommand com = db.GetStoredProcCommand("usp_upPaymentsGetList");
         ds = db.ExecuteDataSet(com);
      }
      catch (Exception ex)
      {
         //To Do: Handle Exception
      }

      return ds;
   }
   public DataSet GetListDealName(int CompId)
   {
       DataSet ds = null;
       try
       {
           DbCommand com = db.GetStoredProcCommand("usp_GetDealNameByCompId");
           db.AddInParameter(com, "CompanyID", DbType.Int32, CompId);
           ds = db.ExecuteDataSet(com);
       }
       catch (Exception ex)
       {
           //To Do: Handle Exception
       }

       return ds;
   }
   /// <summary>
   /// Get list of Payments for provided parameters optimized for custom paging technique.
   /// </summary>
   /// <param name="filter">Provide expression for filter result</param>
   /// <param name="sort">Provide expression to sort result</param>
   /// <param name="pageSize">Provide page size for custom paging.</param>
   /// <param name="pageNumber">Provide page number for custom paging</param>
   /// <param name="rowCount">Get total count of rows in result </param>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetPagingList(string filter, string sort, int pageSize, int pageNumber, int rowCount)
   {
      DataSet ds = null;
      try
      {
         DbCommand com = db.GetStoredProcCommand("usp_PaymentsGetPagingList");
         db.AddInParameter(com, "Filter", DbType.String, filter);
         db.AddInParameter(com, "Sort", DbType.String, sort);
         db.AddInParameter(com, "PageNo", DbType.Int32, pageSize);
         db.AddInParameter(com, "PageSize", DbType.Int32, pageNumber);
         db.AddInParameter(com, "RowCount", DbType.Int32, rowCount);
         ds = db.ExecuteDataSet(com);
      }
      catch (Exception ex)
      {
         //To Do: Handle Exception
      }

      return ds;
   }

   /// <summary>
   /// Get list of Payments for provided parameters optimized for custom paging technique.
   /// </summary>
   /// <param name="expression">Provide expression for filter or sort result</param>
   /// <param name="isFilterExpression">If provided expression is filter expression then True; Else if sort expression then false.</param>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetPagingList(string expression, bool isFilterExpression)
   {
      if (isFilterExpression == true)
      {
         return GetPagingList(expression, "", 0, 0, 0);
      }
      else
      {
         return GetPagingList("", expression, 0, 0, 0);
      }
   }

   /// <summary>
   /// Get list of Payments for provided parameters optimized for custom paging technique.
   /// </summary>
   /// <param name="pageSize">Provide page size for custom paging.</param>
   /// <param name="pageNumber">Provide page number for custom paging</param>
   /// <param name="rowCount">Get total count of rows in result </param>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetPagingList(int pageSize, int pageNumber, int rowCount)
   {
      return GetPagingList("", "", pageSize, pageNumber, rowCount);
   }
   public DataSet UpdatePaymentAmount(Int32 PaymentId, decimal Amount, Int32 AdminUserid)
   {
       DataSet dt = new DataSet();

       DbCommand com = this.db.GetStoredProcCommand("usp_UpdatePaymentAmount");
       if (AdminUserid > 0)
       {
           this.db.AddInParameter(com, "AdminUserid", DbType.Int32, AdminUserid);
       }
       else
       {
           this.db.AddInParameter(com, "AdminUserid", DbType.Int32, DBNull.Value);
       }
       this.db.AddInParameter(com, "@PaymentId", DbType.Int32, PaymentId);
       this.db.AddInParameter(com, "@Amount", DbType.Decimal, Amount);
       // DbCommand com = this.db.GetSqlStringCommand("UPDATE DealTransactions SET AdditionalNotes='" + this.AdditionalNotes + "' WHERE ChargeSlipID=" + this.ChargeSlipID);
       //this.db.ExecuteNonQuery(com);
       dt = this.db.ExecuteDataSet(com);
       return dt;
   }
   public DataTable PaymentPDFOpen(Int32 PaymentId)
   {
       DataTable dt = new DataTable();

       DbCommand com = this.db.GetStoredProcCommand("usp_GetPaymentDetail");
       this.db.AddInParameter(com, "@PaymentId", DbType.Int32, PaymentId);
       dt = this.db.ExecuteDataSet(com).Tables[0];
       return dt;
   }
   public DataSet GetIndividualBrokerPaymentMessage(Int32 PaymentHistoryId)
   {
       DataSet dt = new DataSet();

       DbCommand com = this.db.GetStoredProcCommand("usp_GetIndividualBrokerPaymentMessage");
       if (PaymentHistoryId > 0)
       {
           this.db.AddInParameter(com, "PaymentHistoryId", DbType.Int32, PaymentHistoryId);
       }
       else
       {
           this.db.AddInParameter(com, "PaymentHistoryId", DbType.Int32, DBNull.Value);
       }
       
       dt = this.db.ExecuteDataSet(com);
       return dt;
   }
   public DataSet GetPaymentMessage(Int32 MessageId)
   {
       DataSet dt = new DataSet();

       DbCommand com = this.db.GetStoredProcCommand("usp_GetPaymentMessage");
       if (MessageId > 0)
       {
           this.db.AddInParameter(com, "MessageId", DbType.Int32, MessageId);
       }
       else
       {
           this.db.AddInParameter(com, "MessageId", DbType.Int32, DBNull.Value);
       }

       dt = this.db.ExecuteDataSet(com);
       return dt;
   }
   //[Edit individual Broker payment Amount]

   public DataSet SaveIndividualBrokerPaymentEdit(Int32 PaymentHistoryId, decimal Amount, Int32 AdminUserid, Int32 ChargeslipId, string ReasonToEdit)
   {
       DataSet ds = new DataSet();
       DbCommand com = this.db.GetStoredProcCommand("usp_SaveIndividualBrokerPaymentEdit");
       if (AdminUserid > 0)
       {
           this.db.AddInParameter(com, "AdminUserid", DbType.Int32, AdminUserid);
       }
       else
       {
           this.db.AddInParameter(com, "AdminUserid", DbType.Int32, DBNull.Value);
       }
       this.db.AddInParameter(com, "@PaymentHistoryId", DbType.Int32, PaymentHistoryId);
       this.db.AddInParameter(com, "@ChargeslipId", DbType.Int32, ChargeslipId);
       this.db.AddInParameter(com, "@NewAmount", DbType.Decimal, Amount);
       this.db.AddInParameter(com, "@ReasonToEdit", DbType.String, ReasonToEdit);
       // DbCommand com = this.db.GetSqlStringCommand("UPDATE DealTransactions SET AdditionalNotes='" + this.AdditionalNotes + "' WHERE ChargeSlipID=" + this.ChargeSlipID);
       //this.db.ExecuteNonQuery(com);
       ds= this.db.ExecuteDataSet(com);
       return ds;
   }
   public void UpdateCreditAmount(Int32 CommissionDueId, decimal OutstandingAmount, Int32 AdminUserid)
   {
       //DataSet ds = new DataSet();
       DbCommand com = this.db.GetStoredProcCommand("usp_UpdateCreditAmount");
       if (AdminUserid > 0)
       {
           this.db.AddInParameter(com, "AdminUserid", DbType.Int32, AdminUserid);
       }
       else
       {
           this.db.AddInParameter(com, "AdminUserid", DbType.Int32, DBNull.Value);
       }
       if (CommissionDueId > 0)
       {
           this.db.AddInParameter(com, "CommissionDueId", DbType.Int32, CommissionDueId);
       }
       else
       {
           this.db.AddInParameter(com, "CommissionDueId", DbType.Int32, DBNull.Value);
       }
       if (OutstandingAmount > 0)
       {
           this.db.AddInParameter(com, "OutstandingAmount", DbType.Decimal, OutstandingAmount);
       }
       else
       {
           this.db.AddInParameter(com, "OutstandingAmount", DbType.Decimal, DBNull.Value);
       }
       
       // DbCommand com = this.db.GetSqlStringCommand("UPDATE DealTransactions SET AdditionalNotes='" + this.AdditionalNotes + "' WHERE ChargeSlipID=" + this.ChargeSlipID);
       this.db.ExecuteNonQuery(com);
       //ds = this.db.ExecuteDataSet(com);
       //return ds;
   }
   #region [Add for Payment Void on 15-07-2016]
   public int UpdateChargeslipPaymentVoid(Int32 ChargeslipId,Int32 Userid)
   {
       //DataSet ds = new DataSet();
       DbCommand com = this.db.GetStoredProcCommand("usp_ChargeslipPaymentVoid");
       if (ChargeslipId > 0)
       {
           this.db.AddInParameter(com, "ChargeslipId", DbType.Int32, ChargeslipId);
       }
       else
       {
           this.db.AddInParameter(com, "ChargeslipId", DbType.Int32, DBNull.Value);
       }
       if (Userid > 0)
       {
           this.db.AddInParameter(com, "UserId", DbType.Int32, Userid);
       }
       else
       {
           this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
       }
       return this.db.ExecuteNonQuery(com);
   }
   #endregion
   public void UpdateChargeslipOverPayment(Int32 ChargeslipId, Int32 PaymentId, decimal RemainingAmount)
   {
       //DataSet ds = new DataSet();
       DbCommand com = this.db.GetStoredProcCommand("usp_UpdateChargeslipOverPayment");
       if (ChargeslipId > 0)
       {
           this.db.AddInParameter(com, "ChargeslipId", DbType.Int32, ChargeslipId);
       }
       else
       {
           this.db.AddInParameter(com, "ChargeslipId", DbType.Int32, DBNull.Value);
       }
       if (PaymentId > 0)
       {
           this.db.AddInParameter(com, "PaymentId", DbType.Int32, PaymentId);
       }
       else
       {
           this.db.AddInParameter(com, "PaymentId", DbType.Int32, DBNull.Value);
       }
       if (RemainingAmount > 0)
       {
           this.db.AddInParameter(com, "RemainingAmount", DbType.Decimal, RemainingAmount);
       }
       else
       {
           this.db.AddInParameter(com, "RemainingAmount", DbType.Decimal, DBNull.Value);
       }

       // DbCommand com = this.db.GetSqlStringCommand("UPDATE DealTransactions SET AdditionalNotes='" + this.AdditionalNotes + "' WHERE ChargeSlipID=" + this.ChargeSlipID);
       this.db.ExecuteNonQuery(com);
       //ds = this.db.ExecuteDataSet(com);
       //return ds;
   }
    #region Broker Check Number Insert
   public void BrokerCheckNumberInsert(Int32 PaymentId, Int32 BrokerId, string CheckNumber,Int32 TermTypeId)
   {
       //DataSet ds = new DataSet();
       DbCommand com = this.db.GetStoredProcCommand("usp_BrokerCheckNumberInsert");
       if (PaymentId > 0)
       {
           this.db.AddInParameter(com, "PaymentId", DbType.Int32, PaymentId);
       }
       else
       {
           this.db.AddInParameter(com, "PaymentId", DbType.Int32, DBNull.Value);
       }
       if (BrokerId > 0)
       {
           this.db.AddInParameter(com, "BrokerId", DbType.Int32, BrokerId);
       }
       else
       {
           this.db.AddInParameter(com, "BrokerId", DbType.Int32, DBNull.Value);
       }
       if (!string.IsNullOrEmpty(CheckNumber))
       {
           this.db.AddInParameter(com, "CheckNumber", DbType.String, CheckNumber);
       }
       else
       {
           this.db.AddInParameter(com, "CheckNumber", DbType.String, DBNull.Value);
       }
       if (TermTypeId > 0)
       {
           this.db.AddInParameter(com, "TermTypeId", DbType.Int32, TermTypeId);
       }
       else
       {
           this.db.AddInParameter(com, "TermTypeId", DbType.Int32, DBNull.Value);
       }
       this.db.ExecuteNonQuery(com);      
   }
    #endregion
   #region Update Payment PDF Path
   public void UpdatePaymentPDFPath(Int32 PaymentId, string PDFPaymentPath)
   {
       DbCommand com = this.db.GetStoredProcCommand("usp_ChargeslipPaymentFilePathSaved");
       if (PaymentId > 0)
       {
           this.db.AddInParameter(com, "PaymentId", DbType.Int32, PaymentId);
       }
       else
       {
           this.db.AddInParameter(com, "PaymentId", DbType.Int32, DBNull.Value);
       }

       if (!string.IsNullOrEmpty(PDFPaymentPath))
       {
           this.db.AddInParameter(com, "ChargeslipPaymentPath", DbType.String, PDFPaymentPath);
       }
       else
       {
           this.db.AddInParameter(com, "ChargeslipPaymentPath", DbType.String, DBNull.Value);
       }
      
       this.db.ExecuteNonQuery(com);
   }
   #endregion
#endregion
#endregion
}
