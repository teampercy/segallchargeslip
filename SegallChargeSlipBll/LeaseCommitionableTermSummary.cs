﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
namespace SegallChargeSlipBll
{
    [Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
    public class LeaseCommitionableTermSummary
    {
        #region Variables
     //   string _BaseTerm, _CommitionableTerm, _Options, _OptionsPayable, _OptionsCommissionable;
        #endregion

        #region Properties

        public string _BaseTerm
        {
            get;
            set;
        }

        public string _CommitionableTerm
        {
            get;
            set;
        }

        public string _Options
        {
            get;
            set;
        }

        public string _OptionsPayable
        {
            get;
            set;
        }

        public string _OptionsCommissionable
        {
            get;
            set;
        }

        #endregion
    }
}