﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SegallChargeSlipBLL
{
    public class CommissionDueDatesNotification
    {
        #region Variable Declaration
        private Database db;
        #endregion

        #region Properties
      public long  NotificationId                           {    get;     set;   }
      public long CommissionDueId                   {    get;     set;   }
      public int  IsNotifyOn                           {    get;     set;   }
      public DateTime?  IsNotifyEmailSendDate          {    get;     set;   }
      public bool  IsNotifyEmailSent                 {    get;     set;   }
        public DateTime? DueDate { get; set; }
        public long ChargeSlipId { get; set; }

        #endregion

        #region Constructors

        public CommissionDueDatesNotification()
        {
            this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        }
        #endregion

        #region functions
        public DataSet GetCommissionDueDatesByChargeSlip(long ChargeSlipId)
        {

            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("GetCommissionDueDatesByChargeSlip");
                this.db.AddInParameter(com, "ChargeSlipId", DbType.String, ChargeSlipId);
                ds = db.ExecuteDataSet(com);
            }
             catch (Exception ex)
            {
                ErrorLog.WriteLog("CommissionDueDatesNotification", "GetCommissionDueDatesByChargeSlip", ex.Message);
            }
            return ds;
        }

        public bool Save()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("InsertUpdateCommissionDueDatesNotification");
                if (this.CommissionDueId > 0)
                {
                    this.db.AddInParameter(com, "CommissionDueId", DbType.Int64, this.CommissionDueId);
                }
                else
                {
                    this.db.AddInParameter(com, "CommissionDueId", DbType.Int64, DBNull.Value);
                }
                if ( this.IsNotifyOn > 0)
                {
                    this.db.AddInParameter(com, "IsNotifyOn", DbType.Int32, this.IsNotifyOn);
                }
                else
                {
                    this.db.AddInParameter(com, "IsNotifyOn", DbType.Int32, DBNull.Value);
                }
                this.db.AddInParameter(com, "DueDate", DbType.DateTime, this.DueDate);
                if (this.ChargeSlipId > 0)
                {
                    this.db.AddInParameter(com, "ChargeSlipId", DbType.Int64, this.ChargeSlipId);
                }
                else
                {
                    this.db.AddInParameter(com, "ChargeSlipId", DbType.Int64, DBNull.Value);
                }
                this.db.ExecuteNonQuery(com);
                return true;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }
        }

        public bool Delete(string IsNotifyOn)
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("DeleteCommissionDueDatesNotification");
                if (this.CommissionDueId > 0)
                {
                    this.db.AddInParameter(com, "CommissionDueId", DbType.Int64, this.CommissionDueId);
                }
                else
                {
                    this.db.AddInParameter(com, "CommissionDueId", DbType.Int64, DBNull.Value);
                }
                if (!string.IsNullOrEmpty(IsNotifyOn))
                {
                    this.db.AddInParameter(com, "IsNotifyOn", DbType.String,  IsNotifyOn);
                }
                else
                {
                    this.db.AddInParameter(com, "IsNotifyOn", DbType.String, DBNull.Value);
                }
                this.db.ExecuteNonQuery(com);
                return true;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }
            
        }

        public DataSet BindNotificationDaysByChargeSlip(long ChargeSlipId)
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("BindNotificationDaysByChargeSlip");
                this.db.AddInParameter(com, "ChargeSlipId", DbType.String, ChargeSlipId);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteLog("CommissionDueDatesNotification", "BindNotificationDaysByChargeSlip", ex.Message);
            }
            return ds;
        }

        #endregion
    }
}