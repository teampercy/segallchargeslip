﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
using System.Data.Common;

namespace SegallChargeSlipBll
{
    [Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
    public class ChargeSlipNewsForms
    {
        #region Basic Functionality

        #region Variable Declaration

        /// <summary>
        /// Variable to store Database object to interact with database.
        /// </summary>
        private Database db;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the Payments class.
        /// </summary>
        public ChargeSlipNewsForms()
        {
            this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or Sets ChargeSlipID
        /// </summary>
        /// 
        public int NewsFormsID
        { get; set; }

        public int ChargeSlipID
        { get; set; }

        public string BrokerName
        { get; set; }

        public string DealName
        { get; set; }

        public DateTime ChargeDate
        { get; set; }

        /// <summary>
        /// This property is not filled during Load(). Address is set during Insert() call.
        /// </summary>
        public string Address
        { get; set; }

        public string LocShoppingCenter
        { get; set; }

        public string LocAddress1
        { get; set; }

        public string LocAddress2
        { get; set; }

        public string LocCity
        { get; set; }

        public string LocState
        { get; set; }

        public string LocZipCode
        { get; set; }

        public string TenantName
        { get; set; }

        public string LandlordName
        { get; set; }

        public decimal Size
        { get; set; }

        //public string SizeIn
        //{ get; set; }

        public int UnitValue
        { get; set; }

        public int UnitTypeValue
        { get; set; }

        public string StatusOfLease
        { get; set; }

        public DateTime AnnounceDate
        { get; set; }

        public string Title
        { get; set; }

        public string Description
        { get; set; }

        public string TenantRepresentedBy
        { get; set; }

        public string LandlordRepresentedBy
        { get; set; }

        public bool Photos
        { get; set; }

        public string PhotosText
        { get; set; }

        public string AdditionalNotes
        { get; set; }

        public string EmailListTo
        { get; set; }

        /// <summary>
        /// Gets or sets CreatedBy
        /// </summary>
        public int CreatedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedOn
        /// </summary>
        public DateTime CreatedDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LastModifiedBy
        /// </summary>
        public int ModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LastModifiedOn
        /// </summary>
        public DateTime ModifiedDate
        {
            get;
            set;
        }

        #endregion

        #region Actions

        public bool Load()
        {
            try
            {
                //if (DateTime.Now <= DateTime.Parse("2014/09/03"))
                //{
                if (this.ChargeSlipID != 0)
                {
                    DbCommand com = this.db.GetStoredProcCommand("usp_ChargeslipNewsFormsGetDetail");
                    this.db.AddInParameter(com, "ChargeslipId", DbType.Int32, this.ChargeSlipID);
                    DataSet ds = this.db.ExecuteDataSet(com);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        //this.ChargeSlipID = Convert.ToInt32(dt.Rows[0]["ChargeSlipId"]);
                        this.BrokerName = Convert.ToString(dt.Rows[0]["BrokerName"]);
                        this.DealName = Convert.ToString(dt.Rows[0]["DealName"]);
                        this.ChargeDate = Convert.ToDateTime(dt.Rows[0]["ChargeDate"]);
                        this.LocShoppingCenter = Convert.ToString(dt.Rows[0]["LocShoppingCenter"]);
                        this.LocAddress1 = Convert.ToString(dt.Rows[0]["LocAddress1"]);
                        this.LocAddress2 = Convert.ToString(dt.Rows[0]["LocAddress2"]);
                        this.LocCity = Convert.ToString(dt.Rows[0]["LocCity"]);
                        this.LocState = Convert.ToString(dt.Rows[0]["LocState"]);
                        this.LocZipCode = Convert.ToString(dt.Rows[0]["LocZipCode"]);
                        this.TenantName = Convert.ToString(dt.Rows[0]["PartyCompanyName"]);
                        this.LandlordName = Convert.ToString(dt.Rows[0]["LandLordName"]);
                        this.Size = Convert.ToDecimal(dt.Rows[0]["Size"]);
                        this.UnitValue = Convert.ToInt32(dt.Rows[0]["UnitValue"]);//Since 1 is for Size and 2 for Time. We are dealing with size in this page, hence 1. 
                        this.UnitTypeValue = Convert.ToInt32(dt.Rows[0]["UnitTypeValue"]);
                         

                        return true;
                    }
                    //  }
                }

                return false;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }
        }


        /// <summary>
        /// Inserts and Updates News Form
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            if (this.NewsFormsID == 0)
            {
                return this.Insert();
            }
            else
            {
                if (this.NewsFormsID > 0)
                {
                    return this.Update();
                }
                else
                {
                    this.NewsFormsID = 0;
                    return false;
                }
            }
        }

        /// <summary>
        /// Inserts details for Payments.
        /// Saves newly created Id in PaymentId.
        /// </summary>
        /// <returns>True if Insert operation is successful; Else False.</returns>
        private bool Insert()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_ChargeslipNewsFormsInsert");
                this.db.AddOutParameter(com, "NewsFormsID", DbType.Int32, 1024);
                if (this.ChargeSlipID > 0)
                {
                    this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipID);
                }
                else
                {
                    this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.BrokerName))
                {
                    this.db.AddInParameter(com, "BrokerName", DbType.String, this.BrokerName);
                }
                else
                {
                    this.db.AddInParameter(com, "BrokerName", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.DealName))
                {
                    this.db.AddInParameter(com, "DealName", DbType.String, this.DealName);
                }
                else
                {
                    this.db.AddInParameter(com, "DealName", DbType.String, DBNull.Value);
                }
                if (this.ChargeDate > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "ChargeDate", DbType.DateTime, this.ChargeDate);
                }
                else
                {
                    this.db.AddInParameter(com, "ChargeDate", DbType.DateTime, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address))
                {
                    this.db.AddInParameter(com, "Address", DbType.String, this.Address);
                }
                else
                {
                    this.db.AddInParameter(com, "Address", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.TenantName))
                {
                    this.db.AddInParameter(com, "TenantBuyerName", DbType.String, this.TenantName);
                }
                else
                {
                    this.db.AddInParameter(com, "TenantBuyerName", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LandlordName))
                {
                    this.db.AddInParameter(com, "LandlordSellerName", DbType.String, this.LandlordName);
                }
                else
                {
                    this.db.AddInParameter(com, "LandlordSellerName", DbType.String, DBNull.Value);
                }
                if (this.Size > 0)
                {
                    this.db.AddInParameter(com, "Size", DbType.Decimal, this.Size);
                }
                else
                {
                    this.db.AddInParameter(com, "Size", DbType.Decimal, DBNull.Value);
                }
                if (this.UnitValue > 0)
                {
                    this.db.AddInParameter(com, "UnitValue", DbType.Int32, this.UnitValue);
                }
                else
                {
                    this.db.AddInParameter(com, "UnitValue", DbType.Int32, DBNull.Value);
                }
                if (this.UnitTypeValue > 0)
                {
                    this.db.AddInParameter(com, "UnitTypeValue", DbType.Int32, this.UnitTypeValue);
                }
                else
                {
                    this.db.AddInParameter(com, "UnitTypeValue", DbType.Int32, DBNull.Value);
                }
                //if (this.StatusOfLease > 0)
                //{
                //    this.db.AddInParameter(com, "StatusOfLease", DbType.Decimal, this.StatusOfLease);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "StatusOfLease", DbType.Decimal, DBNull.Value);
                //}
                if (!String.IsNullOrEmpty(this.StatusOfLease))
                {
                    this.db.AddInParameter(com, "StatusOfLease", DbType.String, this.StatusOfLease);
                }
                else
                {
                    this.db.AddInParameter(com, "StatusOfLease", DbType.String, DBNull.Value);
                }
                if (this.AnnounceDate > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "AnnounceDate", DbType.DateTime, this.AnnounceDate);
                }
                else
                {
                    this.db.AddInParameter(com, "AnnounceDate", DbType.DateTime, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Title))
                {
                    this.db.AddInParameter(com, "Title", DbType.String, this.Title);
                }
                else
                {
                    this.db.AddInParameter(com, "Title", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Description))
                {
                    this.db.AddInParameter(com, "Description", DbType.String, this.Description);
                }
                else
                {
                    this.db.AddInParameter(com, "Description", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.TenantRepresentedBy))
                {
                    this.db.AddInParameter(com, "TenantRepresentedBy", DbType.String, this.TenantRepresentedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "TenantRepresentedBy", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.LandlordRepresentedBy))
                {
                    this.db.AddInParameter(com, "LandlordRepresentedBy", DbType.String, this.LandlordRepresentedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "LandlordRepresentedBy", DbType.String, DBNull.Value);
                }

                this.db.AddInParameter(com, "Photos", DbType.Boolean, this.Photos);
                if (!String.IsNullOrEmpty(this.PhotosText))
                {
                    this.db.AddInParameter(com, "PhotosText", DbType.String, this.PhotosText);
                }
                else
                {
                    this.db.AddInParameter(com, "PhotosText", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.AdditionalNotes))
                {
                    this.db.AddInParameter(com, "AdditionalNotes", DbType.String, this.AdditionalNotes);
                }
                else
                {
                    this.db.AddInParameter(com, "AdditionalNotes", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.EmailListTo))
                {
                    this.db.AddInParameter(com, "EmailListTo", DbType.String, this.EmailListTo);
                }
                else
                {
                    this.db.AddInParameter(com, "EmailListTo", DbType.String, DBNull.Value);
                }
                if (this.CreatedBy > 0)
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedDate > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "CreatedDate", DbType.DateTime, this.CreatedDate);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedDate", DbType.DateTime, DBNull.Value);
                }
                if (this.ModifiedBy > 0)
                {
                    this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, this.ModifiedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, DBNull.Value);
                }
                if (this.ModifiedDate > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "ModifiedDate", DbType.DateTime, this.ModifiedDate);
                }
                else
                {
                    this.db.AddInParameter(com, "ModifiedDate", DbType.DateTime, DBNull.Value);
                }

                this.db.ExecuteNonQuery(com);
                this.NewsFormsID = Convert.ToInt32(this.db.GetParameterValue(com, "NewsFormsID"));      // Read in the output parameter value
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return this.NewsFormsID > 0; // Return whether ID was returned
        }

        /// <summary>
        /// Updates details for Payments.
        /// </summary>
        /// <returns>True if Update operation is successful; Else False.</returns>
        private bool Update()
        {
            try
            {
                //DbCommand com = this.db.GetStoredProcCommand("usp_upPaymentsUpdate");
                //this.db.AddInParameter(com, "PaymentId", DbType.Int32, this.PaymentId);
                //if (this.ChargeSlipId > 0)
                //{
                //    this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
                //}
                //if (this.DealTransactionId > 0)
                //{
                //    this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, this.DealTransactionId);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, DBNull.Value);
                //}
                //if (this.ReceivedFrom > 0)
                //{
                //    this.db.AddInParameter(com, "ReceivedFrom", DbType.Int32, this.ReceivedFrom);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "ReceivedFrom", DbType.Int32, DBNull.Value);
                //}
                //if (this.PaymentAmount > 0)
                //{
                //    this.db.AddInParameter(com, "PaymentAmount", DbType.Decimal, this.PaymentAmount);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "PaymentAmount", DbType.Double, DBNull.Value);
                //}
                //if (this.PaymentMethod > 0)
                //{
                //    this.db.AddInParameter(com, "PaymentMethod", DbType.Int32, this.PaymentMethod);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "PaymentMethod", DbType.Int32, DBNull.Value);
                //}
                //if (this.BalanceAmount > 0)
                //{
                //    this.db.AddInParameter(com, "BalanceAmount", DbType.Decimal, this.BalanceAmount);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "BalanceAmount", DbType.Double, DBNull.Value);
                //}
                //if (this.PaymentDate > DateTime.MinValue)
                //{
                //    this.db.AddInParameter(com, "PaymentDate", DbType.DateTime, this.PaymentDate);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "PaymentDate", DbType.DateTime, DBNull.Value);
                //}
                //if (!String.IsNullOrEmpty(this.CheckNo))
                //{
                //    this.db.AddInParameter(com, "CheckNo", DbType.String, this.CheckNo);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "CheckNo", DbType.String, DBNull.Value);
                //}
                //if (this.PaymentStatus > 0)
                //{
                //    this.db.AddInParameter(com, "PaymentStatus", DbType.Int32, this.PaymentStatus);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "PaymentStatus", DbType.Int32, DBNull.Value);
                //}
                //if (this.PaymentAppliedBy > 0)
                //{
                //    this.db.AddInParameter(com, "PaymentAppliedBy", DbType.Int32, this.PaymentAppliedBy);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "PaymentAppliedBy", DbType.Int32, DBNull.Value);
                //}
                //if (this.CommissionDueId > 0)
                //{
                //    this.db.AddInParameter(com, "CommissionDueId", DbType.Int32, this.CommissionDueId);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "CommissionDueId", DbType.Int32, DBNull.Value);
                //}
                //this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }

        #endregion

        #endregion
    }
}