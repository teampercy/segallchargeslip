// -----------------------------------------------------------------------
// <copyright file="UserDetails.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing UserDetails
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class UserDetails
{
    #region Basic Functionality

    #region Variable Declaration

    /// <summary>
    /// Variable to store Database object to interact with database.
    /// </summary>
    private Database db;
    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the UserDetails class.
    /// </summary>
    public UserDetails()
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
    }

    /// <summary>
    /// Initializes a new instance of the UserDetails class.
    /// </summary>
    /// <param name="userDetailsID">Sets the value of UserDetailsID.</param>
    public UserDetails(int userDetailsID)
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        this.UserDetailsID = userDetailsID;
    }
    #endregion

    #region Properties
    /// <summary>
    /// Gets or sets CreatedBy
    /// </summary>
    public int CreatedBy
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets CreatedOn
    /// </summary>
    public DateTime CreatedOn
    {
        get;
        set;
    }
    /// <summary>
    /// Gets or sets UserDetailsID
    /// </summary>
    public int UserDetailsID
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets UserID
    /// </summary>
    public bool IsActive
    {
        get;
        set;
    }
    public int UserID
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets FromDate
    /// </summary>
    public DateTime FromDate
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets ToDate
    /// </summary>
    public DateTime ToDate
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets EarningsFrom
    /// </summary>
    public decimal EarningsFrom
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets EarningsTo
    /// </summary>
    public decimal EarningsTo
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets PercentBroker
    /// </summary>
    public decimal PercentBroker
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets PercentCompany
    /// </summary>
    public decimal PercentCompany
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets LastModifiedBy
    /// </summary>
    public int LastModifiedBy
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets LastModifiedOn
    /// </summary>
    public DateTime LastModifiedOn
    {
        get;
        set;
    }

    // cUSTOM PROP

    /// <summary>
    /// Gets or sets LastModifiedBy
    /// </summary>
    public string CompanyName
    {
        get;
        set;
    }
    #endregion

    #region Actions

    /// <summary>
    /// Loads the details for UserDetails.
    /// </summary>
    /// <returns>True if Load operation is successful; Else False.</returns>
    public bool Load()
    {
        try
        {
            if (this.UserDetailsID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_UserDetailsGetDetails");
                this.db.AddInParameter(com, "UserDetailsID", DbType.Int32, this.UserDetailsID);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    this.UserDetailsID = Convert.ToInt32(dt.Rows[0]["UserDetailsID"]);
                    this.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    this.FromDate = Convert.ToDateTime(dt.Rows[0]["FromDate"]);
                    this.ToDate = Convert.ToDateTime(dt.Rows[0]["ToDate"]);
                    this.EarningsFrom = Convert.ToDecimal(dt.Rows[0]["EarningsFrom"]);
                    this.EarningsTo = Convert.ToDecimal(dt.Rows[0]["EarningsTo"]);
                    this.PercentBroker = Convert.ToDecimal(dt.Rows[0]["PercentBroker"]);
                    this.PercentCompany = Convert.ToDecimal(dt.Rows[0]["PercentCompany"]);
                    this.LastModifiedBy = Convert.ToInt32(dt.Rows[0]["LastModifiedBy"]);
                    this.LastModifiedOn = Convert.ToDateTime(dt.Rows[0]["LastModifiedOn"]);
                    return true;
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }
    }
    public void UserThresoldExist(int AdminUserid)
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_UsersThresholdExist");
            if (AdminUserid > 0)
            {
                this.db.AddInParameter(com, "AdminUserid", DbType.Int32, AdminUserid);
            }
            else
            {
                this.db.AddInParameter(com, "AdminUserid", DbType.Int32, DBNull.Value);
            }
            if (this.UserID > 0)
            {
                this.db.AddInParameter(com, "UserID", DbType.Int32, this.UserID);
            }
            else
            {
                this.db.AddInParameter(com, "UserID", DbType.Int32, DBNull.Value);
            }
            if (this.EarningsFrom >= 0)
            {
                this.db.AddInParameter(com, "EarningsFrom", DbType.Decimal, this.EarningsFrom);
            }
            else
            {
                this.db.AddInParameter(com, "EarningsFrom", DbType.Double, DBNull.Value);
            }
            if (this.EarningsTo >= 0)
            {
                this.db.AddInParameter(com, "EarningsTo", DbType.Decimal, this.EarningsTo);
            }
            else
            {
                this.db.AddInParameter(com, "EarningsTo", DbType.Double, DBNull.Value);
            }
            if (this.PercentBroker >= 0)
            {
                this.db.AddInParameter(com, "PercentBroker", DbType.Decimal, this.PercentBroker);
            }
            else
            {
                this.db.AddInParameter(com, "PercentBroker", DbType.Double, DBNull.Value);
            }
            if (this.PercentCompany >= 0)
            {
                this.db.AddInParameter(com, "PercentCompany", DbType.Decimal, this.PercentCompany);
            }
            else
            {
                this.db.AddInParameter(com, "PercentCompany", DbType.Double, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);

        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            ErrorLog.WriteLog("UserDetails", "UserThresoldExist", ex.Message);
        }
    }
    /// <summary>
    /// Inserts details for UserDetails if UserDetailsID = 0.
    /// Else updates details for UserDetails.
    /// </summary>
    /// <returns>True if Save operation is successful; Else False.</returns>
    public bool Save()
    {
        if (this.UserDetailsID == 0)
        {

            return this.Insert();
        }
        else
        {
            if (this.UserDetailsID > 0)
            {
                return this.Update();
            }
            else
            {
                this.UserDetailsID = 0;
                return false;
            }
        }
    }

    /// <summary>
    /// Inserts details for UserDetails.
    /// Saves newly created Id in UserDetailsID.
    /// </summary>
    /// <returns>True if Insert operation is successful; Else False.</returns>
    private bool Insert()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_UserDetailsInsert");
            this.db.AddOutParameter(com, "UserDetailsID", DbType.Int32, 1024);
            if (this.UserID > 0)
            {
                this.db.AddInParameter(com, "UserID", DbType.Int32, this.UserID);
            }
            else
            {
                this.db.AddInParameter(com, "UserID", DbType.Int32, DBNull.Value);
            }
            if (this.FromDate > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "FromDate", DbType.DateTime, this.FromDate);
            }
            else
            {
                this.db.AddInParameter(com, "FromDate", DbType.DateTime, DBNull.Value);
            }
            if (this.ToDate > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "ToDate", DbType.DateTime, this.ToDate);
            }
            else
            {
                this.db.AddInParameter(com, "ToDate", DbType.DateTime, DBNull.Value);
            }
            if (this.EarningsFrom >= 0)
            {
                this.db.AddInParameter(com, "EarningsFrom", DbType.Decimal, this.EarningsFrom);
            }
            else
            {
                this.db.AddInParameter(com, "EarningsFrom", DbType.Double, DBNull.Value);
            }
            if (this.EarningsTo >= 0)
            {
                this.db.AddInParameter(com, "EarningsTo", DbType.Decimal, this.EarningsTo);
            }
            else
            {
                this.db.AddInParameter(com, "EarningsTo", DbType.Double, DBNull.Value);
            }
            if (this.PercentBroker >= 0)
            {
                this.db.AddInParameter(com, "PercentBroker", DbType.Decimal, this.PercentBroker);
            }
            else
            {
                this.db.AddInParameter(com, "PercentBroker", DbType.Double, DBNull.Value);
            }
            if (this.PercentCompany >= 0)
            {
                this.db.AddInParameter(com, "PercentCompany", DbType.Decimal, this.PercentCompany);
            }
            else
            {
                this.db.AddInParameter(com, "PercentCompany", DbType.Double, DBNull.Value);
            }

            this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
            this.db.ExecuteNonQuery(com);
            this.UserDetailsID = Convert.ToInt32(this.db.GetParameterValue(com, "UserDetailsID"));      // Read in the output parameter value
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return this.UserDetailsID > 0; // Return whether ID was returned
    }

    /// <summary>
    /// Updates details for UserDetails.
    /// </summary>
    /// <returns>True if Update operation is successful; Else False.</returns>
    private bool Update()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_UserDetailsUpdate");
            this.db.AddInParameter(com, "UserDetailsID", DbType.Int32, this.UserDetailsID);
            if (this.UserID > 0)
            {
                this.db.AddInParameter(com, "UserID", DbType.Int32, this.UserID);
            }
            else
            {
                this.db.AddInParameter(com, "UserID", DbType.Int32, DBNull.Value);
            }
            if (this.FromDate > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "FromDate", DbType.DateTime, this.FromDate);
            }
            else
            {
                this.db.AddInParameter(com, "FromDate", DbType.DateTime, DBNull.Value);
            }
            if (this.ToDate > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "ToDate", DbType.DateTime, this.ToDate);
            }
            else
            {
                this.db.AddInParameter(com, "ToDate", DbType.DateTime, DBNull.Value);
            }
            if (this.EarningsFrom >= 0)
            {
                this.db.AddInParameter(com, "EarningsFrom", DbType.Decimal, this.EarningsFrom);
            }
            else
            {
                this.db.AddInParameter(com, "EarningsFrom", DbType.Double, DBNull.Value);
            }
            if (this.EarningsTo >= 0)
            {
                this.db.AddInParameter(com, "EarningsTo", DbType.Decimal, this.EarningsTo);
            }
            else
            {
                this.db.AddInParameter(com, "EarningsTo", DbType.Double, DBNull.Value);
            }
            if (this.PercentBroker >= 0)
            {
                this.db.AddInParameter(com, "PercentBroker", DbType.Decimal, this.PercentBroker);
            }
            else
            {
                this.db.AddInParameter(com, "PercentBroker", DbType.Double, DBNull.Value);
            }
            if (this.PercentCompany >= 0)
            {
                this.db.AddInParameter(com, "PercentCompany", DbType.Decimal, this.PercentCompany);
            }
            else
            {
                this.db.AddInParameter(com, "PercentCompany", DbType.Double, DBNull.Value);
            }

            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Deletes details of UserDetails for provided UserDetailsID.
    /// </summary>
    /// <returns>True if Delete operation is successful; Else False.</returns>
    public bool Delete()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_UserDetailsDelete");
            this.db.AddInParameter(com, "UserID", DbType.Int32, this.UserID);
            //this.db.AddInParameter(com, "IsActive", DbType.Int32, this.IsActive);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Get list of UserDetails for provided parameters.
    /// </summary>
    /// <returns>DataSet of result</returns>
    /// <remarks></remarks>
    public DataSet GetList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_UserDetailsGetList");
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }

    /// <summary>
    /// Get list of UserDetails for provided parameters optimized for custom paging technique.
    /// </summary>
    /// <param name="filter">Provide expression for filter result</param>
    /// <param name="sort">Provide expression to sort result</param>
    /// <param name="pageSize">Provide page size for custom paging.</param>
    /// <param name="pageNumber">Provide page number for custom paging</param>
    /// <param name="rowCount">Get total count of rows in result </param>
    /// <returns>DataSet of result</returns>
    /// <remarks></remarks>
    public DataSet GetPagingList(string filter, string sort, int pageSize, int pageNumber, int rowCount)
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_UserDetailsGetPagingList");
            db.AddInParameter(com, "Filter", DbType.String, filter);
            db.AddInParameter(com, "Sort", DbType.String, sort);
            db.AddInParameter(com, "PageNo", DbType.Int32, pageSize);
            db.AddInParameter(com, "PageSize", DbType.Int32, pageNumber);
            db.AddInParameter(com, "RowCount", DbType.Int32, rowCount);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }

    /// <summary>
    /// Get list of UserDetails for provided parameters optimized for custom paging technique.
    /// </summary>
    /// <param name="expression">Provide expression for filter or sort result</param>
    /// <param name="isFilterExpression">If provided expression is filter expression then True; Else if sort expression then false.</param>
    /// <returns>DataSet of result</returns>
    /// <remarks></remarks>
    public DataSet GetPagingList(string expression, bool isFilterExpression)
    {
        if (isFilterExpression == true)
        {
            return GetPagingList(expression, "", 0, 0, 0);
        }
        else
        {
            return GetPagingList("", expression, 0, 0, 0);
        }
    }

    /// <summary>
    /// Get list of UserDetails for provided parameters optimized for custom paging technique.
    /// </summary>
    /// <param name="pageSize">Provide page size for custom paging.</param>
    /// <param name="pageNumber">Provide page number for custom paging</param>
    /// <param name="rowCount">Get total count of rows in result </param>
    /// <returns>DataSet of result</returns>
    /// <remarks></remarks>
    public DataSet GetPagingList(int pageSize, int pageNumber, int rowCount)
    {
        return GetPagingList("", "", pageSize, pageNumber, rowCount);
    }


    #region Custom Action

    public bool LoadByUserIdAndCurrentDate(Int64 ChargeSlipId)//decimal CommAmount
    {
        try
        {
            if (this.UserID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_UserDetailsGetDetailsByUserId");
                this.db.AddInParameter(com, "UserID", DbType.Int32, this.UserID);
                this.db.AddInParameter(com, "ChargeSlipId", DbType.Int64, ChargeSlipId);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataTable dt = ds.Tables[1];
                    this.UserDetailsID = Convert.ToInt32(dt.Rows[0]["UserDetailsID"]);
                    this.UserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                    this.FromDate = Convert.ToDateTime(dt.Rows[0]["FromDate"]);
                    this.ToDate = Convert.ToDateTime(dt.Rows[0]["ToDate"]);
                    this.EarningsFrom = Convert.ToInt64(dt.Rows[0]["EarningsFrom"]);
                    this.EarningsTo = Convert.ToInt64(dt.Rows[0]["EarningsTo"]);
                    this.PercentBroker = Convert.ToDecimal(dt.Rows[0]["PercentBroker"]);
                    this.PercentCompany = Convert.ToDecimal(dt.Rows[0]["PercentCompany"]);
                    this.CompanyName = Convert.ToString(dt.Rows[0]["CompanyName"]);
                    return true;
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }
    }
    #endregion
    #endregion

    #endregion
}
