// -----------------------------------------------------------------------
// <copyright file="State.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing State
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class State
{
#region Basic Functionality

#region Variable Declaration

   /// <summary>
   /// Variable to store Database object to interact with database.
   /// </summary>
   private Database db;
#endregion

#region Constructors

   /// <summary>
   /// Initializes a new instance of the State class.
   /// </summary>
   public State()
   {
      this.db  = DatabaseFactory.CreateDatabase();
   }

   /// <summary>
   /// Initializes a new instance of the State class.
   /// </summary>
   /// <param name="stateID">Sets the value of StateID.</param>
   public State(int stateID)
   {
      this.db = DatabaseFactory.CreateDatabase();
      this.StateID = stateID;
   }
#endregion

#region Properties

   /// <summary>
   /// Gets or sets StateID
   /// </summary>
   public int StateID
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets State
   /// </summary>
   public string state
   {
      get; set;
   }
#endregion

#region Actions

   /// <summary>
   /// Loads the details for State.
   /// </summary>
   /// <returns>True if Load operation is successful; Else False.</returns>
   public bool Load()
   {
      try
      {
         if (this.StateID != 0)
         {
            DbCommand com = this.db.GetStoredProcCommand("");
            this.db.AddInParameter(com, "StateID", DbType.Int32, this.StateID);
            DataSet ds = this.db.ExecuteDataSet(com);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
               DataTable dt = ds.Tables[0];
               this.StateID = Convert.ToInt32(dt.Rows[0]["StateID"]);
               this.state = Convert.ToString(dt.Rows[0]["State"]);
               return true;
            }
         }

      return false;
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }
   }

   /// <summary>
   /// Inserts details for State if StateID = 0.
   /// Else updates details for State.
   /// </summary>
   /// <returns>True if Save operation is successful; Else False.</returns>
   public bool Save()
   {
      if (this.StateID == 0)
      {
         return this.Insert();
      }
      else
      {
         if (this.StateID > 0)
         {
            return this.Update();
         }
         else
         {
            this.StateID = 0;
            return false;
         }
      }
   }

   /// <summary>
   /// Inserts details for State.
   /// Saves newly created Id in StateID.
   /// </summary>
   /// <returns>True if Insert operation is successful; Else False.</returns>
   private bool Insert()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("");
         this.db.AddOutParameter(com, "StateID", DbType.Int32, 1024);
         if (!String.IsNullOrEmpty(this.state))
         {
             this.db.AddInParameter(com, "State", DbType.String, this.state);
         }
         else
         {
            this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
         }
         this.db.ExecuteNonQuery(com);
         this.StateID = Convert.ToInt32(this.db.GetParameterValue(com, "StateID"));      // Read in the output parameter value
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return this.StateID > 0; // Return whether ID was returned
   }

   /// <summary>
   /// Updates details for State.
   /// </summary>
   /// <returns>True if Update operation is successful; Else False.</returns>
   private bool Update()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("");
         this.db.AddInParameter(com, "StateID", DbType.Int32, this.StateID);
         if (!String.IsNullOrEmpty(this.state))
         {
             this.db.AddInParameter(com, "State", DbType.String, this.state);
         }
         else
         {
            this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
         }
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Get list of State for provided parameters.
   /// </summary>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetList()
   {
      DataSet ds = null;
      try
      {
         DbCommand com = db.GetStoredProcCommand("usp_upStateGetList");
         ds = db.ExecuteDataSet(com);
      }
      catch (Exception ex)
      {
         //To Do: Handle Exception
      }

      return ds;
   }
#endregion

#endregion
}
