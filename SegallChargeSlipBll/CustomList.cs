﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Reflection;
/// <summary>
/// Summary description for CustomList
/// </summary>
/// 
namespace SegallChargeSlipBll
{
    [Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
    public class CustomList
    {
        #region Variables
        Database db;
        #endregion

        public CustomList()
        {
            this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        }

        #region CustomActions

        public DataTable GetDealTypes()
        {
            DbCommand com = this.db.GetSqlStringCommand("SELECT DealTypeID,DealType FROM DealTypes where IsActive=1");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }
        public DataTable GetDealUseTypes()
        {
            DbCommand com = this.db.GetSqlStringCommand("SELECT DealUseId,DealUseType FROM DealUse where IsActive=1");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }

        public DataTable GetDealSubTypes()
        {
            DbCommand com = this.db.GetSqlStringCommand("SELECT DealSubTypeID,DealSubType,DealTypeID FROM DealSubTypes where IsActive=1");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }

        public DataTable GetLocationList()
        {
            DbCommand com = this.db.GetSqlStringCommand("select isnull(ShoppingCenter,Address1)as Name,LocationID from locations where (Address1 is not null or ShoppingCenter is not null) ORDER BY ShoppingCenter");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }

        public DataTable GetCompanyList()
        {
            DbCommand com = this.db.GetSqlStringCommand("SELECT ISNULL(Company,Name) as Company,CompanyID FROM COMPANY ORDER BY COMPANY");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }
        public DataTable GetDealName()
        {
            DbCommand com = this.db.GetSqlStringCommand("SELECT ChargeSlipID,DealName,CompanyID FROM ChargeSlip ORDER BY DealName");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }
        public DataTable GetBrokerList()
        {
            DbCommand com = this.db.GetSqlStringCommand("select UserId,FirstName+ ' '+ isnull(LastName,'') as Name from Users  where BrokerTypeId = 4 ORDER BY Name");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }

        //Added by Shishir 28-04-2016
        public DataTable GetBrokerListForAllBrokers()
        {
            DbCommand com = this.db.GetSqlStringCommand("select UserId,FirstName+ ' '+ isnull(LastName,'') as Name from Users where (UserTypeID<>1 or UserTypeID is null) ORDER BY Name");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }

        public DataTable GetOutsideBrokerList()
        {
            DbCommand com = this.db.GetSqlStringCommand("select UserId,FirstName+ ' '+ LastName as Name from Users  where BrokerTypeId in (1,2,3) ORDER BY Name");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }
        public DataTable GetUserList()
        {
            DbCommand com = this.db.GetSqlStringCommand("select FirstName+' '+ isnull(LastName,'') as Name,UserID from Users ");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }
        public DataTable GetUserCompanywiseList()
        {
            DbCommand com = this.db.GetSqlStringCommand("SELECT  distinct CompanyName,CompanyID FROM Users where CompanyID is not null ");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }
        public DataTable GetOwnerList()
        {
            DbCommand com = this.db.GetSqlStringCommand("SELECT OwnershipEntity,CompanyID FROM COMPANY where OwnershipEntity is not null");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }

        public DataTable GetStateList()
        {
            DbCommand com = this.db.GetSqlStringCommand("SELECT distinct State,StateName,StateSortOrder FROM ZipUSA ORDER BY StateSortOrder,StateName");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }

        //Modified by Shishir 02-05-2016. Added a Parameter
        public DataTable GetReportType(int UserType)
        {
            string Query;
            if(UserType==1)
            {
                Query = "SELECT  * FROM  ReportType ORDER BY ReportID";
            }
            else
            {
                Query = "SELECT  * FROM  ReportType Where ReportID <> 17 ORDER BY ReportID";
            }
            //DbCommand com = this.db.GetSqlStringCommand("SELECT  * FROM  ReportType ORDER BY ReportID");
            DbCommand com = this.db.GetSqlStringCommand(Query);
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }
        public DataTable GetTenant()
        {
            DbCommand com = this.db.GetSqlStringCommand("SELECT * FROM DealParties where PartyCompanyName is not null ORDER BY PartyCompanyName");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }

        public DataTable GetCountry()
        {
            DbCommand com = this.db.GetSqlStringCommand("SELECT * FROM Country ");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }
        public DataTable GetDealDesignationTypes()
        {
            DbCommand com = this.db.GetSqlStringCommand("SELECT DealDesignationTypesId,DealDesignationTypes FROM DealDesignationTypes where IsActive=1");
            DataSet ds = this.db.ExecuteDataSet(com);
            return ds.Tables[0];
        }
        public DataTable GetZipCodeSeachList(String City, String State)
        {
            DataSet ds = null;
            DataTable dt = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_ZipCodeSearchByCityState");
                this.db.AddInParameter(com, "State", DbType.String, State);
                this.db.AddInParameter(com, "City", DbType.String, City);
                ds = db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }

            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return dt;
        }

        #endregion

    }
}