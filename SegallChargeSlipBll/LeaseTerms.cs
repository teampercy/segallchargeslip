// -----------------------------------------------------------------------
// <copyright file="LeaseTerms.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing LeaseTerms
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class LeaseTerms
{
    #region Basic Functionality

    #region Variable Declaration

    /// <summary>
    /// Variable to store Database object to interact with database.
    /// </summary>
    private Database db;
    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the LeaseTerms class.
    /// </summary>
    public LeaseTerms()
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
    }

    /// <summary>
    /// Initializes a new instance of the LeaseTerms class.
    /// </summary>
    /// <param name="leaseTermId">Sets the value of LeaseTermId.</param>
    public LeaseTerms(int leaseTermId)
    {
        this.db = DatabaseFactory.CreateDatabase();
        this.LeaseTermId = leaseTermId;
    }
    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets LeaseTermId
    /// </summary>
    public int LeaseTermId
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets ChargeSlipId
    /// </summary>
    public int ChargeSlipId
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets DealTransactionId
    /// </summary>
    public int DealTransactionId
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets TermTypeId
    /// </summary>
    public int TermTypeId
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets RentPeriodFrom
    /// </summary>
    public int RentPeriodFrom
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets RentPeriodTo
    /// </summary>
    public int RentPeriodTo
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets TotalMonths
    /// </summary>
    public int TotalMonths
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets RentPerSqFt
    /// </summary>
    public decimal RentPerSqFt
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets AnnualRent
    /// </summary>
    public decimal AnnualRent
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets PercentIncrease
    /// </summary>
    public int PercentIncrease
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets PeriodRent
    /// </summary>
    public decimal PeriodRent
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets TotalSqFt
    /// </summary>
    public decimal TotalSqFt
    {
        get;
        set;
    }
    #endregion

    #region Actions

    /// <summary>
    /// Loads the details for LeaseTerms.
    /// </summary>
    /// <returns>True if Load operation is successful; Else False.</returns>
    public bool Load()
    {
        try
        {
            if (this.LeaseTermId != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_LeaseTermsGetDetails");
                this.db.AddInParameter(com, "LeaseTermId", DbType.Int32, this.LeaseTermId);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    this.LeaseTermId = Convert.ToInt32(dt.Rows[0]["LeaseTermId"]);
                    this.ChargeSlipId = Convert.ToInt32(dt.Rows[0]["ChargeSlipId"]);
                    this.DealTransactionId = Convert.ToInt32(dt.Rows[0]["DealTransactionId"]);
                    this.TermTypeId = Convert.ToInt32(dt.Rows[0]["TermTypeId"]);
                    this.RentPeriodFrom = Convert.ToInt32(dt.Rows[0]["RentPeriodFrom"]);
                    this.RentPeriodTo = Convert.ToInt32(dt.Rows[0]["RentPeriodTo"]);
                    this.TotalMonths = Convert.ToInt32(dt.Rows[0]["TotalMonths"]);
                    this.RentPerSqFt = Convert.ToDecimal(dt.Rows[0]["RentPerSqFt"]);
                    this.AnnualRent = Convert.ToDecimal(dt.Rows[0]["AnnualRent"]);
                    this.PercentIncrease = Convert.ToInt32(dt.Rows[0]["PercentIncrease"]);
                    this.PeriodRent = Convert.ToDecimal(dt.Rows[0]["PeriodRent"]);
                    this.TotalSqFt = Convert.ToDecimal(dt.Rows[0]["TotalSqFt"]);
                    return true;
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }
    }

    /// <summary>
    /// Inserts details for LeaseTerms if LeaseTermId = 0.
    /// Else updates details for LeaseTerms.
    /// </summary>
    /// <returns>True if Save operation is successful; Else False.</returns>
    public bool Save()
    {
        if (this.LeaseTermId == 0)
        {
            return this.Insert();
        }
        else
        {
            if (this.LeaseTermId > 0)
            {
                return this.Update();
            }
            else
            {
                this.LeaseTermId = 0;
                return false;
            }
        }
    }

    /// <summary>
    /// Inserts details for LeaseTerms.
    /// Saves newly created Id in LeaseTermId.
    /// </summary>
    /// <returns>True if Insert operation is successful; Else False.</returns>
    private bool Insert()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_LeaseTermsInsert");
            this.db.AddOutParameter(com, "LeaseTermId", DbType.Int32, 1024);
            if (this.ChargeSlipId > 0)
            {
                this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
            }
            else
            {
                this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
            }
            if (this.DealTransactionId > 0)
            {
                this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, this.DealTransactionId);
            }
            else
            {
                this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, DBNull.Value);
            }
            if (this.TermTypeId > 0)
            {
                this.db.AddInParameter(com, "TermTypeId", DbType.Int32, this.TermTypeId);
            }
            else
            {
                this.db.AddInParameter(com, "TermTypeId", DbType.Int32, DBNull.Value);
            }
            if (this.RentPeriodFrom > 0)
            {
                this.db.AddInParameter(com, "RentPeriodFrom", DbType.Int32, this.RentPeriodFrom);
            }
            else
            {
                this.db.AddInParameter(com, "RentPeriodFrom", DbType.Int32, DBNull.Value);
            }
            if (this.RentPeriodTo > 0)
            {
                this.db.AddInParameter(com, "RentPeriodTo", DbType.Int32, this.RentPeriodTo);
            }
            else
            {
                this.db.AddInParameter(com, "RentPeriodTo", DbType.Int32, DBNull.Value);
            }
            if (this.TotalMonths > 0)
            {
                this.db.AddInParameter(com, "TotalMonths", DbType.Int32, this.TotalMonths);
            }
            else
            {
                this.db.AddInParameter(com, "TotalMonths", DbType.Int32, DBNull.Value);
            }
            if (this.RentPerSqFt > 0)
            {
                this.db.AddInParameter(com, "RentPerSqFt", DbType.Decimal, this.RentPerSqFt);
            }
            else
            {
                this.db.AddInParameter(com, "RentPerSqFt", DbType.Double, DBNull.Value);
            }
            if (this.AnnualRent > 0)
            {
                this.db.AddInParameter(com, "AnnualRent", DbType.Decimal, this.AnnualRent);
            }
            else
            {
                this.db.AddInParameter(com, "AnnualRent", DbType.Double, DBNull.Value);
            }
            if (this.PercentIncrease > 0)
            {
                this.db.AddInParameter(com, "PercentIncrease", DbType.Int32, this.PercentIncrease);
            }
            else
            {
                this.db.AddInParameter(com, "PercentIncrease", DbType.Int32, DBNull.Value);
            }
            if (this.PeriodRent > 0)
            {
                this.db.AddInParameter(com, "PeriodRent", DbType.Decimal, this.PeriodRent);
            }
            else
            {
                this.db.AddInParameter(com, "PeriodRent", DbType.Double, DBNull.Value);
            }
            if (this.TotalSqFt > 0)
            {
                this.db.AddInParameter(com, "TotalSqFt", DbType.Decimal, this.TotalSqFt);
            }
            else
            {
                this.db.AddInParameter(com, "TotalSqFt", DbType.Double, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
            this.LeaseTermId = Convert.ToInt32(this.db.GetParameterValue(com, "LeaseTermId"));      // Read in the output parameter value
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return this.LeaseTermId > 0; // Return whether ID was returned
    }

    /// <summary>
    /// Updates details for LeaseTerms.
    /// </summary>
    /// <returns>True if Update operation is successful; Else False.</returns>
    private bool Update()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_LeaseTermsUpdate");
            this.db.AddInParameter(com, "LeaseTermId", DbType.Int32, this.LeaseTermId);
            if (this.ChargeSlipId > 0)
            {
                this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
            }
            else
            {
                this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
            }
            if (this.DealTransactionId > 0)
            {
                this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, this.DealTransactionId);
            }
            else
            {
                this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, DBNull.Value);
            }
            if (this.TermTypeId > 0)
            {
                this.db.AddInParameter(com, "TermTypeId", DbType.Int32, this.TermTypeId);
            }
            else
            {
                this.db.AddInParameter(com, "TermTypeId", DbType.Int32, DBNull.Value);
            }
            if (this.RentPeriodFrom > 0)
            {
                this.db.AddInParameter(com, "RentPeriodFrom", DbType.Int32, this.RentPeriodFrom);
            }
            else
            {
                this.db.AddInParameter(com, "RentPeriodFrom", DbType.Int32, DBNull.Value);
            }
            if (this.RentPeriodTo > 0)
            {
                this.db.AddInParameter(com, "RentPeriodTo", DbType.Int32, this.RentPeriodTo);
            }
            else
            {
                this.db.AddInParameter(com, "RentPeriodTo", DbType.Int32, DBNull.Value);
            }
            if (this.TotalMonths > 0)
            {
                this.db.AddInParameter(com, "TotalMonths", DbType.Int32, this.TotalMonths);
            }
            else
            {
                this.db.AddInParameter(com, "TotalMonths", DbType.Int32, DBNull.Value);
            }
            if (this.RentPerSqFt > 0)
            {
                this.db.AddInParameter(com, "RentPerSqFt", DbType.Decimal, this.RentPerSqFt);
            }
            else
            {
                this.db.AddInParameter(com, "RentPerSqFt", DbType.Double, DBNull.Value);
            }
            if (this.AnnualRent > 0)
            {
                this.db.AddInParameter(com, "AnnualRent", DbType.Decimal, this.AnnualRent);
            }
            else
            {
                this.db.AddInParameter(com, "AnnualRent", DbType.Double, DBNull.Value);
            }
            if (this.PercentIncrease > 0)
            {
                this.db.AddInParameter(com, "PercentIncrease", DbType.Int32, this.PercentIncrease);
            }
            else
            {
                this.db.AddInParameter(com, "PercentIncrease", DbType.Int32, DBNull.Value);
            }
            if (this.PeriodRent > 0)
            {
                this.db.AddInParameter(com, "PeriodRent", DbType.Decimal, this.PeriodRent);
            }
            else
            {
                this.db.AddInParameter(com, "PeriodRent", DbType.Double, DBNull.Value);
            }
            if (this.TotalSqFt > 0)
            {
                this.db.AddInParameter(com, "TotalSqFt", DbType.Decimal, this.TotalSqFt);
            }
            else
            {
                this.db.AddInParameter(com, "TotalSqFt", DbType.Double, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Deletes details of LeaseTerms for provided LeaseTermId.
    /// </summary>
    /// <returns>True if Delete operation is successful; Else False.</returns>
    public bool Delete()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_LeaseTermsDelete");
            this.db.AddInParameter(com, "LeaseTermId", DbType.Int32, this.LeaseTermId);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Get list of LeaseTerms for provided parameters.
    /// </summary>
    /// <returns>DataSet of result</returns>
    /// <remarks></remarks>
    public DataSet GetList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_LeaseTermsGetList");
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    #endregion

    #region CustomAction

    public bool SaveTb(DataTable dtLeaseTerm, DataTable dtCommTerm)
    {
        string ConnStr = this.db.ConnectionString;
        SqlConnection con = new SqlConnection(ConnStr);
        con.Open();
        SqlCommand cmd = new SqlCommand("usp_LeaseTermsUpdate", con);
        cmd.CommandType = CommandType.StoredProcedure;

        SqlParameter[] tblvaluetype = new SqlParameter[2];


        tblvaluetype[0] = cmd.Parameters.AddWithValue("@Insert_TVTLeaseTerm", dtLeaseTerm);  //Passing table value parameter
        tblvaluetype[0].SqlDbType = SqlDbType.Structured; // This one is used to tell ADO.NET we are passing Table value Parameter

        tblvaluetype[1] = cmd.Parameters.AddWithValue("@Insert_TVTCommissionableTerms", dtCommTerm);  //Passing table value parameter
        tblvaluetype[1].SqlDbType = SqlDbType.Structured; // This one is used to tell ADO.NET we are passing Table value Parameter


        int result = cmd.ExecuteNonQuery();
        //DataSet result =db.ExecuteDataSet(cmd);
        con.Close();
        if (result >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public DataSet GetListByChargeSlip()
    {
        DataSet ds = null;

        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_LeaseTermsGetListByChargeSlip");
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }
        return ds;
    }

    /// <summary>
    /// Deletes details of LeaseTerms for provided LeaseTermId.
    /// </summary>
    /// <returns>True if Delete operation is successful; Else False.</returns>
    public bool LeaseAndCommTermsDelete(Int32 ChargeSlipID)
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_LeaseAndCommTermDeleteAsPerChargeSlip");
            this.db.AddInParameter(com, "@ChargeSlipID", DbType.Int32, ChargeSlipID);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }
    #endregion


    #endregion
}
