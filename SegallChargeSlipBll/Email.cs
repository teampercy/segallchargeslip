﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Text;
using System.Threading.Tasks;
//using Microsoft.Reporting.WebForms;


public class Email
{

    #region Variables
    //string EmailBody, EmailSub, EmailFrom, EmailTo, EmailCC;
    #endregion

    #region Properties
    private Database db;
    public string EmailBody
    {
        get;
        set;
    }

    public string EmailFrom
    {
        get;
        set;
    }
    public string EmailTo
    {
        get;
        set;
    }

    public string EmailCC
    {
        get;
        set;
    }
    public string EmailSub
    {
        get;
        set;
    }

    // For sending PDF attachment -- read from memory steam
    public byte[] Memorybyte
    {
        get;
        set;
    }

    public string EmailAttachmentName
    {
        get;
        set;
    }
    #endregion


    public Boolean SendEmailWithPdfAttachment()
    {
        try
        {
            //if (DateTime.Now <= DateTime.Parse("2015/11/23"))
            //{
            MailMessage oMsg = new MailMessage();
            SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPHOST"]);
            String portNo = System.Configuration.ConfigurationManager.AppSettings["SMTPPORT"];

            oMsg.From = (new MailAddress(EmailFrom));
            oMsg.To.Add(new MailAddress(EmailTo));
            oMsg.Subject = EmailSub;
            oMsg.Body = EmailBody;
            oMsg.Attachments.Add(new Attachment(new MemoryStream(Memorybyte), EmailAttachmentName));
            oMsg.IsBodyHtml = true;
            System.Net.NetworkCredential basicAuthentication = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["USER"], System.Configuration.ConfigurationManager.AppSettings["PASSWORD"]);
            smtp.UseDefaultCredentials = false;
            smtp.Port = Int16.Parse(portNo);
            smtp.Credentials = basicAuthentication;
            smtp.Send(oMsg);
            // }

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }
    public Boolean SendEmail()
    {
        try
        {
            //if (DateTime.Now <= DateTime.Parse("2015/11/23"))
            //{
            MailMessage oMsg = new MailMessage();
            SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPHOST"]);
            String portNo = System.Configuration.ConfigurationManager.AppSettings["SMTPPORT"];

            oMsg.From = (new MailAddress(EmailFrom));
            oMsg.To.Add(new MailAddress(EmailTo));
            oMsg.Subject = EmailSub;
            oMsg.Body = EmailBody;
            oMsg.IsBodyHtml = true;
            System.Net.NetworkCredential basicAuthentication = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["USER"], System.Configuration.ConfigurationManager.AppSettings["PASSWORD"]);
            smtp.UseDefaultCredentials = false;
            smtp.Port = Int16.Parse(portNo);
            smtp.Credentials = basicAuthentication;
            smtp.Send(oMsg);
            //  }

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }


    public Email()
    {

        //db = DatabaseFactory.CreateDatabase("ConnectionString");

    }


}