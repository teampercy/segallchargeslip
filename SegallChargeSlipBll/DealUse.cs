// -----------------------------------------------------------------------
// <copyright file="DealUse.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing DealUse
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class DealUse
{
#region Basic Functionality

#region Variable Declaration

   /// <summary>
   /// Variable to store Database object to interact with database.
   /// </summary>
   private Database db;
#endregion

#region Constructors

   /// <summary>
   /// Initializes a new instance of the DealUse class.
   /// </summary>
   public DealUse()
   {
       this.db = DatabaseFactory.CreateDatabase("ConnectionString");
   }

   /// <summary>
   /// Initializes a new instance of the DealUse class.
   /// </summary>
   /// <param name="dealUseId">Sets the value of DealUseId.</param>
   public DealUse(int dealUseId)
   {
       this.db = DatabaseFactory.CreateDatabase("ConnectionString");
      this.DealUseId = dealUseId;
   }
#endregion

#region Properties

   /// <summary>
   /// Gets or sets DealUseId
   /// </summary>
   public int DealUseId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets DealUseType
   /// </summary>
   public string DealUseType
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets IsActive
   /// </summary>
   public bool IsActive
   {
      get; set;
   }
#endregion

#region Actions

   /// <summary>
   /// Loads the details for DealUse.
   /// </summary>
   /// <returns>True if Load operation is successful; Else False.</returns>
   public bool Load()
   {
      try
      {
         if (this.DealUseId != 0)
         {
            DbCommand com = this.db.GetStoredProcCommand("usp_DealUseGetDetails");
            this.db.AddInParameter(com, "DealUseId", DbType.Int32, this.DealUseId);
            DataSet ds = this.db.ExecuteDataSet(com);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
               DataTable dt = ds.Tables[0];
               this.DealUseId = Convert.ToInt32(dt.Rows[0]["DealUseId"]);
               this.DealUseType = Convert.ToString(dt.Rows[0]["DealUseType"]);
               this.IsActive = Convert.ToBoolean(dt.Rows[0]["IsActive"]);
               return true;
            }
         }

      return false;
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }
   }

   /// <summary>
   /// Inserts details for DealUse if DealUseId = 0.
   /// Else updates details for DealUse.
   /// </summary>
   /// <returns>True if Save operation is successful; Else False.</returns>
   public bool Save()
   {
      if (this.DealUseId == 0)
      {
         return this.Insert();
      }
      else
      {
         if (this.DealUseId > 0)
         {
            return this.Update();
         }
         else
         {
            this.DealUseId = 0;
            return false;
         }
      }
   }

   /// <summary>
   /// Inserts details for DealUse.
   /// Saves newly created Id in DealUseId.
   /// </summary>
   /// <returns>True if Insert operation is successful; Else False.</returns>
   private bool Insert()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_DealUseInsert");
         this.db.AddOutParameter(com, "DealUseId", DbType.Int32, 1024);
         if (!String.IsNullOrEmpty(this.DealUseType))
         {
            this.db.AddInParameter(com, "DealUseType", DbType.String, this.DealUseType);
         }
         else
         {
            this.db.AddInParameter(com, "DealUseType", DbType.String, DBNull.Value);
         }
         this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
         this.db.ExecuteNonQuery(com);
         this.DealUseId = Convert.ToInt32(this.db.GetParameterValue(com, "DealUseId"));      // Read in the output parameter value
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return this.DealUseId > 0; // Return whether ID was returned
   }

   /// <summary>
   /// Updates details for DealUse.
   /// </summary>
   /// <returns>True if Update operation is successful; Else False.</returns>
   private bool Update()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_DealUseUpdate");
         this.db.AddInParameter(com, "DealUseId", DbType.Int32, this.DealUseId);
         if (!String.IsNullOrEmpty(this.DealUseType))
         {
            this.db.AddInParameter(com, "DealUseType", DbType.String, this.DealUseType);
         }
         else
         {
            this.db.AddInParameter(com, "DealUseType", DbType.String, DBNull.Value);
         }
         this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Deletes details of DealUse for provided DealUseId.
   /// </summary>
   /// <returns>True if Delete operation is successful; Else False.</returns>
   public bool Delete()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_DealUseDelete");
         this.db.AddInParameter(com, "DealUseId", DbType.Int32, this.DealUseId);
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Get list of DealUse for provided parameters.
   /// </summary>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetList()
   {
      DataSet ds = null;
      try
      {
         DbCommand com = db.GetStoredProcCommand("usp_DealUseGetList");
         ds = db.ExecuteDataSet(com);
      }
      catch (Exception ex)
      {
         //To Do: Handle Exception
      }

      return ds;
   }
#endregion
   #region [CUSTOM ACTION]
   public DataTable LoadDealTransaction()
   {
       try
       {
           DataTable dt = new DataTable();
           if (this.DealUseId != 0)
           {
               DbCommand com = this.db.GetStoredProcCommand("usp_DealUseExist");
               this.db.AddInParameter(com, "DealUseId", DbType.Int32, this.DealUseId);
               DataSet ds = this.db.ExecuteDataSet(com);
               if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
               {
                   dt = ds.Tables[0];
                   return dt;
               }
           }
           return dt;


       }
       catch (Exception ex)
       {
           // To Do: Handle Exception
           return null;
       }
   }
   #endregion
#endregion
}
