// -----------------------------------------------------------------------
// <copyright file="Attachments.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing Attachments
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class Attachments
{
    #region Basic Functionality

    #region Variable Declaration

    /// <summary>
    /// Variable to store Database object to interact with database.
    /// </summary>
    private Database db;
    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the Attachments class.
    /// </summary>
    public Attachments()
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
    }

    /// <summary>
    /// Initializes a new instance of the Attachments class.
    /// </summary>
    /// <param name="attachmentID">Sets the value of AttachmentID.</param>
    public Attachments(int attachmentID)
    {
        this.db = DatabaseFactory.CreateDatabase();
        this.AttachmentID = attachmentID;
    }
    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets AttachmentID
    /// </summary>
    public int AttachmentID
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets AttachmentDescription
    /// </summary>
    public string AttachmentDescription
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets ChargeSlipID
    /// </summary>
    public int ChargeSlipID
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets CreatedBy
    /// </summary>
    public int CreatedBy
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets CreatedOn
    /// </summary>
    public DateTime CreatedOn
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets LastModifiedBy
    /// </summary>
    public int LastModifiedBy
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets LastModifiedOn
    /// </summary>
    public DateTime LastModifiedOn
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets AttachmentDescription
    /// </summary>
    public string FilePath
    {
        get;
        set;
    }
    #endregion

    #region Actions

    /// <summary>
    /// Loads the details for Attachments.
    /// </summary>
    /// <returns>True if Load operation is successful; Else False.</returns>
    public bool Load()
    {
        try
        {
            if (this.AttachmentID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_AttachmentsGetDetails");
                this.db.AddInParameter(com, "AttachmentID", DbType.Int32, this.AttachmentID);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    this.AttachmentID = Convert.ToInt32(dt.Rows[0]["AttachmentID"]);
                    this.AttachmentDescription = Convert.ToString(dt.Rows[0]["AttachmentDescription"]); 
                    this.ChargeSlipID = Convert.ToInt32(dt.Rows[0]["ChargeSlipID"]);
                    this.CreatedBy = Convert.ToInt32(dt.Rows[0]["CreatedBy"]);
                    this.CreatedOn = Convert.ToDateTime(dt.Rows[0]["CreatedOn"]);
                    this.LastModifiedBy = Convert.ToInt32(dt.Rows[0]["LastModifiedBy"]);
                    this.LastModifiedOn = Convert.ToDateTime(dt.Rows[0]["LastModifiedOn"]);
                    this.FilePath = Convert.ToString(dt.Rows[0]["FilePath"]); 
                    return true;
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }
    }

    /// <summary>
    /// Inserts details for Attachments if AttachmentID = 0.
    /// Else updates details for Attachments.
    /// </summary>
    /// <returns>True if Save operation is successful; Else False.</returns>
    public bool Save()
    {
        if (this.AttachmentID == 0)
        {
            return this.Insert();
        }
        else
        {
            if (this.AttachmentID > 0)
            {
                return this.Update();
            }
            else
            {
                this.AttachmentID = 0;
                return false;
            }
        }
    }

    /// <summary>
    /// Inserts details for Attachments.
    /// Saves newly created Id in AttachmentID.
    /// </summary>
    /// <returns>True if Insert operation is successful; Else False.</returns>
    private bool Insert()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_AttachmentsInsert");
            this.db.AddOutParameter(com, "AttachmentID", DbType.Int32, 1024);
            if (this.AttachmentDescription != null)
            {
                this.db.AddInParameter(com, "AttachmentDescription", DbType.String, this.AttachmentDescription);
            }
            else
            {
                this.db.AddInParameter(com, "AttachmentDescription", DbType.String, DBNull.Value);
            }
            if (this.ChargeSlipID > 0)
            {
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
            }
            else
            {
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, DBNull.Value);
            }
            if (this.CreatedBy > 0)
            {
                this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
            }
            else
            {
                this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
            }
            if (this.CreatedOn > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
            }
            else
            {
                this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
            }
            if (this.LastModifiedBy > 0)
            {
                this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, this.LastModifiedBy);
            }
            else
            {
                this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, DBNull.Value);
            }
            if (this.LastModifiedOn > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, this.LastModifiedOn);
            }
            else
            {
                this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, DBNull.Value);
            }
            if (this.FilePath != null)
            {
                this.db.AddInParameter(com, "FilePath", DbType.String, this.FilePath);
            }
            else
            {
                this.db.AddInParameter(com, "FilePath", DbType.String, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
            this.AttachmentID = Convert.ToInt32(this.db.GetParameterValue(com, "AttachmentID"));      // Read in the output parameter value
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return this.AttachmentID > 0; // Return whether ID was returned
    }

    /// <summary>
    /// Updates details for Attachments.
    /// </summary>
    /// <returns>True if Update operation is successful; Else False.</returns>
    private bool Update()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_AttachmentsUpdate");
            this.db.AddInParameter(com, "AttachmentID", DbType.Int32, this.AttachmentID);
            if (this.AttachmentDescription != null)
            {
                this.db.AddInParameter(com, "AttachmentDescription", DbType.String, this.AttachmentDescription);
            }
            else
            {
                this.db.AddInParameter(com, "AttachmentDescription", DbType.String, DBNull.Value);
            }
            if (this.ChargeSlipID > 0)
            {
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
            }
            else
            {
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, DBNull.Value);
            }
            if (this.CreatedBy > 0)
            {
                this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
            }
            else
            {
                this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
            }
            if (this.CreatedOn > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
            }
            else
            {
                this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
            }
            if (this.LastModifiedBy > 0)
            {
                this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, this.LastModifiedBy);
            }
            else
            {
                this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, DBNull.Value);
            }
            if (this.LastModifiedOn > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, this.LastModifiedOn);
            }
            else
            {
                this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, DBNull.Value);
            }
            if (this.FilePath != null)
            {
                this.db.AddInParameter(com, "FilePath", DbType.String, this.FilePath);
            }
            else
            {
                this.db.AddInParameter(com, "FilePath", DbType.String, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Deletes details of Attachments for provided AttachmentID.
    /// </summary>
    /// <returns>True if Delete operation is successful; Else False.</returns>
    public bool Delete()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_AttachmentDelete");
            this.db.AddInParameter(com, "AttachmentID", DbType.String, this.AttachmentID);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Get list of Attachments for provided parameters.
    /// </summary>
    /// <returns>DataSet of result</returns>
    /// <remarks></remarks>
    public DataSet GetList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_AttachmentsGetList");
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }



    // Custom Action

    public DataTable GetDetailsByUserAndChargeSlip()
    {
        DataSet ds = null;
        DataTable dt = null;
        try
        {
            if (this.CreatedBy != 0 && this.ChargeSlipID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_AttachmentsGetDetailsByUserAndChargeSlip");
                this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
                ds = this.db.ExecuteDataSet(com);
                if (ds != null)
                {
                    dt = ds.Tables[0];
                }
            }

        }
        catch (Exception ex)
        {
            // To Do: Handle Exception

        }
        return dt;
    }

    public DataTable GetDetailsByChargeSlip()
    {
        DataSet ds = null;
        DataTable dt = null;
        try
        {
            if (this.ChargeSlipID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_AttachmentsGetDetailsAsPerChargeSlip");
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
                ds = this.db.ExecuteDataSet(com);
                if (ds != null)
                {
                    dt = ds.Tables[0];
                }
            }

        }
        catch (Exception ex)
        {
            // To Do: Handle Exception

        }
        return dt;
    }
    public DataTable DeleteByChargeSlip()
    {
        DataSet ds = null;
        DataTable dt = null;
        try
        {
            if (this.ChargeSlipID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_AttachmentsDeleteAsPerChargeSlip");
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
                ds = this.db.ExecuteDataSet(com);
                if (ds != null)
                {
                    dt = ds.Tables[0];
                }
            }

        }
        catch (Exception ex)
        {
            // To Do: Handle Exception

        }
        return dt;
    }
    #endregion

    #endregion
}
