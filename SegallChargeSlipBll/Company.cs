// -----------------------------------------------------------------------
// <copyright file="Company.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing Company
/// </summary>
namespace SegallChargeSlipBll
{
    [Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
    public class Company
    {
        #region Basic Functionality

        #region Variable Declaration

        /// <summary>
        /// Variable to store Database object to interact with database.
        /// </summary>
        private Database db;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the Company class.
        /// </summary>
        public Company()
        {
            this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        }

        /// <summary>
        /// Initializes a new instance of the Company class.
        /// </summary>
        /// <param name="companyID">Sets the value of CompanyID.</param>
        public Company(int companyID)
        {
            this.db = DatabaseFactory.CreateDatabase("ConnectionString");
            this.CompanyID = companyID;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets CompanyID
        /// </summary>
        public int CompanyID
        {
            get;
            set;
        }
        public string County
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets Company
        /// </summary>
        public string CompanyName
        {
            get;
            set;
        }
        public Int64 LastModifiedNotesBy
        {
            get;
            set;
        }
        public DateTime LastModifiedNotesOn
        {
            get;
            set;
        }
        public string CompaniesNotes
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets Address1
        /// </summary>
        public string Address1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Address2
        /// </summary>
        public string Address2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets City
        /// </summary>
        public string City
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets State
        /// </summary>
        public string State
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ZipCode
        /// </summary>
        public string ZipCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedBy
        /// </summary>
        public int CreatedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedOn
        /// </summary>
        public DateTime CreatedOn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LastModifiedOn
        /// </summary>
        public DateTime LastModifiedOn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Name a.k.a Landlord Name
        /// </summary>
        public string Name
        {
            get;
            set;
        }
        public string CompanyNotes
        {
            get;
            set;
        }


        //---------vv-------- SK 08/06 : Added new fielsds OwnershipEntity, Email
        /// <summary>
        /// Gets or sets Ownership Entity
        /// </summary>
        public string OwnershipEntity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Email
        /// </summary>
        public string Email
        {
            get;
            set;
        }


        //Custom Property
        public Boolean IsDuplicate
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets StateName
        /// </summary>
        public string StateName
        {
            get;
            set;
        }
        public bool IsActive
        {
            get;
            set;
        }
        #endregion

        #region Actions

        /// <summary>
        /// Loads the details for Company.
        /// </summary>
        /// <returns>True if Load operation is successful; Else False.</returns>
        public bool Load()
        {
            try
            {
                //if (DateTime.Now <= DateTime.Parse("2014/09/03"))
                //{
                    if (this.CompanyID != 0)
                    {
                        DbCommand com = this.db.GetStoredProcCommand("usp_CompanyGetDetails");
                        this.db.AddInParameter(com, "CompanyID", DbType.Int32, this.CompanyID);
                        DataSet ds = this.db.ExecuteDataSet(com);
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            DataTable dt = ds.Tables[0];
                            this.CompanyID = Convert.ToInt32(dt.Rows[0]["CompanyID"]);
                            this.CompanyName = Convert.ToString(dt.Rows[0]["Company"]);
                            this.Address1 = Convert.ToString(dt.Rows[0]["Address1"]);
                            this.Address2 = Convert.ToString(dt.Rows[0]["Address2"]);
                            this.City = Convert.ToString(dt.Rows[0]["City"]);
                            this.State = Convert.ToString(dt.Rows[0]["State"]);
                            this.ZipCode = Convert.ToString(dt.Rows[0]["ZipCode"]);
                            this.CreatedBy = Convert.ToInt32(dt.Rows[0]["CreatedBy"]);
                            this.CreatedOn = Convert.ToDateTime(dt.Rows[0]["CreatedOn"]);
                            this.LastModifiedBy = Convert.ToInt32(dt.Rows[0]["LastModifiedBy"]);
                            this.LastModifiedOn = Convert.ToDateTime(dt.Rows[0]["LastModifiedOn"]);
                            this.Name = Convert.ToString(dt.Rows[0]["Name"]);
                            this.OwnershipEntity = Convert.ToString(dt.Rows[0]["OwnershipEntity"]);
                            this.Email = Convert.ToString(dt.Rows[0]["Email"]);
                            this.StateName = Convert.ToString(dt.Rows[0]["StateName"]);
                            this.IsActive = Convert.ToBoolean(dt.Rows[0]["IsActive"]);
                            this.County = Convert.ToString(dt.Rows[0]["County"]);
                            this.CompaniesNotes = Convert.ToString(dt.Rows[0]["CompanyNotes"]);
                            this.LastModifiedNotesBy = Convert.ToInt64(dt.Rows[0]["LastModifiedNotesBy"]);
                            if (!string.IsNullOrEmpty(dt.Rows[0]["LastModifiedNotesOn"].ToString()))
                            {
                                this.LastModifiedNotesOn = Convert.ToDateTime(dt.Rows[0]["LastModifiedNotesOn"]);
                            }


                            return true;
                        }
                    //}
                }

                return false;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }
        }

        /// <summary>
        /// Inserts details for Company if CompanyID = 0.
        /// Else updates details for Company.
        /// </summary>
        /// <returns>True if Save operation is successful; Else False.</returns>
        public bool Save()
        {
            if (this.CompanyID == 0)
            {
                return this.Insert();
            }
            else
            {
                if (this.CompanyID > 0)
                {
                    return this.Update();
                }
                else
                {
                    this.CompanyID = 0;
                    return false;
                }
            }
        }

        /// <summary>
        /// Inserts details for Company.
        /// Saves newly created Id in CompanyID.
        /// </summary>
        /// <returns>True if Insert operation is successful; Else False.</returns>
        public void MessagesInsert(Int64 UserId)
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_MessagesInsertAsPerActive");
                this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
                this.db.AddInParameter(com, "CompanyId", DbType.Int32, this.CompanyID);
                this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
               
            }
        }
        private bool Insert()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_CompanyInsert");
                this.db.AddOutParameter(com, "CompanyID", DbType.Int32, 1024);
                if (!String.IsNullOrEmpty(this.CompanyName))
                {
                    this.db.AddInParameter(com, "Company", DbType.String, this.CompanyName);
                }
                else
                {
                    this.db.AddInParameter(com, "Company", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address1))
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
                }
                else
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address2))
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
                }
                else
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.City))
                {
                    this.db.AddInParameter(com, "City", DbType.String, this.City);
                }
                else
                {
                    this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.State))
                {
                    this.db.AddInParameter(com, "State", DbType.String, this.State);
                }
                else
                {
                    this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.ZipCode))
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, this.ZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, DBNull.Value);
                }
                if (this.CreatedBy > 0)
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
                }
                if (this.LastModifiedBy > 0)
                {
                    this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, this.LastModifiedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, DBNull.Value);
                }
                if (this.LastModifiedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, this.LastModifiedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Name))
                {
                    this.db.AddInParameter(com, "Name", DbType.String, this.Name);
                }
                else
                {
                    this.db.AddInParameter(com, "Name", DbType.String, DBNull.Value);
                }

                //JAY 09/26 Req. ownership entity will be diff for all charge slips
                //if (!String.IsNullOrEmpty(this.OwnershipEntity))
                //{
                //    this.db.AddInParameter(com, "OwnershipEntity", DbType.String, this.OwnershipEntity);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "OwnershipEntity", DbType.String, DBNull.Value);
                //}
                this.db.AddInParameter(com, "OwnershipEntity", DbType.String, DBNull.Value);

                if (!String.IsNullOrEmpty(this.Email))
                {
                    this.db.AddInParameter(com, "Email", DbType.String, this.Email);
                }
                else
                {
                    this.db.AddInParameter(com, "Email", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.County))
                {
                    this.db.AddInParameter(com, "County", DbType.String, this.County);
                }
                else
                {
                    this.db.AddInParameter(com, "County", DbType.String, DBNull.Value);
                }
                this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
                if (this.LastModifiedNotesBy > 0)
                {
                    this.db.AddInParameter(com, "LastModifiedNotesBy", DbType.Int32, this.LastModifiedNotesBy);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedNotesBy", DbType.Int32, DBNull.Value);
                }
                if (this.LastModifiedNotesOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "LastModifiedNotesOn", DbType.DateTime, this.LastModifiedNotesOn);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedNotesOn", DbType.DateTime, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CompaniesNotes))
                {
                    this.db.AddInParameter(com, "CompanyNotes", DbType.String, this.CompaniesNotes);
                }
                else
                {
                    this.db.AddInParameter(com, "CompanyNotes", DbType.String, DBNull.Value);
                }
                this.db.ExecuteNonQuery(com);
                this.CompanyID = Convert.ToInt32(this.db.GetParameterValue(com, "CompanyID"));      // Read in the output parameter value
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return this.CompanyID > 0; // Return whether ID was returned
        }

        /// <summary>
        /// Updates details for Company.
        /// </summary>
        /// <returns>True if Update operation is successful; Else False.</returns>
        private bool Update()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_CompanyUpdate");
                this.db.AddInParameter(com, "CompanyID", DbType.Int32, this.CompanyID);
                if (!String.IsNullOrEmpty(this.CompanyName))
                {
                    this.db.AddInParameter(com, "Company", DbType.String, this.CompanyName);
                }
                else
                {
                    this.db.AddInParameter(com, "Company", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address1))
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
                }
                else
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address2))
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
                }
                else
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.City))
                {
                    this.db.AddInParameter(com, "City", DbType.String, this.City);
                }
                else
                {
                    this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.State))
                {
                    this.db.AddInParameter(com, "State", DbType.String, this.State);
                }
                else
                {
                    this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.ZipCode))
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, this.ZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, DBNull.Value);
                }
                if (this.CreatedBy > 0)
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
                }
                if (this.LastModifiedBy > 0)
                {
                    this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, this.LastModifiedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, DBNull.Value);
                }
                if (this.LastModifiedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, this.LastModifiedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Name))
                {
                    this.db.AddInParameter(com, "Name", DbType.String, this.Name);
                }
                else
                {
                    this.db.AddInParameter(com, "Name", DbType.String, DBNull.Value);
                }

                //JAY 09/26 Req. ownership entity will be diff for all charge slips
                //if (!String.IsNullOrEmpty(this.OwnershipEntity))
                //{
                //    this.db.AddInParameter(com, "OwnershipEntity", DbType.String, this.OwnershipEntity);
                //}
                //else
                //{
                //    this.db.AddInParameter(com, "OwnershipEntity", DbType.String, DBNull.Value);
                //}
                this.db.AddInParameter(com, "OwnershipEntity", DbType.String, DBNull.Value);

                if (!String.IsNullOrEmpty(this.Email))
                {
                    this.db.AddInParameter(com, "Email", DbType.String, this.Email);
                }
                else
                {
                    this.db.AddInParameter(com, "Email", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.County))
                {
                    this.db.AddInParameter(com, "County", DbType.String, this.County);
                }
                else
                {
                    this.db.AddInParameter(com, "County", DbType.String, DBNull.Value);
                }
                this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
                if (this.LastModifiedNotesBy > 0)
                {
                    this.db.AddInParameter(com, "LastModifiedNotesBy", DbType.Int32, this.LastModifiedNotesBy);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedNotesBy", DbType.Int32, DBNull.Value);
                }
                if (this.LastModifiedNotesOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "LastModifiedNotesOn", DbType.DateTime, this.LastModifiedNotesOn);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedNotesOn", DbType.DateTime, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.CompaniesNotes))
                {
                    this.db.AddInParameter(com, "CompanyNotes", DbType.String, this.CompaniesNotes);
                }
                else
                {
                    this.db.AddInParameter(com, "CompanyNotes", DbType.String, DBNull.Value);
                }
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes details of Company for provided CompanyID.
        /// </summary>
        /// <returns>True if Delete operation is successful; Else False.</returns>
        public bool Delete()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_CompanyDelete");
                this.db.AddInParameter(com, "CompanyID", DbType.Int32, this.CompanyID);
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get list of Company for provided parameters.
        /// </summary>
        /// <returns>DataSet of result</returns>
        /// <remarks></remarks>
        public DataSet GetList(int pageNumber, int pageSize)
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_CompanyGetList");
                if (!string.IsNullOrEmpty(this.CompanyName))
                {
                    db.AddInParameter(com, "Company", DbType.String, this.CompanyName);
                }
                else
                {
                    db.AddInParameter(com, "Company", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.City))
                {
                    this.db.AddInParameter(com, "City", DbType.String, this.City);
                }
                else
                {
                    this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.State))
                {
                    this.db.AddInParameter(com, "State", DbType.String, this.State);
                }
                else
                {
                    this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.ZipCode))
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, this.ZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, DBNull.Value);
                }
                if (pageNumber > 0)
                    db.AddInParameter(com, "Pageno", DbType.Int32, pageNumber);
                else
                    db.AddInParameter(com, "Pageno", DbType.Int32, 1);
                if (pageSize > 0)
                    db.AddInParameter(com, "PageSize", DbType.Int32, pageSize);
                else
                    db.AddInParameter(com, "PageSize", DbType.Int32, 25);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }

        // Custom Action For Serach List

        public DataSet GetLocationSeachList()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_CompanySearchByName");
                this.db.AddInParameter(com, "Company", DbType.String, this.CompanyName);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }
        public DataSet GetDealNameSeachList(int compid, string DealName)
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_DealNameByCompanyGetList");
                this.db.AddInParameter(com, "CompId", DbType.Int32, compid);
                this.db.AddInParameter(com, "DealName", DbType.String, DealName);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }

        #endregion

        #region More

        public DataSet GetDealInfoList(int userid)
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_DealInfoList");
                this.db.AddInParameter(com, "CompanyID", DbType.Int32, this.CompanyID);
                this.db.AddInParameter(com, "userid", DbType.Int32, userid);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }

        public DataSet GetCustInfoList()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_CustInfoList");
                this.db.AddInParameter(com, "CompanyID", DbType.Int32, this.CompanyID);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }

        #endregion



        #region Custom Actions
        public DataTable CompanyExists()
        {
            try
            {
                DataTable dt = new DataTable();
                if (this.CompanyID != 0)
                {
                    DbCommand com = this.db.GetStoredProcCommand("usp_CompanyExist");
                    this.db.AddInParameter(com, "CompanyId", DbType.Int32, this.CompanyID);
                    DataSet ds = this.db.ExecuteDataSet(com);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        dt = ds.Tables[0];
                        return dt;
                    }
                }
                return dt;


            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return null;
            }
        }
        public DataTable CheckForDuplication()
        {
            DataSet ds = null;
            DataTable dt = null;

            DbCommand com = this.db.GetStoredProcCommand("usp_CheckForCompanyDuplication");
            //     this.db.AddOutParameter(com, "LocationID", DbType.Int32, 1024);
            //     this.db.AddOutParameter(com, "IsDuplicate", DbType.Boolean, 2);
            if (!String.IsNullOrEmpty(this.CompanyName))
            {
                this.db.AddInParameter(com, "Company", DbType.String, this.CompanyName);
            }
            else
            {
                this.db.AddInParameter(com, "Company", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Name))
            {
                this.db.AddInParameter(com, "Name", DbType.String, this.Name);
            }
            else
            {
                this.db.AddInParameter(com, "Name", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Address1))
            {
                this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
            }
            else
            {
                this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Address2))
            {
                this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
            }
            else
            {
                this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.City))
            {
                this.db.AddInParameter(com, "City", DbType.String, this.City);
            }
            else
            {
                this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.State))
            {
                this.db.AddInParameter(com, "State", DbType.String, this.State);
            }
            else
            {
                this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.ZipCode))
            {
                this.db.AddInParameter(com, "ZipCode", DbType.String, this.ZipCode);
            }
            else
            {
                this.db.AddInParameter(com, "ZipCode", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Email))
            {
                this.db.AddInParameter(com, "Email", DbType.String, this.Email);
            }
            else
            {
                this.db.AddInParameter(com, "Email", DbType.String, DBNull.Value);
            }
            ds = this.db.ExecuteDataSet(com);

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count >= 1)
            {
                dt = ds.Tables[0];
                this.IsDuplicate = true;
            }
            else
            {
                this.IsDuplicate = false;
            }
            return dt;
        }
        #endregion

        #endregion
    }
}