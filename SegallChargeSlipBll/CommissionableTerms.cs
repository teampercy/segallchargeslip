// -----------------------------------------------------------------------
// <copyright file="CommissionableTerms.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing CommissionableTerms
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class CommissionableTerms
{
#region Basic Functionality

#region Variable Declaration

   /// <summary>
   /// Variable to store Database object to interact with database.
   /// </summary>
   private Database db;
#endregion

#region Constructors

   /// <summary>
   /// Initializes a new instance of the CommissionableTerms class.
   /// </summary>
   public CommissionableTerms()
   {
      this.db  = DatabaseFactory.CreateDatabase("ConnectionString");
   }

   /// <summary>
   /// Initializes a new instance of the CommissionableTerms class.
   /// </summary>
   /// <param name="commissionableTermId">Sets the value of CommissionableTermId.</param>
   public CommissionableTerms(int commissionableTermId)
   {
      this.db = DatabaseFactory.CreateDatabase();
      this.CommissionableTermId = commissionableTermId;
   }
#endregion

#region Properties

   /// <summary>
   /// Gets or sets CommissionableTermId
   /// </summary>
   public int CommissionableTermId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets ChargeSlipId
   /// </summary>
   public int ChargeSlipId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets DealTransactionId
   /// </summary>
   public int DealTransactionId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets TermTypeId
   /// </summary>
   public int TermTypeId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets RentPeriodFrom
   /// </summary>
   public int RentPeriodFrom
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets RentPeriodTo
   /// </summary>
   public int RentPeriodTo
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets TotalMonths
   /// </summary>
   public int TotalMonths
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets PercentBrokerComm
   /// </summary>
   public double PercentBrokerComm
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets Fee
   /// </summary>
   public decimal Fee
   {
      get; set;
   }
#endregion

#region Actions

   /// <summary>
   /// Loads the details for CommissionableTerms.
   /// </summary>
   /// <returns>True if Load operation is successful; Else False.</returns>
   public bool Load()
   {
      try
      {
         if (this.CommissionableTermId != 0)
         {
            DbCommand com = this.db.GetStoredProcCommand("usp_CommissionableTermsGetDetails");
            this.db.AddInParameter(com, "CommissionableTermId", DbType.Int32, this.CommissionableTermId);
            DataSet ds = this.db.ExecuteDataSet(com);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
               DataTable dt = ds.Tables[0];
               this.CommissionableTermId = Convert.ToInt32(dt.Rows[0]["CommissionableTermId"]);
               this.ChargeSlipId = Convert.ToInt32(dt.Rows[0]["ChargeSlipId"]);
               this.DealTransactionId = Convert.ToInt32(dt.Rows[0]["DealTransactionId"]);
               this.TermTypeId = Convert.ToInt32(dt.Rows[0]["TermTypeId"]);
               this.RentPeriodFrom = Convert.ToInt32(dt.Rows[0]["RentPeriodFrom"]);
               this.RentPeriodTo = Convert.ToInt32(dt.Rows[0]["RentPeriodTo"]);
               this.TotalMonths = Convert.ToInt32(dt.Rows[0]["TotalMonths"]);
               this.PercentBrokerComm = Convert.ToDouble(dt.Rows[0]["PercentBrokerComm"]);
               this.Fee = Convert.ToDecimal(dt.Rows[0]["Fee"]);
               return true;
            }
         }

      return false;
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }
   }

   /// <summary>
   /// Inserts details for CommissionableTerms if CommissionableTermId = 0.
   /// Else updates details for CommissionableTerms.
   /// </summary>
   /// <returns>True if Save operation is successful; Else False.</returns>
   public bool Save()
   {
      if (this.CommissionableTermId == 0)
      {
         return this.Insert();
      }
      else
      {
         if (this.CommissionableTermId > 0)
         {
            return this.Update();
         }
         else
         {
            this.CommissionableTermId = 0;
            return false;
         }
      }
   }

   /// <summary>
   /// Inserts details for CommissionableTerms.
   /// Saves newly created Id in CommissionableTermId.
   /// </summary>
   /// <returns>True if Insert operation is successful; Else False.</returns>
   private bool Insert()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_CommissionableTermsInsert");
         this.db.AddOutParameter(com, "CommissionableTermId", DbType.Int32, 1024);
         if (this.ChargeSlipId > 0)
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
         }
         else
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
         }
         if (this.DealTransactionId > 0)
         {
            this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, this.DealTransactionId);
         }
         else
         {
            this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, DBNull.Value);
         }
         if (this.TermTypeId > 0)
         {
            this.db.AddInParameter(com, "TermTypeId", DbType.Int32, this.TermTypeId);
         }
         else
         {
            this.db.AddInParameter(com, "TermTypeId", DbType.Int32, DBNull.Value);
         }
         if (this.RentPeriodFrom > 0)
         {
            this.db.AddInParameter(com, "RentPeriodFrom", DbType.Int32, this.RentPeriodFrom);
         }
         else
         {
            this.db.AddInParameter(com, "RentPeriodFrom", DbType.Int32, DBNull.Value);
         }
         if (this.RentPeriodTo > 0)
         {
            this.db.AddInParameter(com, "RentPeriodTo", DbType.Int32, this.RentPeriodTo);
         }
         else
         {
            this.db.AddInParameter(com, "RentPeriodTo", DbType.Int32, DBNull.Value);
         }
         if (this.TotalMonths > 0)
         {
            this.db.AddInParameter(com, "TotalMonths", DbType.Int32, this.TotalMonths);
         }
         else
         {
            this.db.AddInParameter(com, "TotalMonths", DbType.Int32, DBNull.Value);
         }
         if (this.PercentBrokerComm > 0)
         {
            this.db.AddInParameter(com, "PercentBrokerComm", DbType.Double, this.PercentBrokerComm);
         }
         else
         {
            this.db.AddInParameter(com, "PercentBrokerComm", DbType.Double, DBNull.Value);
         }
         if (this.Fee > 0)
         {
            this.db.AddInParameter(com, "Fee", DbType.Decimal, this.Fee);
         }
         else
         {
            this.db.AddInParameter(com, "Fee", DbType.Double, DBNull.Value);
         }
         this.db.ExecuteNonQuery(com);
         this.CommissionableTermId = Convert.ToInt32(this.db.GetParameterValue(com, "CommissionableTermId"));      // Read in the output parameter value
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return this.CommissionableTermId > 0; // Return whether ID was returned
   }

   /// <summary>
   /// Updates details for CommissionableTerms.
   /// </summary>
   /// <returns>True if Update operation is successful; Else False.</returns>
   private bool Update()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_CommissionableTermsUpdate");
         this.db.AddInParameter(com, "CommissionableTermId", DbType.Int32, this.CommissionableTermId);
         if (this.ChargeSlipId > 0)
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
         }
         else
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
         }
         if (this.DealTransactionId > 0)
         {
            this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, this.DealTransactionId);
         }
         else
         {
            this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, DBNull.Value);
         }
         if (this.TermTypeId > 0)
         {
            this.db.AddInParameter(com, "TermTypeId", DbType.Int32, this.TermTypeId);
         }
         else
         {
            this.db.AddInParameter(com, "TermTypeId", DbType.Int32, DBNull.Value);
         }
         if (this.RentPeriodFrom > 0)
         {
            this.db.AddInParameter(com, "RentPeriodFrom", DbType.Int32, this.RentPeriodFrom);
         }
         else
         {
            this.db.AddInParameter(com, "RentPeriodFrom", DbType.Int32, DBNull.Value);
         }
         if (this.RentPeriodTo > 0)
         {
            this.db.AddInParameter(com, "RentPeriodTo", DbType.Int32, this.RentPeriodTo);
         }
         else
         {
            this.db.AddInParameter(com, "RentPeriodTo", DbType.Int32, DBNull.Value);
         }
         if (this.TotalMonths > 0)
         {
            this.db.AddInParameter(com, "TotalMonths", DbType.Int32, this.TotalMonths);
         }
         else
         {
            this.db.AddInParameter(com, "TotalMonths", DbType.Int32, DBNull.Value);
         }
         if (this.PercentBrokerComm > 0)
         {
            this.db.AddInParameter(com, "PercentBrokerComm", DbType.Double, this.PercentBrokerComm);
         }
         else
         {
            this.db.AddInParameter(com, "PercentBrokerComm", DbType.Double, DBNull.Value);
         }
         if (this.Fee > 0)
         {
            this.db.AddInParameter(com, "Fee", DbType.Decimal, this.Fee);
         }
         else
         {
            this.db.AddInParameter(com, "Fee", DbType.Double, DBNull.Value);
         }
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Deletes details of CommissionableTerms for provided CommissionableTermId.
   /// </summary>
   /// <returns>True if Delete operation is successful; Else False.</returns>
   public bool Delete()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_CommissionableTermsDelete");
         this.db.AddInParameter(com, "CommissionableTermId", DbType.Int32, this.CommissionableTermId);
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Get list of CommissionableTerms for provided parameters.
   /// </summary>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetList()
   {
      DataSet ds = null;
      try
      {
         DbCommand com = db.GetStoredProcCommand("usp_CommissionableTermsGetList");
         ds = db.ExecuteDataSet(com);
      }
      catch (Exception ex)
      {
         //To Do: Handle Exception
      }

      return ds;
   }


   public DataSet GetCommTermsChargeSlipWise()
   {
       DataSet ds = null;
       try
       {
           DbCommand com = db.GetStoredProcCommand("usp_CommissionableTermsGetListChargeSlipWise");
           this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
           ds = db.ExecuteDataSet(com);
       }
       catch (Exception ex)
       {
           //To Do: Handle Exception
       }

       return ds;
   }
#endregion

#endregion
}
