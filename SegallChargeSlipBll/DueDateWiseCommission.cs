// -----------------------------------------------------------------------
// <copyright file="DueDateWiseCommission.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing DueDateWiseCommission
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class DueDateWiseCommission
{
#region Basic Functionality

#region Variable Declaration

   /// <summary>
   /// Variable to store Database object to interact with database.
   /// </summary>
   private Database db;
#endregion

#region Constructors

   /// <summary>
   /// Initializes a new instance of the DueDateWiseCommission class.
   /// </summary>
   public DueDateWiseCommission()
   {
      this.db  = DatabaseFactory.CreateDatabase("ConnectionString");
   }

   /// <summary>
   /// Initializes a new instance of the DueDateWiseCommission class.
   /// </summary>
   /// <param name="id">Sets the value of Id.</param>
   public DueDateWiseCommission(int id)
   {
      this.db = DatabaseFactory.CreateDatabase();
      this.Id = id;
   }
#endregion

#region Properties

   /// <summary>
   /// Gets or sets Id
   /// </summary>
   public int Id
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets CommPer
   /// </summary>
   public double CommPer
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets CommAmount
   /// </summary>
   public double CommAmount
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets DueDate
   /// </summary>
   public DateTime DueDate
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets DealTransactionID
   /// </summary>
   public int DealTransactionID
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets CreatedBy
   /// </summary>
   public int CreatedBy
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets CreatedOn
   /// </summary>
   public DateTime CreatedOn
   {
      get; set;
   }
#endregion

#region Actions

   /// <summary>
   /// Loads the details for DueDateWiseCommission.
   /// </summary>
   /// <returns>True if Load operation is successful; Else False.</returns>
   public bool Load()
   {
      try
      {
         if (this.Id != 0)
         {
            DbCommand com = this.db.GetStoredProcCommand("usp_DueDateWiseCommissionGetDetails");
            this.db.AddInParameter(com, "Id", DbType.Int32, this.Id);
            DataSet ds = this.db.ExecuteDataSet(com);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
               DataTable dt = ds.Tables[0];
               this.Id = Convert.ToInt32(dt.Rows[0]["Id"]);
               this.CommPer = Convert.ToDouble(dt.Rows[0]["CommPer"]);
               this.CommAmount = Convert.ToDouble(dt.Rows[0]["CommAmount"]);
               this.DueDate = Convert.ToDateTime(dt.Rows[0]["DueDate"]);
               this.DealTransactionID = Convert.ToInt32(dt.Rows[0]["DealTransactionID"]);
               this.CreatedBy = Convert.ToInt32(dt.Rows[0]["CreatedBy"]);
               this.CreatedOn = Convert.ToDateTime(dt.Rows[0]["CreatedOn"]);
               return true;
            }
         }

      return false;
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }
   }
   public DataTable GetPaymentDistributionGetDetails(int ChargeSlip, double  NewPayment,string CommissionDueDatesId)
   {
         DataTable dt = new DataTable();
         try
         {
             DbCommand com = this.db.GetStoredProcCommand("usp_PaymentDistributionGetDetails");
             this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, ChargeSlip);
             this.db.AddInParameter(com, "NewPayment", DbType.Double , NewPayment);
             this.db.AddInParameter(com, "CommissionDuedatesId", DbType.String, CommissionDueDatesId);
             DataSet ds = this.db.ExecuteDataSet(com);
             dt = ds.Tables[0];
         }
         catch (Exception ex)
         {
             ErrorLog.WriteLog("DueDateWiseCommission.cs", "GetPaymentDistributionGetDetails", ex.Message);
         }
         return dt;
   }
   public DataTable usp_GetCompIdByDealName(int ChargeSlip)
   {
       DataTable dt = new DataTable();
       try
       {
           //DbCommand com = this.db.GetStoredProcCommand("usp_DealPaymentTransactionGetList");
           DbCommand com = this.db.GetStoredProcCommand("usp_GetCompIdByDealName");
           this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, ChargeSlip);
           DataSet ds = this.db.ExecuteDataSet(com);
           dt = ds.Tables[0];
       }
       catch (Exception ex)
       {
           ErrorLog.WriteLog("DueDateWiseCommission.cs", "GetCommissionDueDateByChargeSlip", ex.Message);
       }
       return dt;
   }
   public DataTable GetCommissionDueDateByChargeSlip(int ChargeSlip)
   {
         DataTable dt = new DataTable();
         try {
             //DbCommand com = this.db.GetStoredProcCommand("usp_DealPaymentTransactionGetList");
             DbCommand com = this.db.GetStoredProcCommand("usp_DealPaymentTransactionGetList");
             this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, ChargeSlip);
             DataSet ds = this.db.ExecuteDataSet(com);
             dt = ds.Tables[0];
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("DueDateWiseCommission.cs", "GetCommissionDueDateByChargeSlip", ex.Message);
        }
       return dt;
   }
   public DataTable GetTotalBalance(int ChargeSlip)
   {
       DataTable dt = new DataTable();
       try {
           DbCommand com = this.db.GetStoredProcCommand("usp_TotalBalanceDueDate");
           this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32,ChargeSlip);
           DataSet ds = this.db.ExecuteDataSet(com);
           if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
           {
                dt = ds.Tables[0];
               return dt;
           }
          
       }
       catch (Exception ex)
       {
           ErrorLog.WriteLog("DueDateWiseCommission.cs", "GetTotalBalance", ex.Message);
       }
       return dt;
   }
   /// <summary>
   /// Inserts details for DueDateWiseCommission if Id = 0.
   /// Else updates details for DueDateWiseCommission.
   /// </summary>
   /// <returns>True if Save operation is successful; Else False.</returns>
   public bool Save()
   {
      if (this.Id == 0)
      {
         return this.Insert();
      }
      else
      {
         if (this.Id > 0)
         {
            return this.Update();
         }
         else
         {
            this.Id = 0;
            return false;
         }
      }
   }

   /// <summary>
   /// Inserts details for DueDateWiseCommission.
   /// Saves newly created Id in Id.
   /// </summary>
   /// <returns>True if Insert operation is successful; Else False.</returns>
   public bool PaymentCommssionInsert(Int32 Paymentid,Int32 CommissionDueId,decimal Amount)
   {
       try
       {
           DbCommand com = this.db.GetStoredProcCommand("usp_PaymentCommssionInsert");
           if (Paymentid > 0)
           {
               this.db.AddInParameter(com, "PaymentId", DbType.Int32, Paymentid);
           }
           else
           {
               this.db.AddInParameter(com, "PaymentId", DbType.Int32, DBNull.Value);
           }
           if (CommissionDueId > 0)
           {
               this.db.AddInParameter(com, "CommissionDueId", DbType.Int32, CommissionDueId);
           }
           else
           {
               this.db.AddInParameter(com, "CommissionDueId", DbType.Int32, DBNull.Value);
           }
           if (Amount > 0)
           {
               this.db.AddInParameter(com, "CommissionPaid", DbType.Double, Amount);
           }
           else
           {
               this.db.AddInParameter(com, "CommissionPaid", DbType.Double, DBNull.Value);
           }
           this.db.ExecuteNonQuery(com);
       }
       catch (Exception ex)
       {
           // To Do: Handle Exception
           return false;
       }
       return true;
   }
   private bool Insert()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_DueDateWiseCommissionInsert");
         this.db.AddOutParameter(com, "Id", DbType.Int32, 1024);
         if (this.CommPer > 0)
         {
            this.db.AddInParameter(com, "CommPer", DbType.Double, this.CommPer);
         }
         else
         {
            this.db.AddInParameter(com, "CommPer", DbType.Double, DBNull.Value);
         }
         if (this.CommAmount > 0)
         {
            this.db.AddInParameter(com, "CommAmount", DbType.Double, this.CommAmount);
         }
         else
         {
            this.db.AddInParameter(com, "CommAmount", DbType.Double, DBNull.Value);
         }
         if (this.DueDate > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "DueDate", DbType.DateTime, this.DueDate);
         }
         else
         {
            this.db.AddInParameter(com, "DueDate", DbType.DateTime, DBNull.Value);
         }
         if (this.DealTransactionID > 0)
         {
            this.db.AddInParameter(com, "DealTransactionID", DbType.Int32, this.DealTransactionID);
         }
         else
         {
            this.db.AddInParameter(com, "DealTransactionID", DbType.Int32, DBNull.Value);
         }
         if (this.CreatedBy > 0)
         {
            this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
         }
         else
         {
            this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
         }
         if (this.CreatedOn > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
         }
         else
         {
            this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
         }
         this.db.ExecuteNonQuery(com);
         this.Id = Convert.ToInt32(this.db.GetParameterValue(com, "Id"));      // Read in the output parameter value
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return this.Id > 0; // Return whether ID was returned
   }

   /// <summary>
   /// Updates details for DueDateWiseCommission.
   /// </summary>
   /// <returns>True if Update operation is successful; Else False.</returns>
   private bool Update()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_DueDateWiseCommissionUpdate");
         this.db.AddInParameter(com, "Id", DbType.Int32, this.Id);
         if (this.CommPer > 0)
         {
            this.db.AddInParameter(com, "CommPer", DbType.Double, this.CommPer);
         }
         else
         {
            this.db.AddInParameter(com, "CommPer", DbType.Double, DBNull.Value);
         }
         if (this.CommAmount > 0)
         {
            this.db.AddInParameter(com, "CommAmount", DbType.Double, this.CommAmount);
         }
         else
         {
            this.db.AddInParameter(com, "CommAmount", DbType.Double, DBNull.Value);
         }
         if (this.DueDate > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "DueDate", DbType.DateTime, this.DueDate);
         }
         else
         {
            this.db.AddInParameter(com, "DueDate", DbType.DateTime, DBNull.Value);
         }
         if (this.DealTransactionID > 0)
         {
            this.db.AddInParameter(com, "DealTransactionID", DbType.Int32, this.DealTransactionID);
         }
         else
         {
            this.db.AddInParameter(com, "DealTransactionID", DbType.Int32, DBNull.Value);
         }
         if (this.CreatedBy > 0)
         {
            this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
         }
         else
         {
            this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
         }
         if (this.CreatedOn > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
         }
         else
         {
            this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
         }
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Deletes details of DueDateWiseCommission for provided Id.
   /// </summary>
   /// <returns>True if Delete operation is successful; Else False.</returns>
   public bool Delete()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_DueDateWiseCommissionDelete");
         this.db.AddInParameter(com, "Id", DbType.Int32, this.Id);
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Get list of DueDateWiseCommission for provided parameters.
   /// </summary>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetList()
   {
      DataSet ds = null;
      try
      {
         DbCommand com = db.GetStoredProcCommand("usp_DueDateWiseCommissionGetList");
         ds = db.ExecuteDataSet(com);
      }
      catch (Exception ex)
      {
         //To Do: Handle Exception
      }

      return ds;
   }
#endregion

#endregion
}
