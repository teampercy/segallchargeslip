// -----------------------------------------------------------------------
// <copyright file="CommissionDueDates.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing CommissionDueDates
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class CommissionDueDates
{
#region Basic Functionality

#region Variable Declaration

   /// <summary>
   /// Variable to store Database object to interact with database.
   /// </summary>
   private Database db;
#endregion

#region Constructors

   /// <summary>
   /// Initializes a new instance of the CommissionDueDates class.
   /// </summary>
   public CommissionDueDates()
   {
      this.db  = DatabaseFactory.CreateDatabase("ConnectionString");
   }

   /// <summary>
   /// Initializes a new instance of the CommissionDueDates class.
   /// </summary>
   /// <param name="commissionDueId">Sets the value of CommissionDueId.</param>
   public CommissionDueDates(int commissionDueId)
   {
      this.db = DatabaseFactory.CreateDatabase();
      this.CommissionDueId = commissionDueId;
   }
#endregion

#region Properties

   /// <summary>
   /// Gets or sets CommissionDueId
   /// </summary>
   public int CommissionDueId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets ChargeSlipId
   /// </summary>
   public int ChargeSlipId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets DealTransactionId
   /// </summary>
   public int DealTransactionId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets TermTypeId
   /// </summary>
   public int TermTypeId
   {
      get; set;
   }
   public int RowId
   {
       get;
       set;
   }
   /// <summary>
   /// Gets or sets DueDate
   /// </summary>
   public DateTime DueDate
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets PercentComm
   /// </summary>
   public double PercentComm
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets CommAmountDue
   /// </summary>
   public decimal CommAmountDue
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets CommAmountDue
   /// </summary>
   public Boolean IsFullCommPaid
   {
       get;
       set;
   }

#endregion

#region Actions

   /// <summary>
   /// Loads the details for CommissionDueDates.
   /// </summary>
   /// <returns>True if Load operation is successful; Else False.</returns>
   public bool Load()
   {
      try
      {
         if (this.CommissionDueId != 0)
         {
            DbCommand com = this.db.GetStoredProcCommand("usp_CommissionDueDatesGetDetails");
            this.db.AddInParameter(com, "CommissionDueId", DbType.Int32, this.CommissionDueId);
            DataSet ds = this.db.ExecuteDataSet(com);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
               DataTable dt = ds.Tables[0];
               this.CommissionDueId = Convert.ToInt32(dt.Rows[0]["CommissionDueId"]);
               this.ChargeSlipId = Convert.ToInt32(dt.Rows[0]["ChargeSlipId"]);
               this.DealTransactionId = Convert.ToInt32(dt.Rows[0]["DealTransactionId"]);
               this.TermTypeId = Convert.ToInt32(dt.Rows[0]["TermTypeId"]);
               this.DueDate = Convert.ToDateTime(dt.Rows[0]["DueDate"]);
               this.PercentComm = Convert.ToDouble(dt.Rows[0]["PercentComm"]);
               this.CommAmountDue = Convert.ToDecimal(dt.Rows[0]["CommAmountDue"]);
               return true;
            }
         }

      return false;
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }
   }

   /// <summary>
   /// Inserts details for CommissionDueDates if CommissionDueId = 0.
   /// Else updates details for CommissionDueDates.
   /// </summary>
   /// <returns>True if Save operation is successful; Else False.</returns>
   public bool Save()
   {
      if (this.CommissionDueId == 0)
      {
         return this.Insert();
      }
      else
      {
         if (this.CommissionDueId > 0)
         {
            return this.Update();
         }
         else
         {
            this.CommissionDueId = 0;
            return false;
         }
      }
   }

   /// <summary>
   /// Inserts details for CommissionDueDates.
   /// Saves newly created Id in CommissionDueId.
   /// </summary>
   /// <returns>True if Insert operation is successful; Else False.</returns>
   public void CommissiionDueDateCheckExist(DataTable dtComm)
   {
       try
       {
           
           string ConnStr = this.db.ConnectionString;
           SqlConnection con = new SqlConnection(ConnStr);
           con.Open();
           SqlCommand cmd = new SqlCommand("usp_CommissionDueDateCheckExist", con);
           cmd.CommandType = CommandType.StoredProcedure;

           SqlParameter[] tblvaluetype = new SqlParameter[2];
           tblvaluetype[0] = cmd.Parameters.AddWithValue("@ChargeSLipId", ChargeSlipId);  //Passing table value parameter
           tblvaluetype[0].SqlDbType = SqlDbType.BigInt; // This one is used to tell ADO.NET we are passing Table value Parameter           
           tblvaluetype[1] = cmd.Parameters.AddWithValue("@CommissionDueDates", dtComm);  //Passing table value parameter
           tblvaluetype[1].SqlDbType = SqlDbType.Structured; // This one is used to tell ADO.NET we are passing Table value Parameter

          
          
           int result = cmd.ExecuteNonQuery();
           //DataSet result =db.ExecuteDataSet(cmd);
           con.Close();

       }
       catch (Exception ex)
       {
           ErrorLog.WriteLog("NewCharge", "SetProperties", ex.Message);
           // throw;
       }
   }
   public void CommissionDueDateExist()
   {
       try
       {
           DbCommand com = this.db.GetStoredProcCommand("usp_CommissionDueDateCheckExist");
           if (this.ChargeSlipId > 0)
           {
               this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
           }
           else
           {
               this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
           }
           if (this.DealTransactionId > 0)
           {
               this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, this.DealTransactionId);
           }
           else
           {
               this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, DBNull.Value);
           }
           
           if (this.DueDate > DateTime.MinValue)
           {
               this.db.AddInParameter(com, "DueDate", DbType.DateTime, this.DueDate);
           }
           else
           {
               this.db.AddInParameter(com, "DueDate", DbType.DateTime, DBNull.Value);
           }
           if (this.PercentComm > 0)
           {
               this.db.AddInParameter(com, "PercentComm", DbType.Double, this.PercentComm);
           }
           else
           {
               this.db.AddInParameter(com, "PercentComm", DbType.Double, DBNull.Value);
           }
           if (this.CommAmountDue > 0)
           {
               this.db.AddInParameter(com, "CommAmountDue", DbType.Decimal, this.CommAmountDue);
           }
           else
           {
               this.db.AddInParameter(com, "CommAmountDue", DbType.Double, DBNull.Value);
           }
           if (this.TermTypeId > 0)
           {
               this.db.AddInParameter(com, "TermTypeId", DbType.Int32, this.TermTypeId);
           }
           else
           {
               this.db.AddInParameter(com, "TermTypeId", DbType.Int32, DBNull.Value);
           }
           if (this.RowId > 0)
           {
               this.db.AddInParameter(com, "RowId", DbType.Int32, this.RowId);
           }
           else
           {
               this.db.AddInParameter(com, "RowId", DbType.Int32, DBNull.Value);
           }
           this.db.ExecuteNonQuery(com);
               
       }
       catch (Exception ex)
       {
           // To Do: Handle Exception
        
       }
   }
   private bool Insert()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_CommissionDueDatesInsert");
         this.db.AddOutParameter(com, "CommissionDueId", DbType.Int32, 1024);
         if (this.ChargeSlipId > 0)
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
         }
         else
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
         }
         if (this.DealTransactionId > 0)
         {
            this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, this.DealTransactionId);
         }
         else
         {
            this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, DBNull.Value);
         }
         if (this.TermTypeId > 0)
         {
            this.db.AddInParameter(com, "TermTypeId", DbType.Int32, this.TermTypeId);
         }
         else
         {
            this.db.AddInParameter(com, "TermTypeId", DbType.Int32, DBNull.Value);
         }
         if (this.DueDate > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "DueDate", DbType.DateTime, this.DueDate);
         }
         else
         {
            this.db.AddInParameter(com, "DueDate", DbType.DateTime, DBNull.Value);
         }
         if (this.PercentComm > 0)
         {
            this.db.AddInParameter(com, "PercentComm", DbType.Double, this.PercentComm);
         }
         else
         {
            this.db.AddInParameter(com, "PercentComm", DbType.Double, DBNull.Value);
         }
         if (this.CommAmountDue > 0)
         {
            this.db.AddInParameter(com, "CommAmountDue", DbType.Decimal, this.CommAmountDue);
         }
         else
         {
            this.db.AddInParameter(com, "CommAmountDue", DbType.Double, DBNull.Value);
         }
         this.db.ExecuteNonQuery(com);
         this.CommissionDueId = Convert.ToInt32(this.db.GetParameterValue(com, "CommissionDueId"));      // Read in the output parameter value
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return this.CommissionDueId > 0; // Return whether ID was returned
   }

   /// <summary>
   /// Updates details for CommissionDueDates.
   /// </summary>
   /// <returns>True if Update operation is successful; Else False.</returns>
   private bool Update()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_CommissionDueDatesUpdate");
         this.db.AddInParameter(com, "CommissionDueId", DbType.Int32, this.CommissionDueId);
         if (this.ChargeSlipId > 0)
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
         }
         else
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
         }
         if (this.DealTransactionId > 0)
         {
            this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, this.DealTransactionId);
         }
         else
         {
            this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, DBNull.Value);
         }
         if (this.TermTypeId > 0)
         {
            this.db.AddInParameter(com, "TermTypeId", DbType.Int32, this.TermTypeId);
         }
         else
         {
            this.db.AddInParameter(com, "TermTypeId", DbType.Int32, DBNull.Value);
         }
         if (this.DueDate > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "DueDate", DbType.DateTime, this.DueDate);
         }
         else
         {
            this.db.AddInParameter(com, "DueDate", DbType.DateTime, DBNull.Value);
         }
         if (this.PercentComm > 0)
         {
            this.db.AddInParameter(com, "PercentComm", DbType.Double, this.PercentComm);
         }
         else
         {
            this.db.AddInParameter(com, "PercentComm", DbType.Double, DBNull.Value);
         }
         if (this.CommAmountDue > 0)
         {
            this.db.AddInParameter(com, "CommAmountDue", DbType.Decimal, this.CommAmountDue);
         }
         else
         {
            this.db.AddInParameter(com, "CommAmountDue", DbType.Double, DBNull.Value);
         }
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Deletes details of CommissionDueDates for provided CommissionDueId.
   /// </summary>
   /// <returns>True if Delete operation is successful; Else False.</returns>
   public bool Delete()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_CommissionDueDatesDelete");
         this.db.AddInParameter(com, "CommissionDueId", DbType.Int32, this.CommissionDueId);
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Get list of CommissionDueDates for provided parameters.
   /// </summary>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetList()
   {
      DataSet ds = null;
      try
      {
         DbCommand com = db.GetStoredProcCommand("usp_CommissionDueDatesGetList");
         ds = db.ExecuteDataSet(com);
      }
      catch (Exception ex)
      {
         //To Do: Handle Exception
      }

      return ds;
   }


    #region Custom Actions

   public DataTable GetListAsPerChargeSlipId()
   {
       DataSet ds = null;
       try
       {
           DbCommand com = db.GetStoredProcCommand("usp_CommissionDueDatesGetListAsPerChargeSlipId");
           this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
           ds = db.ExecuteDataSet(com);
       }
       catch (Exception ex)
       {
           //To Do: Handle Exception
       }

       return ds.Tables[0];
   }
   public DataTable CheckPaymentpaid()
   {
       DataSet ds = null;
       try
       {
           DbCommand com = db.GetStoredProcCommand("usp_CheckPaymentpaid");
           this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
           ds = db.ExecuteDataSet(com);
       }
       catch (Exception ex)
       {
           //To Do: Handle Exception
       }

       return ds.Tables[0];
   }

   public bool DeleteByChargeSlip()
   {
       try
       {
           if (this.ChargeSlipId > 0)
           {
               DbCommand com = this.db.GetStoredProcCommand("usp_CommissionDueDatesDeleteByChargeSlip");
               this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
               this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, this.DealTransactionId);
               this.db.ExecuteNonQuery(com);
           }
       }
       catch (Exception ex)
       {
           // To Do: Handle Exception
           return false;
       }

       return true;
   }

    #endregion

#endregion

#endregion
}
