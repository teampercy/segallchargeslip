// -----------------------------------------------------------------------
// <copyright file="Locations.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing Locations
/// </summary>
/// 
namespace SegallChargeSlipBll
{
    [Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
    public class Locations
    {
        #region Basic Functionality

        #region Variable Declaration

        /// <summary>
        /// Variable to store Database object to interact with database.
        /// </summary>
        private Database db;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the Locations class.
        /// </summary>
        public Locations()
        {
            this.db = DatabaseFactory.CreateDatabase("ConnectionString");
            this.IsAddressSearch = false;
        }

        /// <summary>
        /// Initializes a new instance of the Locations class.
        /// </summary>
        /// <param name="locationID">Sets the value of LocationID.</param>
        public Locations(int locationID)
        {
            this.db = DatabaseFactory.CreateDatabase();
            this.LocationID = locationID;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets LocationID
        /// </summary>
        public int LocationID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ShoppingCenter
        /// </summary>
        public string ShoppingCenter
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Address1
        /// </summary>
        public string Address1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Address2
        /// </summary>
        public string Address2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets City
        /// </summary>
        public string City
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets State
        /// </summary>
        public string State
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ZipCode
        /// </summary>
        public string ZipCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedBy
        /// </summary>
        public int CreatedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedOn
        /// </summary>
        public DateTime CreatedOn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LastModifiedBy
        /// </summary>
        public int LastModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LastModifiedOn
        /// </summary>
        public DateTime LastModifiedOn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Country
        /// </summary>
        public string Country
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Latitude
        /// </summary>
        public decimal Latitude
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Longitude
        /// </summary>
        public decimal Longitude
        {
            get;
            set;
        }
        //Custom Property 

        public Boolean IsDuplicate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets StateName
        /// </summary>
        public string StateName
        {
            get;
            set;
        }
        public int CurrentPageNo
        {
            get;
            set;

        }
        public int PageSize
        {
            get;
            set;
        }
        public int TotalRecords
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets IsAddressSearch
        /// </summary>
        public Boolean IsAddressSearch
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets LocationID
        /// </summary>
        public Boolean IsActive
        {
            get;
            set;
        }
        public int IsShoppingCenter
        {
            get;
            set;
        }

        #endregion

        #region Actions

        /// <summary>
        /// Loads the details for Locations.
        /// </summary>
        /// <returns>True if Load operation is successful; Else False.</returns>
        public bool Load()
        {
            try
            {
                //if (DateTime.Now <= DateTime.Parse("2014/02/03"))
                //{
                    if (this.LocationID != 0)
                    {
                        DbCommand com = this.db.GetStoredProcCommand("usp_LocationsGetDetails");
                        this.db.AddInParameter(com, "LocationID", DbType.Int32, this.LocationID);
                        DataSet ds = this.db.ExecuteDataSet(com);
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            DataTable dt = ds.Tables[0];
                            this.LocationID = Convert.ToInt32(dt.Rows[0]["LocationID"]);
                            this.ShoppingCenter = Convert.ToString(dt.Rows[0]["ShoppingCenter"]);
                            this.Address1 = Convert.ToString(dt.Rows[0]["Address1"]);
                            this.Address2 = Convert.ToString(dt.Rows[0]["Address2"]);
                            this.City = Convert.ToString(dt.Rows[0]["City"]);
                            this.State = Convert.ToString(dt.Rows[0]["State"]);
                            this.ZipCode = Convert.ToString(dt.Rows[0]["ZipCode"]);
                            this.CreatedBy = Convert.ToInt32(dt.Rows[0]["CreatedBy"]);
                            this.CreatedOn = Convert.ToDateTime(dt.Rows[0]["CreatedOn"]);
                            this.LastModifiedBy = Convert.ToInt32(dt.Rows[0]["LastModifiedBy"]);
                            this.LastModifiedOn = Convert.ToDateTime(dt.Rows[0]["LastModifiedOn"]);
                            this.Country = Convert.ToString(dt.Rows[0]["Country"]);
                            this.Latitude = Convert.ToDecimal(dt.Rows[0]["Latitude"]);
                            this.Longitude = Convert.ToDecimal(dt.Rows[0]["Longitude"]);
                            this.StateName = Convert.ToString(dt.Rows[0]["StateName"]);
                            this.IsActive = Convert.ToBoolean(dt.Rows[0]["IsActive"]);
                            return true;
                        }
                    }
                //}

                return false;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }
        }

        /// <summary>
        /// Inserts details for Locations if LocationID = 0.
        /// Else updates details for Locations.
        /// </summary>
        /// <returns>True if Save operation is successful; Else False.</returns>
        public bool Save()
        {
            //if (this.ShoppingCenter.Trim() != "")
            //{
            //    this.Address1 = "";
            //    this.Address2 = "";
            //}
            //else
            //{
            //    this.ShoppingCenter = "";
            //}

            if (this.LocationID == 0)
            {
                return this.Insert();
            }
            else
            {
                if (this.LocationID > 0)
                {
                    return this.Update();
                }
                else
                {
                    this.LocationID = 0;
                    return false;
                }
            }
        }

        /// <summary>
        /// Inserts details for Locations.
        /// Saves newly created Id in LocationID.
        /// </summary>
        /// <returns>True if Insert operation is successful; Else False.</returns>
        private bool Insert()
        {
            try
            {

                DbCommand com = this.db.GetStoredProcCommand("usp_LocationsInsert");
                this.db.AddOutParameter(com, "LocationID", DbType.Int32, 1024);
                if (!String.IsNullOrEmpty(this.ShoppingCenter))
                {
                    this.db.AddInParameter(com, "ShoppingCenter", DbType.String, this.ShoppingCenter);
                }
                else
                {
                    this.db.AddInParameter(com, "ShoppingCenter", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address1))
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
                }
                else
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address2))
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
                }
                else
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.City))
                {
                    this.db.AddInParameter(com, "City", DbType.String, this.City);
                }
                else
                {
                    this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.State))
                {
                    this.db.AddInParameter(com, "State", DbType.String, this.State);
                }
                else
                {
                    this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.ZipCode))
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, this.ZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, DBNull.Value);
                }
                if (this.CreatedBy > 0)
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
                }
                if (this.LastModifiedBy > 0)
                {
                    this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, this.LastModifiedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, DBNull.Value);
                }
                if (this.LastModifiedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, this.LastModifiedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Country))
                {
                    this.db.AddInParameter(com, "Country", DbType.String, this.Country);
                }
                else
                {
                    this.db.AddInParameter(com, "Country", DbType.String, DBNull.Value);
                }
                if (this.Latitude != 0)
                {
                    this.db.AddInParameter(com, "Latitude", DbType.Decimal, this.Latitude);
                } 
                else
                {
                    this.db.AddInParameter(com, "Latitude", DbType.Double, DBNull.Value);
                }
                if (this.Longitude != 0)
                {
                    this.db.AddInParameter(com, "Longitude", DbType.Decimal, this.Longitude);
                }
                else
                {
                    this.db.AddInParameter(com, "Longitude", DbType.Double, DBNull.Value);
                }
                this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
                this.db.ExecuteNonQuery(com);
                this.LocationID = Convert.ToInt32(this.db.GetParameterValue(com, "LocationID"));      // Read in the output parameter value
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return this.LocationID > 0; // Return whether ID was returned
        }

        /// <summary>
        /// Updates details for Locations.
        /// </summary>
        /// <returns>True if Update operation is successful; Else False.</returns>
        private bool Update()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_LocationsUpdate");
                this.db.AddInParameter(com, "LocationID", DbType.Int32, this.LocationID);
                if (!String.IsNullOrEmpty(this.ShoppingCenter))
                {
                    this.db.AddInParameter(com, "ShoppingCenter", DbType.String, this.ShoppingCenter);
                }
                else
                {
                    this.db.AddInParameter(com, "ShoppingCenter", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address1))
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
                }
                else
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address2))
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
                }
                else
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.City))
                {
                    this.db.AddInParameter(com, "City", DbType.String, this.City);
                }
                else
                {
                    this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.State))
                {
                    this.db.AddInParameter(com, "State", DbType.String, this.State);
                }
                else
                {
                    this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.ZipCode))
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, this.ZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, DBNull.Value);
                }
                if (this.CreatedBy > 0)
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
                }
                if (this.LastModifiedBy > 0)
                {
                    this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, this.LastModifiedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedBy", DbType.Int32, DBNull.Value);
                }
                if (this.LastModifiedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, this.LastModifiedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "LastModifiedOn", DbType.DateTime, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Country))
                {
                    this.db.AddInParameter(com, "Country", DbType.String, this.Country);
                }
                else
                {
                    this.db.AddInParameter(com, "Country", DbType.String, DBNull.Value);
                }
                if (this.Latitude != 0)
                {
                    this.db.AddInParameter(com, "Latitude", DbType.Decimal, this.Latitude);
                }
                else
                {
                    this.db.AddInParameter(com, "Latitude", DbType.Double, DBNull.Value);
                }
                if (this.Longitude != 0)
                {
                    this.db.AddInParameter(com, "Longitude", DbType.Decimal, this.Longitude);
                }
                else
                {
                    this.db.AddInParameter(com, "Longitude", DbType.Double, DBNull.Value);
                }
                this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }
        /// <summary>
        /// Deletes details of Locations for provided LocationID.
        /// </summary>
        /// <returns>True if Delete operation is successful; Else False.</returns>
        public bool Delete()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_LocationsDelete");
                this.db.AddInParameter(com, "LocationID", DbType.Int32, this.LocationID);
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get list of Locations for provided parameters.
        /// </summary>
        /// <returns>DataSet of result</returns>
        /// <remarks></remarks>
        public DataSet GetList()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_LocationsGetList");
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }

        // Custom Action For Serach List

        public DataSet GetLocationSeachList()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_LocationSearchByName");
                this.db.AddInParameter(com, "Location", DbType.String, this.ShoppingCenter);
                this.db.AddInParameter(com, "ISAddress", DbType.Boolean, this.IsAddressSearch);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }

        #endregion


        #region Custom Actions

        public DataTable CheckForDuplication()
        {
            DataSet ds;
            DataTable dt = null;

            DbCommand com = this.db.GetStoredProcCommand("usp_CheckForLocationsDuplication");
            //     this.db.AddOutParameter(com, "LocationID", DbType.Int32, 1024);
            //     this.db.AddOutParameter(com, "IsDuplicate", DbType.Boolean, 2);
            if (!String.IsNullOrEmpty(this.ShoppingCenter))
            {
                this.db.AddInParameter(com, "ShoppingCenter", DbType.String, this.ShoppingCenter);
            }
            else
            {
                this.db.AddInParameter(com, "ShoppingCenter", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Address1))
            {
                this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
            }
            else
            {
                this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
            }
            //JAY Uncommented for the duplicate
            if (!String.IsNullOrEmpty(this.Address2))
            {
                this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
            }
            else
            {
                this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.City))
            {
                this.db.AddInParameter(com, "City", DbType.String, this.City);
            }
            else
            {
                this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.State))
            {
                this.db.AddInParameter(com, "State", DbType.String, this.State);
            }
            else
            {
                this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.ZipCode))
            {
                this.db.AddInParameter(com, "ZipCode", DbType.String, this.ZipCode);
            }
            else
            {
                this.db.AddInParameter(com, "ZipCode", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Country))
            {
                this.db.AddInParameter(com, "Country", DbType.String, this.Country);
            }
            else
            {
                this.db.AddInParameter(com, "Country", DbType.String, DBNull.Value);
            }
            ds = this.db.ExecuteDataSet(com);

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count >= 1)
            {
                dt = ds.Tables[0];
                this.IsDuplicate = true;
            }
            else
            {
                this.IsDuplicate = false;
            }
            return dt;
        }
        public DataSet GetListPageWise()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_LocationsGetPagingList");
                this.db.AddInParameter(com, "CurrentPageNo", DbType.Int32, this.CurrentPageNo);
                this.db.AddInParameter(com, "PageSize", DbType.Int32, this.PageSize);
                this.db.AddInParameter(com, "IsShoppingCenter", DbType.Int32, this.IsShoppingCenter);
                this.db.AddOutParameter(com, "@TotalRecords", DbType.Int32, 0);
                if (!String.IsNullOrEmpty(this.ShoppingCenter))
                {
                    this.db.AddInParameter(com, "ShoppingCenter", DbType.String, this.ShoppingCenter);
                }
                else
                {
                    this.db.AddInParameter(com, "ShoppingCenter", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.City))
                {
                    this.db.AddInParameter(com, "City", DbType.String, this.City);
                }
                else
                {
                    this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.State))
                {
                    this.db.AddInParameter(com, "State", DbType.String, this.State);
                }
                else
                {
                    this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.ZipCode))
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, this.ZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, DBNull.Value);
                }
                ds = db.ExecuteDataSet(com);
                this.TotalRecords = Convert.ToInt32(this.db.GetParameterValue(com, "@TotalRecords"));
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }

        public bool CheckBeforeDelete()
        {
            DataSet ds = null;
            try
            {
                DbCommand com1 = this.db.GetStoredProcCommand("usp_LocationExist");
                this.db.AddInParameter(com1, "LocationID", DbType.Int32, this.LocationID);
                ds = db.ExecuteDataSet(com1);
                if (ds.Tables[0].Rows.Count == 0)
                {
                    DbCommand com = this.db.GetStoredProcCommand("usp_LocationsDelete");
                    this.db.AddInParameter(com, "LocationID", DbType.Int32, this.LocationID);
                    this.db.ExecuteNonQuery(com);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }


        }
        #endregion

        #endregion
    }
}
