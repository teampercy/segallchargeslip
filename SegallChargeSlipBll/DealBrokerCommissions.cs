// -----------------------------------------------------------------------
// <copyright file="DealBrokerCommissions.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing DealBrokerCommissions
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class DealBrokerCommissions
{
    #region Basic Functionality

    #region Variable Declaration

    /// <summary>
    /// Variable to store Database object to interact with database.
    /// </summary>
    private Database db;
    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the DealBrokerCommissions class.
    /// </summary>
    public DealBrokerCommissions()
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
    }

    /// <summary>
    /// Initializes a new instance of the DealBrokerCommissions class.
    /// </summary>
    /// <param name="dealBrokerCommissionId">Sets the value of DealBrokerCommissionId.</param>
    public DealBrokerCommissions(int dealBrokerCommissionId)
    {
        this.db = DatabaseFactory.CreateDatabase();
        this.DealBrokerCommissionId = dealBrokerCommissionId;
    }
    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets DealBrokerCommissionId
    /// </summary>
    public int DealBrokerCommissionId
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets BrokerId
    /// </summary>
    public int BrokerId
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets BrokerTypeId
    /// </summary>
    public int BrokerTypeId
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets ChargeSlipId
    /// </summary>
    public int ChargeSlipId
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets DealTransactionId
    /// </summary>
    public int DealTransactionId
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets BrokerPercent
    /// </summary>
    public decimal BrokerPercent
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets BrokerCommission
    /// </summary>
    public decimal BrokerCommission
    {
        get;
        set;
    }
    public decimal CommissionPaid
    {
        get;
        set;
    }
    /// <summary>
    /// Gets or sets TermTypeId
    /// </summary>
    public int TermTypeId
    {
        get;
        set;
    }
    #endregion

    #region Actions

    /// <summary>
    /// Loads the details for DealBrokerCommissions.
    /// </summary>
    /// <returns>True if Load operation is successful; Else False.</returns>
    public bool Load()
    {
        try
        {
            //if (DateTime.Now <= DateTime.Parse("2014/09/03"))
            //{
                if (this.DealBrokerCommissionId != 0)
                {
                    DbCommand com = this.db.GetStoredProcCommand("usp_DealBrokerCommissionsGetDetails");
                    this.db.AddInParameter(com, "DealBrokerCommissionId", DbType.Int32, this.DealBrokerCommissionId);
                    DataSet ds = this.db.ExecuteDataSet(com);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        this.DealBrokerCommissionId = Convert.ToInt32(dt.Rows[0]["DealBrokerCommissionId"]);
                        this.BrokerId = Convert.ToInt32(dt.Rows[0]["BrokerId"]);
                        this.BrokerTypeId = Convert.ToInt32(dt.Rows[0]["BrokerTypeId"]);
                        this.ChargeSlipId = Convert.ToInt32(dt.Rows[0]["ChargeSlipId"]);
                        this.DealTransactionId = Convert.ToInt32(dt.Rows[0]["DealTransactionId"]);
                        this.BrokerPercent = Convert.ToDecimal(dt.Rows[0]["BrokerPercent"]);
                        this.BrokerCommission = Convert.ToDecimal(dt.Rows[0]["BrokerCommission"]);
                        this.TermTypeId = Convert.ToInt32(dt.Rows[0]["TermTypeId"]);
                        return true;
                    }
               // }
            }

            return false;
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }
    }

    /// <summary>
    /// Inserts details for DealBrokerCommissions if DealBrokerCommissionId = 0.
    /// Else updates details for DealBrokerCommissions.
    /// </summary>
    /// <returns>True if Save operation is successful; Else False.</returns>
    public bool Save()
    {
        if (this.DealBrokerCommissionId == 0)
        {
            return this.Insert();
        }
        else
        {
            if (this.DealBrokerCommissionId > 0)
            {
                return this.Update();
            }
            else
            {
                this.DealBrokerCommissionId = 0;
                return false;
            }
        }
    }

    /// <summary>
    /// Inserts details for DealBrokerCommissions.
    /// Saves newly created Id in DealBrokerCommissionId.
    /// </summary>
    /// <returns>True if Insert operation is successful; Else False.</returns>
    private bool Insert()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_DealBrokerCommissionsInsert");
            this.db.AddOutParameter(com, "DealBrokerCommissionId", DbType.Int32, 1024);
            if (this.BrokerId > 0)
            {
                this.db.AddInParameter(com, "BrokerId", DbType.Int32, this.BrokerId);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerId", DbType.Int32, DBNull.Value);
            }
            if (this.BrokerTypeId > 0)
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, this.BrokerTypeId);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, DBNull.Value);
            }
            if (this.ChargeSlipId > 0)
            {
                this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
            }
            else
            {
                this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
            }
            if (this.DealTransactionId > 0)
            {
                this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, this.DealTransactionId);
            }
            else
            {
                this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, DBNull.Value);
            }
            if (this.BrokerPercent > 0)
            {
                this.db.AddInParameter(com, "BrokerPercent", DbType.Decimal, this.BrokerPercent);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerPercent", DbType.Double, 0);
            }
            if (this.BrokerCommission > 0)
            {
                this.db.AddInParameter(com, "BrokerCommission", DbType.Decimal, this.BrokerCommission);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerCommission", DbType.Double, 0);
            }
            if (this.TermTypeId > 0)
            {
                this.db.AddInParameter(com, "TermTypeId", DbType.Int32, this.TermTypeId);
            }
            else
            {
                this.db.AddInParameter(com, "TermTypeId", DbType.Int32, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
            this.DealBrokerCommissionId = Convert.ToInt32(this.db.GetParameterValue(com, "DealBrokerCommissionId"));      // Read in the output parameter value
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("DealBrokerCommissions", "Insert", ex.Message);
            // To Do: Handle Exception
            return false;
        }

        return this.DealBrokerCommissionId > 0; // Return whether ID was returned
    }

    /// <summary>
    /// Updates details for DealBrokerCommissions.
    /// </summary>
    /// <returns>True if Update operation is successful; Else False.</returns>
    /// 
    public bool UpdateDealBrokerCommissions()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_DealBrokerCommissionsUpdate");
            if (this.BrokerId > 0)
            {
                this.db.AddInParameter(com, "BrokerId", DbType.Int32, this.BrokerId);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerId", DbType.Int32, DBNull.Value);
            }
            if (this.ChargeSlipId > 0)
            {
                this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
            }
            else
            {
                this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
            }
            if (this.CommissionPaid > 0)
            {
                this.db.AddInParameter(com, "CommissionPaid", DbType.Decimal, this.CommissionPaid);
            }
            else
            {
                this.db.AddInParameter(com, "CommissionPaid", DbType.Double, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }
        return true;
    }
    private bool Update()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_upDealBrokerCommissionsUpdate");
            this.db.AddInParameter(com, "DealBrokerCommissionId", DbType.Int32, this.DealBrokerCommissionId);
            if (this.BrokerId > 0)
            {
                this.db.AddInParameter(com, "BrokerId", DbType.Int32, this.BrokerId);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerId", DbType.Int32, DBNull.Value);
            }
            if (this.BrokerTypeId > 0)
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, this.BrokerTypeId);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, DBNull.Value);
            }
            if (this.ChargeSlipId > 0)
            {
                this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
            }
            else
            {
                this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
            }
            if (this.DealTransactionId > 0)
            {
                this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, this.DealTransactionId);
            }
            else
            {
                this.db.AddInParameter(com, "DealTransactionId", DbType.Int32, DBNull.Value);
            }
            if (this.BrokerPercent > 0)
            {
                this.db.AddInParameter(com, "BrokerPercent", DbType.Double, this.BrokerPercent);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerPercent", DbType.Double, DBNull.Value);
            }
            if (this.BrokerCommission > 0)
            {
                this.db.AddInParameter(com, "BrokerCommission", DbType.Decimal, this.BrokerCommission);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerCommission", DbType.Double, DBNull.Value);
            }
            if (this.TermTypeId > 0)
            {
                this.db.AddInParameter(com, "TermTypeId", DbType.Int32, this.TermTypeId);
            }
            else
            {
                this.db.AddInParameter(com, "TermTypeId", DbType.Int32, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Deletes details of DealBrokerCommissions for provided DealBrokerCommissionId.
    /// </summary>
    /// <returns>True if Delete operation is successful; Else False.</returns>
    public bool Delete()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_upDealBrokerCommissionsDelete");
            this.db.AddInParameter(com, "DealBrokerCommissionId", DbType.Int32, this.DealBrokerCommissionId);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Get list of DealBrokerCommissions for provided parameters.
    /// </summary>
    /// <returns>DataSet of result</returns>
    /// <remarks></remarks>
    public DataSet GetList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_DealBrokerCommissionsGetList");
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    #endregion

    #region Custom Action

    public DataSet GetListByChargeSlip()
    {
        DataSet ds = null;

        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_DealBrokerCommissionsGetListByChargeSlip");
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
            this.db.AddInParameter(com, "TermTypeId", DbType.Int32, this.TermTypeId);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }
        return ds;
    }

    // Set Data In DealBrokerCommissions as per change in datatable
    public DataSet SetCommAmountAsPerChangeInBaseAmt(decimal CommOnAmount)
    {
        DataSet ds = null;

        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_DealBrokerCommissionsUpdateAsPerBaseAmount");
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
            this.db.AddInParameter(com, "TermTypeId", DbType.Int32, this.TermTypeId);
            this.db.AddInParameter(com, "CommOnAmount", DbType.Decimal, CommOnAmount);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }
        return ds;
    }

    /// <summary>
    /// Deletes details of DealBrokerCommissions for provided ChargeSlipId In case Any Related Terms records are deleted from Lease Term Table
    /// </summary>
    /// <returns>True if Delete operation is successful; Else False.</returns>
    public bool DeleteDealBrokerCommissionsAsPerLeaseTerms()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_DeleteDealBrokerCommissionsAsPerLeaseTerms");
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
            //this.db.AddInParameter(com, "TermTypeId", DbType.Int32, this.TermTypeId);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }
    #endregion


    #endregion
}
