// -----------------------------------------------------------------------
// <copyright file="PaymentHistory.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing PaymentHistory
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class PaymentHistory
{
#region Basic Functionality

#region Variable Declaration

   /// <summary>
   /// Variable to store Database object to interact with database.
   /// </summary>
   private Database db;
#endregion

#region Constructors

   /// <summary>
   /// Initializes a new instance of the PaymentHistory class.
   /// </summary>
   public PaymentHistory()
   {
       this.db = DatabaseFactory.CreateDatabase("ConnectionString");
   }

   /// <summary>
   /// Initializes a new instance of the PaymentHistory class.
   /// </summary>
   /// <param name="paymentHistoryId">Sets the value of PaymentHistoryId.</param>
   public PaymentHistory(int paymentHistoryId)
   {
       this.db = DatabaseFactory.CreateDatabase("ConnectionString");
      this.PaymentHistoryId = paymentHistoryId;
   }
#endregion

#region Properties

   /// <summary>
   /// Gets or sets PaymentHistoryId
   /// </summary>
   public int PaymentHistoryId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets ChargeSlipId
   /// </summary>
   public int ChargeSlipId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets BrokerId
   /// </summary>
   public int BrokerId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets PaymentDate
   /// </summary>
   public DateTime PaymentDate
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets PaymentReceived
   /// </summary>
   public decimal PaymentReceived
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets CheckNo
   /// </summary>
   public string CheckNo
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets PaymentId
   /// </summary>
   public int PaymentId
   {
      get; set;
   }
#endregion

#region Actions

   /// <summary>
   /// Loads the details for PaymentHistory.
   /// </summary>
   /// <returns>True if Load operation is successful; Else False.</returns>
   public bool Load()
   {
      try
      {
         if (this.PaymentHistoryId != 0)
         {
            DbCommand com = this.db.GetStoredProcCommand("usp_PaymentHistoryGetDetails");
            this.db.AddInParameter(com, "PaymentHistoryId", DbType.Int32, this.PaymentHistoryId);
            DataSet ds = this.db.ExecuteDataSet(com);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
               DataTable dt = ds.Tables[0];
               this.PaymentHistoryId = Convert.ToInt32(dt.Rows[0]["PaymentHistoryId"]);
               this.ChargeSlipId = Convert.ToInt32(dt.Rows[0]["ChargeSlipId"]);
               this.BrokerId = Convert.ToInt32(dt.Rows[0]["BrokerId"]);
               this.PaymentDate = Convert.ToDateTime(dt.Rows[0]["PaymentDate"]);
               this.PaymentReceived = Convert.ToDecimal(dt.Rows[0]["PaymentReceived"]);
               this.CheckNo = Convert.ToString(dt.Rows[0]["CheckNo"]);
               this.PaymentId = Convert.ToInt32(dt.Rows[0]["PaymentId"]);
               return true;
            }
         }

      return false;
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }
   }

   /// <summary>
   /// Inserts details for PaymentHistory if PaymentHistoryId = 0.
   /// Else updates details for PaymentHistory.
   /// </summary>
   /// <returns>True if Save operation is successful; Else False.</returns>
   public bool Save()
   {
      if (this.PaymentHistoryId == 0)
      {
         return this.Insert();
      }
      else
      {
         if (this.PaymentHistoryId > 0)
         {
            return this.Update();
         }
         else
         {
            this.PaymentHistoryId = 0;
            return false;
         }
      }
   }

   /// <summary>
   /// Inserts details for PaymentHistory.
   /// Saves newly created Id in PaymentHistoryId.
   /// </summary>
   /// <returns>True if Insert operation is successful; Else False.</returns>
   private bool Insert()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_PaymentHistoryInsert");
         this.db.AddOutParameter(com, "PaymentHistoryId", DbType.Int32, 1024);
         if (this.ChargeSlipId > 0)
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
         }
         else
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
         }
         if (this.BrokerId > 0)
         {
            this.db.AddInParameter(com, "BrokerId", DbType.Int32, this.BrokerId);
         }
         else
         {
            this.db.AddInParameter(com, "BrokerId", DbType.Int32, DBNull.Value);
         }
         if (this.PaymentDate > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "PaymentDate", DbType.DateTime, this.PaymentDate);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentDate", DbType.DateTime, DBNull.Value);
         }
         if (this.PaymentReceived > 0)
         {
            this.db.AddInParameter(com, "PaymentReceived", DbType.Decimal, this.PaymentReceived);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentReceived", DbType.Double, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.CheckNo))
         {
            this.db.AddInParameter(com, "CheckNo", DbType.String, this.CheckNo);
         }
         else
         {
            this.db.AddInParameter(com, "CheckNo", DbType.String, DBNull.Value);
         }
         if (this.PaymentId > 0)
         {
            this.db.AddInParameter(com, "PaymentId", DbType.Int32, this.PaymentId);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentId", DbType.Int32, DBNull.Value);
         }
         this.db.ExecuteNonQuery(com);
         this.PaymentHistoryId = Convert.ToInt32(this.db.GetParameterValue(com, "PaymentHistoryId"));      // Read in the output parameter value
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return this.PaymentHistoryId > 0; // Return whether ID was returned
   }

   /// <summary>
   /// Updates details for PaymentHistory.
   /// </summary>
   /// <returns>True if Update operation is successful; Else False.</returns>
   private bool Update()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_PaymentHistoryUpdate");
         this.db.AddInParameter(com, "PaymentHistoryId", DbType.Int32, this.PaymentHistoryId);
         if (this.ChargeSlipId > 0)
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
         }
         else
         {
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, DBNull.Value);
         }
         if (this.BrokerId > 0)
         {
            this.db.AddInParameter(com, "BrokerId", DbType.Int32, this.BrokerId);
         }
         else
         {
            this.db.AddInParameter(com, "BrokerId", DbType.Int32, DBNull.Value);
         }
         if (this.PaymentDate > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "PaymentDate", DbType.DateTime, this.PaymentDate);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentDate", DbType.DateTime, DBNull.Value);
         }
         if (this.PaymentReceived > 0)
         {
            this.db.AddInParameter(com, "PaymentReceived", DbType.Decimal, this.PaymentReceived);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentReceived", DbType.Double, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.CheckNo))
         {
            this.db.AddInParameter(com, "CheckNo", DbType.String, this.CheckNo);
         }
         else
         {
            this.db.AddInParameter(com, "CheckNo", DbType.String, DBNull.Value);
         }
         if (this.PaymentId > 0)
         {
            this.db.AddInParameter(com, "PaymentId", DbType.Int32, this.PaymentId);
         }
         else
         {
            this.db.AddInParameter(com, "PaymentId", DbType.Int32, DBNull.Value);
         }
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Deletes details of PaymentHistory for provided PaymentHistoryId.
   /// </summary>
   /// <returns>True if Delete operation is successful; Else False.</returns>
   public bool Delete()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_PaymentHistoryDelete");
         this.db.AddInParameter(com, "PaymentHistoryId", DbType.Int32, this.PaymentHistoryId);
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Get list of PaymentHistory for provided parameters.
   /// </summary>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetList()
   {
      DataSet ds = null;
      try
      {
         DbCommand com = db.GetStoredProcCommand("usp_PaymentHistoryGetList");
         ds = db.ExecuteDataSet(com);
      }
      catch (Exception ex)
      {
         //To Do: Handle Exception
      }

      return ds;
   }

   /// <summary>
   /// Get list of PaymentHistory for provided parameters optimized for custom paging technique.
   /// </summary>
   /// <param name="filter">Provide expression for filter result</param>
   /// <param name="sort">Provide expression to sort result</param>
   /// <param name="pageSize">Provide page size for custom paging.</param>
   /// <param name="pageNumber">Provide page number for custom paging</param>
   /// <param name="rowCount">Get total count of rows in result </param>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetPagingList(string filter, string sort, int pageSize, int pageNumber, int rowCount)
   {
      DataSet ds = null;
      try
      {
         DbCommand com = db.GetStoredProcCommand("usp_PaymentHistoryGetPagingList");
         db.AddInParameter(com, "Filter", DbType.String, filter);
         db.AddInParameter(com, "Sort", DbType.String, sort);
         db.AddInParameter(com, "PageNo", DbType.Int32, pageSize);
         db.AddInParameter(com, "PageSize", DbType.Int32, pageNumber);
         db.AddInParameter(com, "RowCount", DbType.Int32, rowCount);
         ds = db.ExecuteDataSet(com);
      }
      catch (Exception ex)
      {
         //To Do: Handle Exception
      }

      return ds;
   }

   /// <summary>
   /// Get list of PaymentHistory for provided parameters optimized for custom paging technique.
   /// </summary>
   /// <param name="expression">Provide expression for filter or sort result</param>
   /// <param name="isFilterExpression">If provided expression is filter expression then True; Else if sort expression then false.</param>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetPagingList(string expression, bool isFilterExpression)
   {
      if (isFilterExpression == true)
      {
         return GetPagingList(expression, "", 0, 0, 0);
      }
      else
      {
         return GetPagingList("", expression, 0, 0, 0);
      }
   }

   /// <summary>
   /// Get list of PaymentHistory for provided parameters optimized for custom paging technique.
   /// </summary>
   /// <param name="pageSize">Provide page size for custom paging.</param>
   /// <param name="pageNumber">Provide page number for custom paging</param>
   /// <param name="rowCount">Get total count of rows in result </param>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetPagingList(int pageSize, int pageNumber, int rowCount)
   {
      return GetPagingList("", "", pageSize, pageNumber, rowCount);
   }
#endregion

#endregion
}
