﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
using System.Collections.Generic;

namespace SegallChargeSlipBll
{
    [Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
    public class SnapShot
    {
        #region Basic Functionality

        #region Variable Declaration

        /// <summary>
        /// Variable to store Database object to interact with database.
        /// </summary>
        private Database db;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the DealBrokerCommissions class.
        /// </summary>
        public SnapShot()
        {
            this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        }

        #endregion
        #endregion


        #region Properties 
        public int UserId { get; set; }
        public DateTime Begindate { get; set; }
        public DateTime EndDate { get; set; }
        public Boolean IsAdmin { get; set; }
        #endregion


        #region Cusstom Actions

        public DataSet GetSnapShotList()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_GetSnapStDateDetails");
                this.db.AddInParameter(com, "UserId", DbType.Int32, this.UserId);
                this.db.AddInParameter(com, "Isadmin", DbType.Boolean, this.IsAdmin);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }
            return ds;
        }

        public DataSet GetRecevableSummary()
        {
            DataSet ds = null;
            try
            {
                //if (DateTime.Now <= DateTime.Parse("2016/04/30")) //Commented by Jaywanti on 2016/03/30// 03 is month.
                //{
                DbCommand com = db.GetStoredProcCommand("usp_getrecevableSummary_Nov2015");
                this.db.AddInParameter(com, "UserId", DbType.Int32, this.UserId);
                this.db.AddInParameter(com, "Begindate", DbType.DateTime, this.Begindate);
                this.db.AddInParameter(com, "Enddate", DbType.DateTime, this.EndDate);
                ds = db.ExecuteDataSet(com);
                //}
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }
            return ds;
        }
        public DataSet GetRecevableSummaryDetails(int index)
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_ReceivableSummaryDetails");
                this.db.AddInParameter(com, "UserId", DbType.Int32, this.UserId);
                this.db.AddInParameter(com, "Begindate", DbType.DateTime, this.Begindate);
                this.db.AddInParameter(com, "Enddate", DbType.DateTime, this.EndDate);
                this.db.AddInParameter(com, "IndexNo", DbType.Int32, index);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }
            return ds;
        }

        //public DataSet GetSnapShotDetails()
        //{
        //    DataSet ds = null;
        //    try
        //    {
        //        DbCommand com = db.GetStoredProcCommand("usp_GetSnapStPastDue");
        //        this.db.AddInParameter(com, "UserId", DbType.Int32, this.UserId);
        //        this.db.AddInParameter(com, "ReportType", DbType.Int32, this.ReportType);
        //        ds = db.ExecuteDataSet(com);
        //    }
        //    catch (Exception ex)
        //    {
        //        //To Do: Handle Exception
        //    }
        //    return ds;
        //}
        //#endregion


        #endregion

        #region Timeline Menu Section       
        public List<TimelineChartDataChargeslip> GetTimelineChartChargeSlip(int userId, int Year,string duration)
        {
            List<TimelineChartDataChargeslip> ChartDataChargeslipList = new List<TimelineChartDataChargeslip>();
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_TimelineChartSummary");
                this.db.AddInParameter(com, "userId", DbType.Int32, userId);
                this.db.AddInParameter(com, "Year", DbType.Int32, Year);
                this.db.AddInParameter(com, "duration", DbType.String, duration);
                using (DataSet ds = db.ExecuteDataSet(com))
                {
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            ChartDataChargeslipList.Add(new TimelineChartDataChargeslip()
                            {
                                ChargeSlipId = Convert.ToInt32(dr["ChargeSlipId"]),
                                DealName = Convert.ToString(dr["DealName"]),
                                DueDateShort = Convert.ToString(dr["DueDateShort"]),
                                CommAmountDue = Convert.ToDouble(dr["CommAmountDue"]),
                                PaymentDateShort = Convert.ToString(dr["PaymentDateShort"]),
                                PaidAmount = Convert.ToDouble(dr["PaidAmount"]),
                                UnApplied = Convert.ToDouble(dr["UnApplied"]),
                                CommissionDueId = Convert.ToInt32(dr["CommissionDueId"]),
                                IsFullCommPaid = Convert.ToBoolean(dr["IsFullCommPaid"]),
                                Duration = Convert.ToString(dr["MonthYear"])
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }
            return ChartDataChargeslipList;
        }

        public List<DashboardTimelineChart> GetTimelineChartData(int userId, int Year, string duration)
        {
            List<DashboardTimelineChart> ChartData = new List<DashboardTimelineChart>();
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_TimelineChartSummary");
                this.db.AddInParameter(com, "userId", DbType.Int32, userId);
                this.db.AddInParameter(com, "Year", DbType.Int32, Year);
                this.db.AddInParameter(com, "duration", DbType.String, duration);
                using (DataSet ds = db.ExecuteDataSet(com))
                {
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        ChartData.Add(GetChartDataFromDatatable("TotalMonthCommAmtDue", ds.Tables[1], "CommAmountDueCount"));
                        ChartData.Add(GetChartDataFromDatatable("TotalMonthPaidAmount", ds.Tables[1], "CommAmountPaidCount"));
                    }
                }
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }
            return ChartData;
        }

        private DashboardTimelineChart GetChartDataFromDatatable(string Legend, DataTable dtChartData, string Count)
        {
            DashboardTimelineChart obj = new DashboardTimelineChart();
            obj.Legend = Legend;
            //obj.AnnotationCount = Count;

            List<TimelineChartData> DataList = new List<TimelineChartData>();
            /*It is assume that chart x axis will have months for now. So we are creating collection having 
             * month and data
            */
            foreach (DataRow dr in dtChartData.Rows)
            {
                DataList.Add(new TimelineChartData()
                {
                    XaxisValue = Convert.ToString(dr["MonthYear"]),
                    YaxisValue = Convert.ToDouble(dr[Legend]),
                    CaseCount = Convert.ToInt32(dr[Count])
                });
            }
            obj.ChartData = DataList;

            return obj;
        }

        public class DashboardTimelineChart
        {
            public string Legend { get; set; }
            public List<TimelineChartData> ChartData { get; set; }
            //public string AnnotationCount { get; set; }
        }

        public class TimelineChartData
        {
            public string XaxisValue { get; set; }
            public double YaxisValue { get; set; }
            public int CaseCount { get; set; }
        }

        public class TimelineChartDataChargeslip
        {
            public int ChargeSlipId { get; set; }
            public string DealName { get; set; }
            public string DueDateShort { get; set; }
            public double CommAmountDue { get; set; }
            public string PaymentDateShort { get; set; }
            public double PaidAmount { get; set; }
            public double UnApplied { get; set; }
            public Boolean IsFullCommPaid { get; set; }
            public int CommissionDueId { get; set; }
            public string Duration { get; set; } 
        }

        #endregion
    }
}