// -----------------------------------------------------------------------
// <copyright file="DealTransactions.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing DealTransactions
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class DealTransactions
{
    #region Basic Functionality

    #region Variable Declaration

    /// <summary>
    /// Variable to store Database object to interact with database.
    /// </summary>
    private Database db;
    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the DealTransactions class.
    /// </summary>
    public DealTransactions()
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
    }
    /// <summary>
    /// Initializes a new instance of the DealTransactions class.
    /// </summary>
    /// <param name="dealTransactionID">Sets the value of DealTransactionID.</param>
    public DealTransactions(int dealTransactionID)
    {
        this.db = DatabaseFactory.CreateDatabase();
        this.DealTransactionID = dealTransactionID;
    }
    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets DealTransactionID
    /// </summary>
    public int DealTransactionID
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets ChargeSlipID
    /// </summary>
    public Int64 ChargeSlipID
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets DealType
    /// </summary>
    public int DealType
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets DealSubType
    /// </summary>
    public int DealSubType
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Size
    /// </summary>
    public decimal Size
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets BldgSize
    /// </summary>
    public decimal BldgSize
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Buyer
    /// </summary>
    public int Buyer
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets DealUse
    /// </summary>
    public string DealUse
    {
        get;
        set;
    }
    public string DealUseText
    {
        get;
        set;
    }
    /// <summary>
    /// Gets or sets SalePrice
    /// </summary>
    public decimal SalePrice
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets DueDate
    /// </summary>
    public DateTime DueDate
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Commission
    /// </summary>
    public decimal Commission
    {
        get;
        set;
    }
    public int CommissionDueDateId
    {
        get;
        set;
    }
    /// <summary>
    /// Gets or sets SetFee
    /// </summary>
    public decimal SetFee
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Notes
    /// </summary>
    public string Notes
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Tenant
    /// </summary>
    public int Tenant
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets LeaseStartDate
    /// </summary>
    public DateTime LeaseStartDate
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets CAM
    /// </summary>
    public decimal CAM
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets INS
    /// </summary>
    public decimal INS
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets TAX
    /// </summary>
    public decimal TAX
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets NNN
    /// </summary>
    public decimal NNN
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets TI
    /// </summary>
    public decimal TI
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets FreeRent
    /// </summary>
    public decimal FreeRent
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets UnitValue
    /// </summary>
    public int UnitValue
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets UnitTypeValue
    /// </summary>
    public int UnitTypeValue
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Nets
    /// </summary>
    public decimal Nets
    {
        get;
        set;
    }
    // Newly added 19/08/2013 --Diptee

    /// <summary>
    /// Gets or sets UnitValue
    /// </summary>
    public int FreeRentUnitValue
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets UnitTypeValue
    /// </summary>
    public int FreeRentUnitTypeValue
    {
        get;
        set;
    }


    /// <summary>
    /// Gets or sets BaseLeaseTerm
    /// </summary>
    public int BaseLeaseTerm
    {
        get;
        set;
    }


    /// <summary>
    /// Gets or sets BaseCommissionableTerm
    /// </summary>
    public int BaseCommissionableTerm
    {
        get;
        set;
    }



    /// <summary>
    /// Gets or sets OptionLeaseTerm
    /// </summary>
    public int OptionLeaseTerm
    {
        get;
        set;
    }


    /// <summary>
    /// Gets or sets OptionCommissionableTerm
    /// </summary>
    public int OptionCommissionableTerm
    {
        get;
        set;
    }


    /// <summary>
    /// Gets or sets UnitTypeValue
    /// </summary>
    public int ContingentLeaseTerm
    {
        get;
        set;
    }


    /// <summary>
    /// Gets or sets UnitTypeValue
    /// </summary>
    public int ContingentCommissionableTerm
    {
        get;
        set;
    }


    /// <summary>
    /// Gets or sets Additional Notes
    /// </summary>
    public string AdditionalNotes
    {
        get;
        set;
    }
    public string DealPartyCompanyName
    {
        get;
        set;
    }

    //Added By Shishir 29-04-2016
    public int BrokerID
    { get; set; }

    //Added by Shishir 17-05-2016
    public int TIAllowanceType
    {
        get;
        set;
    }
    #endregion

    #region Actions

    /// <summary>
    /// Loads the details for DealTransactions.
    /// </summary>
    /// <returns>True if Load operation is successful; Else False.</returns>
    public bool Load()
    {
        try
        {
            //if (DateTime.Now <= DateTime.Parse("2014/09/03"))
            //{
            if (this.DealTransactionID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_DealTransactionsGetDetails");
                this.db.AddInParameter(com, "DealTransactionID", DbType.Int32, this.DealTransactionID);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    this.DealTransactionID = Convert.ToInt32(dt.Rows[0]["DealTransactionID"]);
                    this.ChargeSlipID = Convert.ToInt32(dt.Rows[0]["ChargeSlipID"]);
                    this.DealType = Convert.ToInt32(dt.Rows[0]["DealType"]);
                    this.DealSubType = Convert.ToInt32(dt.Rows[0]["DealSubType"]);
                    this.Size = Convert.ToDecimal(dt.Rows[0]["Size"]);
                    this.BldgSize = Convert.ToDecimal(dt.Rows[0]["BldgSize"]);
                    this.Buyer = Convert.ToInt32(dt.Rows[0]["Buyer"]);
                    this.DealUse = Convert.ToString(dt.Rows[0]["DealUse"]);
                    this.SalePrice = Convert.ToDecimal(dt.Rows[0]["SalePrice"]);
                    this.DueDate = Convert.ToDateTime(dt.Rows[0]["DueDate"]);
                    this.Commission = Convert.ToDecimal(dt.Rows[0]["Commission"]);
                    this.SetFee = Convert.ToDecimal(dt.Rows[0]["SetFee"]);
                    this.Notes = Convert.ToString(dt.Rows[0]["Notes"]);
                    this.Tenant = Convert.ToInt32(dt.Rows[0]["Tenant"]);
                    this.LeaseStartDate = Convert.ToDateTime(dt.Rows[0]["LeaseStartDate"]);
                    this.CAM = Convert.ToDecimal(dt.Rows[0]["CAM"]);
                    this.INS = Convert.ToDecimal(dt.Rows[0]["INS"]);
                    this.TAX = Convert.ToDecimal(dt.Rows[0]["TAX"]);
                    this.NNN = Convert.ToDecimal(dt.Rows[0]["NNN"]);
                    this.TI = Convert.ToDecimal(dt.Rows[0]["TI"]);
                    this.FreeRent = Convert.ToDecimal(dt.Rows[0]["FreeRent"]);
                    this.UnitValue = Convert.ToInt32(dt.Rows[0]["UnitValue"]);
                    this.UnitTypeValue = Convert.ToInt32(dt.Rows[0]["UnitTypeValue"]);
                    this.Nets = Convert.ToDecimal(dt.Rows[0]["Nets"]);
                    this.FreeRentUnitValue = Convert.ToInt32(dt.Rows[0]["FreeRentUnitValue"]);
                    this.FreeRentUnitTypeValue = Convert.ToInt32(dt.Rows[0]["FreeRentUnitTypeValue"]);
                    this.AdditionalNotes = Convert.ToString(dt.Rows[0]["AdditionalNotes"]);
                    this.TIAllowanceType = Convert.ToInt32(dt.Rows[0]["TIAllowanceType"]);//Added by Shishir 17-05-2016
                    return true;
                }
                //}
            }

            return false;
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }
    }

    /// <summary>
    /// Inserts details for DealTransactions if DealTransactionID = 0.
    /// Else updates details for DealTransactions.
    /// </summary>
    /// <returns>True if Save operation is successful; Else False.</returns>
    public bool Save()
    {
        if (this.DealTransactionID == 0)
        {
            return this.Insert();
        }
        else
        {
            if (this.DealTransactionID > 0)
            {
                return this.Update();
            }
            else
            {
                this.DealTransactionID = 0;
                return false;
            }
        }
    }

    /// <summary>
    /// Inserts details for DealTransactions.
    /// Saves newly created Id in DealTransactionID.
    /// </summary>
    /// <returns>True if Insert operation is successful; Else False.</returns>
    private bool Insert()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_DealTransactionsInsert");
            this.db.AddOutParameter(com, "DealTransactionID", DbType.Int32, 1024);
            if (this.ChargeSlipID > 0)
            {
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
            }
            else
            {
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, DBNull.Value);
            }
            if (this.DealType > 0)
            {
                this.db.AddInParameter(com, "DealType", DbType.Int32, this.DealType);
            }
            else
            {
                this.db.AddInParameter(com, "DealType", DbType.Int32, DBNull.Value);
            }
            if (this.DealSubType > 0)
            {
                this.db.AddInParameter(com, "DealSubType", DbType.Int32, this.DealSubType);
            }
            else
            {
                this.db.AddInParameter(com, "DealSubType", DbType.Int32, DBNull.Value);
            }
            if (this.Size > 0)
            {
                this.db.AddInParameter(com, "Size", DbType.Decimal, this.Size);
            }
            else
            {
                this.db.AddInParameter(com, "Size", DbType.Double, 0);
            }
            if (this.BldgSize > 0)
            {
                this.db.AddInParameter(com, "BldgSize", DbType.Decimal, this.BldgSize);
            }
            else
            {
                this.db.AddInParameter(com, "BldgSize", DbType.Double, 0);
            }
            if (this.Buyer > 0)
            {
                this.db.AddInParameter(com, "Buyer", DbType.Int32, this.Buyer);
            }
            else
            {
                this.db.AddInParameter(com, "Buyer", DbType.Int32, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.DealUse))
            {
                this.db.AddInParameter(com, "DealUse", DbType.String, this.DealUse);
            }
            else
            {
                this.db.AddInParameter(com, "DealUse", DbType.String, DBNull.Value);
            }
            if (this.SalePrice > 0)
            {
                this.db.AddInParameter(com, "SalePrice", DbType.Decimal, this.SalePrice);
            }
            else
            {
                this.db.AddInParameter(com, "SalePrice", DbType.Double, DBNull.Value);
            }
            if (this.DueDate > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "DueDate", DbType.DateTime, this.DueDate);
            }
            else
            {
                this.db.AddInParameter(com, "DueDate", DbType.DateTime, DBNull.Value);
            }
            if (this.Commission > 0)
            {
                this.db.AddInParameter(com, "Commission", DbType.Decimal, this.Commission);
            }
            else
            {
                this.db.AddInParameter(com, "Commission", DbType.Double, DBNull.Value);
            }
            if (this.SetFee > 0)
            {
                this.db.AddInParameter(com, "SetFee", DbType.Decimal, this.SetFee);
            }
            else
            {
                this.db.AddInParameter(com, "SetFee", DbType.Double, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Notes))
            {
                this.db.AddInParameter(com, "Notes", DbType.String, this.Notes);
            }
            else
            {
                this.db.AddInParameter(com, "Notes", DbType.String, DBNull.Value);
            }
            if (this.Tenant > 0)
            {
                this.db.AddInParameter(com, "Tenant", DbType.Int32, this.Tenant);
            }
            else
            {
                this.db.AddInParameter(com, "Tenant", DbType.Int32, DBNull.Value);
            }
            if (this.LeaseStartDate > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "LeaseStartDate", DbType.DateTime, this.LeaseStartDate);
            }
            else
            {
                this.db.AddInParameter(com, "LeaseStartDate", DbType.DateTime, DBNull.Value);
            }
            if (this.CAM > 0)
            {
                this.db.AddInParameter(com, "CAM", DbType.Decimal, this.CAM);
            }
            else
            {
                this.db.AddInParameter(com, "CAM", DbType.Double, DBNull.Value);
            }
            if (this.INS > 0)
            {
                this.db.AddInParameter(com, "INS", DbType.Decimal, this.INS);
            }
            else
            {
                this.db.AddInParameter(com, "INS", DbType.Double, DBNull.Value);
            }
            if (this.TAX > 0)
            {
                this.db.AddInParameter(com, "TAX", DbType.Decimal, this.TAX);
            }
            else
            {
                this.db.AddInParameter(com, "TAX", DbType.Double, DBNull.Value);
            }
            if (this.NNN > 0)
            {
                this.db.AddInParameter(com, "NNN", DbType.Decimal, this.NNN);
            }
            else
            {
                this.db.AddInParameter(com, "NNN", DbType.Double, DBNull.Value);
            }
            if (this.TI > 0)
            {
                this.db.AddInParameter(com, "TI", DbType.Decimal, this.TI);
            }
            else
            {
                this.db.AddInParameter(com, "TI", DbType.Double, DBNull.Value);
            }
            if (this.FreeRent > 0)
            {
                this.db.AddInParameter(com, "FreeRent", DbType.Decimal, this.FreeRent);
            }
            else
            {
                this.db.AddInParameter(com, "FreeRent", DbType.Double, DBNull.Value);
            }
            if (this.UnitValue > 0)
            {
                this.db.AddInParameter(com, "UnitValue", DbType.Int32, this.UnitValue);
            }
            else
            {
                this.db.AddInParameter(com, "UnitValue", DbType.Int32, DBNull.Value);
            }
            if (this.UnitTypeValue > 0)
            {
                this.db.AddInParameter(com, "UnitTypeValue", DbType.Int32, this.UnitTypeValue);
            }
            else
            {
                this.db.AddInParameter(com, "UnitTypeValue", DbType.Int32, DBNull.Value);
            }
            if (this.Nets > 0)
            {
                this.db.AddInParameter(com, "Nets", DbType.Decimal, this.Nets);
            }
            else
            {
                this.db.AddInParameter(com, "Nets", DbType.Double, DBNull.Value);
            }
            // 19/08/2013--Diptee
            if (this.FreeRentUnitValue > 0)
            {
                this.db.AddInParameter(com, "FreeRentUnitValue", DbType.Int32, this.FreeRentUnitValue);
            }
            else
            {
                this.db.AddInParameter(com, "FreeRentUnitValue", DbType.Int32, DBNull.Value);
            }
            if (this.FreeRentUnitTypeValue > 0)
            {
                this.db.AddInParameter(com, "FreeRentUnitTypeValue", DbType.Int32, this.FreeRentUnitTypeValue);
            }
            else
            {
                this.db.AddInParameter(com, "FreeRentUnitTypeValue", DbType.Int32, DBNull.Value);
            }
            //Added by Shishir 17-05-2016
            if (this.TIAllowanceType > 0)
            {
                this.db.AddInParameter(com, "TIAllowanceType", DbType.Decimal, this.TIAllowanceType);
            }
            else
            {
                this.db.AddInParameter(com, "TIAllowanceType", DbType.Double, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
            this.DealTransactionID = Convert.ToInt32(this.db.GetParameterValue(com, "DealTransactionID"));      // Read in the output parameter value
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return this.DealTransactionID > 0; // Return whether ID was returned
    }

    /// <summary>
    /// Updates details for DealTransactions.
    /// </summary>
    /// <returns>True if Update operation is successful; Else False.</returns>
    private bool Update()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_DealTransactionsUpdate");
            this.db.AddInParameter(com, "DealTransactionID", DbType.Int32, this.DealTransactionID);
            if (this.ChargeSlipID > 0)
            {
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
            }
            else
            {
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, DBNull.Value);
            }
            if (this.DealType > 0)
            {
                this.db.AddInParameter(com, "DealType", DbType.Int32, this.DealType);
            }
            else
            {
                this.db.AddInParameter(com, "DealType", DbType.Int32, 0);
            }
            if (this.DealSubType > 0)
            {
                this.db.AddInParameter(com, "DealSubType", DbType.Int32, this.DealSubType);
            }
            else
            {
                this.db.AddInParameter(com, "DealSubType", DbType.Int32, 0);
            }
            if (this.Size > 0)
            {
                this.db.AddInParameter(com, "Size", DbType.Decimal, this.Size);
            }
            else
            {
                this.db.AddInParameter(com, "Size", DbType.Double, DBNull.Value);
            }
            if (this.BldgSize > 0)
            {
                this.db.AddInParameter(com, "BldgSize", DbType.Decimal, this.BldgSize);
            }
            else
            {
                this.db.AddInParameter(com, "BldgSize", DbType.Double, DBNull.Value);
            }
            if (this.Buyer > 0)
            {
                this.db.AddInParameter(com, "Buyer", DbType.Int32, this.Buyer);
            }
            else
            {
                this.db.AddInParameter(com, "Buyer", DbType.Int32, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.DealUse))
            {
                this.db.AddInParameter(com, "DealUse", DbType.String, this.DealUse);
            }
            else
            {
                this.db.AddInParameter(com, "DealUse", DbType.String, DBNull.Value);
            }
            if (this.SalePrice > 0)
            {
                this.db.AddInParameter(com, "SalePrice", DbType.Decimal, this.SalePrice);
            }
            else
            {
                this.db.AddInParameter(com, "SalePrice", DbType.Double, DBNull.Value);
            }
            if (this.DueDate > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "DueDate", DbType.DateTime, this.DueDate);
            }
            else
            {
                this.db.AddInParameter(com, "DueDate", DbType.DateTime, DBNull.Value);
            }
            if (this.Commission > 0)
            {
                this.db.AddInParameter(com, "Commission", DbType.Decimal, this.Commission);
            }
            else
            {
                this.db.AddInParameter(com, "Commission", DbType.Double, DBNull.Value);
            }
            if (this.SetFee > 0)
            {
                this.db.AddInParameter(com, "SetFee", DbType.Decimal, this.SetFee);
            }
            else
            {
                this.db.AddInParameter(com, "SetFee", DbType.Double, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Notes))
            {
                this.db.AddInParameter(com, "Notes", DbType.String, this.Notes);
            }
            else
            {
                this.db.AddInParameter(com, "Notes", DbType.String, DBNull.Value);
            }
            if (this.Tenant > 0)
            {
                this.db.AddInParameter(com, "Tenant", DbType.Int32, this.Tenant);
            }
            else
            {
                this.db.AddInParameter(com, "Tenant", DbType.Int32, DBNull.Value);
            }
            if (this.LeaseStartDate > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "LeaseStartDate", DbType.DateTime, this.LeaseStartDate);
            }
            else
            {
                this.db.AddInParameter(com, "LeaseStartDate", DbType.DateTime, DBNull.Value);
            }
            if (this.CAM > 0)
            {
                this.db.AddInParameter(com, "CAM", DbType.Decimal, this.CAM);
            }
            else
            {
                this.db.AddInParameter(com, "CAM", DbType.Double, DBNull.Value);
            }
            if (this.INS > 0)
            {
                this.db.AddInParameter(com, "INS", DbType.Decimal, this.INS);
            }
            else
            {
                this.db.AddInParameter(com, "INS", DbType.Double, DBNull.Value);
            }
            if (this.TAX > 0)
            {
                this.db.AddInParameter(com, "TAX", DbType.Decimal, this.TAX);
            }
            else
            {
                this.db.AddInParameter(com, "TAX", DbType.Double, DBNull.Value);
            }
            if (this.NNN > 0)
            {
                this.db.AddInParameter(com, "NNN", DbType.Decimal, this.NNN);
            }
            else
            {
                this.db.AddInParameter(com, "NNN", DbType.Double, DBNull.Value);
            }
            if (this.TI > 0)
            {
                this.db.AddInParameter(com, "TI", DbType.Decimal, this.TI);
            }
            else
            {
                this.db.AddInParameter(com, "TI", DbType.Double, DBNull.Value);
            }
            if (this.FreeRent > 0)
            {
                this.db.AddInParameter(com, "FreeRent", DbType.Decimal, this.FreeRent);
            }
            else
            {
                this.db.AddInParameter(com, "FreeRent", DbType.Double, DBNull.Value);
            }
            if (this.UnitValue > 0)
            {
                this.db.AddInParameter(com, "UnitValue", DbType.Int32, this.UnitValue);
            }
            else
            {
                this.db.AddInParameter(com, "UnitValue", DbType.Int32, DBNull.Value);
            }
            if (this.UnitTypeValue > 0)
            {
                this.db.AddInParameter(com, "UnitTypeValue", DbType.Int32, this.UnitTypeValue);
            }
            else
            {
                this.db.AddInParameter(com, "UnitTypeValue", DbType.Int32, DBNull.Value);
            }
            if (this.Nets > 0)
            {
                this.db.AddInParameter(com, "Nets", DbType.Decimal, this.Nets);
            }
            else
            {
                this.db.AddInParameter(com, "Nets", DbType.Double, DBNull.Value);
            }
            // 19/08/2013--Diptee
            if (this.FreeRentUnitValue > 0)
            {
                this.db.AddInParameter(com, "FreeRentUnitValue", DbType.Int32, this.FreeRentUnitValue);
            }
            else
            {
                this.db.AddInParameter(com, "FreeRentUnitValue", DbType.Int32, DBNull.Value);
            }
            if (this.FreeRentUnitTypeValue > 0)
            {
                this.db.AddInParameter(com, "FreeRentUnitTypeValue", DbType.Int32, this.FreeRentUnitTypeValue);
            }
            else
            {
                this.db.AddInParameter(com, "FreeRentUnitTypeValue", DbType.Int32, DBNull.Value);
            }
            //Added by Shishir 17-05-2016
            if (this.TIAllowanceType > 0)
            {
                this.db.AddInParameter(com, "TIAllowanceType", DbType.Decimal, this.TIAllowanceType);
            }
            else
            {
                this.db.AddInParameter(com, "TIAllowanceType", DbType.Double, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Deletes details of DealTransactions for provided DealTransactionID.
    /// </summary>
    /// <returns>True if Delete operation is successful; Else False.</returns>
    public bool Delete()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_DealTransactionsDelete");
            this.db.AddInParameter(com, "DealTransactionID", DbType.Int32, this.DealTransactionID);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Get list of DealTransactions for provided parameters.
    /// </summary>
    /// <returns>DataSet of result</returns>
    /// <remarks></remarks>
    public DataSet GetList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_usp_DealTransactionsGetList");
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    #endregion


    #region More
    public DataSet CheckCommissiondueDatePayment(Int32 ChargeslipId, Int32 TermTypeid, string DueDate)
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_CheckCommissiondueDatePayment");
            this.db.AddInParameter(com, "@Chargeslipid", DbType.Int32, ChargeslipId);
            this.db.AddInParameter(com, "@TermTypeId", DbType.Int32, TermTypeid);
            this.db.AddInParameter(com, "@Duedate", DbType.String, DueDate);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
            string errormsg = ex.Message;
        }

        return ds;
    }
    public DataSet GetDealCustomerTransactionsList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_DealCustomerTransactions");
            //  this.db.AddInParameter(com, "DealTransactionID", DbType.Int32, this.DealTransactionID);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }

    public DataSet GetDealInfoUsingTransIDList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_DealInfoUsingTransIDList");
            this.db.AddInParameter(com, "DealTransactionID", DbType.Int32, this.DealTransactionID);
            this.db.AddInParameter(com, "CommissionDueDateId", DbType.Int32, this.CommissionDueDateId);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }

    public DataSet GetVendorTransInfoList(int TransactionType, string UserName, int ViewType, DateTime FromDate, DateTime ToDate)
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_TransVendorInfoList");
            this.db.AddInParameter(com, "UserName", DbType.String, UserName);
            this.db.AddInParameter(com, "TransactionType", DbType.String, TransactionType);
            if (ViewType > 0)
            {
                this.db.AddInParameter(com, "ViewType", DbType.Int32, ViewType);
            }
            else
            {
                this.db.AddInParameter(com, "ViewType", DbType.Int32, DBNull.Value);
            }
            if (FromDate > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "FromDate", DbType.DateTime, FromDate);
            }
            else
            {
                this.db.AddInParameter(com, "FromDate", DbType.DateTime, DBNull.Value);
            }
            if (ToDate > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "ToDate", DbType.DateTime, ToDate);
            }
            else
            {
                this.db.AddInParameter(com, "ToDate", DbType.DateTime, DBNull.Value);
            }
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    public DataSet GetTransInfoList(int Userid, int TransactionType, string Company, int ViewType, DateTime FromDate, DateTime ToDate)
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_TransInfoList");
            this.db.AddInParameter(com, "userid", DbType.Int32, Userid);
            this.db.AddInParameter(com, "TransactionType", DbType.Int32, TransactionType);
            this.db.AddInParameter(com, "Company", DbType.String, Company);
            if (ViewType > 0)
            {
                this.db.AddInParameter(com, "ViewType", DbType.Int32, ViewType);
            }
            else
            {
                this.db.AddInParameter(com, "ViewType", DbType.Int32, DBNull.Value);
            }
            if (FromDate > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "FromDate", DbType.DateTime, FromDate);
            }
            else
            {
                this.db.AddInParameter(com, "FromDate", DbType.DateTime, DBNull.Value);
            }
            if (ToDate > DateTime.MinValue)
            {
                this.db.AddInParameter(com, "ToDate", DbType.DateTime, ToDate);
            }
            else
            {
                this.db.AddInParameter(com, "ToDate", DbType.DateTime, DBNull.Value);
            }
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }

    public DataSet GetChargeSlipDetails(Int64 UserId)
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_GetChargeSlipDetails");
            this.db.AddInParameter(com, "ChargeSlipId", DbType.Int64, this.ChargeSlipID);
            this.db.AddInParameter(com, "LoggedInUserId", DbType.Int64, UserId);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }

    //public DataSet GetPaymentHistoryDetails()
    //{
    //    DataSet ds = null;
    //    try
    //    {
    //        DbCommand com = db.GetStoredProcCommand("usp_GetPaymentHistoryDetails");
    //        this.db.AddInParameter(com, "DealTransactionID", DbType.Int32, this.DealTransactionID);
    //        ds = db.ExecuteDataSet(com);
    //    }
    //    catch (Exception ex)
    //    {
    //        //To Do: Handle Exception
    //    }

    //    return ds;
    //}

    public DataSet GetPaymentHistoryDetails()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_PaymentHistoryDetailsGet");
            this.db.AddInParameter(com, "ChargeslipId", DbType.Int32, this.ChargeSlipID);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }

    //Added by Shishir 29-04-2016
    public DataSet GetBrokerReport()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_BrokerReportGetList");
            if (this.BrokerID > 0)
            {
                this.db.AddInParameter(com, "BrokerID", DbType.Int32, this.BrokerID);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerID", DbType.Int32, DBNull.Value);
            }

            //this.db.AddInParameter(com, "BrokerID", DbType.Int32, this.BrokerID);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    public DataSet GetBrokerReportList(Int32 UserId, string BrokerId, Int32 CriteriaId1, string Criteria1From, string Criteria1To, Int32 CriteriaId2, string Criteria2From, string Criteria2To, Int32 CriteriaId3, string Criteria3From, string Criteria3To)
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_GetBrokerReportGetList");
            if (UserId > 0)
            {
                this.db.AddInParameter(com, "UserId", DbType.Int32, UserId);
            }
            else
            {
                this.db.AddInParameter(com, "UserId", DbType.Int32, DBNull.Value);
            }
            if (!string.IsNullOrEmpty(BrokerId))
            {
                this.db.AddInParameter(com, "BrokerId", DbType.String, BrokerId);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerId", DbType.String, DBNull.Value);
            }
            if (CriteriaId1 > 0)
            {
                this.db.AddInParameter(com, "CriteriaId1", DbType.Int32, CriteriaId1);
            }
            else
            {
                this.db.AddInParameter(com, "CriteriaId1", DbType.Int32, DBNull.Value);
            }
            this.db.AddInParameter(com, "Criteria1From", DbType.String, Criteria1From);
            this.db.AddInParameter(com, "Criteria1To", DbType.String, Criteria1To);
            if (CriteriaId2 > 0)
            {
                this.db.AddInParameter(com, "CriteriaId2", DbType.Int32, CriteriaId2);
            }
            else
            {
                this.db.AddInParameter(com, "CriteriaId2", DbType.Int32, DBNull.Value);
            }
            this.db.AddInParameter(com, "Criteria2From", DbType.String, Criteria2From);
            this.db.AddInParameter(com, "Criteria2To", DbType.String, Criteria2To);
            if (CriteriaId3 > 0)
            {
                this.db.AddInParameter(com, "CriteriaId3", DbType.Int32, CriteriaId3);
            }
            else
            {
                this.db.AddInParameter(com, "CriteriaId3", DbType.Int32, DBNull.Value);
            }
            this.db.AddInParameter(com, "Criteria3From", DbType.String, Criteria3From);
            this.db.AddInParameter(com, "Criteria3To", DbType.String, Criteria3To);           
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
            string msg = ex.Message;
        }

        return ds;
    }

    public DataSet GetDealCustomerTransactionsListUsingConditions(int status, string Company, int userid)
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_DealCustomerTransactions");
            this.db.AddInParameter(com, "userid", DbType.Int32, userid);
            this.db.AddInParameter(com, "Status", DbType.Int32, status);
            this.db.AddInParameter(com, "Company", DbType.String, Company);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    public DataSet GetDealVendorTransactions(int status, string UserName)
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_DealVendorTransactions");
            this.db.AddInParameter(com, "Status", DbType.Int32, status);
            this.db.AddInParameter(com, "UserName", DbType.String, UserName);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }
        return ds;
    }

    public bool LoadByChargeSlipId()
    {
        try
        {
            if (this.ChargeSlipID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_DealTransactionsGetDetailsByChargeSlipId");
                this.db.AddInParameter(com, "ChargeSlipID", DbType.Int32, this.ChargeSlipID);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    this.DealTransactionID = Convert.ToInt32(dt.Rows[0]["DealTransactionID"]);
                    this.ChargeSlipID = Convert.ToInt32(dt.Rows[0]["ChargeSlipID"]);
                    this.DealType = Convert.ToInt32(dt.Rows[0]["DealType"]);
                    this.DealSubType = Convert.ToInt32(dt.Rows[0]["DealSubType"]);
                    this.Size = Convert.ToDecimal(dt.Rows[0]["Size"]);
                    this.BldgSize = Convert.ToDecimal(dt.Rows[0]["BldgSize"]);
                    this.Buyer = Convert.ToInt32(dt.Rows[0]["Buyer"]);
                    this.DealUse = Convert.ToString(dt.Rows[0]["DealUse"]);
                    this.SalePrice = Convert.ToDecimal(dt.Rows[0]["SalePrice"]);
                    // this.DueDate = Convert.ToDateTime(dt.Rows[0]["DueDate"]);
                    this.Commission = Convert.ToDecimal(dt.Rows[0]["Commission"]);
                    this.SetFee = Convert.ToDecimal(dt.Rows[0]["SetFee"]);
                    this.Notes = Convert.ToString(dt.Rows[0]["Notes"]);
                    this.Tenant = Convert.ToInt32(dt.Rows[0]["Tenant"]);
                    this.LeaseStartDate = Convert.ToDateTime(dt.Rows[0]["LeaseStartDate"]);
                    this.CAM = Convert.ToDecimal(dt.Rows[0]["CAM"]);
                    this.INS = Convert.ToDecimal(dt.Rows[0]["INS"]);
                    this.TAX = Convert.ToDecimal(dt.Rows[0]["TAX"]);
                    this.NNN = Convert.ToDecimal(dt.Rows[0]["NNN"]);
                    this.TI = Convert.ToDecimal(dt.Rows[0]["TI"]);
                    this.FreeRent = Convert.ToDecimal(dt.Rows[0]["FreeRent"]);
                    this.UnitValue = Convert.ToInt32(dt.Rows[0]["UnitValue"]);
                    this.UnitTypeValue = Convert.ToInt32(dt.Rows[0]["UnitTypeValue"]);
                    this.Nets = Convert.ToDecimal(dt.Rows[0]["Nets"]);
                    this.FreeRentUnitValue = Convert.ToInt32(dt.Rows[0]["FreeRentUnitValue"]);
                    this.FreeRentUnitTypeValue = Convert.ToInt32(dt.Rows[0]["FreeRentUnitTypeValue"]);
                    this.AdditionalNotes = Convert.ToString(dt.Rows[0]["AdditionalNotes"]);
                    this.TIAllowanceType = Convert.ToInt32(dt.Rows[0]["TIAllowanceType"]);//Added by Shishir 17-05-2016
                    return true;
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }
    }




    public void AddNotesInsert()
    {

        DbCommand com = this.db.GetStoredProcCommand("usp_AdditionalNotes");
        this.db.AddInParameter(com, "@ChargeSlipID", DbType.Int32, this.ChargeSlipID);
        this.db.AddInParameter(com, "@AdditionalNotes", DbType.String, this.AdditionalNotes);
        // DbCommand com = this.db.GetSqlStringCommand("UPDATE DealTransactions SET AdditionalNotes='" + this.AdditionalNotes + "' WHERE ChargeSlipID=" + this.ChargeSlipID);
        this.db.ExecuteNonQuery(com);
    }
    public void CommissionTypeUpdate(Int32 Chargeslipid, Int32 CommissionTypeContingentTerm, Int32 CommissionTypeOptionTerm, Int32 CommissionTypeBaseTerm, Int32 CommissionTypeOptionTerm1, Int32 CommissionTypeOptionTerm2, Int32 CommissionTypeOptionTerm3)
    {

        DbCommand com = this.db.GetStoredProcCommand("usp_CommissionTypeUpdate");
        this.db.AddInParameter(com, "@Chargeslipid", DbType.Int32, Chargeslipid);
        this.db.AddInParameter(com, "@CommissionTypeContingentTerm", DbType.Int32, CommissionTypeContingentTerm);
        this.db.AddInParameter(com, "@CommissionTypeOptionTerm", DbType.Int32, CommissionTypeOptionTerm);
        this.db.AddInParameter(com, "@CommissionTypeBaseTerm", DbType.Int32, CommissionTypeBaseTerm);

        this.db.AddInParameter(com, "@CommissionTypeOptionTerm1", DbType.Int32, CommissionTypeOptionTerm1);
        this.db.AddInParameter(com, "@CommissionTypeOptionTerm2", DbType.Int32, CommissionTypeOptionTerm2);
        this.db.AddInParameter(com, "@CommissionTypeOptionTerm3", DbType.Int32, CommissionTypeOptionTerm3);

        // DbCommand com = this.db.GetSqlStringCommand("UPDATE DealTransactions SET AdditionalNotes='" + this.AdditionalNotes + "' WHERE ChargeSlipID=" + this.ChargeSlipID);
        this.db.ExecuteNonQuery(com);
    }
    public DataSet GetCommissionType(Int32 ChargeslipId)
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_GetCommissionType");
            this.db.AddInParameter(com, "@ChargeslipId", DbType.Int32, ChargeslipId);
            //  this.db.AddInParameter(com, "DealTransactionID", DbType.Int32, this.DealTransactionID);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    public DataSet IsChargeslipPayment(Int32 ChargeslipId, Int32 TermTypeid)
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_IsChargeslipPayment");
            this.db.AddInParameter(com, "@ChargeslipId", DbType.Int32, ChargeslipId);
            this.db.AddInParameter(com, "@TermTypeId", DbType.Int32, TermTypeid);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }

    ////Commented By Shishir 29-04-2016
    //public void UpdateCheckNo(int PaymentHistoryId, string CheckNo)
    //{
    //    DbCommand com = this.db.GetSqlStringCommand("UPDATE PaymentHistory Set BrokerCheckNo = '" + CheckNo + "' Where PaymentHistoryId = " + PaymentHistoryId);
    //    this.db.ExecuteNonQuery(com);
    //}

    //Added by Shishir 29-04-2016
    public bool UpdateCheckNo(int PaymentHistoryId, string CheckNo)
    {
        DbCommand com = this.db.GetStoredProcCommand("usp_PaymentHistoryCheckNoUpdate");
        this.db.AddInParameter(com, "@BrokerCheckNo", DbType.Int32, CheckNo);
        this.db.AddInParameter(com, "@PaymentHistoryId", DbType.Int32, PaymentHistoryId);
        this.db.ExecuteNonQuery(com);
        return true;
    }

    //Added by Jaywanti on 31-08-2015 
    public void UpdateAmountAdjustment()
    {
        DbCommand com = this.db.GetStoredProcCommand("usp_UpdateAmountAdjustment");
        this.db.AddInParameter(com, "@ChargeslipId", DbType.Int32, this.ChargeSlipID);
        this.db.ExecuteNonQuery(com);
    }
    #endregion

    #endregion
}
