﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class DealSubTypes
{
    #region Basic Functionality

    #region Variable Declaration

    /// <summary>
    /// Variable to store Database object to interact with database.
    /// </summary>
    private Database db;
    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the DealParties class.
    /// </summary>
    public DealSubTypes()
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
    }

    /// <summary>
    /// Initializes a new instance of the DealParties class.
    /// </summary>
    /// <param name="DealSubTypeID">Sets the value of DealSubTypeID.</param>
    public DealSubTypes(int DealSubTypeID)
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        this.DealSubTypeID = DealSubTypeID;
    }
    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets DealTypeID
    /// </summary>
    public int DealTypeID
    {
        get;
        set;
    }


    /// <summary>
    /// Gets or sets DealSubTypeID
    /// </summary>
    public int DealSubTypeID
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets DealSubType
    /// </summary>
    public string DealSubType
    {
        get;
        set;
    }

    public int CurrentPageNo
    {
        get;
        set;

    }
    public int PageSize
    {
        get;
        set;
    }
    public int TotalRecords
    {
        get;
        set;
    }
    public bool IsActive
    {
        get;
        set;
    }
    #endregion

    #region Actions

    /// <summary>
    /// Loads the details for DealParties.
    /// </summary>
    /// <returns>True if Load operation is successful; Else False.</returns>
    public bool Load()
    {
        try
        {
            if (this.DealSubTypeID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_DealSubTypesGetDetails");
                this.db.AddInParameter(com, "DealSubTypeID", DbType.Int32, this.DealSubTypeID);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    this.DealSubTypeID = Convert.ToInt32(dt.Rows[0]["DealSubTypeID"]);
                    this.DealTypeID = Convert.ToInt32(dt.Rows[0]["DealTypeID"]);
                    this.DealSubType = Convert.ToString(dt.Rows[0]["DealSubType"]);
                    this.IsActive = Convert.ToBoolean(dt.Rows[0]["IsActive"]);
           
                    return true;
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }
    }

    /// <summary>
    /// Inserts details for DealParties if DealSubTypeID = 0.
    /// Else updates details for DealParties.
    /// </summary>
    /// <returns>True if Save operation is successful; Else False.</returns>
    public bool Save()
    {
        if (this.DealSubTypeID == 0)
        {
            return this.Insert();
        }
        else
        {
            if (this.DealSubTypeID > 0)
            {
                return this.Update();
            }
            else
            {
                this.DealSubTypeID = 0;
                return false;
            }
        }
    }

    /// <summary>
    /// Inserts details for DealParties.
    /// Saves newly created Id in DealSubTypeID.
    /// </summary>
    /// <returns>True if Insert operation is successful; Else False.</returns>
    private bool Insert()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_DealSubTypesInsert");
            this.db.AddOutParameter(com, "DealSubTypeID", DbType.Int32, 1024);
            if (!String.IsNullOrEmpty(this.DealSubType))
            {
                this.db.AddInParameter(com, "DealSubType", DbType.String, this.DealSubType);
            }
            else
            {
                this.db.AddInParameter(com, "DealSubType", DbType.String, DBNull.Value);
            }
            if (this.DealTypeID > 0)
            {
                this.db.AddInParameter(com, "DealTypeID", DbType.String, this.DealTypeID);
            }
            else
            {
                this.db.AddInParameter(com, "DealTypeID", DbType.String, DBNull.Value);
            }
            this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
            this.db.ExecuteNonQuery(com);
            this.DealSubTypeID = Convert.ToInt32(this.db.GetParameterValue(com, "DealSubTypeID"));      // Read in the output parameter value
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return this.DealSubTypeID > 0; // Return whether ID was returned
    }

    /// <summary>
    /// Updates details for DealParties.
    /// </summary>
    /// <returns>True if Update operation is successful; Else False.</returns>
    private bool Update()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_DealSubTypesUpdate");
            this.db.AddInParameter(com, "DealSubTypeID", DbType.Int32, this.DealSubTypeID);
            if (!String.IsNullOrEmpty(this.DealSubType))
            {
                this.db.AddInParameter(com, "DealSubType", DbType.String, this.DealSubType);
            }
            else
            {
                this.db.AddInParameter(com, "DealSubType", DbType.String, DBNull.Value);
            }
            if (this.DealTypeID > 0)
            {
                this.db.AddInParameter(com, "DealTypeID", DbType.String, this.DealTypeID);
            }
            else
            {
                this.db.AddInParameter(com, "DealTypeID", DbType.String, DBNull.Value);
            }
            this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Deletes details of DealParties for provided DealSubTypeID.
    /// </summary>
    /// <returns>True if Delete operation is successful; Else False.</returns>
    public bool Delete()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_DealSubTypesDelete");
            this.db.AddInParameter(com, "DealSubTypeID", DbType.Int32, this.DealSubTypeID);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Get list of DealParties for provided parameters.
    /// </summary>
    /// <returns>DataSet of result</returns>
    /// <remarks></remarks>
    public DataSet GetList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_DealSubTypesGetList");
            if (this.DealTypeID > 0)
            {
                this.db.AddInParameter(com, "DealTypeID", DbType.String, this.DealTypeID);
            }
            else
            {
                this.db.AddInParameter(com, "DealTypeID", DbType.String, DBNull.Value);
            }
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }

    #endregion
    #region [Custom Function]
    public DataSet GetAllSubType()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_DealSubTypeGetAllList");            
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    #endregion
    #region [CUSTOM ACTION]
    public DataTable LoadDealTransaction()
    {
        try
        {
            DataTable dt = new DataTable();
            if (this.DealSubTypeID != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_DealSubTypeExist");
                this.db.AddInParameter(com, "DealSubTypeId", DbType.Int32, this.DealSubTypeID);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                    return dt;
                }
            }
            return dt;


        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return null;
        }
    }
    #endregion
    #endregion
}
