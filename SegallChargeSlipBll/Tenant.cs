// -----------------------------------------------------------------------
// <copyright file="Tenant.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing Tenant
/// </summary>
/// 
namespace SegallChargeSlipBll
{
    [Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
    public class Tenant
    {
        #region Basic Functionality

        #region Variable Declaration

        /// <summary>
        /// Variable to store Database object to interact with database.
        /// </summary>
        private Database db;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the Tenant class.
        /// </summary>
        public Tenant()
        {
            this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        }

        /// <summary>
        /// Initializes a new instance of the Tenant class.
        /// </summary>
        /// <param name="tenantID">Sets the value of TenantID.</param>
        public Tenant(int tenantID)
        {
            this.db = DatabaseFactory.CreateDatabase();
            this.TenantID = tenantID;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets TenantID
        /// </summary>
        public int TenantID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets TenantName
        /// </summary>
        public string TenantName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Address1
        /// </summary>
        public string Address1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Address2
        /// </summary>
        public string Address2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets City
        /// </summary>
        public string City
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets State
        /// </summary>
        public int State
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ZipCode
        /// </summary>
        public int ZipCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedBy
        /// </summary>
        public int CreatedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedOn
        /// </summary>
        public DateTime CreatedOn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedBy
        /// </summary>
        public int ModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedOn
        /// </summary>
        public DateTime ModifiedOn
        {
            get;
            set;
        }
        #endregion

        #region Actions

        /// <summary>
        /// Loads the details for Tenant.
        /// </summary>
        /// <returns>True if Load operation is successful; Else False.</returns>
        public bool Load()
        {
            try
            {
                if (this.TenantID != 0)
                {
                    DbCommand com = this.db.GetStoredProcCommand("usp_TenantGetDetails");
                    this.db.AddInParameter(com, "TenantID", DbType.Int32, this.TenantID);
                    DataSet ds = this.db.ExecuteDataSet(com);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        this.TenantID = Convert.ToInt32(dt.Rows[0]["TenantID"]);
                        this.TenantName = Convert.ToString(dt.Rows[0]["TenantName"]);
                        this.Address1 = Convert.ToString(dt.Rows[0]["Address1"]);
                        this.Address2 = Convert.ToString(dt.Rows[0]["Address2"]);
                        this.City = Convert.ToString(dt.Rows[0]["City"]);
                        this.State = Convert.ToInt32(dt.Rows[0]["State"]);
                        this.ZipCode = Convert.ToInt32(dt.Rows[0]["ZipCode"]);
                        this.CreatedBy = Convert.ToInt32(dt.Rows[0]["CreatedBy"]);
                        this.CreatedOn = Convert.ToDateTime(dt.Rows[0]["CreatedOn"]);
                        this.ModifiedBy = Convert.ToInt32(dt.Rows[0]["ModifiedBy"]);
                        this.ModifiedOn = Convert.ToDateTime(dt.Rows[0]["ModifiedOn"]);
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }
        }

        /// <summary>
        /// Inserts details for Tenant if TenantID = 0.
        /// Else updates details for Tenant.
        /// </summary>
        /// <returns>True if Save operation is successful; Else False.</returns>
        public bool Save()
        {
            if (this.TenantID == 0)
            {
                return this.Insert();
            }
            else
            {
                if (this.TenantID > 0)
                {
                    return this.Update();
                }
                else
                {
                    this.TenantID = 0;
                    return false;
                }
            }
        }

        /// <summary>
        /// Inserts details for Tenant.
        /// Saves newly created Id in TenantID.
        /// </summary>
        /// <returns>True if Insert operation is successful; Else False.</returns>
        private bool Insert()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_TenantInsert");
                this.db.AddOutParameter(com, "TenantID", DbType.Int32, 1024);
                if (!String.IsNullOrEmpty(this.TenantName))
                {
                    this.db.AddInParameter(com, "TenantName", DbType.String, this.TenantName);
                }
                else
                {
                    this.db.AddInParameter(com, "TenantName", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address1))
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
                }
                else
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address2))
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
                }
                else
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.City))
                {
                    this.db.AddInParameter(com, "City", DbType.String, this.City);
                }
                else
                {
                    this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
                }
                if (this.State > 0)
                {
                    this.db.AddInParameter(com, "State", DbType.Int32, this.State);
                }
                else
                {
                    this.db.AddInParameter(com, "State", DbType.Int32, DBNull.Value);
                }
                if (this.ZipCode > 0)
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.Int32, this.ZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedBy > 0)
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
                }
                if (this.ModifiedBy > 0)
                {
                    this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, this.ModifiedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, DBNull.Value);
                }
                if (this.ModifiedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "ModifiedOn", DbType.DateTime, this.ModifiedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "ModifiedOn", DbType.DateTime, DBNull.Value);
                }
                this.db.ExecuteNonQuery(com);
                this.TenantID = Convert.ToInt32(this.db.GetParameterValue(com, "TenantID"));      // Read in the output parameter value
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return this.TenantID > 0; // Return whether ID was returned
        }

        /// <summary>
        /// Updates details for Tenant.
        /// </summary>
        /// <returns>True if Update operation is successful; Else False.</returns>
        private bool Update()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_TenantUpdate");
                this.db.AddInParameter(com, "TenantID", DbType.Int32, this.TenantID);
                if (!String.IsNullOrEmpty(this.TenantName))
                {
                    this.db.AddInParameter(com, "TenantName", DbType.String, this.TenantName);
                }
                else
                {
                    this.db.AddInParameter(com, "TenantName", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address1))
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
                }
                else
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address2))
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
                }
                else
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.City))
                {
                    this.db.AddInParameter(com, "City", DbType.String, this.City);
                }
                else
                {
                    this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
                }
                if (this.State > 0)
                {
                    this.db.AddInParameter(com, "State", DbType.Int32, this.State);
                }
                else
                {
                    this.db.AddInParameter(com, "State", DbType.Int32, DBNull.Value);
                }
                if (this.ZipCode > 0)
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.Int32, this.ZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedBy > 0)
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
                }
                if (this.ModifiedBy > 0)
                {
                    this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, this.ModifiedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, DBNull.Value);
                }
                if (this.ModifiedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "ModifiedOn", DbType.DateTime, this.ModifiedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "ModifiedOn", DbType.DateTime, DBNull.Value);
                }
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes details of Tenant for provided TenantID.
        /// </summary>
        /// <returns>True if Delete operation is successful; Else False.</returns>
        public bool Delete()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_TenantDelete");
                this.db.AddInParameter(com, "TenantID", DbType.Int32, this.TenantID);
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get list of Tenant for provided parameters.
        /// </summary>
        /// <returns>DataSet of result</returns>
        /// <remarks></remarks>
        public DataSet GetList()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_TenantGetList");
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }


        // Custom Action For Serach List

        public DataSet GetTenantSeachList()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_TenantSearchByName");
                this.db.AddInParameter(com, "Tenant", DbType.String, this.TenantName);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }
        #endregion

        #endregion
    }
}
