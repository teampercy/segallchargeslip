﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Microsoft.Reporting.WebForms;
using System.IO;

    public class Report
    {
        private Database db;
        public int ChargeSlipId
        {
            get;
            set;
        }
        public int CreatedBy
        {
            get;
            set;
        }
        public int UserId
        {
            get;
            set;
        }
        public int PaymentId
        {
            get;
            set;
        }
        public Report()
        {
            this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        }
        public DataTable GetCobrokerList()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("_Shishir_rpt_ChargeSlipCoBrokers");
                this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
                this.db.AddInParameter(com, "BrokerID", DbType.Int32, this.UserId);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds.Tables[0];

        }
        public DataSet GetPaymentCommissionDetail()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_GetPaymentCommissionDetail");
                this.db.AddInParameter(com, "ChargeSlipId", DbType.Int32, this.ChargeSlipId);
                this.db.AddInParameter(com, "Userid", DbType.Int32, this.UserId);
                this.db.AddInParameter(com, "PaymentId", DbType.Int32, this.PaymentId);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;

        }
       
        public DataTable GetUserAndAdminEmail()
        {

            try
            {
                DataTable dt = new DataTable();
                if (this.CreatedBy != 0)
                {

                    DbCommand com = this.db.GetStoredProcCommand("usp_UsersGetAdminAccountEmail");
                    this.db.AddInParameter(com, "@UserID", DbType.Int32, this.CreatedBy);
                    DataSet ds = this.db.ExecuteDataSet(com);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        dt = ds.Tables[0];
                        return dt;
                    }
                }


            }
            catch (Exception ex)
            {
                // To Do: Handle Exception

            }
            return null;
        }

        public void SetMessageEmailSent()
        {
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_SetMessageEmailSent");
                this.db.AddInParameter(com, "PaymentId", DbType.Int32, this.PaymentId);
                db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

        }
        
    }



