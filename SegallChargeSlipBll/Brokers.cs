// -----------------------------------------------------------------------
// <copyright file="Brokers.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing Brokers
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class Brokers
{
    #region Basic Functionality

    #region Variable Declaration

    /// <summary>
    /// Variable to store Database object to interact with database.
    /// </summary>
    private Database db;
    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the Brokers class.
    /// </summary>
    public Brokers()
    {
        this.db = DatabaseFactory.CreateDatabase("ConnectionString");
    }

    /// <summary>
    /// Initializes a new instance of the Brokers class.
    /// </summary>
    /// <param name="brokerId">Sets the value of BrokerId.</param>
    public Brokers(int brokerId)
    {
        this.db = DatabaseFactory.CreateDatabase();
        this.BrokerId = brokerId;
    }
    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets BrokerId
    /// </summary>
    public int BrokerId
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets BrokerTypeId
    /// </summary>
    public int BrokerTypeId
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets CompanyName
    /// </summary>
    public string CompanyName
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Address
    /// </summary>
    public string Address
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets City
    /// </summary>
    public string City
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets State
    /// </summary>
    public string State
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Zipcode
    /// </summary>
    public string Zipcode
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Name
    /// </summary>
    public string Name
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Name
    /// </summary>
    public string LastName
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets Email
    /// </summary>
    public string Email
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets EIN
    /// </summary>
    public string EIN
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets SharePercent
    /// </summary>
    public double SharePercent
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets SetAmount
    /// </summary>
    public decimal SetAmount
    {
        get;
        set;
    }

    public string Country
    { get; set; }


    public string UserName
    { get; set; }

    public decimal PercentBroker
    {
        get;
        set;
    }
    #endregion

    #region Actions

    /// <summary>
    /// Loads the details for Brokers.
    /// </summary>
    /// <returns>True if Load operation is successful; Else False.</returns>
    public bool Load()
    {
        try
        {
            if (this.BrokerId != 0)
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_BrokersGetDetails");
                this.db.AddInParameter(com, "BrokerId", DbType.Int32, this.BrokerId);
                DataSet ds = this.db.ExecuteDataSet(com);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    this.BrokerId = Convert.ToInt32(dt.Rows[0]["BrokerId"]);
                    this.BrokerTypeId = Convert.ToInt32(dt.Rows[0]["BrokerTypeId"]);
                    this.CompanyName = Convert.ToString(dt.Rows[0]["CompanyName"]);
                    this.Address = Convert.ToString(dt.Rows[0]["Address"]);
                    this.City = Convert.ToString(dt.Rows[0]["City"]);
                    this.State = Convert.ToString(dt.Rows[0]["State"]);
                    this.Zipcode = Convert.ToString(dt.Rows[0]["Zipcode"]);
                    this.Name = Convert.ToString(dt.Rows[0]["Name"]);
                    this.Email = Convert.ToString(dt.Rows[0]["Email"]);
                    this.EIN = Convert.ToString(dt.Rows[0]["EIN"]);
                    this.SharePercent = Convert.ToDouble(dt.Rows[0]["SharePercent"]);
                    this.SetAmount = Convert.ToDecimal(dt.Rows[0]["SetAmount"]);
                    this.LastName = Convert.ToString(dt.Rows[0]["LastName"]);
                    return true;
                }
            }

            return false;
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }
    }

    /// <summary>
    /// Inserts details for Brokers if BrokerId = 0.
    /// Else updates details for Brokers.
    /// </summary>
    /// <returns>True if Save operation is successful; Else False.</returns>
    public bool Save()
    {
        if (this.BrokerId == 0)
        {
            return this.Insert();
        }
        else
        {
            if (this.BrokerId > 0)
            {
                return this.Update();
            }
            else
            {
                this.BrokerId = 0;
                return false;
            }
        }
    }

    /// <summary>
    /// Inserts details for Brokers.
    /// Saves newly created Id in BrokerId.
    /// </summary>
    /// <returns>True if Insert operation is successful; Else False.</returns>
    private bool Insert()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_BrokersInsert");
            this.db.AddOutParameter(com, "BrokerId", DbType.Int32, 1024);
            if (this.BrokerTypeId > 0)
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, this.BrokerTypeId);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.CompanyName))
            {
                this.db.AddInParameter(com, "CompanyName", DbType.String, this.CompanyName);
            }
            else
            {
                this.db.AddInParameter(com, "CompanyName", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Address))
            {
                this.db.AddInParameter(com, "Address", DbType.String, this.Address);
            }
            else
            {
                this.db.AddInParameter(com, "Address", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.City))
            {
                this.db.AddInParameter(com, "City", DbType.String, this.City);
            }
            else
            {
                this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.State))
            {
                this.db.AddInParameter(com, "State", DbType.String, this.State);
            }
            else
            {
                this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Zipcode))
            {
                this.db.AddInParameter(com, "Zipcode", DbType.String, this.Zipcode);
            }
            else
            {
                this.db.AddInParameter(com, "Zipcode", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Name))
            {
                this.db.AddInParameter(com, "Name", DbType.String, this.Name);
            }
            else
            {
                this.db.AddInParameter(com, "Name", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Email))
            {
                this.db.AddInParameter(com, "Email", DbType.String, this.Email);
            }
            else
            {
                this.db.AddInParameter(com, "Email", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.EIN))
            {
                this.db.AddInParameter(com, "EIN", DbType.String, this.EIN);
            }
            else
            {
                this.db.AddInParameter(com, "EIN", DbType.String, DBNull.Value);
            }
            if (this.SharePercent > 0)
            {
                this.db.AddInParameter(com, "SharePercent", DbType.Double, this.SharePercent);
            }
            else
            {
                this.db.AddInParameter(com, "SharePercent", DbType.Double, DBNull.Value);
            }
            if (this.SetAmount > 0)
            {
                this.db.AddInParameter(com, "SetAmount", DbType.Decimal, this.SetAmount);
            }
            else
            {
                this.db.AddInParameter(com, "SetAmount", DbType.Double, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
            this.BrokerId = Convert.ToInt32(this.db.GetParameterValue(com, "BrokerId"));      // Read in the output parameter value
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return this.BrokerId > 0; // Return whether ID was returned
    }

    /// <summary>
    /// Updates details for Brokers.
    /// </summary>
    /// <returns>True if Update operation is successful; Else False.</returns>
    private bool Update()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_BrokersUpdate");
            this.db.AddInParameter(com, "BrokerId", DbType.Int32, this.BrokerId);
            if (this.BrokerTypeId > 0)
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, this.BrokerTypeId);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.CompanyName))
            {
                this.db.AddInParameter(com, "CompanyName", DbType.String, this.CompanyName);
            }
            else
            {
                this.db.AddInParameter(com, "CompanyName", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Address))
            {
                this.db.AddInParameter(com, "Address", DbType.String, this.Address);
            }
            else
            {
                this.db.AddInParameter(com, "Address", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.City))
            {
                this.db.AddInParameter(com, "City", DbType.String, this.City);
            }
            else
            {
                this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.State))
            {
                this.db.AddInParameter(com, "State", DbType.String, this.State);
            }
            else
            {
                this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Zipcode))
            {
                this.db.AddInParameter(com, "Zipcode", DbType.String, this.Zipcode);
            }
            else
            {
                this.db.AddInParameter(com, "Zipcode", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Name))
            {
                this.db.AddInParameter(com, "Name", DbType.String, this.Name);
            }
            else
            {
                this.db.AddInParameter(com, "Name", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Email))
            {
                this.db.AddInParameter(com, "Email", DbType.String, this.Email);
            }
            else
            {
                this.db.AddInParameter(com, "Email", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.EIN))
            {
                this.db.AddInParameter(com, "EIN", DbType.String, this.EIN);
            }
            else
            {
                this.db.AddInParameter(com, "EIN", DbType.String, DBNull.Value);
            }
            if (this.SharePercent > 0)
            {
                this.db.AddInParameter(com, "SharePercent", DbType.Double, this.SharePercent);
            }
            else
            {
                this.db.AddInParameter(com, "SharePercent", DbType.Double, DBNull.Value);
            }
            if (this.SetAmount > 0)
            {
                this.db.AddInParameter(com, "SetAmount", DbType.Decimal, this.SetAmount);
            }
            else
            {
                this.db.AddInParameter(com, "SetAmount", DbType.Double, DBNull.Value);
            }
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Deletes details of Brokers for provided BrokerId.
    /// </summary>
    /// <returns>True if Delete operation is successful; Else False.</returns>
    public bool Delete()
    {
        try
        {
            DbCommand com = this.db.GetStoredProcCommand("usp_BrokersDelete");
            this.db.AddInParameter(com, "BrokerId", DbType.Int32, this.BrokerId);
            this.db.ExecuteNonQuery(com);
        }
        catch (Exception ex)
        {
            // To Do: Handle Exception
            return false;
        }

        return true;
    }

    /// <summary>
    /// Get list of Brokers for provided parameters.
    /// </summary>
    /// <returns>DataSet of result</returns>
    /// <remarks></remarks>
    public DataSet GetList()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_BrokersGetList");
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }
    #endregion

    #region CustomActions
    // Custom Action For Serach List

    public DataSet GetBrokerCompSeachList(string Type, int BrokerType)
    {
        DataSet ds = null;
        DataTable dt;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_BrokerSearchByCompanyName");
            this.db.AddInParameter(com, "Company", DbType.String, this.CompanyName);
            this.db.AddInParameter(com, "Type", DbType.String, Type);
            this.db.AddInParameter(com, "BrokerType", DbType.String, BrokerType);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }

    public DataSet GetBrokerDetailsAsPerCompanyName()
    {
        DataSet ds = null;
        DataTable dt;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_BrokerDetailByCompanyName");
            this.db.AddInParameter(com, "Company", DbType.String, this.CompanyName);
            this.db.AddInParameter(com, "BrokerTypeId", DbType.String, this.BrokerTypeId);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }

    public DataSet CheckForDuplicateBrokerEntry()
    {
        DataSet ds = null;
        DataTable dt = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_CheckForBrokerDuplication");

            if (this.BrokerId > 0)
            {
                this.db.AddInParameter(com, "UserID", DbType.String, this.BrokerId);
            }
            else
            {
                this.db.AddInParameter(com, "UserID", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.CompanyName))
            {
                this.db.AddInParameter(com, "CompanyName", DbType.String, this.CompanyName);
            }
            else
            {
                this.db.AddInParameter(com, "CompanyName", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Name))
            {
                this.db.AddInParameter(com, "UserName", DbType.String, this.Name);
            }
            else
            {
                this.db.AddInParameter(com, "UserName", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Address))
            {
                this.db.AddInParameter(com, "Address1", DbType.String, this.Address);
            }
            else
            {
                this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.City))
            {
                this.db.AddInParameter(com, "City", DbType.String, this.City);
            }
            else
            {
                this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.State))
            {
                this.db.AddInParameter(com, "State", DbType.String, this.State);
            }
            else
            {
                this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Country))
            {
                this.db.AddInParameter(com, "Country", DbType.String, this.Country);
            }
            else
            {
                this.db.AddInParameter(com, "Country", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Zipcode))
            {
                this.db.AddInParameter(com, "Zip", DbType.String, this.Zipcode);
            }
            else
            {
                this.db.AddInParameter(com, "Zip", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.Email))
            {
                this.db.AddInParameter(com, "Email", DbType.String, this.Email);
            }
            else
            {
                this.db.AddInParameter(com, "Email", DbType.String, DBNull.Value);
            }
            if (!String.IsNullOrEmpty(this.EIN))
            {
                this.db.AddInParameter(com, "EIN", DbType.String, this.EIN);
            }
            else
            {
                this.db.AddInParameter(com, "EIN", DbType.String, DBNull.Value);
            }
            if (this.SharePercent > 0)
            {
                this.db.AddInParameter(com, "SharePercent", DbType.Double, this.SharePercent);
            }
            else
            {
                this.db.AddInParameter(com, "SharePercent", DbType.Double, DBNull.Value);
            }
            if (this.SetAmount > 0)
            {
                this.db.AddInParameter(com, "SetAmount", DbType.Decimal, this.SetAmount);
            }
            else
            {
                this.db.AddInParameter(com, "SetAmount", DbType.Double, DBNull.Value);
            }
            if (this.BrokerTypeId > 0)
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, this.BrokerTypeId);
            }
            else
            {
                this.db.AddInParameter(com, "BrokerTypeId", DbType.Int32, DBNull.Value);
            }
            ds = db.ExecuteDataSet(com);

            //if (ds.Tables.Count > 0)
            //{
            //    dt = ds.Tables[0];
            //}
        }
        catch (Exception ex)
        {
            //To Do: Handle Exception
        }

        return ds;
    }

    public DataSet GetCoborkerslist()
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_getCoborkerslist");
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            ds = null;
        }
        return ds;

    }


    public DataSet usp_getCoborkerslistComp(Int64 userid)
    {
        DataSet ds = null;
        try
        {
            DbCommand com = db.GetStoredProcCommand("usp_getCoborkerslistComp");
            db.AddInParameter(com, "userid",DbType.Int64,userid);
            ds = db.ExecuteDataSet(com);
        }
        catch (Exception ex)
        {
            ds = null;
        }
        return ds;

    }


    #endregion

    #endregion
}
