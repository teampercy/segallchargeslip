// -----------------------------------------------------------------------
// <copyright file="Buyer.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing Buyer
/// </summary>
namespace SegallChargeSlipBll
{
    [Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
    public class Buyer
    {
        #region Basic Functionality

        #region Variable Declaration

        /// <summary>
        /// Variable to store Database object to interact with database.
        /// </summary>
        private Database db;
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the Buyer class.
        /// </summary>
        public Buyer()
        {
            this.db = DatabaseFactory.CreateDatabase("ConnectionString");
        }

        /// <summary>
        /// Initializes a new instance of the Buyer class.
        /// </summary>
        /// <param name="buyerID">Sets the value of BuyerID.</param>
        public Buyer(int buyerID)
        {
            this.db = DatabaseFactory.CreateDatabase();
            this.BuyerID = buyerID;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets BuyerID
        /// </summary>
        public int BuyerID
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets BuyerName
        /// </summary>
        public string BuyerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Address1
        /// </summary>
        public string Address1
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Address2
        /// </summary>
        public string Address2
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets City
        /// </summary>
        public string City
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets State
        /// </summary>
        public int State
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ZipCode
        /// </summary>
        public int ZipCode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedBy
        /// </summary>
        public int CreatedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets CreatedOn
        /// </summary>
        public DateTime CreatedOn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedBy
        /// </summary>
        public int ModifiedBy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets ModifiedOn
        /// </summary>
        public DateTime ModifiedOn
        {
            get;
            set;
        }
        #endregion

        #region Actions

        /// <summary>
        /// Loads the details for Buyer.
        /// </summary>
        /// <returns>True if Load operation is successful; Else False.</returns>
        public bool Load()
        {
            try
            {
                if (this.BuyerID != 0)
                {
                    DbCommand com = this.db.GetStoredProcCommand("usp_BuyerGetDetails");
                    this.db.AddInParameter(com, "BuyerID", DbType.Int32, this.BuyerID);
                    DataSet ds = this.db.ExecuteDataSet(com);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        this.BuyerID = Convert.ToInt32(dt.Rows[0]["BuyerID"]);
                        this.BuyerName = Convert.ToString(dt.Rows[0]["BuyerName"]);
                        this.Address1 = Convert.ToString(dt.Rows[0]["Address1"]);
                        this.Address2 = Convert.ToString(dt.Rows[0]["Address2"]);
                        this.City = Convert.ToString(dt.Rows[0]["City"]);
                        this.State = Convert.ToInt32(dt.Rows[0]["State"]);
                        this.ZipCode = Convert.ToInt32(dt.Rows[0]["ZipCode"]);
                        this.CreatedBy = Convert.ToInt32(dt.Rows[0]["CreatedBy"]);
                        this.CreatedOn = Convert.ToDateTime(dt.Rows[0]["CreatedOn"]);
                        this.ModifiedBy = Convert.ToInt32(dt.Rows[0]["ModifiedBy"]);
                        this.ModifiedOn = Convert.ToDateTime(dt.Rows[0]["ModifiedOn"]);
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }
        }

        /// <summary>
        /// Inserts details for Buyer if BuyerID = 0.
        /// Else updates details for Buyer.
        /// </summary>
        /// <returns>True if Save operation is successful; Else False.</returns>
        public bool Save()
        {
            if (this.BuyerID == 0)
            {
                return this.Insert();
            }
            else
            {
                if (this.BuyerID > 0)
                {
                    return this.Update();
                }
                else
                {
                    this.BuyerID = 0;
                    return false;
                }
            }
        }

        /// <summary>
        /// Inserts details for Buyer.
        /// Saves newly created Id in BuyerID.
        /// </summary>
        /// <returns>True if Insert operation is successful; Else False.</returns>
        private bool Insert()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_BuyerInsert");
                this.db.AddOutParameter(com, "BuyerID", DbType.Int32, 1024);
                if (!String.IsNullOrEmpty(this.BuyerName))
                {
                    this.db.AddInParameter(com, "BuyerName", DbType.String, this.BuyerName);
                }
                else
                {
                    this.db.AddInParameter(com, "BuyerName", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address1))
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
                }
                else
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address2))
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
                }
                else
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.City))
                {
                    this.db.AddInParameter(com, "City", DbType.String, this.City);
                }
                else
                {
                    this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
                }
                if (this.State > 0)
                {
                    this.db.AddInParameter(com, "State", DbType.Int32, this.State);
                }
                else
                {
                    this.db.AddInParameter(com, "State", DbType.Int32, DBNull.Value);
                }
                if (this.ZipCode > 0)
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.Int32, this.ZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedBy > 0)
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
                }
                if (this.ModifiedBy > 0)
                {
                    this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, this.ModifiedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, DBNull.Value);
                }
                if (this.ModifiedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "ModifiedOn", DbType.DateTime, this.ModifiedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "ModifiedOn", DbType.DateTime, DBNull.Value);
                }
                this.db.ExecuteNonQuery(com);
                this.BuyerID = Convert.ToInt32(this.db.GetParameterValue(com, "BuyerID"));      // Read in the output parameter value
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return this.BuyerID > 0; // Return whether ID was returned
        }

        /// <summary>
        /// Updates details for Buyer.
        /// </summary>
        /// <returns>True if Update operation is successful; Else False.</returns>
        private bool Update()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_BuyerUpdate");
                this.db.AddInParameter(com, "BuyerID", DbType.Int32, this.BuyerID);
                if (!String.IsNullOrEmpty(this.BuyerName))
                {
                    this.db.AddInParameter(com, "BuyerName", DbType.String, this.BuyerName);
                }
                else
                {
                    this.db.AddInParameter(com, "BuyerName", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address1))
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
                }
                else
                {
                    this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.Address2))
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
                }
                else
                {
                    this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.City))
                {
                    this.db.AddInParameter(com, "City", DbType.String, this.City);
                }
                else
                {
                    this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
                }
                if (this.State > 0)
                {
                    this.db.AddInParameter(com, "State", DbType.Int32, this.State);
                }
                else
                {
                    this.db.AddInParameter(com, "State", DbType.Int32, DBNull.Value);
                }
                if (this.ZipCode > 0)
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.Int32, this.ZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedBy > 0)
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
                }
                if (this.CreatedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
                }
                if (this.ModifiedBy > 0)
                {
                    this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, this.ModifiedBy);
                }
                else
                {
                    this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, DBNull.Value);
                }
                if (this.ModifiedOn > DateTime.MinValue)
                {
                    this.db.AddInParameter(com, "ModifiedOn", DbType.DateTime, this.ModifiedOn);
                }
                else
                {
                    this.db.AddInParameter(com, "ModifiedOn", DbType.DateTime, DBNull.Value);
                }
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deletes details of Buyer for provided BuyerID.
        /// </summary>
        /// <returns>True if Delete operation is successful; Else False.</returns>
        public bool Delete()
        {
            try
            {
                DbCommand com = this.db.GetStoredProcCommand("usp_BuyerDelete");
                this.db.AddInParameter(com, "BuyerID", DbType.Int32, this.BuyerID);
                this.db.ExecuteNonQuery(com);
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get list of Buyer for provided parameters.
        /// </summary>
        /// <returns>DataSet of result</returns>
        /// <remarks></remarks>
        public DataSet GetList()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_BuyerGetList");
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }


        // Custom Action For Serach List

        public DataSet GetBuyerList()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_BuyerSearchByName");
                this.db.AddInParameter(com, "Buyer", DbType.String, this.BuyerName);
                ds = db.ExecuteDataSet(com);
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }
        #endregion

        #endregion
    }
}
