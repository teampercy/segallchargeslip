// -----------------------------------------------------------------------
// <copyright file="DealParties.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Reflection;
/// <summary>
/// Business class representing DealParties
/// </summary>
[Obfuscation(Feature = "Apply to member * when method or constructor: virtualization", Exclude = false)]
public class DealParties
{
#region Basic Functionality

#region Variable Declaration

   /// <summary>
   /// Variable to store Database object to interact with database.
   /// </summary>
   private Database db;
#endregion

#region Constructors

   /// <summary>
   /// Initializes a new instance of the DealParties class.
   /// </summary>
   public DealParties()
   {
       this.db = DatabaseFactory.CreateDatabase("ConnectionString");
   }

   /// <summary>
   /// Initializes a new instance of the DealParties class.
   /// </summary>
   /// <param name="partyID">Sets the value of PartyID.</param>
   public DealParties(int partyID)
   {
      this.db = DatabaseFactory.CreateDatabase();
      this.PartyID = partyID;
   }
#endregion

#region Properties

   /// <summary>
   /// Gets or sets PartyID
   /// </summary>
   public int PartyID
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets PartyCompanyName
   /// </summary>
   public string PartyCompanyName
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets PartyName
   /// </summary>
   public string PartyName
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets PartyTypeId
   /// </summary>
   public int PartyTypeId
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets Address1
   /// </summary>
   public string Email
   {
       get;
       set;
   }
   /// <summary>
   /// Gets or sets Address1
   /// </summary>
   public string Address1
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets Address2
   /// </summary>
   public string Address2
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets City
   /// </summary>
   public string City
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets State
   /// </summary>
   public string State
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets ZipCode
   /// </summary>
   public string ZipCode
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets CreatedBy
   /// </summary>
   public int CreatedBy
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets CreatedOn
   /// </summary>
   public DateTime CreatedOn
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets ModifiedBy
   /// </summary>
   public int ModifiedBy
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets ModifiedOn
   /// </summary>
   public DateTime ModifiedOn
   {
      get; set;
   }
   public int CurrentPageNo
   {
       get;
       set;
   }
   public int PageSize
   {
       get;
       set;
   }
   public int TotalRecords
   {
       get;
       set;
   }
   public Boolean IsActive
   {
       get;
       set;
   }

#endregion

#region Actions

   /// <summary>
   /// Loads the details for DealParties.
   /// </summary>
   /// <returns>True if Load operation is successful; Else False.</returns>
   public bool Load()
   {
      try
      {
         if (this.PartyID != 0)
         {
            DbCommand com = this.db.GetStoredProcCommand("usp_DealPartiesGetDetails");
            this.db.AddInParameter(com, "PartyID", DbType.Int32, this.PartyID);
            DataSet ds = this.db.ExecuteDataSet(com);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
               DataTable dt = ds.Tables[0];
               this.PartyID = Convert.ToInt32(dt.Rows[0]["PartyID"]);
               this.PartyCompanyName = Convert.ToString(dt.Rows[0]["PartyCompanyName"]);
               this.PartyName = Convert.ToString(dt.Rows[0]["PartyName"]);
               this.PartyTypeId = Convert.ToInt32(dt.Rows[0]["PartyTypeId"]);
               this.Address1 = Convert.ToString(dt.Rows[0]["Address1"]);
               this.Address2 = Convert.ToString(dt.Rows[0]["Address2"]);
               this.City = Convert.ToString(dt.Rows[0]["City"]);
               this.State = Convert.ToString(dt.Rows[0]["State"]);
               this.ZipCode = Convert.ToString(dt.Rows[0]["ZipCode"]);
               this.CreatedBy = Convert.ToInt32(dt.Rows[0]["CreatedBy"]);
               this.CreatedOn = Convert.ToDateTime(dt.Rows[0]["CreatedOn"]);
               this.ModifiedBy = Convert.ToInt32(dt.Rows[0]["ModifiedBy"]);
               this.ModifiedOn = Convert.ToDateTime(dt.Rows[0]["ModifiedOn"]);
               this.Email = Convert.ToString(dt.Rows[0]["Email"]);
               this.IsActive = Convert.ToBoolean(dt.Rows[0]["IsActive"]);
               return true;
            }
         }

      return false;
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }
   }

   /// <summary>
   /// Inserts details for DealParties if PartyID = 0.
   /// Else updates details for DealParties.
   /// </summary>
   /// <returns>True if Save operation is successful; Else False.</returns>
   public bool Save()
   {
      if (this.PartyID == 0)
      {
         return this.Insert();
      }
      else
      {
         if (this.PartyID > 0)
         {
            return this.Update();
         }
         else
         {
            this.PartyID = 0;
            return false;
         }
      }
   }

   /// <summary>
   /// Inserts details for DealParties.
   /// Saves newly created Id in PartyID.
   /// </summary>
   /// <returns>True if Insert operation is successful; Else False.</returns>
   private bool Insert()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_DealPartiesInsert");
         this.db.AddOutParameter(com, "PartyID", DbType.Int32, 1024);
         if (!String.IsNullOrEmpty(this.PartyCompanyName))
         {
            this.db.AddInParameter(com, "PartyCompanyName", DbType.String, this.PartyCompanyName);
         }
         else
         {
            this.db.AddInParameter(com, "PartyCompanyName", DbType.String, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.PartyName))
         {
            this.db.AddInParameter(com, "PartyName", DbType.String, this.PartyName);
         }
         else
         {
            this.db.AddInParameter(com, "PartyName", DbType.String, DBNull.Value);
         }
         if (this.PartyTypeId > 0)
         {
            this.db.AddInParameter(com, "PartyTypeId", DbType.Int32, this.PartyTypeId);
         }
         else
         {
            this.db.AddInParameter(com, "PartyTypeId", DbType.Int32, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.Address1))
         {
            this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
         }
         else
         {
            this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.Address2))
         {
            this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
         }
         else
         {
            this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.City))
         {
            this.db.AddInParameter(com, "City", DbType.String, this.City);
         }
         else
         {
            this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.State))
         {
            this.db.AddInParameter(com, "State", DbType.String, this.State);
         }
         else
         {
             this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.ZipCode))
         {
             this.db.AddInParameter(com, "ZipCode", DbType.String, this.ZipCode);
         }
         else
         {
             this.db.AddInParameter(com, "ZipCode", DbType.String, DBNull.Value);
         }
         if (this.CreatedBy > 0)
         {
            this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
         }
         else
         {
            this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
         }
         if (this.CreatedOn > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
         }
         else
         {
            this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
         }
         if (this.ModifiedBy > 0)
         {
            this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, this.ModifiedBy);
         }
         else
         {
            this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, DBNull.Value);
         }
         if (this.ModifiedOn > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "ModifiedOn", DbType.DateTime, this.ModifiedOn);
         }
         else
         {
            this.db.AddInParameter(com, "ModifiedOn", DbType.DateTime, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.Email))
         {
             this.db.AddInParameter(com, "Email", DbType.String, this.Email);
         }
         else
         {
             this.db.AddInParameter(com, "Email", DbType.String, DBNull.Value);
         }
         this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
         this.db.ExecuteNonQuery(com);
         this.PartyID = Convert.ToInt32(this.db.GetParameterValue(com, "PartyID"));      // Read in the output parameter value
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return this.PartyID > 0; // Return whether ID was returned
   }

   /// <summary>
   /// Updates details for DealParties.
   /// </summary>
   /// <returns>True if Update operation is successful; Else False.</returns>
   private bool Update()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_DealPartiesUpdate");
         this.db.AddInParameter(com, "PartyID", DbType.Int32, this.PartyID);
         if (!String.IsNullOrEmpty(this.PartyCompanyName))
         {
            this.db.AddInParameter(com, "PartyCompanyName", DbType.String, this.PartyCompanyName);
         }
         else
         {
            this.db.AddInParameter(com, "PartyCompanyName", DbType.String, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.PartyName))
         {
            this.db.AddInParameter(com, "PartyName", DbType.String, this.PartyName);
         }
         else
         {
            this.db.AddInParameter(com, "PartyName", DbType.String, DBNull.Value);
         }
         if (this.PartyTypeId > 0)
         {
            this.db.AddInParameter(com, "PartyTypeId", DbType.Int32, this.PartyTypeId);
         }
         else
         {
            this.db.AddInParameter(com, "PartyTypeId", DbType.Int32, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.Address1))
         {
            this.db.AddInParameter(com, "Address1", DbType.String, this.Address1);
         }
         else
         {
            this.db.AddInParameter(com, "Address1", DbType.String, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.Address2))
         {
            this.db.AddInParameter(com, "Address2", DbType.String, this.Address2);
         }
         else
         {
            this.db.AddInParameter(com, "Address2", DbType.String, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.City))
         {
            this.db.AddInParameter(com, "City", DbType.String, this.City);
         }
         else
         {
            this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
         }
          if (!String.IsNullOrEmpty(this.State))
         {
             this.db.AddInParameter(com, "State", DbType.String, this.State);
         }
         else
         {
             this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.ZipCode))
         {
             this.db.AddInParameter(com, "ZipCode", DbType.String, this.ZipCode);
         }
         else
         {
             this.db.AddInParameter(com, "ZipCode", DbType.String, DBNull.Value);
         }
         if (this.CreatedBy > 0)
         {
            this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
         }
         else
         {
            this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
         }
         if (this.CreatedOn > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
         }
         else
         {
            this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
         }
         if (this.ModifiedBy > 0)
         {
            this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, this.ModifiedBy);
         }
         else
         {
            this.db.AddInParameter(com, "ModifiedBy", DbType.Int32, DBNull.Value);
         }
         if (this.ModifiedOn > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "ModifiedOn", DbType.DateTime, this.ModifiedOn);
         }
         else
         {
            this.db.AddInParameter(com, "ModifiedOn", DbType.DateTime, DBNull.Value);
         }
         if (!String.IsNullOrEmpty(this.Email))
         {
             this.db.AddInParameter(com, "Email", DbType.String, this.Email);
         }
         else
         {
             this.db.AddInParameter(com, "Email", DbType.String, DBNull.Value);
         }
         this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Deletes details of DealParties for provided PartyID.
   /// </summary>
   /// <returns>True if Delete operation is successful; Else False.</returns>
   public bool Delete()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_DealPartiesDelete");
         this.db.AddInParameter(com, "PartyID", DbType.Int32, this.PartyID);
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Get list of DealParties for provided parameters.
   /// </summary>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetList()
   {
      DataSet ds = null;
      try
      {
         DbCommand com = db.GetStoredProcCommand("usp_DealPartiesGetList");
         ds = db.ExecuteDataSet(com);
      }
      catch (Exception ex)
      {
         //To Do: Handle Exception
      }

      return ds;
   }
   // Custom Action For Serach List
   public DataSet GetDealPartiesList()
   {
       DataSet ds = null;
       try
       {
           DbCommand com = db.GetStoredProcCommand("usp_DealPartySearchByName");
           this.db.AddInParameter(com, "Party", DbType.String, this.PartyName);
           ds = db.ExecuteDataSet(com);
       }
       catch (Exception ex)
       {
           //To Do: Handle Exception
       }

       return ds;
   }
   public DataSet GetDealPartiesCompanyList()
   {
       DataSet ds = null;
       try
       {
           DbCommand com = db.GetStoredProcCommand("usp_DealPartyCompanySearchByName");
           this.db.AddInParameter(com, "Company", DbType.String, this.PartyCompanyName);
           ds = db.ExecuteDataSet(com);
       }
       catch (Exception ex)
       {
           //To Do: Handle Exception
       }

       return ds;
   }
     public DataSet GetListPageWise()
        {
            DataSet ds = null;
            try
            {
                DbCommand com = db.GetStoredProcCommand("usp_DealPartiesGetPagingList");
                this.db.AddInParameter(com, "CurrentPageNo", DbType.Int32, this.CurrentPageNo);
                this.db.AddInParameter(com, "PageSize", DbType.Int32, this.PageSize);
                this.db.AddOutParameter(com, "TotalRecords", DbType.Int32, 0);
                if (this.PartyTypeId > 0)
                {
                    this.db.AddInParameter(com, "PartyTypeId", DbType.Int32, this.PartyTypeId);
                }
                else
                {
                    this.db.AddInParameter(com, "PartyTypeId", DbType.Int32, DBNull.Value);
                }
               
                if (!String.IsNullOrEmpty(this.PartyName))
                {
                    this.db.AddInParameter(com, "PartyName", DbType.String, this.PartyName);
                }
                else
                {
                    this.db.AddInParameter(com, "PartyName", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.PartyCompanyName))
                {
                    this.db.AddInParameter(com, "PartyCompanyName", DbType.String, this.PartyCompanyName);
                }
                else
                {
                    this.db.AddInParameter(com, "PartyCompanyName", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.City))
                {
                    this.db.AddInParameter(com, "City", DbType.String, this.City);
                }
                else
                {
                    this.db.AddInParameter(com, "City", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.State))
                {
                    this.db.AddInParameter(com, "State", DbType.String, this.State);
                }
                else
                {
                    this.db.AddInParameter(com, "State", DbType.String, DBNull.Value);
                }
                if (!String.IsNullOrEmpty(this.ZipCode))
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, this.ZipCode);
                }
                else
                {
                    this.db.AddInParameter(com, "ZipCode", DbType.String, DBNull.Value);
                }
                ds = db.ExecuteDataSet(com);
                this.TotalRecords = Convert.ToInt32(this.db.GetParameterValue(com, "@TotalRecords"));
            }
            catch (Exception ex)
            {
                //To Do: Handle Exception
            }

            return ds;
        }
#endregion
    #region CustomActions
     //public DataTable DealPartyExist()
     //{
     //    try
     //    {
     //        DataTable dt = new DataTable();
     //        if (this.PartyID != 0)
     //        {
     //            DbCommand com = this.db.GetStoredProcCommand("usp_DealPartyExist");
     //            this.db.AddInParameter(com, "PartyId", DbType.Int32, this.PartyID);
     //            DataSet ds = this.db.ExecuteDataSet(com);
     //            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
     //            {
     //                dt = ds.Tables[0];
     //                return dt;
     //            }
     //        }
     //        return dt;


     //    }
     //    catch (Exception ex)
     //    {
     //        // To Do: Handle Exception
     //        return null;
     //    }
     //}
     //public bool CheckBeforeDelete()
     //{
     //    DataSet ds = null;
     //    try
     //    {
     //        DbCommand com1 = this.db.GetStoredProcCommand("usp_DealPartyExist");
     //        this.db.AddInParameter(com1, "PartyId", DbType.Int32, this.PartyID);
     //        ds = db.ExecuteDataSet(com1);
     //        if (ds.Tables[0].Rows.Count == 0)
     //        {
     //            DbCommand com = this.db.GetStoredProcCommand("usp_DealPartiesDelete");
     //            this.db.AddInParameter(com, "PartyId", DbType.Int32, this.PartyID);
     //            this.db.ExecuteNonQuery(com);
     //            return true;
     //        }
     //        return false;
     //    }
     //    catch (Exception ex)
     //    {
     //        // To Do: Handle Exception
     //        return false;
     //    }


     //}
    #endregion
#endregion
}
