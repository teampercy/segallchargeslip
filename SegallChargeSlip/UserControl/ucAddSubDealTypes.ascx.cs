﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;

public partial class UserControl_ucAddSubDealTypes : System.Web.UI.UserControl
{
    #region Events
    private int PageSize = GlobleData.Pagesize;

    public string DealTypeMode
    {
        get { return String.IsNullOrEmpty(hdnDealSubTypesMode.Value) ? "L" : Convert.ToString(hdnDealSubTypesMode.Value); }
        set
        {
            switch (value)
            {
                case "E":  // Edit                    
                    divListDealSubTypes.Style.Add("display", "none");// List
                    divEditDealSubTypes.Style.Remove("display");//.Add("display", "");
                    ((Label)this.Parent.FindControl("lbDealSubTypesTitle")).Text = "EDIT DEAL SUBTYPES";
                    break;
                case "A": // Add
                    divListDealSubTypes.Style.Add("display", "none"); // List
                    txtDealSubTypes.Text = "";
                    hdnDealSubTypesId.Value = "0";
                    divEditDealSubTypes.Style.Add("display", "");
                    //  ((Label)this.Parent.FindControl("lbDealSubTypesTitle")).Text = "ADD DEAL TYPE";
                    break;
                case "M":  // Manage 
                    divListDealSubTypes.Style.Add("display", "");// List
                    divEditDealSubTypes.Style.Add("display", "none");
                    hdnDealSubTypesId.Value = "0";
                    ((Label)this.Parent.FindControl("lbDealSubTypesTitle")).Text = "DEAL SUBTYPES";
                    break;
                default:
                    divListDealSubTypes.Style.Add("display", "");// List
                    divEditDealSubTypes.Style.Add("display", "none");
                    break;
            }
            hdnDealSubTypesMode.Value = value;
        }
    }
    public int DealSubTypeID
    {
        get { return String.IsNullOrEmpty(hdnDealSubTypesId.Value) ? 0 : Convert.ToInt32(hdnDealSubTypesId.Value); }
        set { hdnDealSubTypesId.Value = value.ToString(); }
    }


    protected void gvDealSubTypes_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                int TotalRecords = Convert.ToInt32(Session["ucSubDealSubTypesTotalRecords"]);
                double dblPageCount = (double)((decimal)TotalRecords / (decimal)PageSize);
                int PageCount = (int)Math.Ceiling(dblPageCount);

                for (int i = 1; i <= PageCount; i++)
                {
                    LinkButton btn = new LinkButton();
                    btn.Width = Unit.Pixel(20);
                    btn.CommandName = "Page";
                    btn.CommandArgument = i.ToString();

                    btn.Text = i.ToString();

                    PlaceHolder place = e.Row.FindControl("phPageNo") as PlaceHolder;
                    place.Controls.Add(btn);
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("UserControl_ucAddSubDealSubTypes", "gvDealSubTypes_RowCreated", ex.Message);
        }
    }

    protected void gvDealSubTypes_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Del")
            {
                DealSubTypeID = Convert.ToInt32(e.CommandArgument);
                DealSubTypes dealTypes = new DealSubTypes();
                // dealTypes.UserID = UserID;
                //dealTypes.IsActive = false;
                dealTypes.Delete();
                BindGrid(0);


            }
            if (e.CommandName == "EditDealtype")
            {
                DealTypeMode = "E";
                DealSubTypeID = Convert.ToInt32(e.CommandArgument);
                DealSubTypes dealTypes = new DealSubTypes();
                dealTypes.DealSubTypeID = DealSubTypeID;

                Boolean isloaded = dealTypes.Load();
                if (isloaded)
                {
                    txtDealSubTypes.Text = dealTypes.DealSubType;
                    ddlDealTypesParent.ClearSelection();
                    if (ddlDealTypesParent.Items.FindByValue(dealTypes.DealSubTypeID.ToString()) != null)
                    {
                      ddlDealTypesParent.Items.FindByValue(dealTypes.DealSubTypeID.ToString()).Selected = true;
                    }
                    
                }
                divListDealSubTypes.Style.Add("display", "none");// List
                divEditDealSubTypes.Style.Remove("display");//.Add("display","block");
                ((Label)this.Parent.FindControl("lbDealSubTypesTitle")).Text = "EDIT DEAL TYPE";
                lbSubDealTypeErrorMsg.Text = "";
                // upAddNewDealSubTypes.Update();
            }


        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("UserControl_ucAddSubDealSubTypes", "gvDealSubTypes_RowCommand", ex.Message);
        }
    }
    protected void gvDealSubTypes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            int i = e.NewPageIndex + 1;
            ViewState["PageNo"] = i;
            BindGrid(i);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("UserControl_ucAddSubDealSubTypes", "gvDealSubTypes_PageIndexChanging", ex.Message);
        }
    }

    public void BindGrid(int PageIndex)
    {
        try
        {
            DealSubTypes dealTypes = new DealSubTypes();
            dealTypes.CurrentPageNo = Convert.ToInt32(ViewState["PageNo"]);

            dealTypes.PageSize = PageSize;

            DataSet ds = dealTypes.GetList();
            int TotalRecords = dealTypes.TotalRecords;
            Session["ucSubDealSubTypesTotalRecords"] = TotalRecords;
            gvDealSubTypes.DataSource = ds;
            gvDealSubTypes.DataBind();
            if (gvDealSubTypes.Rows.Count > 0)
            {
                gvDealSubTypes.BottomPagerRow.Visible = true;
            }

            // upDealSubTypes.Update();

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("UserControl_ucAddSubDealSubTypes", "BindGrid", ex.Message);
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }


    public void BindControls()
    {
        try
        {
            DealTypes objdealType = new DealTypes();
            ddlDealTypesParent.Items.Clear();
            Utility.FillDropDown(ref ddlDealTypesParent, objdealType.GetList().Tables[0], "DealType", "DealTypeID"); //Fill Deal Type
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("UserControl_ucAddSubDealSubTypes", "BindControls", ex.Message);
        }    
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            DealSubTypes dealType = new DealSubTypes();
            dealType.DealSubTypeID = DealSubTypeID;
            dealType.Load();
            dealType.DealSubType = txtDealSubTypes.Text;
            dealType.DealTypeID = Convert.ToInt32(ddlDealTypesParent.SelectedValue);
            if (!dealType.Save())
            {
                lbSubDealTypeErrorMsg.Text = "Error: Sub Deal Type could not be saved.";
                DealTypeMode = "A";
            }
            else
            {
                DealTypeMode = "M";
                this.Page.GetType().InvokeMember("BindSubDealTypes", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
                BindGrid(0);
            }
          
            upAddNewDealSubTypes.Update();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("UserControl_ucAddSubDealSubTypes", "btnSave_Click", ex.Message);
        }
    }


}