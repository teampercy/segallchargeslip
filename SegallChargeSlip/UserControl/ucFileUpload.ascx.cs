﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using SegallChargeSlipBll;
using System.Data;

public partial class UserControl_ucFileUpload : System.Web.UI.UserControl
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        { 
        this.Page.Form.Enctype = "multipart/form-data";
        if (!Page.IsPostBack)
        {
            
        }
        imgAttach.Attributes.Add("display","block");
            //upFileUpload.Attributes.Clear();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucFileUpload.ascx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    protected void btnFileUpload_Click(object sender, EventArgs e)
    {
        try
        { 
       // pnAttachment.Visible = false;
        if (upFileUpload.HasFile)
        {

            string strGuid = string.Empty;
            strGuid = System.Guid.NewGuid().ToString();
            string ServerPath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
            string fileName = Path.GetFileName(upFileUpload.PostedFile.FileName);


            if (!Directory.Exists(Server.MapPath(ServerPath)))
            { Directory.CreateDirectory(Server.MapPath(ServerPath)); }

            upFileUpload.PostedFile.SaveAs(Server.MapPath(ServerPath) + strGuid + "_" + fileName);


            DataTable dtAttachment = new DataTable();
            DataColumn dtcol = new DataColumn("AttachmentDescription", typeof(string));
            dtAttachment.Columns.Add(dtcol);
            dtcol = new DataColumn("FilePath", typeof(string));
            dtAttachment.Columns.Add(dtcol);
            dtcol = new DataColumn("UniqueAttachmentDescription", typeof(string));
            dtAttachment.Columns.Add(dtcol);
            DataRow dr;
            dr = dtAttachment.NewRow();
            dr[0] = strGuid + "_" + fileName;
            dr[1] = Server.MapPath(ServerPath) + strGuid + "_" + fileName;
            dr[2] = strGuid + "_" + fileName;
            dtAttachment.Rows.Add(dr);
            Session["dtAttachment"] = dtAttachment;
            BindGrid();
            upAtttachment.Update();
            //divAttachment.Attributes.Add("display","block");

            //Attachments objAttachments = new Attachments();
            //objAttachments.AttachmentDescription = strGuid + "_" + fileName;
            //objAttachments.ChargeSlipID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
            //objAttachments.CreatedBy = Convert.ToInt32(Session[GlobleData.User]);
            //objAttachments.CreatedOn = DateTime.Now;
            //objAttachments.FilePath = Server.MapPath(ServerPath) + strGuid + "_" + fileName;
            //Boolean rslt = objAttachments.Save();


        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucFileUpload.ascx", "btnFileUpload_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void imgbtDelete_Click(object sender, ImageClickEventArgs e)
    {
        try
        { 
        string id = (sender as ImageButton).CommandArgument;
        //Attachments objAttachment = new Attachments();
        //objAttachment.AttachmentID = Convert.ToInt32(id);
        //objAttachment.Load();



        string Serverpath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
        string pathfile = Serverpath + id;

        string fullpath = Server.MapPath(pathfile);

        if (File.Exists(fullpath))
        {
            File.Delete(fullpath);
        }
        // objAttachment.Delete();
        Session["dtAttachment"] = null;
        BindGrid();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucFileUpload.ascx", "imgbtDelete_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #region CustomActions

    public void BindGrid()
    {
        try
        { 
        // string[] filePaths = Directory.GetFiles(Server.MapPath("~/Uploads/"));
        //divAttachment.Style.Add("display","block");
        //   string[] fileNames = Directory.GetFiles(Server.MapPath("~/Uploads/")).Select(path => Path.GetFileName(path))  .ToArray();
        DataTable dtGrid = (DataTable)Session["dtAttachment"];
        ////List<ListItem> files = new List<ListItem>();
        ////Attachments objAttachments = new Attachments();
        ////objAttachments.ChargeSlipID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
        ////objAttachments.CreatedBy = Convert.ToInt32(Session[GlobleData.User]);
        ////dtAttachment = objAttachments.GetDetailsByUserAndChargeSlip();
        ////if (dtAttachment != null)
        ////{
        ////    for (int i = 0; i <= dtAttachment.Rows.Count - 1; i++)
        ////    {

        ////        //string filePath = dtAttachment.Rows[i]["FilePath"].ToString();
        ////        //files.Add(new ListItem(Path.GetFileName(filePath)));

        ////        string filePath = dtAttachment.Rows[i]["FilePath"].ToString();
        ////        int id = Convert.ToInt32(dtAttachment.Rows[i]["AttachmentID"].ToString());
        ////        int count = Path.GetFileName(filePath).IndexOf('_');
        ////        string filename = Path.GetFileName(filePath).Substring(count + 1);
        ////        files.Add(new ListItem(filename, Convert.ToString(id)));

        ////    }

        if (dtGrid != null && dtGrid.Rows.Count > 0)
        {
            string attachment = dtGrid.Rows[0]["AttachmentDescription"].ToString();
            int count = attachment.IndexOf('_');
            dtGrid.Rows[0]["AttachmentDescription"] = attachment.Substring(count + 1).ToString();
            gvAttach.DataSource = dtGrid;
            gvAttach.DataBind();
            pnAttachment.Visible = true;
            upAtttachment.Update();
        }
        else
        {
            gvAttach.DataSource = null;
            gvAttach.DataBind();
            pnAttachment.Visible = false;
            upAtttachment.Update();
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucFileUpload.ascx", "BindGrid", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    #endregion
}

