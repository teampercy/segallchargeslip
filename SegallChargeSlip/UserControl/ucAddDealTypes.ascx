﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAddDealTypes.ascx.cs" Inherits="UserControl_ucAddDealTypes" %>
<link href="../Styles/Site.css" rel="stylesheet" />
<script src="../Js/jquery-1.9.1.js" type="text/javascript"></script>
<script type="text/javascript">

    function dealTypeAdd()
    {
        $("[id$=divListDealTypes]").hide();
        $("[id$=divEditDealTypes]").show();
        $("[id$=txtDealType]").val("");
        $("[id$=hdnDealTypeId]").val("");
        $("[id$=lbDealTypesTitle]").text("ADD DEAL TYPE");
        $("[id$=lbDealTypeErrorMsg]").text("");        
        return false;
    }

    function btnBack_Click()
    {
        $("[id$=divListDealTypes]").show();
        $("[id$=divEditDealTypes]").hide();
        $("[id$=txtDealType]").val("");
        $("[id$=hdnDealTypeId]").val("");
        $("[id$=lbDealTypesTitle]").text("DEAL TYPES");
        $("[id$=lbDealTypeErrorMsg]").text("");
        return false;
    }
</script>
<link href="../Styles/SiteTEST.css" rel="stylesheet" />

<asp:UpdatePanel ID="upAddNewDealTypes" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

        <div id="divListDealTypes" runat="server">
            <table style="width:100%">
               <tr>
                   <td>
                       &nbsp;
                   </td>
               </tr>            
                <tr align="Right">
                    <td>
                        <asp:Button Text="ADD" ID="btnAdd" CssClass="GrayRoundButton" BackColor="Gray" runat="server" OnClientClick="return dealTypeAdd();" />
                    </td>
                </tr>
             
                <tr align="Center"> 
                    <td style="width:100%">
                       <asp:GridView ID="gvDealTypes" runat="server" Style="width: 90%; margin: 0px 5px 5px 5px;" AutoGenerateColumns="false" OnPageIndexChanging="gvDealTypes_PageIndexChanging" OnRowCreated="gvDealTypes_RowCreated" AllowPaging="true"
                    EmptyDataText="No Records Found"  DataKeyNames="DealTypeID" 
                    OnRowCommand="gvDealTypes_RowCommand" >
                    <AlternatingRowStyle BackColor="#eeeeee" />
                    <HeaderStyle BackColor="#cccccc" Font-Bold="true" Font-Names="Verdena" HorizontalAlign="Center" />

                    <Columns>
                        <asp:BoundField DataField="DealType" HeaderText="Deal Type" />
                        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                            ItemStyle-VerticalAlign="Middle">
                            <ItemTemplate>
                                 <asp:LinkButton ID="lnkEdit" CommandArgument='<%# Eval("DealTypeID") %>' CommandName="EditDealtype" runat="server">
                                    <asp:Image ID="imgEdit" runat="server" ImageUrl="~/Images/edit.jpg" Height="15"
                                        Width="15" ToolTip="Edit this User" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                            ItemStyle-VerticalAlign="Middle">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDelete" CommandArgument='<%# Eval("DealTypeID") %>' CommandName="Del"
                                    OnClientClick="return confirm('Are you sure you want to delete ?')" runat="server">
                                    <asp:Image ID="imgDel" runat="server" ImageUrl="~/Images/delete.gif" Height="15"
                                        Width="15" ToolTip="Delete this Deal Type" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                   <PagerTemplate>
                        <asp:PlaceHolder ID="phPageNo" runat="server" />
                    </PagerTemplate>
                </asp:GridView>
                    </td>
                  
                </tr>
            </table>
        </div>

        <div id="divEditDealTypes" runat="server" style="height: auto;">

            <table style="width: 100%" class="spacing" cellpadding="3">

                <tr>
                    <td colspan="5">&nbsp;</td>
                </tr>
                <tr id="trName" runat="server">
                    <td>&nbsp;</td>
                    <td style="width: 20%" class="Padding">DEAL TYPE:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDealType" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">
                        <asp:Label runat="server" ID="lbDealTypeErrorMsg" ForeColor="Red" Font-Bold="true" Font-Names="Vardena" Font-Size="Small" Text=""></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: right;" colspan="5">
                         <asp:HiddenField ID="hdnClassRef" runat="server" />
                         &nbsp;&nbsp;  
                          <asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="GrayRoundButton" BackColor="Gray" OnClick="btnSave_Click" Height="18px" /> &nbsp;&nbsp;
                         <asp:Button ID="btnBack" runat="server" Text="BACK" CssClass="GrayRoundButton" BackColor="Gray"  Height="18px"  OnClientClick="return btnBack_Click();" />
                    </td>
                </tr>
            </table>
            
        </div>

        <asp:HiddenField ID="hdnDealTypeMode" runat="server" />   
        <asp:HiddenField ID="hdnDealTypeId" runat="server" />   
         
    </ContentTemplate>
</asp:UpdatePanel>
