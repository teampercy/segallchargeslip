﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAddSubDealTypes.ascx.cs" Inherits="UserControl_ucAddSubDealTypes" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<link href="../Styles/Site.css" rel="stylesheet" />
<script src="../Js/jquery-1.9.1.js" type="text/javascript"></script>
<script type="text/javascript">

    function DealSubTypesAdd() {
        $("[id$=divListDealSubTypes]").hide();
        $("[id$=divEditDealSubTypes]").show();
        $("[id$=txtDealSubTypes]").val("");
        $("[id$=hdnDealSubTypesId]").val("");
        $("[id$=lbDealSubTypesTitle]").text("ADD DEAL SUBTYPE");
        $("[id$=ddlDealTypesParent]").prop('selectedIndex', 0);
        $("[id$=lbSubDealTypeErrorMsg]").text("");        
        return false;
    }

    function btnBack_SubDealType_Click() {
        $("[id$=divListDealSubTypes]").show();
        $("[id$=divEditDealSubTypes]").hide();
        $("[id$=txtDealSubTypes]").val("");
        $("[id$=hdnDealSubTypesId]").val("");
        $("[id$=lbDealSubTypesTitle]").text("DEAL SUBTYPES");
        $("[id$=ddlDealTypesParent]").prop('selectedIndex', 0);
        $("[id$=lbSubDealTypeErrorMsg]").text("");
        return false;
    }

    function btnSaveSubDealType_Click() {
        var selectedDealType =  $("[id$=ddlDealTypesParent]").val();
        if( selectedDealType == "0" || selectedDealType == "")
        {
            alert("Please select a Deal type.");
            return false;
        }
        return true;
    }
</script>
<link href="../Styles/SiteTEST.css" rel="stylesheet" />

<asp:UpdatePanel ID="upAddNewDealSubTypes" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

        <div id="divListDealSubTypes" runat="server">
            <table style="width:100%">
               <tr>
                   <td>
                       &nbsp;
                   </td>
               </tr>            
                <tr align="Right">
                    <td>
                        <asp:Button Text="ADD" ID="btnAdd" CssClass="GrayRoundButton" BackColor="Gray" runat="server" OnClientClick="return DealSubTypesAdd();" />
                    </td>
                </tr>
             
                <tr align="Center"> 
                    <td style="width:100%">
                    <asp:GridView ID="gvDealSubTypes" runat="server" Style="width: 90%; margin: 0px 5px 5px 5px;" AutoGenerateColumns="false" OnPageIndexChanging="gvDealSubTypes_PageIndexChanging" OnRowCreated="gvDealSubTypes_RowCreated" AllowPaging="true"
                    EmptyDataText="No Records Found"  DataKeyNames="DealSubTypeID" 
                    OnRowCommand="gvDealSubTypes_RowCommand" >
                    <AlternatingRowStyle BackColor="#eeeeee" />
                    <HeaderStyle BackColor="#cccccc" Font-Bold="true" Font-Names="Verdena" HorizontalAlign="Center" />
                    <Columns>
                        <asp:BoundField DataField="DealType" HeaderText="DEAL TYPE" />
                        <asp:BoundField DataField="DealSubType" HeaderText="DEAL SUBTYPE" />
                        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                            ItemStyle-VerticalAlign="Middle">
                            <ItemTemplate>
                                 <asp:LinkButton ID="lnkEdit" CommandArgument='<%# Eval("DealSubTypeID") %>' CommandName="EditDealtype" runat="server">
                                    <asp:Image ID="imgEdit" runat="server" ImageUrl="~/Images/edit.jpg" Height="15"
                                        Width="15" ToolTip="Edit this User" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                            ItemStyle-VerticalAlign="Middle">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDelete" CommandArgument='<%# Eval("DealSubTypeID") %>' CommandName="Del"
                                    OnClientClick="return confirm('Are you sure you want to delete ?')" runat="server">
                                    <asp:Image ID="imgDel" runat="server" ImageUrl="~/Images/delete.gif" Height="15"
                                        Width="15" ToolTip="Delete this Deal Type" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                   <PagerTemplate>
                        <asp:PlaceHolder ID="phPageNo" runat="server" />
                    </PagerTemplate>
                </asp:GridView>
                    </td>
                  
                </tr>
            </table>
        </div>

        <div id="divEditDealSubTypes" runat="server" style="height: auto;">

            <table style="width: 100%" class="spacing" cellpadding="3">
                <tr>
                    <td colspan="5">&nbsp;</td>
                </tr>
                <tr >
                    <td>&nbsp;</td>
                    <td style="width: 20%" class="Padding">DEAL TYPE:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlDealTypesParent">
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr >
                    <td>&nbsp;</td>
                    <td style="width: 20%" class="Padding">DEAL SUB TYPE:
                    </td>
                    <td>
                        <asp:TextBox ID="txtDealSubTypes" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">
                        <asp:Label runat="server" ID="lbSubDealTypeErrorMsg" ForeColor="Red" Font-Bold="true" Font-Names="Vardena" Font-Size="Small" Text=""></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: right;" colspan="5">
                         <asp:HiddenField ID="hdnClassRef" runat="server" />
                         &nbsp;&nbsp;  
                          <asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="GrayRoundButton" BackColor="Gray" OnClick="btnSave_Click" Height="18px" OnClientClick="return btnSaveSubDealType_Click" /> &nbsp;&nbsp;
                         <asp:Button ID="btnBack" runat="server" Text="BACK" CssClass="GrayRoundButton" BackColor="Gray"  Height="18px"  OnClientClick="return btnBack_SubDealType_Click();" />
                    </td>
                </tr>
            </table>
            
        </div>

        <asp:HiddenField ID="hdnDealSubTypesMode" runat="server" />   
        <asp:HiddenField ID="hdnDealSubTypesId" runat="server" />   
         
    </ContentTemplate>
</asp:UpdatePanel>
