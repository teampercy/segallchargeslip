﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucTimeline.ascx.cs" Inherits="UserControl_ucTimeline" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<style type="text/css">
    .modalBackground {
        background-color: gray;
        filter: alpha(opacity=90);
        opacity: 0.8;
    }

    .modalPopup {
        background-color: #FFFFFF;
        border-width: 1px;
        border-style: solid;
        border-color: black;
        /*padding-top: 10px;
        padding-left: 10px;*/
        /*width: 300px;
        height: 140px;*/
    }

    .labelText {
        font-weight: bold;
        color: Black;
    }

    .labelOutput {
        color: red;
        font-weight: bold;
    }
</style>

<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel runat="server" ID="upTimeline">
    <ContentTemplate>
        <div runat="server" style="height: 30px; padding: 7px 0px 0px 5px; border-bottom: 1px groove black; vertical-align: central">
            <div id="dvView" runat="server" style="float: left; width: 75%; overflow: hidden;">
                <span style="width: 100PX">VIEW BY:</span>
                <asp:DropDownList ID="drDuration" runat="server">
                    <asp:ListItem Value="Month" Text="MONTH" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="Quarter" Text="QUARTER"></asp:ListItem>
                    <asp:ListItem Value="Year" Text="YEAR"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div id="dvBrokers" runat="server" style="float: left; width: 25%; overflow: hidden;">
                <span style="width: 100PX">BROKER:</span>
                <asp:DropDownList ID="drUsers" runat="server"></asp:DropDownList>
            </div>
        </div>

        <div id="CommissionChart" style="width: 100%; height: 600px;"></div>

        <div style="text-align: center; padding-right: 50px;">
            <input type="button" id="btnPrev" onclick="return NavigateChart('Prev');" value="Prev" class="SqureGrayButtonSmall" />
            <input type="button" id="btnNext" onclick="return NavigateChart('Next');" value="Next" class="SqureGrayButtonSmall" />
        </div>
        <asp:HiddenField ID="hdnYear" runat="server" />

        <asp:Button ID="btnShow" runat="server" Style="display: none;" />
        <asp:LinkButton ID="lnk12" runat="server" Style="display: none;" />
        <ajax:ModalPopupExtender ID="ModalDetail" runat="server" BehaviorID="mpePopup" PopupControlID="Panel1"
            TargetControlID="btnShow" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
        </ajax:ModalPopupExtender>

        <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" align="center" Style="display: none">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr id="divheader" style="background-color: black; height: 35px;">
                    <td style="border-bottom: solid 2px black; text-align: center;">&nbsp;&nbsp;
                        <asp:Label ID="lblCHARGEDETAIL" Style="vertical-align: bottom; font-size: medium; font-weight: bold; color: white;"
                            runat="server" Text="Net Commissions Collected"></asp:Label>
                    </td>
                    <td width="30px" style="text-align: center; border-bottom: solid 2px black;">
                        <asp:ImageButton ID="btnClose" src="../Images/close.png" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>

                <tr>
                    <td>
                        <table width="70%" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                                <td style="padding-left: 25px;">
                                    <asp:Label Text="Period:" runat="server" CssClass="labelText"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPeriod" runat="server" CssClass="labelOutput"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label Text="Net Due:" runat="server" CssClass="labelText"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblNetDue" runat="server" CssClass="labelOutput"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 25px;">
                                    <asp:Label Text="No. of Deals:" runat="server" CssClass="labelText"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblNoDeals" runat="server" CssClass="labelOutput"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label Text="Net Collected:" runat="server" CssClass="labelText"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblNetCollected" runat="server" CssClass="labelOutput"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <br />
                    </td>
                </tr>

                <tr style="background-color: grey; height: 30px;">
                    <td style="border: solid 2px black; text-align: center;" colspan="2">
                        <asp:Label Style="vertical-align: bottom; font-size: medium; font-weight: bold; color: white;"
                            runat="server" Text="Deals"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" align="center">
                        <div style="overflow: auto; width: 900px; height: 650px; margin-left: 20px; margin-right: 20px;">

                            <asp:GridView ID="grdChargeslipList" runat="server" AutoGenerateColumns="false"
                                ShowFooter="True" Font-Size="12px" font-family="Verdana" Width="90%">
                                <%--<Columns>
                                    <asp:BoundField DataField="DealName" HeaderText="Deal Name" />
                                    <asp:BoundField DataFormatString="{0:d}" DataField="DueDateShort" HeaderText="Due Date" ItemStyle-HorizontalAlign="Center" FooterStyle-Width="80px" />

                                    <asp:TemplateField HeaderText="Amount Due" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            $<asp:Label ID="lblCommAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("CommAmountDue"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooCommAmount" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="PaymentDateShort" HeaderText="Paid Date" ItemStyle-HorizontalAlign="Center" FooterStyle-Width="80px" />

                                    <asp:TemplateField HeaderText="Paid Amount" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            $<asp:Label ID="lblPaidAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("PaidAmount"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooPaidAmount" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Unapplied" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            $<asp:Label ID="lblUnapplied" runat="server" Text='<%# String.Format("{0:n2}",Eval("UnApplied"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooUnapplied" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>

                                <EmptyDataTemplate>
                                    <b style="padding: 2px 5px  2px 25px; color: #000000; background-color: #FFFFFF; height: 26px; vertical-align: middle; cursor: pointer;">No records found.</b>
                                </EmptyDataTemplate>
                                <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                <HeaderStyle CssClass="HeaderGridView2" />
                                <RowStyle CssClass="RowGridView2" />
                                <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                <FooterStyle CssClass="FooterGridView2" />--%>
                            </asp:GridView>

                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>




<script type="text/javascript">
    $(document).ready(function () {
        document.getElementById('<%=hdnYear.ClientID %>').value = new Date().getFullYear();                     
    });

    function NavigateChart(Navigation) {
        var Year = parseInt(document.getElementById('<%=hdnYear.ClientID %>').value);
        if (Navigation == 'Prev') {
            Year = Year - 1;
            $('#ContentMain_ucTimeline_hdnYear').val(Year);
            DrawTimelineChart();
        }
        else if (Navigation == 'Next') {
            Year = Year + 1;
            $('#ContentMain_ucTimeline_hdnYear').val(Year);
            DrawTimelineChart();
        }
    }

    $('#ContentMain_ucTimeline_drUsers').change(function () {
        DrawTimelineChart();
    });

    $('#ContentMain_ucTimeline_drDuration').change(function () {
        DrawTimelineChart();
    });

    function DrawTimelineChart() {        
        var UserId = $('#ContentMain_ucTimeline_drUsers option:selected').val();
        var Duration = $('#ContentMain_ucTimeline_drDuration option:selected').val();
        var Year = $('#ContentMain_ucTimeline_hdnYear').val();
        var title;

        if (Duration == 'Month') {
            title = Year;
        }
        else if (Duration == 'Year') {
            title = Year + '-' + (parseInt(Year) + 5);
        }
        else if (Duration == 'Quarter') {
            title = Year + '-' + (parseInt(Year) + 1);
        }

        $.ajax({
            url: "Timeline.aspx/GetTimelineChartData",
            type: "POST",
            data: JSON.stringify({ 'UserId': UserId, 'Year': Year, 'Duration': Duration }),
            contentType: "application/json;charset=utf-8",
            success: function (res) {                
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'MonthYear');
                data.addColumn('number', 'Amount to be Collected');
                data.addColumn({ type: 'number', role: 'annotation' });
                data.addColumn('number', 'Amount Collected');
                data.addColumn({ type: 'number', role: 'annotation' });

                //AmountUnpaidData = res.d.find(obj=>obj.Legend === 'TotalMonthCommAmtDue').ChartData;
                //AmountPaidData = res.d.find(obj=>obj.Legend === 'TotalMonthPaidAmount').ChartData;

                AmountUnpaidData = res.d[0].ChartData;
                AmountPaidData = res.d[1].ChartData;

                for (var month = 1; month <= AmountUnpaidData.length; month++) {
                    if (AmountUnpaidData[month - 1].CaseCount != 0 && AmountPaidData[month - 1].CaseCount != 0) {
                        data.addRow([AmountUnpaidData[month - 1].XaxisValue,
                                 AmountUnpaidData[month - 1].YaxisValue,
                                 AmountUnpaidData[month - 1].CaseCount,
                                 AmountPaidData[month - 1].YaxisValue,
                                 AmountPaidData[month - 1].CaseCount]);
                    }
                    else if (AmountUnpaidData[month - 1].CaseCount == 0 && AmountPaidData[month - 1].CaseCount == 0) {
                        data.addRow([AmountUnpaidData[month - 1].XaxisValue,
                                 AmountUnpaidData[month - 1].YaxisValue,
                                 null,
                                 AmountPaidData[month - 1].YaxisValue,
                                 null]);
                    }
                    else if (AmountUnpaidData[month - 1].CaseCount == 0) {
                        data.addRow([AmountUnpaidData[month - 1].XaxisValue,
                                 AmountUnpaidData[month - 1].YaxisValue,
                                 null,
                                 AmountPaidData[month - 1].YaxisValue,
                                 AmountPaidData[month - 1].CaseCount]);
                    } else if (AmountPaidData[month - 1].CaseCount == 0) {
                        data.addRow([AmountUnpaidData[month - 1].XaxisValue,
                                 AmountUnpaidData[month - 1].YaxisValue,
                                 AmountUnpaidData[month - 1].CaseCount,
                                 AmountPaidData[month - 1].YaxisValue,
                                 null]);
                    }

                }

                // format numbers in second column to 5 decimals
                var formatter = new google.visualization.NumberFormat({
                    prefix: '$'
                    //pattern: 'short'
                });
                formatter.format(data, 1);
                formatter.format(data, 3);

                var options = {
                    title: 'Net Commissions Collected (' + title + ')',
                    titleTextStyle: { fontSize: 20 },
                    textStyle: { fontName: 'Verdana', },
                    vAxis: {
                        title: 'Amount',
                        titleTextStyle: { color: 'red', fontSize: 17, bold: true },
                        //logScale: true,                        
                        minValue: 0,
                        format: 'short',
                        gridlines: { count: 20 },
                        //viewWindowMode: 'explicit',
                        viewWindow: { min: 0, },
                        //ticks: [1, 50000, 100000, 150000, 200000, 250000, 300000, 350000, 400000, 450000, 500000,1000000 ]
                    },
                    //explorer: {},  //zoom the Chart
                    hAxis: {
                        title: 'Year',
                        titleTextStyle: { color: 'red', fontSize: 17, bold: true }
                    },
                    legend: { position: 'bottom' },
                    colors: ['#DC143C', '#32CD32'],
                    backgroundColor: 'none',
                    annotations: {
                        alwaysOutside: true,
                        textStyle: {
                            //fontName: 'Verdana',
                            fontSize: 18,
                            bold: true,
                            //italic: true,
                            color: 'black',     // The color of the text.
                            //auraColor: '#d799ae', // The color of the text outline.
                            //opacity: 0.8          // The transparency of the text.
                        }
                    }

                };

                var chart = new google.visualization.ColumnChart($('#CommissionChart')[0]);

                // use the 'ready' event to modify the chart once it has been drawn
                google.visualization.events.addListener(chart, 'ready', function () {
                    var chartDiv = document.getElementById('CommissionChart');
                    var axisLabels = chartDiv.getElementsByTagName('text');
                    for (var i = 0; i < axisLabels.length; i++) {
                        if (axisLabels[i].getAttribute('text-anchor') === 'end') {
                            axisLabels[i].innerHTML = '$' + axisLabels[i].innerHTML;
                        }
                    }
                });

                google.visualization.events.addListener(chart, 'select', function () {
                    var durationPeriod = data.getValue(chart.getSelection()[0].row, 0);
                    var NetDue = '$' + data.getValue(chart.getSelection()[0].row, 1).toLocaleString('en');
                    var NetCollected = '$' + data.getValue(chart.getSelection()[0].row, 3).toLocaleString('en');
                    var NetDueTransactionCount = data.getValue(chart.getSelection()[0].row, 2);
                    var NetCollectedTransactionCount = data.getValue(chart.getSelection()[0].row, 4);
                    var content = "ContentMain_ucTimeline";

                    $('#' + content + '_lblPeriod').html(durationPeriod);
                    $('#' + content + '_lblNetDue').html(NetDue);
                    $('#' + content + '_lblNetCollected').html(NetCollected);

                    if (NetDueTransactionCount == null) {
                        NetDueTransactionCount = 0;
                    }
                    if (NetCollectedTransactionCount == null) {
                        NetCollectedTransactionCount = 0;
                    }

                    var TotalTransactionCount = NetDueTransactionCount + NetCollectedTransactionCount;
                    $('#' + content + '_lblNoDeals').html(TotalTransactionCount);

                    GetTimelineChartChargeSlip(UserId, Year, Duration, durationPeriod);
                    chart.setSelection(); // nulls out the selection 
                });

                chart.draw(data, options);
            },
            fail: function (response) {
                alert("Unable to display chart");
            }
        });
    }

    function GetTimelineChartChargeSlip(UserId, Year, Duration, durationPeriod) {
        $.ajax({
            url: "Timeline.aspx/GetTimelineChartChargeSlip",
            type: "POST",
            data: JSON.stringify({ 'UserId': UserId, 'Year': Year, 'Duration': Duration }),
            contentType: "application/json;charset=utf-8",
            success: function (res) {
                var totCommAmountDue = 0, totPaidAmount = 0, totUnApplied = 0;
                $('#ContentMain_ucTimeline_grdChargeslipList').empty();

                $('#ContentMain_ucTimeline_grdChargeslipList').
                    append("<tr style='background-color:gainsboro;height:30px;'><th> Deal Name </th><th> Due Date </th><th>Net Amount Due </th><th> Paid Date </th><th>Net Paid Amount </th><th> Unapplied </th><th></th></tr>")
                for (var i = 0; i < res.d.length; i++) {
                    if (res.d[i].Duration == durationPeriod) {
                        $('#ContentMain_ucTimeline_grdChargeslipList')
                                                .append("<tr style='height:25px;'><td>" + res.d[i].DealName +
                                                        "</td><td style='text-align:center;width:11%;'>" + res.d[i].DueDateShort +
                                                        "</td><td style='text-align:right;width:12%;'>$" + parseFloat(res.d[i].CommAmountDue).toFixed(2) +
                                                        "</td><td style='text-align:center;width:11%'>" + res.d[i].PaymentDateShort +
                                                        "</td><td style='text-align:right;width:12%;'>$" + parseFloat(res.d[i].PaidAmount).toFixed(2) +
                                                        "</td><td style='text-align:right;width:12%;'>$" + parseFloat(res.d[i].UnApplied).toFixed(2) +
                                                        "</td><td style='width:3%;'><img src='../Images/Info-Card-Icon.gif' height='20px' width='20px' onclick=\"javascript:return Navigate('" + res.d[i].ChargeSlipId + "');\">" +
                                                        "</td></tr>");
                        totCommAmountDue += res.d[i].CommAmountDue;
                        totPaidAmount += res.d[i].PaidAmount;
                        totUnApplied += res.d[i].UnApplied;
                    }
                }
                $('#ContentMain_ucTimeline_grdChargeslipList')
                        .append("<tr style='background-color:gainsboro;height:30px;'><td><b> Total <b/></td> <td></td> <td style='text-align:right;'><b>$" + totCommAmountDue.toLocaleString('en') +
                        "<b/></td><td></td>><td style='text-align:right;'><b>$" + totPaidAmount.toLocaleString('en') +
                        "<b/></td><td style='text-align:right;'><b>$" + totUnApplied.toLocaleString('en') +
                        "<b/></td><td></td></tr>");

                $find("mpePopup").show();
            },
            fail: function (res) {
                alert("Unable to load data");
            }
        });
    }

    function Navigate(ChargeSlipId) {
        //var Index = 1;
        $.ajax({
            type: "POST",
            url: "Timeline.aspx/GetChargeSlipDetails",
            //data: '{RowIndx: "' + Index + '" }',
            data: '{ChargeSlipId: "' + ChargeSlipId + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                window.open("../Pages/NewCharge.aspx", "_self");
            },
            failure: function (response) {
                alert(response.d);
            }
        });

        return false;
    }

</script>

