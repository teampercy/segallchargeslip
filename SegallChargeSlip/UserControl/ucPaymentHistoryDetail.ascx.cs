﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class ucPaymentHistoryDetail : System.Web.UI.UserControl
{
    public int DealTransactionID
    {
        get;
        set;
    }
    public int ChargeSlipId
    {
        get;
        set;
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Bindata()
    {
        try
        {
            DealTransactions objDealTransactions = new DealTransactions();

            objDealTransactions.ChargeSlipID = ChargeSlipId;
            Session["ChargeSlipId"] = ChargeSlipId;
            DataSet ds = objDealTransactions.GetPaymentHistoryDetails();

            if (ds.Tables[0].Rows.Count > 0)
            {
              
                lblDealName.Text = ds.Tables[0].Rows[0]["DealName"].ToString();
            }

            FormView2.DataSource = ds.Tables[0];
            FormView2.DataBind();
            FormView3.DataSource = ds.Tables[0];
            FormView3.DataBind();
            if (ds.Tables[0].Rows.Count > 0)
            {
                HtmlTableRow trEmails = new HtmlTableRow();
                trEmails = (HtmlTableRow)FormView3.FindControl("trEmails");
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["Email"].ToString()))
                {
                    trEmails.Visible = true;
                }
                else
                {
                    trEmails.Visible = false;
                }
               
            }
            Label lblDueDates = (Label)FormView3.FindControl("lblDueDates");

            for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
            {
                lblDueDates.Text += " " + ds.Tables[1].Rows[i]["DueDate"].ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$" + String.Format("{0:n2}", Convert.ToDecimal(ds.Tables[1].Rows[i]["CommAmountDue"].ToString())) + "<br />";
            }
            if (ds.Tables[2].Rows.Count > 0)
            {
                gvPaymentHistory.DataSource = ds.Tables[2];
                gvPaymentHistory.DataBind();

                for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(ds.Tables[2].Rows[i]["MessageId"].ToString()))
                    {
                        ((Label)gvPaymentHistory.Rows[i].FindControl("lbldollars")).CssClass = "ApplyColor";
                        ((LinkButton)gvPaymentHistory.Rows[i].FindControl("lblAMOUNTPaid")).CssClass = "ApplyColor";
                        ((LinkButton)gvPaymentHistory.Rows[i].FindControl("lblAMOUNTPaid")).Enabled = true;
                    }
                    else
                    {
                        ((Label)gvPaymentHistory.Rows[i].FindControl("lbldollars")).CssClass = "ApplyWithoutColor";
                        ((LinkButton)gvPaymentHistory.Rows[i].FindControl("lblAMOUNTPaid")).CssClass = "ApplyWithoutColor";
                        ((LinkButton)gvPaymentHistory.Rows[i].FindControl("lblAMOUNTPaid")).Enabled = false;
                    }

                }
            }
            else
            {
                gvPaymentHistory.DataSource = null;
                gvPaymentHistory.DataBind();
            }
            if (Session["UserType"].ToString() != "1")
            {

                foreach (GridViewRow dr in gvPaymentHistory.Rows)
                {

                    dr.FindControl("imgEditbtnAmount").Visible = false;
                    dr.FindControl("imgSavebtnAmount").Visible = false;


                }
            }

            int row = 0;
            int total = ds.Tables[3].Rows.Count;
            if (ds.Tables[3].Rows.Count > 0)
            {
                while (row < total)
                {
                    UserControl_ucGridsPaymentHistory uc = LoadControl("~/UserControl/ucGridsPaymentHistory.ascx") as UserControl_ucGridsPaymentHistory; ;
                    uc.ID = "ucGridsPaymentHistory_" + row;
                    
                    //uc.i="Server";

                    DataTable dtControl = ds.Tables[3].Clone();

                    int BrokerId = Convert.ToInt32(ds.Tables[3].Rows[row]["BrokerId"]);
                    string BrokerType = Convert.ToString(ds.Tables[3].Rows[row]["BrokerRole"]);

                    while (BrokerId == Convert.ToInt32(ds.Tables[3].Rows[row]["BrokerId"]) && BrokerType==Convert.ToString(ds.Tables[3].Rows[row]["BrokerRole"]))
                    {

                        DataRow dr;
                        dr = dtControl.NewRow();
                        dr = (DataRow)ds.Tables[3].Rows[row];
                        dtControl.Rows.Add(dr.ItemArray);
                        row++;
                        if (row == total)
                            break;
                    }

                    uc.Bindata(dtControl);
                    if (Session["UserType"].ToString() != "1")
                    {
                        GridView gd = (GridView)uc.FindControl("gvPaymentDistribution");
                        foreach (GridViewRow dr in gd.Rows)
                        {

                            dr.FindControl("imgEditbtn").Visible = false;
                            dr.FindControl("imgSavebtn").Visible = false;
                            dr.FindControl("btnPaymentAmount").Visible = false;
                            //dr.FindControl("imgSavebtnBroker").Visible = false;

                        }
                    }
                    
                    

                    divDistribution.Controls.Add(uc);
                }
            }



            ScriptManager.RegisterStartupScript(this, Page.GetType(), "displayPayHistory", "displayPayHistory();", true);
            //  ScriptManager.RegisterStartupScript(this, Page.GetType(), "displayPayHistory", "displayPayHistory();", true);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucPaymentHistoryDetail.ascx", "Bindata", ex.Message);
        }
    }
    protected void imgClose_Click(object sender, ImageClickEventArgs e)
    {
        //this.Parent.FindControl("divChargeDetailCard").Visible = false;
        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HidePayHistory", "HidePayHistory();", true);
    }
    decimal totalGross = 0;
    protected void gvPaymentHistory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.Eval(e.Row.DataItem, "PaymentAmount") != DBNull.Value)
                    totalGross += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PaymentAmount"));
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbltotalAMOUNTPaid = (Label)e.Row.FindControl("lbltotalAMOUNTPaid");
                lbltotalAMOUNTPaid.Text = "$" + String.Format("{0:n2}", totalGross);
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucPaymentHistoryDetail.ascx", "gvPaymentHistory_RowDataBound", ex.Message);
        }
    }
}