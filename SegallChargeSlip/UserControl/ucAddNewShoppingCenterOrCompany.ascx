﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAddNewShoppingCenterOrCompany.ascx.cs" Inherits="UserControl_ucAddNewShoppingCenterOrCompany" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%--<script src="../Js/jquery-1.9.1.js" type="text/javascript"></script>--%>
<script type="text/javascript">
    function SelectSingleRadiobutton(rdbtnid) {
        var rdBtn = document.getElementById(rdbtnid);
        var rdBtnList = document.getElementsByTagName("input");
        for (i = 0; i < rdBtnList.length; i++) {
            if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                rdBtnList[i].checked = false;
            }
        }
        return false;
    }


    function SelectSingleRadiobuttonCompany(rdbtnid) {
        // .. get class compDupRbl
        var rdBtn = document.getElementById(rdbtnid);
        var rdBtnList = document.getElementsByTagName("input");
        for (i = 0; i < rdBtnList.length; i++) {
            if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                rdBtnList[i].checked = false;
            }
        }
        return false;
    } 
    function ValidateNumber(event) {

        var regex = new RegExp("^[0-9 \-]+$");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();

            return false;
        }
    }
    function CloseWindow() {
        alert("close");
        window.close();
        return true;
    }

    function CloseShoppingCenter() {
        alert("Closing");
        document.getElementById('ctl00$ContentMain$btnCancel').click();

        return false;
    }
    function validateZipLen(strzip) {
        var strziptrm = $.trim(strzip);
        if (strziptrm.length < 5 || strziptrm.length > 10) {
            return false;
        }
        return true;
    }
    function ValidEmail() {
        debugger;
        var txtemail = document.getElementById('<% =txtEmail.ClientID%>');
        var txtzipcode = document.getElementById('<% =txtZipCode.ClientID%>');
        if (txtemail.value != "") {
            if (IsValidEmailId(txtemail)) {
                return true;
            }
            else {
                document.getElementById('<%=lbErrorMsg.ClientID %>').innerHTML = "\t•Please enter valid email";
                txtemail.value = "";
                return false;
            }
               
        }
        if (txtzipcode.value != "")
        {
            if (!validateZipLen($.trim(txtzipcode.value))) {
                document.getElementById('<%=lbErrorMsg.ClientID %>').innerHTML =  "Please Enter between 5 and 10 digit Valid ZipCode";

                txtzipcode.focus();
                return false;
            }
        }
        else
            return true;
    }

</script>

<%--((UpdatePanel)(this.Parent.FindControl("upNewCharge"))).Update();--%>
<asp:UpdatePanel ID="upAddNewShoppingCenterOrCompany" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

        <table style="width: 100%" class="spacing" cellpadding="3">
            <%--   <tr style="background-color: gray;">
                <td colspan="2" style="text-align: left;">

                    <h3>
                        <asp:Label ID="lbTitle" runat="server"></asp:Label></h3>
                </td>
            </tr>--%>
            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>


            <!-- SK : 08/05 : Added Ownership Entity & Landlord Name   
                       -->
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <asp:Label runat="server" ID="lbErrorMsg" ForeColor="Red" Font-Bold="true" Font-Names="Vardena" Font-Size="Small" Text=""></asp:Label>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr id="trOwnershipEntity" runat="server">
                <td>&nbsp;</td>
                <td style="width: 40%" class="Padding">
                    <asp:Label ID="lblOwnershipEntity" runat="server">OWNERSHIP ENTITY</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtOwnershipEntity" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr id="trLandlordName" runat="server">
                <td>&nbsp;</td>
                <td style="width: 40%" class="Padding">
                    <asp:Label ID="lblLandlordName" runat="server">LANDLORD TRADE NAME</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtLandlordName" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>

            <!-- SK : 08/05 : Added Ownership Entity  & Landlord Name           -->



            <tr id="trName" runat="server">
                <td>&nbsp;</td>
                <td style="width: 40%" class="Padding">
                    <asp:Label ID="lbName" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtName" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>

            <tr id="trEmail" runat="server">
                <td>&nbsp;</td>
                <td style="width: 40%" class="Padding">
                    <asp:Label ID="lblEmail" runat="server">EMAIL</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>

            <tr id="trAddr" runat="server">
                <td>&nbsp;</td>
                <td style="width: 40%">ADDRESS LINE 1
                </td>
                <td>
                    <asp:TextBox ID="txtAddress1" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="width: 40%">ADDRESS LINE 2
                </td>
                <td>
                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="width: 40%">CITY
                </td>
                <td>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="modaltextboxsmall"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="width: 40%">COUNTY
                </td>
                <td>
                    <asp:TextBox ID="txtCountry" runat="server" CssClass="modaltextboxsmall"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="width: 40%">STATE
                </td>
                <td>
                    <asp:DropDownList ID="ddlState" runat="server" CssClass="modalddlSmall"></asp:DropDownList>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="width: 40%">ZIPCODE
                </td>
                <td>
                    <asp:TextBox ID="txtZipCode" runat="server" Width="160px" CssClass="modaltextboxsmall" onkeypress="return ValidateNumber(event);"></asp:TextBox>
                    <asp:Label ID="lbllon" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="lbllat" runat="server" Visible="false"></asp:Label>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr><td colspan="5" style="height:10px;"></td></tr>
            <tr>
                <td>&nbsp;</td>
                <td valign="top" style="width: 40%"></td>
                <td colspan="3">
                   <asp:Label ID="lblCompanyNotesModifiedby" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr runat="server" id="trAddNotes">
                <td>&nbsp;</td>
                <td valign="top" style="width: 40%">NOTES</td>
                <td colspan="3">
                    <asp:TextBox ID="txtCompanyNotes" runat="server" TextMode="MultiLine" Rows="12" Columns="35"></asp:TextBox>
                </td>

            </tr>
            <tr id="trIsActive" runat="server" visible="false">
                <td>&nbsp;</td>
                <td style="width: 40%">IsActive</td>
                <td>
                    <asp:CheckBox ID="chkIsActive" runat="server" />
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <%--onmouseover="this.style.backgroundColor='#E0E0E0';"
                        onmouseout="this.style.backgroundColor='#B8B8B8';"--%>
                <td style="text-align: right;" colspan="5">
                    <asp:Button ID="btnAddNew" runat="server" Text="ADD" OnClick="btnAdd_Click" CssClass="SqureGrayButtonSmall" OnClientClick="return ValidEmail();"/>
                    <asp:HiddenField ID="hdnClassRef" runat="server" />
                    <asp:Label ID="lbDummy1" runat="server"></asp:Label>
                    <ajax:ModalPopupExtender ID="mpDuplicateShoppingCenter" TargetControlID="lbDummy1" CancelControlID="btnCancelPopup"
                        PopupControlID="dvOnDuplication" runat="server" BackgroundCssClass="modalBackground">
                    </ajax:ModalPopupExtender>
                </td>
            </tr>
        </table>

        <div id="dvOnDuplication" class="PopupDivBodyStyle" style="height: auto;">
            <table>
                <tr align="right" style="background-color: gray;">
                    <td colspan="2">
                        <%-- <asp:Button ID="btnCancelPopup" Height="20px" Width="20px" Text="X" runat="server" />--%>
                        <asp:ImageButton ID="btnCancelPopup" Height="15px" Width="15px" ImageUrl="~/Images/close.png" runat="server" />
                    </td>
                </tr>
                <tr align="Center">
                    <td colspan="2">
                        <asp:Panel ScrollBars="Both" Width="400px" Height="150px" ID="pnGvDupLoc" runat="server">
                            <asp:UpdatePanel ID="upDupLoc" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="gvDupLocations" runat="server" AutoGenerateColumns="false" Width="99%" OnRowDataBound="gvDupLocations_RowDataBound">
                                        <HeaderStyle BackColor="#cccccc" ForeColor="Black" Font-Bold="true" />
                                        <Columns>
                                            <asp:BoundField DataField="LocationID" />
                                            <asp:BoundField DataField="Address" HeaderText="DB Address" />
                                            <%--      <asp:BoundField DataField="EnteredAddress" HeaderText="New Address" />--%>
                                            <asp:TemplateField HeaderText="Select">
                                                <%--   <HeaderTemplate>
                                    Select
                                </HeaderTemplate>--%>
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="rbSelLocForUpdate" runat="server"  OnClick="javascript:SelectSingleRadiobutton(this.id)" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        <asp:Label ID="lbAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="font-family: Verdana; font-size: smaller;">THE Location ADDRESS ENTERED DOES NOT MATCH
                <br />
                        THE ADDRESS IN THE DATABASE FOR "<b>
                            <asp:Label ID="lblShpName" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="LblActShpAddr" runat="server"></asp:Label>
                        </b>
                        <br />
                        " WOULD YOU LIKE TO UPDATE THE ADDRESS OR SAVE THIS AS<br />
                        A NEW Location?
                    </td>

                </tr>
                <tr align="right">
                    <td>
                        <asp:Button ID="btnupShoppingCenter" runat="server" Text="UPDATE" CssClass="SqureButton" BackColor="Gray" OnClick="btnUpdate_ShoppingCenter" Height="18px" />
                    </td>
                    <td>
                        <asp:Button ID="btnCreateNewShoppingCenter" runat="server" Text="SAVE AS NEW" CssClass="SqureButton" BackColor="Gray" OnClick="btnAddNew_ShoppingCenter" Height="18px" />
                    </td>
                </tr>
            </table>
        </div>

        <asp:Label ID="lbDummy2" runat="server"></asp:Label>
        <ajax:ModalPopupExtender ID="mpDuplicateCompany" TargetControlID="lbDummy2" CancelControlID="btnCancelPopup"
            PopupControlID="dvOnDuplicationCompany" runat="server" BackgroundCssClass="modalBackground">
        </ajax:ModalPopupExtender>

        <div id="dvOnDuplicationCompany" class="PopupDivBodyStyle" style="height: auto;">
            <table>
                <tr align="right" style="background-color: gray;">
                    <td colspan="2">
                        <%-- <asp:Button ID="btnCancelCompanyPopup" Height="20px" Width="20px" Text="X" runat="server" />--%>
                        <asp:ImageButton ID="btnCancelCompanyPopup" Height="15px" Width="15px" ImageUrl="~/Images/close.png" runat="server" />
                    </td>
                </tr>
                <tr align="Center">
                    <td colspan="2">
                        <asp:Panel ScrollBars="Both" Width="400px" Height="150px" ID="pnGvDupComp" runat="server">
                            <asp:UpdatePanel ID="upDupComp" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="gvDupCompanys" runat="server" AutoGenerateColumns="false" Width="99%" OnRowDataBound="gvDupCompanys_RowDataBound">
                                        <HeaderStyle BackColor="#cccccc" ForeColor="Black" Font-Bold="true" />
                                        <Columns>
                                            <asp:BoundField DataField="CompanyID" />
                                            <asp:BoundField DataField="Address" HeaderText="Address" />
                                            <asp:TemplateField HeaderText="Select">
                                                <%--   <HeaderTemplate>
                                    Select
                                </HeaderTemplate>--%>
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="rbSelCompForUpdate" runat="server" class="compDupRbl" OnClick="javascript:SelectSingleRadiobuttonCompany(this.id)" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        <asp:Label ID="lbAddressCompany" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="font-family: Verdana; font-size: smaller;">THE COMPANY ADDRESS ENTERED DOES NOT MATCH
                <br />
                        THE ADDRESS IN THE DATABASE FOR "
                        <b>
                            <asp:Label ID="lblCmpName" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lblactCmpAddr" runat="server"></asp:Label>

                        </b>

                        <br />
                        " WOULD YOU LIKE TO UPDATE THE ADDRESS OR SAVE THIS AS<br />
                        A COMPANY?
                    </td>

                </tr>
                <tr align="right">
                    <td>
                        <asp:Button ID="btnupCompany" runat="server" Text="UPDATE" CssClass="SqureButton" BackColor="Gray" OnClick="btnUpdate_Company" Height="18px" />
                    </td>
                    <td>
                        <asp:Button ID="btnCreateNewCompany" runat="server" Text="SAVE AS NEW" CssClass="SqureButton" BackColor="Gray" OnClick="btnAddNew_Company" Height="18px" />
                    </td>
                </tr>
            </table>
        </div>

    </ContentTemplate>
</asp:UpdatePanel>
