﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SegallChargeSlipBll;

public partial class UserControl_ucCommissionSplitCalculator : System.Web.UI.UserControl
{
    #region Variables
    DataTable dt;
    DataRow dRow;
    decimal Co_BrokerPer, Co_BrokerAmt;

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Utility.FillStates(ref ddlState);
                Utility.FillStates(ref ddlEditState); // Binding Edit View State ddl
                // Utility.FillBrokerDropDownFromUserCompany(ref ddlBrokers);
                SetCommissionTableName();
            }
            SetAttributes();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucCommissionSplitCalculator.ascx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #region CustomActions

    public void SetCommissionTableName()
    {
        try
        {
            if (Session[GlobleData.NewChargeSlipObj] != null)
            {
                ChargeSlip objChargeslip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
                if (objChargeslip.DealDesignationTypesId == 1)
                {
                    //CoBrokerText.InnerText = "SG-LANDLORD";
                    trCLBBrokerSection.Style.Add("display", "table-row");
                }
                else if (objChargeslip.DealDesignationTypesId == 2)
                {
                    //CoBrokerText.InnerText = "SG-TENANT";
                    trCoBrokerSection.Style.Add("display", "table-row");
                }
                else if (objChargeslip.DealDesignationTypesId == 3)
                {
                    //CLBBrokerText.InnerText = "SG-LANDLORD";
                    //CoBrokerText.InnerText = "SG-TENANT";
                    trCLBBrokerSection.Style.Add("display", "table-row");
                    trCoBrokerSection.Style.Add("display", "table-row");
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucCommissionSplitCalculator.ascx", "SetCommissionTableName", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void SaveAllBrokerCommission(int TermType, decimal CommAmount)
    {
        try
        {
            Users objUsers = new Users();
            objUsers.UserID = Convert.ToInt32(Session[GlobleData.User]);
            objUsers.Load();
            int type = objUsers.UserTypeID;
            if (type != 1)
            {
                UserDetails objUserDetails = new UserDetails();
                objUserDetails.UserID = Convert.ToInt32(Session[GlobleData.User]);
                Boolean result = objUserDetails.LoadByUserIdAndCurrentDate(Convert.ToInt64(HttpContext.Current.Session[GlobleData.NewChargeSlipId].ToString()));//CommAmount
                if (result == true)
                {
                    Co_BrokerPer = objUserDetails.PercentBroker;
                    //decimal Commission = Convert.ToDecimal(Session[GlobleData.cns_totBaseLeaseComm]);
                    decimal Commission = CommAmount;
                    // decimal Commission = 2000;
                    Co_BrokerAmt = Math.Round((Commission * Co_BrokerPer) / 100);
                }
                hdnTotalComm.Value = Co_BrokerAmt.ToString();
                hdnCommPerForCoBrokerInit.Value = Co_BrokerPer.ToString();

                //Insert Record In Deal Broker Commission

                DealBrokerCommissions objDealBrokerCommissions = new DealBrokerCommissions();
                // objDealBrokerCommissions.BrokerCommission = Co_BrokerAmt;
                // objDealBrokerCommissions.BrokerPercent = Convert.ToDecimal(Co_BrokerPer);
                objDealBrokerCommissions.BrokerCommission = CommAmount;
                objDealBrokerCommissions.BrokerPercent = Convert.ToDecimal(100);
                objDealBrokerCommissions.BrokerId = objUsers.UserID;
                objDealBrokerCommissions.DealTransactionId = Convert.ToInt32(Session[GlobleData.DealTransactionId]);
                objDealBrokerCommissions.ChargeSlipId = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                objDealBrokerCommissions.TermTypeId = Convert.ToInt16(TermType);
                objDealBrokerCommissions.BrokerTypeId = Convert.ToInt16(4);
                string tableType = "tblCo_Broker";
                if (Session[GlobleData.NewChargeSlipObj] != null)
                {
                    ChargeSlip objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
                    if (objChargeSlip.DealDesignationTypesId == 1)
                    {
                        objDealBrokerCommissions.BrokerTypeId = Convert.ToInt16(5);
                        tableType = "tblClb_Broker";
                    }
                    else
                    {
                        tableType = "tblCo_Broker";
                    }
                }
                objDealBrokerCommissions.Save();

                string UserName = objUsers.FirstName + " " + objUsers.LastName;

                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Script" + TermType, "javascript:appendExternalBrokerFromPopup('" + UserName + "','" + 100 + "','" + 0 + "','tblCo_Broker','" + Convert.ToInt32(Session[GlobleData.User]) + "','" + this.ClientID + "','" + Convert.ToInt32(objDealBrokerCommissions.DealBrokerCommissionId) + "');", true);//ContentMain_CommCalculator
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Script" + TermType, "javascript:appendExternalBrokerFromPopup('" + UserName + "','" + 100 + "','" + 0 + "','" + tableType + "','" + Convert.ToInt32(Session[GlobleData.User]) + "','" + this.ClientID + "','" + Convert.ToInt32(objDealBrokerCommissions.DealBrokerCommissionId) + "');", true);//ContentMain_CommCalculator

                string UserCommission, CompCommission;
                UserCommission = ((objDealBrokerCommissions.BrokerCommission * objUserDetails.PercentBroker) / 100).ToString("#.##");
                CompCommission = ((objDealBrokerCommissions.BrokerCommission * objUserDetails.PercentCompany) / 100).ToString("#.##");

                if (TermType == 1)
                {
                    string tblBrokerType;
                    if (objDealBrokerCommissions.BrokerTypeId == Convert.ToInt16(5))
                    {
                        tblBrokerType = "tblClb_Broker";
                    }
                    else
                    {
                        tblBrokerType = "";
                    }
                    Page.ClientScript.RegisterStartupScript(GetType(), "FirstSummary" + 1, "javascript:GenerateCompSplitSummary('" + objUserDetails.PercentBroker.ToString() + "','" + objUserDetails.PercentCompany.ToString() + "','" + HttpContext.Current.Session["User"].ToString() + "','" + objUserDetails.CompanyName + "','" + UserCommission + "','" + CompCommission + "','" + tblBrokerType + "');", true);
                }
                //


                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Script", "javascript:appendExternalBrokerFromPopup('" + objUsers.Username + "','" + Co_BrokerPer + "','" + Co_BrokerAmt + "','tblCo_Broker','" + Convert.ToInt32(Session[GlobleData.User]) + "','ContentMain_CommCalculator','" + Convert.ToInt32(objDealBrokerCommissions.DealBrokerCommissionId) + "');", true);
                //   Page.ClientScript.RegisterStartupScript(this.GetType(), "Script", "javascript:appendExternalBrokerFromPopup('" + objUsers.Username + "','" + 0 + "','" + 0.00 + "','tblCo_Broker','" + Convert.ToInt32(Session[GlobleData.User]) + "','ContentMain_CommCalculator');", true);
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucCommissionSplitCalculator.ascx", "SaveAllBrokerCommission", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
        // not use Page.ClientScript.RegisterStartupScript(this.GetType(), "Script_1", "SaveAllBrokersDetail('" + this.ClientID + "');", true);
    }

    #endregion

    public void SaveBroker()
    {
        try
        {
            Brokers objBrokers = new Brokers();
            objBrokers.CompanyName = txtCompany.Text;
            objBrokers.Address = txtAddress.Text;
            objBrokers.City = txtCity.Text;
            objBrokers.State = Convert.ToString(ddlState.SelectedIndex);
            objBrokers.Zipcode = txtZipCode.Text;

            objBrokers.EIN = txtEin.Text;
            objBrokers.Email = txtEmail.Text;
            objBrokers.Name = txtName.Text;
            objBrokers.BrokerTypeId = 1;
            objBrokers.SharePercent = Convert.ToDouble(txtShare.Text);
            objBrokers.SetAmount = Convert.ToDecimal(txtSetAmount.Text);
            objBrokers.Save();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucCommissionSplitCalculator.ascx", "SaveBroker", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    private void ResetAllControls()
    {
        txtCompany.Text = "";
        txtAddress.Text = "";
        txtCity.Text = "";
        txtEin.Text = "";
        txtEmail.Text = "";
        txtName.Text = "";
        txtSetAmount.Text = "";
        txtShare.Text = "";
        txtZipCode.Text = "";
        SetAttributes();
        Utility.FillStates(ref ddlState);
    }
    private void SetAttributes()
    {
        try
        {
            //txtZipCode.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtZipCode.ClientID + "')");
            txtShare.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtShare.ClientID + "')");
            txtSetAmount.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtSetAmount.ClientID + "')");

            txtEditSharePer.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtEditSharePer.ClientID + "')");
            txtEditShareAmt.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtEditShareAmt.ClientID + "')");

            //txtEmail.Attributes.Add("onkeyup", "javascript:alert('hi');");
            txtEmail.Attributes.Add("onblur", "javascript:return EmailValidation('" + txtEmail.ClientID + "')");
            txtCompany.Attributes.Add("onkeyup", "javascript:return SetContextKey('" + this.ClientID + "')");
            btnAddBroker.Attributes.Add("onclick", "javascript:return btnAddExternalBroker_Save('" + this.ClientID + "')");
            ///btnDueDtAdd.Attributes.Add("onclick", "javascript:return addDueDateRecords('" + btnDueDates.ClientID + "','" + pnDueDt.ClientID + "','" + this.ID + "')");
            txtShare.Attributes.Add("onchange", "javascript:return CalBrokerwiseCommAmount('ADD','" + this.ClientID + "')");
            txtSetAmount.Attributes.Add("onchange", "javascript:return CalBrokerwiseCommPercentage('ADD','" + this.ClientID + "')");
            txtEditSharePer.Attributes.Add("onchange", "javascript:return CalBrokerwiseCommAmount('EDIT','" + this.ClientID + "')");
            txtEditShareAmt.Attributes.Add("onchange", "javascript:return CalBrokerwiseCommPercentage('EDIT','" + this.ClientID + "')");
            ddlBrokers.Attributes.Add("onchange", "javascript:return SetCoBrokerDetails('" + this.ClientID + "')");
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucCommissionSplitCalculator.ascx", "SetAttributes", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public Boolean GetCommissionSpilCalRecordsForSelChargeSlip(string ucName, string BaseCommAmt)
    {
        try
        {
            if (Session[GlobleData.NewChargeSlipId] != null)
            {
                int termTyp = 0;
                ucName = ucName.Replace("ContentMain_", "");
                if (ucName == "CommCalculator")
                {
                    termTyp = 1;
                }
                else if (ucName == "OptCommCalculator")
                {
                    termTyp = 2;
                }
                else if (ucName == "ContingentCommCalculator")
                {
                    termTyp = 3;
                }
                else if (ucName == "Opt1CommCalculator")
                {
                    termTyp = 4;
                }
                else if (ucName == "Opt2CommCalculator")
                {
                    termTyp = 5;
                }
                else if (ucName == "Opt3CommCalculator")
                {
                    termTyp = 6;
                }

                DealBrokerCommissions objDealBrokerCommissions = new DealBrokerCommissions();
                objDealBrokerCommissions.ChargeSlipId = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                objDealBrokerCommissions.TermTypeId = Convert.ToInt32(termTyp);

                objDealBrokerCommissions.DeleteDealBrokerCommissionsAsPerLeaseTerms();

                //Set Data as per Change in Commission
                if (BaseCommAmt == "")
                {
                    BaseCommAmt = "0";
                }

                objDealBrokerCommissions.SetCommAmountAsPerChangeInBaseAmt(Convert.ToDecimal(BaseCommAmt));

                DataSet ds = objDealBrokerCommissions.GetListByChargeSlip();
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        int i;
                        int rndNo;
                        Random rnd = new Random();
                        int x = rnd.Next(100);


                        for (i = 0; i < dt.Rows.Count; i++)
                        {
                            string DisplayName;
                            if (dt.Rows[i]["tableRef"].ToString() == "tblCo_Broker" || dt.Rows[i]["tableRef"].ToString() == "tblClb_Broker")
                            {
                                DisplayName = dt.Rows[i]["Broker"].ToString();
                            }
                            else
                            {
                                DisplayName = dt.Rows[i]["CompanyName"].ToString();
                            }
                            //DisplayName = System.Web.HttpUtility.HtmlEncode(DisplayName);
                            rndNo = termTyp * (x + i);
                            Page.ClientScript.RegisterStartupScript(GetType(), "myScript" + rndNo, "javascript:appendExternalBrokerFromPopup('" + DisplayName.Replace("'", "#") + "','" + dt.Rows[i]["BrokerPercent"].ToString() + "','" + dt.Rows[i]["BrokerCommission"].ToString() + "','" + dt.Rows[i]["tableRef"].ToString() + "','" + dt.Rows[i]["BrokerId"].ToString() + "','" + this.ClientID + "','" + dt.Rows[i]["DealBrokerCommissionId"].ToString() + "');", true);
                            // Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + rndNo, "javascript:SetCommAmoutForEachBrokerType('" + dt.Rows[i]["tableRef"].ToString() + "','true','" + this.ClientID + "');", true);
                            //Get DealBrokercommissionId For Company
                            Boolean IsAdmin = false;
                            if (Convert.ToInt16(Session["UserType"].ToString()) == 1)
                            {
                                IsAdmin = true;
                            }

                            //if (termTyp == 1 || termTyp == 2 || termTyp == 4 || termTyp == 5 || termTyp == 6) //added New term type 4,5,6 for Option Term 1,2,3 because Option Term type was available  
                            if (termTyp == 1)
                            {
                                if (dt.Rows[i]["UserName"].ToString() == Session["User"].ToString() || IsAdmin == true)
                                {
                                    if (IsAdmin == true && dt.Rows[i]["BrokerTypeId"].ToString() == "4")
                                    {
                                        objDealBrokerCommissions.DealBrokerCommissionId = Convert.ToInt32(dt.Rows[i]["DealBrokerCommissionId"].ToString());
                                    }
                                    else
                                        objDealBrokerCommissions.DealBrokerCommissionId = Convert.ToInt32(dt.Rows[i]["DealBrokerCommissionId"].ToString());
                                    objDealBrokerCommissions.Load();

                                    //Set Summary Company Split
                                    UserDetails objUserDetails = new UserDetails();
                                    objUserDetails.UserID = objDealBrokerCommissions.BrokerId;
                                    // objUserDetails.UserID = Convert.ToInt32(HttpContext.Current.Session[GlobleData.User]);
                                    //objUserDetails.LoadByUserIdAndCurrentDate(Convert.ToInt64(HttpContext.Current.Session[GlobleData.NewChargeSlipId].ToString()));//objDealBrokerCommissions.BrokerCommission
                                    objUserDetails.LoadByUserIdAndCurrentDate(objDealBrokerCommissions.ChargeSlipId);
                                    string UserCommission, CompCommission;
                                    UserCommission = ((objDealBrokerCommissions.BrokerCommission * objUserDetails.PercentBroker) / 100).ToString("#.##");
                                    CompCommission = ((objDealBrokerCommissions.BrokerCommission * objUserDetails.PercentCompany) / 100).ToString("#.##");
                                    // string Threshold = objUserDetails.PercentBroker + " : " + objUserDetails.PercentCompany;

                                    if (UserCommission != "" || CompCommission != "")
                                    {
                                        string tblBrokerType;
                                        if (dt.Rows[i]["BrokerTypeId"].ToString() == "5")
                                        {
                                            tblBrokerType = "tblClb_Broker";
                                        }
                                        else
                                        {
                                            tblBrokerType = "";
                                        }
                                        Page.ClientScript.RegisterStartupScript(GetType(), "Summary" + rndNo, "javascript:GenerateCompSplitSummary('" + objUserDetails.PercentBroker.ToString() + "','" + objUserDetails.PercentCompany.ToString() + "','" + dt.Rows[i]["Broker"].ToString() + "','" + objUserDetails.CompanyName + "','" + UserCommission + "','" + CompCommission + "','" + tblBrokerType + "');", true);
                                    }
                                }
                            }
                        }
                        DataRow[] foundCoClbBroker = dt.Select("BrokerTypeId='4' OR BrokerTypeId='5'");
                        if (foundCoClbBroker.Length!=0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    }
                }
            }
            return false;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucCommissionSplitCalculator.ascx", "GetCommissionSpilCalRecordsForSelChargeSlip", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return false;
        }
    }

    public void SetAfterPageLoad()
    {
        try
        {
            DealBrokerCommissions objDealBrokerCommissions = new DealBrokerCommissions();
            objDealBrokerCommissions.ChargeSlipId = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
            DataSet ds = objDealBrokerCommissions.GetListByChargeSlip();
            if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    int i;
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + i, "javascript:SetCommAmoutForEachBrokerType('" + dt.Rows[i]["tableRef"].ToString() + "','true','" + this.ClientID + "');", true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucCommissionSplitCalculator.ascx", "SetAfterPageLoad", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #region ButtonEvents

    protected void btnAddBroker_Click(object sender, EventArgs e)
    {
        try
        {
            SaveBroker();
            ResetAllControls();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucCommissionSplitCalculator.ascx", "btnAddBroker_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #endregion






}