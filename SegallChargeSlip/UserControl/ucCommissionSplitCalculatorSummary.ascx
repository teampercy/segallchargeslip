﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCommissionSplitCalculatorSummary.ascx.cs" Inherits="UserControl_ucCommissionSplitCalculatorSummary" %>

<style type="text/css">
    .tblfixayout {
        width: 100%;
        color: black;
        table-layout: fixed;
        padding: 0px 0px 0px 0px;
        border-spacing: 0;
        text-align: center;
        /*cellspacing:0px;
        cellpadding:opx;*/
    }

    /*.tblfixayout tr td {
         height:8px;
        }*/
    .tbOnverFlow {
        overflow: hidden;
        width: 50px;
        text-align:left;
        padding: 0px 0px 0px 0px;
    }

    .tbNormal {
        overflow: hidden;
        width: 50px;
        padding: 0px 0px 0px 0px;
        white-space: nowrap;
        text-align:left;
    }

    .tdmain {
        width: 120px;
        text-align: left;
        /*text-wrap: initial;*/
    }

    .tdspceing {
        width: 20px;
        text-align: right;
    }

    .tdNuneric {
        width: 70px;
        text-align: right;
    }
</style>
<table style="width: 100%; font-size: smaller;" class="ucbackgroundborder">
    <%-- height: 100%; --%>
    <%--<tr>
        <td colspan="3">&nbsp;</td>
    </tr>--%>
    <tr>
        <td class="ucheadings">SUMMARY
                <hr style="width: 100%" />
        </td>
    </tr>
    <tr>

        <td>
            <%--    <asp:UpdatePanel ID="upSummary" runat="server" UpdateMode="Conditional">
                <ContentTemplate>--%>

            <div id="dvMainSumm" style="overflow-y: scroll; margin: 0px 10px 0px 10px; background-color: white;">


                <table style="width: 100%; background-color: white; padding-top: 5px; font-size: x-small;" cellpadding="0" cellspacing="0" id="tbCommsionSummary">
                    <tr id="trbrokersummary">
                        <td style="height: 10px;">

                            <div id="dvcommissionSummary">
                                 <table id="tbBrokersummary" runat="server" class="tblfixayout">
                                    <tr>
                                        <td class="tdmain"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                    </tr>
                                </table>

<%--                                <asp:Table ID="tbBrokersummary" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>--%>
                            </div>
                        </td>
                    </tr>
                    <tr id="troptionsummary">
                        <td style="height: 10px;">
                            <div id="Div1">
                                   <table id="tbOptionsummarry" runat="server" class="tblfixayout">
                                    <tr>
                                        <td class="tdmain"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                    </tr>

                                </table>
                             <%--   <asp:Table ID="tbOptionsummarry" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>--%>
                            </div>
                        </td>
                    </tr>
                    <tr id="trcontingentsummary">
                        <td style="height: 10px;">
                            <div id="div3">

                                <table id="tbcontingent" runat="server" class="tblfixayout">
                                    <tr>
                                        <td class="tdmain"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                    </tr>
                                </table>


                                <%--  <asp:Table ID="tbcontingent" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>--%>
                            </div>
                        </td>
                    </tr>
                    <tr id="troptionsummary1">
                        <td style="height: 10px;">
                            <div id="Div4">
                                   <table id="tbOptionsummarry1" runat="server" class="tblfixayout">
                                    <tr>
                                        <td class="tdmain"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                    </tr>
                                </table>                             
                            </div>
                        </td>
                    </tr>
                    <tr id="troptionsummary2">
                        <td style="height: 10px;">
                            <div id="Div5">
                                   <table id="tbOptionsummarry2" runat="server" class="tblfixayout">
                                    <tr>
                                        <td class="tdmain"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                    </tr>

                                </table>                             
                            </div>
                        </td>
                    </tr>
                    <tr id="troptionsummary3">
                        <td style="height: 10px;">
                            <div id="Div6">
                                   <table id="tbOptionsummarry3" runat="server" class="tblfixayout">
                                    <tr>
                                        <td class="tdmain"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                    </tr>

                                </table>                             
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr style="width:95%;" />
                        </td>
                    </tr>
                    <tr id="trtotbroker">
                        <td>

                            <div id="divtotalbroker">

                                <table id="tbtotalbroker" runat="server" class="tblfixayout">
                                    <tr>
                                        <td class="tdmain"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                    </tr>

                                </table>
                                <%-- <asp:Table ID="tbtotalbroker" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>--%>
                            </div>
                        </td>
                    </tr>
                    <tr id="trtotopt">
                        <td>
                            <div id="div2">
                                <table id="tbtotalopt" runat="server" class="tblfixayout">
                                    <tr>
                                        <td class="tdmain"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                    </tr>
                                </table>
                                <%-- <asp:Table ID="tbtotalopt" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>--%>
                            </div>
                        </td>
                    </tr>
                    <tr id="trtotcontingent">
                        <td>
                            <div id="div4">
                                <table id="tbtotalcontingent" runat="server" class="tblfixayout">
                                    <tr>
                                        <td class="tdmain"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                    </tr>
                                </table>
                                <%-- <asp:Table ID="tbtotalcontingent" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>--%>
                            </div>
                        </td>
                    </tr>
                    <tr id="trtotopt1">
                        <td>
                            <div id="div5">
                                <table id="tbtotalopt1" runat="server" class="tblfixayout">
                                    <tr>
                                        <td class="tdmain"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                    </tr>
                                </table>                               
                            </div>
                        </td>
                    </tr>
                    <tr id="trtotopt2">
                        <td>
                            <div id="div6">
                                <table id="tbtotalopt2" runat="server" class="tblfixayout">
                                    <tr>
                                        <td class="tdmain"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                    </tr>
                                </table>                               
                            </div>
                        </td>
                    </tr>
                    <tr id="trtotopt3">
                        <td>
                            <div id="div7">
                                <table id="tbtotalopt3" runat="server" class="tblfixayout">
                                    <tr>
                                        <td class="tdmain"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                        <td class="tdspceing"></td>
                                        <td class="tdNuneric"></td>
                                    </tr>
                                </table>                                
                            </div>
                        </td>
                    </tr>
                    <%--<tr>
                            <td> &nbsp;</td>
                            <td id="totalpercent"> </td>
                            <td id="total"></td>
                        </tr>--%>
                    <tr>
                        <td style="height: 20px;">
                            <br />
                            &nbsp;
                        </td>
                    </tr>
                </table>


            </div>
            <%--  </ContentTemplate>
            </asp:UpdatePanel>--%>
        </td>

    </tr>
    <tr>
        <td style="height: 20px;">
            <br />
            &nbsp;
        </td>
    </tr>

</table>
