﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SegallChargeSlipBll;

public partial class UserControl_ucGridsPaymentHistory : System.Web.UI.UserControl
{
    public int DealTransactionID
    {
        get;
        set;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Bindata(DataTable dt)
     {
        try
        {

            if (dt.Rows.Count > 0)
            {

                decimal TotalAmountDue = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TotalAmountDue = TotalAmountDue + Convert.ToDecimal(dt.Rows[i]["AmountDue"].ToString());
                }
                lblCommissionAmount.Text = "$" + String.Format("{0:n2}", TotalAmountDue);
                lblUserName.Text = dt.Rows[0]["BrokerName"].ToString();
                if (dt.Rows[0]["BrokerRole"].ToString() == "CB" || dt.Rows[0]["BrokerRole"].ToString()=="CLB")
                {
                    //gvPaymentDistribution.Columns[4].Visible = true;
                    //gvPaymentDistribution.Columns[5].Visible = true;
                }
                else
                {
                    gvPaymentDistribution.Columns[4].Visible = false;
                    gvPaymentDistribution.Columns[5].Visible = false;
                }
                if (Session["UserType"].ToString() == "1")
                {
                    gvPaymentDistribution.DataSource = dt;
                    gvPaymentDistribution.DataBind();
                    tblPaymentDistribution.Visible = true;
                }
                else
                {
                    if (Convert.ToInt32(dt.Rows[0]["BrokerId"].ToString()) == Convert.ToInt32(Session["UserID"].ToString()))
                    {
                        gvPaymentDistribution.DataSource = dt;
                        gvPaymentDistribution.DataBind();
                        tblPaymentDistribution.Visible = true;
                    }
                    if (dt.Rows[0]["BrokerRole"].ToString() == "CB" || dt.Rows[0]["BrokerRole"].ToString()=="CLB")
                    {                        
                        //tblPaymentDistribution.Visible = false;
                    }
                    else
                    {
                        gvPaymentDistribution.DataSource = dt;
                        gvPaymentDistribution.DataBind();
                        tblPaymentDistribution.Visible = true;
                    }


                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["IsBrokerPaymentEdited"].ToString()== "True")
                    {
                        ((Label)gvPaymentDistribution.Rows[i].FindControl("lbldollar")).CssClass = "ApplyColor";
                        ((LinkButton)gvPaymentDistribution.Rows[i].FindControl("lnkAmountDue")).CssClass = "ApplyColor";
                        ((LinkButton)gvPaymentDistribution.Rows[i].FindControl("lnkAmountDue")).Enabled = true;
                    }
                    else
                    {
                        ((Label)gvPaymentDistribution.Rows[i].FindControl("lbldollar")).CssClass = "ApplyWithoutColor";
                        ((LinkButton)gvPaymentDistribution.Rows[i].FindControl("lnkAmountDue")).CssClass = "ApplyWithoutColor";
                        ((LinkButton)gvPaymentDistribution.Rows[i].FindControl("lnkAmountDue")).Enabled = false;
                    }

                }




            }
            else
            {
                tblPaymentDistribution.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucGridsPaymentHistory.ascx", "Bindata", ex.Message);
        }
    }
    decimal totalAmountDue = 0, totalPaymentReceived = 0, totalCheckAmount = 0, totalPaymentReceivedVal = 0;
    //protected void gvPaymentDistribution_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        if (DataBinder.Eval(e.Row.DataItem, "PaymentReceived") != DBNull.Value)
    //            totalPaymentReceived += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "PaymentReceived"));
    //        if (DataBinder.Eval(e.Row.DataItem, "AmountDue") != DBNull.Value)
    //            totalAmountDue += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "AmountDue"));


    //    }
    //    if (e.Row.RowType == DataControlRowType.Footer)
    //    {
    //        Label lbltotalCHECKAMT = (Label)e.Row.FindControl("lbltotalCHECKAMT");
    //        lbltotalCHECKAMT.Text = "$" + totalPaymentReceived.ToString();
    //        Label lbltotalAMOUNTDUE = (Label)e.Row.FindControl("lbltotalAMOUNTDUE");
    //        lbltotalAMOUNTDUE.Text = "$" + totalAmountDue.ToString();
    //    }
    //}
    protected void gvPaymentDistribution_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                //-----------vv------ SK : 12/03/2013 : added 'PaymentReceivedVal' to resolve <br> split value payment issue --------
                if (DataBinder.Eval(e.Row.DataItem, "PaymentReceivedVal") != DBNull.Value)
                    totalPaymentReceivedVal += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PaymentReceivedVal"));

                //if (DataBinder.Eval(e.Row.DataItem, "PaymentReceived") != DBNull.Value)
                //    totalPaymentReceived += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PaymentReceived"));

                //-----------^^------ SK : 12/03/2013 : added to resolve <br> split value payment issue --------

                if (DataBinder.Eval(e.Row.DataItem, "AmountDue") != DBNull.Value)
                    totalAmountDue += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmountDue"));
                if (DataBinder.Eval(e.Row.DataItem, "CheckAmount") != DBNull.Value)
                    totalCheckAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CheckAmount"));

                //-----------vv------ SK : 12/03/2013 :removed as Split value is calculated directly form sp --------
                //decimal percent = Convert.ToDecimal(((Label)e.Row.FindControl("lblSplit")).Text);
                //decimal remainingpercent = 100 - percent;
                //((Label)e.Row.FindControl("lblSplit")).Text = Convert.ToInt32(percent) + ":" + Convert.ToInt32(remainingpercent);
                //-----------^^------ SK : 12/03/2013 :------------------------------------


                //int percent = Convert.ToInt32(((Label)e.Row.FindControl("lblSplit")).Text);
                //int remainingpercent = 100 - percent;
                //((Label)e.Row.FindControl("lblSplit")).Text = percent + ":" + remainingpercent;


            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbltotalCHECKAMT = (Label)e.Row.FindControl("lbltotalCHECKAMT");
                lbltotalCHECKAMT.Text = "$" + String.Format("{0:n2}", totalCheckAmount);
                Label lbltotalAMOUNTDUE = (Label)e.Row.FindControl("lbltotalAMOUNTDUE");
                lbltotalAMOUNTDUE.Text = "$" + String.Format("{0:n2}", totalAmountDue);

                //-----------vv------ SK : 12/03/2013 : added 'PaymentReceivedVal' to resolve <br> split value payment issue --------

                // Label lblPaymentReceived = (Label)e.Row.FindControl("lblPaymentReceived");
                // lblPaymentReceived.Text = "$" + String.Format("{0:n2}", totalPaymentReceived);

                Label lblPaymentReceived = (Label)e.Row.FindControl("lblPaymentReceived");
                lblPaymentReceived.Text = "$" + String.Format("{0:n2}", totalPaymentReceivedVal);


                //Label lbltotalShare = (Label)e.Row.FindControl("lbltotalShare");
                //lbltotalShare.Text = "$" + String.Format("{0:n2}", totalPaymentReceived);

                Label lbltotalShare = (Label)e.Row.FindControl("lbltotalShare");
                lbltotalShare.Text = "$" + String.Format("{0:n2}", totalPaymentReceivedVal);

                //-----------^^------ SK : 12/03/2013 : added to resolve <br> split value payment issue --------


            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucGridsPaymentHistory.ascx", "gvPaymentDistribution_RowDataBound", ex.Message);
        }
    }

}