﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;

public partial class UserControl_ucAddNewBroker : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ResetAllControls();

            btnAddBroker.Attributes.Add("onclick", "javascript:return addDueDateRecords()");
        }
    }

    #region CustomActions

    private void ResetAllControls()
    {
        txtCompany.Text = "";
        txtAddress.Text = "";
        txtCity.Text = "";
        txtEin.Text = "";
        txtEmail.Text = "";
        txtName.Text = "";
        txtSetAmount.Text = "";
        txtShare.Text = "";
        txtZipCode.Text = "";
        Utility.FillStates(ref ddlState);
    }

    private void SaveBroker()
    {
        Brokers objBrokers = new Brokers();
        objBrokers.CompanyName = txtCompany.Text;
        objBrokers.Address = txtAddress.Text;
        objBrokers.City = txtCity.Text;
        objBrokers.State = Convert.ToString(ddlState.SelectedIndex);
        objBrokers.Zipcode = txtZipCode.Text;
        objBrokers.EIN = txtEin.Text;
        objBrokers.Email = txtEmail.Text;
        objBrokers.Name = txtName.Text;
        objBrokers.BrokerTypeId = 1;
        objBrokers.SharePercent = Convert.ToDouble(txtShare.Text);
        objBrokers.SetAmount = Convert.ToDecimal(txtSetAmount.Text);
        objBrokers.Save();
    }

    #endregion

    #region ButtonEvents

    protected void btnAddBroker_Click(object sender, EventArgs e)
    {
        SaveBroker();
        ResetAllControls();
    }

    #endregion

    protected void txtCompany_TextChanged(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(hdnBrokerId.Value))
            {
                Brokers objBrokers = new Brokers();
                objBrokers.BrokerId = Convert.ToInt32(hdnBrokerId.Value);
                objBrokers.Load();
                txtCompany.Text = objBrokers.CompanyName;
                txtAddress.Text = objBrokers.Address;
                txtCity.Text = objBrokers.City;
                ddlState.SelectedIndex =Convert.ToInt32(objBrokers.State);
                txtZipCode.Text = objBrokers.Zipcode;
                txtEin.Text = objBrokers.EIN;
                txtEmail.Text = objBrokers.Email;
                txtName.Text = objBrokers.Name;
                txtShare.Text = Convert.ToString(objBrokers.SharePercent);
                txtSetAmount.Text = Convert.ToString(objBrokers.SetAmount);
            }
        }
        catch { }
    }
}