﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucPaymentHistoryDetail.ascx.cs" Inherits="ucPaymentHistoryDetail" %>
<%@ Reference Control="~/UserControl/ucGridsPaymentHistory.ascx" %>
<script type="text/javascript">
    //function PaymentPDFOpen(id) {
    //    alert('hi');
    //    alert(id);
    //    alert('hhh');
    //    $.ajax({
    //        async: true,
    //        type: "POST",
    //        url: "~/Payments.aspx/PaymentPDFOpen",
    //        data: '{Paymentd:"' + id + '"}',
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json",
    //        success: function (response) {
    //            //alert('p');
    //            if (response.d == "") {
    //                debugger;
    //                alert(response.d);

    //            }
    //            else {

    //                alert(response.d);
    //                return false;

    //            }
    //        },
    //        failure: function (response) {

    //            alert(response.d);
    //            return false;
    //        }
    //    });
    //    return false;
    //}
    function PaymentPDFOpen(filepath)
    {
       // alert(filepath.trim());
        if (filepath.trim() != "") {
            window.open("../PaymentDocument/" + filepath, "_blank");
        }
        return false;
    }
    function checkPaymentReason() {

        var reason = $('#<%=txtReasonEdit.ClientID %>').val();
        $('#<%=hdnPayNewAmount.ClientID %>').val($('#<%=txtPaymentAmount.ClientID %>').val())
        // alert(reason);
        if (reason == '' || reason == null) {
            $('#<%=lblErrorPayment.ClientID %>').text("Please enter valid reason");
            return false;
        }
        else {

            $('#<%=lblErrorPayment.ClientID %>').text('');
            SaveIndividualPaymentEdit($('#<%=hdnPayPaymentHistoryId.ClientID %>').val(), $('#<%=hdnPayChargeslipId.ClientID %>').val(), $('#<%=hdnPayNewAmount.ClientID %>').val(), $('#<%=txtReasonEdit.ClientID %>').val());
            return false;
        }

    }
    function closepopupPayment() {
        $('#<%=txtReasonEdit.ClientID %>').val('');
        $('#<%=dvChargeSlipEditPayment.ClientID %>').hide();
        return false;
    }
    
    function ShowPopUpPayment(PaymentId, ChargeslipId, Amount) {
        debugger
        // alert('hi');
        //$("#txtPaymentAmount").val(Amount);
        $('#<%=hdnPayPaymentHistoryId.ClientID %>').val(PaymentId);
        $('#<%=hdnPayChargeslipId.ClientID %>').val(ChargeslipId);
        $('#<%=hdnPayNewAmount.ClientID %>').val(Amount);
        document.getElementById('<%=txtPaymentAmount.ClientID%>').value = parseFloat(Amount).toFixed(2);
        document.getElementById('<%=txtReasonEdit.ClientID%>').value = "";
        document.getElementById('<%=lblErrorPayment.ClientID%>').innerHTML = "";
        $('#<%=dvChargeSlipEditPayment.ClientID %>').show();



        return false;
    }
    function closepopupPaymentMessage() {
        $('#<%=lblMessageChangeDetail.ClientID %>').text = "";
        $('#<%=divPaymentMessage.ClientID %>').hide();
        return false;
    }
    function closepopupPaymentAmountMessage() {
        $('#<%=lblPaymentAmountMessage.ClientID %>').text = "";
        $('#<%=divPaymentAmount.ClientID %>').hide();
        return false;
    }
</script>
<script type="text/javascript">
    function DisplayPaymentMessage(PaymentHistoryId) {

        debugger;
        var pageurl = document.URL;
        var Page = "ReportChargeslipReviewCard";
        if (pageurl.indexOf(Page) < 0) {
            var UserRole = '<%=Session["UserType"]%>';
            if (UserRole != null) {
                if (UserRole != "") {
                    if (UserRole == "1") {
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "Customers.aspx/GetIndividualBrokerPaymentMessage",
                            data: '{PaymentHistoryId: "' + PaymentHistoryId + '" }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                //alert('p');
                                if (response.d == "") {

                                    document.getElementById('<%=lblMessageChangeDetail.ClientID%>').innerHTML = "<b>No Payment Amount Changed.</b>";

                                }
                                else {

                                    document.getElementById('<%=lblMessageChangeDetail.ClientID%>').innerHTML = "<b> The Reason for Payment Changed</b>" + response.d;
                                    $('#<%=divPaymentMessage.ClientID %>').show();
                                }
                            },
                            failure: function (response) {

                                alert(response.d);
                                return false;
                            }
                        });
                    }
                    else {
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "../Admin/Customers.aspx/GetIndividualBrokerPaymentMessage",
                            data: '{PaymentHistoryId: "' + PaymentHistoryId + '" }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                //alert('p');
                                if (response.d == "") {

                                    document.getElementById('<%=lblMessageChangeDetail.ClientID%>').innerHTML = "<b>No Payment Amount Changed.</b>";

                                }
                                else {

                                    document.getElementById('<%=lblMessageChangeDetail.ClientID%>').innerHTML = "<b> The Reason for Payment Changed</b>" + response.d;
                                    $('#<%=divPaymentMessage.ClientID %>').show();
                                }
                            },
                            failure: function (response) {

                                alert(response.d);
                                return false;
                            }
                        });

                    }
                }
            }
        }
        else {
            $.ajax({
                async: true,
                type: "POST",
                url: "../Admin/Customers.aspx/GetIndividualBrokerPaymentMessage",
                data: '{PaymentHistoryId: "' + PaymentHistoryId + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //alert('p');
                    if (response.d == "") {

                        document.getElementById('<%=lblMessageChangeDetail.ClientID%>').innerHTML = "<b>No Payment Amount Changed.</b>";

                    }
                    else {

                        document.getElementById('<%=lblMessageChangeDetail.ClientID%>').innerHTML = "<b> The Reason for Payment Changed</b>" + response.d;
                        $('#<%=divPaymentMessage.ClientID %>').show();
                    }
                },
                failure: function (response) {

                    alert(response.d);
                    return false;
                }
            });

        }


        return false;
    }
    function DisplayPaymentAmountMessage(MessageId) {

        debugger;
        // alert(document.URL);
        var pageurl = document.URL;
        var Page = "ReportChargeslipReviewCard";
        if (pageurl.indexOf(Page) < 0) {
            var UserRole = '<%=Session["UserType"]%>';
                if (UserRole != null) {
                    if (UserRole != "") {
                        if (UserRole == "1") {
                            $.ajax({
                                async: true,
                                type: "POST",
                                url: "Customers.aspx/GetPaymentMessage",
                                data: '{MessageId: "' + MessageId + '" }',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response) {
                                    //alert('p');
                                    if (response.d == "") {

                                        document.getElementById('<%=lblPaymentAmountMessage.ClientID%>').innerHTML = "<b>No Payment Amount Changed.</b>";

                                    }
                                    else {

                                        document.getElementById('<%=lblPaymentAmountMessage.ClientID%>').innerHTML = "<b> " + response.d + "</b>";
                                        $('#<%=divPaymentAmount.ClientID %>').show();
                                    }
                                },
                                failure: function (response) {

                                    alert(response.d);
                                    return false;
                                }
                            });
                        }
                        else {
                            $.ajax({
                                async: true,
                                type: "POST",
                                url: "../Admin/Customers.aspx/GetPaymentMessage",
                                data: '{MessageId: "' + MessageId + '" }',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response) {
                                    //alert('p');
                                    if (response.d == "") {

                                        document.getElementById('<%=lblPaymentAmountMessage.ClientID%>').innerHTML = "<b>No Payment Amount Changed.</b>";

                                    }
                                    else {

                                        document.getElementById('<%=lblPaymentAmountMessage.ClientID%>').innerHTML = "<b> " + response.d + "</b>";
                                        $('#<%=divPaymentAmount.ClientID %>').show();
                                    }
                                },
                                failure: function (response) {

                                    alert(response.d);
                                    return false;
                                }
                            });

                        }
                    }
                }
            }
            else {
                $.ajax({
                    async: true,
                    type: "POST",
                    url: "../Admin/Customers.aspx/GetPaymentMessage",
                    data: '{MessageId: "' + MessageId + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        //alert('p');
                        if (response.d == "") {

                            document.getElementById('<%=lblPaymentAmountMessage.ClientID%>').innerHTML = "<b>No Payment Amount Changed.</b>";

                        }
                        else {

                            document.getElementById('<%=lblPaymentAmountMessage.ClientID%>').innerHTML = "<b> " + response.d + "</b>";
                            $('#<%=divPaymentAmount.ClientID %>').show();
                        }
                    },
                    failure: function (response) {

                        alert(response.d);
                        return false;
                    }
                });
            }


            return false;
        }

</script>
<style type="text/css">
    .ApplyColor {
        text-decoration: none;
        color: #7853C9;
        font-weight: bold;
    }

    .ApplyWithoutColor {
        text-decoration: none;
        color: black;
    }

    .popupPayment {
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        position: fixed;
        width: 500px;
        height: 280px;
        border: solid 2px black;
        background-color: white;
        z-index: 1002;
        /*overflow: auto;*/
        text-align: left;
    }

    .popupPaymentMessage {
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        position: fixed;
        border: solid 2px black;
        background-color: white;
        z-index: 1002;
        /*overflow: auto;*/
        text-align: left;
    }

    .black_overlay {
        position: fixed;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: black;
        z-index: 100;
        opacity: 0.4;
        filter: alpha(opacity=40);
    }
    /*#content {
   width: 180px;
   height:40%;
   float: left;
   position: fixed;
   overflow: auto;
   overflow-y: auto;
   overflow-x: none;
}*/
    .white_content {
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        position: fixed;
        height: 550px;
        width: 650px;
        border: solid 2px black;
        background-color: white;
        z-index: 1002;
        text-align: left;
    }
</style>

<div id="fade" class="black_overlay" style="display: block; height: 100%">
</div>

<div id="signin" class="white_content" style="display: block; width: 750px; height: 700px;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr style="background-color: gray; height: 35px;">
            <td style="border-bottom: solid 2px black;">&nbsp;&nbsp;<asp:Label ID="lblPayHistory" Style="vertical-align: bottom; font-size: medium; font-weight: bold; color: Black;"
                runat="server" Text="PAYMENT HISTORY"></asp:Label>
            </td>
            <td width="30px" style="text-align: center; border-bottom: solid 2px black;">
                <asp:ImageButton ID="imgClose" src="../Images/close.png" runat="server"
                    OnClientClick="return hidepopup();" /><%--onclick="imgClose_Click"--%>
            </td>
        </tr>
        <tr style="height: 50px;">
            <td colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%" style="padding: 10px;">
                    <tr>
                        <td style="padding-top: 5px;" colspan="2">
                            <asp:Label ID="lblDealName" Style="vertical-align: bottom; font-size: large; font-weight: bold; color: Orange;"
                                runat="server" Text=""></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <div style="width: 15px; float: right;">
                                <asp:ImageButton ID="imgEditForm2" Style="text-align: right;" Width="15px" Height="15px" runat="server" ImageUrl="~/Images/Edit-Icon.gif" OnClientClick="javascript:return Navigate();" />
                            </div>
                        </td>
                    </tr>
                    <tr height="4px">
                        <td colspan="2" height="4px">
                            <hr style="width: 100%; padding: 0px 0px 0px 0px;" />
                        </td>
                    </tr>
                    <tr style="height: 100px;">
                        <td width="50%" style="vertical-align: top;">
                            <asp:FormView ID="FormView2" Height="100px" Width="100%" runat="server">
                                <HeaderTemplate>
                                    <table cellspacing="2" cellpadding="2" style="width: 100%; font-weight: normal; font-size: 12px; font-family: Verdana;">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="vertical-align: top;">Address:
                                        </td>
                                        <td colspan="2">
                                            <%# !String.IsNullOrEmpty(Eval("ShoppingCenter").ToString()) ? Eval("ShoppingCenter")+"<br />":""%>
                                            <%-- <asp:Label ID="lblShoppingCenter" runat="server" Text='<%#Eval("ShoppingCenter")%>'></asp:Label><br />--%>
                                            <%--                                            <asp:Label ID="lblBillingAddress1" runat="server" Text='<%#Eval("LocationAddress1")%>'></asp:Label>
                                            <asp:Label ID="lblBillingAddress2" runat="server" Text='<%#Eval("LocationAddress2")%>'></asp:Label><br />--%>
                                            <%# (!String.IsNullOrEmpty(Eval("LocationAddress1").ToString()) || !String.IsNullOrEmpty(Eval("LocationAddress2").ToString())) ? Eval("LocationAddress1")+" "+Eval("LocationAddress2")+"<br />":""%>
                                            <asp:Label ID="lblBillingCity" runat="server" Text='<%#Eval("LocationCity")%>'></asp:Label>
                                            <asp:Label ID="lblBillingState" runat="server" Text='<%#Eval("LocState")%>'></asp:Label>
                                            <asp:Label ID="lblBillingZip" runat="server" Text='<%#Eval("LocZipCode")%>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; width: 100px;">Charge To:
                                        </td>
                                        <td colspan="2">
                                            <%# !String.IsNullOrEmpty(Eval("CompanyName").ToString()) ? Eval("CompanyName")+"<br />":""%>
                                            <%# !String.IsNullOrEmpty(Eval("CTOwnershipEntity").ToString()) ? Eval("CTOwnershipEntity")+"<br />":""%>
                                            <%# (!String.IsNullOrEmpty(Eval("CompanyAddress1").ToString()) || !String.IsNullOrEmpty(Eval("CompanyAddress2").ToString())) ? Eval("CompanyAddress1")+" "+Eval("CompanyAddress2")+"<br />":""%>
                                            <%-- <asp:Label ID="lblChargeToName" runat="server" Text='<%#Eval("CompanyName")%>'></asp:Label><br />
                                            <asp:Label ID="lblOwnerShipEntity" runat="server" Text='<%#Eval("CTOwnershipEntity")%>'></asp:Label><br />
                                            <asp:Label ID="lblChargeToAddress1" runat="server" Text='<%#Eval("CompanyAddress1")%>'></asp:Label>
                                            <asp:Label ID="lblChargeToAddress2" runat="server" Text='<%#Eval("CompanyAddress2")%>'></asp:Label><br />--%>
                                            <asp:Label ID="lblChargeToCity" runat="server" Text='<%#Eval("CompanyCity")%>'></asp:Label>
                                            <asp:Label ID="lblChargeToSate" runat="server" Text='<%#Eval("CompanyState")%>'></asp:Label>
                                            <asp:Label ID="lblChargeToZipcode" runat="server" Text='<%#Eval("CompanyZipcode")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:FormView>
                        </td>
                        <td width="50%" style="vertical-align: top;">
                            <asp:FormView ID="FormView3" Height="100px" Width="100%" runat="server">
                                <HeaderTemplate>
                                    <table cellspacing="2" cellpadding="2" style="width: 100%; height: 100%; font-weight: normal; vertical-align: top; font-size: 12px; font-family: Verdana;">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td width="150px">Contact Name:
                                        </td>
                                        <td width="60%">
                                            <asp:Label ID="lblContact" runat="server" Text='<%#Eval("ContactName")%>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="trEmails" runat="server">
                                        <td>Email:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email")%>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Deal Type:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDealType" runat="server" Text='<%#Eval("DealType")%>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Deal Subtype:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDealSubtype" runat="server" Text='<%#Eval("DealSubtype")%>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">Due Dates:
                                        </td>
                                        <td>
                                            <div id="content" style="height: 48px; overflow-y: auto; border: 1px solid black; width: 230px;">
                                                <asp:Label ID="lblDueDates" runat="server" Text=""></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:FormView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr style="width: 100%;" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <div style="width: 100%; overflow-y: auto; height: 435px;">
                                <table style="width: 98%;">
                                    <tr>
                                        <td colspan="2">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblPaymentHistory" Font-Size="12px" runat="server" Text="Payment History:"></asp:Label>
                                            <asp:GridView ID="gvPaymentHistory" CssClass="gv" ShowFooter="True" Font-Size="12px" font-family="Verdana" Width="100%"
                                                runat="server" AutoGenerateColumns="False" OnRowDataBound="gvPaymentHistory_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField DataField="Company" HeaderText="CHARGE TO" ItemStyle-HorizontalAlign="left"
                                                        FooterText="Total" FooterStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="PaymentDate" HeaderText="PAID DATE" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderText="AMOUNT" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" ID="txtAmountPaid" Style="display: none"></asp:TextBox>
                                                            <asp:Label ID="lbldollars" runat="server" Text="$"></asp:Label>
                                                            <asp:LinkButton ID="lblAMOUNTPaid" runat="server" Text='<%# String.Format("{0:n2}",Eval("PaymentAmount"))%>' OnClientClick=<%#"javascript:DisplayPaymentAmountMessage('" + Eval("MessageId") + "');return false;" %>></asp:LinkButton>
                                                            <%--<asp:Label ID="lblAMOUNTPaid" runat="server" Text='<%# String.Format("{0:n2}",Eval("PaymentAmount"))%>'></asp:Label>--%>
                                                            <asp:ImageButton runat="server" ID="imgEditbtnAmount" ImageUrl="~/Images/edit.jpg" AlternateText="Edit" OnClientClick="imgEditbtn_ClientAmountClick(this);return false;"></asp:ImageButton>
                                                            <asp:ImageButton runat="server" ID="imgSavebtnAmount" Width="13px" Height="13px" ImageUrl="~/Images/iconSave.png" AlternateText="Save" Style="display: none" text="save" OnClientClick="imgSavebtn_ClientAmountClick(this);return false;"></asp:ImageButton>
                                                              <asp:ImageButton ID="imgCancelAmount" runat="server" Width="12px" Height="12px" ImageUrl="~/Images/close.png" Style="display: none" AlternateText="Cancel" OnClientClick="imgCancelAmountClick(this);return false;" />
                                                             <asp:Label runat="server" ID="lblPayemntHistoryID" Text='<%#Eval("PaymentId")%>' Style="display: none"></asp:Label>
                                                            <asp:Label runat="server" ID="lblChargeslipid" Text='<%#Eval("ChargeslipId")%>' Style="display: none"></asp:Label>


                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalAMOUNTPaid" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:TemplateField HeaderText="AMOUNT" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign ="Right" >
                                                        <ItemTemplate >
                                                            $<asp:Label ID="lblAMOUNTPaid" runat="server" Text='<%# String.Format("{0:n2}",Eval("PaymentAmount"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate >
                                                            <asp:Label ID="lbltotalAMOUNTPaid" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>--%>
                                                    <asp:BoundField DataField="PaymentMETHOD" HeaderText="METHOD" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="CHECKNO" HeaderText="CHECK NO" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderText="Payment PDF" >
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="imgPaymentPDF" Text="Open Payment Doc"  CssClass="SqureButton" runat="server" style="text-decoration:none;" OnClientClick='<%# "return PaymentPDFOpen(\"" +Eval("ChargeslipPaymentPath") + "\");" %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                                <HeaderStyle CssClass="HeaderGridView2" />
                                                <RowStyle CssClass="RowGridView2" />
                                                <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                                <FooterStyle CssClass="FooterGridView2" />
                                                <%-- <AlternatingRowStyle Height="10px" BackColor="#F5F6CE" />
                                                <HeaderStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />
                                                <RowStyle BackColor="#ffffff" Height="10px" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#F7BE81" />
                                                <FooterStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />--%>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr style="width: 100%;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <br />
                                        </td>
                                    </tr>
                                    <%--           <tr>
                                        <td colspan="2">
                                            <table id="tblPaymentDistribution" runat="server" width="100%" style="font-weight: normal; font-size: 10px;">
                                                <tr>
                                                    <td>
                                                        Payment Distribution:<asp:Label ID="lblPaymentDistribution" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td style=" text-align:right;">
                                                        Commission:<asp:Label ID="lblUserName" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>--%>
                                    <%--  <tr>
                                        <td colspan="2">
                                         <asp:GridView ID="gvPaymentDistribution" CssClass="gv" ShowFooter="True" Font-Size="10px"
                                                Width="100%" runat="server" AutoGenerateColumns="False">
                                                <Columns>
                                                    <asp:BoundField DataField="BrokerRole" HeaderText="ROLE" />
                                                    <asp:TemplateField HeaderText="CHECK AMT" ItemStyle-HorizontalAlign="right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblCHECKAMT" runat="server" Text='<%# String.Format("{0:f2}",Eval("BrokerCommission"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalCHECKAMT" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="BrokerCommission" HeaderText="% DUE" />
                                                    <asp:TemplateField HeaderText="AMOUNT DUE" ItemStyle-HorizontalAlign="right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblAMOUNTDUE" runat="server" Text='<%# String.Format("{0:f2}",Eval("BrokerCommission"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalAMOUNTDUE" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="" HeaderText="Split" />
                                                    <asp:TemplateField HeaderText="SHARE" ItemStyle-HorizontalAlign="right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblCOMMISSIONPAID" runat="server" Text='<%# String.Format("{0:f2}",Eval("COMMISSIONPAID"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalCOMMISSIONPAID" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="" HeaderText="PAYMENT DATE" />
                                                    <asp:BoundField DataField="" ControlStyle-Width="20px" HeaderText="CHECK/WIRE NUMBER" />
                                                    <asp:TemplateField HeaderText="PAYMENT" ItemStyle-HorizontalAlign="right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblAMOUNTUNPAID" runat="server" Text='<%# String.Format("{0:f2}",Eval("AMOUNTUNPAID"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalAMOUNTUNPAID" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle Height="10px" BackColor="#F5F6CE" />
                                                <HeaderStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />
                                                <RowStyle BackColor="#ffffff" Height="10px" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#F7BE81" />
                                                <FooterStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />
                                            </asp:GridView>
                                        </td>
                                    </tr>--%>
                                    <%--<tr>
                                        <td colspan="2">
                                            <table id="Table1" runat="server" width="100%" style="font-weight: normal; font-size: 10px;">
                                                <tr>
                                                    <td>
                                                        Payment Distribution:<asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td style=" text-align:right;">
                                                        Commission:<asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td colspan="2">
                                            <div id="divDistribution" runat="server">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div id="dvChargeSlipEditPayment" title="Individual Amount Edit" class="popupPayment" runat="server" style="display: none;">
    <table style="width: 100%" cellpadding="0">

        <tr style="height: 25px">
            <td colspan="2" style="background-color: grey; padding-left: 10px; vertical-align: bottom; font-weight: bold; font-size: medium;">
                <asp:ImageButton ID="ImageButton2" src="../Images/close.png" ImageAlign="Right" runat="server" OnClientClick="return closepopupPayment();" />
                Individual Broker Amount Edit
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width: 40%; padding-left: 10px;">
                <asp:Label runat="server" ID="lblErrorPayment" ForeColor="Red" Font-Bold="true" Font-Names="Vardena" Font-Size="Small" Text=""></asp:Label>
            </td>
        </tr>

        <tr>
            <td style="padding-left: 10px;">New Amount:</td>
            <td>
                <asp:TextBox ID="txtPaymentAmount" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; padding-left: 10px;">Reason to Edit:</td>
            <td>
                <asp:TextBox TextMode="MultiLine" ID="txtReasonEdit" runat="server" Rows="8" Columns="35"></asp:TextBox></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btnPaymentEditOK" Text="OK" CssClass="SqureButton" runat="server" Width="100px" OnClientClick="javascript:return checkPaymentReason();" />
                <asp:HiddenField ID="hdnChargeSlipidPaymentEdit" runat="server" />
            </td>
    </table>
</div>
<asp:HiddenField ID="hdnPayPaymentHistoryId" runat="server" Value="0" />
<asp:HiddenField ID="hdnPayChargeslipId" runat="server" Value="0" />
<asp:HiddenField ID="hdnPayNewAmount" runat="server" Value="0" />
<div runat="server" style="width: 700px; display: none; height: 170px; overflow: auto; margin: auto; margin-top: 250px;" class="popupPaymentMessage" id="divPaymentMessage">
    <table border="0" cellpadding="0" cellspacing="0" style="height: 170px; vertical-align: top; border-bottom-color: #728AA3; border-bottom-style: solid; border-bottom-width: 1px; border-right-color: #728AA3; border-right-style: solid; border-right-width: 1px; border-left-color: #728AA3; border-left-style: solid; border-left-width: 1px; background-color: White;"
        width="100%"
        align="center">
        <tr>
            <td valign="top" style="background-color: gray; height: 21px; width: 590px; padding-left: 10px;" align="center">
                <asp:Label ID="lblTitle" runat="server" Text="Payment Edit Details" Font-Bold="true"
                    ForeColor="White">
                </asp:Label>
            </td>
            <td valign="middle" align="right" style="background-color: gray; height: 21px; width: 20px;">
                <asp:ImageButton ID="imgbtnClose" runat="server" ImageUrl="~/Images/close.png" OnClientClick="javascript:return closepopupPaymentMessage();" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height: 20px;">
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height: 50px; padding-left: 50px; padding-right: 50px;">
                <asp:Label ID="lblMessageChangeDetail" runat="server" Text=""></asp:Label></td>
        </tr>

        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Button ID="btnOk" runat="server" Text="OK" OnClientClick="javascript:return closepopupPaymentMessage();" CssClass="SqureButton" Width="100px" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height: 25px;">
                <br />
            </td>
        </tr>
    </table>

</div>

<div runat="server" style="width: 700px; display: none; height: 170px; overflow: auto; margin: auto; margin-top: 250px;" class="popupPaymentMessage" id="divPaymentAmount">
    <table border="0" cellpadding="0" cellspacing="0" style="height: 170px; vertical-align: top; border-bottom-color: #728AA3; border-bottom-style: solid; border-bottom-width: 1px; border-right-color: #728AA3; border-right-style: solid; border-right-width: 1px; border-left-color: #728AA3; border-left-style: solid; border-left-width: 1px; background-color: White;"
        width="100%"
        align="center">
        <tr>
            <td valign="top" style="background-color: gray; height: 21px; width: 590px; padding-left: 10px;" align="center">
                <asp:Label ID="Label1" runat="server" Text="Payment Edit Details" Font-Bold="true"
                    ForeColor="White">
                </asp:Label>
            </td>
            <td valign="middle" align="right" style="background-color: gray; height: 21px; width: 20px;">
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/close.png" OnClientClick="javascript:return closepopupPaymentAmountMessage();" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height: 20px;">
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height: 50px; padding-left: 50px; padding-right: 50px;">
                <asp:Label ID="lblPaymentAmountMessage" runat="server" Text=""></asp:Label></td>
        </tr>

        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Button ID="Button1" runat="server" Text="OK" OnClientClick="javascript:return closepopupPaymentAmountMessage();" CssClass="SqureButton" Width="100px" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height: 25px;">
                <br />
            </td>
        </tr>
    </table>

</div>
