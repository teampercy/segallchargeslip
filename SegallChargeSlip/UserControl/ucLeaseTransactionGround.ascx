﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucLeaseTransactionGround.ascx.cs"
    Inherits="UserControl_ucLeaseTransactionGround" %>
<%--<% @ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>--%>
<%@ Register Src="~/UserControl/ucAddNewBuyerTenant.ascx" TagName="NewTenant" TagPrefix="uc" %>
<%--<link href="../Styles/SiteTEST.css" rel="stylesheet" />--%>

<script type="text/javascript">
    function ShowIcon(sender, e) {
        sender._element.className = "loadingUC";
    }

    function hideIcon(sender, e) {
        sender._element.className = "textwidthUC";
    }

    function OnTenantSelected(source, eventArgs) {
        document.getElementById('<%=hdnTenantID.ClientID %>').value = eventArgs.get_value();
        //   alert(eventArgs.get_value());
        return false;
    }

    function CalculateAmountGround() {
        debugger;
        //alert("Entered into Calculate Amount");
        var size = document.getElementById("<%=txtParcelSize.ClientID%>").value;
        var txtTI = document.getElementById("<%=txtTI.ClientID%>").value;
        var ddlTIAType = document.getElementById('<%=ddlTIAllowanceType.ClientID%>').value;
        var ddlSizeMeasure = document.getElementById('<%=ddlSizeMeasure.ClientID%>').value;
        //alert(size + " " + txtTI + " " + ddlTIAType);
        // alert(txtTI);
        var msg;
       
        
        if (size != "" && txtTI != "") {

            if (ddlSizeMeasure == 1)
            {
                if (ddlTIAType == 1)
                {
                    msg = "<b>Aggregate Amount :</b> $" + parseFloat(size * txtTI).toFixed(2);
                }
                else if (ddlTIAType == 2) {
                    msg = "<b>Per Square Foot :</b> $" + parseFloat(txtTI / size).toFixed(2);
                }
                else if (ddlTIAType == 0) {
                    msg = "";
                }
            }
            else if (ddlSizeMeasure == 2) {
                

                if (ddlTIAType == 1) {
                    msg = "<b>Aggregate Amount :</b> $" + parseFloat((size * 43560) * txtTI).toFixed(2);
                }
                else if (ddlTIAType == 2) {
                    msg = "<b>Per Square Foot :</b> $" + parseFloat(txtTI / (size * 43560)).toFixed(2);
                }
                else if (ddlTIAType == 0) {
                    msg = "";
                }

            }



            //if (ddlTIAType == 1) {
            //    msg = "<b>Aggregate Amount :</b> $" + parseFloat(size * txtTI);
            //}
            //else if (ddlTIAType == 2) {
            //    msg = "<b>Per Square Foot :</b> $" + parseFloat(txtTI / size);
            //}
            document.getElementById("<%=lblTIAllowanceMsg.ClientID%>").innerHTML = msg;



        }
    }
</script>




<table class="ucbackground" style="width: 100%;">
    <tr>
        <td colspan="3" class="ucheadings">LEASE TRANSACTION: GROUND
            <hr />
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">PARCEL SIZE
        </td>
        <td colspan="2">
            <asp:TextBox ID="txtParcelSize" runat="server" CssClass="uctextboxMed"  TabIndex="24" onblur="CalculateAmountGround()"></asp:TextBox>
            &nbsp;
            <asp:DropDownList ID="ddlSizeMeasure" runat="server" CssClass="ucddlSmall" onchange="CalculateAmountGround()">
                <asp:ListItem Text="Sq Ft" Value="1"></asp:ListItem>
                <asp:ListItem Text="Acres" Value="2"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">TENANT</td>
        <td colspan="2">
            <asp:TextBox runat="server" ID="txtTenant" CssClass="uctextboxMed" AutoPostBack="true" OnTextChanged="txtTenant_TextChanged"  TabIndex="25" />
            <ajax:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server"
                TargetControlID="txtTenant"
                DelimiterCharacters=";, :"
                MinimumPrefixLength="1"
                EnableCaching="true"
                CompletionSetCount="10"
                CompletionInterval="300"
                ServiceMethod="SearchGetDealPartiesList"
                OnClientPopulating="ShowIcon"
                OnClientPopulated="hideIcon"
                OnClientItemSelected="OnTenantSelected"
                ShowOnlyCurrentWordInCompletionListItem="true"
                CompletionListCssClass="AutoExtender"
                CompletionListItemCssClass="AutoExtenderList"
                CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                CompletionListElementID="divwidth" />
            <asp:HiddenField ID="hdnTenantID" runat="server" />
            <ajax:ModalPopupExtender ID="mpeAddNewTenant" TargetControlID="btnAddNewTenant" CancelControlID="ucNwTenant$ImgbtnCancel"
                PopupControlID="dvNwTenant" runat="server" BackgroundCssClass="modalBackground">
            </ajax:ModalPopupExtender>
            &nbsp;
            <asp:ImageButton ID="btnAddNewTenant" runat="server" CssClass="imgSizeSmall" ImageUrl="~/Images/Add-Icon.gif"  />
            <%--   <asp:Button ID="btnAddNewTenant" runat="server" Text="Add New" CssClass="SqureButton" />--%>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">USE
        </td>
        <td colspan="2">
            <asp:DropDownList ID="ddlUse" runat="server" CssClass="ddlSmall" TabIndex="26">
              <%-- <asp:ListItem Text="RETAIL" Value="Retail"></asp:ListItem>
                <asp:ListItem Text="RESTAURANT" Value="Restaurant"></asp:ListItem>--%>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">LEASE START
        </td>
        <td colspan="2">
            <asp:TextBox ID="txtLeaseStart" runat="server" CssClass="uctextboxMed" TabIndex="27"></asp:TextBox>
            <asp:ImageButton ID="btnCalender" runat="server" Width="16px" Height="18px" ImageUrl="~/Images/Calander.jpeg" />
            <ajax:CalendarExtender ID="ajaxCal" runat="server" TargetControlID="txtLeaseStart" PopupButtonID="btnCalender" Format="MM/dd/yyyy"></ajax:CalendarExtender>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">NETS
        </td>
        <td colspan="2">
            <asp:TextBox ID="txtNets" runat="server" CssClass="uctextboxMed DollarTextBox" TabIndex="28"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">TI
        </td>
        <td colspan="2">
            <asp:TextBox runat="server" ID="txtTI" CssClass="uctextboxMed DollarTextBox" TabIndex="29" onblur="CalculateAmountGround()"></asp:TextBox>
            &nbsp;
            <asp:DropDownList ID="ddlTIAllowanceType" runat="server" Font-Size="Small" Width ="90px" onchange="CalculateAmountGround()">
                 <asp:ListItem Value="0" Text="-Select-"  />
                <asp:ListItem Value="1" Text="Per Sqft" Selected="True" />
                <asp:ListItem Value="2" Text="Aggregate" />
            </asp:DropDownList>
            <br/>
            <asp:Label runat="server" ID="lblTIAllowanceMsg"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">FREE RENT
        </td>
        <td>
            <asp:TextBox ID="txtFreeRent" runat="server" CssClass="uctextboxMed" TabIndex="30"></asp:TextBox>
            &nbsp;
            <asp:DropDownList ID="ddlFreeRentTime" runat="server" CssClass="ucddlSmall" TabIndex="31">
                <asp:ListItem Value="1" Text="Days" />
                <asp:ListItem Value="2" Text="Months" />
                <asp:ListItem Value="3" Text="Years" />
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">NOTES
        </td>
        <td colspan="2">
            <asp:TextBox ID="txtNotes" Rows="10" CssClass="uctextArea" runat="server" TextMode="MultiLine" Columns="35" TabIndex="32"></asp:TextBox>
        </td>
    </tr>
</table>
<div style="display: none; height: 380px;" class="PopupDivBodyStyle" id="dvNwTenant">
    <uc:NewTenant ID="ucNwTenant" runat="server" />
</div>
