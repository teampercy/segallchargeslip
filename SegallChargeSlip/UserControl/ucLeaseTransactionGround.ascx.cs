﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SegallChargeSlipBll;

public partial class UserControl_ucLeaseTransactionGround : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        { 
        if (!IsPostBack)
        {
            BindDropDown();
            // Tenant objTenant = new Tenant();
            //DataSet ds = objTenant.GetList();
            // Utility.FillDropDown(ref ajaxCombTenant, ds.Tables[0], "TenantName", "TenantID");
            SetAttributes();
            if (Session[GlobleData.NewChargeSlipObj] != null)
            {
                SetDataInControls();
                //Convert.ToString(DateTime.ParseExact(Convert.ToString(objDealTransactions.LeaseStartDate), "dd/MM/yyyy", null));
            }
            else
            {
                ResetControls();
            }
        }
        //else
        //{
        ucNwTenant.Title = "ADD NEW TENANT";
        ucNwTenant.ClassRef = "T";
            //}
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucLeaseTransactionGround.ascx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #region Custom Actions
    private void BindDropDown()
    {
        CustomList objCL = new CustomList();
        Utility.FillDropDown(ref ddlUse, objCL.GetDealUseTypes(), "DealUseType", "DealUseId");
    }
    public void SetProperties()
    {
        try
        {
            DealTransactions objDealTransactions = new DealTransactions();
            objDealTransactions.DealUse = ddlUse.SelectedValue;
            objDealTransactions.DealUseText = ddlUse.SelectedItem.Text;
            objDealTransactions.LeaseStartDate = DateTime.ParseExact(txtLeaseStart.Text, "MM/dd/yyyy", null);
            objDealTransactions.Notes = txtNotes.Text;
           objDealTransactions.Tenant = hdnTenantID.Value == "" ? 0 : Convert.ToInt32(hdnTenantID.Value);
           if (objDealTransactions.Tenant == 0)
               objDealTransactions.DealPartyCompanyName = txtTenant.Text.Trim();
            objDealTransactions.UnitValue = 1;
            objDealTransactions.UnitTypeValue = Convert.ToInt32(ddlSizeMeasure.SelectedValue);
            objDealTransactions.Size = txtParcelSize.Text.Trim() == "" ? 0 : Convert.ToDecimal(txtParcelSize.Text);
            if (!string.IsNullOrEmpty(txtNets.Text))
            {
                objDealTransactions.Nets = Convert.ToDecimal(txtNets.Text);
            }
            objDealTransactions.DealType = 2;
            objDealTransactions.DealSubType = 3;
            objDealTransactions.FreeRentUnitValue = 2;
            objDealTransactions.FreeRentUnitTypeValue = Convert.ToInt16(ddlFreeRentTime.SelectedValue);
            if (!string.IsNullOrEmpty(txtTI.Text))
            {
                objDealTransactions.TI = Convert.ToDecimal(txtTI.Text);
            }
            if (!string.IsNullOrEmpty(txtFreeRent.Text))
            {
                objDealTransactions.FreeRent = Convert.ToDecimal(txtFreeRent.Text);
            }
            objDealTransactions.TIAllowanceType = Convert.ToInt32(ddlTIAllowanceType.SelectedItem.Value);// Added by Shishir 17-05-2016
            objDealTransactions.AdditionalNotes = ViewState["AddNotes"] == null ? "" : ViewState["AddNotes"].ToString();
            Session.Add(GlobleData.DealTransactionObj, objDealTransactions);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucLeaseTransactionGround.ascx", "SetProperties", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private void ResetControls()
    {
        txtNets.Text = "";
        txtParcelSize.Text = "";
        txtNotes.Text = "";
        txtLeaseStart.Text = "";
        ddlSizeMeasure.SelectedIndex = 0;
        ddlUse.SelectedIndex = 0;
        txtTI.Text = "";
        txtFreeRent.Text = "";
        ddlTIAllowanceType.SelectedValue = "1";
        SetAttributes();

    }

    private void SetAttributes()
    {
        txtParcelSize.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtParcelSize.ClientID + "')");
        txtNets.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtNets.ClientID + "')");
        txtTI.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtTI.ClientID + "')");
        //txtFreeRent.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtFreeRent.ClientID + "')");//Commented by Jaywanti on 24-08-2015
        txtFreeRent.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtFreeRent.ClientID + "')");
    }

    private void SetDataInControls()
    {
        try
        { 
        ChargeSlip objChargeSlip = new ChargeSlip();
        objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];


        DealParties objTenant = new DealParties();
        objTenant.PartyID = objChargeSlip.DealTransactionsProperty.Tenant;
        hdnTenantID.Value = objChargeSlip.DealTransactionsProperty.Tenant.ToString();
        objTenant.Load();

        txtNets.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.Nets);
        // ddlSizeMeasure.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.UnitTypeValue);
        // ddlUse.SelectedValue = objChargeSlip.DealTransactionsProperty.DealUse;
        hdnTenantID.Value = Convert.ToString(objChargeSlip.DealTransactionsProperty.Tenant);
        // txtTenant.Text = objTenant.PartyName;
        txtTenant.Text = objTenant.PartyCompanyName;
        txtParcelSize.Text = Convert.ToString(Convert.ToInt32(objChargeSlip.DealTransactionsProperty.Size));
        HiddenField hdnSize = null, hdnOldUnitValue = null, hdnOldUnitTypeValue = null;
        Control ctl = this.Parent;
        while (true)
        {
            hdnSize = (HiddenField)ctl.FindControl("hdnSize");
            if (hdnSize != null)
            {
                hdnSize.Value = Convert.ToString(Convert.ToInt32(objChargeSlip.DealTransactionsProperty.Size));
            }
            hdnOldUnitValue = (HiddenField)ctl.FindControl("hdnOldUnitValue");
            if (hdnOldUnitValue != null)
            {
                hdnOldUnitValue.Value = Convert.ToString(Convert.ToInt32(objChargeSlip.DealTransactionsProperty.UnitValue));
            }
            hdnOldUnitTypeValue = (HiddenField)ctl.FindControl("hdnOldUnitTypeValue");
            if (hdnOldUnitTypeValue != null)
            {
                hdnOldUnitTypeValue.Value = Convert.ToString(Convert.ToInt32(objChargeSlip.DealTransactionsProperty.UnitTypeValue));
            }
            break;
        }
        if (!string.IsNullOrEmpty(txtParcelSize.Text))
        {
            //Comment by Jaywanti on 31-08-2015
            //LeaseTerms objLeaseTerm = new LeaseTerms();
            //objLeaseTerm.ChargeSlipId = objChargeSlip.ChargeSlipID;
            //DataSet dslease = null;
            //dslease = objLeaseTerm.GetListByChargeSlip();
            //if (dslease.Tables[0] != null)
            //{
            //    if (dslease.Tables[0].Rows.Count > 0)
            //    {
            //        txtParcelSize.ReadOnly = true;
            //        ddlSizeMeasure.Enabled = false;
            //    }
            //    else
            //    {
            //        txtParcelSize.ReadOnly = false;
            //        ddlSizeMeasure.Enabled = true;
            //    }
            //}
            //else
            //{
            //    txtParcelSize.ReadOnly = false;
            //    ddlSizeMeasure.Enabled = true;
            //}
            //Added by Jaywanti on 31-08-2015
            CommissionDueDates objCommissionDueDates = new CommissionDueDates();
            objCommissionDueDates.ChargeSlipId = objChargeSlip.ChargeSlipID;
            DataTable dueDt = objCommissionDueDates.CheckPaymentpaid();
            Boolean IsPaymentComp;
            if (dueDt != null)
            {
                if (dueDt.Rows.Count > 0)
                {
                    //Session[GlobleData.cns_IsFullCommPaid] = true;
                    for (int i = 0; i <= dueDt.Rows.Count - 1; i++)
                    {
                        IsPaymentComp = Convert.ToBoolean(dueDt.Rows[i]["IsPayment"].ToString());
                        if (IsPaymentComp == true)
                        {
                            txtParcelSize.ReadOnly = false;
                            ddlSizeMeasure.Enabled = true;
                            break;
                        }
                        else
                        {
                            txtParcelSize.ReadOnly = false;
                            ddlSizeMeasure.Enabled = true;
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            ddlSizeMeasure.Enabled = true;
            txtParcelSize.ReadOnly = false;
        }
        txtNotes.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.Notes);
        txtLeaseStart.Text = objChargeSlip.DealTransactionsProperty.LeaseStartDate.ToString("MM/dd/yyyy");
        ddlSizeMeasure.SelectedValue = Convert.ToString(objChargeSlip.DealTransactionsProperty.UnitTypeValue);
        ddlUse.SelectedValue = objChargeSlip.DealTransactionsProperty.DealUse;
        txtFreeRent.Text = Convert.ToString(Convert.ToInt32(objChargeSlip.DealTransactionsProperty.FreeRent));
        //  ddlFreeRentTime.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.UnitTypeValue);
        ddlFreeRentTime.SelectedValue = Convert.ToString(objChargeSlip.DealTransactionsProperty.FreeRentUnitTypeValue);
        txtTI.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.TI);
        ddlTIAllowanceType.SelectedValue = objChargeSlip.DealTransactionsProperty.TIAllowanceType == null ? "0" : Convert.ToString(objChargeSlip.DealTransactionsProperty.TIAllowanceType);
        // For Additional Notes
        ViewState["AddNotes"] = Convert.ToString(objChargeSlip.DealTransactionsProperty.AdditionalNotes);
        ScriptManager.RegisterStartupScript(this, Page.GetType(), "RebindTenantAllowance", "CalculateAmountGround();", true);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucLeaseTransactionGround.ascx", "SetDataInControls", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #endregion
    protected void txtTenant_TextChanged(object sender, EventArgs e)
    {
        try
        { 
        DealParties objdealParties = new DealParties();
        if (hdnTenantID.Value != "")
        {
            objdealParties.PartyID = Convert.ToInt32(hdnTenantID.Value);
            objdealParties.Load();
            if (objdealParties.PartyCompanyName != txtTenant.Text)
                hdnTenantID.Value = "";
        }
        txtTenant.Focus();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucLeaseTransactionGround.ascx", "txtTenant_TextChanged", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
}