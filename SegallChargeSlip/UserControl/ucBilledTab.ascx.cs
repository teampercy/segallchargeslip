﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Configuration;
using SegallChargeSlipBll;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class UserControl_ucBilledTab : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //if (DateTime.Now <= DateTime.Parse("2014/02/03"))
            //{
            if (Session["UserId"] == null)
            {
                //Response.Redirect("~/adlogin.aspx");
                Session.Clear();
                Session.RemoveAll();
                Server.Transfer("~/Login.aspx");
                //Response.Redirect("Login.aspx");
            }
            //imgEditForm2.Attributes.Add("onclick", "javascript:test();");
            if (!IsPostBack)
            {
                SetAttributes();
                Utility.FillStates(ref ddlState);
                divDealCustgv.Visible = true;
                divTransgv.Visible = false;
                divStart.Visible = true;

                CustAndDealInfo.Visible = true;
                CustInfo.Visible = false;
                DealInfo.Visible = false;

                lnkCust.Enabled = false;
                lnkTrans.Enabled = true;
                lnkCust.Style.Add("color", "Black");
                lnkTrans.Style.Add("color", "Gray");
                lnkCust.Style.Add("background-color", "white");
                lnkTrans.Style.Add("background-color", "#E6E6E6");
                lnkCust.Style.Add("border-bottom", "solid 1px white");
                lnkTrans.Style.Add("border-bottom", "solid 1px black");

                divTabsCust.Visible = true;
                divTabsTrans.Visible = false;
                divCustomers.Visible = true;

                divCust.Visible = false;
                divDeal.Visible = false;
                divVendor.Visible = false;
                divCustomers.Visible = true;
                divVendorGrid.Visible = false;
                DealTransactions objDealTransactions = new DealTransactions();

                //For Vendor Side
                if (Session["IsVendor"] != null)
                {
                    if (Session["IsVendor"].ToString() == "1")
                    {
                        lnkCust.Text = "VENDORS & DEALS";
                        lblCustAndDealInfo.Text = "VENDOR AND DEAL INFORMATION WINDOW";
                        divCustomers.Visible = false;
                        divVendorGrid.Visible = true;
                        DataSet dsVendor = objDealTransactions.GetDealVendorTransactions(0, txtCustFind.Text.Trim());
                        //DataSet dsVendor = objDealTransactions.GetDealVendorTransactions(Convert.ToInt32(ddlCustShow.SelectedValue), txtCustFind.Text.Trim());//modified by Shishir 04-05-2016
                        if (dsVendor != null)
                        {
                            DataTable dt = dsVendor.Tables[0];
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                gvVendor.DataSource = dt;
                                gvVendor.DataBind();
                            }
                            else
                            {
                                gvVendor.DataSource = null;
                                gvVendor.DataBind();
                            }
                        }
                    }
                }
                if (Session["IsVendor"] != null)
                {
                    if (Session["IsVendor"].ToString() == "0")
                    {

                        int userid = Convert.ToInt32(Session["UserID"].ToString());
                        DataSet ds = objDealTransactions.GetDealCustomerTransactionsListUsingConditions(0, txtCustFind.Text.Trim(), userid);
                        //DataSet ds = objDealTransactions.GetDealCustomerTransactionsList();
                        if (ds != null)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                gvCustInfo.DataSource = ds;
                                gvCustInfo.DataBind();
                            }
                            else
                            {
                                gvCustInfo.DataSource = null;
                                gvCustInfo.DataBind();
                            }
                        }
                    }
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(UpdatePanel1, Page.GetType(), "display1", "FixedCustomerGrid();", true);
                ScriptManager.RegisterStartupScript(UpdatePanel1, Page.GetType(), "display", "FixedVendorGrid();", true);
                ScriptManager.RegisterStartupScript(UpdatePanel4, Page.GetType(), "displays", "FixedTransGrid();", true);
                //ScriptManager.RegisterStartupScript(UpdatePanel4, Page.GetType(), "display9", "FixedDealInfoGrid();", true);
                //ScriptManager.RegisterStartupScript(UpdatePanel4, Page.GetType(), "display9", "applyscrollable();", true);
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Key", "<script>MakeStaticHeader('" + gvDealInfo.ClientID + "', 368, 0 , 40 ,false); </script>", false);
            }
           
            // }

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "Page_Load", ex.Message);
        }

    }

    //Set & get sort state in  viewstate for gvDealInfo
    private string GVSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "DESC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GVSortDirection)
        {
            //If previous sort direction if ascending order then assign new direction as descending order
            case "ASC":
                GVSortDirection = "DESC";
                break;

            //If previous sort direction if descending order then assign new direction as ascending order
            case "DESC":
                GVSortDirection = "ASC";
                break;
        }
        return GVSortDirection;
    }

    protected void gvDealInfo_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            DataTable dataTable = (DataTable)DealInfoList();
            // DataTable dataTable = (DataTable)Session["dt"];
            if (dataTable != null)
            {
                //Create new dataview instance and pass datatable 
                DataView dataView = new DataView(dataTable);

                //get sort direction from the get sort direction method
                string sortDirection = GetSortDirection();

                //Sort dataview data based on the sort directin value
                dataView.Sort = e.SortExpression + " " + sortDirection;

                //Assign datasource and bind data to grid view
                gvDealInfo.DataSource = dataView;
                gvDealInfo.DataBind();
                gvDealInfo.SelectedIndex = -1;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "gvDealInfo_Sorting", ex.Message);
        }
    }


    //Set & get sort state in  viewstate for gvTransInfo

    private string GVSortDirectionTransInfo
    {
        get { return ViewState["SortDirectionTransInfo"] as string ?? "DESC"; }
        set { ViewState["SortDirectionTransInfo"] = value; }
    }

    private string GetSortDirectionTransInfo()
    {
        switch (GVSortDirectionTransInfo)
        {
            //If previous sort direction if ascending order then assign new direction as descending order
            case "ASC":
                GVSortDirectionTransInfo = "DESC";
                break;

            //If previous sort direction if descending order then assign new direction as ascending order
            case "DESC":
                GVSortDirectionTransInfo = "ASC";
                break;
        }
        return GVSortDirectionTransInfo;
    }

    protected void gvTransInfo_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            //TransInfoList(); 
            DataTable dataTable = (DataTable)TransInfoList();
            //DataTable dataTable = (DataTable)Session["dtTransInfo"];
            if (dataTable != null)
            {
                //Create new dataview instance and pass datatable 
                DataView dataView = new DataView(dataTable);

                //get sort direction from the get sort direction method
                string sortDirection = GetSortDirectionTransInfo();

                //Sort dataview data based on the sort directin value
                dataView.Sort = e.SortExpression + " " + sortDirection;

                //Assign datasource and bind data to grid view
                gvTransInfo.DataSource = dataView;
                gvTransInfo.DataBind();
                //gvTransInfo.SelectedIndex = -1;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "gvTransInfo_Sorting", ex.Message);
        }
    }
    decimal TotalVenDue = 0;
    protected void gvVendor_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "javascript:SetMouseOver(this)";
                e.Row.Attributes["onmouseout"] = "javascript:SetMouseOut(this)";
                Label lbl = (Label)e.Row.FindControl("lblUserId");
                e.Row.Attributes["onClick"] = "javascript:ClickTrVendor(" + lbl.Text + "," + e.Row.RowIndex + ")";
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.Eval(e.Row.DataItem, "Unapplied") != DBNull.Value)
                    TotalVenDue += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Unapplied"));
                HiddenField hdnIsActive = new HiddenField();
                ImageButton img = new ImageButton();
                hdnIsActive = (HiddenField)e.Row.FindControl("hdnIsActive");
                img = (ImageButton)e.Row.FindControl("imgStatus");
                HiddenField hdntable = new HiddenField();
                hdntable = (HiddenField)e.Row.FindControl("hdntable");
                Label lblUnapplied = new Label();
                lblUnapplied = (Label)e.Row.FindControl("lblUnapplied");
                if (hdntable != null)
                {
                    if (hdntable.Value == "2")
                    {
                        if (lblUnapplied != null)
                        {
                            lblUnapplied.Text = "";
                            img.ImageUrl = "";
                            img.Visible = false;
                            e.Row.BackColor = System.Drawing.Color.LightGray;
                        }
                    }
                    else
                    {
                        if (hdnIsActive != null)
                        {
                            if (lblUnapplied != null)
                            {
                                lblUnapplied.Text = "$" + lblUnapplied.Text;
                            }
                            if (hdnIsActive.Value == "True")
                            {
                                img.ImageUrl = "~/Images/bullet.gif";
                            }
                            else if (hdnIsActive.Value == "False")
                            {
                                img.ImageUrl = "~/Images/gray_dot.png";
                            }
                        }
                    }
                }

            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblVenUnapplied = (Label)e.Row.FindControl("lblVenUnapplied");
                lblVenUnapplied.Text = "$" + String.Format("{0:n2}", TotalVenDue);
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "gvVendor_RowDataBound", ex.Message);
        }
    }
    decimal TotalUnAPPLIED = 0;
    protected void gvCustInfo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "javascript:SetMouseOver(this)";
                e.Row.Attributes["onmouseout"] = "javascript:SetMouseOut(this)";
                Label lbl = (Label)e.Row.FindControl("lblCompanyID");
                e.Row.Attributes["onClick"] = "javascript:ClickTrCust(" + lbl.Text + "," + e.Row.RowIndex + ")";
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                if (DataBinder.Eval(e.Row.DataItem, "Unapplied") != DBNull.Value)
                    TotalUnAPPLIED += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Unapplied"));

                HiddenField hdnIsActive = new HiddenField();
                ImageButton img = new ImageButton();
                hdnIsActive = (HiddenField)e.Row.FindControl("hdnIsActive");
                img = (ImageButton)e.Row.FindControl("imgStatus");
                if (hdnIsActive != null)
                {
                    if (hdnIsActive.Value == "True")
                    {
                        img.ImageUrl = "~/Images/bullet.gif";
                    }
                    else if (hdnIsActive.Value == "False")
                    {
                        img.ImageUrl = "~/Images/gray_dot.png";
                    }
                }
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblCusUnapplied = (Label)e.Row.FindControl("lblCusUnapplied");
                lblCusUnapplied.Text = "$" + String.Format("{0:n2}", TotalUnAPPLIED);
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (Session["UserID"] != null)
                {
                    //if (Convert.ToInt32(Convert.ToString(Session["UserID"])) == 2)
                    //{

                    //    e.Row.Cells[2].Text = "DUE";

                    //}
                    //else
                    //{

                    //    e.Row.Cells[2].Text = "UNAPPLIED";

                    //}
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "gvCustInfo_RowDataBound", ex.Message);
        }

    }
    private void SetAttributes()
    {
        txtSetAmount.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtSetAmount.ClientID + "')");
        txtShare.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtShare.ClientID + "')");
        txtEin.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtEin.ClientID + "')");
        //txtZipCode.Attributes.Add("onkeyup", "javascript:return ValidateNumber('" + txtZipCode.ClientID + "')");

    }
    decimal totalGross = 0, totalUnapplied = 0, totalShare = 0, TotalPaidAmount = 0, TotalMyShareActual = 0;
    protected void gvDealInfo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            int Chargeslipid, userid, usertype;
            int pdamt;
            userid = Convert.ToInt32(Session["UserID"]);
            usertype = Convert.ToInt32(Session["UserType"]);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "javascript:SetMouseOver(this)";
                e.Row.Attributes["onmouseout"] = "javascript:SetMouseOut(this)";
                Label lbl = (Label)e.Row.FindControl("lblDealTransactionID");
                Label lblCommissionDueDAte = (Label)e.Row.FindControl("lblCommissionDueDateId");
                //e.Row.Attributes["onClick"] = "javascript:ClickTrDeal(" + lbl.Text + "," + e.Row.RowIndex + ")";
                for (int x = 0; x < 7; x++)
                {
                    //e.Row.Cells[x].Attributes["onmouseover"] = "javascript:SetMouseOver(this)";
                    //e.Row.Cells[x].Attributes["onmouseout"] = "javascript:SetMouseOut(this)";
                    if (x != 6)
                    {
                        e.Row.Cells[x].Attributes["onClick"] = "javascript:ClickTrDeal(" + lbl.Text + "," + lblCommissionDueDAte.Text + "," + e.Row.RowIndex + ")";
                    }
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnChargeslipCreatedBy = (HiddenField)e.Row.FindControl("hdnCreatedBy");
                //if (e.Row.RowIndex == 0)
                //{
                //    e.Row.Style.Add("height", "35px");
                //}

                if (DataBinder.Eval(e.Row.DataItem, "Gross") != DBNull.Value)
                    totalGross += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Gross"));
                if (DataBinder.Eval(e.Row.DataItem, "Unapplied") != DBNull.Value)
                    totalUnapplied += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Unapplied"));
                if (DataBinder.Eval(e.Row.DataItem, "CompanyEstimatedShare") != DBNull.Value)
                    totalShare += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CompanyEstimatedShare"));
                if (DataBinder.Eval(e.Row.DataItem, "PaidAmount") != DBNull.Value)
                    TotalPaidAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PaidAmount"));
                if (DataBinder.Eval(e.Row.DataItem, "CompanyActualShare") != DBNull.Value)
                    TotalMyShareActual += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CompanyActualShare"));


                pdamt = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "IsPaid"));
                ImageButton img = (ImageButton)e.Row.FindControl("imgDelete");

                Chargeslipid = Convert.ToInt32(img.CommandArgument);
                //ChargeSlip objchargeslip = new ChargeSlip();
                //objchargeslip.ChargeSlipID = Chargeslipid;
                //objchargeslip.Load();

                if (userid == Convert.ToInt32(hdnChargeslipCreatedBy.Value) || usertype == 1)
                {
                    //We need to enable the Delete Payment Image Button in both cases, if payment alreday made or not
                    if (pdamt != 0)
                    {

                        img.Enabled = true;
                        //img.ImageUrl = "~/Images/deletegray.png";
                    }
                    else
                        img.Enabled = true;

                }
                else
                {
                    img.Enabled = true;
                    //img.ImageUrl = "~/Images/deletegray.png";
                }
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblTotalGross = (Label)e.Row.FindControl("lblTotalGross");
                lblTotalGross.Text = "$" + String.Format("{0:n2}", totalGross);
                Label lbltotalUnapplied = (Label)e.Row.FindControl("lbltotalUnapplied");
                lbltotalUnapplied.Text = "$" + String.Format("{0:n2}", totalUnapplied);
                Label lbltotalShare = (Label)e.Row.FindControl("lbltotalShare");
                //lbltotalShare.Text = "$" + totalShare.ToString();
                lbltotalShare.Text = "$" + String.Format("{0:n2}", totalShare);
                Label lblFooPaidAmount = (Label)e.Row.FindControl("lblFooPaidAmount");
                lblFooPaidAmount.Text = "$" + String.Format("{0:n2}", TotalPaidAmount);
                Label lblMyShareTotalShare = (Label)e.Row.FindControl("lblMyShareTotalShare");
                lblMyShareTotalShare.Text = "$" + String.Format("{0:n2}", TotalMyShareActual);
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (Session["UserID"] != null)
                {
                    //if (Convert.ToInt32(Convert.ToString(Session["UserID"])) == 2)
                    //{
                    if (Session["UserType"].ToString() == "1")
                    {

                        //GridViewRow HeaderRow = gvCustInfo.HeaderRow;
                        gvDealInfo.Columns[7].Visible = false;
                        gvDealInfo.Columns[8].Visible = false;
                        LinkButton HLinksMyShare = (LinkButton)e.Row.Cells[7].Controls[0];
                        HLinksMyShare.Text = "COMPANY SHARE ESTIMATED";
                        LinkButton MyShareActual = (LinkButton)e.Row.Cells[8].Controls[0];
                        MyShareActual.Text = "COMPANY SHARE ACTUAL";
                        if (Session["IsVendor"] != null)
                        {
                            if (Session["IsVendor"].ToString() == "1")
                            {
                                gvDealInfo.Columns[7].Visible = true;
                                gvDealInfo.Columns[8].Visible = true;
                                LinkButton HLink = (LinkButton)e.Row.Cells[4].Controls[0];
                                HLink.Text = "VENDOR PAID";
                                //gvDealInfo.Columns[4].HeaderText = "VENDOR PAID";
                            }
                            else
                            {
                                LinkButton HLink = (LinkButton)e.Row.Cells[4].Controls[0];
                                HLink.Text = "CUSTOMER PAID";
                                //gvDealInfo.Columns[4].HeaderText = "CUSTOMER PAID";
                            }
                        }
                        //HeaderRow.Cells[2].Text = "DUE";
                    }
                    else
                    {
                        //GridViewRow HeaderRow = gvCustInfo.HeaderRow;
                        gvDealInfo.Columns[7].Visible = true;
                        gvDealInfo.Columns[10].Visible = false;
                        //Add for Payment Void on 15-07-2016
                        gvDealInfo.Columns[12].Visible = false;

                        //HeaderRow.Cells[2].Text = "UNAPPLIED";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "gvDealInfo_RowDataBound", ex.Message);
        }

    }

    decimal totalTransGross = 0, totalTransUnapplied = 0, totalTransShare = 0, TotalPaidAmountTran = 0, TotalMyShareActualTran = 0, TotalVendorEstimatedShare = 0, TotalGrossAmount = 0;
    protected void gvTransInfo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (Session["UserID"] != null)
                {
                    //if (Convert.ToInt32(Convert.ToString(Session["UserID"])) == 2)
                    //{
                    if (Session["UserType"].ToString() == "1")
                    {
                        //GridViewRow HeaderRow = gvCustInfo.HeaderRow;
                        gvTransInfo.Columns[10].Visible = false;
                        gvTransInfo.Columns[0].Visible = false;
                        gvTransInfo.Columns[11].Visible = false;
                        gvTransInfo.Columns[6].Visible = false;
                        LinkButton HLinksMyShare = (LinkButton)e.Row.Cells[10].Controls[0];
                        HLinksMyShare.Text = "COMPANY SHARE ESTIMATED";
                        LinkButton MyShareActual = (LinkButton)e.Row.Cells[11].Controls[0];
                        MyShareActual.Text = "COMPANY SHARE ACTUAL";
                        if (Session["IsVendor"] != null)
                        {
                            if (Session["IsVendor"].ToString() == "1")
                            {
                                gvTransInfo.Columns[1].Visible = false;
                                gvTransInfo.Columns[10].Visible = true;
                                gvTransInfo.Columns[0].Visible = true;
                                gvTransInfo.Columns[11].Visible = true;
                                gvTransInfo.Columns[6].Visible = true;
                                LinkButton HLink = (LinkButton)e.Row.Cells[7].Controls[0];
                                HLink.Text = "VENDOR PAID";
                                //gvTransInfo.Columns[6].HeaderText = "VENDOR PAID";
                            }
                            else
                            {
                                LinkButton HLink = (LinkButton)e.Row.Cells[7].Controls[0];
                                HLink.Text = "CUSTOMER PAID";
                                //gvTransInfo.Columns[6].HeaderText = "CUSTOMER PAID";
                            }
                        }
                        //HeaderRow.Cells[2].Text = "DUE";
                    }
                    else
                    {
                        //GridViewRow HeaderRow = gvCustInfo.HeaderRow;
                        gvTransInfo.Columns[0].Visible = false;
                        gvTransInfo.Columns[10].Visible = true;
                        gvTransInfo.Columns[13].Visible = false;
                        gvTransInfo.Columns[6].Visible = false;
                        //HeaderRow.Cells[2].Text = "UNAPPLIED";
                    }
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "javascript:SetMouseOver(this)";
                e.Row.Attributes["onmouseout"] = "javascript:SetMouseOut(this)";
            }
            HiddenField hdnTotalGrossAmount = new HiddenField();
            hdnTotalGrossAmount.Value = "0";
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.Eval(e.Row.DataItem, "Gross") != DBNull.Value)
                    totalTransGross += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Gross"));
                if (DataBinder.Eval(e.Row.DataItem, "Unapplied") != DBNull.Value)
                    totalTransUnapplied += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Unapplied"));
                if (DataBinder.Eval(e.Row.DataItem, "CompanyEstimatedShare") != DBNull.Value)
                    totalTransShare += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CompanyEstimatedShare"));
                if (DataBinder.Eval(e.Row.DataItem, "PaidAmount") != DBNull.Value)
                    TotalPaidAmountTran += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PaidAmount"));
                if (DataBinder.Eval(e.Row.DataItem, "CompanyActualShare") != DBNull.Value)
                    TotalMyShareActualTran += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CompanyActualShare"));
                if (DataBinder.Eval(e.Row.DataItem, "VendorEstimatedShare") != DBNull.Value)
                    TotalVendorEstimatedShare += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "VendorEstimatedShare"));
                hdnTotalGrossAmount = (HiddenField)e.Row.FindControl("hdnTotalGrossAmount");
                TotalGrossAmount = Convert.ToDecimal(hdnTotalGrossAmount.Value);
            }

            if (Session["UserType"].ToString() == "1")
            {
                if (Session["IsVendor"] != null)
                {
                    if (Session["IsVendor"].ToString() == "1")
                    {

                        totalTransGross = TotalGrossAmount;
                    }
                }
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblTotalGross = (Label)e.Row.FindControl("lblTotalGross");
                lblTotalGross.Text = "$" + String.Format("{0:n2}", totalTransGross);
                Label lbltotalUnapplied = (Label)e.Row.FindControl("lbltotalUnapplied");
                lbltotalUnapplied.Text = "$" + String.Format("{0:n2}", totalTransUnapplied);
                Label lbltotalShare = (Label)e.Row.FindControl("lbltotalShare");
                lbltotalShare.Text = "$" + String.Format("{0:n2}", totalTransShare);
                Label lblFooPaidAmount = (Label)e.Row.FindControl("lblFooPaidAmount");
                lblFooPaidAmount.Text = "$" + String.Format("{0:n2}", TotalPaidAmountTran);
                Label lblMyShareTotalShare = (Label)e.Row.FindControl("lblMyShareTotalShare");
                lblMyShareTotalShare.Text = "$" + String.Format("{0:n2}", TotalMyShareActualTran);
                Label lblFooVendorShareEstimated = (Label)e.Row.FindControl("lblFooVendorShareEstimated");
                lblFooVendorShareEstimated.Text = "$" + String.Format("{0:n2}", TotalVendorEstimatedShare);
            }

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "gvTransInfo_RowDataBound", ex.Message);
        }
    }

    protected void lnkCust_Click(object sender, EventArgs e)
    {
        try
        {
            divDealCustgv.Visible = true;
            divTransgv.Visible = false;
            divVendor.Visible = false;
            divStart.Visible = true;
            VendorInfo.Visible = false;
            CustAndDealInfo.Visible = true;

            divTabsCust.Visible = true;
            divTabsTrans.Visible = false;
            if (Session["IsVendor"] != null)
            {
                if (Session["IsVendor"].ToString() == "1")
                {
                    divCustomers.Visible = false;
                    divVendorGrid.Visible = true;
                    lblCustAndDealInfo.Text = "VENDOR AND DEAL INFORMATION WINDOW";
                }
            }
            if (Session["IsVendor"] != null)
            {
                if (Session["IsVendor"].ToString() == "0")
                {
                    divCustomers.Visible = true;
                    divVendorGrid.Visible = false;
                }
            }



            //divCust.Visible = true;
            //CustInfo.Visible = true;

            lnkCust.Enabled = false;
            lnkTrans.Enabled = true;
            lnkCust.Style.Add("color", "Black");
            lnkTrans.Style.Add("color", "Gray");
            lnkCust.Style.Add("background-color", "white");
            lnkTrans.Style.Add("background-color", "#E6E6E6");
            lnkCust.Style.Add("border-bottom", "solid 1px white");
            lnkTrans.Style.Add("border-bottom", "solid 1px black");
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "lnkCust_Click", ex.Message);
        }

    }




    protected void lnkTrans_Click(object sender, EventArgs e)
    {
        try
        {
            divDealCustgv.Visible = false;
            divTransgv.Visible = true;
            divStart.Visible = false;
            divVendorGrid.Visible = false;
            divVendor.Visible = false;
            VendorInfo.Visible = false;
            gvDealInfo.SelectedIndex = -1;
            gvCustInfo.SelectedIndex = -1;

            CustAndDealInfo.Visible = false;
            CustInfo.Visible = false;
            DealInfo.Visible = false;

            divCust.Visible = false;
            divDeal.Visible = false;

            lnkTrans.Enabled = false;
            lnkCust.Enabled = true;
            lnkTrans.Style.Add("color", "Black");
            lnkCust.Style.Add("color", "Gray");
            lnkTrans.Style.Add("background-color", "white");
            lnkCust.Style.Add("background-color", "#E6E6E6");
            lnkTrans.Style.Add("border-bottom", "solid 1px white");
            lnkCust.Style.Add("border-bottom", "solid 1px black");

            divTabsCust.Visible = false;
            divTabsTrans.Visible = true;
            divCustomers.Visible = false;
            // if (Session["dtTransInfo"] == null)
            // {
            gvDealInfo.DataSource = null;
            gvDealInfo.DataBind();
            DealTransactions objDealTransactions = new DealTransactions();

            if (Session["UserID"] != null)
            {
                int userid = Convert.ToInt32(Session["UserID"].ToString());
                ddlTransShow.SelectedValue = "1";
                int ViewType = ddlTransaction.SelectedValue == "" ? 0 : Convert.ToInt16(ddlTransaction.SelectedValue);
                DateTime FromDate = new DateTime(); DateTime ToDate = new DateTime();
                if (txtTransForm.Text != "" && txtTransTo.Text != "")
                {
                    FromDate = Convert.ToDateTime(txtTransForm.Text);
                    ToDate = Convert.ToDateTime(txtTransTo.Text);
                }
                //Alteres by 30-8-13
                if (Session["IsVendor"] != null)
                {
                    if (Session["IsVendor"].ToString() == "1")
                    {
                        //if (ddlTransShow.SelectedIndex == -1)
                        //{

                        // }
                        ddlTransaction.SelectedValue = "3";
                        ViewType = 3;
                        DataSet ds = objDealTransactions.GetVendorTransInfoList(1, txtTransFind.Text, ViewType, FromDate, ToDate);


                        DataTable dt = ds.Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            // Session["dtTransInfo"] = dt;
                            gvTransInfo.DataSource = dt;
                            gvTransInfo.DataBind();
                        }
                    }
                }
                if (Session["IsVendor"] != null)
                {
                    if (Session["IsVendor"].ToString() == "0")
                    {
                        DataSet ds = objDealTransactions.GetTransInfoList(userid, Convert.ToInt32(ddlTransShow.SelectedValue), txtTransFind.Text, ViewType, FromDate, ToDate);

                        DataTable dt = ds.Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            //Session["dtTransInfo"] = dt;
                            gvTransInfo.DataSource = dt;
                            gvTransInfo.DataBind();
                        }
                    }
                }
                //


            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "lnkTrans_Click", ex.Message);
        }
        // }


    }
    protected void btnFillGVDealInfo_Click(object sender, EventArgs e)
    {
        try
        {
            Users obj = new Users();
            if (hdnUserId != null)
            {
                if (hdnUserId.Value != "")
                {
                    obj.UserID = Convert.ToInt32(hdnUserId.Value);
                    if (Session["UserID"] != null)
                    {
                        gvDealInfo.DataSource = null;
                        gvDealInfo.DataBind();
                        //DataSet ds = obj.GetDealInfoListByUserId();
                        //DataTable dt = ds.Tables[0];
                        DataTable dt = (DataTable)DealInfoList();
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            // Session["dt"] = dt;
                            gvDealInfo.DataSource = dt;
                            gvDealInfo.DataBind();
                            gvDealInfo.SelectedIndex = -1;
                        }
                        else
                        {
                            //Session["dt"] = null;
                            gvDealInfo.DataSource = null;
                            gvDealInfo.DataBind();
                            gvDealInfo.SelectedIndex = -1;
                        }
                    }
                    gvDealInfo.SelectedIndex = Convert.ToInt32(hdnDealRowID.Value);
                    lnkCreditAmount.Visible = false;

                    lblOutstanding.Text = "$0.00";
                    trCreditAmount.Visible = true;
                    trCreditAmount.Style.Add("display", "");
                    //trCreditAmount.Style.Add("display", "block");

                    lblCreditAmount.Text = "$" + hdnOutstanding.Value;
                }
              
            }
            if (hdnCompanyID != null)
            {
                if (hdnCompanyID.Value != "")
                {
                    gvDealInfo.DataSource = null;
                    gvDealInfo.DataBind();
                    DataTable dt = (DataTable)DealInfoList();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        gvDealInfo.DataSource = dt;
                        gvDealInfo.DataBind();
                        gvDealInfo.SelectedIndex = -1;
                    }
                    else
                    {
                        gvDealInfo.DataSource = null;
                        gvDealInfo.DataBind();
                        gvDealInfo.SelectedIndex = -1;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "btnFillGVDealInfo_Click", ex.Message);
        }
    }
    protected void btnFillVendorForm_Click(object sender, EventArgs e)
    {
        try
        {
            CustAndDealInfo.Visible = false;
            VendorInfo.Visible = true;
            CustInfo.Visible = false;
            DealInfo.Visible = false;
            divStart.Visible = false;
            Users obj = new Users();
            if (hdnUserId != null)
            {
                if (hdnUserId.Value != "")
                {
                    obj.UserID = Convert.ToInt32(hdnUserId.Value);
                    //obj.ViewDeal = 
                    if (Session["UserID"] != null)
                    {
                        divCust.Visible = false;
                        divDeal.Visible = false;
                        divVendor.Visible = true;
                        gvDealInfo.DataSource = null;
                        gvDealInfo.DataBind();
                        //DataSet ds = obj.GetDealInfoListByUserId();
                        //DataTable dt = ds.Tables[0];
                        ddlViewDealGV.SelectedValue = "3";
                        DataTable dt = (DataTable)DealInfoList();
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            // Session["dt"] = dt;
                            gvDealInfo.DataSource = dt;
                            gvDealInfo.DataBind();
                            gvDealInfo.SelectedIndex = -1;
                        }
                        else
                        {
                            //Session["dt"] = null;
                            gvDealInfo.DataSource = null;
                            gvDealInfo.DataBind();
                            gvDealInfo.SelectedIndex = -1;
                        }
                        //UpdatePanel4.UpdateMode="Always";
                        //UpdatePanel4.Update();
                        DataSet dsVendor = obj.GetVendorInfoList();
                        DataTable dtVendor = dsVendor.Tables[0];
                        if (dtVendor != null && dtVendor.Rows.Count > 0)
                        {
                            lblCompany.Text = dtVendor.Rows[0]["CompanyName"].ToString();
                            lblName.Text = dtVendor.Rows[0]["Name"].ToString();
                            lblEmails.Text = dtVendor.Rows[0]["Email"].ToString();
                            lblAddress1.Text = dtVendor.Rows[0]["Address1"].ToString();
                            lblAddress2.Text = dtVendor.Rows[0]["Address2"].ToString();
                            if (!string.IsNullOrEmpty(dtVendor.Rows[0]["Address1"].ToString()) || !string.IsNullOrEmpty(dtVendor.Rows[0]["Address2"].ToString()))
                            {
                                lblAddress2.Text = dtVendor.Rows[0]["Address2"].ToString() + "<br />";
                            }
                            lblCity.Text = dtVendor.Rows[0]["City"].ToString();
                            if (!string.IsNullOrEmpty(dtVendor.Rows[0]["State"].ToString()))
                            {
                                lblState.Text = dtVendor.Rows[0]["State"].ToString() + "<br />";
                            }

                            lblZipCode.Text = dtVendor.Rows[0]["Zip"].ToString();
                            lblVendorNotes.Text = dtVendor.Rows[0]["UserNotes"].ToString();

                            lblYTDCollected.Text = "$" + String.Format("{0:n2}", Convert.ToDecimal(dtVendor.Rows[0]["YTDAmount"].ToString()));
                            lblCurrentSplit.Text = dtVendor.Rows[0]["CurrSplitLevel"].ToString();
                            lblRemainingNeeded.Text = "$" + String.Format("{0:n2}", Convert.ToDecimal(dtVendor.Rows[0]["RemainingNextSplit"].ToString()));
                            if (dtVendor.Rows[0]["BrokerTypeId"].ToString() != null && dtVendor.Rows[0]["BrokerTypeId"].ToString() == "4")
                            {
                                lblYTD.Visible = true;
                                lblYTDCollected.Visible = true;
                                lblSplit.Visible = true;
                                lblCurrentSplit.Visible = true;
                                lblRemaining.Visible = true;
                                lblRemainingNeeded.Visible = true;
                                btnPostSplitPaid.Visible = true;
                                btnViewBrokerSnapshotDetails.Visible = true;
                            }
                            else
                            {
                                lblYTD.Visible = false;
                                lblYTDCollected.Visible = false;
                                lblSplit.Visible = false;
                                lblCurrentSplit.Visible = false;
                                lblRemaining.Visible = false;
                                lblRemainingNeeded.Visible = false;
                                btnPostSplitPaid.Visible = false;
                                btnViewBrokerSnapshotDetails.Visible = false;
                            }
                        }
                        //Added By Shishir 14-06-2016
                        if (dsVendor.Tables.Count>1 && dsVendor.Tables[1]!=null)
                        {
                            DataTable dtVendorGrid = dsVendor.Tables[1];
                            if (dtVendorGrid != null && dtVendorGrid.Rows.Count > 0)
                            {
                                grdPayment.DataSource = null;
                                grdPayment.DataBind();
                                grdPayment.DataSource = dtVendorGrid;
                                grdPayment.DataBind();
                                grdPayment.Visible = true;
                                btnExportToExcel.Visible = true;
                                btnExporttoPDF.Visible = true;
                            }
                            else
                            {
                                grdPayment.DataSource = null;
                                grdPayment.DataBind();
                                btnExportToExcel.Visible = false;
                                btnExporttoPDF.Visible = false;
                            }
                        }
                        else
                        {
                            grdPayment.DataSource = null;
                            grdPayment.DataBind();
                            btnExportToExcel.Visible = false;
                            btnExporttoPDF.Visible = false;
                        }
                        
                        
                        if (sender == null)
                            gvVendor.SelectedIndex = 0;
                        else
                            gvVendor.SelectedIndex = Convert.ToInt32(hdnVendorRowId.Value);

                    }
                }
            }
            // ScriptManager.RegisterStartupScript(UpdatePanel4, Page.GetType(), "display3", "FixedDealInfoGrid();", true);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "btnFillVendorForm_Click", ex.Message);
        }
    }
    protected void btnFillCustForm_Click(object sender, EventArgs e)
    {
        try
        {
            CustAndDealInfo.Visible = false;
            CustInfo.Visible = true;
            DealInfo.Visible = false;
            divStart.Visible = false;

            Company objCompany = new Company();
            objCompany.CompanyID = Convert.ToInt32(hdnCompanyID.Value);
            if (Session["UserID"] != null)
            {
                int userid = Convert.ToInt32(Session["UserID"].ToString());
                DataSet dsDeal = objCompany.GetDealInfoList(userid);
                DataTable dt = dsDeal.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    //Session["dt"] = dt;
                    gvDealInfo.DataSource = dt;
                    gvDealInfo.DataBind();
                    gvDealInfo.SelectedIndex = -1;
                }
                else
                {
                    // Session["dt"] = null;
                    gvDealInfo.DataSource = null;
                    gvDealInfo.DataBind();
                    gvDealInfo.SelectedIndex = -1;
                }
            }

            divCust.Visible = true;
            divDeal.Visible = false;
            DataSet dsCust = objCompany.GetCustInfoList();

            //Form1
            if (dsCust.Tables[0].Rows.Count > 0)
            {
                Session["CompanyId"] = dsCust.Tables[0].Rows[0]["CompanyID"].ToString();
                lblCustName.Text = dsCust.Tables[0].Rows[0]["Company"].ToString();
                lblContact.Text = dsCust.Tables[0].Rows[0]["Name"].ToString();
                if (!string.IsNullOrEmpty(dsCust.Tables[0].Rows[0]["Email"].ToString()))
                {
                    lblEmail.Text = dsCust.Tables[0].Rows[0]["Email"].ToString();
                    trcompanyemail.Visible = true;
                }
                else
                {
                    lblEmail.Text = string.Empty;
                    trcompanyemail.Visible = false;
                }


                lblBillingAddress1.Text = dsCust.Tables[0].Rows[0]["Address1"].ToString();
                lblBillingAddress2.Text = dsCust.Tables[0].Rows[0]["Address2"].ToString();
                if (!string.IsNullOrEmpty(dsCust.Tables[0].Rows[0]["Address1"].ToString()) || !string.IsNullOrEmpty(dsCust.Tables[0].Rows[0]["Address2"].ToString()))
                {
                    lblBillingAddress2.Text = dsCust.Tables[0].Rows[0]["Address2"].ToString() + "<br/>";
                }
                lblBillingCity.Text = dsCust.Tables[0].Rows[0]["City"].ToString() + ",";
                lblBillingState.Text = dsCust.Tables[0].Rows[0]["State"].ToString();
                lblBillingZip.Text = dsCust.Tables[0].Rows[0]["ZipCode"].ToString();
                lblNotes.Text = dsCust.Tables[0].Rows[0]["CompanyNotes"].ToString();
            }

            if (sender == null)
            {
                gvCustInfo.SelectedIndex = 0;
            }
            else
            {
                gvCustInfo.SelectedIndex = Convert.ToInt32(hdnCustRowID.Value);
            }

            //ScriptManager.RegisterStartupScript(UpdatePanel4, Page.GetType(), "display2", "FixedDealInfoGrid();", true);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "btnFillCustForm_Click", ex.Message);
        }
    }
    protected void btnFillDealFormGVDealInfo_Click(object sender, EventArgs e)
    {
        try
        {

            lblOutstanding.Text = "$0.00";

            lnkCreditAmount.Visible = false;


            //trCreditAmount.Visible = true;
            trCreditAmount.Style.Add("display", "block");
            trCreditAmount.Style.Add("display", "");
            //trCreditAmount.Style.Add("display", "block");

            lblCreditAmount.Text = "$" + hdnOutstanding.Value;




        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "btnFillDealForm_Click", ex.Message);
        }
    }
    protected void btnFillDealForm_Click(object sender, EventArgs e)
    {
        try
        {
            CustAndDealInfo.Visible = false;
            CustInfo.Visible = false;
            DealInfo.Visible = true;
            divStart.Visible = false;
            divVendor.Visible = false;
            divCust.Visible = false;
            divDeal.Visible = true;
            VendorInfo.Visible = false;
            trDealUse.Visible = false;
            trTenant.Visible = false;
            trBuyer.Visible = false;
            DealTransactions objDealTransactions = new DealTransactions();
            objDealTransactions.DealTransactionID = Convert.ToInt32(hdnDealTransactionID.Value);
            objDealTransactions.CommissionDueDateId = Convert.ToInt32(hdnCommissionDueDateId.Value);
            DataSet ds = objDealTransactions.GetDealInfoUsingTransIDList();

            //Form2
            if (ds.Tables[0].Rows.Count > 0)
            {
                Session["ChargeSlipId"] = ds.Tables[0].Rows[0]["ChargeSlipID"].ToString();
                lblDealName.Text = ds.Tables[0].Rows[0]["DealName"].ToString();
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["LocShoppingCenter"].ToString()))
                {
                    lblDealNames.Text = ds.Tables[0].Rows[0]["LocShoppingCenter"].ToString() + "<br />";
                }
                else
                {
                    lblDealNames.Text = "";
                }
                lblBillingAddress1Form2.Text = ds.Tables[0].Rows[0]["LocAddress1"].ToString();
                lblBillingAddress2Form2.Text = ds.Tables[0].Rows[0]["LocAddress2"].ToString();
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["LocAddress1"].ToString()) || !string.IsNullOrEmpty(ds.Tables[0].Rows[0]["LocAddress2"].ToString()))
                {
                    lblBillingAddress2Form2.Text = ds.Tables[0].Rows[0]["LocAddress2"].ToString() + "<br />";
                }
                else
                {
                    lblBillingAddress2Form2.Text = "";
                }
                lblBillingCityForm2.Text = ds.Tables[0].Rows[0]["LocCity"].ToString() + ",";
                lblBillingStateForm2.Text = ds.Tables[0].Rows[0]["LocState"].ToString();
                lblBillingZipForm2.Text = ds.Tables[0].Rows[0]["LocZipCode"].ToString();
                //lblChargeToName.Text = ds.Tables[0].Rows[0]["ContactName"].ToString();
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["Company"].ToString()))
                {
                    lblChargeToCompany.Text = ds.Tables[0].Rows[0]["Company"].ToString() + "<br />";
                }
                else
                {
                    lblChargeToCompany.Text = "";
                }
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["CTOwnershipEntity"].ToString()))
                {
                    lblOwnerShipEntity.Text = ds.Tables[0].Rows[0]["CTOwnershipEntity"].ToString() + "<br />";
                }
                else
                {
                    lblOwnerShipEntity.Text = "";
                }
                lblChargeToAddress1.Text = ds.Tables[0].Rows[0]["CTAddress1"].ToString();
                lblChargeToAddress2.Text = ds.Tables[0].Rows[0]["CTAddress2"].ToString();
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["CTAddress1"].ToString()) || !string.IsNullOrEmpty(ds.Tables[0].Rows[0]["CTAddress2"].ToString()))
                {
                    lblChargeToAddress2.Text = ds.Tables[0].Rows[0]["CTAddress2"].ToString() + "<br />";
                }
                else
                {
                    lblChargeToAddress2.Text = "";
                }
                lblChargeToCity.Text = ds.Tables[0].Rows[0]["CTCity"].ToString() + ",";
                lblChargeToState.Text = ds.Tables[0].Rows[0]["CTState"].ToString();
                lblChargeToZip.Text = ds.Tables[0].Rows[0]["CTZipCode"].ToString();

                //Form3
                lblContactForm2.Text = ds.Tables[0].Rows[0]["ContactName"].ToString();
                lblCompanyEmail.Text = ds.Tables[0].Rows[0]["CTEmail"].ToString();

                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["DealUse"].ToString()))
                {
                    trDealUse.Visible = true;
                    lblUse.Text = ds.Tables[0].Rows[0]["DealUse"].ToString();
                }
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["Tenant"].ToString()))
                {
                    trTenant.Visible = true;
                    trBuyer.Visible = false;
                    lblTenant.Text = ds.Tables[0].Rows[0]["Name"].ToString();
                }
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["Buyer"].ToString()))
                {
                    trBuyer.Visible = true;
                    trTenant.Visible = false;
                    lblBuyer.Text = ds.Tables[0].Rows[0]["Name"].ToString();
                }



                lblAmountDue.Text = "$" + String.Format("{0:n2}", Convert.ToDecimal(ds.Tables[0].Rows[0]["AmountDue"].ToString()));
                lblOutstanding.Text = "$" + String.Format("{0:n2}", Convert.ToDecimal(ds.Tables[0].Rows[0]["Outstanding"].ToString()));
                hdnOutstanding.Value = String.Format("{0:n2}", Convert.ToDecimal(ds.Tables[0].Rows[0]["Outstanding"].ToString()));
                lblNotesForm2.Text = ds.Tables[0].Rows[0]["Notes"].ToString();
                if (Convert.ToDecimal(ds.Tables[0].Rows[0]["Outstanding"].ToString()) == 0)
                {
                    lnkCreditAmount.Visible = false;
                }
                else
                {
                    lnkCreditAmount.Visible = true;
                }
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["CreditAmount"].ToString()))
                {
                    lblCreditAmount.Text = "$" + ds.Tables[0].Rows[0]["CreditAmount"].ToString();
                    trCreditAmount.Visible = true;
                    trCreditAmount.Style.Add("display", "");
                    //trCreditAmount.Style.Add("display", "block");
                }
                else
                {
                    lblCreditAmount.Text = string.Empty;
                    trCreditAmount.Style.Add("display", "none");
                    // trCreditAmount.Visible = false;
                }
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["OverPaymentAmount"].ToString()))
                {
                    if (Convert.ToDecimal(ds.Tables[0].Rows[0]["OverPaymentAmount"].ToString()) > 0)
                    {
                        lblOverPaymentAmount.Text = "$" + ds.Tables[0].Rows[0]["OverPaymentAmount"].ToString();
                        trOverPaymentAmount.Visible = true;
                        trOverPaymentAmount.Style.Add("display", "");
                    }
                    else
                    {
                        lblOverPaymentAmount.Text = string.Empty;
                        trOverPaymentAmount.Style.Add("display", "none");
                    }

                    //trCreditAmount.Style.Add("display", "block");
                }
                else
                {
                    lblOverPaymentAmount.Text = string.Empty;
                    trOverPaymentAmount.Style.Add("display", "none");
                    // trCreditAmount.Visible = false;
                }
                //txtNotes.Text = ds.Tables[0].Rows[0]["Notes"].ToString();
                lblDueDate.Text = ds.Tables[0].Rows[0]["DueDate"].ToString();
                //if (ds.Tables[1].Rows.Count > 0 && ds.Tables[1] != null)
                //{
                //    lblDueDate1.Text = string.Empty;
                //    lblDueDate2.Text = string.Empty;
                //    tdDueDate2.Visible = true;
                //    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                //    {
                //        tableDueDate.Style.Add("border", "none");
                //        if (ds.Tables[1].Rows.Count == 1)
                //        {
                //            tdDueDate2.Visible = false;

                //        }


                //        if (i % 2 == 0)
                //        {
                //            lblDueDate1.Text = lblDueDate1.Text + "" + ds.Tables[1].Rows[i]["DueDate"].ToString() + "<br />";
                //        }
                //        else
                //        {
                //            lblDueDate2.Text = lblDueDate2.Text + "&nbsp;&nbsp; &nbsp; " + ds.Tables[1].Rows[i]["DueDate"].ToString() + "<br />";
                //        }



                //    }


                //    if (ds.Tables[1].Rows.Count > 1 && ds.Tables[1].Rows.Count % 2 != 0)
                //    {
                //        lblDueDate2.Text = lblDueDate2.Text + "" + "<br />";
                //    }



                //}
            }



            gvDealInfo.SelectedIndex = Convert.ToInt32(hdnDealRowID.Value);
            //ScriptManager.RegisterStartupScript(UpdatePanel4, Page.GetType(), "display6", "FixedDealInfoGrid();", true);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "btnFillDealForm_Click", ex.Message);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            txtStartDate.Text = "";
            txtEndDate.Text = "";
            ddlViewDealGV.SelectedIndex = 0;
            DataTable dataTable = (DataTable)DealInfoList();
            //DataTable dataTable = (DataTable)Session["dt"];
            if (dataTable != null)
            {
                if (dataTable.Rows.Count > 0)
                {
                    gvDealInfo.DataSource = dataTable;
                    gvDealInfo.DataBind();
                }
            }
            else
            {
                gvDealInfo.DataSource = null;
                gvDealInfo.DataBind();
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "btnDisplay_Click", ex.Message);
        }
    }

    protected void imgExport_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            //txtNotes.Visible = true;
            //txtNotes.Style.Add("width","320px");
            //lblNotesForm2.Visible = false;
            if (CustAndDealInfo.Visible != true)
            {
                imgEdit.Visible = false;
                imgEditForm2.Visible = false;
                imgVendorEdit.Visible = false;
                //HideControls(FormView1);
                // HideControls(FormView2);
                Response.ClearContent();
                Response.Buffer = true;
                //Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Customers.xls"));

                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);

                if (lnkCust.Enabled == false)
                {
                    HideControls(gvDealInfo);
                    foreach (GridViewRow row in gvDealInfo.Rows)
                    {
                        row.Style.Add("font-size", "12px");
                    }
                    GridViewRow HeaderRow = gvDealInfo.HeaderRow;
                    HeaderRow.Height = 20;
                    HeaderRow.Style.Add("font-size", "12px");
                    GridViewRow FooterRow = gvDealInfo.FooterRow;
                    FooterRow.Height = 20;
                    FooterRow.Style.Add("font-size", "12px");
                    if (DealInfo.Visible == true)
                    {
                        Response.AddHeader("content-disposition", "attachment;filename=" + lblDealName.Text + ".xls");
                        //Response.AddHeader("content-disposition", string.Format("attachment; filename={0},"+lblDealName.Text + ".xls"));
                        DealInfo.RenderControl(htw);
                        //       FormView2.RenderControl(htw);
                        //   FormView3.RenderControl(htw);
                        Form2.Style.Add("font-size", "11.5px");
                        Form3.Style.Add("font-size", "11.5px");
                        Form2.Style.Add("font-family", "Verdana");
                        Form3.Style.Add("font-family", "Verdana");

                        Form2.RenderControl(htw);
                        Form3.RenderControl(htw);
                        //tdNotes.Visible = true;
                        trNotes.RenderControl(htw);
                        htw.WriteBreak();
                        gvDealInfo.RenderControl(htw);


                    }
                    else if (CustInfo.Visible == true)
                    {
                        Response.AddHeader("content-disposition", "attachment;filename=" + lblCustName.Text + ".xls");
                        CustInfo.RenderControl(htw);
                        Form1.Style.Add("font-family", "Verdana");
                        Form1.Style.Add("font-size", "11.5px");
                        Form1.RenderControl(htw);
                        htw.WriteBreak();
                        gvDealInfo.RenderControl(htw);
                    }
                    else if (VendorInfo.Visible == true)
                    {
                        Response.AddHeader("content-disposition", "attachment;filename=" + lblName.Text + ".xls");
                        VendorInfo.RenderControl(htw);
                        Form5.Style.Add("font-family", "Verdana");
                        Form5.Style.Add("font-size", "11.5px");
                        Form5.RenderControl(htw);
                        htw.WriteBreak();
                        gvDealInfo.RenderControl(htw);
                    }
                }
                else
                {
                    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "TransactionList.xls"));

                    HideControlTransaction(gvTransInfo);
                    foreach (GridViewRow row in gvTransInfo.Rows)
                    {
                        row.Style.Add("font-size", "12px");
                    }
                    GridViewRow HeaderRow = gvTransInfo.HeaderRow;
                    HeaderRow.Height = 20;
                    HeaderRow.Style.Add("font-size", "12px");
                    GridViewRow FooterRow = gvTransInfo.FooterRow;
                    FooterRow.Height = 20;
                    FooterRow.Style.Add("font-size", "12px");
                    gvTransInfo.RenderControl(htw);
                }

                Response.Write(sw.ToString());
                //tdNotes.Visible = false;
                Response.Flush();
                Response.End();
            }
            //lblNotesForm2.Visible = true;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "imgExport_Click", ex.Message);
        }
    }

    protected void imgPDF_Click(object sender, ImageClickEventArgs e)
    {
        try
        {

            //txtNotes.Visible = false;
            //lblNotesForm2.Visible = true;
            if (CustAndDealInfo.Visible != true)
            {
                imgEdit.Visible = false;
                imgEditForm2.Visible = false;
                imgVendorEdit.Visible = false;
                // HideControls(FormView1);
                //   HideControls(FormView2);
                Response.ClearContent();
                Response.Buffer = true;
                //Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf");
                Response.ContentType = "application/pdf";
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                if (lnkCust.Enabled == false)
                {
                    HideControls(gvDealInfo);
                    foreach (GridViewRow row in gvDealInfo.Rows)
                    {
                        row.Style.Add("font-size", "7px");
                    }
                    GridViewRow HeaderRow = gvDealInfo.HeaderRow;
                    HeaderRow.Height = 20;
                    HeaderRow.Style.Add("font-size", "7px");
                    GridViewRow FooterRow = gvDealInfo.FooterRow;
                    FooterRow.Height = 20;
                    FooterRow.Style.Add("font-size", "7px");
                    if (DealInfo.Visible == true)
                    {
                        //this.Page.RenderControl(hw);
                        Response.AddHeader("content-disposition", "attachment;filename=" + lblDealName.Text + ".pdf");
                        DealInfo.RenderControl(hw);
                        //FormView2.RenderControl(hw);
                        //FormView3.RenderControl(hw);
                        Form2.RenderControl(hw);
                        Form3.RenderControl(hw);
                        //tdNotes.Visible = true;
                        trNotes.RenderControl(hw);
                        hw.WriteBreak();
                        gvDealInfo.RenderControl(hw);
                    }
                    else if (CustInfo.Visible == true)
                    {
                        Response.AddHeader("content-disposition", "attachment;filename=" + lblCustName.Text + ".pdf");
                        //this.Page.RenderControl(hw);
                        CustInfo.RenderControl(hw);
                        //FormView1.RenderControl(hw);
                        Form1.RenderControl(hw);
                        hw.WriteBreak();
                        gvDealInfo.RenderControl(hw);
                    }
                    else if (VendorInfo.Visible == true)
                    {
                        Response.AddHeader("content-disposition", "attachment;filename=" + lblName.Text + ".pdf");
                        VendorInfo.RenderControl(hw);
                        Form5.RenderControl(hw);
                        hw.WriteBreak();
                        gvDealInfo.RenderControl(hw);
                    }
                }
                else
                {
                    Response.AddHeader("content-disposition", "attachment;filename=TransactionList.pdf");
                    HideControlTransaction(gvTransInfo);
                    foreach (GridViewRow row in gvTransInfo.Rows)
                    {
                        row.Style.Add("font-size", "7px");
                    }
                    GridViewRow HeaderRow = gvTransInfo.HeaderRow;
                    HeaderRow.Height = 20;
                    HeaderRow.Style.Add("font-size", "7px");
                    GridViewRow FooterRow = gvTransInfo.FooterRow;
                    FooterRow.Height = 20;
                    FooterRow.Style.Add("font-size", "7px");
                    gvTransInfo.RenderControl(hw);
                }

                //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                //Document pdfDoc = new Document(new Rectangle(8.5f,11f), 10f, 10f, 10f, 0f);
                Document pdfDoc = new Document(new Rectangle(612f, 792f), 36f, 36f, 36f, 36f);

                pdfDoc.HtmlStyleClass = "SetWidth";
                //pdfDoc.SetPageSize("8.5x11");
                //Document pdfDoc = new Document(PageSize._8.5*11, 10f, 10f, 10f, 0f);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                StringReader sr = new StringReader(sw.ToString());
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                //tdNotes.Visible = false;
                Response.End();


            }



        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "imgPDF_Click", ex.Message);
        }
    }

    private void HideControls(GridView gvDealInfo)
    {
        try
        {
            foreach (GridViewRow row in gvDealInfo.Rows)
            {
                row.Height = 20;
                ImageButton img = (ImageButton)row.FindControl("imgView");
                ImageButton imgPayHistory = (ImageButton)row.FindControl("imgPaysHistory");
                ImageButton imgPaymentHistory = (ImageButton)row.FindControl("imgPaymentHistory");
                ImageButton imgPayment = (ImageButton)row.FindControl("imgPayment");
                //Add for Payment Void on 15-07-2016
                ImageButton imgVoidPayment = (ImageButton)row.FindControl("imgVoidPayment");
                if (img != null)
                {
                    img.Visible = false;
                }
                if (imgPayHistory != null)
                {
                    imgPayHistory.Visible = false;
                }
                if (imgPaymentHistory != null)
                {
                    imgPaymentHistory.Visible = false;
                }
                if (imgPayment != null)
                {
                    imgPayment.Visible = false;
                }
                if (imgVoidPayment != null)
                {
                    imgVoidPayment.Visible = false;
                }
                row.Style.Add("font-size", "9px");
            }

            GridViewRow HeaderRow = gvDealInfo.HeaderRow;
            HeaderRow.Cells[0].Text = "DEAL NAME";
            //HeaderRow.Cells[1].Text = "DEAL TYPE";
            HeaderRow.Cells[1].Text = "DUE DATE";
            HeaderRow.Cells[2].Text = "GROSS";
            HeaderRow.Cells[3].Text = "PAID DATE";
            HeaderRow.Cells[4].Text = "PAID AMT";
            HeaderRow.Cells[5].Text = "";
            HeaderRow.Cells[6].Text = "UNAPPLIED";
            HeaderRow.Cells[7].Text = "MY SHARE ESTIMATED";
            HeaderRow.Cells[8].Text = "MY SHARE ACTUAL";


            if (Session["UserID"] != null)
            {
                if (Session["UserType"].ToString() == "1")
                {
                    HeaderRow.Cells[7].Text = "COMPANY SHARE ESTIMATED";
                    HeaderRow.Cells[8].Text = "COMPANY SHARE ACTUAL";
                    if (Session["IsVendor"] != null)
                    {
                        if (Session["IsVendor"].ToString() == "1")
                        {
                            HeaderRow.Cells[4].Text = "VENDOR PAID";
                        }
                        else
                        {
                            HeaderRow.Cells[4].Text = "CUSTOMER PAID";
                        }
                    }

                }
            }

            HeaderRow.Height = 20;
            HeaderRow.Style.Add("font-size", "9px");
            HeaderRow.Style.Add("text-align", "center");
            gvDealInfo.Columns[5].Visible = false;
            gvDealInfo.Columns[9].Visible = false;
            gvDealInfo.Columns[10].Visible = false;
            gvDealInfo.Columns[11].Visible = false;
            //Add for Payment Void on 15-07-2016
            gvDealInfo.Columns[12].Visible = false;
            GridViewRow FooterRow = gvDealInfo.FooterRow;
            FooterRow.Height = 20;
            FooterRow.Style.Add("font-size", "9px");
            FooterRow.Cells[2].Style.Add("text-align", "right");
            FooterRow.Cells[4].Style.Add("text-align", "right");
            FooterRow.Cells[6].Style.Add("text-align", "right");
            FooterRow.Cells[7].Style.Add("text-align", "right");
            FooterRow.Cells[8].Style.Add("text-align", "right");



        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "HideControls", ex.Message);
        }
    }
    private void HideControlTransaction(GridView gvTransinfo)
    {
        try
        {
            foreach (GridViewRow row in gvTransinfo.Rows)
            {
                row.Height = 20;
                ImageButton img = (ImageButton)row.FindControl("imgView");
                ImageButton imgPayHistory = (ImageButton)row.FindControl("imgPaysHistory");
                ImageButton imgPaymentHistory = (ImageButton)row.FindControl("imgPaymentHistory");
                ImageButton imgPayment = (ImageButton)row.FindControl("imgPayment");
                if (img != null)
                {
                    img.Visible = false;
                }
                if (imgPayHistory != null)
                {
                    imgPayHistory.Visible = false;
                }
                if (imgPaymentHistory != null)
                {
                    imgPaymentHistory.Visible = false;
                }
                if (imgPayment != null)
                {
                    imgPayment.Visible = false;
                }
                row.Style.Add("font-size", "8px");
            }


            GridViewRow HeaderRow = gvTransinfo.HeaderRow;
            HeaderRow.Cells[0].Text = "VENDORS";
            HeaderRow.Cells[1].Text = "CUSTOMERS";
            HeaderRow.Cells[2].Text = "DEAL NAME";
            HeaderRow.Cells[3].Text = "DUE DATE";
            HeaderRow.Cells[4].Text = "GROSS";
            HeaderRow.Cells[5].Text = "PAID DATE";
            HeaderRow.Cells[6].Text = "Vendor Estimated Share";
            HeaderRow.Cells[7].Text = "PAID AMT";
            HeaderRow.Cells[8].Text = "";
            HeaderRow.Cells[9].Text = "UNAPPLIED";
            HeaderRow.Cells[10].Text = "MY SHARE ESTIMATED";
            HeaderRow.Cells[11].Text = "MY SHARE ACTUAL";
            HeaderRow.Height = 20;
            HeaderRow.Style.Add("font-size", "8px");
            HeaderRow.Style.Add("text-align", "center");

            if (Session["UserID"] != null)
            {
                if (Session["UserType"].ToString() == "1")
                {
                    HeaderRow.Cells[10].Text = "COMPANY SHARE ESTIMATED";
                    HeaderRow.Cells[11].Text = "COMPANY SHARE ACTUAL";
                    if (Session["IsVendor"] != null)
                    {
                        if (Session["IsVendor"].ToString() == "1")
                        {
                            HeaderRow.Cells[7].Text = "VENDOR PAID";
                        }
                        else
                        {
                            HeaderRow.Cells[7].Text = "CUSTOMER PAID";
                        }
                    }

                }
            }

            GridViewRow FooterRow = gvTransinfo.FooterRow;
            FooterRow.Height = 20;
            FooterRow.Style.Add("font-size", "8px");
            FooterRow.Cells[4].Style.Add("text-align", "right");
            FooterRow.Cells[6].Style.Add("text-align", "right");
            FooterRow.Cells[7].Style.Add("text-align", "right");
            FooterRow.Cells[9].Style.Add("text-align", "right");
            FooterRow.Cells[10].Style.Add("text-align", "right");
            FooterRow.Cells[11].Style.Add("text-align", "right");
            gvTransinfo.Columns[8].Visible = false;
            gvTransinfo.Columns[12].Visible = false;
            gvTransinfo.Columns[13].Visible = false;

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "HideControlTransaction", ex.Message);
        }
    }
    private void HideControls(FormView form)
    {
        try
        {
            FormViewRow row = form.Row;
            row.Height = 50;
            row.Style.Add("font-size", "20px");
            ImageButton img = (ImageButton)row.FindControl("imgEdit");
            if (img != null)
            {
                img.Visible = false;
            }
            ImageButton imgEditForm2 = (ImageButton)row.FindControl("imgEditForm2");
            if (imgEditForm2 != null)
            {
                imgEditForm2.Visible = false;
            }

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "HideControls", ex.Message);
        }
    }

    protected void gvTransInfo_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "View")
            {
                //divChargeDetailCard.Visible = true;
                ucChargeDetailCard1.ChargeSlipId = Convert.ToInt32(e.CommandArgument);
                Session["FileAttachment"] = null;
                ucChargeDetailCard1.Bindata();
                hdnPopup.Value = "1";
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "display", "display();", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "display", "display();", true);
            }
            if (e.CommandName == "PayHistory")
            {
                //hdnPopup.Value = "0";
                //divPaymentHistoryDetail.Visible = true;
                ucPaymentHistoryDetail1.ChargeSlipId = Convert.ToInt32(e.CommandArgument);
                ucPaymentHistoryDetail1.Bindata();

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "display", "display();", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "display", "display();", true);
            }
            //if (e.CommandName == "Payment")
            //{
            //    ScriptManager.RegisterStartupScript(UpdatePanel4, typeof(string), "PaymentRedirect", "PaymentLink(" + e.CommandArgument + ")", true);

            //}
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "gvTransInfo_RowCommand", ex.Message);
        }

    }
    protected void gvDealInfo_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "View")
            {
                //hdnPopup.Value = "0";
                //divChargeDetailCard.Visible = true;
                ucChargeDetailCard1.ChargeSlipId = Convert.ToInt32(e.CommandArgument);
                ucChargeDetailCard1.Bindata();

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "display", "display();", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "display", "display();", true);
            }
            //if (e.CommandName == "Del")
            //{
            //    txtDeleteCharslipReason.Text = "";
            //    int chargeslipid = Convert.ToInt32(e.CommandArgument);
            //    ChargeSlip objChargeSlip = new ChargeSlip();
            //    objChargeSlip.ChargeSlipID = chargeslipid;

            //    objChargeSlip.DeleteReason = txtDeleteCharslipReason.Text;
            //    objChargeSlip.SetDeleteChargeSlip();

            //    //ScriptManager.RegisterStartupScript(UpdatePanel5, Page.GetType(), "display", "display();", true);


            //}
            if (e.CommandName == "PayHistory1")
            {
                //divPaymentHistoryDetail.Visible = true;
                ucPaymentHistoryDetail1.ChargeSlipId = Convert.ToInt32(e.CommandArgument);
                ucPaymentHistoryDetail1.Bindata();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "display", "display();", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "display", "display();", true);
            }
            //if (e.CommandName == "Payment")
            //{
            //    ScriptManager.RegisterStartupScript(UpdatePanel4, typeof(string), "PaymentRedirect", "PaymentLink("+e.CommandArgument+")", true);

            //}
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "gvDealInfo_RowCommand", ex.Message);
        }
    }


    public void CustDiv()
    {
        divDealCustgv.Visible = true;
        divTransgv.Visible = false;
        divStart.Visible = true;
        CustAndDealInfo.Visible = true;

        DealInfo.Visible = false;
        divDeal.Visible = false;
        CustInfo.Visible = false;
        divCust.Visible = false;

        divTabsCust.Visible = true;
        divTabsTrans.Visible = false;
        divCustomers.Visible = true;
    }
    protected void btnDisplay_Click(object sender, EventArgs e)
    {
        try
        {


            DataTable dt = (DataTable)DealInfoList();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvDealInfo.DataSource = dt;
                    gvDealInfo.DataBind();
                }
                else
                {
                    gvDealInfo.DataSource = null;
                    gvDealInfo.DataBind();
                }
            }
            else
            {
                gvDealInfo.DataSource = null;
                gvDealInfo.DataBind();
            }

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "btnDisplay_Click", ex.Message);
        }
    }
    private DataTable DealInfoList()
    {
        DataTable dataTable = new DataTable();
        try
        {

            if (Session["IsVendor"] != null)
            {
                if (Session["IsVendor"].ToString() == "1")
                {
                    Users obj = new Users();
                    if (hdnUserId != null)
                    {
                        if (hdnUserId.Value != "")
                        {
                            obj.UserID = Convert.ToInt32(hdnUserId.Value);
                            if (Session["UserID"] != null)
                            {
                                DataSet ds = obj.GetDealInfoListByUserId();
                                if (ds != null)
                                {
                                    dataTable = ds.Tables[0];
                                }
                            }
                        }
                    }
                }
                else if (Session["IsVendor"].ToString() == "0")
                {
                    Company objCompany = new Company();
                    objCompany.CompanyID = Convert.ToInt32(hdnCompanyID.Value);
                    if (Session["UserID"] != null)
                    {
                        int userid = Convert.ToInt32(Session["UserID"].ToString());
                        DataSet dsDeal = objCompany.GetDealInfoList(userid);
                        if (dsDeal != null)
                        {
                            dataTable = dsDeal.Tables[0];

                        }

                    }
                }
            }



            if (dataTable.Rows.Count > 0)
            {
                // DataTable dataTable = (DataTable)Session["dt"];
                if (dataTable != null)
                {
                    DataView view = new DataView();
                    view.Table = dataTable;
                    string strquery = string.Empty;
                    if (ddlViewDealGV.SelectedValue == "3")
                    {
                        strquery = "IsFullCommPaid=0 ";
                    }
                    else if (ddlViewDealGV.SelectedValue == "4")
                    {
                        strquery = "IsFullCommPaid=1 ";
                    }
                    else if (ddlViewDealGV.SelectedValue == "2")
                    {
                        strquery = "DueDateYear= " + DateTime.Now.Year + " ";
                    }
                    if (!string.IsNullOrEmpty(txtStartDate.Text.Trim()) && !string.IsNullOrEmpty(txtEndDate.Text.Trim()))
                    {
                        if (ddlViewDealGV.SelectedIndex == -1 || ddlViewDealGV.SelectedValue == "1")
                        {
                            view.RowFilter = "DueDates >= '" + txtStartDate.Text.Trim() + "' AND DueDates <=' " + txtEndDate.Text.Trim() + "'";

                        }
                        else
                        {
                            view.RowFilter = strquery + "AND (DueDates >= '" + txtStartDate.Text.Trim() + "' AND DueDates <=' " + txtEndDate.Text.Trim() + "')";

                        }
                    }
                    else
                    {
                        view.RowFilter = strquery;
                    }
                    dataTable = view.ToTable();
                    return dataTable;
                }
            }
            return dataTable;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "DealInfoList", ex.Message);
            return dataTable;
        }
    }
    protected void btnTransDisplay_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = (DataTable)TransInfoList();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    gvTransInfo.DataSource = dt;
                    gvTransInfo.DataBind();
                }
                else
                {
                    gvTransInfo.DataSource = null;
                    gvTransInfo.DataBind();
                }
            }
            else
            {
                gvTransInfo.DataSource = null;
                gvTransInfo.DataBind();
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "btnTransDisplay_Click", ex.Message);
        }

    }
    private DataTable TransInfoList()
    {
        DataTable dataTable = new DataTable();
        try
        {

            DealTransactions objDealTransactions = new DealTransactions();
            if (Session["UserID"] != null)
            {
                int userid = Convert.ToInt32(Session["UserID"].ToString());
                //Alteres by 30-8-13
                if (Session["IsVendor"] != null)
                {
                    int ViewType = ddlTransaction.SelectedValue == "" ? 0 : Convert.ToInt16(ddlTransaction.SelectedValue);
                    DateTime FromDate = new DateTime(); DateTime ToDate = new DateTime();
                    if (txtTransForm.Text != "" && txtTransTo.Text != "")
                    {
                        FromDate = Convert.ToDateTime(txtTransForm.Text);
                        ToDate = Convert.ToDateTime(txtTransTo.Text);
                    }
                    if (ddlTransShow.SelectedIndex == -1)
                    {
                        ddlTransShow.SelectedValue = "1";
                    }
                    if (Session["IsVendor"].ToString() == "1")
                    {

                        DataSet ds = objDealTransactions.GetVendorTransInfoList(Convert.ToInt32(ddlTransShow.SelectedValue.ToString()), txtTransFind.Text, ViewType, FromDate, ToDate);

                        if (ds != null)
                        {
                            dataTable = ds.Tables[0];
                        }
                    }
                    else if (Session["IsVendor"].ToString() == "0")
                    {
                        DataSet ds = objDealTransactions.GetTransInfoList(userid, Convert.ToInt32(ddlTransShow.SelectedValue), txtTransFind.Text, ViewType, FromDate, ToDate);
                        if (ds != null)
                        {
                            dataTable = ds.Tables[0];

                        }
                    }
                }



                //


            }

            //DataTable dataTable = (DataTable)Session["dtTransInfo"];
            //if (dataTable != null)
            //{
            //    if (dataTable.Rows.Count > 0)
            //    {
            //        DataView view = new DataView();
            //        view.Table = dataTable;
            //        string strquery = string.Empty;
            //        if (ddlTransaction.SelectedValue == "3")
            //        {
            //            strquery = "IsFullCommPaid=0 ";
            //        }
            //        else if (ddlTransaction.SelectedValue == "4")
            //        {
            //            strquery = "IsFullCommPaid=1 ";
            //        }
            //        else if (ddlTransaction.SelectedValue == "2")
            //        {
            //            strquery = "DueDateYear= " + DateTime.Now.Year + " ";
            //        }
            //        if (!string.IsNullOrEmpty(txtTransForm.Text.Trim()) && !string.IsNullOrEmpty(txtTransTo.Text.Trim()))
            //        {
            //            if (ddlTransaction.SelectedIndex == -1 || ddlTransaction.SelectedValue == "1")
            //            {
            //                view.RowFilter = "DueDates >= '" + txtTransForm.Text.Trim() + "' AND DueDates <=' " + txtTransTo.Text.Trim() + "'";

            //            }
            //            else
            //            {
            //                view.RowFilter = strquery + "AND (DueDates >= '" + txtTransForm.Text.Trim() + "' AND DueDates <=' " + txtTransTo.Text.Trim() + "')";

            //            }
            //        }
            //        else
            //        {
            //            view.RowFilter = strquery;
            //        }
            //        dataTable = view.ToTable();
            //        return dataTable;
            //        //gvTransInfo.DataSource = view;
            //        //gvTransInfo.DataBind();
            //    }
            //}
            return dataTable;
        }
        catch (Exception ex)
        {

            ErrorLog.WriteLog("ucBilledTab.ascx", "TransInfoList", ex.Message);
            return dataTable;
        }
    }
    //protected void imgPaysHistory_Click(object sender, ImageClickEventArgs e)
    //{
    //    ImageButton btn = (ImageButton)sender;
    //    ucPaymentHistoryDetail1.ChargeSlipId = Convert.ToInt32(btn.CommandArgument);
    //    ucPaymentHistoryDetail1.Bindata();

    //}
    protected void imgEditForm2_Click(object sender, ImageClickEventArgs e)
    {
        if (hdnChargeSlip != null)
        {
            // string fileToSelect = "../Pages/NewCharge.aspx";
            //int ChargeSlipId = Convert.ToInt32(hdnChargeSlip.Value);
            //GetChargeSlipDetails(ChargeSlipId);
            //GetChargeSlipDetails(1224);
            //ScriptManager.RegisterStartupScript(UpdatePanel3, Page.GetType(), "display", "javascript:<script>alert('hi');</script>", false );
            //ClientScript.RegisterStartupScript(this.GetType(), "OpenWin", "<script>alert('Not Found!');</script>");
            //ScriptManager.RegisterStartupScript(UpdatePanel3, typeof(string), "ShowPopup", "<script>alert('hi');</script>", true);
            //ScriptManager.RegisterStartupScript(UpdatePanel3, typeof(string), "ShowPopup", "javascript: Navigate();return false;", true);
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWin", "<script>window.open('" + fileToSelect + "', 'mynewwin', 'width=600,height=700,resizable=yes');</script>");
            //   Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWin1", "javascript:test();", true);
            // Page.ClientScript.RegisterStartupScript(GetType(), "myScript", "javascript:test();", true);
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "display", "javascript:alert('hi');", true);
            //ScriptManager.RegisterStartupScript(this, Page.GetType(), "Navigate", "<script>alert('hi');</script>", true);
            // ScriptManager.RegisterStartupScript(this, Page.GetType(), "Navigate", "javascript:return Navigate('" + fileToSelect + "')", true);
            //txtLocZipCode.Attributes.Add("onkeyup", "javascript:return ShoppingCenterAddressChanged()");

        }
    }

    protected void imgEdit_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            lbTitle.Text = "UPDATE COMPANY";
            ucNewCenterOrCompany.Name = "COMPANY";
            ucNewCenterOrCompany.ClassRef = "C";
            ucNewCenterOrCompany.PopupType = "C";
            //ucNewCenterOrCompany.CenterName = txtComapny.Text;
            ucNewCenterOrCompany.IsAddressOnly = false; // - SK 07/23
            ucNewCenterOrCompany.IsUpdate = true;
            ucNewCenterOrCompany.IsUpdateBilled = true;
            ucNewCenterOrCompany.IsAdmin = true;
            ucNewCenterOrCompany.SetAdmin(true);
            ucNewCenterOrCompany.ResetControls();
            //hdnIsCompanySaved.Value = "0";
            Company objCompany = new Company();
            if (Session["CompanyId"] != null)
            {

                //objCompany.CompanyID = Convert.ToInt32(hdnCompId.Value);
                objCompany.CompanyID = Convert.ToInt32(Session["CompanyId"].ToString());
                objCompany.Load();

                ucNewCenterOrCompany.SetControlPropertiesCompanyFromObject(objCompany);

            }
            ((UpdatePanel)ucNewCenterOrCompany.FindControl("upAddNewShoppingCenterOrCompany")).Update();
            //IsNextClicked = false; // ------ SK : 08/07

            mpeAddNewComapny.Show();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "imgEdit_Click", ex.Message);
        }
        //


        //UpdatePanel3.Update();

    }


    protected void imgVendorEdit_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            lblNotesModifiedby.Text = string.Empty;
            if (hdnUserId != null)
            {
                if (hdnUserId.Value != "")
                {
                    int UserID = 0;
                    UserID = Convert.ToInt32(hdnUserId.Value.ToString());
                    Users user = new Users();
                    user.UserID = UserID;
                    Boolean isloaded = user.Load();
                    if (isloaded)
                    {
                        txtCompany.Text = user.CompanyName;
                        txtAddress.Text = user.Address1;
                        txtCity.Text = user.City;
                        ddlState.SelectedValue = user.State;
                        txtZipCode.Text = user.Zip;
                        txtName.Text = user.FirstName + " " + user.LastName;
                        txtEmail.Text = user.Email;
                        txtEin.Text = user.EIN;
                        txtSetAmount.Text = String.Format("{0:n2}", user.SetAmount);
                        txtShare.Text = String.Format("{0:n2}", user.SharePercent);
                        txtUserNOTES.Text = user.UserNotes;
                        if (!string.IsNullOrEmpty(user.UserNotes))
                        {
                            if (user.LastModifiedNotesBy > 0)
                            {
                                lblNotesModifiedby.Text = "Last Modified by ";

                                Users userName = new Users();
                                userName.UserID = Convert.ToInt32(Session[GlobleData.User]);
                                Boolean isloadedUserName = userName.Load();
                                if (isloadedUserName)
                                {
                                    lblNotesModifiedby.Text = lblNotesModifiedby.Text + userName.FirstName + " " + userName.LastName + " on " + Convert.ToDateTime(user.LastModifiedNotesOn).ToShortDateString();
                                }
                            }

                        }
                    }
                    modalEdit.Show();
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "imgVendorEdit_Click", ex.Message);
        }
    }
    protected void btnAddBroker_Click(object sender, EventArgs e)
    {
        try
        {
            Users obj;
            if (hdnUserId != null)
            {
                if (hdnUserId.Value != "")
                {
                    obj = new Users(Convert.ToInt32(hdnUserId.Value.ToString()));
                    obj.Load();
                    obj.CompanyName = Convert.ToString(txtCompany.Text);
                    obj.Address1 = Convert.ToString(txtAddress.Text);
                    obj.City = Convert.ToString(txtCity.Text);
                    obj.State = ddlState.SelectedValue;
                    obj.Zip = Convert.ToString(txtZipCode.Text);
                    string[] strname = txtName.Text.Split(' ');
                    for (int i = 0; i < strname.Length; i++)
                    {
                        if (i == 0)
                        {
                            obj.FirstName = strname[0];
                        }
                        else if (i == 1)
                        {
                            obj.LastName = strname[1];
                        }
                        else if (i == 2)
                        {
                            obj.FirstName = strname[0];
                            obj.MiddleName = strname[1];
                            obj.LastName = strname[2];
                        }
                    }

                    //if (hdnCompanyId.Value != null && !(hdnCompanyId.Value == ""))
                    //{
                    //    obj.CompanyID = Convert.ToInt32(Convert.ToString(hdnCompanyId.Value));
                    //}

                    obj.Email = Convert.ToString(txtEmail.Text);
                    obj.EIN = Convert.ToString(txtEin.Text);
                    obj.SharePercent = Convert.ToDouble(txtShare.Text);
                    obj.SetAmount = Convert.ToDecimal(txtSetAmount.Text);
                    obj.LastModifiedOn = DateTime.Now;
                    obj.IsActive = true;
                    //obj.UserTypeID = 2;
                    //obj.BrokerTypeId = 4;
                    int compare = string.Compare(obj.UserNotes.Trim(), txtUserNOTES.Text.Trim());
                    if (compare != 0)
                    {
                        obj.LastModifiedNotesBy = Convert.ToInt64(Session[GlobleData.User]);
                        obj.LastModifiedNotesOn = DateTime.Now;

                    }
                    obj.UserNotes = Convert.ToString(txtUserNOTES.Text);
                    obj.Save();

                    //
                    if (Session["UserID"] != null)
                    {
                        divCust.Visible = false;
                        divDeal.Visible = false;
                        divVendor.Visible = true;
                        DataSet ds = obj.GetDealInfoListByUserId();
                        DataTable dt = ds.Tables[0];
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            //Session["dt"] = dt;
                            gvDealInfo.DataSource = dt;
                            gvDealInfo.DataBind();
                            gvDealInfo.SelectedIndex = -1;
                        }
                        DataSet dsVendor = obj.GetVendorInfoList();
                        DataTable dtVendor = dsVendor.Tables[0];
                        if (dtVendor != null && dtVendor.Rows.Count > 0)
                        {
                            lblCompany.Text = dtVendor.Rows[0]["CompanyName"].ToString();
                            lblName.Text = dtVendor.Rows[0]["Name"].ToString();
                            lblEmails.Text = dtVendor.Rows[0]["Email"].ToString();
                            lblAddress1.Text = dtVendor.Rows[0]["Address1"].ToString();
                            lblAddress2.Text = dtVendor.Rows[0]["Address2"].ToString();
                            if (!string.IsNullOrEmpty(dtVendor.Rows[0]["Address1"].ToString()) || !string.IsNullOrEmpty(dtVendor.Rows[0]["Address2"].ToString()))
                            {
                                lblAddress2.Text = dtVendor.Rows[0]["Address2"].ToString() + "<br />";
                            }
                            lblCity.Text = dtVendor.Rows[0]["City"].ToString();
                            if (!string.IsNullOrEmpty(dtVendor.Rows[0]["State"].ToString()))
                            {
                                lblState.Text = dtVendor.Rows[0]["State"].ToString() + "<br />";
                            }

                            lblZipCode.Text = dtVendor.Rows[0]["Zip"].ToString();
                            lblVendorNotes.Text = dtVendor.Rows[0]["UserNotes"].ToString();
                            lblYTDCollected.Text = "$" + String.Format("{0:n2}", Convert.ToDecimal(dtVendor.Rows[0]["YTDAmount"].ToString()));
                            lblCurrentSplit.Text = dtVendor.Rows[0]["CurrSplitLevel"].ToString();
                            lblRemainingNeeded.Text = "$" + String.Format("{0:n2}", Convert.ToDecimal(dtVendor.Rows[0]["RemainingNextSplit"].ToString()));
                        }
                        if (sender == null)
                            gvVendor.SelectedIndex = 0;
                        else
                            gvVendor.SelectedIndex = Convert.ToInt32(hdnVendorRowId.Value);

                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "btnAddBroker_Click", ex.Message);
        }


    }
    protected void imgCustSearch_Click(object sender, ImageClickEventArgs e)
    {

        CustSearch();

    }
    private void CustSearch()
    {
        try
        {
            DealTransactions objDealTransactions = new DealTransactions();
            int userid = Convert.ToInt32(Session["UserID"].ToString());
            if (ddlCustShow.SelectedIndex == -1)
            {
                ddlCustShow.SelectedValue = "0";
            }
            if (Session["IsVendor"] != null)
            {
                if (Session["IsVendor"].ToString() == "1")
                {
                    DataSet dsVendor = objDealTransactions.GetDealVendorTransactions(Convert.ToInt32(ddlCustShow.SelectedValue), txtCustFind.Text.Trim());
                    if (dsVendor.Tables[0].Rows.Count > 0)
                    {
                        gvVendor.DataSource = dsVendor;
                        gvVendor.DataBind();
                        hdnUserId.Value = dsVendor.Tables[0].Rows[0]["UserID"].ToString();
                        btnFillVendorForm_Click(null, null);
                    }
                    else
                    {
                        gvVendor.DataSource = null;
                        gvVendor.DataBind();
                    }
                }
            }
            if (Session["IsVendor"] != null)
            {
                if (Session["IsVendor"].ToString() == "0")
                {
                    DataSet ds = objDealTransactions.GetDealCustomerTransactionsListUsingConditions(Convert.ToInt32(ddlCustShow.SelectedValue), txtCustFind.Text.Trim(), userid);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvCustInfo.DataSource = ds;
                        gvCustInfo.DataBind();
                        hdnCompanyID.Value = ds.Tables[0].Rows[0]["CompanyID"].ToString();
                        btnFillCustForm_Click(null, null);
                    }
                    else
                    {
                        gvCustInfo.DataSource = null;
                        gvCustInfo.DataBind();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "CustSearch", ex.Message);
        }
    }
    protected void imgTransSearch_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            DealTransactions objDealTransactions = new DealTransactions();
            if (Session["UserID"] != null)
            {
                int userid = Convert.ToInt32(Session["UserID"].ToString());
                int ViewType = ddlTransaction.SelectedValue == "" ? 3 : Convert.ToInt16(ddlTransaction.SelectedValue);
                DateTime FromDate = new DateTime(); DateTime ToDate = new DateTime();
                if (txtTransForm.Text != "" && txtTransTo.Text != "")
                {
                    FromDate = Convert.ToDateTime(txtTransForm.Text);
                    ToDate = Convert.ToDateTime(txtTransTo.Text);
                }
                if (Session["IsVendor"] != null)
                {
                    if (Session["IsVendor"].ToString() == "1")
                    {
                        if (ddlTransShow.SelectedIndex == -1)
                        {
                            ddlTransShow.SelectedValue = "1";
                        }
                        DataSet ds = objDealTransactions.GetVendorTransInfoList(Convert.ToInt32(ddlTransShow.SelectedValue), txtTransFind.Text.Trim(), ViewType, FromDate, ToDate);

                        if (ds != null)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                DataTable dt = ds.Tables[0];
                                //Session["dtTransInfo"] = dt;
                                gvTransInfo.DataSource = dt;
                                gvTransInfo.DataBind();
                            }
                            else
                            {
                                //Session["dtTransInfo"] = null;
                                gvTransInfo.DataSource = null;
                                gvTransInfo.DataBind();
                            }
                        }
                    }
                }
                if (Session["IsVendor"] != null)
                {
                    if (Session["IsVendor"].ToString() == "0")
                    {
                        DataSet ds = objDealTransactions.GetTransInfoList(userid, Convert.ToInt32(ddlTransShow.SelectedValue), txtTransFind.Text, ViewType, FromDate, ToDate);
                        DataTable dt = ds.Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            // Session["dtTransInfo"] = dt;
                            gvTransInfo.DataSource = dt;
                            gvTransInfo.DataBind();
                        }
                        else
                        {
                            // Session["dtTransInfo"] = null;
                            gvTransInfo.DataSource = null;
                            gvTransInfo.DataBind();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "imgTransSearch_Click", ex.Message);
        }
    }
    protected void ddlTransShow_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DealTransactions objDealTransactions = new DealTransactions();
            if (Session["UserID"] != null)
            {
                int ViewType = ddlTransaction.SelectedValue == "" ? 0 : Convert.ToInt16(ddlTransaction.SelectedValue);
                DateTime FromDate = new DateTime(); DateTime ToDate = new DateTime();
                if (txtTransForm.Text != "" && txtTransTo.Text != "")
                {
                    FromDate = Convert.ToDateTime(txtTransForm.Text);
                    ToDate = Convert.ToDateTime(txtTransTo.Text);
                }
                int userid = Convert.ToInt32(Session["UserID"].ToString());
                if (Session["IsVendor"] != null)
                {
                    if (Session["IsVendor"].ToString() == "1")
                    {
                        if (ddlTransShow.SelectedIndex == -1)
                        {
                            ddlTransShow.SelectedValue = "1";
                        }
                        ddlTransaction.SelectedValue = "3";
                        ViewType = ddlTransaction.SelectedValue == "" ? 0 : Convert.ToInt16(ddlTransaction.SelectedValue);
                        DataSet ds = objDealTransactions.GetVendorTransInfoList(Convert.ToInt32(ddlTransShow.SelectedValue), txtTransFind.Text, ViewType, FromDate, ToDate);

                        if (ds != null)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                DataTable dt = ds.Tables[0];
                                //Session["dtTransInfo"] = dt;
                                gvTransInfo.DataSource = dt;
                                gvTransInfo.DataBind();
                            }
                            else
                            {
                                //Session["dtTransInfo"] = null;
                                gvTransInfo.DataSource = null;
                                gvTransInfo.DataBind();
                            }
                        }
                    }
                }
                if (Session["IsVendor"] != null)
                {
                    if (Session["IsVendor"].ToString() == "0")
                    {
                        DataSet ds = objDealTransactions.GetTransInfoList(userid, Convert.ToInt32(ddlTransShow.SelectedValue), txtTransFind.Text, ViewType, FromDate, ToDate);

                        if (ds != null)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                DataTable dt = ds.Tables[0];
                                //Session["dtTransInfo"] = dt;
                                gvTransInfo.DataSource = dt;
                                gvTransInfo.DataBind();
                            }
                            else
                            {
                                //Session["dtTransInfo"] = null;
                                gvTransInfo.DataSource = null;
                                gvTransInfo.DataBind();
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "ddlTransShow_SelectedIndexChanged", ex.Message);
        }
    }
    protected void ddlCustShow_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Session["IsVendor"] != null)
        {
            if (Session["IsVendor"].ToString() == "1")
            {
                ddlViewDealGV.SelectedValue = "3";
            }
        }
        CustSearch();
    }
    protected void btnTranClear_Click(object sender, EventArgs e)
    {
        try
        {
            // DataTable dataTable = (DataTable)Session["dtTransInfo"];
            DataTable dataTable = (DataTable)TransInfoList();
            if (dataTable != null)
            {
                if (dataTable.Rows.Count > 0)
                {
                    gvTransInfo.DataSource = dataTable;
                    gvTransInfo.DataBind();
                }
                else
                {
                    gvTransInfo.DataSource = null;
                    gvTransInfo.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "btnTranClear_Click", ex.Message);
        }
    }
    protected void gvCustInfo_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            DealTransactions objDealTransactions = new DealTransactions();
            int userid = Convert.ToInt32(Session["UserID"].ToString());
            if (ddlCustShow.SelectedIndex == -1)
            {
                ddlCustShow.SelectedValue = "0";
            }
            DataSet ds = objDealTransactions.GetDealCustomerTransactionsListUsingConditions(Convert.ToInt32(ddlCustShow.SelectedValue), txtCustFind.Text.Trim(), userid);

            if (ds != null)
            {

                DataTable dataTable = new DataTable();
                dataTable = ds.Tables[0];
                // DataTable dataTable = (DataTable)Session["dt"];
                if (dataTable != null)
                {
                    //Create new dataview instance and pass datatable 
                    DataView dataView = new DataView(dataTable);

                    //get sort direction from the get sort direction method
                    string sortDirection = GetSortDirection();

                    //Sort dataview data based on the sort directin value
                    dataView.Sort = e.SortExpression + " " + sortDirection;

                    //Assign datasource and bind data to grid view
                    gvCustInfo.DataSource = dataView;
                    gvCustInfo.DataBind();
                    gvCustInfo.SelectedIndex = -1;
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "gvCustInfo_Sorting", ex.Message);
        }
    }
    protected void gvVendor_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            DealTransactions objDealTransactions = new DealTransactions();
            int userid = Convert.ToInt32(Session["UserID"].ToString());
            if (ddlCustShow.SelectedIndex == -1)
            {
                ddlCustShow.SelectedValue = "0";
            }
            DataSet ds = objDealTransactions.GetDealVendorTransactions(Convert.ToInt32(ddlCustShow.SelectedValue), txtCustFind.Text.Trim());

            if (ds != null)
            {

                DataTable dataTable = new DataTable();
                dataTable = ds.Tables[0];
                // DataTable dataTable = (DataTable)Session["dt"];
                if (dataTable != null)
                {
                    //Create new dataview instance and pass datatable 
                    DataView dataView = new DataView(dataTable);

                    //get sort direction from the get sort direction method
                    string sortDirection = GetSortDirection();

                    //Sort dataview data based on the sort directin value
                    dataView.Sort = e.SortExpression + " " + sortDirection;

                    //Assign datasource and bind data to grid view
                    gvVendor.DataSource = dataView;
                    gvVendor.DataBind();
                    gvVendor.SelectedIndex = -1;
                }
            }
            ScriptManager.RegisterStartupScript(UpdatePanel1, Page.GetType(), "display", "FixedVendorGrid();", true);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "gvVendor_Sorting", ex.Message);
        }
    }
    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        //try
        //{
        //    if (!string.IsNullOrEmpty(txtDeleteCharslipReason.Text))
        //    {
        //        ImageButton img = (ImageButton)sender;
        //        int chargeslipid = Convert.ToInt32(img.CommandArgument);
        //        ChargeSlip objChargeSlip = new ChargeSlip();
        //        objChargeSlip.ChargeSlipID = chargeslipid;
        //        objChargeSlip.DeleteReason = txtDeleteCharslipReason.Text;
        //        objChargeSlip.SetDeleteChargeSlip();
        //    }
        //     }
        //catch (Exception ex)
        //{
        //    ErrorLog.WriteLog("ucBilledTab.aspx", "imgDelete_Click", ex.Message);
        //}
    }
    protected void btnDeleteOK_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtDeleteCharslipReason.Text != "")
            {
                int loggedin = 0;
                int chargeslipid = Convert.ToInt32(hdnChargeSlipidDel.Value);
                if (Session["UserID"] != null)
                {
                    loggedin = Convert.ToInt32(Session["UserID"]);
                }
                ChargeSlip objChargeSlip = new ChargeSlip();
                objChargeSlip.ChargeSlipID = chargeslipid;
                objChargeSlip.DeleteReason = txtDeleteCharslipReason.Text;
                objChargeSlip.SetDeleteChargeSlip(loggedin);
                txtDeleteCharslipReason.Text = "";

                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "display", "BindGridAfterDelete();", true);                
                Page.ClientScript.RegisterStartupScript(this.GetType(), "display", "BindGridAfterDelete();", true);
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.aspx", "btnDeleteOK_Click", ex.Message);
        }
    }
    protected void btnRefreshPayment_Click(object sender, EventArgs e)
    {
        if (this.Parent.FindControl("ucPaymentHistoryDetail1") != null)
        {

            ucPaymentHistoryDetail ucb = (ucPaymentHistoryDetail)this.Parent.FindControl("ucPaymentHistoryDetail1");
            ucb.ChargeSlipId = Convert.ToInt32("1528");
            ucb.Bindata();
            if (this.Parent.FindControl("hdnPopup") != null)
            {
                HiddenField hdn = (HiddenField)this.Parent.FindControl("hdnPopup");
                hdn.Value = "1";
            }
        }
    }

    //Added by shishir 13-06-2016
    protected void btnPostSplitPaid_Click(object sender, EventArgs e)
    {
        try
        {
            //SnapShot snapst = new SnapShot();
            //snapst.UserId = Convert.ToInt32(hdnUserId.Value);
            //DataSet dsSummary = snapst.GetPostSplitPaid();
            //if (dsSummary != null && dsSummary.Tables.Count > 0)
            //{
            //    grdPayment.DataSource = null;
            //    grdPayment.DataBind();
            //    grdPayment.DataSource = dsSummary.Tables[1];
            //    grdPayment.DataBind();
            //    grdPayment.Visible = true;
            //}
            ModalShowPostSplitPaid.Show();
        }
        catch (Exception ex)
        {

        }

    }

    //Added by shishir 13-06-2016
    decimal TotGrossPaid = 0, TotalNetPaid = 0;
    protected void grdPayment_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {

            int index = Convert.ToInt32(hdnUserId.Value);
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if ((index == 5) || (index == 23))
                {
                    grdPayment.Columns[7].Visible = true;
                    grdPayment.Columns[8].Visible = true;
                }
                else
                {
                    grdPayment.Columns[7].Visible = false;
                    grdPayment.Columns[8].Visible = false;
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                if (DataBinder.Eval(e.Row.DataItem, "Grosspaid") != DBNull.Value)
                    TotGrossPaid += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Grosspaid"));
                if (DataBinder.Eval(e.Row.DataItem, "NetPaid") != DBNull.Value)
                    TotalNetPaid += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "NetPaid"));

            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblFooPaidAmount = (Label)e.Row.FindControl("lblFooPaidAmount");
                lblFooPaidAmount.Text = "$" + String.Format("{0:n2}", TotGrossPaid);
                Label lblFooNetPaid = (Label)e.Row.FindControl("lblFooNetPaid");
                lblFooNetPaid.Text = "$" + String.Format("{0:n2}", TotalNetPaid);
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnVoidPayment_Click(object sender, EventArgs e)
    {
        try
        {
            Users obj = new Users();
            if (hdnUserId != null)
            {
                if (hdnUserId.Value != "")
                {
                    obj.UserID = Convert.ToInt32(hdnUserId.Value);
                    if (Session["UserID"] != null)
                    {
                        gvDealInfo.DataSource = null;
                        gvDealInfo.DataBind();
                        //DataSet ds = obj.GetDealInfoListByUserId();
                        //DataTable dt = ds.Tables[0];
                        DataTable dt = (DataTable)DealInfoList();
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            // Session["dt"] = dt;
                            gvDealInfo.DataSource = dt;
                            gvDealInfo.DataBind();
                            gvDealInfo.SelectedIndex = -1;
                        }
                        else
                        {
                            //Session["dt"] = null;
                            gvDealInfo.DataSource = null;
                            gvDealInfo.DataBind();
                            gvDealInfo.SelectedIndex = -1;
                        }
                    }
                    //gvDealInfo.SelectedIndex = Convert.ToInt32(hdnDealRowID.Value);
                 
                }

            }
            if (hdnCompanyID != null)
            {
                if (hdnCompanyID.Value != "")
                {
                    gvDealInfo.DataSource = null;
                    gvDealInfo.DataBind();
                    DataTable dt = (DataTable)DealInfoList();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        gvDealInfo.DataSource = dt;
                        gvDealInfo.DataBind();
                        gvDealInfo.SelectedIndex = -1;
                    }
                    else
                    {
                        gvDealInfo.DataSource = null;
                        gvDealInfo.DataBind();
                        gvDealInfo.SelectedIndex = -1;
                    }
                }
               // gvDealInfo.SelectedIndex = Convert.ToInt32(hdnDealRowID.Value);
            }
           // lblAmountDue.Text = gvDealInfo.Rows[Convert.ToInt16(hdnDealRowID.Value)].Cells[2].Text;
            lblOutstanding.Text = lblAmountDue.Text;
           // hdnOutstanding.Value = "1";
            lblCreditAmount.Text = "$0.00";
            lblOverPaymentAmount.Text = "$0.00"; 

            //  document.getElementById('<%=lblAmountDue.ClientID%>').innerText="$"+GrossAmountDue;
            //                document.getElementById('<%=lblOutstanding.ClientID%>').innerText="$"+GrossAmountDue;
            //                document.getElementById('<%=hdnOutstanding.ClientID%>').value=GrossAmountDue;
            //                document.getElementById('<%=lblCreditAmount.ClientID%>').innerText="$0.00";
            //                document.getElementById('<%=lblOverPaymentAmount.ClientID%>').innerText="$0.00";
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBilledTab.ascx", "btnFillGVDealInfo_Click", ex.Message);
        }
    }
    protected void btnExporttoPDF_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            foreach (GridViewRow row in grdPayment.Rows)
            {
                row.Style.Add("font-size", "8px");
            }
            GridViewRow HeaderRow = grdPayment.HeaderRow;
            HeaderRow.Height = 20;
            HeaderRow.Style.Add("font-size", "9px");
            GridViewRow FooterRow = grdPayment.FooterRow;
            FooterRow.Height = 20;
            FooterRow.Style.Add("font-size", "9px");
            FooterRow.Cells[4].Style.Add("text-align", "right");
            FooterRow.Cells[6].Style.Add("text-align", "right");         
            Response.AddHeader("content-disposition", "attachment;filename=VendorPostSplitPaid.pdf");
            grdPayment.RenderControl(hw);
            Document pdfDoc = new Document(new Rectangle(612f, 792f), 36f, 36f, 36f, 36f);
            pdfDoc.HtmlStyleClass = "SetWidth";         
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            StringReader sr = new StringReader(sw.ToString());
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);       
            Response.End();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucSnapshot.ascx", "btnExporttoPDF_Click", ex.Message);
        }
    }
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            foreach (GridViewRow row in grdPayment.Rows)
            {
                row.Style.Add("font-size", "10px");
            }
            GridViewRow HeaderRow = grdPayment.HeaderRow;
            HeaderRow.Height = 20;
            HeaderRow.Style.Add("font-size", "11px");
            GridViewRow FooterRow = grdPayment.FooterRow;
            FooterRow.Height = 20;
            FooterRow.Style.Add("font-size", "11px");
            FooterRow.Cells[4].Style.Add("text-align", "right");
            FooterRow.Cells[6].Style.Add("text-align", "right");            
            Response.AddHeader("content-disposition", "attachment;filename=VendorPostSplitPaid.xls");
            grdPayment.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
        catch (Exception ex)
        {

            ErrorLog.WriteLog("ucSnapshot.ascx", "btnExporttoPDF_Click", ex.Message);
        }
    }
    protected void btnViewBrokerSnapshotDetails_Click(object sender, EventArgs e)
    {
        SnapShot snapst = new SnapShot();
        snapst.UserId = Convert.ToInt32(hdnUserId.Value);
        snapst.Begindate = Convert.ToDateTime("01/01/"+DateTime.Now.Year);
        snapst.EndDate = Convert.ToDateTime("12/31/" + DateTime.Now.Year);
        DataSet dsSumarry = snapst.GetRecevableSummary();
        if (dsSumarry != null && dsSumarry.Tables.Count > 0)
        {
            lblPreSplitRecCur.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["Recevables"]);
            decimal TotalEarning = 0;
            if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Estimated_Post_Split_Commission_Due].Rows.Count > 0)
            {
                for (int i = 0; i < dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Estimated_Post_Split_Commission_Due].Rows.Count; i++)
                {
                    TotalEarning += Convert.ToDecimal(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Estimated_Post_Split_Commission_Due].Rows[i]["EstimatedPostSplitDue"]);
                }
            }
            labelPreSplitRecCur.Text = "Pre Split Receivables" + " (" + DateTime.Now.Year + "):";
            labelPostSplitRecCur.Text = "Post Split Receivables" + " (" + DateTime.Now.Year + "):";
            labelPreSplitRecNex.Text = "Pre Split Receivables" + " (" + (DateTime.Now.Year + 1) + "):";
            labelPreSplitPaid.Text = "Pre Split Paid" + " (" + DateTime.Now.Year + "):";
            labelPostSplitPaid.Text = "Post Split Paid" + " (" + DateTime.Now.Year + "):";
            labelTotalPreSplitOptConCur.Text = "Total Pre Split Option & Contingent Receivables" + " (" + DateTime.Now.Year + "):";
            labelTotalPreSplitOptConNext.Text = "Total Pre Split Option & Contingent Receivables" + " (" + (DateTime.Now.Year + 1) + "):";
            lblPostSplitRecCur.Text = String.Format("{0:c}", TotalEarning);
            lblPreSplitRecNex.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["FutureRecevables"]);
            lblPreSplitPaid.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["Gross"]);
            lblPostSplitPaid.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["Paid"]);
            lblPreSplitTotalRec.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["TOTATLRECEVABLES"]);
            lblTotalPreSplitOptConCur.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["OPTNCONT"]);
            lblTotalPreSplitOptConNext.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["FutureOPTNCONT"]);          
          
        }
        ModalShowBrokerSnapshotDetail.Show();
    }

}