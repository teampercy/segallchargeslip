﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucDueDtWiseCommission.ascx.cs" Inherits="UserControl_ucDueDtWiseCommission" %>

<%--<link href="../Js/jquery-ui.css" rel="stylesheet" />
<script src="../Js/jquery-ui.js"></script>--%>
<script src="../Js/calendar.js"></script>
<%--<link href="../Styles/SiteTEST.css" rel="stylesheet" />
    <script src="../Js/jquery-1.9.1.js"></script>--%>
<link href="../Styles/calendar.css" rel="stylesheet" />

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%--
<link href="../Styles/Hidedate.css" rel="stylesheet" />
<script src="../Js/Extension.min.js"></script>
<style type="text/css">
    .ajax__calendar_day_disabled {
        background-color: #ccc !important;
        color: #eee !important;
    }
</style>


<script type="text/javascript">
    function getElementsByClassName(a, b) { var c = []; var d = a.getElementsByTagName("*"); for (var e = 0; e < d.length; e++) { if (d[e].className.indexOf(b) != -1) { c.push(d[e]) } } return c } function DisableDates(a) { if (a._element.className.indexOf("disable_past_dates") != -1) { var b = getElementsByClassName(a._days, "ajax__calendar_day"); var c = (new Date).setHours(0, 0, 0, 0); for (var d = 0; d < b.length; d++) { try { if (b[d].date.setHours(0, 0, 0, 0) >= c) { b[d].className = "ajax__calendar_day" } else { b[d].className = "ajax__calendar_day ajax__calendar_day_disabled" } } catch (e) { } } } if (a._element.className.indexOf("disable_future_dates") != -1) { var b = getElementsByClassName(a._days, "ajax__calendar_day"); var c = (new Date).setHours(0, 0, 0, 0); for (var d = 0; d < b.length; d++) { try { if (b[d].date.setHours(0, 0, 0, 0) <= c) { b[d].className = "ajax__calendar_day" } else { b[d].className = "ajax__calendar_day ajax__calendar_day_disabled" } } catch (e) { } } } } AjaxControlToolkit.CalendarBehavior.prototype.show = function (a) { this._ensureCalendar(); if (!this._isOpen) { var b = new Sys.CancelEventArgs; this.raiseShowing(b); if (b.get_cancel()) { return } this._isOpen = true; this._popupBehavior.show(); if (this._firstPopUp) { this._switchMonth(null, true); switch (this._defaultView) { case AjaxControlToolkit.CalendarDefaultView.Months: this._switchMode("months", true); break; case AjaxControlToolkit.CalendarDefaultView.Years: this._switchMode("years", true); break } this._firstPopUp = false } this.raiseShown(); DisableDates(this) } }; AjaxControlToolkit.CalendarBehavior.prototype._cell_onclick = function (a) { a.stopPropagation(); a.preventDefault(); if (!this._enabled) return; var b = a.target; var c = this._getEffectiveVisibleDate(); Sys.UI.DomElement.removeCssClass(b.parentNode, "ajax__calendar_hover"); switch (b.mode) { case "prev": case "next": this._switchMonth(b.date); break; case "title": switch (this._mode) { case "days": this._switchMode("months"); break; case "months": this._switchMode("years"); break } break; case "month": if (b.month == c.getMonth()) { this._switchMode("days") } else { this._visibleDate = b.date; this._switchMode("days") } break; case "year": if (b.date.getFullYear() == c.getFullYear()) { this._switchMode("months") } else { this._visibleDate = b.date; this._switchMode("months") } break; case "day": if (this._element.className.indexOf("disable_past_dates") != -1) { if (b.date.setHours(0, 0, 0, 0) >= (new Date).setHours(0, 0, 0, 0)) { this.set_selectedDate(b.date); this._switchMonth(b.date); this._blur.post(true); this.raiseDateSelectionChanged() } } else if (this._element.className.indexOf("disable_future_dates") != -1) { if (b.date.setHours(0, 0, 0, 0) <= (new Date).setHours(0, 0, 0, 0)) { this.set_selectedDate(b.date); this._switchMonth(b.date); this._blur.post(true); this.raiseDateSelectionChanged() } } else { this.set_selectedDate(b.date); this._switchMonth(b.date); this._blur.post(true); this.raiseDateSelectionChanged() } break; case "today": this.set_selectedDate(b.date); this._switchMonth(b.date); this._blur.post(true); this.raiseDateSelectionChanged(); break } DisableDates(this) };
</script>--%>

<script type="text/javascript">
    //Trim string JAY
    function trim(str) {
        return str.replace(/^\s+|\s+$/g, '');
    }
</script>

<script type="text/javascript">
    //function ShowPopUp() {
    //    debugger;
    //    //$('#' + Parent + '_dvNotifyDetails').dialog('option', 'title', title);
    //    $('#ContentMain_ucConsulting_DueDateCommission_dvNotifyDetails').dialog('open');
    //}

    //$(function () {
    //    $("#ContentMain_ucConsulting_DueDateCommission_dvNotifyDetails").dialog({
    //        autoOpen: false,
    //        width: 250,
    //        modal: true,
    //        show: {
    //            duration: 300
    //        },
    //        hide: {
    //            duration: 300
    //        }
    //    });
    //});

    function DueDateRangeUpdate(Parent, Id, Index) {
        DateRangeValidation(Parent, Id)
        var id = Parent + "_" + Id;
        var Date = document.getElementById(id);
        UpdateDueDate(Index, Date.value);
    }

    function DeleteDueDate(Row, dtIndx, Parent) {

        var DetTable = document.getElementById(Parent + "_tbDueWiseCommission");
        if (DetTable.rows.length == 2) {
            DetTable.deleteRow(0);
            DetTable.deleteRow(0);
        }
        else {
            DetTable.deleteRow(Row);
            DetTable.deleteRow(Row);
        }
        DeleteDatatbRecord(dtIndx);
        return true;
    }

    function ScrollDown(parent) {
        $("#" + parent).slideDown('fast');
        return false;
    }

    function ScrollUp(parent) {
        $("#" + parent).slideUp('fast');
        return true;
    }

    function ScrollToggle(parent) {
        //,txtparent
        //document.getElementById(txtparent + "_txtAddPer").value = "";
        //document.getElementById(txtparent + "_txtAddAmount").value = "";

        $("#" + parent).slideToggle('fast');
        return false;
    }

    //Called when new due date comm is added
    //removed lbl err and set alert  JAY
    function CalCommissionAmountOrPer(SalePrice, Parent) {
        debugger;
        var CommPer = document.getElementById(Parent + "_txtAddPer").value;
        var CommAmount = document.getElementById(Parent + "_txtAddAmount").value;
        var SalePiceVal = document.getElementById(SalePrice).value;
        //var lbErr = $("#head_lbErrorMsg");

        if (trim(CommPer) != '' && parseInt(CommPerOnDueDt) > 100) {
            alert("Commision percentage can not be greater then 100");
            return false;
        }

        if (trim(CommPer) != "") {
            document.getElementById(Parent + "_txtAddAmount").value = ((parseFloat(CommPer) / 100) * parseFloat(SalePiceVal)).toFixed(2);
            //lbErr.text("");
        }
        else if (trim(CommAmount) != "") {
            document.getElementById(Parent + "_txtAddPer").value = ((parseFloat(CommAmount) * 100) / parseFloat(SalePiceVal)).toFixed(2);
            //lbErr.text("");
        }

        //Set Comm and Amount
        if (SalePiceVal != "") {
            Parent = Parent.replace("_DueDateCommission", "");
            var CommPer = document.getElementById(Parent + "_txtCommissionPer").value;
            var SetFee = document.getElementById(Parent + "_txtSetFee").value;
            // var callFrom = document.getElementById(Parent + "_hdnis1st").value;

            //if (callFrom != "1") {
            //    CalCommissionAmountFEE(SalePrice, Parent);
            //}


            //if (CommPer != "") {
            //    var CalSetFee = (parseFloat(SalePiceVal) * parseFloat(CommPer)) / (100);
            //    document.getElementById(Parent + "_txtSetFee").value = SetFee = CalSetFee;

            //}
            //else
            if (SetFee != "") {
                var CalCommPer = (parseFloat(SetFee) * 100) / (parseFloat(SalePiceVal));
                document.getElementById(Parent + "_txtCommissionPer").value = CommPer = CalCommPer;
            }
            else {
                document.getElementById(Parent + "_txtSetFee").value = SetFee = "";
                document.getElementById(Parent + "_txtCommissionPer").value = CommPer = "";
            }
            // }
            //document.getElementById(Parent + "_hdnis1st").value = "0";
        }

        //update values in the session since this is client side data will be lost in postback JAY
        //UpdateDatatbRecord(SetFee);


        //OLD LOGIC
        //if (SalePiceVal != "") {
        //    Parent = Parent.replace("_DueDateCommission", "");
        //    var CommPer = document.getElementById(Parent + "_txtCommissionPer").value;
        //    var SetFee = document.getElementById(Parent + "_txtSetFee").value;
        //    var ischnged = document.getElementById(Parent + "_hdnis1st").value;

        //    if (ischnged != "1") { }
        //    if (CommPer != "") {

        //        var CalSetFee = (parseFloat(SalePiceVal) * parseFloat(CommPer)) / (100);
        //        document.getElementById(Parent + "_txtSetFee").value = SetFee = CalSetFee;

        //    }
        //    else if (SetFee != "") {
        //        var CalCommPer = (parseFloat(SetFee) * 100) / (parseFloat(SalePiceVal));
        //        document.getElementById(Parent + "_txtCommissionPer").value = CommPer = CalCommPer;
        //    }
        //    else {
        //        document.getElementById(Parent + "_txtSetFee").value = SetFee = "";
        //        document.getElementById(Parent + "_txtCommissionPer").value = CommPer = "";
        //    }

        //}


    }

    //Called when fee is changed //updated percentage and due date comm values as well as add new comm values
    //removed lbl err and set alert  JAY
    function CalCommissionAmountFEE(SalePrice, Parent) {
        debugger;
        var SalePiceVal = document.getElementById(SalePrice).value;
        //var lbErr = $("#head_lbErrorMsg");

        //Set Comm and Amount
        if (SalePiceVal != "") {
            Parent1 = Parent.replace("_DueDateCommission", "");
            var CommPer = document.getElementById(Parent1 + "_txtCommissionPer").value;
            var SetFee = document.getElementById(Parent1 + "_txtSetFee").value;
            //  var callFrom = document.getElementById(Parent1 + "_hdnis1st").value;
            if (SetFee != "") {

                var CalCommPer = (parseFloat(SetFee) * 100) / (parseFloat(SalePiceVal));

                if (parseInt(CalCommPer) > 100) {
                    alert("Commision percentage can not be greater then 100");
                    return false;
                }
                else {
                    document.getElementById(Parent1 + "_txtCommissionPer").value = CommPer = CalCommPer.toFixed(2);
                }
            }
            else {
                document.getElementById(Parent1 + "_txtSetFee").value = SetFee = "";
                document.getElementById(Parent1 + "_txtCommissionPer").value = CommPer = "";
            }
            //Set amount in add textbox according to the percentage
            var CommPer1 = document.getElementById(Parent + "_txtAddPer").value;
            if (CommPer1 != "") {
                document.getElementById(Parent + "_txtAddAmount").value = ((parseFloat(CommPer1) / 100) * parseFloat(SetFee)).toFixed(2);
                //lbErr.text("");
            }

            //update values in the client side and session since a this is client side data will be lost in postback
            updatefeesvalues(Parent, SetFee);
            UpdateDatatbRecord(SetFee);
        }
        return false;
    }

    function CalCommissionAmountFEEForCon(Parent) {
        debugger;
        Parent1 = Parent.replace("_DueDateCommission", "");
        var SetFee = document.getElementById(Parent1 + "_txtSetFee").value;
        if (SetFee != "") {
            //Set amount in add textbox according to the percentage
            var CommPer1 = document.getElementById(Parent + "_txtAddPer").value;
            if (CommPer1 != "") {
                document.getElementById(Parent + "_txtAddAmount").value = ((parseFloat(CommPer1) / 100) * parseFloat(SetFee)).toFixed(2);
                //lbErr.text("");
            }
            //update values in the client side and session since a this is client side data will be lost in postback
            updatefeesvalues(Parent, SetFee);
            UpdateDatatbRecord(SetFee);
        }
        return false;
    }

    function updatefeesvalues(Parent, setfee) { //JAY
        debugger;
        var PerCommissionTot = 0;
        var opTable = document.getElementById(Parent + "_tbDueWiseCommission");
        for (var r = 1, n = opTable.rows.length; r < n; r = r + 2) { //changed innerText to innerHTML
            PrevDate = opTable.rows[r - 1].cells[0].children[0].innerHTML;
            var duepercentage = opTable.rows[r].cells[0].children[0].innerHTML;
            opTable.rows[r].cells[1].children[0].innerHTML = (duepercentage * setfee / 100);
        }
    }


    //removed lbl err and set alert  JAY
    function CalCommissionAmount(SalePrice, Parent) {
        debugger;
        var CommPer = document.getElementById(Parent + "_txtAddPer").value;
        var SalePiceVal = document.getElementById(SalePrice).value;
        var lbErr = $("#head_lbErrorMsg");
        if (CommPer != "") {
            document.getElementById(Parent + "_txtAddAmount").value = ((parseFloat(CommPer) / 100) * parseFloat(SalePiceVal)).toFixed(2);
            lbErr.text("");
        }
    }

    function CalCommissionPercentage(SalePrice, Parent) {
        debugger;
        var CommAmount = document.getElementById(Parent + "_txtAddAmount").value;
        var SalePiceVal = document.getElementById(SalePrice).value;
        var lbErr = $("#head_lbErrorMsg");
        if (CommAmount != "") {
            document.getElementById(Parent + "_txtAddPer").value = ((parseFloat(CommAmount) * 100) / parseFloat(SalePiceVal)).toFixed(2);
            lbErr.text("");
        }
    }


    //removed lbl err and set alert JAY
    function ChkEntry(SalePrice, Parent) {
        debugger;
        var CommPer = document.getElementById(Parent + "_txtAddPer").value;
        var CommAmount = document.getElementById(Parent + "_txtAddAmount").value;
        var SalePiceVal = document.getElementById(SalePrice).value;
        var DueDate = document.getElementById(Parent + "_txtAddDuedt").value;
        //var lbErr = $("#head_lbErrorMsg");
        if (SalePiceVal == "") {
            alert("Please Enter Sale Price");
            //lbErr.text("Please Enter Sale Price");
            document.getElementById(SalePrice).focus();
            return false;
        }
        else if (DueDate == "") {
            alert("Please EnterCommission Date");
            //lbErr.text("Please EnterCommission Date");
            document.getElementById(Parent + "_txtAddDuedt").focus();
            return false;
        }
        else if (CommPer == "") {
            alert("Please Enter Sale Commission Percentage");
            //lbErr.text("Please Enter Sale Commission Percentage");
            document.getElementById(Parent + "_txtAddPer").focus();
            return false;
        }
        else if (CommAmount == "") {
            alert("Please Enter Sale Commission Amount");
            //lbErr.text("Please Enter Sale Commission Amount");
            document.getElementById(Parent + "_txtAddAmount").focus();
            return false;
        }
        else {
            //lbErr.text("");
            var rslt = ValidatePerCentage(Parent);
            if (rslt == true) {
                controlpostback();
                return true;
            }
            else {
                return false;
            }
        }
    }

    //removed lbl err and set alert JAY
    function ValidatePerCentage(Parent) {
        debugger;
        var DueDate = document.getElementById(Parent + "_txtAddDuedt").value;
        var CommPerOnDueDt = document.getElementById(Parent + "_txtAddPer").value;
        var PrevDate;
        //var lbErr = $("#head_lbErrorMsg");
        if (parseInt(CommPerOnDueDt) > 100) {
            alert("Commission Percentage Cannot Be Greater Than 100 %");
            //lbErr.text("Commission Percentage Cannot Be Greater Than 100 %");
            document.getElementById(Parent + "_txtAddPer").focus;
            return false;
        }
        else if (DueDate != "" && CommPerOnDueDt != "") {
            var PerCommissionTot = 0;
            var opTable = document.getElementById(Parent + "_tbDueWiseCommission");

            var ToDay = new Date();
            var Month = ToDay.getMonth() + 1;
            var Day = ToDay.getDate();
            var Year = ToDay.getFullYear();
            var CurrDate = new Date(Month + "/" + Day + "/" + Year);
            var SelDate = new Date(DueDate);

            //if (Date.parse(SelDate) < Date.parse(CurrDate)) {
            //    alert("You Cannot Select A Day Before Today!");
            //    document.getElementById(Parent + "_txtAddDuedt").focus();
            //    return false;
            //}

            for (var r = 1, n = opTable.rows.length; r < n; r = r + 2) {
                // PrevDate = opTable.rows[r - 1].cells[0].children[0].innerHTML;
                PrevDate = opTable.rows[r - 1].cells[0].children[0].value;

                var dtPrev = new Date(PrevDate);

                if (Date.parse(dtPrev) == Date.parse(SelDate)) {
                    alert("Selected Date Entry Exist Already !");
                    //lbErr.text("Enter Commission Date Properly");
                    document.getElementById(Parent + "_txtAddDuedt").focus();
                    return false;
                }
                //PerCommissionTot += parseFloat(opTable.rows[r].cells[0].children[0].innerHTML);
                PerCommissionTot += parseFloat(opTable.rows[r].cells[0].children[0].innerHTML);
            }

            var CommTot = PerCommissionTot + parseFloat(CommPerOnDueDt);
            if (ValidateForPercentage(CommTot, Parent, PerCommissionTot) == true) {
                return true;
            }
            else {
                return false;
            }
            //call ValidateForPercentage here
        }
        return true;
    }

    function ValidateForPercentage(CommTot, Parent, PerCommissionTot) {
        if (CommTot > 100) {
            alert("Total Commission Percentage is: " + PerCommissionTot + "  Commission Percentage Cannot Be Greater Than 100 %");
            document.getElementById(Parent + "_txtAddPer").focus();
            return false;
        }
        return true;
    }
    function UpdateDueDate(Index, DueDate) {
        $.ajax({
            type: "POST",
            url: "NewCharge.aspx/SetCommissionDate",
            data: '{Index: "' + Index + '" ,Date:"' + DueDate + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    function DeleteDatatbRecord(Row) {

        var Index = parseInt(Row) - 1;
        $.ajax({
            type: "POST",
            url: "NewCharge.aspx/SetCommissionDatatb",
            data: '{RowIndx: "' + Index + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            failure: function (response) {
                alert(response.d);
            }
        });
    }

    function UpdateDatatbRecord(CommFee) {
        $.ajax({
            type: "POST",
            url: "NewCharge.aspx/updateCommisiondata",
            data: '{CommFee: "' + CommFee + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //success: function (result) { alert(result); }, Enable to check the resonce
            failure: function (response) {
                alert(response.d);
            }
        });
    }

    //removed lbl err and set alert JAY
    function DateRangeValidation(Parent, Id) {

        debugger;
        var DueDate = document.getElementById(Parent + "_" + Id);
        if (isDate(DueDate) == true) {
            var arrDate = DueDate.value.split("/");
            var DueDate = new Date(arrDate[2], arrDate[0] - 1, arrDate[1]); //
            var ToDayNow = new Date();
            var ToDayNow = ToDayNow.getDateOnly();
            //ar Today = new date(ToDayNow.
            //var lbErr = $("#head_lbErrorMsg");

            //if (Date.parse(DueDate) < Date.parse(ToDayNow)) {
            //    alert("Due Date Cannot Be Less Than Current Date");
            //    //lbErr.text("Due Date Cannot Be Less Than Current Date");
            //    document.getElementById(Parent + "_" + Id).value = "";
            //    document.getElementById(Parent + "_" + Id).focus();
            //    return false;
            //}
            //else {
            //    //lbErr.text("");
            //    return true;
            //}
            return true;
        }
        else {
            return false;
        }
    }

    function controlpostback() {
        debugger;
        document.getElementById('ContentMain_ucSaleImmproved_DueDateCommission_hdnupdate').value = 'true';
        // alert(document.getElementById(' hdnupdate.ClientID %>').value);
    }

    function isInteger(evt, id) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        var NotifiDays = $("#" + id).val()
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        if (NotifiDays.length > 2)
            return false;

        return true;
    }

    function FillNotifyDaysPopup(id) {
        debugger;
        var Parent = id.replace("_btnNotifyMe", "")
        $find(Parent + '_mpNotifyExtendr').show();        
        var txtNotifyDaysVal = $('#' + Parent + '_txtNotify').val()        
        var NotifyDaysArray = txtNotifyDaysVal.split(',').map(function (item) {
            return parseInt(item, 10)
        });
        var DefaultNotifyDays = [15, 30, 60, 90]
   
        for (var i = 0; i < DefaultNotifyDays.length; i++) {
            if (NotifyDaysArray.indexOf(DefaultNotifyDays[i]) > -1)
                //$('#' + Parent + '_chkNotifyDays' + DefaultNotifyDays[i]).attr("checked", true);
                document.getElementById(Parent + "_chkNotifyDays" + DefaultNotifyDays[i]).checked = true;
            else
                //$('#' + Parent + '_chkNotifyDays' + DefaultNotifyDays[i]).attr("checked", false);
                document.getElementById(Parent + "_chkNotifyDays" + DefaultNotifyDays[i]).checked = false;
        }

        NotifyDaysArray = NotifyDaysArray.filter(function (item) {
            return DefaultNotifyDays.indexOf(item) === -1;
            //return !DefaultNotifyDays.includes(item);
        });

        //to check this condition
        //if (isNaN(NotifyDaysArray)===true) {            
        //    NotifyDaysArray = []
        //}

        $('#' + Parent + '_tbCustomNotificatonDays tr').remove();

        BindNotificationTable(NotifyDaysArray, Parent);
    }

    function BindNotificationTable(NotifyDaysArray, Parent) {
        for (var i = 0; i < NotifyDaysArray.length; i++) {
            var randomnumber = parseInt($('#' + Parent + '_hdnCount').val());
            var output = document.getElementById(Parent + '_tbCustomNotificatonDays');
            var rowCount = output.rows.length;
            var cellCount = 0;
            var row = output.insertRow(rowCount);
            var cell1 = row.insertCell(cellCount);

            var element1 = document.createElement("input");
            element1.type = "hidden";
            rowCount = rowCount + 1;
            element1.name = Parent + "hdn1_" + rowCount;
            element1.id = Parent + "hdn1_" + rowCount;
            element1.value = 0;
            cell1.appendChild(element1);

            var element2 = document.createElement("input");
            element2.type = "hidden";
            element2.name = Parent + "hdnCheck_" + rowCount;
            element2.id = Parent + "hdnCheck_" + rowCount;
            element2.value = rowCount;
            cell1.appendChild(element2);
            cellCount++;
            cell1 = row.insertCell(cellCount);

            element1 = document.createElement("Label");
            element1.name = Parent + '_lbl1_' + rowCount;
            element1.id = Parent + '_lbl1_' + rowCount;
            element1.innerText = NotifyDaysArray[i]
            element1.width = "100%";
            cell1.appendChild(element1);
            cellCount++;
            cell1 = row.insertCell(cellCount);

            var cell1 = row.insertCell(cellCount);
            var element1 = document.createElement("input");
            $(cell1).addClass("tdgray");
            $(cell1).addClass("td-DeleteRecord");
            element1.type = "image";
            element1.src = "../Images/delete.gif";
            element1.setAttribute('onclick', 'javascript:DeleteRecord("' + rowCount + '","' + Parent + '");return false;');
            cell1.appendChild(element1);

            randomnumber = randomnumber + 1;
            $('#' + Parent + '_hdnCount').val();
        }
    }

    function addCustomNotificationDayRows(id) {
        debugger;        
        var Parent = id.replace("_btnAddDays", "");        
        var randomnumber = parseInt($('#' + Parent + '_hdnCount').val());        
        var output = document.getElementById(Parent + '_tbCustomNotificatonDays');
        var NotifyDaysval = $('#' + Parent + '_txtAddDays').val()
        var DefaultNotifyDays = [15, 30, 60, 90]

        for (var i = 0; i < output.rows.length; i++) {
            DefaultNotifyDays.push(parseInt(output.rows[i].cells[1].innerText))
        }

        if (NotifyDaysval == "" || NotifyDaysval == 0) {
            alert("Blank Notification Days value not allowed")
            return false;
        }                
        
        if (DefaultNotifyDays.indexOf(parseInt(NotifyDaysval)) != -1) {
            alert('Entered Notification Days already Present')
            return false
        }        

        var rowCount = output.rows.length;
        var cellCount = 0;
        var row = output.insertRow(rowCount);
        var cell1 = row.insertCell(cellCount);

        var element1 = document.createElement("input");
        element1.type = "hidden";
        rowCount = rowCount + 1;
        element1.name = Parent + "hdn1_" + rowCount;
        element1.id = Parent + "hdn1_" + rowCount;
        element1.value = 0;
        cell1.appendChild(element1);

        var element2 = document.createElement("input");
        element2.type = "hidden";
        element2.name = Parent + "hdnCheck_" + rowCount;
        element2.id = Parent + "hdnCheck_" + rowCount;
        element2.value = rowCount;
        cell1.appendChild(element2);
        cellCount++;
        cell1 = row.insertCell(cellCount);

        element1 = document.createElement("Label");
        element1.name = Parent + '_lbl1_' + rowCount;
        element1.id = Parent + '_lbl1_' + rowCount;        
        element1.innerText = NotifyDaysval
        element1.width = "100%";
        element1.style.paddingTop="5%";
        cell1.appendChild(element1);
        cellCount++;
        cell1 = row.insertCell(cellCount);

        var cell1 = row.insertCell(cellCount);
        var element1 = document.createElement("input");
        $(cell1).addClass("tdgray");
        $(cell1).addClass("td-DeleteRecord");
        element1.style.paddingTop="5%";
        element1.type = "image";
        element1.src = "../Images/delete.gif";
        element1.setAttribute('onclick', 'javascript:DeleteRecord("' + rowCount + '","' + Parent + '");return false;');
        cell1.appendChild(element1);

        $('#' + Parent + '_txtAddDays').val("");
        randomnumber = randomnumber + 1;        
        $('#' + Parent + '_hdnCount').val(randomnumber);

        return false;
    }

    function DeleteRecord(RowIndex, Parent) {
        debugger;
        var Row;        
        var table = document.getElementById(Parent + '_tbCustomNotificatonDays');
        var NotifyDaysArray = [];
        for (var i = 0; i < table.rows.length; i++) {
            if (typeof table.rows[i].cells[0].children[1] != 'undefined') {
                if (parseInt(RowIndex) == parseInt(table.rows[i].cells[0].children[1].value)) {
                    Row = i;
                    break;
                }
            }
        }
        $('#' + table.id + " tr:eq(" + Row + ")").remove();

        for (var i = 0; i < table.rows.length; i++) {
            NotifyDaysArray.push(table.rows[i].cells[1].innerText)
        }
        $('#' + Parent + '_tbCustomNotificatonDays tr').remove();
        BindNotificationTable(NotifyDaysArray, Parent);
        return false;
    }

    function SaveNotoficationDays(id) {
        debugger;        
        var Parent = id.replace("_btnSave", "")
        var SelectedNotificatonDays = [];
        var DefaultNotifyDays = [15, 30, 60, 90]        
        var table = document.getElementById(Parent + '_tbCustomNotificatonDays')

        for (var i = 0; i < DefaultNotifyDays.length; i++) {
            $("input:checkbox[id=" + Parent + "_chkNotifyDays" + DefaultNotifyDays[i] + "]:checked").each(function () {
                var label = $('label[for="' + this.id + '"]').text();
                SelectedNotificatonDays.push(label);
            });
        }

        for (var i = 0; i < table.rows.length; i++) {
            SelectedNotificatonDays.push(table.rows[i].cells[1].innerText)
        }
        $('#' + Parent + '_txtNotify').val(SelectedNotificatonDays.sort())
    
    }
</script>
<div style="max-height: 255px; overflow: no-display; display: block;">
    <table style="width: 100%">
        <tr>
            <td style="width: 97%">
                <div style="max-height: 166px; overflow: auto; display: block;">
                    <asp:HiddenField ID="hdnupdate" runat="server" />
                    <%--   <asp:UpdatePanel ID="upTable" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>--%>
                    <asp:Table ID="tbDueWiseCommission" runat="server" Width="100%" BorderStyle="Ridge" CellSpacing="2" HorizontalAlign="Right" BorderColor="Transparent">
                        <%-- <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">
                            <%--   <asp:TextBox ID="txtDueDate" runat="server" Width="130px" Height="18px"></asp:TextBox>
                                <asp:ImageButton ID="btnCalender" runat="server" Width="16px" Height="16px" ImageUrl="~/Images/Calander.jpeg" />
                                <ajax:CalendarExtender ID="ajaxCal" runat="server" TargetControlID="txtDueDate" PopupButtonID="btnCalender" Format="dd/MM/yyyy"></ajax:CalendarExtender> 
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:TextBox ID="txtCommPerDuedt" runat="server" Width="40px" Height="18px"></asp:TextBox>   &nbsp; % &nbsp; 
                            
                      <asp:TextBox ID="txtCommAmtDuedt" runat="server" Width="100px" Height="18px"></asp:TextBox> 
                            </asp:TableCell>
                            <asp:TableCell>
                               <asp:ImageButton ID="imgBtnDelDuedt" ImageUrl="~/Images/close.png" runat="server" /> 
                            </asp:TableCell>
                        </asp:TableRow>--%>
                    </asp:Table>
                    <%--  </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
            </td>
            <td style="width: 3%; height: 18px; vertical-align: top;">
                <asp:Button ID="btnAddDueDate" runat="server" Text="ADD" CssClass="SqureButton" Width="75px" TabIndex="31" />
                <br />
                <br />
                <asp:Button runat="server" ID="btnNotifyMe" Text="Notify Me" CssClass="SqureButton" Width="75px" Style="display:none;white-space: normal;"  OnClientClick="javascript:FillNotifyDaysPopup(this.id);return false;"  />
                <asp:HiddenField ID="hdnPopup" runat="server" />
                <ajax:ModalPopupExtender ID="mpNotifyExtendr" runat="server" PopupControlID="dvPopupNotifyDetails" TargetControlID="hdnPopup" CancelControlID="btnCancelPopup" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>
                <br />
              <%--  <asp:TextBox runat="server" ID="txtNotify" Text="15,30,60,90" Style="width: 75px;display:none;"></asp:TextBox>--%>
                <asp:HiddenField runat="server" ID="txtNotify" Value="15,30,60,90"/>
            </td>
        </tr>
        <tr>
            <td style="width: 100%" colspan="2">
                <div id="dvNewDuedtwiseComm" runat="server" style="display: none;">
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="txtAddDuedt" runat="server" Width="130px" Height="18px" TabIndex="32"></asp:TextBox>
                                <asp:ImageButton ID="ImageButton1" runat="server" Width="16px" Height="16px" ImageUrl="~/Images/Calander.jpeg" />
                                <ajax:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtAddDuedt" PopupButtonID="ImageButton1" Format="MM/dd/yyyy"></ajax:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtAddPer" runat="server" Width="40px" Height="18px" TabIndex="33"></asp:TextBox>
                                &nbsp; % &nbsp;
                        <asp:TextBox ID="txtAddAmount" runat="server" Width="100px" Height="18px" CssClass="DollarTextBox" TabIndex="34"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="btnAddDueDtComm" runat="server" Text="+" CssClass="SqureButton" OnClick="btnAddDueDtComm_Click" TabIndex="35"></asp:Button>
                                <%-- OnClientClick="controlpostback();"--%>
                            </td>
                        </tr>
                        <%--<tr>
                            <td colspan="2">
                                <asp:Button ID="btnNotifyMe" runat="server" Text="Notify Me" CssClass="SqureButton"></asp:Button>
                                <ajax:ModalPopupExtender ID="mpNotifyExtendr" runat="server" PopupControlID="dvPopupNotifyDetails" TargetControlID="btnNotifyMe" CancelControlID="btnCancelPopup" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="txtNotify" runat="server" Height="18px" Width="100px"></asp:TextBox>
                            </td>
                        </tr>--%>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnDueDatetbCount" runat="server" />
    <asp:HiddenField ID="hdnParentId" runat="server" />
</div>

<%--<asp:HiddenField id="hdnPopup" runat="server"/>
<ajax:ModalPopupExtender ID="mpNotifyExtendr" runat="server" PopupControlID="dvPopupNotifyDetails" TargetControlID="hdnPopup" CancelControlID="btnCancelPopup" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>--%>

<div id="dvPopupNotifyDetails" runat="server" class="PopupDivBodyStyle" style="display: none; height: 230px; width: 250px;">
    <table cellspacing="0" cellpadding="4" style="width: 100%">
        <asp:UpdatePanel ID="upNotifyDays" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <tr class="trBorder">
                    <td colspan="2">
                        <h3 style="margin: 6px;">Notify Days</h3>
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="btnCancelPopup" Height="15px" Width="15px" ImageUrl="~/Images/close.png" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:CheckBox runat="server" ID="chkNotifyDays15" Text="15" Checked="true" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="chkNotifyDays30" Text="30" Checked="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:CheckBox runat="server" ID="chkNotifyDays60" Text="60" Checked="true" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="chkNotifyDays90" Text="90" Checked="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">
                       <div Style="overflow: auto; height: 50px;">
                        <asp:Table runat="server" ID="tbCustomNotificatonDays" Style="Width:50%;">
                        </asp:Table>
         </div>
                        <asp:HiddenField ID="hdnCount" runat="server" Value="2" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center;">
                        <asp:TextBox ID="txtAddDays" runat="server"  Width="30%" onkeypress="return isInteger(event,this.id)"></asp:TextBox>&nbsp;&nbsp;
                        <asp:Button ID="btnAddDays" runat="server" Text="+" CssClass="SqureButton" OnClientClick="addCustomNotificationDayRows(this.id);return false;"></asp:Button>
                    </td>
                </tr>
   
                <tr>
                    <td colspan="3" style="text-align: center;">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnSmallOrange" OnClientClick="SaveNotoficationDays(this.id);" Style="height: 30px; width: 65px; margin-top: 5px;" OnClick="btnSave_Click"></asp:Button>
                    </td>
                </tr>
            </ContentTemplate>
            <Triggers>
            </Triggers>
        </asp:UpdatePanel>
    </table>
</div>


