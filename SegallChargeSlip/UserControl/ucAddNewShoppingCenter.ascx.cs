﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;

public partial class UserControl_ucAddNewShoppingCenter : System.Web.UI.UserControl
{
    # region properties
    public string LocationId
    {
        get { return hdnLocationId.Value; }
        set { hdnLocationId.Value = value; }

    }
    public string btnaddedit
    {
        get { return btnAddNew.Text; }
        set { btnAddNew.Text = value; }
    }
    public string Name
    {
        get { return txtName.Text; }
        set { txtName.Text = value; }
    }
    public string Address1
    {
        get { return txtAddress1.Text; }
        set { txtAddress1.Text = value; }
    }
    public string Address2
    {
        get { return txtAddress2.Text; }
        set { txtAddress2.Text = value; }
    }
    public string City
    {
        get { return txtCity.Text; }
        set { txtCity.Text = value; }
    }
    public string State
    {
        get { return ddlState.SelectedValue; }
        set { ddlState.SelectedValue = value; }
    }
    public string County
    {
        get { return txtCounty.Text; }
        set { txtCounty.Text = value; }
    }
    public string ZipCode
    {
        get { return txtZipCode.Text; }
        set { txtZipCode.Text = value; }
    }
    public string Longitute
    {
        get { return txtLongitude.Text; }
        set
        {
            txtLongitude.Text = value;
            lbllon.Text = value;
        }
    }
    public string Latitude
    {
        get { return txtLatitude.Text; }
        set
        {
            txtLatitude.Text = value;
            lbllat.Text = value;
        }
    }
    public int LocationType
    {
        get { return rdbLocationType.SelectedIndex; }
        set
        {
            rdbLocationType.SelectedIndex = value;
            if (value == 0)
            {
                trShoppingCenter.Attributes.Add("class", " ");
                trAddr.Attributes.Add("class", "hide");
                trAdd2.Attributes.Add("class", "hide");
                //imgMapMarker.Visible = false;
                //imgMapMarker.Attributes.Add("CssClass", "hide");
                imgMapMarker.Style.Add("display", "none");
                
                //imgMapMarker.CssClass="hide";
               // trShoppingCenter.Visible = true;
            }
            else
            {
                 trShoppingCenter.Attributes.Add("class", "hide");
                 trAddr.Attributes.Add("class", " ");
                 trAdd2.Attributes.Add("class", " ");
                 imgMapMarker.Style.Add("display", "block");
                //imgMapMarker.Attributes.Add("display", "block");
                //imgMapMarker.Attributes.Add("CssClass", " ");
                 //imgMapMarker.Visible = true;
            }
        }
    }
    public bool Isactive
    {
        get { return chkIsactive.Checked; }
        set { chkIsactive.Checked = value; }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        //txtZipCode.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtZipCode.ClientID + "')");
        txtLatitude.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtLatitude.ClientID + "')");
        txtLongitude.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtLongitude.ClientID + "')");
       
        if (!IsPostBack)
        {
            ResetControls();
        }
    }
    public void SetProperties(Locations objLoc)
    {
        //if (!string.IsNullOrEmpty(hdnLocationId.Value))
        //       {
        //           objLoc.LocationID = Convert.ToInt32(hdnLocationId.Value);
        //           objLoc.LastModifiedBy = Convert.ToInt32(Session["UserID"]);
        //           objLoc.LastModifiedOn = DateTime.Now;
        //       }
        if (objLoc.LocationID == 0)
        {
            objLoc.CreatedBy = Convert.ToInt32(Session[GlobleData.User]); //Convert.ToInt32(Session["UserID"]);
            objLoc.CreatedOn = DateTime.Now;
            objLoc.LastModifiedBy = Convert.ToInt32(Session[GlobleData.User]);// Convert.ToInt32(Session["UserID"]);
            objLoc.LastModifiedOn = DateTime.Now;
        }
        else
        {
            objLoc.LastModifiedBy = Convert.ToInt32(Session[GlobleData.User]);// Convert.ToInt32(Session["UserID"]);
            objLoc.LastModifiedOn = DateTime.Now;
        }

        if (rdbLocationType.SelectedIndex == 0)
            objLoc.ShoppingCenter = txtName.Text.Trim();
        else
        {
            objLoc.Address1 = txtAddress1.Text.Trim();
            objLoc.Address2 = txtAddress2.Text.Trim();
        }
        objLoc.City = txtCity.Text;
        objLoc.Country = txtCounty.Text;
        objLoc.State = ddlState.SelectedValue;
        objLoc.ZipCode = txtZipCode.Text;
        if (txtLongitude.Text != "" && txtLatitude.Text != "")
        {
            objLoc.Longitude = Convert.ToDecimal(txtLongitude.Text);
            objLoc.Latitude = Convert.ToDecimal(txtLatitude.Text);
        }
        objLoc.IsActive = Convert.ToBoolean(chkIsactive.Checked);
        objLoc.CreatedBy = Convert.ToInt32(Session["UserID"]);
        objLoc.CreatedOn = DateTime.Now;
        //    obj.ShoppingCenter = txtName.Text;
        //    obj.Address1 = txtAddress1.Text;
        //    obj.Address2 = txtAddress2.Text;
        //    obj.City = txtCity.Text;
        //    obj.State = ddlState.SelectedValue;
        //    obj.ZipCode = txtZipCode.Text == "" ? 0 : Convert.ToInt32(txtZipCode.Text);
        //    obj.Country = txtCounty.Text;
        //    if (obj.LocationID == 0)
        //    {
        //        obj.CreatedBy = Convert.ToInt32(Session[GlobleData.User]); //Convert.ToInt32(Session["UserID"]);
        //        obj.CreatedOn = DateTime.Now;
        //    }
        //    else
        //    {
        //        obj.LastModifiedBy = Convert.ToInt32(Session[GlobleData.User]);// Convert.ToInt32(Session["UserID"]);
        //        obj.LastModifiedOn = DateTime.Now;
        //    }

        //    if (IsAddressOnly)
        //    {
        //        obj.ShoppingCenter = ((TextBox)(this.Parent.FindControl("txtSearchLocShoppingCenter"))).Text;
        //    }
        //    //JAY
        //    if (lbllat.Text != "" && lbllon.Text != "")
        //    {
        //        obj.Latitude = Convert.ToDecimal(lbllat.Text);
        //        obj.Longitude = Convert.ToDecimal(lbllon.Text);
        //    }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        int same = 0;
        if (!string.IsNullOrEmpty(Session["UserID"].ToString()))
        {
            //Perform Validations Before Insert
            Locations objLoc = new Locations();
            if (!string.IsNullOrEmpty(hdnLocationId.Value))
            {
                objLoc.LocationID = Convert.ToInt32(hdnLocationId.Value);
                objLoc.LastModifiedBy = Convert.ToInt32(Session["UserID"]);
                objLoc.LastModifiedOn = DateTime.Now;
            }
            else
            {
                objLoc.CreatedBy = Convert.ToInt32(Session["UserID"]);
                objLoc.CreatedOn = DateTime.Now;
                objLoc.LastModifiedBy = Convert.ToInt32(Session["UserID"]);
                objLoc.LastModifiedOn = DateTime.Now;
            }
            if (rdbLocationType.SelectedIndex == 0)
                objLoc.ShoppingCenter = txtName.Text;
            objLoc.Address1 = txtAddress1.Text;
            objLoc.Address2 = txtAddress2.Text;
            objLoc.City = txtCity.Text;
            objLoc.Country = txtCounty.Text;
            objLoc.State = ddlState.SelectedValue;
            objLoc.ZipCode = txtZipCode.Text;
            if (txtLongitude.Text != "" && txtLatitude.Text != "")
            {
                objLoc.Longitude = Convert.ToDecimal(txtLongitude.Text);
                objLoc.Latitude = Convert.ToDecimal(txtLatitude.Text);
            }
            objLoc.IsActive = Convert.ToBoolean(chkIsactive.Checked);
            objLoc.CreatedBy = Convert.ToInt32(Session["UserID"]);
            objLoc.CreatedOn = DateTime.Now;
            DataTable dt = objLoc.CheckForDuplication();
            if (dt != null && objLoc.IsDuplicate == true)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (row["Address"].ToString() == row["EnteredAddress"].ToString())
                    {
                        same = 1;
                        break;
                    }
                }
                if (same == 1)
                {
                    mpDuplicateShoppingCenter.Hide();
                    bool rslt = objLoc.Save();
                    ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
                    //((UpdatePanel)(this.Parent.FindControl("upLocation"))).Update();
                    ((UpdatePanel)(this.Parent.FindControl("upNewCharge"))).Update();
                    this.Page.GetType().InvokeMember("Bindgrid", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
                    ResetControls();
                }
                // mpSameLocation.Show();
                //System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('The Location already exist in the Database');", false);
                else
                {
                    SetDuplicateRecordsinGrid(dt);
                    if (rdbLocationType.SelectedIndex == 0)
                    {
                        lblShpName.Text = txtName.Text;
                        LblActShpAddr.Text = dt.Rows[0]["EnteredAddress"].ToString().Trim(); //txtAddress1.Text + " " + txtAddress2.Text + " " + txtCity.Text + " " + txtCounty.Text + " " + txtZipCode.Text;
                    }
                    else
                    {
                        lblShpName.Text = txtAddress1.Text;
                        LblActShpAddr.Text = dt.Rows[0]["EnteredAddress"].ToString().Trim();
                    }

                    //if (objLoc.IsDuplicate == true)
                    //{
                    ViewState.Add("IsDuplicate", objLoc.IsDuplicate);
                    ViewState.Add("LocationID", objLoc.LocationID);


                    //To Show Model Popup Control In another Modeol Popup
                    AjaxControlToolkit.ModalPopupExtender mpEx = (AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny");

                    mpDuplicateShoppingCenter.Show();
                }
            }
            else
            {
                mpDuplicateShoppingCenter.Hide();
                bool rslt = objLoc.Save();
                ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
                //((UpdatePanel)(this.Parent.FindControl("upLocation"))).Update();
                ((UpdatePanel)(this.Parent.FindControl("upNewCharge"))).Update();
                this.Page.GetType().InvokeMember("Bindgrid", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
                ResetControls();
                //  rslt = objLoc.Save();

                //------------vv-------- SK : 07/25 : to verify if shopping center address saved for Next click
                //if (rslt)
                //{
                //    ((HiddenField)this.Parent.FindControl("hdnIsShoppingCenterAddressSaved")).Value = "1";
                //}
                //--------------------^^-------------------
                //UpdateAjaxCombo(rslt, objLoc);
                //  ((UpdatePanel)(this.Parent.FindControl("upNewCharge"))).Update();


            }

        }
    }
    public void SetDuplicateRecordsinGrid(DataTable dt)
    {

        gvDupLocations.DataSource = dt;
        gvDupLocations.DataBind();
        upDupLoc.Update();
        mpDuplicateShoppingCenter.Show();
    }

    #region Custom Actions

    public void ResetControls()
    {
        Utility.FillStates(ref ddlState);
        txtName.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtCity.Text = "";
        txtCounty.Text = "";
        txtZipCode.Text = "";
        txtLatitude.Text = "";
        txtLongitude.Text = "";
        chkIsactive.Checked = true;
        rdbLocationType.SelectedIndex = 0;
        imgMapMarker.Style.Add("display", "none");

    }
    protected void btnUpdate_ShoppingCenter(object sender, EventArgs e)
    {
        //if (!string.IsNullOrEmpty(ViewState["LocationID"].ToString()))        //---- SK : 07/24 : removed
        //{

        //  objLoc.LocationID = Convert.ToInt32(ViewState["LocationID"].ToString());
        Locations objLoc = new Locations();
        SelectUpdateLocation(objLoc);
        SetProperties(objLoc); //--- SK : set the porperties in master page
        bool rslt = objLoc.Save();
        mpDuplicateShoppingCenter.Hide();
        ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
        ((UpdatePanel)(this.Parent.FindControl("upNewCharge"))).Update();
        this.Page.GetType().InvokeMember("Bindgrid", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
        ResetControls();

        //objLoc.Address1 = txtAddress1.Text;
        //objLoc.Address2 = txtAddress2.Text;
        //UpdateAjaxCombo(rslt, objLoc);
        // ((UpdatePanel)(this.Parent.FindControl("upNewCharge"))).Update();
        //  IsAddressSaved = "0";
        //-------vv---------- SK : 07/24 : Hide the modal issue
        // ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
        // ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
        //mpDuplicateShoppingCenter.Hide();
        // mpDuplicateCompany.Hide(); // --- SK : 08/07 : duplicate company modification
        //if (IsAddressOnly)
        //{
        //    //JAY
        //    //((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
        //    //((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
        //    //mpDuplicateShoppingCenter.Hide();
        //    //mpDuplicateCompany.Hide(); // --- SK : 08/07 : duplicate company modification
        //    this.Page.GetType().InvokeMember("ContinueFromUpdateExistingToNextStep", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
        //}
        //if (ISNEXT)
        //{
        //    HiddenField hdnIsShoppingCenterAddressSaved = (HiddenField)this.Parent.FindControl("hdnIsShoppingCenterAddressSaved");
        //    hdnIsShoppingCenterAddressSaved.Value = "1";


        //    //    ((HiddenField)this.Parent.FindControl("hdnIsShoppingCenterAddressSaved")).Value = "1"; //JAY 
        //    //    ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
        //    //    ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
        //    //    mpDuplicateShoppingCenter.Hide();
        //    //    mpDuplicateCompany.Hide(); // --- SK : 08/07 : duplicate company modification

        //    this.Page.GetType().InvokeMember("ContinueFromUpdateExistingToNextStep", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
        //}
        //---------^^--------
        // }
    }
    protected void btnAddNew_ShoppingCenter(object sender, EventArgs e)
    {
        Locations objLoc = new Locations();
        objLoc.LocationID = 0;
        SetProperties(objLoc);
        bool rslt = objLoc.Save();
        mpDuplicateShoppingCenter.Hide();
        ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
        //((UpdatePanel)(this.Parent.FindControl("upLocation"))).Update();
        ((UpdatePanel)(this.Parent.FindControl("upNewCharge"))).Update();
        this.Page.GetType().InvokeMember("Bindgrid", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
        ResetControls();
        // UpdateAjaxCombo(rslt, objLoc);
        // ((UpdatePanel)(this.Parent.FindControl("upNewCharge"))).Update();
        //-------vv---------- SK : 07/24 : Hide the modal issue
        //if (IsAddressOnly)
        //{
        //    ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
        //    ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
        //    mpDuplicateShoppingCenter.Hide();
        //    mpDuplicateCompany.Hide(); // --- SK : 08/07 : duplicate company modification
        //    this.Page.GetType().InvokeMember("ContinueFromUpdateExistingToNextStep", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
        //}
        //if (ISNEXT)
        //{
        //    ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
        //    ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
        //    mpDuplicateShoppingCenter.Hide();
        //    mpDuplicateCompany.Hide(); // --- SK : 08/07 : duplicate company modification
        //    this.Page.GetType().InvokeMember("ContinueFromUpdateExistingToNextStep", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
        //}
        //---------^^--------645
    }
    #endregion
    #region GridEvents

    protected void gvDupLocations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Style.Add("display", "none");
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Style.Add("display", "none");
            if (e.Row.RowIndex == 0)
            {
                ((RadioButton)(e.Row.FindControl("rbSelLocForUpdate"))).Checked = true;
            }
        }
    }
    private void SelectUpdateLocation(Locations obj)
    {
        foreach (GridViewRow gRow in gvDupLocations.Rows)
        {
            RadioButton rb = (RadioButton)gRow.FindControl("rbSelLocForUpdate");
            if (rb.Checked == true)
            {
                gRow.Cells[0].Style.Remove("display");
                obj.LocationID = Convert.ToInt32(gRow.Cells[0].Text);
            }
        }
    }
    //public void GetTheSelectedLocation(Locations objLocation)
    //{
    //    SelectUpdateLocation(objLocation);
    //    objLocation.Load();
    //    objLocation.ShoppingCenter = ((TextBox)this.Parent.FindControl("txtSearchLocShoppingCenter")).Text;

    //}
    #endregion
}