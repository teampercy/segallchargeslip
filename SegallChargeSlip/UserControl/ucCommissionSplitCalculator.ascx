﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCommissionSplitCalculator.ascx.cs" Inherits="UserControl_ucCommissionSplitCalculator" %>
<%--<%@ Register Src="~/UserControl/ucAddNewBroker.ascx" TagName="AddBroker" TagPrefix="uc" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>


<link href="../Styles/SiteTEST.css" rel="stylesheet" />
<style type="text/css">
    /*.ui-state-default .ui-icon { background-image: url('../Images/close.png') !important;/*{iconsDefault}*/
    /*}*/

    .ui-widget-header {
        /*background-image:none;
    background-color:gray;*/
    }

    .btnAddBrokertype {
        font-size: .9em;
        font-weight: bold;
        text-align: center;
        width: 70px;
        height: 24px;
        background-color: gray;
    }

    .td-BrokerName {
        width: 64%;
        text-align: left;
        padding-left: 2px;
    }

    .td-SharePercentage {
        width: 16%;
        text-align: right;
    }

    .td-Commission {
        text-align: right;
        padding-left: 4px;
        width: 20%;
    }

    .td-DeleteRecord {
        width: 4px;
        border-right: none !important;
    }

    .tbBorder {
        border: none;
        border-color: transparent;
        border-collapse: collapse;
        /*border: 1px solid #CCC;
        border-collapse: collapse;*/
    }

    .tddynamictotper {
        border: none !important;
    }

    .tddynamicshareper {
        border-left: none !important;
        /*border-right: none !important;*/
        border-color: gray !important;
        border-top: none !important;
    }

    .tdgray {
        border-color: gray !important;
        border-top: none !important;
        border-left: none !important;
    }
    /*.tbBorder tr td {
            border: none;
        }*/

    .textWrap {
        text-wrap: normal;
        word-break: break-all;
    }

    .ui-dialog .ui-dialog-title {
        font-size: large;
    }
</style>

<script type="text/javascript">
    function Validate(event) {

        var regex = new RegExp("^[0-9 \-]+$");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    }
    $(document).ready(function () {
        $(".ui-dialog-titlebar-close").css('background-color', 'Gray');
        $(".ui-dialog-titlebar-close").css('background-image', 'url(../Images/close.png)');
        $(".ui-dialog-titlebar-close").css('border', 'none');
        $(".ui-dialog-titlebar-close").css('background-repeat', 'no-repeat');
    });
    function validateZipLen(strzip) {
        var strziptrm = $.trim(strzip);
        if (strziptrm.length <= 10 && strziptrm.length >= 5) {
            return true;
        }
        return false;
    }

    $(document).ready(function () {
        SetCommissionValues();
    });
    function SetCommissionValues() {
        $("#ContentMain_CommCalculator_hdnTotalComm").val($("#ContentMain_hdnLeaseCommTot").val());
        $("#ContentMain_OptCommCalculator_hdnTotalComm").val($("#ContentMain_hdnOptCommTot").val());
        $("#ContentMain_ContingentCommCalculator_hdnTotalComm").val($("#ContentMain_hdnContigentTot").val());

        $("#ContentMain_Opt1CommCalculator_hdnTotalComm").val($("#ContentMain_hdnOptCommTot1").val());
        $("#ContentMain_Opt2CommCalculator_hdnTotalComm").val($("#ContentMain_hdnOptCommTot2").val());
        $("#ContentMain_Opt3CommCalculator_hdnTotalComm").val($("#ContentMain_hdnOptCommTot3").val());
    }
    function EmailValidation() {
        var Input = $('#ContentMain_CommCalculator_txtEmail').val();

        var regExp = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;

        if (!regExp.test(Input)) {
            $('#ContentMain_CommCalculator_txtEmail').val('');
        }
        else {

            return true;
        }

        return false;
    }
    function ShowPopUp(title, tableRef, Parent) {
        debugger;
        $('#' + Parent + '_dvAddBroker').dialog('option', 'title', title);
        clearAddExternalBroker(Parent);
        ClearCompany(Parent);
        //if (title == 'CO-BROKER') {
        //    $("#" + Parent + "_txtCompany").val('abc');
        //}
        ClearBrokerDropDown(Parent);
        $('#' + Parent + '_lblErrorMsg').text("");
        $("#" + Parent + "_dvAddBroker").dialog("open");
        //$("#" + Parent + "_dvAddBroker").dialog("open");
        //$('#' + Parent + '_dvAddBroker').dialog({
        //    title: $('#' + Parent + '_dvAddBroker').attr('title') + title
        //});
        $('[id^=' + Parent + '_ddlState]').attr("disabled", false);
        $('[id^=' + Parent + '_hdnBrokerType]').val(tableRef);
        // do not set bellow hidden field as per parent
        $('[id^=ContentMain_CommCalculator_hdnUcName]').val(Parent);
        $('[id^=' + Parent + '_txtName]').attr("disabled", false);
        if (tableRef == "tblCo_Broker" || tableRef == "tblClb_Broker") {
            //document.getElementById(Parent + '_tr_BrokerDropDown').style.display = 'block';
            $('[id^=' + Parent + '_tr_BrokerDropDown]').show();
            //hiding Company tr for co broker
            $('[id^=' + Parent + '_tr_BrokerCompany]').hide();
        }
        else {
            $('[id^=' + Parent + '_tr_BrokerDropDown]').hide();
            $('[id^=' + Parent + '_tr_BrokerCompany]').show();


            //$('[id^=' + Parent + '_txtAddress]').attr("disabled", false);
            //$('[id^=' + Parent + '_txtCity]').attr("disabled", false);
            //$('[id^=' + Parent + '_ddlState]').attr("disabled", false);
            //$('[id^=' + Parent + '_txtZipCode]').attr("disabled", false);
            $('[id^=' + Parent + '_txtName]').attr("disabled", false);
            //$('[id^=' + Parent + '_txtEmail]').attr("disabled", false);
            //$('[id^=' + Parent + '_txtEin]').attr("disabled", false);
        }
    }

    function ShowEditPopup(title, tableRef, Parent) {
        debugger;
        clearEditDiv(Parent);
        //$('#' + Parent + '_dvEditRecord').dialog('option', 'title', title);
        $('#' + Parent + '_dvEditRecord').dialog('option', 'title', title);
        $("#" + Parent + "_dvEditRecord").dialog("open");
        // do not set bellow hidden field as per parent
        $('[id^=ContentMain_CommCalculator_hdnUcName]').val(Parent);

    }

    function ShowGridPopup(Parent) {

        $("#" + Parent + "_dvGetSameCmpNmBrokers").dialog("open");
    }
    function CloseGridPopup(Parent) {

        $("#" + Parent + "_dvGetSameCmpNmBrokers").dialog("close");
    }
    function ShowDupBrokerPopup(Parent) {

        $("#" + Parent + "_dvDuplicateBrokerEntry").dialog("open");
    }
    function CloseDupBrokerdPopup(Parent) {

        var tb = Parent + "_tbDuplicateBroker";
        $("#" + tb).empty();
        $("#" + Parent + "_dvDuplicateBrokerEntry").dialog("close");
    }



    function UpdateCommPercentage() {
        debugger;
    
        Parent = $('[id^=ContentMain_CommCalculator_hdnUcName]').val();
        var NewCommPer = $('[id^=' + Parent + '_txtEditSharePer]').val();
        var NewCommAmt = $('[id^=' + Parent + '_txtEditShareAmt]').val();
        var RowIndex = $('[id^=' + Parent + '_hdnRowTndex]').val();
        var tableRef = $('[id^=' + Parent + '_hdnBrokerType]').val();

        // New changes
        var ActualPer = $('[id^=' + Parent + '_hdnSharePer]').val();

        var tb = Parent + "_" + tableRef;
        var table = document.getElementById(tb);
        //change the parameter value NewCommPer to ActualPer
        if (ValidateForPercentageOnEdit(table, table.rows.length, ActualPer, table.rows[RowIndex].cells[0].children[0].value, RowIndex) == true) {
            var BrokerId = table.rows[RowIndex].cells[0].children[0].value;
            $('[id^=' + Parent + '_hdnBrokerId]').val(BrokerId);
            var DealBrokerCommissionId = table.rows[RowIndex].cells[0].children[1].value;
            $('[id^=' + Parent + '_hdnDealBrokerCommissionId]').val(DealBrokerCommissionId);
            if (NewCommPer != "" && NewCommAmt != "") {
                //var BrokerId = table.rows[RowIndex].cells[0].children[0].value;
                //$('[id^=' + Parent + '_hdnBrokerId]').val(BrokerId);
                //var DealBrokerCommissionId = table.rows[RowIndex].cells[0].children[1].value;
                //$('[id^=' + Parent + '_hdnDealBrokerCommissionId]').val(DealBrokerCommissionId);

                table.rows[RowIndex].cells[1].innerHTML = parseFloat(NewCommPer).toFixed(2) + '%';                
                table.rows[RowIndex].cells[2].innerHTML = parseFloat(NewCommAmt).toFixed(2);

                table.rows[RowIndex].cells[0].children[2].value = ActualPer;
                table.rows[RowIndex].cells[0].children[3].value = NewCommAmt;
            }
            SetCommAmoutForEachBrokerType(tableRef, true, Parent);

            if (NewCommPer != "" && NewCommAmt != "") {
                UpadteBrokerAmountDetails(Parent);
            }

            //Validate broker edit popup and then update the broker details.
            if (ValidateBrokerEditfields(Parent) == "true") {
                UpadteBrokerDetails(Parent);
            }
            else
            {
                return false;
            }

            $("#" + Parent + "_dvEditRecord").dialog("close");
            clearEditDiv(Parent);
            return true;
        }
        else {
            alert("Total Broker Percentage Cannot Be Greater Than 100%");
            return false;
        }
        //}
        //else {
        //    return false;
        //}
        //txtEditSharePer txtEditShareAmt
    }

    $(function () {

        //dvAddBroker
        $("#ContentMain_OptCommCalculator_dvAddBroker").dialog({
            autoOpen: false,
            width: 550,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });


        $("#ContentMain_CommCalculator_dvAddBroker").dialog({
            autoOpen: false,
            width: 550,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        $("#ContentMain_ContingentCommCalculator_dvAddBroker").dialog({
            autoOpen: false,
            width: 550,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        $("#ContentMain_Opt1CommCalculator_dvAddBroker").dialog({
            autoOpen: false,
            width: 550,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        $("#ContentMain_Opt2CommCalculator_dvAddBroker").dialog({
            autoOpen: false,
            width: 550,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        $("#ContentMain_Opt3CommCalculator_dvAddBroker").dialog({
            autoOpen: false,
            width: 550,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });
        // $("#dvAddBroker").parent().appendTo($("form:first")); dvEditRecord CommCalculator

        //dvEditRecord
        $("#ContentMain_CommCalculator_dvEditRecord").dialog({
            autoOpen: false,
            //width: 250,
            width: 550,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        $("#ContentMain_OptCommCalculator_dvEditRecord").dialog({
            autoOpen: false,
            //width: 250,
            width: 550,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        $("#ContentMain_ContingentCommCalculator_dvEditRecord").dialog({
            autoOpen: false,
            //width: 250,
            width: 550,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        $("#ContentMain_Opt1CommCalculator_dvEditRecord").dialog({
            autoOpen: false,
            //width: 250,
            width: 550,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        $("#ContentMain_Opt2CommCalculator_dvEditRecord").dialog({
            autoOpen: false,
            //width: 250,
            width: 550,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        $("#ContentMain_Opt3CommCalculator_dvEditRecord").dialog({
            autoOpen: false,
            //width: 250,
            width: 550,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        //dvGetSameCmpNmBrokers
        $("#ContentMain_CommCalculator_dvGetSameCmpNmBrokers").dialog({
            autoOpen: false,
            width: 400,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }

        });
        $("#ContentMain_OptCommCalculator_dvGetSameCmpNmBrokers").dialog({
            autoOpen: false,
            width: 400,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });
        $("#ContentMain_ContingentCommCalculator_dvGetSameCmpNmBrokers").dialog({
            autoOpen: false,
            width: 400,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        $("#ContentMain_Opt1CommCalculator_dvGetSameCmpNmBrokers").dialog({
            autoOpen: false,
            width: 400,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        $("#ContentMain_Opt2CommCalculator_dvGetSameCmpNmBrokers").dialog({
            autoOpen: false,
            width: 400,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        $("#ContentMain_Opt3CommCalculator_dvGetSameCmpNmBrokers").dialog({
            autoOpen: false,
            width: 400,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            }
        });

        //dvDuplicateBrokerEntry
        $("#ContentMain_CommCalculator_dvDuplicateBrokerEntry").dialog({
            autoOpen: false,
            width: 600,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            },
            close: function (ev, ui) {
                CloseDupBrokerdPopup("ContentMain_CommCalculator");
            }
        });

        $("#ContentMain_OptCommCalculator_dvDuplicateBrokerEntry").dialog({
            autoOpen: false,
            width: 600,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            },
            close: function (ev, ui) {
                CloseDupBrokerdPopup("ContentMain_OptCommCalculator");
            }
        });

        $("#ContentMain_ContingentCommCalculator_dvDuplicateBrokerEntry").dialog({
            autoOpen: false,
            width: 600,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            },
            close: function (ev, ui) {
                CloseDupBrokerdPopup("ContentMain_ContingentCommCalculator");
            }
        });

        $("#ContentMain_Opt1CommCalculator_dvDuplicateBrokerEntry").dialog({
            autoOpen: false,
            width: 600,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            },
            close: function (ev, ui) {
                CloseDupBrokerdPopup("ContentMain_Opt1CommCalculator");
            }
        });

        $("#ContentMain_Opt2CommCalculator_dvDuplicateBrokerEntry").dialog({
            autoOpen: false,
            width: 600,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            },
            close: function (ev, ui) {
                CloseDupBrokerdPopup("ContentMain_Opt2CommCalculator");
            }
        });

        $("#ContentMain_Opt3CommCalculator_dvDuplicateBrokerEntry").dialog({
            autoOpen: false,
            width: 600,
            modal: true,
            show: {
                duration: 300
            },
            hide: {
                duration: 300
            },
            close: function (ev, ui) {
                CloseDupBrokerdPopup("ContentMain_Opt3CommCalculator");
            }
        });
    });
    function Validatefields(Parent) {
        
        //debugger;
        //alert(Parent);
        var lblerror = document.getElementById('ContentMain_CommCalculator_lblErrorMsg');
        lblerror.innerHTML = "";
        $('[id^=' + Parent + '_txtEditSharePer]').val();
        var Company = $('#' + Parent + '_txtCompany').val();
        var Address = $('#' + Parent + '_txtAddress').val();
        var City = $('#' + Parent + '_txtCity').val();
        var txtZipCode = $('#' + Parent + '_txtZipCode').val();
        var brokers = $('#' + Parent + '_txtName').val();
        var txtEmail = $('#' + Parent + '_txtEmail').val();
        var txtEin = $('#' + Parent + '_txtEin').val();
        var sharePercentage = $('#' + Parent + '_txtShare').val();
        var commisionAmount = $('#' + Parent + '_txtSetAmount').val();
        var hdnBrokerType = $('#' + Parent + '_hdnBrokerType').val();
        var state = $('#' + Parent + '_ddlState').val();
        var flag = "true";
        if (Company.trim() == "" || Company == null) {
            flag = "false";
            lblerror.innerHTML = "Company Name cannot be blank<br>";
        }
        if (Address.trim() == "") {
            flag = "false";
            lblerror.innerHTML += "Address cannot be blank";
        }
        if (City.trim() == "") {
            flag = "false";
            lblerror.innerHTML += "<br>City cannot be blank";
        }
        if (state.trim() == "") {
            flag = "false";
            lblerror.innerHTML += "<br>State cannot be blank";
        }

        if (txtZipCode == null || txtZipCode.trim() == "") {
            flag = "false";
            lblerror.innerHTML += "<br>Zipcode cannot be blank";
        }
        else {
            if (!validateZipLen(txtZipCode)) {
                lblerror.innerHTML += "<br>Enter valid Zipcode";
                flag = "false";
            }
        }
        if (brokers.trim() == "" || brokers == null) {
            flag = "false";
            lblerror.innerHTML += "<br>Name cannot be blank";
        }
        //if (txtEmail.trim() == "" || txtEmail == null) {
        //    flag = "false";
        //    lblerror.innerHTML += "<br>Email cannot be blank";
        //}
        //if (txtEin.trim() == "" || txtEin == null) {
        //    flag = "false";
        //    lblerror.innerHTML += "<br>EIN cannot be blank";
        //}
        if (sharePercentage.trim() == "" || sharePercentage == null) {
            flag = "false";
            lblerror.innerHTML += "<br>Share Percentage cannot be blank";
        }
        if (commisionAmount.trim() == "" || commisionAmount == null) {
            flag = "false";
            lblerror.innerHTML += "<br>Set Amount cannot be blank";
        }
        return flag;
    }

    function btnAddExternalBroker_Save(Parent) {
        debugger;
        // alert('name cannot be blank');
        // alert(Parent);
        //var lblerror = $('[id^=' + Parent + '_lblErrorMsg]').val();

        if (Validatefields(Parent) == "true") {
            //EmailValidation();
            //alert('save');
            //test for duplication

            CheckForDupicateEntry(Parent);
        }
        //$('[id^=' + Parent + '_hdnBrokerId]').val(brokerId);
        //if (appendExternalBrokerFromPopup(brokers, sharePercentage, commisionAmount, hdnBrokerType, brokerId, Parent) == true) {
        //    $("#" + Parent + "_dvAddBroker").dialog("close");
        //    clearAddExternalBroker(Parent);
        //    EditCommissionPer(Parent);
        //}
        //else {
        //    return false;
        //}
    }

    function clearAddExternalBroker(Parent) {

        $('[id^=' + Parent + '_txtAddress]').val("");
        $('[id^=' + Parent + '_txtCity]').val("");
        $('[id^=' + Parent + '_ddlState]').val("");
        $('[id^=' + Parent + '_txtZipCode]').val("");
        $('[id^=' + Parent + '_txtName]').val("");
        $('[id^=' + Parent + '_txtEmail]').val("");
        $('[id^=' + Parent + '_txtEin]').val("");
        $('[id^=' + Parent + '_txtShare]').val("");
        $('[id^=' + Parent + '_txtSetAmount]').val("");

    }

    function ClearBrokerDropDown(Parent) {
       
        $("#" + Parent + "_ddlBrokers").empty();
    }

    function ClearCompany(Parent) {
        $('[id^=' + Parent + '_txtCompany]').val("");
    }
    function clearEditDiv(Parent) {
        $('[id^=' + Parent + '_txtEditShareAmt]').val("");
        $('[id^=' + Parent + '_txtEditSharePer]').val("");

        $('[id^=' + Parent + '_txtEditCompany]').val("");
        $('[id^=' + Parent + '_txtEditAddress]').val("");
        $('[id^=' + Parent + '_txtEditCity]').val("");
        $('[id^=' + Parent + '_ddlEditState]').val("");
        $('[id^=' + Parent + '_txtEditZipCode]').val("");
        $('[id^=' + Parent + '_txtEditName]').val("");
        $('[id^=' + Parent + '_txtEditEmail]').val("");
        $('[id^=' + Parent + '_txtEditEin]').val("");

        var lblEditErrorMsg = document.getElementById('ContentMain_CommCalculator_lblEditErrorMsg');
        lblEditErrorMsg.innerHTML = "";
    }

    function appendExternalBrokerFromPopup(brokers, sharePercentage, commisionAmount, tableRef, brokerID, Parent, DealBrokerCommissionId) {
        debugger;
        SetCommissionValues();
        var tb = Parent + "_" + tableRef;
        var table = document.getElementById(tb);
        var rowCount = table.rows.length;
        //Validate For Proper Percentage 

        if (rowCount > 1) {
            if (ValidateForPercentage(table, rowCount, sharePercentage) == false) {
                alert("Total Broker Percentage Cannot Be Greater Than 100%");
                $('[id^=' + Parent + '_txtShare]').focus();
                return false;
            }

        }

        var CommissionOn = parseFloat($('[id^=' + Parent + '_hdnTotalComm]').val());
        if (commisionAmount != "") {
            var totPer = (parseFloat(commisionAmount) / CommissionOn) * 100;

            var tb1 = Parent + "_" + tableRef + "_PerTotal";
            var table1 = document.getElementById(tb1);
            var rowCount1 = table1.rows.length;
            var row1 = table1.insertRow(rowCount1);
            var cell0 = row1.insertCell(0);
            cell0.style.height = "19px";
            cell0.id = Parent + "_" + tableRef + "_PerTotal" + rowCount1;
            var cell0_element0 = document.createTextNode(parseFloat(totPer).toFixed(2) + '%');
            //alert("total" + totPer);
            cell0_element0.id = tableRef + "_PerTotal" + rowCount1;
            cell0.appendChild(cell0_element0);
            cell0.className = "tddynamictotper";
        }
        var row = table.insertRow(rowCount);
        var broker = brokers.replace("#", "'");
        if (tableRef == "tblClb_Broker") {
            broker = broker + ' (CLB-Broker)';
        }
        //brokers 
        var cell0 = row.insertCell(0);
        $(cell0).addClass("td-BrokerName");
        $(cell0).addClass("tdgray");
        var cell0_element0 = document.createTextNode(broker);

        var cell0_element1 = document.createElement("input");
        cell0_element1.type = "hidden";
        cell0_element1.id = "hdnBrokerID_" + rowCount;
        cell0_element1.name = "hdnBrokerID_" + rowCount;
        cell0_element1.value = brokerID;

        var cell0_element2 = document.createElement("input");
        cell0_element2.type = "hidden";
        cell0_element2.id = "hdnDealBrokerCommissionId_" + rowCount;
        cell0_element2.name = "hdnDealBrokerCommissionIdID_" + rowCount;
        cell0_element2.value = DealBrokerCommissionId;

        var cell0_element3 = document.createElement("input");
        cell0_element3.type = "hidden";
        cell0_element3.id = "hdnSharePer_" + rowCount;
        cell0_element3.name = "hdnSharePer_" + rowCount;
        cell0_element3.value = parseFloat(sharePercentage);

        var cell0_element4 = document.createElement("input");
        cell0_element4.type = "hidden";
        cell0_element4.id = "hdnShareAmt_" + rowCount;
        cell0_element4.name = "hdnShareAmt_" + rowCount;
        cell0_element4.value = parseFloat(commisionAmount);


        // alert(DealBrokerCommissionId);
        //var cell0_element0 = document.createElement("input");
        //cell0_element0.type = "text";
        //cell0_element0.width = "100%";

        //cell0_element0.value = brokers;
        //cell0_element0.style.width = "100%";
        //cell0_element0.style.height = "30px";
        //cell0_element0.readOnly = true;
        cell0.appendChild(cell0_element0);
        cell0.appendChild(cell0_element1);
        cell0.appendChild(cell0_element2);
        cell0.appendChild(cell0_element3);
        cell0.appendChild(cell0_element4);



        //sharePercentage
        var cell1 = row.insertCell(1);
        $(cell1).addClass("td-SharePercentage");
        cell1.className = "tddynamicshareper";        
        var cell1_element0 = document.createTextNode(parseFloat(sharePercentage).toFixed(2) + '%');

        //alert(sharePercentage);
        //alert(parseFloat(sharePercentage).toFixed(2));

        //cell1_element0.type = "text";
        // cell1_element0.name = "SharePercentage" + rowCount;
        //cell1_element0.value = sharePercentage;
        //cell1_element0.style.width = "100%";
        //cell1_element0.style.height = "30px";
        //cell1_element0.readOnly = true;
        cell1.appendChild(cell1_element0);

        //commisionAmount
        var cell2 = row.insertCell(2);
        $(cell2).addClass("td-Commission");
        $(cell2).addClass("tdgray");
        var cell2_element0 = document.createTextNode("$" + parseFloat(commisionAmount).toFixed(2));
        //cell2_element0.id = "CommissionAmt" + rowCount; ;
        //cell2_element0.name = "CommissionAmt" + rowCount;
        cell2.appendChild(cell2_element0);


        //Delete button
        var cell3 = row.insertCell(3);
        var cell3_element0 = document.createElement("input");
        $(cell3).addClass("tdgray");
        $(cell3).addClass("td-DeleteRecord");
        cell3_element0.type = "image";
        cell3_element0.src = " ../Images/delete.gif";
        cell3_element0.setAttribute('onclick', 'javascript:DeleteRecord("' + rowCount + '","' + tableRef + '","' + Parent + '","' + DealBrokerCommissionId + '","' + brokerID + '");return false;');
        cell3.appendChild(cell3_element0);

        var LoggedInUser = '<%=Session["UserId"]%>';
        //var loggedInUser = $("#lbUser").text();
        if (brokerID == LoggedInUser && tableRef != "tblClb_Broker" && Parent == 'ContentMain_CommCalculator') {
            //cell3_element0.disabled = true;
            cell3_element0.disabled = true;
        }


        //CalCulate Group Total
        //
        //var iCnt;
        //var totGrpAmt = 0;
        //for (iCnt = 1; iCnt <= rowCount; iCnt++) {
        //    // alert(rowCount); 
        //    totGrpAmt = parseFloat(totGrpAmt) + parseFloat(table.rows[iCnt].cells[2].innerText);
        //}
        //if (totGrpAmt > 0) {

        //    var CommAmt = SetCommAmoutForEachBrokerType(tableRef,true);
        //    CalBrokerGroupTotalAmt(totGrpAmt, tableRef);
        //}

        var CommAmt = SetCommAmoutForEachBrokerType(tableRef, true, Parent);
        clearAddExternalBroker(Parent);
        ClearBrokerDropDown(Parent);
        ClearCompany(Parent);
        EditCommissionPer(Parent);
        //Addtosummary(Parent);

        return true;
    }

    function Cleartb(Parent, tbName) {

        var tb = Parent + "_tbBrokerFromSameCmp";
        $('#' + tb).empty();
    }

    function AddSameCompanyBrokers(Parent, brokerID, Name, Address, PerCentage, SetAmount) {
        //Cleartb(Parent, 'tbBrokerFromSameCmp');
    
        var tb = Parent + "_tbBrokerFromSameCmp";
        var table = document.getElementById(tb);
        var rowCount = table.rows.length;



        //if (rowCount > 1) {
        //    var i = 1;
        //    for (i = 1; i <= rowCount; i++) {
        //        $("#" + tb + " tr:eq(" + i + ")").remove();
        //    }
        //}
        var row = table.insertRow(rowCount);


        // Hidden Field and Check Box--cell-0
        var cell0 = row.insertCell(0);

        cell0.style.width = "15px";

        var cell0_element0 = document.createElement("input");
        cell0_element0.type = "hidden";
        cell0_element0.id = "hdnBrokerID_" + rowCount;
        cell0_element0.name = "hdnBrokerID_" + rowCount;
        cell0_element0.value = brokerID;

        var cell0_element1 = document.createElement("input");
        cell0_element1.type = "checkbox";
        cell0_element1.id = "chk_" + rowCount;
        cell0_element1.name = "chk_" + rowCount;
        cell0_element1.setAttribute('onclick', 'javascript:SelectOneCheckBox("' + cell0_element1.id + '","' + brokerID + '","' + Parent + '");');
        $(cell0_element1).addClass("cbRowItem");
        cell0_element1.style.textAlign = "left";
        cell0.style.width = "6%";
        cell0.style.paddingLeft = "2px";
        cell0.appendChild(cell0_element0);
        cell0.appendChild(cell0_element1);

        //cell-1

        var cell1 = row.insertCell(1);
        cell1.style.width = "30%";
        cell1.style.textAlign = "left";
        cell1.style.paddingLeft = "2px";
        var cell1_element0 = document.createTextNode(Name);
        cell1.appendChild(cell1_element0);

        var cell2 = row.insertCell(2);
        cell2.style.width = "64%";
        cell2.style.textAlign = "left";
        cell2.style.paddingLeft = "2px";
        var cell2_element0 = document.createTextNode(Address);
        cell2.appendChild(cell2_element0);


        ShowGridPopup(Parent);
        //var cell3 = row.insertCell(3);
        //cell3.style.width = "15%";
        //cell3.style.textAlign = "right";
        //$(cell3).addClass("td-SharePercentage");
        //var cell3_element0 = document.createTextNode(PerCentage);
        //cell3.appendChild(cell3_element0);

        //var cell4 = row.insertCell(4);
        //cell4.style.width = "20%";
        //cell4.style.textAlign = "right";
        //$(cell4).addClass("td-SharePercentage");
        //var cell4_element0 = document.createTextNode(roundToTwo(parseFloat(SetAmount)));
        //cell4.appendChild(cell4_element0);
    }

    function AddDuplicateBrokers(Parent, brokerID, Name, Address, PerCentage, SetAmount, EMail, EIN) {
       
        //Cleartb(Parent);
        var tb = Parent + "_tbDuplicateBroker";
        var table = document.getElementById(tb);
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);

        //if (rowCount >= 2) {
        //    var i = 1;
        //    for (i = 1; i <= rowCount; i++) {
        //        $("#" + tb + " tr:eq(" + i + ")").remove();
        //    }

        //}
        // Hidden Field and Check Box--cell-0
        // var cell0 = row.insertCell(0);

        var cell0_element0 = document.createElement("input");
        cell0_element0.type = "hidden";
        cell0_element0.id = "hdnBrokerID_" + rowCount;
        cell0_element0.name = "hdnBrokerID_" + rowCount;
        cell0_element0.value = brokerID;

        var cell0 = row.insertCell(0);
        cell0.style.width = "6%";
        var cell0_element1 = document.createElement("input");
        cell0_element1.type = "checkbox";
        cell0_element1.id = "chkUpBroker_" + rowCount;
        cell0_element1.name = "chkUpBroker_" + rowCount;
        cell0_element1.setAttribute('onclick', 'javascript:SelectOneCheckBox("' + cell0_element1.id + '","' + brokerID + '","' + Parent + '");');
        $(cell0_element1).addClass("cbRowItem");
        cell0.appendChild(cell0_element1);
        //  cell0.appendChild(cell0_element1);

        //cell-1

        var cell1 = row.insertCell(1);
        cell1.style.textAlign = "left";
        cell1.style.width = "15%";
        var cell1_element0 = document.createTextNode(Name);

        cell1.appendChild(cell0_element0);
        cell1.appendChild(cell1_element0);

        var cell2 = row.insertCell(2);
        cell2.style.textAlign = "left";
        cell2.style.width = "49%";
        cell2.className = "textWrap";
        var cell2_element0 = document.createTextNode(Address);
        cell2.appendChild(cell2_element0);

        var cell3 = row.insertCell(3);
        cell3.style.textAlign = "left";
        cell3.style.width = "20%";
        cell3.className = "textWrap";
        var cell3_element0 = document.createTextNode(EMail);
        cell3.appendChild(cell3_element0);

        var cell4 = row.insertCell(4);
        cell4.style.textAlign = "left";
        cell4.style.width = "10%";
        cell4.className = "textWrap";
        var cell4_element0 = document.createTextNode(EIN);
        cell4.appendChild(cell4_element0);

        //var cell5 = row.insertCell(5);
        //cell5.style.textAlign = "right";
        //cell5.style.width = "15%";
        //var cell5_element0 = document.createTextNode(PerCentage);
        //cell5.appendChild(cell5_element0);

        //var cell6 = row.insertCell(6);
        //cell6.style.textAlign = "right";
        //cell6.style.width = "15%";
        //var cell6_element0 = document.createTextNode(roundToTwo(parseFloat(SetAmount)));
        //cell6.appendChild(cell6_element0);
    }

    function SelectOneCheckBox(id, BrokerId, Parent) {
        //debugger;
        var $Selchk = $("#" + id);
        if ($Selchk.is(":checked")) {
            var group = $('input:checkbox.cbRowItem');;
            //$(group).prop("checked", false);  commented by sudhakar 20-11-2017
            $Selchk.prop("checked", true);
            $('[id^=' + Parent + '_hdnBrokerIdForSameCmp]').val(BrokerId);
        } else {
            $Selchk.prop("checked", false);
            $('[id^=' + Parent + '_hdnBrokerIdForSameCmp]').val(0); //to store BroKerId Value zero in hidden field when checkbox is checked & then unchecked by sudhakar 20-11-2017
        }
        return false;
    }

    function ValidateForDuplicateBrokerEntry(NewBrokerId, Parent) {
        debugger;
        var tableRef = $('[id^=' + Parent + '_hdnBrokerType]').val();
        var tbName = Parent + "_" + tableRef;
        var table = document.getElementById(tbName);
        var rowCount = table.rows.length;

        if (rowCount > 1) {
            var cnt;
            for (cnt = 1; cnt < rowCount; cnt++) {
                var BrokerId = table.rows[cnt].cells[0].children[0].value;
                if (NewBrokerId == BrokerId) {
                    alert("Duplicate Broker Entry Found For Same Broker Select Other Broker Or Add New.");
                    return false;
                }
            }
        }
        return true;
    }


    function SelectBroker(Parent) {
        debugger;
        var BrokerId = $('[id^=' + Parent + '_hdnBrokerIdForSameCmp]').val();
        var Company = $.trim($('[id^=' + Parent + '_txtCompany]').val());
        var ucid = $('[id^=ContentMain_CommCalculator_hdnUcName]').val();

        if (ValidateForDuplicateBrokerEntry(BrokerId, Parent) == false) {
            return false;
        }

        CloseGridPopup(Parent);
        $('[id^=' + ucid + '_hdnBrokerId]').val(BrokerId);
        FillBrokerInfoOnAutoComplete(Company, ucid, BrokerId);


        var tb = Parent + "_tbBrokerFromSameCmp";
        var table = document.getElementById(tb);
        var rowCount = table.rows.length;

        if (rowCount > 1) {
            //var i = 1;
            //for (i = 0; i <= rowCount + 1; i++) {
            //    $("#" + tb + " tr:eq(" + i + ")").remove();
            //}
            $("#" + tb).empty();
        }
    }

    function SetCoBrokerDetails(Parent) {
        debugger;

        BrokerId = $("option:selected", $("#" + Parent + "_ddlBrokers")).val();
        if (BrokerId == 0) {
            $('[id^=' + Parent + '_txtName]').attr("disabled", false);
        }
        else {
            $('[id^=' + Parent + '_txtName]').attr("disabled", true);
        }
        if (ValidateForDuplicateBrokerEntry(BrokerId, Parent) == false) {
            return false;
        }
        else {
            var Company = $('[id^=ContentMain_CommCalculator_txtCompany]').val();
            var ucid = $('[id^=ContentMain_CommCalculator_hdnUcName]').val();
            $('[id^=' + ucid + '_hdnBrokerId]').val(BrokerId);
            var Company1 = $('[id^=' + ucid + '_txtCompany]').val();
            Company1 = 'segall';
            FillBrokerInfoOnAutoComplete(Company1, ucid, BrokerId);

            return true;
        }
    }

    function AddNewBroker(Parent) {
     
        var ucid = $('[id^=ContentMain_CommCalculator_hdnUcName]').val();
        $('[id^=' + ucid + '_hdnBrokerId]').val(0);

        $('[id^=' + Parent + '_txtAddress]').val("");
        $('[id^=' + Parent + '_txtCity]').val("");
        $('[id^=' + Parent + '_ddlState]').val("");
        $('[id^=' + Parent + '_txtZipCode]').val("");
        $('[id^=' + Parent + '_txtName]').val("");
        $('[id^=' + Parent + '_txtEmail]').val("");
        $('[id^=' + Parent + '_txtEin]').val("");
        $('[id^=' + Parent + '_txtShare]').val("");
        $('[id^=' + Parent + '_txtSetAmount]').val("");
        CloseGridPopup(Parent);
    }

    function DeleteRecord(RowIndex, tableRef, Parent, DealBrokerCommissionId, brokerID) {
        debugger;
        var tbName = Parent + "_" + tableRef;
        var table = document.getElementById(tbName);
        var iCnt;
        var RowIndex;

        for (iCnt = 1; iCnt < table.rows.length; iCnt++) {
            var tbBrokerId = table.rows[iCnt].cells[0].children[0].value;
            var tbDealBrokerCommissionId = table.rows[iCnt].cells[0].children[1].value;

            if (tbBrokerId == brokerID && DealBrokerCommissionId == tbDealBrokerCommissionId) {
                RowIndex = iCnt;
            }
        }
        //alert(DealBrokerCommissionId+ ' '+brokerID);
       
        // alert(table.rows[RowIndex].cells[0].textContent);
        //  alert(table.rows[RowIndex].cells[0].text());


        var tbTotPer = Parent + "_" + tableRef + "_PerTotal";
        var bname = table.rows[RowIndex].cells[0].textContent;
        // var BrokerId = $('[id^=' + ucid + '_hdnBrokerId]').val(); children[0]
        $("#" + tbName + " tr:eq(" + RowIndex + ")").remove();
        $("#" + tbTotPer + " tr:eq(" + RowIndex + ")").remove();

        SetCommAmoutForEachBrokerType(tableRef, true, Parent);

        $.ajax({
            type: "POST",
            url: "NewChargeCommissionSpitCalCulator.aspx/DeleteBroker",
            data: '{BrokerId: "' + brokerID + '",DealBrokerCommissionId:"' + DealBrokerCommissionId + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            failure: function (response) {
                alert(response.d);
            }
        });
        
        //var bname = table.rows[RowIndex].cells[0].textContent;               
        var uid = bname.trim().replace(/[^A-Za-z0-9]+/gi, '-');
        //alert(uid);
        $('table[id$="tbCompanyCommsionSummary"] tr.' + uid + '').remove();
        return false;
    }

    // Validation For Correct Percentage Insertion

    function ValidateForPercentage(table, rowCount, SharePer) {
        
        var iCnt;
        var totGrpPer = 0;
        
        // var totCommissionAmt=0;
        for (iCnt = 1; iCnt <= rowCount - 1; iCnt++) {
            var HiddenSharePerValue = $("#" + $(table).prop("id") + " #hdnSharePer_" + iCnt).val();
            
            //totGrpPer = parseFloat(totGrpPer) + parseFloat(table.rows[iCnt].cells[1].innerHTML);
            totGrpPer = parseFloat(totGrpPer) + parseFloat(HiddenSharePerValue); //fetch here % value in more than 2 decimal
            
            //   totCommissionAmt = parseFloat(totCommissionAmt) + parseFloat(table.rows[iCnt].cells[2].innerText);
        }
        var totPer = parseFloat(totGrpPer) + parseFloat(SharePer);
        
        if (parseFloat(totPer).toFixed(4) > 100) {
            //  alert(totPer);
            return false;
        }
        return true;
    }

    function ValidateForPercentageOnEdit(table, rowCount, SharePer, BrokerId, EditIndex) {
        
        var iCnt;
        var totGrpPer = 0;
        // var totCommissionAmt=0;
        for (iCnt = 1; iCnt <= rowCount - 1; iCnt++) {
            var HiddenSharePerValue = $("#" +$(table).prop("id") + " #hdnSharePer_" + iCnt).val();
            if (parseInt(table.rows[EditIndex].cells[0].children[0].value) != parseInt(table.rows[iCnt].cells[0].children[0].value)) {

                //totGrpPer = parseFloat(totGrpPer) + parseFloat(table.rows[iCnt].cells[1].innerHTML);
                totGrpPer = parseFloat(totGrpPer) + parseFloat(HiddenSharePerValue);
            }
            //   totCommissionAmt = parseFloat(totCommissionAmt) + parseFloat(table.rows[iCnt].cells[2].innerText);
        }
        var totPer = parseFloat(totGrpPer) + parseFloat(SharePer);
        if (parseFloat(totPer).toFixed(4) > 100) {
            //  alert(totPer);
            return false;
        }
        return true;
    }

    function ShowIcon(sender, e) {
        sender._completionListElement.style.zIndex = 10000001;
        sender._element.className = "loading";
    }

    function hideIcon(sender, e) {
        sender._element.className = "textwidth";
    }

    function SetContextKey(parent) {
        
        var popEx = parent + "_acBrokerInfo";
        var Broker = $('[id^=' + parent + '_hdnBrokerType]').val();
        $find(popEx).set_contextKey(Broker);
        return;
    }

    function OnBrokerSelected(source, eventArgs) {
        debugger;
        var ucid = $('[id^=ContentMain_CommCalculator_hdnUcName]').val();
        if (eventArgs.get_value() == null) {
            $('[id^=' + ucid + '_hdnBrokerId]').val(0);
        }
        else {
            $('[id^=' + ucid + '_hdnBrokerId]').val(eventArgs.get_value());
        }

        FillBrokerInfoOnAutoComplete($.trim($('[id^=' + ucid + '_txtCompany]').val()), ucid, 0);
        // FillBrokerInfoOnAutoComplete($('[id^=' + ucid + '_hdnBrokerId]').val(), ucid);
        return false;
    }

    function CheckForDupicateEntry(Parent) {
        
        var brokerId, DealBrokerCommissionId;
        var txtCompany = $.trim($('[id^=' + Parent + '_txtCompany]').val());
        var txtAddress = $.trim($('[id^=' + Parent + '_txtAddress]').val());
        var txtCity = $.trim($('[id^=' + Parent + '_txtCity]').val());
        var ddlState = $.trim($('[id^=' + Parent + '_ddlState]').val());
        var txtZipCode = $.trim($('[id^=' + Parent + '_txtZipCode]').val());
        var txtName = $.trim($('[id^=' + Parent + '_txtName]').val());
        var txtEmail = $.trim($('[id^=' + Parent + '_txtEmail]').val());
        var txtEin = $.trim($('[id^=' + Parent + '_txtEin]').val());
        //var txtShare = $.trim($('[id^=' + Parent + '_txtShare]').val());
        var txtShare = $.trim($('[id^=' + Parent + '_hdnSharePer]').val());
        var txtSetAmount = $.trim($('[id^=' + Parent + '_txtSetAmount]').val());
        var hdnBrokerId = $.trim($('[id^=' + Parent + '_hdnBrokerId]').val());
        var hdnBrokerType = $.trim($('[id^=' + Parent + '_hdnBrokerType]').val());
        var RowCnt;
        $.ajax({
            type: "POST",
            url: "NewChargeCommissionSpitCalCulator.aspx/CheckDuplicateEntryForSelBroker",
            data: '{txtCompany: "' + txtCompany + '",txtAddress:"' + txtAddress + '",txtCity:"' + txtCity + '",ddlState:"' + ddlState + '",txtZipCode:"' + txtZipCode + '",txtName:"' + txtName + '",txtEmail:"' + txtEmail + '",txtEin:"' + txtEin + '",txtShare:"' + txtShare + '",txtSetAmount:"' + txtSetAmount + '",hdnBrokerId:"' + hdnBrokerId + '",hdnBrokerType:"' + hdnBrokerType + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                
                var xmlDoc = $.parseXML(response.d);
                var xml = $(xmlDoc);
                var tbRow = xml.find("Table");
                if (tbRow.length > 0) {
                    var Name, Address, Email, EIN, SharePer, SetAmount, CompName, BrokerId;
                    $.each(tbRow, function () {
                        $('[id^=' + Parent + '_hdnIdDuplicate]').val(0);
                        Name = $.trim($(this).find("Name").text());
                        Address = $.trim($(this).find("Address").text());
                        Address = $.trim($(this).find("Address").text());
                        Email = $.trim($(this).find("Email").text());
                        EIN = $.trim($(this).find("EIN").text());
                        SharePer = $.trim($(this).find("SharePercent").text());
                        SetAmount = $.trim($(this).find("SetAmount").text());
                        CompName = $.trim($(this).find("CompanyName").text());
                        BrokerId = $.trim($(this).find("BrokerId").text());
                       
                        AddDuplicateBrokers(Parent, BrokerId, Name, Address, SharePer, SetAmount, Email, EIN);
                    });
                    ShowDupBrokerPopup(Parent);
                    return false;
                }
                else {
                    SaveBroker(Parent);

                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    function addexternal(name, per, tot, Parent) {
        
        var uc = Parent.replace("ContentMain_", "");
        if (uc == 'CommCalculator') {
            var DestTable = document.getElementById("ContentMain_ucCommSummary_tbBrokersummary");
            //var Totaltb = document.getElementById("ContentMain_ucCommSummary_tbtotalbroker");
        }
        else if (uc == 'OptCommCalculator') {
            var DestTable = document.getElementById("ContentMain_ucCommSummary_tbOptionsummarry");
            //var Totaltb = document.getElementById("ContentMain_ucCommSummary_tbtotalopt");
        }
        else if (uc == 'ContingentCommCalculator') {
            var DestTable = document.getElementById("ContentMain_ucCommSummary_tbcontingent");
        }
        else if (uc == 'Opt1CommCalculator') {
            var DestTable = document.getElementById("ContentMain_ucCommSummary_tbOptionsummarry1");            
        }
        else if (uc == 'Opt2CommCalculator') {
            var DestTable = document.getElementById("ContentMain_ucCommSummary_tbOptionsummarry2");            
        }
        else if (uc == 'Opt3CommCalculator') {
            var DestTable = document.getElementById("ContentMain_ucCommSummary_tbOptionsummarry3");            
        }
        var index = name.indexOf('<');
        var brokername = name.substring(0, index);
        //DestTable = document.getElementById("ContentMain_ucCommSummary_tbBrokersummary");
        //for (var i = DestTable.rows.length; i > 0; i--) {
        //    DestTable.deleteRow(i - 1);
        //}
        // var cellCount = DestTable.rows[0].cells.length;

        //if (document.getElementById(Parent + '_tblExternalBroker_PerTotal') != null) {
        //    var pertable = document.getElementById(Parent + '_tblExternalBroker_PerTotal');
        //    var rowcnt = pertable.rows.length;
        //    for (iCnt = 1; iCnt <= rowcnt - 1; iCnt++) {
        //        (pertable.rows[iCnt].cells[0].innerHTML);
        //    }
        //}

        var rowno = DestTable.rows.length;
        var row = DestTable.insertRow(rowno);
        var cell_1 = row.insertCell(0);
        var element_1 = document.createElement("Lable");
        element_1.type = "lable";
        element_1.name = 'lbtot_' + 1;
        element_1.innerHTML = name;
        //element_1.style.width = "30%";
        //element_1.style.height = "18px";
        cell_1.style.paddingLeft = "5px";
        cell_1.style.width = "30%";
        cell_1.style.textAlign = "left";

        cell_1.appendChild(element_1);



        var cell_2 = row.insertCell(1);
        var element_2 = document.createElement("Lable");
        element_2.type = "lable";
        element_2.style.width = "10%";
        // element_4.style.height = "18px";
        element_2.innerHTML = '--';
        //cell_4.style.textAlign = "right";
        //cell_4.style.paddingRight = "15px";
        //cell_2.style.paddingLeft = "15px";
        //cell_4.style.width = "40px";
        cell_2.appendChild(element_2);


        var cell_3 = row.insertCell(2);
        var element_3 = document.createElement("Lable");
        element_3.type = "lable";
        element_3.style.width = "25%";
        //element_3.style.height = "18px";
        if (per < 10)
            element_3.innerHTML = parseFloat(per).toFixed(2) + '%';
        else
            element_3.innerHTML = parseFloat(per).toFixed(2) + '%';
        cell_3.style.textAlign = "right";

        //cell_2.style.paddingRight = "25px";
        //cell_2.style.paddingLeft = "15px";
        //cell_3.style.width = "40px";
        cell_3.appendChild(element_3);



        var cell_4 = row.insertCell(3);
        var element_4 = document.createElement("Lable");
        element_4.type = "lable";
        element_4.style.width = "10%";
        // element_4.style.height = "18px";
        element_4.innerHTML = '--';
        //cell_4.style.textAlign = "right";
        //cell_4.style.paddingRight = "15px";
        //cell_2.style.paddingLeft = "15px";
        //cell_4.style.width = "40px";
        cell_4.appendChild(element_4);




        var cell_5 = row.insertCell(4);
        var element_5 = document.createElement("Lable");
        element_5.type = "lable";
        //element_5.style.width = "40px";
        //element_5.style.height = "18px";
        element_5.innerHTML = FormatCurrency(parseFloat(tot), '$');
        cell_5.style.textAlign = "right";
        cell_5.style.paddingRight = "5px";  //MAy have to add else where 
        cell_5.style.width = "25%";
        cell_5.appendChild(element_5);

        var tableRef = "tblCo_Broker";
        var table = document.getElementById(Parent + "_" + tableRef);
        var lbUser = '<%=Session["UserName"]%>';
        var loggedintype = '<%=Session["UserType"]%>';
        //var Tab = $("#content").tabs('option', 'active');
        // alert(Tab);
        
        //if(brokername.includes('(CLB-Broker)')) //includes function not works in ie browser so we used indexOf function
        if (brokername.indexOf('(CLB-Broker)')>=0)
        {
            lbUser = lbUser + ' (CLB-Broker)'
        }        
        debugger;
        if ((lbUser == brokername || loggedintype == "1") && uc == 'CommCalculator') {
            //
            //if (uc == 'CommCalculator' && Tab == 0) {
            //    UpdateCompSplitSummary(tot);
            //}
            //else if (uc == 'OptCommCalculator' && Tab == 1) {
            //    UpdateCompSplitSummary(tot);
            //}
            //else if (uc == 'ContingentCommCalculator' && Tab == 2) {
            //    UpdateCompSplitSummary(tot);
            //}
            // alert('in add external' + brokername);
            UpdateCompSplitSummary(tot, brokername);
        }
               
    }
    function Addtosummary(Parent) {        
        var uc = Parent.replace("ContentMain_", "");
        //var DetTable = document.getElementById(TableStr);

        if (uc == 'CommCalculator') {
            $('ContentMain_ucCommSummary_tbtotalbroker').empty();
            var SummTable = document.getElementById("ContentMain_ucCommSummary_tbtotalbroker");
            for (var i = SummTable.rows.length; i > 0; i--) {
                SummTable.deleteRow(i - 1);
            }

        }
        else if (uc == 'OptCommCalculator') {
            $('ContentMain_ucCommSummary__tbtotalopt').empty();
            var SummTable = document.getElementById("ContentMain_ucCommSummary_tbtotalopt");
            for (var i = SummTable.rows.length; i > 0; i--) {
                SummTable.deleteRow(i - 1);
            }

        }
        else if (uc == 'ContingentCommCalculator') {
            var SummTable = document.getElementById("ContentMain_ucCommSummary_tbtotalcontingent");
            for (var i = SummTable.rows.length; i > 0; i--) {
                SummTable.deleteRow(i - 1);
            }
        }
        else if (uc == 'Opt1CommCalculator') {
            $('ContentMain_ucCommSummary__tbtotalopt1').empty();
            var SummTable = document.getElementById("ContentMain_ucCommSummary_tbtotalopt1");
            for (var i = SummTable.rows.length; i > 0; i--) {
                SummTable.deleteRow(i - 1);
            }

        }
        else if (uc == 'Opt2CommCalculator') {
            $('ContentMain_ucCommSummary__tbtotalopt2').empty();
            var SummTable = document.getElementById("ContentMain_ucCommSummary_tbtotalopt2");
            for (var i = SummTable.rows.length; i > 0; i--) {
                SummTable.deleteRow(i - 1);
            }

        }
        else if (uc == 'Opt3CommCalculator') {
            $('ContentMain_ucCommSummary__tbtotalopt3').empty();
            var SummTable = document.getElementById("ContentMain_ucCommSummary_tbtotalopt3");
            for (var i = SummTable.rows.length; i > 0; i--) {
                SummTable.deleteRow(i - 1);
            }

        }



        var brokerpertotal = $("#" + Parent + "_td_tblBrokersPerTot").text();
        var brokertotal = $("#" + Parent + "_td_tblBrokersAmtTot").text();
        //
        //if (document.getElementById(Parent+'_tblExternalBroker_PerTotal') != null)
        //{
        //    var pertable = document.getElementById(Parent + '_tblExternalBroker_PerTotal');
        //    var rowcnt = pertable.rows.length
        //    alert(pertable.rows[1].cells[0].innerHTML);
        //    }

        //$("#totalpercent").html(brokerpertotal);
        //$("#total").html('$' + brokertotal);
        //var Extot = ( $('[id^=' + Parent + '_hdnTotalComm]').val());
        //var Mastot = ( $('[id^=' + Parent + '_hdnCommForMasterBroker]').val());
        //var Reftot = ($('[id^=' + Parent + '_hdnCommForRefferalBroker]').val());
        //var Cotot = ( $('[id^=' + Parent + '_hdnCommForCoBroker]').val());

        //var rowno = SummTable.rows.length;



        var row = SummTable.insertRow(0);
        var cell_1 = row.insertCell(0);
        var element_1 = document.createElement("Lable");
        element_1.type = "lable";
        element_1.name = 'lbtot_' + 1;
        element_1.innerHTML = "";
        //element_1.style.width = "30%";
        //element_1.style.height = "18px";
        cell_1.style.paddingLeft = "5px";
        cell_1.style.width = "30%";
        cell_1.style.textAlign = "left";

        cell_1.appendChild(element_1);



        var cell_2 = row.insertCell(1);
        var element_2 = document.createElement("Lable");
        element_2.type = "lable";
        element_2.style.width = "10%";
        // element_4.style.height = "18px";
        element_2.innerHTML = '';
        //cell_4.style.textAlign = "right";
        //cell_4.style.paddingRight = "15px";
        //cell_2.style.paddingLeft = "15px";
        //cell_4.style.width = "40px";
        cell_2.appendChild(element_2);


        var cell_3 = row.insertCell(2);
        var element_3 = document.createElement("Lable");
        element_3.type = "lable";
        element_3.style.width = "25%";
        element_1.name = 'lbtot_' + 1;
        element_3.innerHTML = parseFloat(brokerpertotal).toFixed(2) + '%';
        cell_3.style.textAlign = "right";

        //cell_2.style.paddingRight = "25px";
        //cell_2.style.paddingLeft = "15px";
        //cell_3.style.width = "40px";
        cell_3.appendChild(element_3);

        //OLD JAY
        //var row = SummTable.insertRow(0);
        //var cell_1 = row.insertCell(0);
        //var element_1 = document.createElement("Lable");
        //element_1.type = "lable";
        ////element_1.name = 'lbtot_' + 1;
        //cell_1.style.paddingLeft = "5";
        //brokerpertotal = parseFloat(brokerpertotal).toFixed(2);
        //element_1.innerHTML = brokerpertotal + '%';
        //cell_1.style.textAlign = "left";
        //cell_1.appendChild(element_1);




        var cell_4 = row.insertCell(3);
        var element_4 = document.createElement("Lable");
        element_4.type = "lable";
        element_4.style.width = "10%";
        // element_4.style.height = "18px";
        element_4.innerHTML = '--';
        //cell_4.style.textAlign = "right";
        //cell_4.style.paddingRight = "15px";
        //cell_2.style.paddingLeft = "15px";
        //cell_4.style.width = "40px";
        cell_4.appendChild(element_4);




        var cell_5 = row.insertCell(4);
        var element_5 = document.createElement("Lable");
        element_5.type = "lable";
        //element_5.style.width = "40px";
        //element_5.style.height = "18px";
        element_5.innerHTML = brokertotal;
        cell_5.style.textAlign = "right";
        cell_5.style.paddingRight = "5px";  //MAy have to add else where 
        cell_5.style.width = "25%";
        cell_5.appendChild(element_5);

        //OLD JAY
        //var cell_2 = row.insertCell(1);
        //var element_2 = document.createElement("Lable");
        //element_2.type = "lable";
        ////element_2.style.width = "70px";
        ////element_2.style.height = "18px";
        //element_2.innerHTML = '$' + parseFloat(brokertotal).toFixed(2);
        //cell_2.style.paddingRight = "15px";
        //cell_2.style.paddingLeft = "50px";
        //cell_2.style.textAlign = "right";
        //cell_2.appendChild(element_2);





        ////DestTable = $('#ContentMain_ucCommSummary_dvcommissionSummary_tbBrokersummary');
        //DestTable = document.getElementById("ContentMain_ucCommSummary_tbBrokersummary");
        //// var sourcetable = $("#" + Parent + "_tblExternalBroker");
        //var sourcetable = document.getElementById("ContentMain_CommCalculator_maintb");
        //var Srow = sourcetable.rows.length - 1;
        // var totcommAmt = sourcetable.rows[0].cells[3].val();
        //var totcommAmt = sourcetable.rows[1].cells[4].val;
        //alert(Parent);
        // var data = $("#" + Parent + "_maintb_td_tblBrokersPerTot").val(); 

        // var amt = document.getElementById('ContentMain_CommCalculator_td_tblExternalBroker_PerTotal').innerText;
        //// var amt = $("#ContentMain_CommCalculator_td_tblExternalBroker_AmtTotal").val();
        //// var totcommAmt = $('[id^=' + Parent + '_maintb_td_tblExternalBroker_AmtTotal]').innerText;

        //totcommAmt =SourceTable.rows[2].cells[3].firstChild.value;
        //var rowcount = Srow - 1;
        //var cellCount = DestTable.rows[0].cells.length;
        //var rowno = 0;
        //
        //if (brokerpertotal != "") {
        //    var row = DestTable.insertRow(rowno);
        //    var cell_1 = row.insertCell(0);
        //    var element_1 = document.createElement("Lable");
        //    element_1.type = "lable";
        //    
        //    element_1.name = 'lbtot_' + 1;
        //    element_1.innerHTML = 'external broker .... $' + Extot;
        //    cell_1.appendChild(element_1);
        //}
        //if (Cotot != "") {
        //    var row = DestTable.insertRow(rowno);
        //    var cell_1 = row.insertCell(0);
        //    var element_1 = document.createElement("Lable");
        //    element_1.type = "lable";
        //    
        //    element_1.name = 'lbtot_' + 1;
        //    element_1.innerHTML = 'Co-broker .... $' + Cotot;
        //    cell_1.appendChild(element_1);
        //}

    }
    function SaveBroker(Parent) {
        
        var brokerId, DealBrokerCommissionId;
        var txtCompany = $.trim($('[id^=' + Parent + '_txtCompany]').val());
        var txtAddress = $.trim($('[id^=' + Parent + '_txtAddress]').val());
        var txtCity = $.trim($('[id^=' + Parent + '_txtCity]').val());
        var ddlState = $.trim($('[id^=' + Parent + '_ddlState]').val());
        var txtZipCode = $.trim($('[id^=' + Parent + '_txtZipCode]').val());
        var txtName = $.trim($('[id^=' + Parent + '_txtName]').val());
        var txtEmail = $.trim($('[id^=' + Parent + '_txtEmail]').val());
        var txtEin = $.trim($('[id^=' + Parent + '_txtEin]').val());
        var txtShare = $.trim($('[id^=' + Parent + '_txtShare]').val());
        var txtSetAmount = $.trim($('[id^=' + Parent + '_txtSetAmount]').val());
        var hdnBrokerId = $.trim($('[id^=' + Parent + '_hdnBrokerId]').val());
        var hdnBrokerType = $.trim($('[id^=' + Parent + '_hdnBrokerType]').val());

        var sharePercentage = $.trim($('[id^=' + Parent + '_txtShare]').val());
        //New Changes
        var ActSharePer = $.trim($('[id^=' + Parent + '_hdnSharePer]').val());

        var tb = Parent + "_" + hdnBrokerType;
        var table = document.getElementById(tb);
        var rowCount = table.rows.length;
        if (ValidateForPercentage(table, rowCount, ActSharePer) == false) {     //replace parameter from sharePercentage to ActSharePer
            alert("Total Broker Percentage Cannot Be Greater Than 100%");
            $('[id^=' + Parent + '_txtShare]').focus();
            return false;
        }
      
        $.ajax({
            type: "POST",
            url: "NewChargeCommissionSpitCalCulator.aspx/SaveBroker",
            data: '{txtCompany: "' + txtCompany + '",txtAddress:"' + txtAddress + '",txtCity:"' + txtCity + '",ddlState:"' + ddlState + '",txtZipCode:"' + txtZipCode + '",txtName:"' + txtName + '",txtEmail:"' + txtEmail + '",txtEin:"' + txtEin + '",txtShare:"' + ActSharePer + '",txtSetAmount:"' + txtSetAmount + '",hdnBrokerId:"' + hdnBrokerId + '",hdnBrokerType:"' + hdnBrokerType + '",TermType:"' + Parent + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                debugger;
                brokerId = data.d[0];
                DealBrokerCommissionId = data.d[1];

                var brokers;
                if (hdnBrokerType == "tblCo_Broker" || hdnBrokerType=="tblClb_Broker") {

                    var CoBroker = $("#" + Parent + "_ddlBrokers option:selected").text();
                    if (CoBroker != "" && CoBroker != "Select(Add New Co-broker)") {
                        brokers = CoBroker;
                    }
                    else {
                        brokers = $('[id^=' + Parent + '_txtName]').val();
                    }
                }
                else {
                    brokers = $('[id^=' + Parent + '_txtCompany]').val();
                }

                //   var sharePercentage = $('[id^=' + Parent + '_txtShare]').val()l;
                var commisionAmount = $('[id^=' + Parent + '_txtSetAmount]').val();
                //  var hdnBrokerType = $('[id^=' + Parent + '_hdnBrokerType]').val();sharePercentage
                var loggedintype = '<%=Session["UserType"]%>';
                $('[id^=' + Parent + '_hdnBrokerId]').val(brokerId);
                
                if (appendExternalBrokerFromPopup(brokers, ActSharePer, commisionAmount, hdnBrokerType, brokerId, Parent, DealBrokerCommissionId) == true) {
                    debugger;
                    $("#" + Parent + "_dvAddBroker").dialog("close");
                    if ((hdnBrokerType == "tblCo_Broker" || hdnBrokerType=="tblClb_Broker") && loggedintype == "1") {
                        var Percentbroker = $.trim($('[id^=' + Parent + '_hdnBrokerPercent]').val());
                        var percentComp = 100 - Percentbroker;
                        var UserComm = (parseFloat(Percentbroker) / 100) * parseFloat(commisionAmount);
                        var CompanyComm = (parseFloat(50) / 100) * parseFloat(commisionAmount);
                        //alert("2");

                        var uc = Parent.replace("ContentMain_", "");   //Only show base term Projected Company Split Summary so loop added 21-11-2017
                        if (uc == 'CommCalculator') {   
                            GenerateCompSplitSummary(Percentbroker, percentComp, brokers, txtCompany, UserComm, CompanyComm, hdnBrokerType);
                        }
                    }
                    //Display Logged In Broker Company Split Summary
                    debugger;
                    var lbUser = '<%=Session["UserName"]%>';
                    if (lbUser == brokers && (hdnBrokerType == "tblCo_Broker" || hdnBrokerType == "tblClb_Broker"))
                    {
                        var Percentbroker = $.trim($('[id^=' + Parent + '_hdnBrokerPercent]').val());
                        var percentComp = 100 - Percentbroker;
                        var UserComm = (parseFloat(Percentbroker) / 100) * parseFloat(commisionAmount);
                        var CompanyComm = (parseFloat(50) / 100) * parseFloat(commisionAmount);
                        //alert("2");

                        var uc = Parent.replace("ContentMain_", "");   //Only show base term Projected Company Split Summary so loop added 21-11-2017
                        if (uc == 'CommCalculator') {
                            GenerateCompSplitSummary(Percentbroker, percentComp, brokers, txtCompany, UserComm, CompanyComm, hdnBrokerType);
                        }
                    }

                    clearAddExternalBroker(Parent);
                    ClearBrokerDropDown(Parent);
                    ClearCompany(Parent);
                }
                else {
                    return false;
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
        //var id = $('[id^=CommCalculator_hdnBrokerId]').val();
    }

    function UpadteBrokerAmountDetails(Parent) {
        debugger;
        var hdnBrokerId = parseInt($('[id^=' + Parent + '_hdnBrokerId]').val());
        var CommPer = parseFloat($('[id^=' + Parent + '_txtEditSharePer]').val());
        var CommAmt = parseFloat($('[id^=' + Parent + '_txtEditShareAmt]').val());
        var DealBrokerCommissionId = $('[id^=' + Parent + '_hdnDealBrokerCommissionId]').val();


        // new Change
        var ActCommPer = $('[id^=' + Parent + '_hdnSharePer]').val();

        $.ajax({
            type: "POST",
            url: "NewChargeCommissionSpitCalCulator.aspx/UpdateBrokerAmount",
            data: '{BrokerId: "' + hdnBrokerId + '",CommPer:"' + ActCommPer + '",CommAmt:"' + CommAmt + '",DealBrokerCommissionId:"' + DealBrokerCommissionId + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                // GenerateCompSplitSummary(data.d[0], data.d[1], data.d[2], data.d[3], data.d[4], data.d[5]);
                //  data: '{BrokerId: "' + hdnBrokerId + '",CommPer:"' + CommPer + '",CommAmt:"' + CommAmt + '",DealBrokerCommissionId:"' + DealBrokerCommissionId + '"}',
            },
            failure: function (response) {
                alert(response.d);
            }
        });

    }

    function GenerateCompSplitSummary(ThresholdPer1, ThresholdPer2, UserName, CompanyName, UserComm, CompanyComm, tblBrokerType) {
        debugger;
        //alert('1');
      

        //var lbUser = $("#lbUser").text();
        // var lbUser1 = '<%=Session["UserName"]%>';
        // if (lbUser1 == lbUser) {
        //alert(ThresholdPer1 + ThresholdPer2 + UserName + CompanyName + UserComm + CompanyComm);
        var Total = parseFloat(UserComm) + parseFloat(CompanyComm);
        var Threshold = parseFloat(ThresholdPer1).toFixed(2) + " : " + parseFloat(ThresholdPer2).toFixed(2);        
        //$("#td_Threashold").text(Threshold);
        //$("#td_UserName").text(UserName + ":");
        //$("#td_UserComm").text("$" + UserComm);
        //$("#td_CompName").text(CompanyName + ":");
        //$("#td_CompComm").text("$" + CompanyComm);
        //$("#td_total").text("$" + parseFloat(Total).toFixed(2));
        //    $('#ContentMain_ucCompanySumm_tbCommsionSummary tbody>tr:last #td1').text("Your Data");
        //    var tablesum = $("#tbCommsionSummary");
        //    var tabelcomp = document.getElementById("ContentMain_ucCompanySumm_tbCommsionSummary");
        var cnt = $('table[id$="tbCompanyCommsionSummary"] tr').length;
        if (tblBrokerType == "tblClb_Broker") {
            UserName = UserName + " (CLB-Broker)";
        }
        var uid = UserName.trim().replace(/[^A-Za-z0-9]+/gi, '-');
        
        $('table[id$="tbCompanyCommsionSummary"] tr:last').after("<tr class='" + uid + "'><td class='td1' >Threshold:</td><td colspan='2' class='td_threshold" + uid + "'>" + ThresholdPer1 + " : " + ThresholdPer2 + "</td></tr>");
        $('table[id$="tbCompanyCommsionSummary"] tr:last').after("<tr class='" + uid + "'><td colspan=3></td></tr>");
        $('table[id$="tbCompanyCommsionSummary"] tr:last').after("<tr  class='" + uid + "'><td class='td1'>" + UserName + "</td><td class='td2'></td><td class='td_UserComm" + uid + "' id='td_UserComm'>$" + parseFloat(UserComm).toFixed(2) + "</td></tr>");
        $('table[id$="tbCompanyCommsionSummary"] tr:last').after("<tr  class='" + uid + "'><td class='td1'>" + CompanyName + "</td><td class='td2'></td><td class='td_CompComm" + uid + "' id='td_CompComm'>$" + parseFloat(CompanyComm).toFixed(2) + "</td></tr>");
        $('table[id$="tbCompanyCommsionSummary"] tr:last').after("<tr  class='" + uid + "'><td colspan='3' align='left'><hr style='width: 95%;' /></td>");
        $('table[id$="tbCompanyCommsionSummary"] tr:last').after("<tr  class='" + uid + "'><td class='td1'>Total: </td><td class='td2'></td><td class='td_total" + uid + "' id='td_total'>$" + parseFloat(Total).toFixed(2) + "</td></tr>");
        $('table[id$="tbCompanyCommsionSummary"] tr:last').after("<tr  class='" + uid + "'><td colspan='3' align='left'><hr style='width: 95%;' /></td>");

        // }
    }

    function UpdateCompSplitSummary(UserCommTot, brokername) {
        debugger;
        //alert('3');
        var uid = brokername.trim().replace(/[^A-Za-z0-9]+/gi, '-');
        var tdname = "td_threshold" + uid;
        var Comm = $('table[id$="tbCompanyCommsionSummary"] td.' + tdname + '').text();
        //  $('table[id$="tbCompanyCommsionSummary"] tr.' + uid + '').each(function () {
        //    var $cells = $(this).children();
        //    alert($cells.eq(1).text());
        //});
        // alert(Comm);
        //alert( $('table[id$="td_threshold"+brokername+"]').html());


        //var tdcomm = "td_threshold" + brokername;
        //var items = new Array();
        //$('table[id$="tbCompanyCommsionSummary"] tr.tr_UserComm').each(function () {
        //    var $cellsuser = $(this).children();
        //    if (brokername == $cellsuser.eq(0).text()) {
        //        $('table[id$="tbCompanyCommsionSummary"] tr.tr_threshold').each(function () {
        //            var $cellsthre = $(this).children();
        //            alert($cellsthre.eq(1).text());
        //        });
        //    }

        //});

        //$('table[id$="tbCompanyCommsionSummary"] tr.tr_total').each(function () {
        //    var $cells = $(this).children();
        //    alert($cells.eq(1).text());
        //});

        //var mytable = $('table[id$="tbCompanyCommsionSummary"]');

        //mytable.find("tr").each(function () {
        //    alert(mytable.find(td).text());
        //});

        
        if (Comm != "" && UserCommTot != '') {
            var index = Comm.indexOf(':');

            var UserPer = Comm.substring(0, index);
            var CompanyPer = Comm.substring(index + 1, Comm.length);

            var UserComm = (parseFloat(UserPer) / 100) * parseFloat(UserCommTot);
            var CompanyComm = (parseFloat(CompanyPer) / 100) * parseFloat(UserCommTot);

            var Total = parseFloat(UserComm) + parseFloat(CompanyComm);
            var usertd = "td_UserComm" + uid;
            var tdper = "td_CompComm" + uid;
            var tdtotal = "td_total" + uid;
            $('table[id$="tbCompanyCommsionSummary"] td.' + usertd + '').text("$" + parseFloat(UserComm).toFixed(2));
            $('table[id$="tbCompanyCommsionSummary"] td.' + tdper + '').text("$" + parseFloat(CompanyComm).toFixed(2));
            $('table[id$="tbCompanyCommsionSummary"] td.' + tdtotal + '').text("$" + parseFloat(Total).toFixed(2));

        }
    }

    function FillBrokerInfoOnAutoComplete(CmpName, Parent, Broker) {
     
        $.ajax({
            type: "POST",
            url: "NewChargeCommissionSpitCalCulator.aspx/FillBrokerInfo",
            data: '{CmpName:"' + CmpName + '",BrokerId:"' + Broker + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                debugger;
                var brokers = response.d;
                var tableRef = $('[id^=' + Parent + '_hdnBrokerType]').val();

                if (brokers.BrokerId > 0) {
                    var BrokerFullName = brokers.Name + ' ' + brokers.LastName;
                    //debugger;
                    $('[id^=' + Parent + '_txtCompany]').val(brokers.CompanyName);
                    $('[id^=' + Parent + '_txtAddress]').val(brokers.Address);
                    $('[id^=' + Parent + '_txtCity]').val(brokers.City);
                    $('[id^=' + Parent + '_ddlState]').val(brokers.State);
                    $('[id^=' + Parent + '_txtZipCode]').val(brokers.Zipcode);
                    //$('[id^=' + Parent + '_txtName]').val(brokers.Name);
                    $('[id^=' + Parent + '_txtName]').val(BrokerFullName);
                    $('[id^=' + Parent + '_txtEmail]').val(brokers.Email);
                    $('[id^=' + Parent + '_txtEin]').val(brokers.EIN);
                    $('[id^=' + Parent + '_hdnBrokerId]').val(brokers.BrokerId);
                    //In hdnBrokerPercent value only stores when UserDetails table contain record present for respected broker i.e. GETDATE() between FromDate and ToDate otherwise 0 will stored
                    $('[id^=' + Parent + '_hdnBrokerPercent]').val(brokers.PercentBroker);
                    //$('[id^=' + Parent + '_txtShare]').val(brokers.SharePercent);
                    //CalBrokerwiseCommAmount('ADD', Parent);

                    //$('[id^=CommCalculator_txtSetAmount]').val(brokers.SetAmount);

                    if (tableRef == "tblCo_Broker" || tableRef=="tblClb_Broker") {

                        //$('[id^=' + Parent + '_txtAddress]').attr("readonly", true);
                        //$('[id^=' + Parent + '_txtCity]').attr("readonly", true);
                        //$('[id^=' + Parent + '_ddlState]').attr("disabled", true);
                        //$('[id^=' + Parent + '_txtZipCode]').attr("readonly", true);                    
                        //$('[id^=' + Parent + '_txtName]').attr("readonly", true);
                        //$('[id^=' + Parent + '_txtEmail]').attr("readonly", true);
                        //$('[id^=' + Parent + '_txtEin]').attr("readonly", true);

                        //$('[id^=' + Parent + '_txtAddress]').attr("disabled", true);
                        //$('[id^=' + Parent + '_txtCity]').attr("disabled", true);
                        //$('[id^=' + Parent + '_ddlState]').attr("disabled", true);
                        //$('[id^=' + Parent + '_txtZipCode]').attr("disabled", true);
                        $('[id^=' + Parent + '_txtName]').attr("disabled", true);
                        //$('[id^=' + Parent + '_txtEmail]').attr("disabled", true);
                        //$('[id^=' + Parent + '_txtEin]').attr("disabled", true);
                    }
                    else {

                        //$('[id^=' + Parent + '_txtAddress]').attr("disabled", false);
                        //$('[id^=' + Parent + '_txtCity]').attr("disabled", false);
                        //$('[id^=' + Parent + '_ddlState]').attr("disabled", false);
                        //$('[id^=' + Parent + '_txtZipCode]').attr("disabled", false);
                        //$('[id^=' + Parent + '_txtName]').attr("disabled", false);
                        //$('[id^=' + Parent + '_txtEmail]').attr("disabled", false);
                        //$('[id^=' + Parent + '_txtEin]').attr("disabled", false);
                        //$('[id^=' + Parent + '_txtAddress]').removeAttr("disabled");
                        //$('[id^=' + Parent + '_txtCity]').removeAttr("disabled");
                        //$('[id^=' + Parent + '_ddlState]').attr("disabled", false);
                        //$('[id^=' + Parent + '_txtZipCode]').removeAttr("disabled");
                        $('[id^=' + Parent + '_txtName]').removeAttr("disabled");
                        //$('[id^=' + Parent + '_txtEmail]').removeAttr("disabled");
                        //$('[id^=' + Parent + '_txtEin]').removeAttr("disabled");
                    }
                }
                else {
                    //var tableRef = $('[id^=' + Parent + '_hdnBrokerType]').val();
                    if (tableRef == "tblCo_Broker" || tableRef=="tblClb_Broker") {
                        //alert(BrokerId);
                        //alert($('[id^=' + Parent + '_txtCompany]').val(brokers.CompanyName));
                        $('[id^=' + Parent + '_txtCompany]').val(brokers.CompanyName);
                        $('[id^=' + Parent + '_txtAddress]').val('');
                        $('[id^=' + Parent + '_txtCity]').val('');
                        $('[id^=' + Parent + '_ddlState]').val('');
                        $('[id^=' + Parent + '_txtZipCode]').val('');
                        //$('[id^=' + Parent + '_txtName]').val(brokers.Name);
                        $('[id^=' + Parent + '_txtName]').val('');
                        $('[id^=' + Parent + '_txtEmail]').val('');
                        $('[id^=' + Parent + '_txtEin]').val('');
                        //$('[id^=' + Parent + '_hdnBrokerId]').val('')
                        //$('[id^=' + Parent + '_txtCompany]').val('aa');
                        //GetCoBrokersOfSameCompany(CmpName, tableRef);
                    }
                    else {
                        var tableRef = $('[id^=' + Parent + '_hdnBrokerType]').val();
                        GetAllBrokersOfSameCompany(CmpName, tableRef);
                        
                        //debugger;
                        //var tb = Parent + "_tbBrokerFromSameCmp";
                        //var table = document.getElementById(tb);
                        //var rowCount = table.rows.length;

                        //if (rowCount > 0) {
                        //    ShowGridPopup(Parent);
                        //}
                    }
                }
                //$('[id^=CommCalculator_txtShare]').val(brokers.SharePercent);
                //$('[id^=CommCalculator_txtSetAmount]').val(brokers.SetAmount);
            },
            failure: function (msg) {
                alert(msg);
            }
        });
    }


    function GetAllBrokersOfSameCompany(CmpName, tableRef) {

        $.ajax({
            type: "POST",
            url: "NewChargeCommissionSpitCalCulator.aspx/FillBrokerInfoForSameCmp",
            data: '{CmpName: "' + CmpName + '",tableRef:"' + tableRef + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: OnSuccess,
            failure: function (msg) {
                alert(msg);
            }
        });
    }


    function OnSuccess(response) {
        debugger;
        var ucName = $('[id^=ContentMain_CommCalculator_hdnUcName]').val();
        var xmlDoc = $.parseXML(response.d);
        var xml = $(xmlDoc);
        var tbRow = xml.find("Table");
        var row = $("[id*=tbBrokerFromSameCmp] tr:last-child").clone(true);
        var brokerID, Name, Address, SharePercent, SetAmount, UserTypeId;
        $("[id*=tbBrokerFromSameCmp] tr").not($("[id*=tbBrokerFromSameCmp] tr:first-child")).remove();
        //Added only 1  line By Jaywanti on 27-11-13
        Cleartb(ucName, 'tbBrokerFromSameCmp');
        //
        $.each(tbRow, function () {
            //debugger;
            brokerID = $(this).find("BrokerId").text();
            Name = $(this).find("Name").text();
            Address = $(this).find("Address").text();
            SharePercent = $(this).find("SharePercent").text();
            SetAmount = $(this).find("SetAmount").text();
            UserTypeId = $(this).find("BrokerTypeId").text();

            AddSameCompanyBrokers(ucName, brokerID, Name, Address, SharePercent, SetAmount);


            //  $("td", row).eq(0).html($(this).find("BrokerId").text());
            //$("td", row).eq(0).html($(this).find("Name").text());
            //$("td", row).eq(1).html($(this).find("Address").text());
            //$("td", row).eq(2).html($(this).find("SharePercent").text());
            //$("td", row).eq(3).html($(this).find("SetAmount").text());
            //$("[id*=gvBrokerFromSameCmp]").append(row);
            //row = $("[id*=gvBrokerFromSameCmp] tr:last-child").clone(true);
        });
    };

    function GetCoBrokersOfSameCompany(CmpName, tableRef) {

        $.ajax({
            type: "POST",
            url: "NewChargeCommissionSpitCalCulator.aspx/FillCoBrokerInfoForSameCmp",
            data: '{CmpName: "' + CmpName + '",tableRef:"' + tableRef + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: CoBrokerOnSuccess,
            failure: function (msg) {
                alert(msg);
            }
        });
    }

    function CoBrokerOnSuccess(response) {        
        var Broker = 0; var CmpName = '';
        $.ajax({
            type: "POST",
            url: "NewChargeCommissionSpitCalCulator.aspx/FillBrokerInfo",
            data: '{CmpName:"' + CmpName + '",BrokerId:"' + Broker + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                debugger;
                var brokers = response.d;
                var tableRef = $('[id^=' + Parent + '_hdnBrokerType]').val();
                if (tableRef == "tblCo_Broker" || tableRef == "tblClb_Broker") {
                    //alert(BrokerId);
                    //alert($('[id^=' + Parent + '_txtCompany]').val(brokers.CompanyName));
                    $('[id^=' + Parent + '_txtCompany]').val(brokers.CompanyName);
                    $('[id^=' + Parent + '_txtAddress]').val('');
                    $('[id^=' + Parent + '_txtCity]').val('');
                    $('[id^=' + Parent + '_ddlState]').val('');
                    $('[id^=' + Parent + '_txtZipCode]').val('');
                    //$('[id^=' + Parent + '_txtName]').val(brokers.Name);
                    $('[id^=' + Parent + '_txtName]').val('');
                    $('[id^=' + Parent + '_txtEmail]').val('');
                    $('[id^=' + Parent + '_txtEin]').val('');
                    //$('[id^=' + Parent + '_hdnBrokerId]').val('')
                    //$('[id^=' + Parent + '_txtCompany]').val('aa');
                    //GetCoBrokersOfSameCompany(CmpName, tableRef);
                }
            },
            failure: function (msg) {
                alert(msg);
            }
        });
        debugger;
        var xmlDoc = $.parseXML(response.d);
        var xml = $(xmlDoc);
        var tbRow = xml.find("Table");
        var Parent = $('[id^=ContentMain_CommCalculator_hdnUcName]').val();
        var brokerID, Name, Address, SharePercent, SetAmount;
        $("#" + Parent + "_ddlBrokers").empty();
        clearAddExternalBroker(Parent);
        $("#" + Parent + "_ddlBrokers").append($("<option></option>").val(0).html("Select(Add New Co-broker)"));
        //$('[id^=' + Parent + '_txtCompany]').val('abc');
        $.each(tbRow, function () {
            var brokerID = $(this).find("BrokerId").text();
            var Name = $(this).find("Name").text();
            var BrokerTypeId = $(this).find("BrokerTypeId").text();
            if (BrokerTypeId == 4) {
                $("#" + Parent + "_ddlBrokers").append($("<option></option>").val(brokerID).html(Name));
            }
        });
    };

    function CalBrokerwiseCommPercentage(Mode, Parent) {
        debugger;
        var hdnBrokerType = $('[id^=' + Parent + '_hdnBrokerType]').val();
        var TotalCommission = SetCommAmoutForEachBrokerType(hdnBrokerType, false, Parent);
        debugger;
        var CommAmt;
        var CommPer;
        if (Mode == "ADD") {
            CommAmt = $('[id^=' + Parent + '_txtSetAmount]');
            CommPer = $('[id^=' + Parent + '_txtShare]');
        }
        else if (Mode == "EDIT") {
            CommAmt = $('[id^=' + Parent + '_txtEditShareAmt]');
            CommPer = $('[id^=' + Parent + '_txtEditSharePer]');
        }
        if (parseFloat(CommAmt.val()) > TotalCommission) {
            alert("Commission Amount Cannot Be Greater Than Total Commission");
            if (Mode == "ADD") {
                $('[id^=' + Parent + '_txtSetAmount]').focus();
            }
            else {
                $('[id^=' + Parent + '_txtEditShareAmt]').focus();
            }
            return false;
        }
        else if (CommAmt.val() != "") {
         
            var Per = (parseFloat(CommAmt.val()) * 100 / parseFloat(TotalCommission));
            //CommPer.val(roundToTwo(Per)); 
            CommPer.val(parseFloat(Per).toFixed(2));
            //Do Not Use Rounding For Basic Cal
            $('[id^=' + Parent + '_hdnSharePer]').val(parseFloat(Per));

            $('[id^=' + Parent + '_hdnShareAmt]').val(parseFloat(TotalCommission));
        }
    }

    function CalBrokerwiseCommAmount(Mode, Parent) {
        debugger;
        var hdnBrokerType = $('[id^=' + Parent + '_hdnBrokerType]').val();
        var TotalCommission = parseFloat(SetCommAmoutForEachBrokerType(hdnBrokerType, false, Parent));

        if (TotalCommission < 0) {
            TotalCommission = 0;
        }

        var CommAmt;
        var CommPer;
        if (Mode == "ADD") {
            CommAmt = $('[id^=' + Parent + '_txtSetAmount]');
            CommPer = $('[id^=' + Parent + '_txtShare]');
        }
        else if (Mode == "EDIT") {
            CommAmt = $('[id^=' + Parent + '_txtEditShareAmt]');
            CommPer = $('[id^=' + Parent + '_txtEditSharePer]');
        }

        if (parseFloat(CommPer.val()) > 100) {
            alert("Commission Percentage Cannot Be Greater Than 100");
            if (Mode == "ADD") {
                $('[id^=' + Parent + '_txtShare]').focus();
            }
            else {
                $('[id^=' + Parent + '_txtEditSharePer]').focus();
            }
            $('[id^=' + Parent + '_txtShare]').focus();
            return false;
        }
        else if (CommPer != "") {
            var Amt = (parseFloat(TotalCommission) * parseFloat(CommPer.val()) / 100);
            CommAmt.val(parseFloat(Amt).toFixed(2));//

            $('[id^=' + Parent + '_hdnSharePer]').val(parseFloat(CommPer.val()));
            $('[id^=' + Parent + '_hdnShareAmt]').val(parseFloat(Amt));
        }


    }

    function CalBrokerGroupTotPer(CommPer, tableRef, Parent) {
        debugger;
        var CommissionOn = $('[id^=' + Parent + '_hdnTotalComm]').val();

        //if (tableRef == "tblExternalBroker") {
        //    CommissionOn = $('[id^=' + Parent + '_hdnTotalComm]').val();
        //}
        //else if (tableRef == "tblMasterBroker") {
        //    CommissionOn = $('[id^=' + Parent + '_hdnCommForMasterBroker]').val();
        //}
        //else if (tableRef == "tblReferralBroker") {
        //    CommissionOn = $('[id^=' + Parent + '_hdnCommForRefferalBroker]').val();
        //}
        //else if (tableRef == "tblCo_Broker") {
        //    CommissionOn = $('[id^=' + Parent + '_hdnCommForCoBroker]').val();
        //}

        var totPer = (parseFloat(CommPer) * 100 / parseFloat(CommissionOn));
        $("#" + Parent + "_td_" + tableRef + "_PerTotal").text(parseFloat(totPer).toFixed(2) + "%");


        // alert(totPer);
        //td-tblExternalBroker-PerTotal
    }

    function CalBrokerTotPer(CommPer, tableRef, Parent, Row) {
        
        var CommissionOn = parseFloat($('[id^=' + Parent + '_hdnTotalComm]').val());
        var totCommPer = (parseFloat(CommPer) / CommissionOn) * 100;
        var Element = Parent + "_" + tableRef + "_PerTotal" + Row;
        $("#" + Element).text(parseFloat(totCommPer).toFixed(2) + "%");
        // alert(totCommPer); Parent + "_" +tableRef + "_PerTotal" + Row

        //return parseFloat(totCommPer).toFixed(2);
        return parseFloat(totCommPer);


    }

    function CalBrokerGroupTotalAmt(Value, tableRef, Parent) {

        var Result = FormatCurrency(parseFloat(Value), '$');
        $("#" + Parent + "_td_" + tableRef + "_AmtTotal").text(Result);
        //  CalBrokerGroupTotPer(Value, tableRef, Parent);
    }

    function ClearGropTotals(tableRef, Parent) {
        $("#" + Parent + "_td_" + tableRef + "_AmtTotal").text("");
        //  $("#" + Parent + "_td_" + tableRef + "_PerTotal").text("");
    }

    function SetCommAmoutForEachBrokerType(tableRef, Updation, Parent) {
        debugger;
        var rowCount;
        var totGrpAmt = 0;
        var table;
        var CommAmtBrokerWise = 0;
        var CommAmt = 0;
        var peroftotal = 0;
        var GrandTotAmt = 0;
        var Expertable = 0;;
        var ExPer = 0;
        var MasPer = 0;
        var RefPer = 0;
        var ClbPer = 0;
        var CoPer = 0;        
        var perTable;

        var tb = Parent + "_" + tableRef;
        var table = document.getElementById(tb);
        var ExtTb = Parent + '_tblExternalBroker';
        table = document.getElementById(ExtTb);
        //Expertable = ExtTb + '_PerTotal';
        //perTable = document.getElementById(Expertable);
        rowCount = table.rows.length;

        var uc = Parent.replace("ContentMain_", "");
        if (uc == 'CommCalculator') {
            $('#ContentMain_ucCommSummary_tbBrokersummary').empty();
        }
        else if (uc == 'OptCommCalculator') {
            $('#ContentMain_ucCommSummary_tbOptionsummarry').empty();
        }
        else if (uc == 'ContingentCommCalculator') {
            $('#ContentMain_ucCommSummary_tbcontingent').empty();
        }
        else if (uc == 'Opt1CommCalculator') {
            $('#ContentMain_ucCommSummary_tbOptionsummarry1').empty();
        }
        else if (uc == 'Opt2CommCalculator') {
            $('#ContentMain_ucCommSummary_tbOptionsummarry2').empty();
        }
        else if (uc == 'Opt3CommCalculator') {
            $('#ContentMain_ucCommSummary_tbOptionsummarry3').empty();
        }
        //$('#ContentMain_ucCommSummary_tbBrokersummary').empty();
        //$('#ContentMain_ucCommSummary_tbOptionsummarry').empty();
        //debugger;
        for (iCnt = 1; iCnt <= rowCount - 1; iCnt++) {
            //totGrpAmt = parseFloat(totGrpAmt) + parseFloat(table.rows[iCnt].cells[2].innerHTML);
            totGrpAmt = parseFloat(totGrpAmt) + parseFloat(table.rows[iCnt].cells[0].children[3].value);
            //For Fetching % of total
            if (document.getElementById(Parent + '_tblExternalBroker_PerTotal') != null) {
                var pertable = document.getElementById(Parent + '_tblExternalBroker_PerTotal');
            }
            addexternal(table.rows[iCnt].cells[0].innerHTML, parseFloat(pertable.rows[iCnt].cells[0].innerHTML.replace("$", "")), parseFloat(table.rows[iCnt].cells[2].innerHTML.replace("$", "")), Parent);
            ////test
            //debugger;
            ExPer = parseFloat(ExPer) + parseFloat(CalBrokerTotPer(parseFloat(table.rows[iCnt].cells[2].innerHTML.replace("$", "")), "tblExternalBroker", Parent, iCnt));

            // addexternal(table.rows[iCnt].cells[0].innerHTML, peroftotal, parseFloat(table.rows[iCnt].cells[2].innerHTML), Parent);
        }
        if (totGrpAmt > 0) {

            CalBrokerGroupTotalAmt(totGrpAmt, "tblExternalBroker", Parent);
            GrandTotAmt = GrandTotAmt + totGrpAmt;
            //for (iCnt = 1; iCnt <= rowCount - 1; iCnt++) {
            //    peroftotal = parseFloat(CalBrokerTotPer(parseFloat(table.rows[iCnt].cells[2].innerHTML), "tblExternalBroker", Parent, iCnt));
            //    addexternal(table.rows[iCnt].cells[0].innerHTML, peroftotal, parseFloat(table.rows[iCnt].cells[2].innerHTML), Parent);
            //}

        }
        else {
            ClearGropTotals("tblExternalBroker", Parent);
        }
        CommAmt = parseFloat($('[id^=' + Parent + '_hdnTotalComm]').val()) - totGrpAmt;

        $('[id^=' + Parent + '_hdnCommForMasterBroker]').val(CommAmt);

        //set totComm
        //$('[id^=' + Parent + '_hdnTotalComm]').val(totGrpAmt);
        totGrpAmt = 0;
        var MasTb = Parent + '_tblMasterBroker';
        table = document.getElementById(MasTb);
        rowCount = table.rows.length;
        for (iCnt = 1; iCnt <= rowCount - 1; iCnt++) {
            if (Updation == true) {
                var UpdatedAmt = UpdateAllBrokerTables(parseFloat($('[id^=' + Parent + '_hdnCommForMasterBroker]').val()), table, iCnt);

                table.rows[iCnt].cells[2].innerHTML = "$" + UpdatedAmt.toFixed(2);
                table.rows[iCnt].cells[0].children[3].value = UpdatedAmt;
                //test

                MasPer = parseFloat(MasPer) + parseFloat(CalBrokerTotPer(parseFloat(table.rows[iCnt].cells[2].innerHTML.replace("$", "")), "tblMasterBroker", Parent, iCnt));
            }

            // totGrpAmt = parseFloat(totGrpAmt) + parseFloat(table.rows[iCnt].cells[2].innerHTML);
            totGrpAmt = parseFloat(totGrpAmt) + parseFloat(table.rows[iCnt].cells[0].children[3].value);
            //For Fetching % of total

            if (document.getElementById(Parent + '_tblMasterBroker_PerTotal') != null) {
                var pertable = document.getElementById(Parent + '_tblMasterBroker_PerTotal');
                var rowcnt = pertable.rows.length;
                //for (iCnt = 1; iCnt <= rowcnt - 1; iCnt++) {
                //
                //alert(pertable.rows[iCnt].cells[0].innerHTML);
                //}
            }
            addexternal(table.rows[iCnt].cells[0].innerHTML, parseFloat(pertable.rows[iCnt].cells[0].innerHTML), parseFloat(table.rows[iCnt].cells[2].innerHTML.replace("$", "")), Parent);

        }
        if (totGrpAmt > 0) {
            CalBrokerGroupTotalAmt(totGrpAmt, "tblMasterBroker", Parent);
            GrandTotAmt = GrandTotAmt + totGrpAmt;
        }
        else {
            ClearGropTotals("tblMasterBroker", Parent);
        }
        CommAmt = (parseFloat($('[id^=' + Parent + '_hdnCommForMasterBroker]').val()) - totGrpAmt);


        $('[id^=' + Parent + '_hdnCommForRefferalBroker]').val(CommAmt);
        totGrpAmt = 0;

        var RefTb = Parent + '_tblReferralBroker';
        table = document.getElementById(RefTb);
        rowCount = table.rows.length;
        for (iCnt = 1; iCnt <= rowCount - 1; iCnt++) {
            if (Updation == true) {
                var UpdatedAmt = UpdateAllBrokerTables(parseFloat($('[id^=' + Parent + '_hdnCommForRefferalBroker]').val()), table, iCnt);
                table.rows[iCnt].cells[2].innerHTML = "$" + UpdatedAmt.toFixed(2);
                table.rows[iCnt].cells[0].children[3].value = UpdatedAmt;

                RefPer = parseFloat(RefPer) + parseFloat(CalBrokerTotPer(parseFloat(table.rows[iCnt].cells[2].innerHTML.replace("$", "")), "tblReferralBroker", Parent, iCnt));
            }
            //  totGrpAmt = parseFloat(totGrpAmt) + parseFloat(table.rows[iCnt].cells[2].innerHTML);
            totGrpAmt = parseFloat(totGrpAmt) + parseFloat(table.rows[iCnt].cells[0].children[3].value);
            //For Fetching % of total
            if (document.getElementById(Parent + '_tblReferralBroker_PerTotal') != null) {
                var pertable = document.getElementById(Parent + '_tblReferralBroker_PerTotal');
            }
            addexternal(table.rows[iCnt].cells[0].innerHTML, parseFloat(pertable.rows[iCnt].cells[0].innerHTML), parseFloat(table.rows[iCnt].cells[2].innerHTML.replace("$", "")), Parent);
        }
        if (totGrpAmt > 0) {
            CalBrokerGroupTotalAmt(totGrpAmt, "tblReferralBroker", Parent);
            GrandTotAmt = GrandTotAmt + totGrpAmt;
        }
        else {
            ClearGropTotals("tblReferralBroker", Parent);
        }
        CommAmt = (parseFloat($('[id^=' + Parent + '_hdnCommForRefferalBroker]').val()) - totGrpAmt);

        //Added CLB Broker Type Code 5-6-2018 Sudhakar
        $('[id^=' + Parent + '_hdnCommForClbBroker]').val(CommAmt);

        var ClbTb = Parent + '_tblClb_Broker';
        table = document.getElementById(ClbTb);
        rowCount = table.rows.length;
        totGrpAmt = 0;

        for (iCnt = 1; iCnt <= rowCount - 1;iCnt++){
            if (Updation == true) {
                var UpdatedAmt = UpdateAllBrokerTables(parseFloat($('[id^=' + Parent + '_hdnCommForClbBroker').val()), table, iCnt);
                table.rows[iCnt].cells[2].innerHTML = "$" + UpdatedAmt.toFixed(2);
                table.rows[iCnt].cells[0].children[3].value = UpdatedAmt;

                ClbPer = parseFloat(ClbPer) + parseFloat(CalBrokerTotPer(parseFloat(table.rows[iCnt].cells[2].innerHTML.replace("$","")),"tblClb_Broker",Parent,iCnt));
            }
            totGrpAmt = parseFloat(totGrpAmt) + parseFloat(table.rows[iCnt].cells[0].children[3].value);
            if (document.getElementById(Parent + '_tblClb_Broker_PerTotal') != null) {
                var pertable = document.getElementById(Parent + '_tblClb_Broker_PerTotal');
            }
            addexternal(table.rows[iCnt].cells[0].innerHTML, parseFloat(pertable.rows[iCnt].cells[0].innerHTML), parseFloat(table.rows[iCnt].cells[2].innerHTML.replace("$", "")), Parent);
        }
        if (totGrpAmt > 0) {
            CalBrokerGroupTotalAmt(totGrpAmt, "tblClb_Broker", Parent);
            GrandTotAmt = GrandTotAmt + totGrpAmt;
        }
        else {
            ClearGropTotals("tblClb_Broker", Parent);
        }
        CommAmt = (parseFloat($('[id^=' + Parent + '_hdnCommForClbBroker]').val()) - totGrpAmt);
        //End of Code

        $('[id^=' + Parent + '_hdnCommForCoBroker]').val(CommAmt);

        var CoTb = Parent + '_tblCo_Broker';
        table = document.getElementById(CoTb);
        rowCount = table.rows.length;
        totGrpAmt = 0;

        for (iCnt = 1; iCnt <= rowCount - 1; iCnt++) {
            if (Updation == true) {
                var UpdatedAmt = UpdateAllBrokerTables(parseFloat($('[id^=' + Parent + '_hdnCommForCoBroker]').val()), table, iCnt);
                //table.rows[iCnt].cells[2].innerText = UpdatedAmt;
                table.rows[iCnt].cells[2].innerHTML = "$" + UpdatedAmt.toFixed(2);
                table.rows[iCnt].cells[0].children[3].value = UpdatedAmt;

                CoPer = parseFloat(CoPer) + parseFloat(CalBrokerTotPer(parseFloat(table.rows[iCnt].cells[2].innerHTML.replace("$", "")), "tblCo_Broker", Parent, iCnt));
            }
            // totGrpAmt = parseFloat(totGrpAmt) + parseFloat(table.rows[iCnt].cells[2].innerHTML);
            totGrpAmt = parseFloat(totGrpAmt) + parseFloat(table.rows[iCnt].cells[0].children[3].value);
            //For Fetching % of total
            if (document.getElementById(Parent + '_tblCo_Broker_PerTotal') != null) {
                var pertable = document.getElementById(Parent + '_tblCo_Broker_PerTotal');
            }
            addexternal(table.rows[iCnt].cells[0].innerHTML, parseFloat(pertable.rows[iCnt].cells[0].innerHTML), parseFloat(table.rows[iCnt].cells[2].innerHTML.replace("$", "")), Parent);
        }
        if (totGrpAmt > 0) {
            CalBrokerGroupTotalAmt(totGrpAmt, "tblCo_Broker", Parent);
            GrandTotAmt = GrandTotAmt + totGrpAmt;
        }
        else {
            ClearGropTotals("tblCo_Broker", Parent);
        }


        //set grand total

        //td - tblBrokersPerTot  td-tblBrokersAmtTot

        //var ExPer = ($("#" + Parent + "_td_tblExternalBroker_PerTotal").text()).replace("%", "");
        //var MasPer = ($("#" + Parent + "_td_tblMasterBroker_PerTotal").text()).replace("%", "");
        //var RefPer = ($("#" + Parent + "_td_tblReferralBroker_PerTotal").text()).replace("%", "");
        //var CoPer = ($("#" + Parent + "_td_tblCo_Broker_PerTotal").text()).replace("%", "");

        var totPer = 0;
        if (ExPer != "") {
            totPer = totPer + parseFloat(ExPer);
        }
        if (MasPer != "") {
            totPer = totPer + parseFloat(MasPer);
        }
        if (RefPer != "") {
            totPer = totPer + parseFloat(RefPer);
        }
        if(ClbPer!=""){
            totPer = totPer + parseFloat(ClbPer);
        }
        if (CoPer != "") {
            totPer = totPer + parseFloat(CoPer);
        }

        if (Updation == true) {
            $("#" + Parent + "_td_tblBrokersPerTot").text(parseFloat(totPer).toFixed(2) + "%");
            $("#" + Parent + "_td_tblBrokersAmtTot").text(FormatCurrency(parseFloat(GrandTotAmt), '$'));
        }

        if (tableRef == "tblExternalBroker") {
            CommAmtBrokerWise = $('[id^=' + Parent + '_hdnTotalComm]').val();
        }
        else if (tableRef == "tblMasterBroker") {
            CommAmtBrokerWise = $('[id^=' + Parent + '_hdnCommForMasterBroker]').val();
        }
        else if (tableRef == "tblReferralBroker") {
            CommAmtBrokerWise = $('[id^=' + Parent + '_hdnCommForRefferalBroker]').val();
        }
        else if(tableRef=="tblClb_Broker"){
            CommAmtBrokerWise = $('[id^=' + Parent + '_hdnCommForClbBroker]').val();
        }
        else if (tableRef == "tblCo_Broker") {
            CommAmtBrokerWise = $('[id^=' + Parent + '_hdnCommForCoBroker]').val();
        }
        Addtosummary(Parent);
        //GenerateCompSplitSummary('', '', '', '', '55', '')
        return CommAmtBrokerWise;
    }

    function UpdateAllBrokerTables(TotCommission, Table, Cnt) {
        
        var Percentage;
        //Percentage = Table.rows[Cnt].cells[1].innerHTML;

        Percentage = Table.rows[Cnt].cells[0].children[2].value;

        if (Percentage == "") {
            Percentage = parseFloat($("#ContentMain_CommCalculator_hdnCommPerForCoBrokerInit").val());
        }
        var NewCommAmount = (parseFloat(TotCommission) * parseFloat(Percentage) / 100);
        $("#ContentMain_CommCalculator_hdnCommPerForCoBrokerInit").val("");
        return parseFloat(NewCommAmount);


    }

    function SaveDupBrokerAsNew(Parent) {
       
        $('[id^=' + Parent + '_hdnBrokerId]').val(0);
        SaveBroker(Parent);
        CloseDupBrokerdPopup(Parent);

        var tb = Parent + "_tbDuplicateBroker";
        var table = document.getElementById(tb);
        var rowCount = table.rows.length;
        if (rowCount > 1) {
            $("#" + tb).empty();
        }
    }

    function UpdateDupBroker(Parent) {
   
        SaveBroker(Parent);
        CloseDupBrokerdPopup(Parent);

        var tb = Parent + "_tbDuplicateBroker";
        var table = document.getElementById(tb);
        var rowCount = table.rows.length;
        if (rowCount > 1) {
            $("#" + tb).empty();
        }
    }
    //hover For All Tables
    $('#ContentMain_CommCalculator_tblExternalBroker tr').hover(function () {
        $(this).addClass('hover');
    }, function () {
        $(this).removeClass('hover');
    });
    $('#ContentMain_CommCalculator_tblMasterBroker tr').hover(function () {
        $(this).addClass('hover');
    }, function () {
        $(this).removeClass('hover');
    });
    $('#ContentMain_CommCalculator_tblReferralBroker tr').hover(function () {
        $(this).addClass('hover');
    }, function () {
        $(this).removeClass('hover');
    });
    $('#ContentMain_CommCalculator_tblClb_Broker tr').hover(function () {
        $(this).addClass('hover');
    }, function () {
        $(this).removeClass('hover');
    });
    $('#ContentMain_CommCalculator_tblCo_Broker tr').hover(function () {
        $(this).addClass('hover');
    }, function () {
        $(this).removeClass('hover');
    });


    function EditCommissionPer(Parent) {
        debugger;
        var rowIndex;
        $("#" + Parent + "_tblExternalBroker tr").click(function () {
            rowIndex = $('#' + Parent + '_tblExternalBroker tr').index(this);
            $('[id^=' + Parent + '_hdnBrokerType]').val("tblExternalBroker");
            $('[id^=' + Parent + '_hdnRowTndex]').val(rowIndex);
            //ShowEditPopup("External Broker Edit", "tblExternalBroker", Parent);
          
            var tableRef = $('[id^=' + Parent + '_hdnBrokerType]').val();
            var tb = Parent + "_" + tableRef;
            var table = document.getElementById(tb);
            var hdnBrokerId = table.rows[rowIndex].cells[0].children[0].value;
            $('[id^=' + Parent + '_hdnBrokerId]').val(hdnBrokerId);
            ShowEditPopup("External Broker Edit", "tblExternalBroker", Parent, hdnBrokerId);
        })
        $("#" + Parent + "_tblMasterBroker tr").click(function () {
            debugger;
            rowIndex = $('#' + Parent + '_tblMasterBroker tr').index(this);
            $('[id^=' + Parent + '_hdnBrokerType]').val("tblMasterBroker");
            $('[id^=' + Parent + '_hdnRowTndex]').val(rowIndex);
            //ShowEditPopup("Master Broker Edit", "tblMasterBroker", Parent);
          
            var tableRef = $('[id^=' + Parent + '_hdnBrokerType]').val();
            var tb = Parent + "_" + tableRef;
            var table = document.getElementById(tb);
            var hdnBrokerId = table.rows[rowIndex].cells[0].children[0].value;
            $('[id^=' + Parent + '_hdnBrokerId]').val(hdnBrokerId);
            ShowEditPopup("Master Broker Edit", "tblMasterBroker", Parent, hdnBrokerId);
        })
        $("#" + Parent + "_tblReferralBroker tr").click(function () {
            debugger;
            rowIndex = $('#' + Parent + '_tblReferralBroker tr').index(this);
            $('[id^=' + Parent + '_hdnBrokerType]').val("tblReferralBroker");
            $('[id^=' + Parent + '_hdnRowTndex]').val(rowIndex);
            //ShowEditPopup("Referral Broker Edit", "tblReferralBroker", Parent);

            var tableRef = $('[id^=' + Parent + '_hdnBrokerType]').val();
            var tb = Parent + "_" + tableRef;
            var table = document.getElementById(tb);
            var hdnBrokerId = table.rows[rowIndex].cells[0].children[0].value;
            $('[id^=' + Parent + '_hdnBrokerId]').val(hdnBrokerId);
            ShowEditPopup("Referral Broker Edit", "tblReferralBroker", Parent, hdnBrokerId);
        })
        $("#" + Parent + "_tblClb_Broker tr").click(function () {
            rowIndex = $('#' + Parent + '_tblClb_Broker tr').index(this);
            $('[id^=' + Parent + '_hdnBrokerType]').val("tblClb_Broker");
            $('[id^=' + Parent + '_hdnRowTndex]').val(rowIndex);
            //ShowEditPopup("Clb_Broker Edit","tblClb_Broker",Parent);
           
            var tableRef = $('[id^=' + Parent + '_hdnBrokerType]').val();
            var tb = Parent + "_" + tableRef;
            var table = document.getElementById(tb);
            var hdnBrokerId = table.rows[rowIndex].cells[0].children[0].value;
            $('[id^=' + Parent + '_hdnBrokerId]').val(hdnBrokerId);
            ShowEditPopup("Clb_Broker Edit", "tblClb_Broker", Parent, hdnBrokerId);
        })
        $("#" + Parent + "_tblCo_Broker tr").click(function () {
            rowIndex = $('#' + Parent + '_tblCo_Broker tr').index(this);
            $('[id^=' + Parent + '_hdnBrokerType]').val("tblCo_Broker");
            $('[id^=' + Parent + '_hdnRowTndex]').val(rowIndex);
            //ShowEditPopup("Co-Broker Edit", "tblCo_Broker", Parent);
           
            var tableRef = $('[id^=' + Parent + '_hdnBrokerType]').val();
            var tb = Parent + "_" + tableRef;
            var table = document.getElementById(tb);
            var hdnBrokerId = table.rows[rowIndex].cells[0].children[0].value;
            $('[id^=' + Parent + '_hdnBrokerId]').val(hdnBrokerId);
            ShowEditPopup("Co-Broker Edit", "tblCo_Broker", Parent, hdnBrokerId);
        })
    };

    function SaveAllBrokersDetail(Parent) {
        //not working

        var table;
        var rowCount;
        var iCnt;
        var BrokerCommission = new Object();
        table = document.getElementById(Parent + '_tblExternalBroker');
        rowCount = table.rows.length;
        for (iCnt = 1; iCnt <= rowCount - 1; iCnt++) {
            BrokerCommission.BrokerTypeId = 1;
            BrokerCommission.BrokerId = parseInt(table.rows[iCnt].cells[0].lastChild.value);
            BrokerCommission.BrokerPercent = parseFloat(table.rows[iCnt].cells[1].innerHTML);
            BrokerCommission.BrokerCommission = parseFloat(table.rows[iCnt].cells[2].innerHTML);
            BrokerCommission.TermTypeId = 1
        }
        $.ajax({
            type: "POST",
            url: "NewChargeCommissionSpitCalCulator.aspx/SaveDealBrokerCommissions",
            data: JSON.stringify(BrokerCommission),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            failure: function (response) {
                alert(response.d);
            }
        });
    }

    function roundToTwo(num) {
        return +(Math.round(num + "e+2") + "e-2");
    }

    function FormatCurrency(n, currency) {

        return currency + " " + n.toFixed(2).replace(/./g, function (c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
        });
    }
    function CheckCoBrokerPer(title, tableRef, Parent) {
        debugger;
        var tb = Parent + "_" + tableRef;
        var table = document.getElementById(tb);
        var rowCount = table.rows.length;
        var iCnt;
        var totGrpPer = 0;

        for (iCnt = 1; iCnt <= rowCount - 1; iCnt++) {
            totGrpPer = parseFloat(totGrpPer) + parseFloat(table.rows[iCnt].cells[1].innerHTML);
            //   totCommissionAmt = parseFloat(totCommissionAmt) + parseFloat(table.rows[iCnt].cells[2].innerText);
        }
        var coBrokercompany = $("#ContentMain_hdnCoBrokerCompany").val(); 
        if (totGrpPer == 100) {
            alert("The Total Commission is 100%. Please change the commission % to add a new Co-Broker");
            return false;
        }
        else {
            GetCoBrokersOfSameCompany(coBrokercompany, tableRef);
            ShowPopUp(title, tableRef, Parent);
        }
    }


    
<%--New changes 9/23/19 start--%>
    function FillEditBrokerInfoByBrokerId(Parent, BrokerId) {
        debugger;
        $.ajax({
            type: "POST",
            url: "NewChargeCommissionSpitCalCulator.aspx/FillEditBrokerInfoByBrokerId",
            data: '{BrokerId:"' + BrokerId + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                debugger;
                var brokers = response.d;
                var tableRef = $('[id^=' + Parent + '_hdnBrokerType]').val();

                if (brokers.BrokerId > 0) {
                    var BrokerFullName = brokers.Name + ' ' + brokers.LastName;
                    //debugger;
                    $('[id^=' + Parent + '_txtEditCompany]').val(brokers.CompanyName);
                    $('[id^=' + Parent + '_txtEditAddress]').val(brokers.Address);
                    $('[id^=' + Parent + '_txtEditCity]').val(brokers.City);
                    $('[id^=' + Parent + '_ddlEditState]').val(brokers.State);
                    $('[id^=' + Parent + '_txtEditZipCode]').val(brokers.Zipcode);
                    $('[id^=' + Parent + '_txtEditName]').val(BrokerFullName);
                    $('[id^=' + Parent + '_txtEditEmail]').val(brokers.Email);
                    $('[id^=' + Parent + '_txtEditEin]').val(brokers.EIN);

                    //$('[id^=' + Parent + '_txtEditSharePer]').val(brokers.SharePercent);
                    //$('[id^=' + Parent + '_txtEditShareAmt]').val(brokers.SetAmount);

                    $('[id^=' + Parent + '_txtEditCompany]').attr("readonly", true);
                }
            },
            failure: function (msg) {
                alert(msg);
            }
        });
    }


    function ShowEditPopup(title, tableRef, Parent, hdnBrokerId) {
        debugger;
        clearEditDiv(Parent);
        FillEditBrokerInfoByBrokerId(Parent, hdnBrokerId)
        //$('#' + Parent + '_dvEditRecord').dialog('option', 'title', title);
        $('#' + Parent + '_dvEditRecord').dialog('option', 'title', title);
        $("#" + Parent + "_dvEditRecord").dialog("open");
        // do not set bellow hidden field as per parent
        $('[id^=ContentMain_CommCalculator_hdnUcName]').val(Parent);
    }

    function UpadteBrokerDetails(Parent) {
        debugger;
        var hdnBrokerId = parseInt($('[id^=' + Parent + '_hdnBrokerId]').val());
        var DealBrokerCommissionId = $('[id^=' + Parent + '_hdnDealBrokerCommissionId]').val();

        var Address = $('[id^=' + Parent + '_txtEditAddress]').val();
        var City = $('[id^=' + Parent + '_txtEditCity]').val();
        var State = $('[id^=' + Parent + '_ddlEditState]').val();
        var ZipCode = $('[id^=' + Parent + '_txtEditZipCode]').val();
        var Name = $('[id^=' + Parent + '_txtEditName]').val();
        var Email = $('[id^=' + Parent + '_txtEditEmail]').val();
        var Ein = $('[id^=' + Parent + '_txtEditEin]').val();

        $.ajax({
            type: "POST",
            url: "NewChargeCommissionSpitCalCulator.aspx/UpdateBroker",
            data: '{BrokerId: "' + hdnBrokerId + '",DealBrokerCommissionId:"' + DealBrokerCommissionId + '",Address:"' + Address + '",City:"' + City + '",State:"' + State + '",ZipCode:"' + ZipCode + '",Name:"' + Name + '",Email:"' + Email + '",Ein:"' + Ein + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                // GenerateCompSplitSummary(data.d[0], data.d[1], data.d[2], data.d[3], data.d[4], data.d[5]);
                //  data: '{BrokerId: "' + hdnBrokerId + '",CommPer:"' + CommPer + '",CommAmt:"' + CommAmt + '",DealBrokerCommissionId:"' + DealBrokerCommissionId + '"}',
            },
            failure: function (response) {
                alert(response.d);
            }
        });

    }

    function ValidateBrokerEditfields(Parent) {

        //debugger;
        //alert(Parent);
        var lblEditErrorMsg = document.getElementById('ContentMain_CommCalculator_lblEditErrorMsg');
        lblEditErrorMsg.innerHTML = "";

        var txtEditAddress = $('#' + Parent + '_txtEditAddress').val();
        var txtEditCity = $('#' + Parent + '_txtEditCity').val();
        var txtEditZipCode = $('#' + Parent + '_txtEditZipCode').val();
        var txtEditName = $('#' + Parent + '_txtEditName').val();
        var txtEditEmail = $('#' + Parent + '_txtEditEmail').val();
        var txtEditEin = $('#' + Parent + '_txtEditEin').val();
        var ddlEditState = $('#' + Parent + '_ddlEditState').val();
        var flag = "true";
        
        if (txtEditAddress.trim() == "") {
            flag = "false";
            lblEditErrorMsg.innerHTML += "Address cannot be blank";
        }
        if (txtEditCity.trim() == "") {
            flag = "false";
            lblEditErrorMsg.innerHTML += "<br>City cannot be blank";
        }
        if (ddlEditState.trim() == "") {
            flag = "false";
            lblEditErrorMsg.innerHTML += "<br>State cannot be blank";
        }

        if (txtEditZipCode == null || txtEditZipCode.trim() == "") {
            flag = "false";
            lblEditErrorMsg.innerHTML += "<br>Zipcode cannot be blank";
        }
        else {
            if (!validateZipLen(txtEditZipCode)) {
                lblEditErrorMsg.innerHTML += "<br>Enter valid Zipcode";
                flag = "false";
            }
        }
        if (txtEditName.trim() == "" || txtEditName == null) {
            flag = "false";
            lblEditErrorMsg.innerHTML += "<br>Name cannot be blank";
        }
        //if (txtEditEmail.trim() == "" || txtEditEmail == null) {
        //    flag = "false";
        //    lblEditErrorMsg.innerHTML += "<br>Email cannot be blank";
        //}
        //if (txtEditEin.trim() == "" || txtEditEin == null) {
        //    flag = "false";
        //    lblEditErrorMsg.innerHTML += "<br>EIN cannot be blank";
        //}

        return flag;
    }

<%--New changes 9/23/19 end--%>
</script>

<style type="text/css">
    #ContentMain_CommCalculator_tblExternalBroker tr:hover {
        background-color: #D8D8D8;
    }

    #ContentMain_CommCalculator_tblMasterBroker tr:hover {
        background-color: #D8D8D8;
    }

    #ContentMain_CommCalculator_tblReferralBroker tr:hover {
        background-color: #D8D8D8;
    }

    #ContentMain_CommCalculator_tblClb_Broker tr:hover {
        background-color: #D8D8D8;
    }

    #ContentMain_CommCalculator_tblCo_Broker tr:hover {
        background-color: #D8D8D8;
    }

    .tableStyle {
        border-collapse: collapse;
        border: 1px solid black;
    }

        .tableStyle td {
            border: 1px solid black;
        }

    .auto-style1 {
        width: 10%;
    }

    .buttonSmall {
        background-color: #B8B8B8;
        font-family: Verdana;
        font-weight: 600;
        border-radius: 0px;
        border: 1px solid #000000;
        color: black;
        cursor: pointer;
    }

    .tableheader {
        background-color: #E0E0E0;
    }

    .Brk_td_1 {
        width: 17%;
        vertical-align: top;
        text-align: center;
    }

    .Brk_td_2 {
        width: 57%;
        text-align: center;
        vertical-align: top;
    }

    .Brk_td_3 {
        width: 11%;
        text-align: center;
        vertical-align: top;
    }

    .Brk_td_4 {
        width: 15%;
        text-align: center;
        vertical-align: central;
    }

    .BtnFixPosition {
        /*position: absolute; margin-top: 50px; margin-left: 20px;*/
    }

    .TitleFixPosition {
        /*position: absolute;
        margin-left: 15px;*/
    }

    .td-SharePercentage {
        text-align: center;
    }

    .divMinHeight {
        min-height: 95px;
    }
</style>

<%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
    <ContentTemplate>--%>
<asp:HiddenField ID="hdnTotalComm" Value="" runat="server" />
<table style="width: 100%; table-layout: fixed;" cellpadding="0" cellspacing="0" class="tableStyle" runat="server" id="maintb">
    <tr class="tableheader">
        <td>
            <div>
                <table class="tbBorder" style="width: 100%;">
                    <tr>
                        <td style="border-bottom: none;" class="Brk_td_1"></td>
                        <td style="border-bottom: none;" class="Brk_td_2"><span class="SummaryHeading">BROKER(S) COMMISSION TOTALS</span><br />
                            <table id="rbHeading" runat="server" width="100%" border="0" style="border-color: transparent; font-size: 8px;">
                                <tr>
                                    <td class="td-BrokerName" style="border: none; text-align: center">BROKERS</td>
                                    <td class="td-SharePercentage" style="border: none; text-align: center">PERCENTAGE</td>
                                    <td class="td-Commission" style="border: none; text-align: center;">COMMISSION</td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: none;" class="Brk_td_3 auto-style1 SummaryHeading">% OF<br />
                            TOTAL</td>
                        <td style="border-bottom: none;" class="Brk_td_4 SummaryHeading">GROUP TOTAL</td>
                    </tr>
                    <%-- <tr>
                        <td style="border-bottom: none;"></td>
                        <td style="border-bottom: none; text-align: center; width: 55.75%;"><span class="SummaryHeading">BROKER(S) COMMISSION TOTALS</span><br />
                            <table id="rbHeading" runat="server" width="100%" border="0" style="border-color: transparent; font-size: 8px;">
                                <tr>
                                    <td class="td-BrokerName" style="border: none; text-align: center">BROKERS</td>
                                    <td class="td-SharePercentage" style="border: none; text-align: center">PERCENTAGE</td>
                                    <td class="td-Commission" style="border: none; text-align: center;">COMMISSION</td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: none; width: 10.75%; text-align: center;" class="auto-style1 SummaryHeading">% OF<br />
                            TOTAL</td>
                        <td style="border-bottom: none; width: 17%; text-align: center;" class="SummaryHeading">GROUP TOTAL</td>
                    </tr>--%>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div>
                <table class="tbBorder divMinHeight" cellspacing="0" cellpadding="0" style="width: 100%; height: 100%;">
                    <tr>
                        <td class="Brk_td_1" style="border-bottom: none;">
                            <h5 class="TitleFixPosition">EXTERNAL
                                <br />
                                BROKER</h5>
                            <button id="btnAddExternalBroker" onclick="ShowPopUp('EXTERNAL BROKER','tblExternalBroker','<%= this.ClientID %>');return false;" class="buttonSmall BtnFixPosition">ADD</button>

                        </td>
                        <td class="Brk_td_2" style="position: initial; border-bottom: none;">
                            <%--    <div style="height: 100px;"> style="border:none"--%>
                            <table id="tblExternalBroker" cellspacing="0" cellpadding="0" width="100%" style="max-height: 100px" runat="server">
                                <tr style="display: none;">
                                    <td class="td-BrokerName"></td>
                                    <td class="td-SharePercentage"></td>
                                    <td class="td-Commission"></td>
                                    <td class="td-DeleteRecord" style="width: 4%"></td>
                                </tr>
                            </table>
                            <%-- </div>--%>
                            <asp:HiddenField ID="hdnRowCount" runat="server" Value="" />
                        </td>
                        <td class="Brk_td_3" style="border-bottom: none;">
                            <table runat="server" id="tblExternalBroker_PerTotal" cellspacing="0" cellpadding="0" style="width: 100%">
                                <tr style="display: none;">
                                    <td class="td-PercentageOfTotal"></td>
                                </tr>
                            </table>
                        </td>
                        <%-- <td id="td_tblExternalBroker_PerTotal" style="visibility: hidden;"></td>--%>
                        <td id="td_tblExternalBroker_AmtTotal" class="Brk_td_4" style="border-bottom: none; color: #E65C00;" runat="server"></td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div>
                <table class="tbBorder divMinHeight" cellspacing="0" cellpadding="0" style="width: 100%; height: 100%;">
                    <tr>
                        <td class="Brk_td_1" style="border-bottom: none;">
                            <h5 class="TitleFixPosition">MASTER BROKER</h5>
                            <button id="btnAddMasterBroker" onclick="ShowPopUp('MASTER BROKER','tblMasterBroker','<%= this.ClientID %>');return false;" class="buttonSmall BtnFixPosition">ADD</button>
                        </td>
                        <td class="Brk_td_2" style="border-bottom: none; position: initial;">
                            <%-- <div style="height: 100px; overflow-y: scroll;">--%>
                            <table id="tblMasterBroker" cellpadding="0" cellspacing="0" width="100%" runat="server">
                                <tr style="display: none;">
                                    <td class="td-BrokerName"></td>
                                    <td class="td-SharePercentage"></td>
                                    <td class="td-Commission"></td>
                                    <td class="td-DeleteRecord" style="width: 4%"></td>
                                </tr>
                            </table>
                            <%--</div>--%>
                        </td>
                        <td class="Brk_td_3" style="border-bottom: none;">
                            <table runat="server" id="tblMasterBroker_PerTotal" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr style="display: none;">
                                    <td style="border-top: none"></td>
                                </tr>
                            </table>
                        </td>
                        <%--  <td class="auto-style1" id="td_tblMasterBroker_PerTotal" style="visibility: hidden;"></td>--%>
                        <td class="Brk_td_4" id="td_tblMasterBroker_AmtTotal" style="border-bottom: none; color: #E65C00;" runat="server"></td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div>
                <table class="tbBorder divMinHeight" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
                    <tr>
                        <td class="Brk_td_1" style="border-bottom: none;">
                            <h5 class="TitleFixPosition">REFERRAL BROKER</h5>
                            <button id="btnAddReferralBroker" onclick="ShowPopUp('REFERRAL BROKER','tblReferralBroker','<%= this.ClientID %>');return false;" class="buttonSmall BtnFixPosition">ADD</button>
                        </td>
                        <td class="Brk_td_2" style="border-bottom: none; position: initial;">
                            <%--    <div style="height: 100px; overflow-y: scroll;">--%>
                            <table id="tblReferralBroker" cellpadding="0" cellspacing="0" width="100%" runat="server">
                                <tr style="display: none;">
                                    <td class="td-BrokerName"></td>
                                    <td class="td-SharePercentage"></td>
                                    <td class="td-Commission"></td>
                                    <td class="td-DeleteRecord" style="width: 4%;"></td>
                                </tr>
                            </table>
                            <%-- </div>--%>
                        </td>
                        <td class="Brk_td_3" style="border-bottom: none;">
                            <table runat="server" id="tblReferralBroker_PerTotal" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr style="display: none;">
                                    <td style="border-top: none;"></td>
                                </tr>
                            </table>
                        </td>
                        <%-- <td class="auto-style1" id="td_tblReferralBroker_PerTotal" style="visibility: hidden;"></td>--%>
                        <td class="Brk_td_4" id="td_tblReferralBroker_AmtTotal" style="border-bottom: none; color: #E65C00;" runat="server"></td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <%-- Added New Broker Type Company Landlord Broker which will act same as Co-Broker their split n all 23-05-2018 Sudhakar --%>
    <tr id="trCLBBrokerSection" runat="server" style="display:none;">
        <td>
            <div>
                <table class="tbBorder divMinHeight" style="width: 100%; height: 100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Brk_td_1" style="border-bottom: none;">
                            <%--<h5 class="TitleFixPosition">COMPANY LANDLORD BROKER</h5>--%>
                            <%--<h5 class="TitleFixPosition">CLB-BROKER</h5>--%>
                            <h5 class="TitleFixPosition" id="CLBBrokerText" runat="server">SG-LANDLORD</h5>
                            <button id="btnAddClbBroker" onclick="CheckCoBrokerPer('COMPANY LANDLORD BROKER','tblClb_Broker','<%= this.ClientID %>');return false;" class="buttonSmall BtnFixPosition">ADD</button>
                        </td>
                        <td class="Brk_td_2" style="border-bottom: none; position: initial;">                            
                            <table id="tblClb_Broker" width="100%" cellpadding="0" cellspacing="0" runat="server" style="width: 100%">
                                <tr style="display: none;">
                                    <td class="td-BrokerName"></td>
                                    <td class="td-SharePercentage"></td>
                                    <td class="td-Commission"></td>
                                    <td class="td-DeleteRecord" style="width: 4%"></td>
                                </tr>
                            </table>                            
                        </td>
                        <td class="Brk_td_3" style="border-bottom: none;">
                            <table runat="server" id="tblClb_Broker_PerTotal" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr style="display: none;">
                                    <td></td>
                                </tr>
                            </table>
                        </td>                        
                        <td class="Brk_td_4" id="td_tblClb_Broker_AmtTotal" style="border-bottom: none; color: #E65C00;" runat="server"></td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <%-- End On New Broker Region --%>
    <tr id="trCoBrokerSection" runat="server" style="display:none;">
        <td><%--onclick="ShowPopUp('CO-BROKER','tblCo_Broker','<%= this.ClientID %>');return false;"--%>
            <div>
                <table class="tbBorder divMinHeight" style="width: 100%; height: 100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Brk_td_1" style="border-bottom: none;">
                            <%--<h5 class="TitleFixPosition">CO-BROKER</h5>--%>
                            <h5 class="TitleFixPosition" id="CoBrokerText" runat="server">SG-TENANT</h5>
                            <button id="btnAddCoBroker" onclick="CheckCoBrokerPer('CO-BROKER','tblCo_Broker','<%= this.ClientID %>');return false;" class="buttonSmall BtnFixPosition">ADD</button>
                        </td>
                        <td class="Brk_td_2" style="border-bottom: none; position: initial;">
                            <%--  <div style="height: 100px; overflow-y: scroll;">--%>
                            <table id="tblCo_Broker" width="100%" cellpadding="0" cellspacing="0" runat="server" style="width: 100%">
                                <tr style="display: none;">
                                    <td class="td-BrokerName"></td>
                                    <td class="td-SharePercentage"></td>
                                    <td class="td-Commission"></td>
                                    <td class="td-DeleteRecord" style="width: 4%"></td>
                                </tr>
                            </table>
                            <%--</div>--%>
                        </td>
                        <td class="Brk_td_3" style="border-bottom: none;">
                            <table runat="server" id="tblCo_Broker_PerTotal" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr style="display: none;">
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                        <%-- <td class="auto-style1" id="td_tblCo_Broker_PerTotal" style="visibility: hidden;"></td>--%>
                        <td class="Brk_td_4" id="td_tblCo_Broker_AmtTotal" style="border-bottom: none; color: #E65C00;" runat="server"></td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td style="border: none;">
            <div>
                <table class="tbBorder" style="width: 100%;" runat="server">
                    <tr>
                        <td class="Brk_td_1" style="font-weight: bold; border-right: none !important">TOTAL</td>
                        <td class="Brk_td_2" style="border-left: none !important;"></td>
                        <%--<td style="width: 74%; font-weight: bold; padding-left: 5em; padding-bottom: 1em; padding-top: .5em;">TOTAL
                        </td>--%>
                        <td id="td_tblBrokersPerTot" style="text-align: center;" class="Brk_td_3"></td>
                        <td id="td_tblBrokersAmtTot" style="color: #E65C00; text-align: center;" class="Brk_td_4"></td>
                        <%--   <td style="width:1.5%; border-left:none"></td>--%>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>

<%-- <asp:Button ID="btnSave" runat="server" Text="Save Test" OnClick="btnSave_Click" Visible="false" />--%>
<%-- </ContentTemplate>
</asp:UpdatePanel>--%>

<asp:Label ID="lbDummybtn" runat="server"></asp:Label>
<%--  style="height: auto; width: 400px;display:none;"--%>
<div id="dvAddBroker" title="" runat="server">
    <%-- <table style="width: 100%" class="spacing">--%>
    <%--  <tr style="background-color: gray; border-bottom: solid;">
                    <td style="text-align: left; width: 95%">
                        <asp:UpdatePanel ID="upTitile" runat="server">
                            <ContentTemplate>
                                <h3>&nbsp; &nbsp;      
                                <asp:Label ID="lbTitle" runat="server"></asp:Label></h3>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td style="text-align: right;">
                        <asp:ImageButton ImageUrl="~/Images/close.png" ID="btnCancel" runat="server" />
                    </td>
                </tr>--%>
    <%--  <tr>
            <td colspan="2">--%>
    <%--<uc:AddBroker ID="AddNewBroker" runat="server" />--%>
    <%--  <asp:UpdatePanel ID="upAddBroker" runat="server" UpdateMode="Always">
        <ContentTemplate>--%>
    <table style="width: 100%">
        <tr>
            <td colspan="2">ENTER INFORMATION. PRESS OK WHEN DONE.
            
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="lblErrorMsg" ForeColor="Red" Font-Bold="true" Font-Names="Vardena" Font-Size="Small" Text=""></asp:Label>
            </td>
        </tr>
        <tr id="tr_BrokerCompany" runat="server">
            <td style="width: 25%">COMPANY
            </td>
            <td>
                <asp:TextBox ID="txtCompany" runat="server" Width="250px" Height="18px"></asp:TextBox>
                <ajax:AutoCompleteExtender ID="acBrokerInfo" runat="server"
                    TargetControlID="txtCompany"
                    DelimiterCharacters=";, :"
                    MinimumPrefixLength="1"
                    EnableCaching="true"
                    CompletionSetCount="10"
                    CompletionInterval="300"
                    ServiceMethod="SearchBrokerComapny"
                    OnClientPopulating="ShowIcon"
                    OnClientPopulated="hideIcon"
                    OnClientItemSelected="OnBrokerSelected"
                    ShowOnlyCurrentWordInCompletionListItem="true"
                    CompletionListCssClass="AutoExtender"
                    CompletionListItemCssClass="AutoExtenderList"
                    CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                    CompletionListElementID="divwidth"
                    UseContextKey="true" />
                <asp:HiddenField ID="hdnBrokerId" runat="server" Value="" />
                <asp:HiddenField ID="hdnDealBrokerCommissionId" runat="server" Value="" />
            </td>
        </tr>
        <tr id="tr_BrokerDropDown" runat="server">
            <td style="width: 25%">Broker
            </td>
            <td>
                <asp:DropDownList ID="ddlBrokers" runat="server" Width="250px" Font-Size="Small"></asp:DropDownList>
            </td>
        </tr>

        <tr>
            <td style="width: 25%">ADDRESS
            </td>
            <td>
                <asp:TextBox ID="txtAddress" runat="server" Width="250px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%">CITY
            </td>
            <td>
                <asp:TextBox ID="txtCity" runat="server" Width="170px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%">STATE
            </td>
            <td>
                <asp:DropDownList ID="ddlState" runat="server" Width="170px" Font-Size="Small"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 25%">ZIPCODE
            </td>
            <td>
                <asp:TextBox ID="txtZipCode" runat="server" Width="170px" Height="18px" onkeypress="return Validate(event);"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%">NAME
            </td>
            <td>
                <asp:TextBox ID="txtName" runat="server" Width="250px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <%-- Onblur= "javascript:EmailValidation();"--%>
            <td style="width: 25%">EMAIL
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" Width="250px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align: top">EIN
            </td>
            <td>
                <asp:TextBox ID="txtEin" runat="server" Width="170px" Height="18px"></asp:TextBox>
                <br />
                <br />
                <br />
                <br />

            </td>
        </tr>

        <tr>
            <td style="width: 25%">SHARE (%)
            </td>
            <td>
                <asp:TextBox ID="txtShare" runat="server" Width="100px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%">SET AMOUNT
            </td>
            <td>
                <asp:TextBox ID="txtSetAmount" runat="server" Width="100px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%"></td>
            <td style="text-align: right;">
                <asp:Button ID="btnAddBroker" runat="server" Text="OK" CssClass="SqureButton" Width="100px" OnClick="btnAddBroker_Click" />
                <%-- <button id="b" onclick="btnAddExternalBroker_Save();return false;" class="btnAddBrokertype">Add Broker</button> OnClientClick="btnAddExternalBroker_Save();return true;"--%> 
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnBrokerType" runat="server" />
    <asp:HiddenField ID="hdnBrokerPercent" runat="server" />
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
    <%--</td>
        </tr>
    </table>--%>
</div>


<%--<div id="dvEditRecord" title="" style="text-align: center" runat="server">
    <table style="width: 100%">
        <tr>
            <td style="width: 45%">SHARE (%)
            </td>
            <td>
                <asp:TextBox ID="txtEditSharePer" runat="server" Width="100px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 45%">SET AMOUNT
            </td>
            <td>
                <asp:TextBox ID="txtEditShareAmt" runat="server" Width="100px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnEditRecord" Text="OK" CssClass="SqureButton" runat="server" OnClientClick="UpdateCommPercentage();" />
            </td>
        </tr>
    </table>
</div>--%>

<%--New changes 9/23/19 start--%>
<div id="dvEditRecord" title="" runat="server">
    <table style="width: 100%">
        <tr>
            <td colspan="2">ENTER INFORMATION. PRESS OK WHEN DONE.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="lblEditErrorMsg" ForeColor="Red" Font-Bold="true" Font-Names="Vardena" Font-Size="Small" Text=""></asp:Label>
            </td>
        </tr>
        <tr id="tr1" runat="server">
            <td style="width: 25%">COMPANY
            </td>
            <td>
                <asp:TextBox ID="txtEditCompany" runat="server" Width="250px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%">ADDRESS
            </td>
            <td>
                <asp:TextBox ID="txtEditAddress" runat="server" Width="250px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%">CITY
            </td>
            <td>
                <asp:TextBox ID="txtEditCity" runat="server" Width="170px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%">STATE
            </td>
            <td>
                <asp:DropDownList ID="ddlEditState" runat="server" Width="170px" Font-Size="Small"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 25%">ZIPCODE
            </td>
            <td>
                <asp:TextBox ID="txtEditZipCode" runat="server" Width="170px" Height="18px" onkeypress="return Validate(event);"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%">NAME
            </td>
            <td>
                <asp:TextBox ID="txtEditName" runat="server" Width="250px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%">EMAIL
            </td>
            <td>
                <asp:TextBox ID="txtEditEmail" runat="server" Width="250px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align: top">EIN
            </td>
            <td>
                <asp:TextBox ID="txtEditEin" runat="server" Width="170px" Height="18px"></asp:TextBox>
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>

          <tr>
            <td style="width: 25%">SHARE (%)
            </td>
            <td>
                <asp:TextBox ID="txtEditSharePer" runat="server" Width="100px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%">SET AMOUNT
            </td>
            <td>
                <asp:TextBox ID="txtEditShareAmt" runat="server" Width="100px" Height="18px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%"></td>
            <td style="text-align: right;">
                <asp:Button ID="btnEditRecord" Text="OK" CssClass="SqureButton" runat="server"  Width="100px" OnClientClick="UpdateCommPercentage();" />
            </td>
        </tr>
    </table>
</div>
<%--New changes 9/23/19 end--%>

<div id="dvGetSameCmpNmBrokers" title="Brokers From Same Company" style="text-align: center; width: 400px;" runat="server" class="spacing">
    <table style="width: 100%">
        <tr>
            <td colspan="2">
                <table style="width: 97%; border: solid 1px">
                    <tr style="font-weight: 900; background-color: gray;">
                        <td style="width: 6%; text-align: left; padding-left: 2px"></td>
                        <td style="width: 30%; text-align: left; padding-left: 2px">Name</td>
                        <td style="width: 64%; text-align: left; padding-left: 2px">Address</td>
                        <%--  <td style="width: 15%; text-align: right; visibility: hidden">Share %</td>
                        <td style="width: 20%; text-align: right; visibility: hidden">Share Amount</td>--%>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="dvGvDupLoc" style="overflow-y: scroll; height: 200px; border: solid 1px">
                    <table id="tbBrokerFromSameCmp" width="100%" runat="server">
                        <tr>
                            <td style="width: 4%; text-align: left; padding-left: 2px"></td>
                            <td style="width: 30%; text-align: left; padding-left: 2px"></td>
                            <td style="width: 64%; text-align: left; padding-left: 2px"></td>
                            <%--     <td style="width: 15%; text-align: right; visibility: hidden"></td>
                            <td style="width: 20%; text-align: right; visibility: hidden"></td>--%>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <button id="btnAddNewBroker" onclick="AddNewBroker('<%= this.ClientID %>');return false;" class="buttonSmall">ADD NEW</button>
            </td>
            <td style="text-align: right;">
                <button id="btnOk" onclick="SelectBroker('<%= this.ClientID %>');return false;" class="buttonSmall">OK</button>
            </td>
        </tr>
    </table>
</div>

<div id="dvDuplicateBrokerEntry" title="Add Or Update Broker" style="text-align: center; width: 600px;" runat="server">
    <table style="width: 100%" class="spacing">
        <tr>
            <td colspan="2">
                <table style="width: 97%; border: solid 1px; font-family: Verdana;">
                    <tr style="font-weight: 900; vertical-align: top; text-align: left; background-color: gray;">
                        <td style="width: 6%"></td>
                        <td style="width: 15%;">Name</td>
                        <td style="width: 49%">Address</td>
                        <td style="width: 20%;">E-Mail</td>
                        <td style="width: 10%;">EIN</td>
                        <%--   <td style="width: 15%; text-align: left">Share %</td>
                        <td style="width: 15%; text-align: left">Share Amount</td>--%>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="Div2" style="overflow-y: scroll; height: 250px; border: solid 1px">
                    <table id="tbDuplicateBroker" style="width: 100%; border: solid 1px" runat="server">
                        <tr style="vertical-align: top; text-align: left;">
                            <td style="width: 6%;"></td>
                            <td style="width: 15%; text-wrap: normal;"></td>
                            <td style="width: 49%; text-wrap: normal;"></td>
                            <td style="width: 20%; text-wrap: normal; word-break: break-all;"></td>
                            <td style="width: 10%; text-wrap: normal;"></td>
                            <%--  <td style="width: 15%;"></td>
                            <td style="width: 15%;"></td>--%>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <button id="btnUpdateBroker" class="buttonSmall" onclick="UpdateDupBroker('<%=this.ClientID %>');return false;">UPDATE BROKER</button>
            </td>
            <td style="text-align: right;">
                <button id="btnInsertBroker" class="buttonSmall" onclick="SaveDupBrokerAsNew('<%=this.ClientID %>');return false;">ADD NEW BROKER</button>
            </td>
        </tr>
    </table>
</div>


<asp:HiddenField ID="hdnCommForMasterBroker" runat="server" Value="0" />
<asp:HiddenField ID="hdnCommForRefferalBroker" runat="server" Value="0" />
<asp:HiddenField ID="hdnCommForClbBroker" runat="server" Value="0"/>
<asp:HiddenField ID="hdnCommForCoBroker" runat="server" Value="0" />
<asp:HiddenField ID="hdnRowTndex" runat="server" />
<asp:HiddenField ID="hdnUcName" runat="server" />
<asp:HiddenField ID="hdnCommPerForCoBrokerInit" runat="server"></asp:HiddenField>
<asp:HiddenField ID="hdnBrokerIdForSameCmp" runat="server" />

<asp:HiddenField ID="hdnSharePer" runat="server" />
<asp:HiddenField ID="hdnShareAmt" runat="server" />




