﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_ucAddNewBuyerTenantAdmin : System.Web.UI.UserControl
{
    #region Properties

    public string ClassRef
    {
        get;
        set;
    }
    public string btnaddedit
    {
        get { return btnAddNew.Text; }
        set { btnAddNew.Text = value; }
    }
    public string PartyId
    {
        get { return hdnPartyId.Value; }
        set { hdnPartyId.Value = value; }

    }
    public string PartyTypeId
    {
        get { return hdnPartyTypeId.Value; }
        set { hdnPartyTypeId.Value = value; }

    }
    public string CompanyName
    {
        get { return txtComapanyName.Text; }
        set { txtComapanyName.Text = value; }
    }
    public string Name
    {
        get { return txtName.Text; }
        set { txtName.Text = value; }
    }
    public string Address1
    {
        get { return txtAdd1.Text; }
        set { txtAdd1.Text = value; }
    }
    public string Address2
    {
        get { return txtAdd2.Text; }
        set { txtAdd2.Text = value; }
    }
    public string City
    {
        get { return txtCity.Text; }
        set { txtCity.Text = value; }
    }
    public string State
    {
        get { return ddlState.SelectedValue; }
        set { ddlState.SelectedValue = value; }
    }
    public string Email
    {
        get { return txtEmail.Text; }
        set { txtEmail.Text = value; }
    }
    public string ZipCode
    {
        get { return txtZipCode.Text; }
        set { txtZipCode.Text = value; }
    }
    public bool Isactive
    {
        get { return chkIsactive.Checked; }
        set { chkIsactive.Checked = value; }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ResetControls();
        }

    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        lbErrorMsg.Text = "";

        //Choose obj of which class to be invoked

        //Perform validation before save
        DealParties objDp = new DealParties();

        objDp.PartyTypeId = Convert.ToInt32(hdnPartyTypeId.Value);
        objDp.PartyCompanyName = txtComapanyName.Text.Trim();
        objDp.Email = txtEmail.Text.Trim();
        objDp.PartyName = txtName.Text.Trim();
        objDp.Address1 = txtAdd1.Text.Trim();
        objDp.Address2 = txtAdd2.Text.Trim();
        objDp.City = txtCity.Text.Trim();
        if (!string.IsNullOrEmpty(ddlState.SelectedValue))
        objDp.State = Convert.ToString(ddlState.SelectedValue);
        if (!string.IsNullOrEmpty(txtZipCode.Text.Trim()))
            objDp.ZipCode = Convert.ToString(txtZipCode.Text.Trim());
        objDp.IsActive = Convert.ToBoolean(chkIsactive.Checked);
        if (!string.IsNullOrEmpty(hdnPartyId.Value))
        {
            objDp.PartyID = Convert.ToInt32(hdnPartyId.Value);
            
            objDp.ModifiedBy = Convert.ToInt32(Session["UserId"]);
            objDp.ModifiedOn = DateTime.Now;
         
        }
        else
        {
            objDp.CreatedBy = Convert.ToInt32(Session["UserId"]);
            objDp.CreatedOn = DateTime.Now;
        }
        bool rslt = objDp.Save();
        this.Page.GetType().InvokeMember("Bindgrid", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
        ResetControls();
    }
    #region Custom Methods
    public void ResetControls()
    {
        Utility.FillStates(ref ddlState);
        txtComapanyName.Text = "";
        txtAdd1.Text = "";
        txtAdd2.Text = "";
        txtCity.Text = "";
        txtName.Text = "";
        txtZipCode.Text = "";
        lbErrorMsg.Text = "";
        txtEmail.Text = "";
        chkIsactive.Checked = true;
        SetAttributes();


    }
    private void SetAttributes()
    {
        //txtZipCode.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtZipCode.ClientID + "')");
       
    }
    #endregion
}