﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucLeaseTransactionBuilding.ascx.cs" Inherits="UserControl_ucLeaseTransactionBuilding" %>
<%--< %@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>--%>
<%@ Register Src="~/UserControl/ucAddNewBuyerTenant.ascx" TagName="NewBuyer" TagPrefix="uc" %>
<%--<link href="../Styles/Site.css" rel="stylesheet" />

<link href="../Styles/SiteTEST.css" rel="stylesheet" />--%>

<script type="text/javascript">
    function SetNNN() {
        var NNN = 0;
        var CAM = parseFloat(document.getElementById("<%=txtCAM.ClientID%>").value);
        var INS = parseFloat(document.getElementById("<%=txtINS.ClientID%>").value);
        var TAX = parseFloat(document.getElementById("<%=txtTAX.ClientID%>").value);
        if (!isNaN(CAM)) {
            NNN = CAM;
        }
        if (!isNaN(INS)) {
            NNN = NNN + INS;
        }
        if (!isNaN(TAX)) {
            NNN = NNN + TAX;
        }
        document.getElementById("<%=txtNNN.ClientID%>").value = NNN;
        return false;
    }


    function ShowIcon(sender, e) {
        sender._element.className = "loadingUC";
    }

    function hideIcon(sender, e) {
        sender._element.className = "textwidthUC";
    }

    function OnLocationSelected(source, eventArgs) {
        document.getElementById('<%=hdnTenantID.ClientID %>').value = eventArgs.get_value();
        //   alert(eventArgs.get_value());
        return false;
    }

    function CalculateAmount()
    {
       
        var size = document.getElementById("<%=txtSize.ClientID%>").value;
        var txtTI =document.getElementById("<%=txtTI.ClientID%>").value;
       
        var ddlTIAType = document.getElementById('<%=ddlTIAllowanceType.ClientID%>').value;       
        //alert(size + " " + txtTI + " " + ddlTIAType);
       // alert(txtTI);
        var msg;
        if(size!="" && txtTI != "")
        {
            if (ddlTIAType == 1)
            {
                msg = "<b>Aggregate Amount :</b> $" + parseFloat(size * txtTI).toFixed(2);            
            }
            else if (ddlTIAType == 2)
            {
                msg = "<b>Per Square Foot :</b> $" + parseFloat(txtTI / size).toFixed(2);             
            }          
            else if (ddlTIAType == 0) {
                msg = "";
            }
            
            document.getElementById("<%=lblTIAllowanceMsg.ClientID%>").innerHTML = msg;
            
        }
    }
    
</script>

<%--<link href="../Styles/AutoCompleteStyle.css" rel="stylesheet" />--%>
 

<table  class="ucbackground" style="width:100%">
    <tr>
        <td colspan="3" class="ucheadings">
            LEASE TRANSACTION: BUILDING
            <hr" />
        </td>
    </tr>
     <tr>
        <td colspan ="3">
          <br /> 
        </td>
    </tr>
    <tr>
        <td class="uclabels">SIZE
        </td>
        <td colspan="2" >
            <asp:TextBox ID="txtSize" runat="server" CssClass="uctextboxMed" TabIndex="24" onblur="CalculateAmount()"></asp:TextBox>
            &nbsp;
            <b>SF</b>
        </td>
    </tr>
     <tr>
        <td>
          <br /> 
        </td>
    </tr>
    <tr>
        <td class="uclabels">TENANT
        </td>
        <td colspan="2">
            <asp:TextBox runat="server" ID="txtTenant" CssClass="uctextboxMed" AutoPostBack="true" OnTextChanged="txtTenant_TextChanged" TabIndex="25" />
            <ajax:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server"
                TargetControlID="txtTenant"
                DelimiterCharacters=";, :"
                MinimumPrefixLength="1"
                EnableCaching="true"
                CompletionSetCount="10"
                CompletionInterval="300"
                ServiceMethod="SearchGetDealPartiesList"
                OnClientPopulating="ShowIcon"
                OnClientPopulated="hideIcon"
                OnClientItemSelected="OnLocationSelected"
                ShowOnlyCurrentWordInCompletionListItem="true"
                CompletionListCssClass="AutoExtender"
                CompletionListItemCssClass="AutoExtenderList"
                CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                CompletionListElementID="divwidth" />
            <asp:HiddenField ID="hdnTenantID" runat="server" />
            <ajax:ModalPopupExtender ID="mpeAddNewTenantBuilding" TargetControlID="btnAddNewTenantBuilding" CancelControlID="ucNwTenant$ImgbtnCancel" 
                PopupControlID="dvNwTenantBuilding" runat="server" BackgroundCssClass="modalBackground">
            </ajax:ModalPopupExtender>
            &nbsp;
              <asp:ImageButton ID="btnAddNewTenantBuilding" runat="server" CssClass="imgSizeSmall" ImageUrl="~/Images/Add-Icon.gif" />
          <%--  <asp:Button ID="btnAddNewTenantBuilding" runat="server" Text="Add New" CssClass="SqureButton"   />--%>
        </td>
    </tr>
     <tr>
        <td>
          <br /> 
        </td>
    </tr>
    <tr>
        <td class="uclabels">USE
        </td>
        <td colspan="2">
            <asp:DropDownList ID="ddlUse" runat="server" CssClass="ddlSmall" TabIndex="26" >
                <%--<asp:ListItem Text="Select"></asp:ListItem>
                <asp:ListItem Text="RETAIL" Value="Retail"></asp:ListItem>
                <asp:ListItem Text="RESTAURANT" Value="Restaurant"></asp:ListItem>--%>
            </asp:DropDownList>
        </td>
    </tr>
     <tr>
        <td>
          <br /> 
        </td>
    </tr>
    <tr>
        <td class="uclabels">LEASE START
        </td>
        <td colspan="2">
            <asp:TextBox ID="txtLeaseStart" runat="server" CssClass="uctextboxMed" TabIndex="27"></asp:TextBox>
            <asp:ImageButton ID="btnCalender" runat="server" CssClass="imgSizeSmall" ImageUrl="~/Images/Calander.jpeg" />
            <ajax:CalendarExtender ID="ajaxCal" runat="server" TargetControlID="txtLeaseStart" PopupButtonID="btnCalender" Format="MM/dd/yyyy"></ajax:CalendarExtender>
        </td>
    </tr>
     <tr>
        <td>
          <br /> 
        </td>
    </tr>
    <tr>
        <td class="uclabels">NETS
        </td>
        <td colspan ="2">
            <asp:TextBox ID="txtCAM" runat="server" CssClass="uctextboxMed DollarTextBox" onchange="return SetNNN();" TabIndex="28"></asp:TextBox> <b style="padding-left:10px;"> CAM</b><br style="height:0.1px"></br>
            <asp:TextBox ID="txtINS" runat="server" CssClass="uctextboxMed DollarTextBox" onchange="return SetNNN();" TabIndex="29"></asp:TextBox> <b style="padding-left:10px;"> INS</b><br style="height:0.1px"></br>
            <asp:TextBox ID="txtTAX" runat="server" CssClass="uctextboxMed DollarTextBox" onchange="return SetNNN();" TabIndex="30"></asp:TextBox> <b style="padding-left:10px;"> TAX</b><br style="height:0.1px"></br>
            <asp:TextBox ID="txtNNN" runat="server" CssClass="uctextboxMed DollarTextBox" TabIndex="31"></asp:TextBox> <b style="padding-left:10px;">NNN</b>
        </td>
       <%-- <td>
            <br />
           <b> CAM</b>
            <br />
            <br />
             <b>INS</b><br />
            <br />
             <b>TAX</b><br />
            <br />
             <b>NNN</b>
            <br />
        </td>--%>
    </tr>
     <tr>
        <td>
          <br /> 
        </td>
    </tr>
    <tr>
        <td class="uclabels">TI
        </td>
        <td colspan="2">
            <asp:TextBox runat="server" ID="txtTI" CssClass="uctextboxMed DollarTextBox" TabIndex="32" onblur="CalculateAmount()"></asp:TextBox>
            &nbsp;
            <asp:DropDownList ID="ddlTIAllowanceType" runat="server" Font-Size="Small" Width ="90px" onchange="CalculateAmount()">
                  <asp:ListItem Value="0" Text="-Select-"  />
                <asp:ListItem Value="1" Text="Per Sqft" Selected="True" />
                <asp:ListItem Value="2" Text="Aggregate" />
            </asp:DropDownList>
            <br/>
            <asp:Label runat="server" ID="lblTIAllowanceMsg"></asp:Label>
        </td>
    </tr>
     <tr>
        <td>
          <br /> 
        </td>
    </tr>
    <tr>
        <td class="uclabels">FREE RENT
        </td>
        <td>
            <asp:TextBox ID="txtFreeRent" runat="server" CssClass="uctextboxMed" TabIndex="33"></asp:TextBox>
            &nbsp;
            <asp:DropDownList ID="ddlFreeRentTime" runat="server" Font-Size="Small" Width ="90px">
                <asp:ListItem Value="1" Text="Days" />
                <asp:ListItem Value="2" Text="Months" />
                <asp:ListItem Value="3" Text="Years" />
            </asp:DropDownList>
        </td>
    </tr>
     <tr>
        <td>
          <br /> 
        </td>
    </tr>
    <tr>
        <td class="uclabels">NOTES
        </td>
        <td colspan="2">
            <asp:TextBox Rows="10" runat="server" CssClass="uctextArea" TextMode="MultiLine" Columns="35" ID="txtNotes" TabIndex="34"></asp:TextBox>
        </td>
    </tr>
</table>
<div id="dvNwTenantBuilding" style="display:none;height:380px;" class="PopupDivBodyStyle">
    <uc:NewBuyer ID="ucNwTenant" runat="server" />
</div>
