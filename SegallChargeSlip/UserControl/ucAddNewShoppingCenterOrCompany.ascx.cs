﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SegallChargeSlipBll;
using AjaxControlToolkit;

public partial class UserControl_ucAddNewShoppingCenterOrCompany : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["UserID"].ToString() == "2")
        //{
        //    trIsActive.Visible = true;
        //}
        //else
        //{
        //    trIsActive.Visible = false;
        //}
    }

    #region Properties
    //public bool  IsAdmin
    //{ get; set; }

    public Boolean IsAdmin
    {
        get { return Convert.ToBoolean(ViewState["IsAdmin"]); }
        set
        {
            ViewState["IsAdmin"] = value;
            if (value)
            {
                trOwnershipEntity.Visible = false;
            }
            else
            {
                trOwnershipEntity.Visible = true;
            }
        }
    }

    public string Title
    { get; set; }

    public string Name
    { get; set; }

    public string ClassRef
    {
        //get;
        //set;
        get { return hdnClassRef.Value; }
        set
        {

            if (value == "SC")
            {
                trAddNotes.Visible = false;

            }
            else
            {
                trAddNotes.Visible = true;
            }
        }
    }

    public string CenterName
    { get; set; }

    public Boolean IsUpdateBilled
    {
        get { return Convert.ToBoolean(ViewState["IsUpdateBilled"]); }
        set { ViewState["IsUpdateBilled"] = value; }
    }
    public Boolean IsUpdate
    {
        get { return Convert.ToBoolean(ViewState["IsUpdate"]); }
        set { ViewState["IsUpdate"] = value; }
    }

    public string PopupType
    { get { return hdnClassRef.Value; } set { hdnClassRef.Value = value; } }

    //----vv--- SK : 07/23  Shopping address  properties for  NewCharge.aspx 
    public string Address1 { get { return txtAddress1.Text; } }
    public string Address2 { get { return txtAddress2.Text; } }
    public string City { get { return txtCity.Text; } }
    public string State { get { return ddlState.SelectedValue; } }
    public string County { get { return txtCountry.Text; } }
    public string ZipCode { get { return txtZipCode.Text; } }
    // ---- SK : 08/05 : add ownership entity, Email
    public string OwnershipEntity { get { return txtOwnershipEntity.Text == "" ? "" : Convert.ToString(txtOwnershipEntity.Text); } }// { get; set; }JAY OWN not req
    public string LandlordName { get { return txtLandlordName.Text == "" ? "" : Convert.ToString(txtLandlordName.Text); } }
    public string CompanyName { get { return txtName.Text == "" ? "" : Convert.ToString(txtName.Text); } }
    public string Email { get { return txtEmail.Text == "" ? "" : Convert.ToString(txtEmail.Text); } }
    //--JAY 08/26

    public string Lattitude { get { return lbllat.Text; } }
    public string Lontitude { get { return lbllon.Text; } }

    public Boolean ISNEXT
    {
        get { return Convert.ToBoolean(ViewState["ISNEXT"]); }
        set { ViewState["ISNEXT"] = value; }
    }




    //----vv--- SK : 07/23 to hide name for adding only adress and retruning the object
    // public string IsAddressOnlyShoppingCenterName { get { return Convert.ToInt32(ViewState["IsAddressOnlyLocationId"]); } set { ViewState["IsAddressOnlyLocationId"] = value; } }
    public bool IsAddressOnly
    {
        get { return Convert.ToBoolean(ViewState["IsAddressOnly"]); }
        set
        {
            ViewState["IsAddressOnly"] = value; trName.Visible = !(value);
            // popup type = compnay
            if (hdnClassRef.Value == "C")
            {
                trAddr.Visible = true;
                if (value)
                {
                    // if address only they hide the landlord, ownership, email
                    trLandlordName.Visible = false; trOwnershipEntity.Visible = false;
                    trEmail.Visible = false;
                }
                else
                {
                    // show landlord, ownership, email
                    trLandlordName.Visible = true; trOwnershipEntity.Visible = true;
                    trEmail.Visible = true;
                }

            }
            else
            {
                trLandlordName.Visible = false; trOwnershipEntity.Visible = false;
                trEmail.Visible = false;

                if (value)
                {
                    trAddr.Visible = true;
                }
                else
                {
                    trAddr.Visible = true;

                }
            }
        }
    }
    public bool ShowDuplicateLocationsPopup
    { set { if (value) { mpDuplicateShoppingCenter.Show(); } else { mpDuplicateShoppingCenter.Hide(); } } }

    public string IsAddressSaved
    { get { return ViewState["IsAddressSaved"] == null ? "0" : Convert.ToString(ViewState["IsAddressSaved"]); } set { ViewState["IsAddressSaved"] = value; } }


    //public string ActName
    //{
    //    get { return ViewState["ActName"] == null ? "0" : Convert.ToString(ViewState["ActName"]); }
    //    set
    //    {
    //        ViewState["ActName"] = value;
    //        lblCmpName.Text = value;
    //        lblShpName.Text = value;
    //    }
    //}


    public void SetAdmin(bool IsAdmin)
    {
        if (IsAdmin == true)
        {
            trIsActive.Visible = true;
            this.IsAdmin = true;


        }
        else
        {
            trIsActive.Visible = false;
            this.IsAdmin = false;
        }
    }





    #endregion

    #region Button Events
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            //if (!string.IsNullOrEmpty(Session["UserID"].ToString()))//JAY
            if (Session[GlobleData.User] != null && Session[GlobleData.User].ToString() != "")//JAY
            {
                if (!IsAddressOnly)
                {
                    // SK : 07/23 : process Add on Shopping center/ company where the record is saved in db

                    //Perform Validations Before Insert


                    if (ValidateBeforeInsert() == false)
                    {
                        return;
                    }


                    lbErrorMsg.Text = "";
                    bool rslt;
                    rslt = false;
                    if (hdnClassRef.Value == "SC")
                    {
                        Locations objLoc = new Locations();
                        SetProperties(objLoc);
                        DataTable dt = objLoc.CheckForDuplication();
                        if (dt != null && objLoc.IsDuplicate == true)
                        {
                            if (dt.Rows[0]["IsDuplicate"].ToString().Trim() == "0")
                            {

                                SetDuplicateRecordsinGrid(dt);
                                if (!IsAddressOnly)
                                {
                                    lblShpName.Text = txtName.Text;
                                }
                                else
                                {
                                    lblShpName.Text = txtAddress1.Text;
                                }

                                //if (objLoc.IsDuplicate == true)
                                //{
                                ViewState.Add("IsDuplicate", objLoc.IsDuplicate);
                                ViewState.Add("LocationID", objLoc.LocationID);

                                //To Show Model Popup Control In another Modeol Popup
                                AjaxControlToolkit.ModalPopupExtender mpEx = (AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny");

                                mpDuplicateShoppingCenter.Show();
                            }
                            else  //JAYDUP
                            {
                                mpDuplicateShoppingCenter.Hide();
                                objLoc.LocationID = Convert.ToInt32(dt.Rows[0]["LocationID"].ToString().Trim());
                                objLoc.Load();
                                ((HiddenField)this.Parent.FindControl("hdnIsShoppingCenterAddressSaved")).Value = "1";

                                //if (objLoc.ShoppingCenter.Trim() != "")
                                //{
                                //    objLoc.Address1 = txtAddress1.Text;
                                //    objLoc.Address2 = txtAddress2.Text;
                                //}
                                //--------------------^^-------------------
                                UpdateAjaxCombo(true, objLoc);
                                ((UpdatePanel)(this.Parent.FindControl("upLocation"))).Update(); //JAY upChargeto
                                ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
                            }
                        }
                        else
                        {
                            mpDuplicateShoppingCenter.Hide();
                            if (objLoc.ShoppingCenter.Trim() != "")
                            {
                                objLoc.Address1 = "";
                                objLoc.Address2 = "";
                            }
                            rslt = objLoc.Save();

                            //------------vv-------- SK : 07/25 : to verify if shopping center address saved for Next click
                            if (rslt)
                            {
                                ((HiddenField)this.Parent.FindControl("hdnIsShoppingCenterAddressSaved")).Value = "1";
                            }
                            if (objLoc.ShoppingCenter.Trim() != "")
                            {
                                objLoc.Address1 = txtAddress1.Text;
                                objLoc.Address2 = txtAddress2.Text;
                            }
                            //--------------------^^-------------------
                            UpdateAjaxCombo(rslt, objLoc);
                            ((UpdatePanel)(this.Parent.FindControl("upLocation"))).Update();
                            ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
                            // this.Page.GetType().InvokeMember("SetLocationAddress", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
                        }

                    }
                    else if (hdnClassRef.Value == "C")
                    {

                        Company objComp = new Company();
                        if (ViewState["CompanyId"] != null)
                        {
                            objComp.CompanyID = Convert.ToInt32(ViewState["CompanyId"].ToString());
                        }

                        SetPropertiesCompany(objComp);
                        DataTable dt = new DataTable();
                        dt = objComp.CheckForDuplication();
                        //if (!IsUpdate)
                        //{
                        //    dt = objComp.CheckForDuplication();
                        //}
                        //else
                        //{
                        //    objComp.IsDuplicate = false;
                        //}
                        if (!IsUpdateBilled)
                        {
                            if (dt != null && objComp.IsDuplicate == true)
                            {

                                if (dt.Rows[0]["IsDuplicate"].ToString().Trim() == "0")
                                {
                                    // SHOW THE DUPLICATE RECORDS FOR SHOPPING CENTER ( iff the location is not saved after update or save as new)
                                    // set duplicate locations
                                    SetDuplicateRecordsinGridCompany(dt);
                                    lblCmpName.Text = txtLandlordName.Text;
                                    //lblactCmpAddr.Text = dt.Rows[0]["EnteredAddress"].ToString();
                                    lblactCmpAddr.Text = txtAddress1.Text + " " + txtAddress2.Text + " " + txtCity.Text + " " + txtZipCode.Text;
                                    // show the dulicate records modal popup and proceed save from User control                       
                                    AjaxControlToolkit.ModalPopupExtender mpEx = (AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny");
                                    mpEx.Show();
                                    mpDuplicateShoppingCenter.Hide();
                                    mpDuplicateCompany.Show();
                                }
                                else
                                {
                                    Int64 CompanyId = Convert.ToInt64(dt.Rows[0]["CompanyID"].ToString().Trim());
                                    HideDuplicatePopup(CompanyId);

                                    //updateLocNonDup(objComp);
                                }
                            }
                            else
                            {
                                updateLocNonDup(objComp);

                            }
                        }
                        else
                        {
                            updateLocNonDup(objComp);

                        }


                    }
                }
                else
                {
                    // SK : 07/23 : process Add on Shopping center/ company
                    //where the record is  NOT saved in db
                    // SK : 07/23 :  Return the location object 


                    //Only for address

                    if (hdnClassRef.Value == "SC")
                    {


                        if (ValidateBeforeInsert() == false)
                        {
                            return;
                        }

                        lbErrorMsg.Text = "";
                        bool rslt;
                        rslt = false;
                        //if (hdnClassRef.Value == "SC")
                        //{

                        txtName.Text = "";
                        Locations objLoc = new Locations();
                        SetProperties(objLoc);
                        // objLoc.ShoppingCenter = "";
                        DataTable dt = objLoc.CheckForDuplication();
                        if (dt != null && objLoc.IsDuplicate == true)
                        {
                            if (dt.Rows[0]["IsDuplicate"].ToString().Trim() == "0")
                            {
                                if (!IsAddressOnly)
                                {
                                    lblShpName.Text = txtName.Text;
                                }
                                else
                                {
                                    lblShpName.Text = txtAddress1.Text;
                                }
                                SetDuplicateRecordsinGrid(dt);
                                ViewState.Add("IsDuplicate", objLoc.IsDuplicate);
                                ViewState.Add("LocationID", objLoc.LocationID);

                                //To Show Model Popup Control In another Modeol Popup
                                AjaxControlToolkit.ModalPopupExtender mpEx = (AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny");
                                mpDuplicateShoppingCenter.Show();
                            }
                            else
                            {
                                objLoc.LocationID = Convert.ToInt32(dt.Rows[0]["LocationID"].ToString().Trim());
                                objLoc.Load();
                                ((HiddenField)this.Parent.FindControl("hdnIsShoppingCenterAddressSaved")).Value = "1";
                                ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
                                this.Page.GetType().InvokeMember("SetLocationAddress", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
                            }
                        }
                        else
                        {
                            mpDuplicateShoppingCenter.Hide();
                            rslt = objLoc.Save();
                            //------------vv-------- SK : 07/25 : to verify if shopping center address saved for Next click
                            if (rslt)
                            {
                                ((HiddenField)this.Parent.FindControl("hdnIsShoppingCenterAddressSaved")).Value = "1";
                                ///----VV--jay --   ((HiddenField)this.Parent.FindControl("hdnLocId")).Value = objLoc.LocationID.ToString();
                            }
                            //--------------------^^-------------------
                            // UpdateAjaxCombo(rslt, objLoc);
                            // ((UpdatePanel)(this.Parent.FindControl("upNewCharge"))).Update();
                            // ResetControls();
                            ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();

                            this.Page.GetType().InvokeMember("SetLocationAddress", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
                        }

                        //}

                        // Acess objLoc properties view PROPERTIES
                        //mpDuplicateShoppingCenter.Hide();
                        //((UpdatePanel)(this.Parent.FindControl("upNewCharge"))).Update();
                        //this.Page.GetType().InvokeMember("SetLocationAddress", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
                        //((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();

                    }
                    else if (hdnClassRef.Value == "C")
                    {

                        // to return the company address object
                        mpDuplicateShoppingCenter.Hide();
                        ((UpdatePanel)(this.Parent.FindControl("upChargeto"))).Update();
                        this.Page.GetType().InvokeMember("SetCompanyAddress", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
                        ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();

                    }

                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "btnAdd_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    public void HideDuplicatePopup(Int64 CompanyId)
    {
        try
        { 
        if (!IsUpdate)
        {
            mpDuplicateCompany.Hide();
            TextBox txtOwnershipnewcharge = (TextBox)this.Parent.FindControl("txtCTOwnershipEntity");
            txtOwnershipnewcharge.Text = txtOwnershipEntity.Text;

            //JP//TextBox txtComapny = (TextBox)this.Parent.FindControl("txtComapny");
            TextBox txtComapny = (TextBox)this.Parent.FindControl("txtCTName");

            HiddenField hdnCompId = (HiddenField)this.Parent.FindControl("hdnCompId");
            if (txtComapny != null)
            {
                txtComapny.Text = txtLandlordName.Text;//JP //txtName.Text;
            }
            if (hdnCompId != null)
            {
                hdnCompId.Value = Convert.ToString(CompanyId);
            }
            if (((UpdatePanel)(this.Parent.FindControl("upChargeto"))) != null)
            {
                ((UpdatePanel)(this.Parent.FindControl("upChargeto"))).Update();
            }
            ResetControls();
            if (((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")) != null)
            {
                ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
                if (!IsUpdate) this.Page.GetType().InvokeMember("SetCompanyAddress", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);

            }
        }
        else
        {
            if (this.Page.GetType().BaseType.Name == "Admin_ChargeTo")
            {
                this.Page.GetType().InvokeMember("Bindgrid", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
            }
            ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "HideDuplicatePopup", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    public void updateLocNonDup(Company objComp)
    {
        try
        { 
        if (!IsUpdate)
        {
            mpDuplicateCompany.Hide();
            // SAVE THE COMPANY 
            SaveCompanyDetails();
        }
        else
        {
            PerformDbOperationsCompany(objComp);
            ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
            //
            DataSet dsCust = objComp.GetCustInfoList();

            //Form1
            if (dsCust.Tables[0].Rows.Count > 0)
            {
                if ((Label)this.Parent.FindControl("lblCustName") != null)
                {
                    Label lbl = (Label)this.Parent.FindControl("lblCustName");
                    lbl.Text = dsCust.Tables[0].Rows[0]["Company"].ToString();
                }
                if ((Label)this.Parent.FindControl("lblContact") != null)
                {
                    Label lbl = (Label)this.Parent.FindControl("lblContact");
                    lbl.Text = dsCust.Tables[0].Rows[0]["Name"].ToString();
                }
                if ((Label)this.Parent.FindControl("lblEmail") != null)
                {
                    Label lbl = (Label)this.Parent.FindControl("lblEmail");
                    lbl.Text = dsCust.Tables[0].Rows[0]["Email"].ToString();
                }
                if ((Label)this.Parent.FindControl("lblBillingAddress1") != null)
                {
                    Label lbl = (Label)this.Parent.FindControl("lblBillingAddress1");
                    lbl.Text = dsCust.Tables[0].Rows[0]["Address1"].ToString();
                }
                if ((Label)this.Parent.FindControl("lblBillingAddress2") != null)
                {
                    Label lbl = (Label)this.Parent.FindControl("lblBillingAddress2");
                    lbl.Text = dsCust.Tables[0].Rows[0]["Address2"].ToString();
                }
                if ((Label)this.Parent.FindControl("lblBillingCity") != null)
                {
                    Label lbl = (Label)this.Parent.FindControl("lblBillingCity");
                    lbl.Text = dsCust.Tables[0].Rows[0]["City"].ToString() + ",";
                }
                if ((Label)this.Parent.FindControl("lblBillingState") != null)
                {
                    Label lbl = (Label)this.Parent.FindControl("lblBillingState");
                    lbl.Text = dsCust.Tables[0].Rows[0]["State"].ToString();
                }
                if ((Label)this.Parent.FindControl("lblBillingZip") != null)
                {
                    Label lbl = (Label)this.Parent.FindControl("lblBillingZip");
                    lbl.Text = dsCust.Tables[0].Rows[0]["ZipCode"].ToString();
                }
                if ((Label)this.Parent.FindControl("lblNotes") != null)
                {
                    Label lbl = (Label)this.Parent.FindControl("lblNotes");
                    lbl.Text = dsCust.Tables[0].Rows[0]["CompanyNotes"].ToString();
                }
                if (this.Page.GetType().BaseType.Name == "Admin_ChargeTo")
                {
                    this.Page.GetType().InvokeMember("Bindgrid", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
                }
                //lblCustName.Text = dsCust.Tables[0].Rows[0]["Company"].ToString();
                //lblContact.Text = dsCust.Tables[0].Rows[0]["Name"].ToString();
                //lblEmail.Text = dsCust.Tables[0].Rows[0]["Email"].ToString();
                //lblBillingAddress1.Text = dsCust.Tables[0].Rows[0]["Address1"].ToString();
                //lblBillingAddress2.Text = dsCust.Tables[0].Rows[0]["Address2"].ToString();
                //lblBillingCity.Text = dsCust.Tables[0].Rows[0]["City"].ToString() + ",";
                //lblBillingState.Text = dsCust.Tables[0].Rows[0]["State"].ToString();
                //lblBillingZip.Text = dsCust.Tables[0].Rows[0]["ZipCode"].ToString();
            }
            //
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "updateLocNonDup", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    private void SaveCompanyDetails()
    {
        try
        { 
        Company objComp = new Company();
        SetPropertiesCompany(objComp);
        bool rslt = objComp.Save();

        if (rslt)
        {
            if (((HiddenField)this.Parent.FindControl("hdnIsCompanySaved")) != null)
            {
                ((HiddenField)this.Parent.FindControl("hdnIsCompanySaved")).Value = "1";
            }
        }


        if (rslt == true)
        {
            TextBox txtOwnershipnewcharge = (TextBox)this.Parent.FindControl("txtCTOwnershipEntity");
            txtOwnershipnewcharge.Text = txtOwnershipEntity.Text;

            //JP//TextBox txtComapny = (TextBox)this.Parent.FindControl("txtComapny");
            TextBox txtComapny = (TextBox)this.Parent.FindControl("txtCTName");

            HiddenField hdnCompId = (HiddenField)this.Parent.FindControl("hdnCompId");
            if (txtComapny != null)
            {
                txtComapny.Text = txtLandlordName.Text;//JP //txtName.Text;
            }
            if (hdnCompId != null)
            {
                hdnCompId.Value = Convert.ToString(objComp.CompanyID);
            }
            if (((UpdatePanel)(this.Parent.FindControl("upChargeto"))) != null)
            {
                ((UpdatePanel)(this.Parent.FindControl("upChargeto"))).Update();
            }
            ResetControls();
            if (((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")) != null)
            {
                ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
                if (!IsUpdate) this.Page.GetType().InvokeMember("SetCompanyAddress", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);

            }
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "SaveCompanyDetails", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    //---------------------vv------------------------------
    protected void btnUpdate_ShoppingCenter(object sender, EventArgs e)
    {
        try
        { 
        //if (!string.IsNullOrEmpty(ViewState["LocationID"].ToString()))        //---- SK : 07/24 : removed
        //{

        //  objLoc.LocationID = Convert.ToInt32(ViewState["LocationID"].ToString());
        Locations objLoc = new Locations();
        GetTheSelectedLocation(objLoc);
        SetProperties(objLoc); //--- SK : set the porperties in master page
        bool rslt = PerformDbOperations(objLoc);

        objLoc.Address1 = txtAddress1.Text;
        objLoc.Address2 = txtAddress2.Text;
        UpdateAjaxCombo(rslt, objLoc);
        ((UpdatePanel)(this.Parent.FindControl("upLocation"))).Update();
        ((UpdatePanel)(this.Parent.FindControl("upChargeto"))).Update();
        IsAddressSaved = "0";
        //-------vv---------- SK : 07/24 : Hide the modal issue
        ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
        ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
        mpDuplicateShoppingCenter.Hide();
        mpDuplicateCompany.Hide(); // --- SK : 08/07 : duplicate company modification
        if (IsAddressOnly)
        {
            //JAY
            //((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
            //((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
            //mpDuplicateShoppingCenter.Hide();
            //mpDuplicateCompany.Hide(); // --- SK : 08/07 : duplicate company modification
            this.Page.GetType().InvokeMember("ContinueFromUpdateExistingToNextStep", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
        }
        if (ISNEXT)
        {
            HiddenField hdnIsShoppingCenterAddressSaved = (HiddenField)this.Parent.FindControl("hdnIsShoppingCenterAddressSaved");
            hdnIsShoppingCenterAddressSaved.Value = "1";


            //    ((HiddenField)this.Parent.FindControl("hdnIsShoppingCenterAddressSaved")).Value = "1"; //JAY 
            //    ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
            //    ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
            //    mpDuplicateShoppingCenter.Hide();
            //    mpDuplicateCompany.Hide(); // --- SK : 08/07 : duplicate company modification

            this.Page.GetType().InvokeMember("ContinueFromUpdateExistingToNextStep", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
        }
            //---------^^--------
            // }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "btnUpdate_ShoppingCenter", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void GetTheSelectedLocation(Locations objLocation)
    {
        SelectUpdateLocation(objLocation);
        objLocation.Load();
        objLocation.ShoppingCenter = ((TextBox)this.Parent.FindControl("txtSearchLocShoppingCenter")).Text;

    }

    public void SetDuplicateRecordsinGrid(DataTable dt)
    {

        gvDupLocations.DataSource = dt;
        gvDupLocations.DataBind();
        upDupLoc.Update();
        LblActShpAddr.Text = dt.Rows[0]["EnteredAddress"].ToString().Trim();
        mpDuplicateShoppingCenter.Show();

    }

    protected void btnAddNew_ShoppingCenter(object sender, EventArgs e)
    {
        try
        { 
        Locations objLoc = new Locations();
        objLoc.LocationID = 0;
        SetProperties(objLoc);
        bool rslt = PerformDbOperations(objLoc);
        UpdateAjaxCombo(rslt, objLoc);
        ((UpdatePanel)(this.Parent.FindControl("upLocation"))).Update();
        //-------vv---------- SK : 07/24 : Hide the modal issue
        if (IsAddressOnly)
        {
            ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
            ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
            mpDuplicateShoppingCenter.Hide();
            mpDuplicateCompany.Hide(); // --- SK : 08/07 : duplicate company modification
            this.Page.GetType().InvokeMember("ContinueFromUpdateExistingToNextStep", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
        }
        if (ISNEXT)
        {
            ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
            ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
            mpDuplicateShoppingCenter.Hide();
            mpDuplicateCompany.Hide(); // --- SK : 08/07 : duplicate company modification
            HiddenField hdnIsShoppingCenterAddressSaved = (HiddenField)this.Parent.FindControl("hdnIsShoppingCenterAddressSaved");
            hdnIsShoppingCenterAddressSaved.Value = "1";
            this.Page.GetType().InvokeMember("ContinueFromUpdateExistingToNextStep", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
        }
            //---------^^--------645
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "btnAddNew_ShoppingCenter", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    //-----------------------^^-------------------------------------


    //---------------------vv------------------------------ 08/07 : SK : Compay Duplication 
    protected void gvDupCompanys_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Style.Add("display", "none");
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Style.Add("display", "none");
            if (e.Row.RowIndex == 0)
            {
                ((RadioButton)(e.Row.FindControl("rbSelCompForUpdate"))).Checked = true;
            }
        }
    }


    protected void btnUpdate_Company(object sender, EventArgs e)
    {
        try
        { 
        //if (!string.IsNullOrEmpty(ViewState["CompanyID"].ToString()))        //---- SK : 07/24 : removed
        //{

        //  objLoc.CompanyID = Convert.ToInt32(ViewState["CompanyID"].ToString());
        Company objComp = new Company();
        GetTheSelectedCompany(objComp);
        SetPropertiesCompany(objComp);   //--- SK : set the porperties in master page
        bool rslt = PerformDbOperationsCompany(objComp);
        UpdateAjaxComboCompany(rslt, objComp);
        if (((UpdatePanel)(this.Parent.FindControl("upChargeto"))) != null)
        {
            IsAddressSaved = "0";
            ((UpdatePanel)(this.Parent.FindControl("upChargeto"))).Update();
        }
        if (!IsUpdate)
        {
            if (ISNEXT)
            {
                this.Page.GetType().InvokeMember("ContinueFromUpdateExistingToNextStep", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
            }
            //-------vv---------- SK : 07/24 : Hide the modal issue
            if (IsAddressOnly)
            {
                if (((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")) != null)
                {
                    ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
                }
                if (((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")) != null)
                {
                    ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
                }
                mpDuplicateShoppingCenter.Hide();
                mpDuplicateCompany.Hide(); // --- SK : 08/07 : duplicate company modification
                this.Page.GetType().InvokeMember("ContinueFromUpdateExistingToNextStep", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);

            }
        }
        else
        {
            if (this.Page.GetType().BaseType.Name == "Admin_ChargeTo")
            {
                this.Page.GetType().InvokeMember("Bindgrid", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
            }
            ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
        }


            //---------^^--------
            // }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "btnUpdate_Company", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void GetTheSelectedCompany(Company objCompany)
    {
        try
        { 
        SelectUpdateCompany(objCompany);
        objCompany.Load();
        if (((TextBox)(this.Parent.FindControl("txtCTOwnershipEntity"))) != null)
        {
            objCompany.OwnershipEntity = ((TextBox)(this.Parent.FindControl("txtCTOwnershipEntity"))).Text;
        }
        if (((TextBox)(this.Parent.FindControl("txtCTName"))) != null)
        {
            objCompany.Name = ((TextBox)(this.Parent.FindControl("txtCTName"))).Text;
        }
        if (((TextBox)(this.Parent.FindControl("txtComapny"))) != null)
        {
            objCompany.CompanyName = ((TextBox)(this.Parent.FindControl("txtComapny"))).Text;
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "GetTheSelectedCompany", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void SetDuplicateRecordsinGridCompany(DataTable dt)
    {
        try
        { 
        gvDupCompanys.DataSource = dt;
        gvDupCompanys.DataBind();
        upDupLoc.Update();
        mpDuplicateCompany.Show(); // --- SK : 08/07 : duplicate company modification

        if (((TextBox)(this.Parent.FindControl("txtCTAddress1"))) != null)
        {
            lblactCmpAddr.Text = lblactCmpAddr.Text + " " + ((TextBox)(this.Parent.FindControl("txtCTAddress1"))).Text;
        }
        if (((TextBox)(this.Parent.FindControl("txtCTAddress2"))) != null)
        {
            lblactCmpAddr.Text = lblactCmpAddr.Text + " " + ((TextBox)(this.Parent.FindControl("txtCTAddress2"))).Text;
        }
        if (((TextBox)(this.Parent.FindControl("txtCTCity"))) != null)
        {
            lblactCmpAddr.Text = lblactCmpAddr.Text + " " + ((TextBox)(this.Parent.FindControl("txtCTCity"))).Text;
        }
        if (((DropDownList)(this.Parent.FindControl("ddlCTState"))) != null)
        {
            lblactCmpAddr.Text = lblactCmpAddr.Text + " " + ((DropDownList)(this.Parent.FindControl("ddlCTState"))).SelectedValue;
        }
        if (((TextBox)(this.Parent.FindControl("txtCTZipCode"))) != null)
        {
            lblactCmpAddr.Text = lblactCmpAddr.Text + " " + ((TextBox)(this.Parent.FindControl("txtCTZipCode"))).Text;
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "SetDuplicateRecordsinGridCompany", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void btnAddNew_Company(object sender, EventArgs e)
    {
        try
        { 
        Company objComp = new Company();
        objComp.CompanyID = 0;
        SetPropertiesCompany(objComp);  //-- new function
        bool rslt = PerformDbOperationsCompany(objComp);  // new function
        UpdateAjaxComboCompany(rslt, objComp);
        if (((UpdatePanel)(this.Parent.FindControl("upChargeto"))) != null)
        {
            ((UpdatePanel)(this.Parent.FindControl("upChargeto"))).Update();
        }
        //-------vv---------- SK : 07/24 : Hide the modal issue
        if (IsAddressOnly)
        {

        }
        if (!IsUpdate)
        {
            ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewShoppingCenter")).Hide();
            ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
            mpDuplicateShoppingCenter.Hide();
            mpDuplicateCompany.Hide(); // --- SK : 08/07 : duplicate company modification
            this.Page.GetType().InvokeMember("ContinueFromUpdateExistingToNextStep", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
        }
        else
        {
            if (this.Page.GetType().BaseType.Name == "Admin_ChargeTo")
            {
                this.Page.GetType().InvokeMember("Bindgrid", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
            }
            ((AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny")).Hide();
        }
            //---------^^--------
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "btnAddNew_Company", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void SetPropertiesCompany(Company obj)
    {
        try
        { 
        obj.Name = txtName.Text;
        obj.Address1 = txtAddress1.Text;
        obj.Address2 = txtAddress2.Text;
        obj.City = txtCity.Text;
        obj.State = ddlState.SelectedValue;
        obj.ZipCode = txtZipCode.Text;
        obj.Email = txtEmail.Text; // SK : 08/07 : Added Email for company
        // obj.Country = txtCountry.Text; 
        if (obj.CompanyID == 0)
        {
            obj.CreatedBy = Convert.ToInt32(Session[GlobleData.User]); //Convert.ToInt32(Session["UserID"]);
            obj.CreatedOn = DateTime.Now;
        }
        else
        {
            obj.LastModifiedBy = Convert.ToInt32(Session[GlobleData.User]);// Convert.ToInt32(Session["UserID"]);
            obj.LastModifiedOn = DateTime.Now;
        }
        obj.OwnershipEntity = txtOwnershipEntity.Text; //"";
        obj.County = txtCountry.Text;
        //obj.Name = txtName.Text;
        obj.CompanyName = txtLandlordName.Text;
        if (IsAddressOnly)
        {

            // // get the landlordship entity, ownership entity and company name from parent page.
            obj.OwnershipEntity = ((TextBox)(this.Parent.FindControl("txtCTOwnershipEntity"))).Text;
            obj.CompanyName = ((TextBox)(this.Parent.FindControl("txtCTName"))).Text;
            obj.Name = ((TextBox)(this.Parent.FindControl("txtComapny"))).Text;


            //obj.ShoppingCenter = ((TextBox)(this.Parent.FindControl("txtSearchLocShoppingCenter"))).Text;
        }
        //Add Jaywanti on 06-09-2013
        obj.CompaniesNotes = txtCompanyNotes.Text.Trim();
        if (ViewState["CompaniesNotes"] != null)
        {
            if (!string.IsNullOrEmpty(ViewState["CompaniesNotes"].ToString()))
            {

                int compare = string.Compare(ViewState["CompaniesNotes"].ToString().Trim(), txtCompanyNotes.Text.Trim());
                if (compare != 0)
                {
                    obj.LastModifiedNotesBy = Convert.ToInt64(Session[GlobleData.User]);
                    obj.LastModifiedNotesOn = DateTime.Now;

                }
                else
                {
                    if (ViewState["LastModifiedNotesBy"] != null)
                    {
                        if (!string.IsNullOrEmpty(ViewState["LastModifiedNotesBy"].ToString()))
                        {
                            obj.LastModifiedNotesBy = Convert.ToInt64(ViewState["LastModifiedNotesBy"].ToString());
                        }
                    }
                    if (ViewState["LastModifiedNotesOn"] != null)
                    {
                        if (!string.IsNullOrEmpty(ViewState["LastModifiedNotesOn"].ToString()))
                        {
                            obj.LastModifiedNotesOn = Convert.ToDateTime(ViewState["LastModifiedNotesOn"]);
                        }
                    }

                }

            }
            else
            {
                obj.CompaniesNotes = txtCompanyNotes.Text.Trim();
                obj.LastModifiedNotesBy = Convert.ToInt64(Session[GlobleData.User]);
                obj.LastModifiedNotesOn = DateTime.Now;
            }
        }
        else
        {
            obj.CompaniesNotes = txtCompanyNotes.Text.Trim();
            obj.LastModifiedNotesBy = Convert.ToInt64(Session[GlobleData.User]);
            obj.LastModifiedNotesOn = DateTime.Now;
        }

        if (IsAdmin == true)
        {
            if (chkIsActive.Checked)
                obj.IsActive = true;
            else
                obj.IsActive = false;
        }
        else
        {
            obj.IsActive = true;
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "SetPropertiesCompany", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }


    private void SelectUpdateCompany(Company obj)
    {
        foreach (GridViewRow gRow in gvDupCompanys.Rows)
        {
            RadioButton rb = (RadioButton)gRow.FindControl("rbSelCompForUpdate");
            if (rb.Checked == true)
            {
                gRow.Cells[0].Style.Remove("display");
                obj.CompanyID = Convert.ToInt32(gRow.Cells[0].Text);
            }
        }
    }

    private bool PerformDbOperationsCompany(Company obj)
    {
        bool saveVal;
        //-------vv---------- SK : 07/24 : Hide the modal issue
        saveVal = obj.Save();
        if (saveVal)//if (IsAddressOnly && saveVal)
        {
            IsAddressSaved = "1";
            if (((HiddenField)this.Parent.FindControl("hdnCompId")) != null)
            {
                ((HiddenField)this.Parent.FindControl("hdnCompId")).Value = obj.CompanyID.ToString();
            }
            if (((HiddenField)this.Parent.FindControl("hdnIsCompanySaved")) != null)
            {
                ((HiddenField)this.Parent.FindControl("hdnIsCompanySaved")).Value = "1";
            }
        }
        return saveVal;
    }

    private void UpdateAjaxComboCompany(Boolean rslt, Company objLoc)
    {
        try
        { 
        if (rslt == true)
        {
            string companyname = txtName.Text;


            TextBox txtLandlordName = (TextBox)this.Parent.FindControl("txtCTName"); // landlordname
            TextBox txtCTOwnershipEntity = (TextBox)this.Parent.FindControl("txtCTOwnershipEntity");  // ownernship entity
            TextBox txtComapny = (TextBox)this.Parent.FindControl("txtComapny");  // companyname

            if (!IsAddressOnly)
            {
                //---- SK : 07/24 : Set landlordEntity, ownership identity &  company name from Parent   
                if (txtLandlordName != null)
                {
                    txtLandlordName.Text = txtLandlordName.Text;
                }
                if (txtCTOwnershipEntity != null)
                {
                    txtCTOwnershipEntity.Text = txtOwnershipEntity.Text;//txtComapny.Text;
                }
                if (txtComapny != null)
                {
                    txtComapny.Text = companyname;
                }

            }

            HiddenField hdnCompId = (HiddenField)this.Parent.FindControl("hdnCompId");
            if (hdnCompId != null)
            {
                hdnCompId.Value = Convert.ToString(objLoc.CompanyID);
            }
            objLoc.Load();


            TextBox txtCTName = (TextBox)this.Parent.FindControl("txtCTName");
            if (txtCTOwnershipEntity != null)
            {
                txtOwnershipEntity.Text = objLoc.OwnershipEntity;
            }



            TextBox txtCTAddress1 = (TextBox)this.Parent.FindControl("txtCTAddress1");
            if (txtCTAddress1 != null)
            {
                txtCTAddress1.Text = objLoc.Address1;
            }
            TextBox txtCTAddress2 = (TextBox)this.Parent.FindControl("txtCTAddress2");
            if (txtCTAddress2 != null)
            {
                txtCTAddress2.Text = objLoc.Address2;
            }
            TextBox txtCTCity = (TextBox)this.Parent.FindControl("txtCTCity");
            if (txtCTCity != null)
            {
                txtCTCity.Text = objLoc.City;
            }
            DropDownList ddlLocState = (DropDownList)this.Parent.FindControl("ddlLocState");
            if (ddlLocState != null)
            {
                ddlLocState.SelectedValue = objLoc.State;
            }
            TextBox txtCTZipCode = (TextBox)this.Parent.FindControl("txtCTZipCode");
            if (txtCTZipCode != null)
            {
                txtCTZipCode.Text = objLoc.ZipCode.ToString();
            }
            ResetControls();
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "UpdateAjaxComboCompany", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    //-----------------------^^-------------------------------------

    //protected void test(object sender, EventArgs e)
    //{
    //   // mpDuplicateShoppingCenter.TargetControlID = "test";
    //    AjaxControlToolkit.ModalPopupExtender mpEx = (AjaxControlToolkit.ModalPopupExtender)this.Parent.FindControl("mpeAddNewComapny");
    //    mpEx.Show();
    //    mpDuplicateShoppingCenter.Show();
    //}

    #endregion

    #region GridEvents

    protected void gvDupLocations_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Style.Add("display", "none");
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Style.Add("display", "none");
            if (e.Row.RowIndex == 0)
            {
                ((RadioButton)(e.Row.FindControl("rbSelLocForUpdate"))).Checked = true;
            }
        }
    }

    #endregion

    #region Custom Actions

    public void ResetControls()
    {
        Utility.FillStates(ref ddlState);
        txtName.Text = CenterName;
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtCity.Text = "";
        txtCountry.Text = "";
        txtZipCode.Text = "";
        lbName.Text = "NAME";
        // lbTitle.Text = Title;
        hdnClassRef.Value = ClassRef;
        lbllat.Text = "";
        lbllon.Text = "";
        lbErrorMsg.Text = "";
        //reset JAY
        SetAttributes();
    }

    private void SetAttributes()
    {
        //txtZipCode.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtZipCode.ClientID + "')");
    }

    private void UpdateAjaxCombo(Boolean rslt, Locations objLoc)
    {
        try
        { 
        if (rslt == true)
        {
            //AjaxControlToolkit.ComboBox ajaxcombLocShoppingCenter = (AjaxControlToolkit.ComboBox)this.Parent.FindControl("ajaxcombLocShoppingCenter");
            //CustomList objCL = new CustomList();
            // Utility.FillDropDown(ref ajaxcombLocShoppingCenter, objCL.GetLocationList(), "ShoppingCenter", "LocationID");
            // ajaxcombLocShoppingCenter.SelectedItem.Text = txtName.Text;

            TextBox txtLocation = (TextBox)this.Parent.FindControl("txtSearchLocShoppingCenter");
            if (!IsAddressOnly)
            {
                //---- SK : 07/24 : Set center 
                txtLocation.Text = txtName.Text;
                TextBox txtspaddress = (TextBox)this.Parent.FindControl("txtlocSHPaddress1");
                txtspaddress.Text = objLoc.Address1;

            }
            else
            {
                txtLocation.Text = "";
                TextBox txtLocAddress1 = (TextBox)this.Parent.FindControl("txtLocAddress1");
                txtLocAddress1.Text = objLoc.Address1;
            }

            HiddenField hdnLocId = (HiddenField)this.Parent.FindControl("hdnLocId");
            hdnLocId.Value = Convert.ToString(objLoc.LocationID);


            TextBox txtLocAddress2 = (TextBox)this.Parent.FindControl("txtLocAddress2");
            txtLocAddress2.Text = objLoc.Address2;
            TextBox txtLocCity = (TextBox)this.Parent.FindControl("txtLocCity");
            txtLocCity.Text = objLoc.City;
            DropDownList ddlLocState = (DropDownList)this.Parent.FindControl("ddlLocState");
            ddlLocState.SelectedValue = objLoc.State;
            TextBox txtLocZipCode = (TextBox)this.Parent.FindControl("txtLocZipCode");
            txtLocZipCode.Text = objLoc.ZipCode.ToString();
            //JAY update here 

            TextBox txtCounty = (TextBox)this.Parent.FindControl("txtLocCounty");
            txtCounty.Text = txtCountry.Text;


            if (objLoc.Latitude != 0 && objLoc.Longitude != 0)
            {
                ((TextBox)this.Parent.FindControl("txtMapLocation")).Text = " Lat :" + objLoc.Latitude + " Lon: " + objLoc.Longitude;
                ((HiddenField)this.Parent.FindControl("hdnMapLatLon")).Value = objLoc.Latitude + "|" + objLoc.Longitude;
            }
            else
            {
                ((TextBox)this.Parent.FindControl("txtMapLocation")).Text = " ";
                ((HiddenField)this.Parent.FindControl("hdnMapLatLon")).Value = objLoc.Address1 + " " + objLoc.Address2 + "  " + objLoc.City + " " + objLoc.State + " " + objLoc.Country + " " + objLoc.ZipCode;
            }
            ResetControls();
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "UpdateAjaxCombo", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void SetControlPropertiesLocationFromObject(Locations obj)
    {
        txtName.Text = obj.ShoppingCenter;
        txtAddress1.Text = obj.Address1;
        txtAddress2.Text = obj.Address2;
        txtCity.Text = obj.City;
        ddlState.SelectedValue = obj.State;
        txtZipCode.Text = obj.ZipCode;
        txtCountry.Text = obj.Country;

        //JAY set location from the control
        lbllat.Text = obj.Latitude.ToString();
        lbllon.Text = obj.Longitude.ToString();

        if (txtName.Text != "") { lblShpName.Text = obj.ShoppingCenter; }
        else { lblShpName.Text = obj.Address1; }
        //lblCmpName
    }

    public void SetControlPropertiesCompanyFromObject(Company obj)
    {
        try
        { 
        lblCompanyNotesModifiedby.Text = string.Empty;
        txtEmail.Text = obj.Email;
        txtOwnershipEntity.Text = obj.OwnershipEntity;
        txtLandlordName.Text = obj.CompanyName;
        txtName.Text = obj.Name;
        txtAddress1.Text = obj.Address1;
        txtAddress2.Text = obj.Address2;
        txtCity.Text = obj.City;
        ddlState.SelectedValue = obj.State;
        txtZipCode.Text = obj.ZipCode;
        //if (obj.ZipCode.ToString() == "0")
        //{
        //    txtZipCode.Text = string.Empty;
        //}
        txtCountry.Text = obj.County;
        txtCompanyNotes.Text = obj.CompaniesNotes;
        ViewState["CompaniesNotes"] = obj.CompaniesNotes;
        ViewState["LastModifiedNotesBy"] = obj.LastModifiedNotesBy;
        ViewState["LastModifiedNotesOn"] = obj.LastModifiedNotesOn;
        if (!string.IsNullOrEmpty(obj.CompaniesNotes))
        {
            Users userName = new Users();
            userName.UserID = Convert.ToInt32(obj.LastModifiedNotesBy);
            Boolean isloadedUserName = userName.Load();
            if (isloadedUserName)
            {
                lblCompanyNotesModifiedby.Text = "Last Modified by " + userName.FirstName + " " + userName.LastName + " on " + Convert.ToDateTime(obj.LastModifiedNotesOn).ToShortDateString();
            }

        }
        if (IsAdmin)
        {
            if (obj.IsActive)
            {
                chkIsActive.Checked = true;
            }
            else
            {
                chkIsActive.Checked = false;
            }
        }

        lblCmpName.Text = obj.CompanyName;
        ViewState["CompanyId"] = obj.CompanyID;
            // txtCountry.Text = obj.Country;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "SetControlPropertiesCompanyFromObject", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void SetProperties(Locations obj)
    {
        try
        { 
        obj.ShoppingCenter = txtName.Text;
        obj.Address1 = txtAddress1.Text;
        obj.Address2 = txtAddress2.Text;
        obj.City = txtCity.Text;
        obj.State = ddlState.SelectedValue;
        obj.ZipCode = txtZipCode.Text;
        obj.Country = txtCountry.Text; // SK : 08/06 : added Country
        if (obj.LocationID == 0)
        {
            obj.CreatedBy = Convert.ToInt32(Session[GlobleData.User]); //Convert.ToInt32(Session["UserID"]);
            obj.CreatedOn = DateTime.Now;
            obj.LastModifiedBy = Convert.ToInt32(Session[GlobleData.User]);// Convert.ToInt32(Session["UserID"]);
            obj.LastModifiedOn = DateTime.Now;
        }
        else
        {
            obj.LastModifiedBy = Convert.ToInt32(Session[GlobleData.User]);// Convert.ToInt32(Session["UserID"]);
            obj.LastModifiedOn = DateTime.Now;
        }

        //if (!IsAddressOnly)
        //{
        //    obj.ShoppingCenter = ((TextBox)(this.Parent.FindControl("txtSearchLocShoppingCenter"))).Text;
        //}
        //JAY
        if (IsAddressOnly)
        {
            obj.ShoppingCenter = "";
        }
        if (lbllat.Text != "" && lbllon.Text != "")
        {
            obj.Latitude = Convert.ToDecimal(lbllat.Text);
            obj.Longitude = Convert.ToDecimal(lbllon.Text);
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "SetProperties", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public bool PerformDbOperations(Locations obj)
    {
        bool saveVal;
        //-------vv---------- SK : 07/24 : Hide the modal issue
        if (obj.ShoppingCenter.Trim() != "")
        {
            obj.Address1 = "";
            obj.Address2 = "";
        }
        else
        {
            obj.ShoppingCenter = "";
        }
        saveVal = obj.Save();
        if (IsAddressOnly && saveVal)
        {
            IsAddressSaved = "1";
            ((HiddenField)this.Parent.FindControl("hdnLocId")).Value = obj.LocationID.ToString();
            ((HiddenField)this.Parent.FindControl("hdnIsShoppingCenterAddressSaved")).Value = "1";
            ((HiddenField)this.Parent.FindControl("hndIsShoppingCenterAddressChanged")).Value = "0";
        }

        return saveVal;
    }

    private void SelectUpdateLocation(Locations obj)
    {
        foreach (GridViewRow gRow in gvDupLocations.Rows)
        {
            RadioButton rb = (RadioButton)gRow.FindControl("rbSelLocForUpdate");
            if (rb.Checked == true)
            {
                gRow.Cells[0].Style.Remove("display");
                obj.LocationID = Convert.ToInt32(gRow.Cells[0].Text);
            }
        }
    }

    private Boolean ValidateBeforeInsert()
    {
        try
        { 
        //if (txtOwnershipEntity.Text == String.Empty)
        //{
        //    lbErrorMsg.Text = "Please Enter Ownership Entity";
        //    txtOwnershipEntity.Focus();
        //    return false;
        //}
        //if (txtLandlordName.Text == String.Empty)
        //{
        //    lbErrorMsg.Text = "Please Enter Landlord Trade Name ";
        //    txtLandlordName.Focus();
        //    return false;
        //}
        //if (hdnClassRef.Value == "C")
        //{
        //    if (txtLandlordName.Text.Trim() == String.Empty)
        //    {
        //        lbErrorMsg.Text = "Please Enter Landlord Trade Name";
        //        txtLandlordName.Focus();
        //        return false;
        //    }

        //}
        if (!this.IsAddressOnly)
        {
            if (hdnClassRef.Value == "C")
            {
                if (txtName.Text.Trim() == String.Empty)
                {
                    lbErrorMsg.Text = "Please Enter Name";
                    txtName.Focus();
                    return false;
                }
            }
            else
            {

                if (txtName.Text.Trim() == String.Empty)
                {
                    lbErrorMsg.Text = "Please Enter Name";
                    txtName.Focus();
                    return false;
                }
            }
        }
        else
        {
            if (txtAddress1.Text.Trim() == String.Empty)
            {
                lbErrorMsg.Text = "Please Enter Address Line 1";
                txtAddress1.Focus();
                return false;
            }
        }
        if (txtCity.Text.Trim() == String.Empty)
        {
            lbErrorMsg.Text = "Please Enter City";
            txtCity.Focus();
            return false;
        }
        //if (txtCountry.Text.Trim() == String.Empty)
        //{
        //    lbErrorMsg.Text = "Please Enter County";
        //    txtCountry.Focus();
        //    return false;
        //}
        if (ddlState.SelectedIndex == 0)
        {
            lbErrorMsg.Text = "Please Enter State";
            ddlState.Focus();
            return false;
        }
        if (txtZipCode.Text.Trim() == String.Empty)
        {
            lbErrorMsg.Text = "Please Enter Zip Code";
            txtZipCode.Focus();
            return false;
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewShoppingCenterOrCompany.ascx", "ValidateBeforeInsert", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return false;
        }
        return true;
    }
    #endregion
}