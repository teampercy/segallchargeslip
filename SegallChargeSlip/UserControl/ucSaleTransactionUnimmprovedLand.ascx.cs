﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SegallChargeSlipBll;
using System.IO;
public partial class UserControl_ucSaleTransactionUnimmprovedLand : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        { 
        if (!IsPostBack)
        {
            BindDropDown();
            SetAttributes(); //JAY

            if ((ChargeSlip)Session[GlobleData.NewChargeSlipObj] != null)
            {
                SetDataInControls();
            }
            else
            {
                ResetControls();
            }
            //pnAttachment.Visible = false;
        }
        //else
        //{
        ucNwBuyer.Title = "ADD NEW BUYER";
        ucNwBuyer.ClassRef = "B";
            //}
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucSaleTransactionUnimmprovedLand.ascx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #region Custom Actions
    private void BindDropDown()
    {
        CustomList objCL = new CustomList();
        Utility.FillDropDown(ref ddlUse, objCL.GetDealUseTypes(), "DealUseType", "DealUseId");
    }
    public void SetProperties()
    {
        try
        {
            DealTransactions objDealTransactions = new DealTransactions();
            objDealTransactions.Buyer = hdnBuyerId.Value == "" ? 0 : Convert.ToInt32(hdnBuyerId.Value);
            if (objDealTransactions.Buyer == 0)
                objDealTransactions.DealPartyCompanyName = txtBuyer.Text.Trim();
            objDealTransactions.DealUse = ddlUse.SelectedValue;
            objDealTransactions.DealUseText = ddlUse.SelectedItem.Text;
            objDealTransactions.Commission = txtCommissionPer.Text.Trim() == "" ? 0 : Convert.ToDecimal(txtCommissionPer.Text);
            // objDealTransactions.DueDate = Convert.ToDateTime(txtDueDate.Text);
            objDealTransactions.Notes = txtNotes.Text;
            objDealTransactions.SalePrice = txtSalePrice.Text.Trim() == "" ? 0 : Convert.ToDecimal(txtSalePrice.Text);
            objDealTransactions.SetFee = txtSetFee.Text.Trim() == "" ? 0 : Convert.ToDecimal(txtSetFee.Text);
            objDealTransactions.UnitValue = 1;
            objDealTransactions.UnitTypeValue = Convert.ToInt32(ddlSizeMeasure.SelectedValue);
            objDealTransactions.Size = txtSize.Text.Trim() == "" ? 0 : Convert.ToDecimal(txtSize.Text);
            objDealTransactions.DealType = 1;
            objDealTransactions.DealSubType = 2;
            Session.Add(GlobleData.DealTransactionObj, objDealTransactions);
            // DueDateCommission.SetPropDueDateWiseCommission();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucSaleTransactionUnimmprovedLand.ascx", "SetProperties", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
    }

    private void ResetControls()
    {
        txtCommissionPer.Text = "";
        //   txtDueDate.Text = "";
        txtSize.Text = "";
        txtNotes.Text = "";
        txtSalePrice.Text = "";
        txtSetFee.Text = "";
        ddlSizeMeasure.SelectedIndex = 0;
        ddlUse.SelectedIndex = 0;
        //hdnDueDatetbCount.Value = "1";

        //Buyer objBuyer = new Buyer();
        //DataSet ds = objBuyer.GetList();
        //SetAttributes(); //JAY
        // Utility.FillDropDown(ref ajaxCombBuyer, ds.Tables[0], "BuyerName", "BuyerID");
    }


    private void SetDataInControls()
    {
        try
        { 
        ChargeSlip objChargeSlip = new ChargeSlip();
        objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
        if (objChargeSlip.DealTransactionsProperty.DealSubType == 2)
        {
            txtCommissionPer.Text = String.Format("{0:f}", Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.Commission));
            txtSize.Text = String.Format("{0:f}", Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.Size)); 
            txtSalePrice.Text = String.Format("{0:f}",Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.SalePrice));
            txtSetFee.Text = String.Format("{0:f}", Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.SetFee)); 
            ddlSizeMeasure.SelectedValue = Convert.ToString(objChargeSlip.DealTransactionsProperty.UnitTypeValue);
            ddlUse.SelectedValue = objChargeSlip.DealTransactionsProperty.DealUse.ToString();
            txtNotes.Text = objChargeSlip.DealTransactionsProperty.Notes;
            DealParties objDealParties = new DealParties();
            objDealParties.PartyID = objChargeSlip.DealTransactionsProperty.Buyer;
            objDealParties.Load();
            // txtBuyer.Text = objDealParties.PartyName;
            txtBuyer.Text = objDealParties.PartyCompanyName;
            hdnBuyerId.Value = objChargeSlip.DealTransactionsProperty.Buyer.ToString();

            CommissionDueDates objCommissionDueDates = new CommissionDueDates();
            objCommissionDueDates.ChargeSlipId = objChargeSlip.ChargeSlipID;
            Session[GlobleData.CommissionTb] = objCommissionDueDates.GetListAsPerChargeSlipId();
            // DueDateCommission.GenerateTableDynamically();

            Attachments objAttachments = new Attachments();
            objAttachments.ChargeSlipID = objChargeSlip.ChargeSlipID;
            DataTable dt = objAttachments.GetDetailsByChargeSlip();


            //Remove repeted code

            DataTable dtAttachment = new DataTable();
            DataColumn dtcol = new DataColumn("AttachmentDescription", typeof(string));
            dtAttachment.Columns.Add(dtcol);
            dtcol = new DataColumn("FilePath", typeof(string));
            dtAttachment.Columns.Add(dtcol);
            dtcol = new DataColumn("UniqueAttachmentDescription", typeof(string));
            dtAttachment.Columns.Add(dtcol);
            DataRow dr;
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dRow in dt.Rows)
                    {
                        dr = dtAttachment.NewRow();
                        dr[0] = dRow["FileName"];
                        dr[1] = dRow["FilePath"];
                        dr[2] = dRow["AttachmentDescription"];
                        dtAttachment.Rows.Add(dr);
                    }
                }
            }
            //if (Session[GlobleData.Attachments] != null)
            //{
            //    dtAttachment = (DataTable)Session[GlobleData.Attachments];
            //}
            //foreach (DataRow dRow in dt.Rows)
            //{
            //    dr = dtAttachment.NewRow();
            //    dr[0] = dRow["FileName"];
            //    dr[1] = dRow["FilePath"];
            //    dr[2] = dRow["AttachmentDescription"];
            //    dtAttachment.Rows.Add(dr);
            //}

            Session[GlobleData.Attachments] = dtAttachment;
            BindGrid();
            upAtttachment.Update();
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucSaleTransactionUnimmprovedLand.ascx", "SetDataInControls", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private void SetAttributes()
    {

        txtSize.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtSize.ClientID + "')");
        txtSalePrice.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtSalePrice.ClientID + "')");
        txtCommissionPer.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtCommissionPer.ClientID + "')");
        txtSetFee.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtSetFee.ClientID + "')");
    }

    public void test()
    {
        try
        { 
        if (Session[GlobleData.cns_IsFullCommPaid] != null)
        {
            if (Convert.ToBoolean(Session[GlobleData.cns_IsFullCommPaid].ToString()))
            {
                DueDateCommission.IsFullCommPaid = true;
            }
        }
        DueDateCommission.SetAttributesDueDtCommission();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucSaleTransactionUnimmprovedLand.ascx", "test", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }


    #endregion
    protected void btnFileUpload_Click(object sender, EventArgs e)
    {
        try
        { 
        // pnAttachment.Visible = false;
        if (upFileUpload.HasFile)
        {
            // pnAttachment.Visible = true;
            HiddenField hdnPageLeave = (HiddenField)this.Parent.FindControl("hdnPageLeave");
            hdnPageLeave.Value = "0";
            string strGuid = string.Empty;
            strGuid = System.Guid.NewGuid().ToString();
            string ServerPath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
            string fileName = Path.GetFileName(upFileUpload.PostedFile.FileName);


            if (!Directory.Exists(Server.MapPath(ServerPath)))
            { Directory.CreateDirectory(Server.MapPath(ServerPath)); }

            upFileUpload.PostedFile.SaveAs(Server.MapPath(ServerPath) + strGuid + "_" + fileName);


            DataTable dtAttachment = new DataTable();
            DataColumn dtcol = new DataColumn("AttachmentDescription", typeof(string));
            dtAttachment.Columns.Add(dtcol);
            dtcol = new DataColumn("FilePath", typeof(string));
            dtAttachment.Columns.Add(dtcol);
            dtcol = new DataColumn("UniqueAttachmentDescription", typeof(string));
            dtAttachment.Columns.Add(dtcol);
            DataRow dr;
            if (Session[GlobleData.Attachments] != null)
            {
                dtAttachment = (DataTable)Session[GlobleData.Attachments];
            }
            dr = dtAttachment.NewRow();
            dr[0] = fileName;
            dr[1] = Server.MapPath(ServerPath) + strGuid + "_" + fileName;
            dr[2] = strGuid + "_" + fileName;
            dtAttachment.Rows.Add(dr);
            Session[GlobleData.Attachments] = dtAttachment;
            BindGrid();
            upAtttachment.Update();
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucSaleTransactionUnimmprovedLand.ascx", "btnFileUpload_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    protected void imgbtDelete_Click(object sender, ImageClickEventArgs e)
    {
        try
        { 
        string id = (sender as ImageButton).CommandArgument;
        //Attachments objAttachment = new Attachments();
        //objAttachment.AttachmentID = Convert.ToInt32(id);
        //objAttachment.Load();



        string Serverpath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
        string pathfile = Serverpath + id;

        string fullpath = Server.MapPath(pathfile);

        if (File.Exists(fullpath))
        {
            File.Delete(fullpath);
        }
        // objAttachment.Delete();
        // Session[GlobleData.Attachments] = null;
        //pnAttachment.Visible = false;

        DataTable dt = new DataTable();
        dt = (DataTable)Session[GlobleData.Attachments];
        DataRow[] r = dt.Select();
        for (int i = 0; i < r.Length; i++)
        {
            if (id == r[i]["UniqueAttachmentDescription"].ToString())
            {
                r[i].Delete();
            }

        }
        BindGrid();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucSaleTransactionUnimmprovedLand.ascx", "imgbtDelete_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    private void BindGrid()
    {
        try
        { 
        // string[] filePaths = Directory.GetFiles(Server.MapPath("~/Uploads/"));
        //divAttachment.Style.Add("display","block");
        //   string[] fileNames = Directory.GetFiles(Server.MapPath("~/Uploads/")).Select(path => Path.GetFileName(path))  .ToArray();
        DataTable dtGrid = (DataTable)Session[GlobleData.Attachments];
        ////List<ListItem> files = new List<ListItem>();
        ////Attachments objAttachments = new Attachments();
        ////objAttachments.ChargeSlipID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
        ////objAttachments.CreatedBy = Convert.ToInt32(Session[GlobleData.User]);
        ////dtAttachment = objAttachments.GetDetailsByUserAndChargeSlip();
        ////if (dtAttachment != null)
        ////{
        ////    for (int i = 0; i <= dtAttachment.Rows.Count - 1; i++)
        ////    {

        ////        //string filePath = dtAttachment.Rows[i]["FilePath"].ToString();
        ////        //files.Add(new ListItem(Path.GetFileName(filePath)));

        ////        string filePath = dtAttachment.Rows[i]["FilePath"].ToString();
        ////        int id = Convert.ToInt32(dtAttachment.Rows[i]["AttachmentID"].ToString());
        ////        int count = Path.GetFileName(filePath).IndexOf('_');
        ////        string filename = Path.GetFileName(filePath).Substring(count + 1);
        ////        files.Add(new ListItem(filename, Convert.ToString(id)));

        ////    }

        if (dtGrid != null && dtGrid.Rows.Count > 0)
        {
            //string attachment = dtGrid.Rows[0]["AttachmentDescription"].ToString();
            //int count = attachment.IndexOf('_');
            // dtGrid.Rows[0]["AttachmentDescription"] = attachment.Substring(count + 1).ToString();
            gvAttach.DataSource = dtGrid;
            gvAttach.DataBind();
            pnAttachment.Visible = true;
            upAtttachment.Update();
        }
        else
        {
            gvAttach.DataSource = null;
            gvAttach.DataBind();
            pnAttachment.Visible = false;
            upAtttachment.Update();
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucSaleTransactionUnimmprovedLand.ascx", "BindGrid", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }


    protected void txtBuyer_TextChanged(object sender, EventArgs e)
    {
        try
        { 
        DealParties objdealParties = new DealParties();
        if (hdnBuyerId.Value != "")
        {
            objdealParties.PartyID = Convert.ToInt32(hdnBuyerId.Value);
            objdealParties.Load();
            if (objdealParties.PartyCompanyName != txtBuyer.Text)
                hdnBuyerId.Value = "";
        }
        txtBuyer.Focus();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucSaleTransactionUnimmprovedLand.ascx", "txtBuyer_TextChanged", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
}