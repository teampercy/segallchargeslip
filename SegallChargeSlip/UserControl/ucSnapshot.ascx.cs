﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SegallChargeSlipBll;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
public partial class UserControl_ucSnapshot : System.Web.UI.UserControl
{
    private string selectedGrid = string.Empty;
    #region Properties
    public Boolean isAdmin
    {
        get
        {
            return Convert.ToBoolean(ViewState["isuseradmin"]);
        }
        set
        {
            ViewState["isuseradmin"] = value;
            dvBrokers.Visible = value;
            if (value)
            {
                trPresplitBrk.Visible = false;
                trPreSplitNextBrk.Visible = false;
                trEstimatedPostSplitBrk.Visible = false;
                trPreSplitpaidBrk.Visible = false;
                trPostSplitBrk.Visible = false;
                trAmountneededBrk.Visible = false;
                trPreSplitTotalBrk.Visible = false;
                trTotalPreSplitBrk.Visible = false;
                trTotalPreSplitOptBrk.Visible = false;
                trPreSplitRecSelect.Visible = false;
                trPostSplitSelect.Visible = false;
                trPreSplitNextYearSelect.Visible = false;
                trPostSplitNexSelect.Visible = false;
                trPreSplitpaidSelect.Visible = false;
                trPostSplitPaidSelect.Visible = false;
                trReachesToReachNextSelect.Visible = false;
                trPreSplitTotalRecSelect.Visible = false;
                trTotalPreSplitOptionConSelect.Visible = false;
                trTotalPreSplitOptionConNextSelect.Visible = false;
            }
            else
            {
                trGrossRecAdm.Visible = false;
                trGrossRecNextYearAdm.Visible = false;
                trPostSplitRecAdm.Visible = false;
                trComShareAdm.Visible = false;
                trPreSplitOptAdm.Visible = false;
                trTotalPreSplitOptAdm.Visible = false;
                trCollectedAdm.Visible = false;
                trCollectYearPostAdm.Visible = false;
                trYearTodateDeal.Visible = true;
                trPreviousYearDeal.Visible = true;
                trPreSplitRecSelect.Visible = false;
                trPostSplitSelect.Visible = false;
                trPreSplitNextYearSelect.Visible = false;
                trPostSplitNexSelect.Visible = false;
                trPreSplitpaidSelect.Visible = false;
                trPostSplitPaidSelect.Visible = false;
                trReachesToReachNextSelect.Visible = false;
                trPreSplitTotalRecSelect.Visible = false;
                trTotalPreSplitOptionConSelect.Visible = false;
                trTotalPreSplitOptionConNextSelect.Visible = false;
            }
            //if (Session["UserType"] != null)
            //{
            //    if (Session["UserType"].ToString() == "1")
            //    {
            //trRemNxttier.Visible = !(value);
            //trearningsplt.Visible = !(value);
            //trNxtearningsplt.Visible = !(value);
            //  trEstimatedPostSplit.Visible=!(value);//Commented by Jaywanti on 02-10-2015
            //    }
            //}

        }
    }

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (Session[GlobleData.User] != null)
            {
                if (!IsPostBack)
                {
                    lblPreSplitCommDueBrk.Text = "Pre Split Commission Due" + " (" + DateTime.Now.Year + ")";
                    lblPreSplitCommDueNextYearBrk.Text = "Pre Split Commission Due" + " (" + (DateTime.Now.Year + 1) + ")";
                    lblEstimatedPostSplitCommBrk.Text = "Estimated Post Split Commission Due" + " (" + DateTime.Now.Year + ")";
                    lblPreSplitpaidBrk.Text = "Pre Split Paid" + " (" + DateTime.Now.Year + ")";
                    lnkPostSplitPaidBrk.Text = "Post Split Paid" + " (" + DateTime.Now.Year + ")";
                    lnkTotalPreSplitOptConBrk.Text = "Total Pre Split Option & Contingent Receivables" + " (" + DateTime.Now.Year + ")";
                    lnkTotalPreSplitOptConNextYearBrk.Text = "Total Pre Split Option & Contingent Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
                    lnkGrossRecNextYearAdm.Text = "Gross Receivables" + " (" + DateTime.Now.Year + ")";
                    lnkPostSplitRecAdm.Text = "Post Split Receivables" + " (" + DateTime.Now.Year + ")";
                    lnkComShareRecAdm.Text = "Company Share Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
                    lnkPreSplitOptConAdm.Text = "Pre Split Options & Contingent Receivables" + " (" + DateTime.Now.Year + ")";
                    lnkPreSplitRecSelect.Text = "Pre Split Receivables" + " (" + DateTime.Now.Year + ")";
                    lnkPostSplitRecSelect.Text = "Post Split Receivables" + " (" + DateTime.Now.Year + ")";
                    lnkPreSplitRecNextYearSelect.Text = "Pre Split Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
                    lnkPostSplitRecNextYearSelect.Text = "Post Split Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
                    lnkPreSplitpaidSelect.Text = "Pre Split Paid" + " (" + DateTime.Now.Year + ")";
                    lnkPostSplitpaidSelect.Text = "Post Split Paid" + " (" + DateTime.Now.Year + ")";
                    lnkTotalPreSplitOptionConSelect.Text = "Total Pre Split Option & Contingent Receivables" + " (" + DateTime.Now.Year + ")";
                    lnkTotalPreSplitOptionConNextSelect.Text = "Total Pre Split Option & Contingent Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
                    //lblhdrecevables.Text = "Gross Receivables" + " (" + DateTime.Now.Year + ")";
                    //lblhdPaid.Text = "Gross Paid" + " (" + DateTime.Now.Year + ")";
                    //lblhdgross.Text = "Gross Commission Due" + " (" + DateTime.Now.Year + ")";
                    //lblhdEstimatedNetRec.Text = "Estimated Net Receivables" + " (" + DateTime.Now.Year + ")";
                    //lblhdNetPaid.Text = "Net Paid" + " (" + DateTime.Now.Year + ")";
                    //lbloptiontdNcontgent.Text = " Option & Contingent Gross Receivables " + " (" + DateTime.Now.Year + ")";

                    FillUserDropDown();
                    GetSummary(true);
                    Fillsnapshotdetails(true);
                }
                else
                {
                    //Commented by Jaywanti on 11-02-2016
                    //calfrom.SelectedDate = DateTime.Parse(txtdtFrom.Text);
                    //caltxtdtTo.SelectedDate = DateTime.Parse(txtdtTo.Text);

                    //if (drUsers.SelectedItem.Value.Trim() == "2")
                    //{

                    //    trRemNxttier.Visible = false;
                    //    trearningsplt.Visible = false;
                    //    trNxtearningsplt.Visible = false;
                    //    // trEstimatedPostSplit.Visible = false;//Commented by Jaywanti on 02-10-2015
                    //}
                    //else
                    //{

                    //    trRemNxttier.Visible = true;
                    //    trearningsplt.Visible = true;
                    //    trNxtearningsplt.Visible = true;
                    //    //trEstimatedPostSplit.Visible = true;//Commented by Jaywanti on 02-10-2015
                    //}
                }
                if (dvBrokers.Visible == true)
                {
                    divSummaryReceivables.Style.Add("height", "225px");
                }
                else
                {
                    divSummaryReceivables.Style.Add("height", "250px");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx", false);
            }
            // grdData.FooterRow.TableSection = TableRowSection.TableFooter; 
        }
        catch (Exception)
        {

            throw;
        }

    }


    public void FillUserDropDown()
    {
        if (Session[GlobleData.User] != null)
        {
            if (isAdmin)
            {

                Brokers brokers = new Brokers();

                DataSet dsBrokers = brokers.usp_getCoborkerslistComp(Convert.ToInt64(Session[GlobleData.User]));
                if (dsBrokers != null && dsBrokers.Tables.Count > 0 && dsBrokers.Tables[0].Rows.Count > 0)
                {
                    drUsers.DataSource = dsBrokers;
                    drUsers.DataTextField = "BrokerName";
                    drUsers.DataValueField = "UserID";
                    drUsers.DataBind();
                    // drUsers.Items.Insert(0, new ListItem("-- select -- ", Convert.ToString(Session[GlobleData.User])));

                }
            }
            else
            {
                if (Session[GlobleData.User] != null)
                {
                    drUsers.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", Convert.ToString(Session[GlobleData.User])));
                }
            }
        }
    }

    public void Fillsnapshotdetails(bool GetAll)
    {

        SegallChargeSlipBll.SnapShot snapSt = new SnapShot();
        if (GetAll)
        {
            snapSt.UserId = (int)Session[GlobleData.User];
        }
        else
        {
            snapSt.UserId = Convert.ToInt16(drUsers.SelectedItem.Value);
        }

        snapSt.IsAdmin = this.isAdmin;

        DataSet dsSnapstDetails = new DataSet();
        dsSnapstDetails = snapSt.GetSnapShotList();

        if (dsSnapstDetails != null && dsSnapstDetails.Tables.Count > 2)
        {
            gvPastdue.DataSource = dsSnapstDetails.Tables[0];
            gvPastdue.DataBind();
            if (gvPastdue.HeaderRow != null && gvPastdue.FooterRow != null)
            {
                gvPastdue.HeaderRow.TableSection = TableRowSection.TableHeader;
                gvPastdue.FooterRow.TableSection = TableRowSection.TableFooter;
                lblpastDueTotal.Text = String.Format("{0:c}", dsSnapstDetails.Tables[0].Compute("Sum(AmountDue)", ""));
            }
            dvRecentCharge.DataSource = dsSnapstDetails.Tables[1];
            dvRecentCharge.DataBind();
            if (dvRecentCharge.HeaderRow != null && dvRecentCharge.FooterRow != null)
            {
                dvRecentCharge.HeaderRow.TableSection = TableRowSection.TableHeader;
                dvRecentCharge.FooterRow.TableSection = TableRowSection.TableFooter;
                lblrecntTotal.Text = String.Format("{0:c}", dsSnapstDetails.Tables[1].Compute("Sum(AmountDue)", ""));
            }
            dvComingdue.DataSource = dsSnapstDetails.Tables[2];
            dvComingdue.DataBind();
            if (dvComingdue.HeaderRow != null && dvComingdue.FooterRow != null)
            {
                dvComingdue.HeaderRow.TableSection = TableRowSection.TableHeader;
                dvComingdue.FooterRow.TableSection = TableRowSection.TableFooter;

                lblComingDue.Text = String.Format("{0:c}", dsSnapstDetails.Tables[2].Compute("Sum(AmountDue)", ""));
            }
        }
    }
    public void GetSummary(Boolean isdefault)
    {
        try
        {
            //if (Session[GlobleData.User] != null)
            //{


            int userID = 0;
            if (isdefault)
            {
                userID = Convert.ToInt16(Session[GlobleData.User]);
                //Commented by Jaywanti on 11-02-2016
                //calfrom.SelectedDate = txtdtFrom.Text.Trim() == "" ? new DateTime(DateTime.Now.Year, 01, 01) : Convert.ToDateTime(txtdtFrom.Text.Trim());
                //caltxtdtTo.SelectedDate = txtdtTo.Text.Trim() == "" ? new DateTime(DateTime.Now.Year, 12, 31) : Convert.ToDateTime(txtdtTo.Text.Trim());
                txtdtFrom.Text = txtdtFrom.Text.Trim() == "" ? new DateTime(DateTime.Now.Year, 01, 01).ToShortDateString() : Convert.ToDateTime(txtdtFrom.Text.Trim()).ToShortDateString();
                txtdtTo.Text = txtdtTo.Text.Trim() == "" ? new DateTime(DateTime.Now.Year, 12, 31).ToShortDateString() : Convert.ToDateTime(txtdtTo.Text.Trim()).ToShortDateString();
                trPreSplitRecSelect.Visible = false;
                trPostSplitSelect.Visible = false;
                trPreSplitNextYearSelect.Visible = false;
                trPostSplitNexSelect.Visible = false;
                trPreSplitpaidSelect.Visible = false;
                trPostSplitPaidSelect.Visible = false;
                trReachesToReachNextSelect.Visible = false;
                trPreSplitTotalRecSelect.Visible = false;
                trTotalPreSplitOptionConSelect.Visible = false;
                trTotalPreSplitOptionConNextSelect.Visible = false;

                //trOPTCTN.Visible = true;
                //trTRECV.Visible = true;
            }
            else
            {
                userID = getUserID();
            }


            //if (calfrom.SelectedDate == DateTime.Now.Date && caltxtdtTo.SelectedDate == new DateTime(DateTime.Now.Year, 12, 31))
            //{
            //    trOPTCTN.Visible = true;
            //    trTRECV.Visible = true;
            //}
            //else
            //{
            //    trOPTCTN.Visible = false;
            //    trTRECV.Visible = false;
            //}

            SnapShot snapst = new SnapShot();
            snapst.UserId = userID; //Convert.ToInt32(Session[GlobleData.User].ToString());
            //Commented by Jaywanti on 11-02-2016
            //snapst.Begindate = calfrom.SelectedDate.Value;
            //snapst.EndDate = caltxtdtTo.SelectedDate.Value;
            snapst.Begindate = Convert.ToDateTime(txtdtFrom.Text);
            snapst.EndDate = Convert.ToDateTime(txtdtTo.Text);
            DataSet dsSumarry = snapst.GetRecevableSummary();
            if (dsSumarry != null && dsSumarry.Tables.Count > 0)
            {

                lbllblPreSplitCommDuesBrk.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["Recevables"]);
                lblPreSplitCommDuesNextYearBrk.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["FutureRecevables"]);

                lblPreSplitpaidsBrk.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["Gross"]);
                lblPostSplitPaidsBrk.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["Paid"]);
                lblAmountneededsBrk.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["REMNXTYR"]);
                lblPreSplitTotalRecsBrk.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["TOTATLRECEVABLES"]);
                lblTotalPreSplitOptConsBrk.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["OPTNCONT"]);
                lnkTotalPreSplitOptConNextYearsBrk.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["FutureOPTNCONTNextYear"]);

                lnkGrossRecsAdm.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["TotalGrossCommissionDue"]);
                lblGrossRecsNextYearAdm.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["GrossCommissionDue"]);
                lblPostSplitRecsAdm.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["CompanyPostSplitReceivables"]);
                lblComShareRecsAdm.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["CompanyPostSplitReceivablesFuture"]);
                lblPreSplitOptConsAdm.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["OPTNCONT"]);
                lnkTotalPreSplitOptConsAdm.Text = String.Format("{0:c}", (Convert.ToDecimal(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["OPTNCONT"]) + Convert.ToDecimal(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["FutureOPTNCONT"])));
                lblCollectedYearPreSplitsAdm.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["Gross"]);
                lblCollecYearPostSplitsAdm.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["Paid"]);
              

                lnkPreSplitRecSelects.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["Recevables"]);

                lnkPreSplitRecNextYearSelects.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["FutureRecevables"]);
                lblPostSplitRecNextYearSelects.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["CompanyPostSplitReceivablesFuture"]);
                lnkPreSplitpaidSelects.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["Gross"]);
                lnkPostSplitpaidSelects.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["Paid"]);
                //lblAmountneededBrk.Text = "Gross Commission needed to Reach Next Tier (" + dsSumarry.Tables[7].Rows[0]["EarnigspiltNext"].ToString() + ")";
                lnkAmountneededBrk.Text = "Gross Commission needed to Reach Next Tier (" + dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["EarnigspiltNext"].ToString() + ")";//Modified by Shishir 13-05-2016

                lnlReachesToReachNextSelect.Text = "Gross Commission needed to Reach Next Tier (" + dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["EarnigspiltNext"].ToString() + ")";
                lblReachesToReachNextSelect.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["REMNXTYR"]);
                lblPreSplitTotalRecSelect.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["TOTATLRECEVABLES"]);
                lblTotalPreSplitOptionConSelect.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["OPTNCONT"]);
                lblTotalPreSplitOptionConNextSelect.Text = String.Format("{0:c}", dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["FutureOPTNCONTNextYear"]);
                decimal TotalEarning = 0;
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Estimated_Post_Split_Commission_Due].Rows.Count > 0)
                {
                    for (int i = 0; i < dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Estimated_Post_Split_Commission_Due].Rows.Count; i++)
                    {
                        TotalEarning += Convert.ToDecimal(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Estimated_Post_Split_Commission_Due].Rows[i]["EstimatedPostSplitDue"]);
                    }
                }
                
                lblEstimatedPostSplitCommsBrk.Text = String.Format("{0:c}", TotalEarning);
                lblPostSplitRecSelects.Text = String.Format("{0:c}", TotalEarning);
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.CurrentYearChargeslip] != null && dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.CurrentYearChargeslip].Rows.Count > 0)
                {
                    lblYearToDateDeals.Text = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.CurrentYearChargeslip].Rows[dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.CurrentYearChargeslip].Rows.Count - 1]["TotalNoOfChargeslipCurrentYear"].ToString();
                }
                else
                {
                    lblYearToDateDeals.Text = "0";
                }
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PreviousYearChargeslip] != null && dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PreviousYearChargeslip].Rows.Count > 0)
                {
                    lblPreviousYearDeal.Text = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PreviousYearChargeslip].Rows[dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PreviousYearChargeslip].Rows.Count - 1]["TotalNoOfChargeslipPreviousYear"].ToString();
                }
                else
                {
                    lblPreviousYearDeal.Text = "0";
                }
                //lblYearToDateDeals.Text = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["YearTodateDeal"].ToString();
                //lblPreviousYearDeal.Text = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["PreviousYearDeal"].ToString();
                Session["SummaryDetail"] = dsSumarry;

                //lblrecevables.Text = String.Format("{0:c}", dsSumarry.Tables[0].Rows[0]["Recevables"]);
                //lblEstimatedNetRec.Text = String.Format("{0:c}", dsSumarry.Tables[0].Rows[0]["EstimatedPostSplitReceivables"]);
                //lblpaid.Text = String.Format("{0:c}", dsSumarry.Tables[0].Rows[0]["Gross"]);
                //lblgross.Text = String.Format("{0:c}", dsSumarry.Tables[0].Rows[0]["GrossCommissionDue"]);
                //lblNetPaid.Text = String.Format("{0:c}", dsSumarry.Tables[0].Rows[0]["Paid"]);
                //lbloptionNcontgent.Text = String.Format("{0:c}", dsSumarry.Tables[0].Rows[0]["OPTNCONT"]);
                //lbltotalrecevables.Text = String.Format("{0:c}", dsSumarry.Tables[0].Rows[0]["TOTATLRECEVABLES"]);
                //lblTotaloptionNcontgent.Text = String.Format("{0:c}", dsSumarry.Tables[0].Rows[0]["TOTATLOPTCONTRECEVABLES"]);
                //lblRemNxtYr.Text = String.Format("{0:c}", dsSumarry.Tables[0].Rows[0]["REMNXTYR"]);
                //lblEarningSplit.Text = dsSumarry.Tables[0].Rows[0]["EARNINGSPLIT"].ToString();
                //lblNxtEarningSplit.Text = (dsSumarry.Tables[0].Rows[0]["EarnigspiltNext"].ToString().Trim() == "0") ? dsSumarry.Tables[0].Rows[0]["EarnigspiltNext"].ToString() : dsSumarry.Tables[0].Rows[0]["EarnigspiltNext"].ToString().Trim();
                //lblEstimatedPostSplitReceivables.Text = String.Format("{0:c}", dsSumarry.Tables[0].Rows[0]["EstimatedTotalPostSplitReceivables"]);
                //lblEstNetTotalOptConRec.Text = String.Format("{0:c}", dsSumarry.Tables[0].Rows[0]["EstimatedOPTCONTPostSplitReceivables"]);
            }

        }
        catch (Exception ex)
        {


        }
    }

    public int getUserID()
    {
        int userId = 0;
        if (Session[GlobleData.User] != null)
        {
            if (this.isAdmin)
            {
                userId = Convert.ToInt16(drUsers.SelectedItem.Value);
                if (userId != Convert.ToInt32(Session["UserId"]))
                {
                    trPreSplitRecSelect.Visible = true;
                    trPostSplitSelect.Visible = true;
                    trPreSplitNextYearSelect.Visible = true;
                    // trPostSplitNexSelect.Visible = true;
                    trPreSplitpaidSelect.Visible = true;
                    trPostSplitPaidSelect.Visible = true;
                    trReachesToReachNextSelect.Visible = true;
                    trPreSplitTotalRecSelect.Visible = true;
                    trTotalPreSplitOptionConSelect.Visible = true;
                    trTotalPreSplitOptionConNextSelect.Visible = true;

                }
                else
                {
                    trPreSplitRecSelect.Visible = false;
                    trPostSplitSelect.Visible = false;
                    trPreSplitNextYearSelect.Visible = false;
                    trPostSplitNexSelect.Visible = false;
                    trPreSplitpaidSelect.Visible = false;
                    trPostSplitPaidSelect.Visible = false;
                    trReachesToReachNextSelect.Visible = false;
                    trPreSplitTotalRecSelect.Visible = false;
                    trTotalPreSplitOptionConSelect.Visible = false;
                    trTotalPreSplitOptionConNextSelect.Visible = false;

                }
            }
            else
            {
                userId = Convert.ToInt16(Session[GlobleData.User]);
                trPreSplitRecSelect.Visible = false;
                trPostSplitSelect.Visible = false;
                trPreSplitNextYearSelect.Visible = false;
                trPostSplitNexSelect.Visible = false;
                trPreSplitpaidSelect.Visible = false;
                trPostSplitPaidSelect.Visible = false;
                trReachesToReachNextSelect.Visible = false;
                trPreSplitTotalRecSelect.Visible = false;
                trTotalPreSplitOptionConSelect.Visible = false;
                trTotalPreSplitOptionConNextSelect.Visible = false;
            }
        }
        else
        {
            //session out
        }
        return userId;
    }

    public DataTable getdummytable()
    {

        DataTable dt = new DataTable();
        dt.Columns.Add("C1");
        dt.Columns.Add("C2");
        dt.Columns.Add("C3");
        dt.Columns.Add("C4");
        dt.Columns.Add("C5");
        dt.Columns.Add("C6");
        dt.Columns.Add("C7");

        DataRow dr;

        for (int i = 0; i < 100; i++)
        {
            dr = dt.NewRow();
            dr[0] = "" + i;
            dr[1] = "C2" + i;
            dr[2] = "23/12/201" + i;
            dr[3] = "CHARGE TO " + i;
            dr[4] = "25/1/210" + i;
            dr[5] = "8756" + i + "0.00";
            dr[6] = "ID_" + i;
            dt.Rows.Add(dr);
        }
        return dt;

    }

    //[System.Web.Services.WebMethod(EnableSession = true)]
    //public static void Getrecivable()
    //{
    //    //int UserId = HttpContext.Current.Session[GlobleData.User];
    //    return getdummytable();
    //}

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        if (drUsers.SelectedItem.Value.Trim() == "2")
        {
            GetSummary(true);
            //trRemNxttier.Visible = false;
            //trearningsplt.Visible = false;
            //trNxtearningsplt.Visible = false;
            // trEstimatedPostSplit.Visible = false;//Commented by Jaywanti on 02-10-2015
        }
        else
        {
            GetSummary(false);
            //trRemNxttier.Visible = true;
            //trearningsplt.Visible = true;
            //trNxtearningsplt.Visible = true;
            // trEstimatedPostSplit.Visible = true;//Commented by Jaywanti on 02-10-2015
        }
        string DateFrom = txtdtFrom.Text.ToString().Trim();
        string DateTo = txtdtTo.Text.ToString().Trim();
        DateTime datefrom = Convert.ToDateTime(txtdtFrom.Text.ToString().Trim());
        DateTime dateto = Convert.ToDateTime(txtdtTo.Text.ToString().Trim());
        if (dateto.Year != DateTime.Now.Year || datefrom.Year != DateTime.Now.Year)
        {
            lblPreSplitCommDueBrk.Text = "Pre Split Commission Due" + " (" + datefrom.Year + ")";
            lblPreSplitCommDueNextYearBrk.Text = "Pre Split Commission Due" + " (" + (datefrom.Year + 1) + ")";
            lblEstimatedPostSplitCommBrk.Text = "Estimated Post Split Commission Due" + " (" + datefrom.Year + ")";
            lblPreSplitpaidBrk.Text = "Pre Split Paid" + " (" + datefrom.Year + ")";
            lnkPostSplitPaidBrk.Text = "Post Split Paid" + " (" + datefrom.Year + ")";
            lnkTotalPreSplitOptConBrk.Text = "Total Pre Split Option & Contingent Receivables" + " (" + datefrom.Year + ")";
            lnkTotalPreSplitOptConNextYearBrk.Text = "Total Pre Split Option & Contingent Receivables" + " (" + (datefrom.Year + 1) + ")";
            lnkGrossRecNextYearAdm.Text = "Gross Receivables" + " (" + datefrom.Year + ")";
            lnkPostSplitRecAdm.Text = "Post Split Receivables" + " (" + datefrom.Year + ")";
            lnkComShareRecAdm.Text = "Company Share Receivables" + " (" + (datefrom.Year + 1) + ")";
            lnkPreSplitOptConAdm.Text = "Pre Split Options & Contingent Receivables" + " (" + datefrom.Year + ")";
            lnkPreSplitRecSelect.Text = "Pre Split Receivables" + " (" + datefrom.Year + ")";
            lnkPostSplitRecSelect.Text = "Post Split Receivables" + " (" + datefrom.Year + ")";
            lnkPreSplitRecNextYearSelect.Text = "Pre Split Receivables" + " (" + (datefrom.Year + 1) + ")";
            lnkPostSplitRecNextYearSelect.Text = "Post Split Receivables" + " (" + (datefrom.Year + 1) + ")";
            lnkPreSplitpaidSelect.Text = "Pre Split Paid" + " (" + datefrom.Year + ")";
            lnkPostSplitpaidSelect.Text = "Post Split Paid" + " (" + datefrom.Year + ")";
            lnkTotalPreSplitOptionConSelect.Text = "Total Pre Split Option & Contingent Receivables" + " (" + datefrom.Year + ")";
            lblTotalPreSplitOptionConSelect.Text = "Total Pre Split Option & Contingent Receivables" + " (" + (datefrom.Year + 1) + ")";
            //lblhdrecevables.Text = "Gross Receivables";
            //lblhdPaid.Text = "Gross Paid" ;
            //lblhdgross.Text = "Gross Commission Due";
            //lblhdNetPaid.Text = "Net Paid" ;
            //lbloptiontdNcontgent.Text = "Option & Contingent Gross Receivables";
            //lblhdEstimatedNetRec.Text = "Estimated Net Receivables";

        }
        else
        {
            lblPreSplitCommDueBrk.Text = "Pre Split Commission Due" + " (" + DateTime.Now.Year + ")";
            lblPreSplitCommDueNextYearBrk.Text = "Pre Split Commission Due" + " (" + (DateTime.Now.Year + 1) + ")";
            lblEstimatedPostSplitCommBrk.Text = "Estimated Post Split Commission Due" + " (" + DateTime.Now.Year + ")";
            lblPreSplitpaidBrk.Text = "Pre Split Paid" + " (" + DateTime.Now.Year + ")";
            lnkPostSplitPaidBrk.Text = "Post Split Paid" + " (" + DateTime.Now.Year + ")";
            lnkTotalPreSplitOptConBrk.Text = "Total Pre Split Option & Contingent Receivables" + " (" + DateTime.Now.Year + ")";
            lnkTotalPreSplitOptConNextYearBrk.Text = "Total Pre Split Option & Contingent Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
            lnkGrossRecNextYearAdm.Text = "Gross Receivables" + " (" + DateTime.Now.Year + ")";
            lnkPostSplitRecAdm.Text = "Post Split Receivables" + " (" + DateTime.Now.Year + ")";
            lnkComShareRecAdm.Text = "Company Share Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
            lnkPreSplitOptConAdm.Text = "Pre Split Options & Contingent Receivables" + " (" + DateTime.Now.Year + ")";
            lnkPreSplitRecSelect.Text = "Pre Split Receivables" + " (" + DateTime.Now.Year + ")";
            lnkPostSplitRecSelect.Text = "Post Split Receivables" + " (" + DateTime.Now.Year + ")";
            lnkPreSplitRecNextYearSelect.Text = "Pre Split Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
            lnkPostSplitRecNextYearSelect.Text = "Post Split Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
            lnkPreSplitpaidSelect.Text = "Pre Split Paid" + " (" + DateTime.Now.Year + ")";
            lnkPostSplitpaidSelect.Text = "Post Split Paid" + " (" + DateTime.Now.Year + ")";
            lnkTotalPreSplitOptionConSelect.Text = "Total Pre Split Option & Contingent Receivables" + " (" + DateTime.Now.Year + ")";
            lblTotalPreSplitOptionConSelect.Text = "Total Pre Split Option & Contingent Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
            //lblhdrecevables.Text = "Gross Receivables" + " (" + DateTime.Now.Year + ")";
            //lblhdPaid.Text = "Gross Paid" + " (" + DateTime.Now.Year + ")";
            //lblhdgross.Text = "Gross Commission Due" + " (" + DateTime.Now.Year + ")";
            //lblhdNetPaid.Text = "Net Paid" + " (" + DateTime.Now.Year + ")";
            //lbloptiontdNcontgent.Text = "Option & Contingent Gross Receivables " + " (" + DateTime.Now.Year + ")";
            //lblhdEstimatedNetRec.Text = "Estimated Net Receivables " + " (" + DateTime.Now.Year + ")";
        }
        GetSummary(false);
        if (isAdmin)
        {
            Fillsnapshotdetails(false);
        }
        txtdtFrom.Text = DateFrom;
        txtdtTo.Text = DateTo;
        //Commented by Jaywanti on 11-02-2016
        //calfrom.SelectedDate = Convert.ToDateTime(txtdtFrom.Text.ToString().Trim());
        //caltxtdtTo.SelectedDate = Convert.ToDateTime(txtdtTo.Text.ToString().Trim());
        ScriptManager.RegisterStartupScript(updsnapsht, updsnapsht.GetType(), "locpopup2", " applyscrollable(); ", true);

    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        txtdtFrom.Text = (new DateTime(DateTime.Now.Year, 01, 01).ToShortDateString());// DateTime.Now.Date.ToShortDateString();
        txtdtTo.Text = (new DateTime(DateTime.Now.Year, 12, 31).ToShortDateString());
        lblPreSplitCommDueBrk.Text = "Pre Split Commission Due" + " (" + DateTime.Now.Year + ")";
        lblPreSplitCommDueNextYearBrk.Text = "Pre Split Commission Due" + " (" + (DateTime.Now.Year + 1) + ")";
        lblEstimatedPostSplitCommBrk.Text = "Estimated Post Split Commission Due" + " (" + DateTime.Now.Year + ")";
        lblPreSplitpaidBrk.Text = "Pre Split Paid" + " (" + DateTime.Now.Year + ")";
        lnkPostSplitPaidBrk.Text = "Post Split Paid" + " (" + DateTime.Now.Year + ")";
        lnkTotalPreSplitOptConBrk.Text = "Total Pre Split Option & Contingent Receivables" + " (" + DateTime.Now.Year + ")";
        lnkTotalPreSplitOptConNextYearBrk.Text = "Total Pre Split Option & Contingent Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
        lnkGrossRecNextYearAdm.Text = "Gross Receivables" + " (" + DateTime.Now.Year + ")";
        lnkPostSplitRecAdm.Text = "Post Split Receivables" + " (" + DateTime.Now.Year + ")";
        lnkComShareRecAdm.Text = "Company Share Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
        lnkPreSplitOptConAdm.Text = "Pre Split Options & Contingent Receivables" + " (" + DateTime.Now.Year + ")";
        lnkPreSplitRecSelect.Text = "Pre Split Receivables" + " (" + DateTime.Now.Year + ")";
        lnkPostSplitRecSelect.Text = "Post Split Receivables" + " (" + DateTime.Now.Year + ")";
        lnkPreSplitRecNextYearSelect.Text = "Pre Split Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
        lnkPostSplitRecNextYearSelect.Text = "Post Split Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
        lnkPreSplitpaidSelect.Text = "Pre Split Paid" + " (" + DateTime.Now.Year + ")";
        lnkPostSplitpaidSelect.Text = "Post Split Paid" + " (" + DateTime.Now.Year + ")";
        lnkTotalPreSplitOptionConSelect.Text = "Total Pre Split Option & Contingent Receivables" + " (" + DateTime.Now.Year + ")";
        lnkTotalPreSplitOptionConNextSelect.Text = "Total Pre Split Option & Contingent Receivables" + " (" + (DateTime.Now.Year + 1) + ")";
        //calfrom.SelectedDate = txtdtFrom.Text.Trim() == "" ? DateTime.Now.Date : Convert.ToDateTime(txtdtFrom.Text.Trim());
        // caltxtdtTo.SelectedDate = txtdtTo.Text.Trim() == "" ? new DateTime(DateTime.Now.Year, 12, 31) : Convert.ToDateTime(txtdtTo.Text.Trim());
        GetSummary(true);
        if (isAdmin)
        {
            GetSummary(false);
            Fillsnapshotdetails(false);
        }

        ScriptManager.RegisterStartupScript(updsnapsht, updsnapsht.GetType(), "locpopup1", " applyscrollable(); ", true);

    }

    protected void drUsers_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (drUsers.SelectedItem.Value.Trim() == "2")
        {
            GetSummary(true);
            //trRemNxttier.Visible = false;
            //trearningsplt.Visible = false;
            //trNxtearningsplt.Visible = false;
            // trEstimatedPostSplit.Visible = false;//Commented by Jaywanti on 02-10-2015
        }
        else
        {
            GetSummary(false);
            //trRemNxttier.Visible = true;
            //trearningsplt.Visible = true;
            //trNxtearningsplt.Visible = true;
            //trEstimatedPostSplit.Visible = true;//Commented by Jaywanti on 02-10-2015
        }
        Fillsnapshotdetails(false);
        ScriptManager.RegisterStartupScript(updsnapsht, updsnapsht.GetType(), "locpopup3", "applyscrollable(); ", true);

    }
    protected void dvComingdue_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            hdnPopupFromGrid.Value = "0";
            //divChargeDetailCard.Visible = true;
            ucChargeDetailCard1.ChargeSlipId = Convert.ToInt32(e.CommandArgument);
            ucChargeDetailCard1.Bindata();
            hdnPopup.Value = "1";
            if (this.isAdmin)
            {
                int userId = Convert.ToInt16(drUsers.SelectedItem.Value);
                if (userId != Convert.ToInt32(Session["UserId"]))
                {
                    trPreSplitRecSelect.Visible = true;
                    trPostSplitSelect.Visible = true;
                    trPreSplitNextYearSelect.Visible = true;
                    //  trPostSplitNexSelect.Visible = true;
                    trPreSplitpaidSelect.Visible = true;
                    trPostSplitPaidSelect.Visible = true;
                    trReachesToReachNextSelect.Visible = true;
                    trPreSplitTotalRecSelect.Visible = true;
                    trTotalPreSplitOptionConSelect.Visible = true;
                    trTotalPreSplitOptionConNextSelect.Visible = true;
                }
                else
                {
                    trPreSplitRecSelect.Visible = false;
                    trPostSplitSelect.Visible = false;
                    trPreSplitNextYearSelect.Visible = false;
                    trPostSplitNexSelect.Visible = false;
                    trPreSplitpaidSelect.Visible = false;
                    trPostSplitPaidSelect.Visible = false;
                    trReachesToReachNextSelect.Visible = false;
                    trPreSplitTotalRecSelect.Visible = false;
                    trTotalPreSplitOptionConSelect.Visible = false;
                    trTotalPreSplitOptionConNextSelect.Visible = false;
                }
            }
            //ScriptManager.RegisterStartupScript(this, Page.GetType(), "display", "display();", true);
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "display", "display();", true);
        }
    }
    protected void lblhdgross_Click(object sender, EventArgs e)
    {
        grdReceivables.DataSource = null;
        grdReceivables.DataBind();
        grdEarning.DataSource = null;
        grdEarning.DataBind();
        grdPayment.DataSource = null;
        grdPayment.DataBind();
        grdFutureRec.DataSource = null;
        grdFutureRec.DataBind();
        //btnExporttoPDF.Visible = false;
        //btnExportToExcel.Visible = false;
        grdReceivables.Visible = false;
        grdFutureRec.Visible = false;
        grdPayment.Visible = false;
        grdEarning.Visible = false;
        DataSet dsSumarry = null;
        int index = Convert.ToInt32(hdnModalPopup.Value);
        if ((index == 6) || (index == 24))
        {
            grdFutureRec.Columns[3].Visible = false;
            grdFutureRec.Columns[4].Visible = false;
            grdFutureRec.Columns[6].Visible = false;
            divNextSplitDetailSummary.Visible = true;
           
        }
        else
        {
            grdFutureRec.Columns[3].Visible = true;
            grdFutureRec.Columns[4].Visible = true;
            grdFutureRec.Columns[6].Visible = true;
            divNextSplitDetailSummary.Visible = false;
        }
        if (Session["SummaryDetail"] != null)
        {
            dsSumarry = (DataSet)Session["SummaryDetail"];
        }
        else
        {
            bool isdefault = false;
            if (drUsers.SelectedItem.Value.Trim() == "2")
            {
                isdefault = true;
            }
            else
            {
                isdefault = false;
            }
            //Commented by Jaywanti on 11-02-2016
            //calfrom.SelectedDate = txtdtFrom.Text.Trim() == "" ? new DateTime(DateTime.Now.Year, 01, 01) : Convert.ToDateTime(txtdtFrom.Text.Trim());
            //caltxtdtTo.SelectedDate = txtdtTo.Text.Trim() == "" ? new DateTime(DateTime.Now.Year, 12, 31) : Convert.ToDateTime(txtdtTo.Text.Trim());

            txtdtFrom.Text = txtdtFrom.Text.Trim() == "" ? new DateTime(DateTime.Now.Year, 01, 01).ToShortDateString() : Convert.ToDateTime(txtdtFrom.Text.Trim()).ToShortDateString();
            txtdtTo.Text = txtdtTo.Text.Trim() == "" ? new DateTime(DateTime.Now.Year, 12, 31).ToShortDateString() : Convert.ToDateTime(txtdtTo.Text.Trim()).ToShortDateString();
            int userID = 0;
            if (isdefault)
            {
                userID = Convert.ToInt16(Session[GlobleData.User]);         
            }
            else
            {
                userID = getUserID();
            }
            SnapShot snapst = new SnapShot();
            snapst.UserId = userID; //Convert.ToInt32(Session[GlobleData.User].ToString());
            //Commented by Jaywanti on 11-02-2016
            //snapst.Begindate = calfrom.SelectedDate.Value;
            //snapst.EndDate = caltxtdtTo.SelectedDate.Value;
            snapst.Begindate =Convert.ToDateTime(txtdtFrom.Text);
            snapst.EndDate =Convert.ToDateTime(txtdtTo.Text);
            dsSumarry = snapst.GetRecevableSummary();
        }
      
        if (dsSumarry.Tables.Count > 0)
        {
            if (index == 1)
            {
                lblCHARGEDETAIL.Text = "Pre Split Commission Due";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PAST_DUE] != null)
                {
                    grdReceivables.Visible = true;
                    grdReceivables.DataSource = null;
                    grdReceivables.DataBind();
                    DataTable dtAll = new DataTable();
                    dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PAST_DUE]);
                    dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Base_Term_Current_Year]);
                    grdReceivables.DataSource = dtAll;
                    grdReceivables.DataBind();                   
                    selectedGrid = grdReceivables.ClientID.ToString();
                }
            }
            else if (index == 2)
            {
                lblCHARGEDETAIL.Text = "Pre Split Commission Due";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.POSTsplit_Receivables_for_future_due] != null)
                {
                    grdReceivables.Visible = true;
                    grdReceivables.DataSource = null;
                    grdReceivables.DataBind();
                    grdReceivables.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.POSTsplit_Receivables_for_future_due];
                    grdReceivables.DataBind();                   
                    selectedGrid = grdReceivables.ClientID.ToString();
                }
            }
            else if (index == 3)
            {
                lblCHARGEDETAIL.Text = "Estimated Post Split Commission Due";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Estimated_Post_Split_Commission_Due] != null)
                {
                    grdEarning.Visible = true;
                    grdEarning.DataSource = null;
                    grdEarning.DataBind();
                    grdEarning.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Estimated_Post_Split_Commission_Due];
                    grdEarning.DataBind();                   
                    selectedGrid = grdEarning.ClientID.ToString();
                }
            }
            else if (index == 4)
            {
                lblCHARGEDETAIL.Text = "Pre Split Paid";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Payments_Current_year] != null)
                {
                    grdPayment.Visible = true;
                    grdPayment.DataSource = null;
                    grdPayment.DataBind();
                    grdPayment.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Payments_Current_year];
                    grdPayment.Columns[8].Visible = false;
                    grdPayment.Columns[9].Visible = false;
                    grdPayment.Columns[10].Visible = false;
                    grdPayment.DataBind();                 
                    selectedGrid = grdPayment.ClientID.ToString();
                }
            }
            else if (index == 5)
            {
                lblCHARGEDETAIL.Text = "Post Split Paid";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Payments_Current_year] != null)
                {
                    grdPayment.Visible = true;
                    grdPayment.DataSource = null;
                    grdPayment.DataBind();
                    grdPayment.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Payments_Current_year];
                    grdPayment.Columns[8].Visible = true;
                    grdPayment.Columns[9].Visible = true;
                    grdPayment.Columns[10].Visible = true;
                    grdPayment.DataBind();
                    btnExporttoPDF.Visible = true;
                    btnExportToExcel.Visible = true;                  
                    selectedGrid = grdPayment.ClientID.ToString();
                }
            }
            else if (index == 6)
            {
                lblCHARGEDETAIL.Text = "Gross Commission needed to Reach Next Tier";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Gross_Commission_needed_to_Reach_Next_Tier] != null)
                {
                    grdFutureRec.Visible = true;
                    grdFutureRec.DataSource = null;
                    grdFutureRec.DataBind();
                    grdFutureRec.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Gross_Commission_needed_to_Reach_Next_Tier];
                    grdFutureRec.DataBind();                   
                    selectedGrid = grdFutureRec.ClientID.ToString();
                }
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table] != null)
                {
                    if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows.Count > 0)
                    {
                        lblCurrentSplitRange.Text = "$" + String.Format("{0:n2}", Convert.ToDecimal(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["RangeFromCurrent"].ToString())) + " - $" + String.Format("{0:n2}", Convert.ToDecimal(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["RangeToCurrent"].ToString()));
                        lblCurrentSplitPercent.Text = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["EARNINGSPLIT"].ToString();
                        lblEarningNextRange.Text = "$" + String.Format("{0:n2}", Convert.ToDecimal(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["RangeFromNext"].ToString() == "" ? "0" : dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["RangeFromNext"].ToString())) + " - $" + String.Format("{0:n2}", Convert.ToDecimal(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["RangeToNext"].ToString() == "" ? "0" : dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["RangeToNext"].ToString()));
                        lblEarningNextSplitPercent.Text = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["EarnigspiltNext"].ToString();
                    }
                 
                }
            }
            else if (index == 7)
            {
                lblCHARGEDETAIL.Text = "Pre Split Total Receivables";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PAST_DUE] != null)
                {
                    grdReceivables.Visible = true;
                    grdReceivables.DataSource = null;
                    grdReceivables.DataBind();
                    DataTable dtAll = new DataTable();
                    dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PAST_DUE]);
                    dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Base_Term_Current_Year]);
                    dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.POSTsplit_Receivables_for_future_due]);
                    grdReceivables.DataSource = dtAll;
                    grdReceivables.DataBind();
                    grdReceivables.ClientID.ToString();                   
                    selectedGrid = grdReceivables.ClientID.ToString();
                }
              
            }
            else if (index == 8)
            {
                lblCHARGEDETAIL.Text = "Total Pre Split Option & Contingent Receivables";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.OPT_Contingent_for_Currentyear_and_PastDue] != null)
                {
                    grdFutureRec.Visible = true;
                    grdFutureRec.DataSource = null;
                    grdFutureRec.DataBind();
                    grdFutureRec.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.OPT_Contingent_for_Currentyear_and_PastDue];
                    grdFutureRec.DataBind();                    
                    selectedGrid = grdFutureRec.ClientID.ToString();
                }
            }
            else if (index == 9)
            {
                lblCHARGEDETAIL.Text = "Total Pre Split Option & Contingent Receivables";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.OPTION_COntin_NetPOSTsplit_Receivables_for_future_due] != null)
                {
                    grdFutureRec.Visible = true;
                    grdFutureRec.DataSource = null;
                    grdFutureRec.DataBind();
                    grdFutureRec.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.OPTION_COntin_NetPOSTsplit_Receivables_for_future_due];
                    grdFutureRec.DataBind();
                    selectedGrid = grdFutureRec.ClientID.ToString();
                }
            }
            else if (index == 10)
            {
                lblCHARGEDETAIL.Text = "Gross Receivables";
                grdReceivables.Visible = true;
                grdReceivables.DataSource = null;
                grdReceivables.DataBind();
                DataTable dtAll = new DataTable();
                dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.GrossReceivables_1]);
                dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.GrossReceivables_2]);
                dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.GrossReceivables_3]);
                grdReceivables.DataSource = dtAll;
                grdReceivables.DataBind();
                selectedGrid = grdReceivables.ClientID.ToString();
            }
            else if (index == 11)
            {
                lblCHARGEDETAIL.Text = "Gross Receivables";
                grdReceivables.Visible = true;
                grdReceivables.DataSource = null;
                grdReceivables.DataBind();
                DataTable dtAll = new DataTable();
                dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.GrossReceivables_1]);
                dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.GrossReceivables_2]);               
                grdReceivables.DataSource = dtAll;
                grdReceivables.DataBind();
                selectedGrid = grdReceivables.ClientID.ToString();
            }
            else if (index == 12)
            {
                lblCHARGEDETAIL.Text = "Post Split Receivables";
                grdFutureRec.Visible = true;
                grdFutureRec.DataSource = null;
                grdFutureRec.DataBind();
                DataTable dtAll = new DataTable();
                dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.CompanyPostSplit_1]);
                dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.CompanyPostSplit_2]);
                grdFutureRec.DataSource = dtAll;
                grdFutureRec.DataBind();
                selectedGrid = grdFutureRec.ClientID.ToString();
            }
            else if (index == 13)
            {
                lblCHARGEDETAIL.Text = "Company Share Receivables";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.CompanyPostSplitFuture] != null)
                {
                    grdFutureRec.Visible = true;
                    grdFutureRec.DataSource = null;
                    grdFutureRec.DataBind();
                    grdFutureRec.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.CompanyPostSplitFuture];
                    grdFutureRec.DataBind();
                    selectedGrid = grdFutureRec.ClientID.ToString();
                }
            }
            else if (index == 14)
            {
                lblCHARGEDETAIL.Text = "Pre Split Options & Contingent Receivables";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Pre_Split_Options_Contingent_Receivables] != null)
                {
                    grdFutureRec.Visible = true;
                    grdFutureRec.DataSource = null;
                    grdFutureRec.DataBind();
                    grdFutureRec.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Pre_Split_Options_Contingent_Receivables];
                    grdFutureRec.DataBind();
                    selectedGrid = grdFutureRec.ClientID.ToString();
                }
            }
            else if (index == 15)
            {
                lblCHARGEDETAIL.Text = "Total Pre Split Options & Contingent Receivables";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Pre_Split_Options_Contingent_Receivables] != null || dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Total_Pre_Split_Options_Contingent_Receivables] != null)
                {
                    grdFutureRec.Visible = true;
                    grdFutureRec.DataSource = null;
                    grdFutureRec.DataBind();
                    DataTable dtAll = new DataTable();
                    dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Pre_Split_Options_Contingent_Receivables]);
                    dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Total_Pre_Split_Options_Contingent_Receivables]);
                    grdFutureRec.DataSource = dtAll;
                    grdFutureRec.DataBind();
                    selectedGrid = grdFutureRec.ClientID.ToString();
                }
            }
            else if (index == 16)
            {
                lblCHARGEDETAIL.Text = "Collected Year to Date - Pre Split";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.GrossPaid_NetPaid] != null)
                {
                    grdPayment.Visible = true;
                    grdPayment.DataSource = null;
                    grdPayment.DataBind();
                    grdPayment.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.GrossPaid_NetPaid];
                    grdPayment.DataBind();
                    selectedGrid = grdPayment.ClientID.ToString();
                }
            }
            else if (index == 17)
            {
                lblCHARGEDETAIL.Text = "Collected Year to Date - Post Split";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.GrossPaid_NetPaid] != null)
                {
                    grdPayment.Visible = true;
                    grdPayment.DataSource = null;
                    grdPayment.DataBind();
                    grdPayment.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.GrossPaid_NetPaid];
                    grdPayment.DataBind();
                    selectedGrid = grdPayment.ClientID.ToString();
                }
            }
            else if (index == 18)
            {
                lblCHARGEDETAIL.Text = "Pre Split Receivables";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PAST_DUE] != null)
                {
                    grdReceivables.Visible = true;
                    grdReceivables.DataSource = null;
                    grdReceivables.DataBind();
                    DataTable dtAll = new DataTable();
                    dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PAST_DUE]);
                    dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Base_Term_Current_Year]);
                    grdReceivables.DataSource = dtAll;
                    grdReceivables.DataBind();                   
                    selectedGrid = grdReceivables.ClientID.ToString();
                }
            }
            else if (index == 19)
            {
                lblCHARGEDETAIL.Text = "Post Split Receivables";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Estimated_Post_Split_Commission_Due] != null)
                {
                    grdEarning.Visible = true;
                    grdEarning.DataSource = null;
                    grdEarning.DataBind();
                    grdEarning.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Estimated_Post_Split_Commission_Due];
                    grdEarning.DataBind();                   
                    selectedGrid = grdEarning.ClientID.ToString();
                }
            }
            else if (index == 20)
            {
                lblCHARGEDETAIL.Text = "Pre Split Receivables";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.POSTsplit_Receivables_for_future_due] != null)
                {
                    grdReceivables.Visible = true;
                    grdReceivables.DataSource = null;
                    grdReceivables.DataBind();
                    grdReceivables.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.POSTsplit_Receivables_for_future_due];
                    grdReceivables.DataBind();                  
                    selectedGrid = grdReceivables.ClientID.ToString();
                }
            }
            else if (index == 21)
            {
                lblCHARGEDETAIL.Text = "Post Split Receivables";
            }
            else if (index == 22)
            {
                lblCHARGEDETAIL.Text = "Pre Split Paid";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Payments_Current_year] != null)
                {
                    grdPayment.Visible = true;
                    grdPayment.DataSource = null;
                    grdPayment.DataBind();
                    grdPayment.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Payments_Current_year];
                    grdPayment.Columns[8].Visible = false;
                    grdPayment.Columns[9].Visible = false;
                    grdPayment.Columns[10].Visible = false;
                    grdPayment.DataBind();                  
                    selectedGrid = grdPayment.ClientID.ToString();
                    
                }
            }
            else if (index == 23)
            {
                lblCHARGEDETAIL.Text = "Post Split Paid";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Payments_Current_year] != null)
                {
                    grdPayment.Visible = true;
                    grdPayment.DataSource = null;
                    grdPayment.DataBind();
                    grdPayment.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Payments_Current_year];
                    grdPayment.Columns[8].Visible = true;
                    grdPayment.Columns[9].Visible = true;
                    grdPayment.Columns[10].Visible = true;
                    grdPayment.DataBind();
                    btnExporttoPDF.Visible = true;
                    btnExportToExcel.Visible = true;                  
                    selectedGrid = grdPayment.ClientID.ToString();
                }
            }
            else if (index == 24)
            {
                lblCHARGEDETAIL.Text = "Gross Commission needed to Reach Next Tier";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Gross_Commission_needed_to_Reach_Next_Tier] != null)
                {
                    grdFutureRec.Visible = true;
                    grdFutureRec.DataSource = null;
                    grdFutureRec.DataBind();
                    grdFutureRec.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Gross_Commission_needed_to_Reach_Next_Tier];
                    grdFutureRec.DataBind();                   
                    selectedGrid = grdFutureRec.ClientID.ToString();
                }
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table] != null)
                {
                    if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows.Count > 0)
                    {
                        lblCurrentSplitRange.Text = "$" + String.Format("{0:n2}", Convert.ToDecimal(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["RangeFromCurrent"].ToString())) + " - $" + String.Format("{0:n2}", Convert.ToDecimal(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["RangeToCurrent"].ToString()));
                        lblCurrentSplitPercent.Text = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["EARNINGSPLIT"].ToString();
                        lblEarningNextRange.Text = "$" + String.Format("{0:n2}", Convert.ToDecimal(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["RangeFromNext"].ToString() == "" ? "0" : dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["RangeFromNext"].ToString())) + " - $" + String.Format("{0:n2}", Convert.ToDecimal(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["RangeToNext"].ToString() == "" ? "0" : dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["RangeToNext"].ToString()));
                        lblEarningNextSplitPercent.Text = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.MainSummary_Table].Rows[0]["EarnigspiltNext"].ToString();
                    }

                }
            }
            else if (index == 25)
            {
                lblCHARGEDETAIL.Text = "Pre Split Total Receivables";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PAST_DUE] != null)
                {
                    grdReceivables.Visible = true;
                    grdReceivables.DataSource = null;
                    grdReceivables.DataBind();
                    DataTable dtAll = new DataTable();
                    dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PAST_DUE]);
                    dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.Base_Term_Current_Year]);
                    dtAll.Merge(dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.POSTsplit_Receivables_for_future_due]);
                    grdReceivables.DataSource = dtAll;
                    grdReceivables.DataBind();                  
                    selectedGrid = grdReceivables.ClientID.ToString();
                }

            }
            else if (index == 26)
            {
                lblCHARGEDETAIL.Text = "Total Pre Split Option & Contingent Receivables";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.OPT_Contingent_for_Currentyear_and_PastDue] != null)
                {
                    grdFutureRec.Visible = true;
                    grdFutureRec.DataSource = null;
                    grdFutureRec.DataBind();
                    grdFutureRec.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.OPT_Contingent_for_Currentyear_and_PastDue];
                    grdFutureRec.DataBind();                   
                    selectedGrid = grdFutureRec.ClientID.ToString();
                }
            }
            else if (index == 27)
            {
                lblCHARGEDETAIL.Text = "Total Pre Split Option & Contingent Receivables";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.OPTION_COntin_NetPOSTsplit_Receivables_for_future_due] != null)
                {
                    grdFutureRec.Visible = true;
                    grdFutureRec.DataSource = null;
                    grdFutureRec.DataBind();
                    grdFutureRec.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.OPTION_COntin_NetPOSTsplit_Receivables_for_future_due];
                    grdFutureRec.DataBind();
                    selectedGrid = grdFutureRec.ClientID.ToString();
                }
            }
            else if (index == 28)
            {
                lblCHARGEDETAIL.Text = "Total Transactions Submitted (Current Year)";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.CurrentYearChargeslip] != null)
                {
                    grdPayment.Visible = true;
                    grdPayment.DataSource = null;
                    grdPayment.DataBind();
                    grdPayment.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.CurrentYearChargeslip];
                    grdPayment.DataBind();
                    selectedGrid = grdPayment.ClientID.ToString();
                }
            }
            else if (index == 29)
            {
                lblCHARGEDETAIL.Text = "Total Transactions Submitted (Previous Year)";
                if (dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PreviousYearChargeslip] != null)
                {
                    grdPayment.Visible = true;
                    grdPayment.DataSource = null;
                    grdPayment.DataBind();
                    grdPayment.DataSource = dsSumarry.Tables[(int)GlobleData.SnapShotDetailTable.PreviousYearChargeslip];
                    grdPayment.DataBind();
                    selectedGrid = grdPayment.ClientID.ToString();
                }
            }
            if (grdReceivables.Rows.Count != 0)
            {
                grdReceivables.HeaderRow.TableSection = TableRowSection.TableHeader;
                grdReceivables.FooterRow.TableSection = TableRowSection.TableFooter;
            }
            else if (grdPayment.Rows.Count != 0)
            {
                grdPayment.HeaderRow.TableSection = TableRowSection.TableHeader;
                grdPayment.FooterRow.TableSection = TableRowSection.TableFooter;
            }
            else if (grdFutureRec.Rows.Count != 0)
            {
                grdFutureRec.HeaderRow.TableSection = TableRowSection.TableHeader;
                grdFutureRec.FooterRow.TableSection = TableRowSection.TableFooter;
            }
            else if (grdEarning.Rows.Count != 0)
            {
                grdEarning.HeaderRow.TableSection = TableRowSection.TableHeader;
                grdEarning.FooterRow.TableSection = TableRowSection.TableFooter;
            }
        }
        
        ScriptManager.RegisterStartupScript(updsnapsht, updsnapsht.GetType(), "locpopup1", " applyscrollable(); ", true);
        ScriptManager.RegisterStartupScript(updsnapsht, updsnapsht.GetType(), "DataTable", "BindDataTable(" + selectedGrid + ");", true);
        ModalDetail.Show();
        if (this.isAdmin)
        {
            int userId = Convert.ToInt16(drUsers.SelectedItem.Value);
            if (userId != Convert.ToInt32(Session["UserId"]))
            {
                trPreSplitRecSelect.Visible = true;
                trPostSplitSelect.Visible = true;
                trPreSplitNextYearSelect.Visible = true;
              //  trPostSplitNexSelect.Visible = true;
                trPreSplitpaidSelect.Visible = true;
                trPostSplitPaidSelect.Visible = true;
                trReachesToReachNextSelect.Visible = true;
                trPreSplitTotalRecSelect.Visible = true;
                trTotalPreSplitOptionConSelect.Visible = true;
                trTotalPreSplitOptionConNextSelect.Visible = true;
            }
            else
            {
                trPreSplitRecSelect.Visible = false;
                trPostSplitSelect.Visible = false;
                trPreSplitNextYearSelect.Visible = false;
                trPostSplitNexSelect.Visible = false;
                trPreSplitpaidSelect.Visible = false;
                trPostSplitPaidSelect.Visible = false;
                trReachesToReachNextSelect.Visible = false;
                trPreSplitTotalRecSelect.Visible = false;
                trTotalPreSplitOptionConSelect.Visible = false;
                trTotalPreSplitOptionConNextSelect.Visible = false;
            }
        }
        //else
        //{
        //    userId = Convert.ToInt16(Session[GlobleData.User]);
           
        //}
    }
    decimal totalGrossAmount = 0, totalPreSplitAmount = 0, totalGrosspaid = 0, totalEstimatedPostSplitReceivables = 0, totalPreSplitOutstanding=0;
    protected void grdReceivables_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int index = Convert.ToInt32(hdnModalPopup.Value);
        if (e.Row.RowType == DataControlRowType.Header)
        {
            if (index == 10 || index == 11)
            {
                grdReceivables.Columns[3].Visible = false;
                grdReceivables.Columns[4].Visible = false;
                grdReceivables.Columns[5].Visible = false;
                grdReceivables.Columns[6].Visible = false;
                grdReceivables.Columns[7].Visible = false;
            }
            else
            {
                grdReceivables.Columns[3].Visible = true;
                grdReceivables.Columns[4].Visible = true;
                grdReceivables.Columns[5].Visible = true;
                grdReceivables.Columns[6].Visible = true;
                grdReceivables.Columns[7].Visible = true;
            }
            //else
            //{
            //    grdReceivables.Columns[8].Visible = false;
            //    grdReceivables.Columns[9].Visible = false;
            //    grdPayment.Columns[10].Visible = false;
            //}
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (DataBinder.Eval(e.Row.DataItem, "CommAmountDue") != DBNull.Value)
                totalGrossAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CommAmountDue"));
            if (DataBinder.Eval(e.Row.DataItem, "PreSplitComm") != DBNull.Value)
                totalPreSplitAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PreSplitComm"));
            if (DataBinder.Eval(e.Row.DataItem, "GrossPaid") != DBNull.Value)
                totalGrosspaid += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "GrossPaid"));
            if (DataBinder.Eval(e.Row.DataItem, "EstimatedPostSplitReceivables") != DBNull.Value)
                totalEstimatedPostSplitReceivables += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "EstimatedPostSplitReceivables"));
            if (DataBinder.Eval(e.Row.DataItem, "PreSplitOutstanding") != DBNull.Value)
                totalPreSplitOutstanding += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PreSplitOutstanding"));

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblFooCommAmount = (Label)e.Row.FindControl("lblFooCommAmount");
            lblFooCommAmount.Text = "$" + String.Format("{0:n2}", totalGrossAmount);
            Label lblFooPreSplitAmount = (Label)e.Row.FindControl("lblFooPreSplitAmount");
            lblFooPreSplitAmount.Text = "$" + String.Format("{0:n2}", totalPreSplitAmount);
            Label lblFooPaidAmount = (Label)e.Row.FindControl("lblFooPaidAmount");
            lblFooPaidAmount.Text = "$" + String.Format("{0:n2}", totalGrosspaid);
            Label lblFooPostSplit = (Label)e.Row.FindControl("lblFooPostSplit");
            lblFooPostSplit.Text = "$" + String.Format("{0:n2}", totalEstimatedPostSplitReceivables);
            Label lblFooPreSplitOutstanding = (Label)e.Row.FindControl("lblFooPreSplitOutstanding");
            lblFooPreSplitOutstanding.Text = "$" + String.Format("{0:n2}", totalPreSplitOutstanding);
        }
    }
    decimal TotPreSplitPaid = 0, TotPreSplitOutstanding = 0, TotEstimatedPostSplitDue = 0;
    protected void grdEarning_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (DataBinder.Eval(e.Row.DataItem, "PreSplitPaid") != DBNull.Value)
                TotPreSplitPaid += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PreSplitPaid"));
            if (DataBinder.Eval(e.Row.DataItem, "PreSplitOutstanding") != DBNull.Value)
                TotPreSplitOutstanding += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PreSplitOutstanding"));
            if (DataBinder.Eval(e.Row.DataItem, "EstimatedPostSplitDue") != DBNull.Value)
                TotEstimatedPostSplitDue += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "EstimatedPostSplitDue"));
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblFooPreSplipaid = (Label)e.Row.FindControl("lblFooPreSplipaid");
            lblFooPreSplipaid.Text = "$" + String.Format("{0:n2}", TotPreSplitPaid);
            Label lblFooPreSplitOut = (Label)e.Row.FindControl("lblFooPreSplitOut");
            lblFooPreSplitOut.Text = "$" + String.Format("{0:n2}", TotPreSplitOutstanding);
            Label lblFooEstPostSplitDue = (Label)e.Row.FindControl("lblFooEstPostSplitDue");
            lblFooEstPostSplitDue.Text = "$" + String.Format("{0:n2}", TotEstimatedPostSplitDue);
        }
    }
    decimal TotGrossPaid = 0, TotalNetPaid = 0;
    protected void grdPayment_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int index = Convert.ToInt32(hdnModalPopup.Value);
        if (e.Row.RowType == DataControlRowType.Header)
        {
            grdPayment.Columns[1].Visible = true;
            grdPayment.Columns[2].Visible = true;
            grdPayment.Columns[3].Visible = true;
            grdPayment.Columns[4].Visible = true;
            grdPayment.Columns[5].Visible = true;
            grdPayment.Columns[6].Visible = true;
            grdPayment.Columns[7].Visible = true;
            grdPayment.Columns[8].Visible = false;
            grdPayment.Columns[9].Visible = false;
            grdPayment.Columns[10].Visible = false;
            if ((index == 5) || (index == 23))
            {
                grdPayment.Columns[8].Visible = true;
                grdPayment.Columns[9].Visible = true;
                grdPayment.Columns[10].Visible = true;
            }
            else if ((index == 16) || (index == 17))
            {
                e.Row.Cells[4].Text = "DUE DATE";               
                grdPayment.Columns[3].Visible = false;
                grdPayment.Columns[6].Visible = false;
            }
            else if ((index == 28) || (index == 29))
            {
                grdPayment.Columns[3].Visible = false;
                grdPayment.Columns[4].Visible = false;
                grdPayment.Columns[5].Visible = false;
                grdPayment.Columns[6].Visible = false;
                grdPayment.Columns[7].Visible = false;
            }
            else
            {
                e.Row.Cells[4].Text = "PAYMENT DATE";
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
           
            if (DataBinder.Eval(e.Row.DataItem, "Grosspaid") != DBNull.Value)
                TotGrossPaid += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Grosspaid"));
            if (DataBinder.Eval(e.Row.DataItem, "NetPaid") != DBNull.Value)
                TotalNetPaid += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "NetPaid"));

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblFooPaidAmount = (Label)e.Row.FindControl("lblFooPaidAmount");
            lblFooPaidAmount.Text = "$" + String.Format("{0:n2}", TotGrossPaid);
            Label lblFooNetPaid = (Label)e.Row.FindControl("lblFooNetPaid");
            lblFooNetPaid.Text = "$" + String.Format("{0:n2}", TotalNetPaid);
        }
    }
    decimal totalFutGrossAmount = 0, totalFutPreSplitAmount = 0, totalFutGrosspaid = 0, totalFutEstimatedPostSplitReceivables = 0;
    protected void grdFutureRec_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int index = Convert.ToInt32(hdnModalPopup.Value);
        if (e.Row.RowType == DataControlRowType.Header)
        {
            grdFutureRec.Columns[5].Visible = true;
            e.Row.Cells[6].Text = "Post Split";     
            if ((index == 12) || (index == 13))
            {
                grdFutureRec.Columns[5].Visible = false;
                e.Row.Cells[6].Text = "Company Post Split";               
            }
            else if (index == 14 || index == 15)
            {                
                e.Row.Cells[6].Text = "Net Amount Due";                
            }           
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (DataBinder.Eval(e.Row.DataItem, "CommAmountDue") != DBNull.Value)
                totalFutGrossAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CommAmountDue"));
            if (DataBinder.Eval(e.Row.DataItem, "PreSplitComm") != DBNull.Value)
                totalFutPreSplitAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PreSplitComm"));
            if (DataBinder.Eval(e.Row.DataItem, "GrossPaid") != DBNull.Value)
                totalFutGrosspaid += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "GrossPaid"));
            if (DataBinder.Eval(e.Row.DataItem, "EstimatedPostSplitReceivables") != DBNull.Value)
                totalFutEstimatedPostSplitReceivables += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "EstimatedPostSplitReceivables"));

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblFooCommAmount = (Label)e.Row.FindControl("lblFooCommAmount");
            lblFooCommAmount.Text = "$" + String.Format("{0:n2}", totalFutGrossAmount);
            Label lblFooPreSplitAmount = (Label)e.Row.FindControl("lblFooPreSplitAmount");
            lblFooPreSplitAmount.Text = "$" + String.Format("{0:n2}", totalFutPreSplitAmount);
            Label lblFooPaidAmount = (Label)e.Row.FindControl("lblFooPaidAmount");
            lblFooPaidAmount.Text = "$" + String.Format("{0:n2}", totalFutGrosspaid);
            Label lblFooPostSplit = (Label)e.Row.FindControl("lblFooPostSplit");
            lblFooPostSplit.Text = "$" + String.Format("{0:n2}", totalFutEstimatedPostSplitReceivables);
        }
    }
    protected void imgHelpAndSupport_Click(object sender, ImageClickEventArgs e)
    {
        int index = Convert.ToInt32(hdnModalPopup.Value);
        if (index == 1)
        {
            lblTitle.Text = "Pre Split Commission Due";
            lblMessageHelpAndSupport.Text = "Sum of all pre-split commissions due to the broker for the current year (1/1 to 12/31) plus any outstanding commissions due from previous years.";
        }
        else if (index == 2)
        {
            lblTitle.Text = "Pre Split Commission Due";
             lblMessageHelpAndSupport.Text = "Sum of all pre-split commissions due to the broker in the next calendar year (1/2 to 12/31).";
        }
        else if (index == 3)
        {
            lblTitle.Text = "Estimated Post Split Commission Due";
            lblMessageHelpAndSupport.Text = "Estimated total of all outstanding post-split commissions due in the current year for the broker calculated using the split threshold tiers entered by Admin.";
        }
        else if (index == 4)
        {
            lblTitle.Text = "Pre Split Paid";
            lblMessageHelpAndSupport.Text = "Sum of all pre-split commissions paid to the broker in the current year.";
        }
        else if (index == 5)
        {
            lblTitle.Text = "Post Split Paid";
            lblMessageHelpAndSupport.Text = "Sum of all post-split commissions paid to the broker in the current year (regardless of when the payment was stated to be due in the charge slip; can include charges from previous years or future years).";
        }
        else if (index == 6)
        {
            lblTitle.Text = "Amount Needed to Reach Next Tier";
            lblMessageHelpAndSupport.Text = "Pre-split commission amount needed to reach the next split threshold tier [on 1/1 split automatically resets to 50:50].";
        }
        else if (index == 7)
        {
            lblTitle.Text = "Pre Split Total Receivables";
            lblMessageHelpAndSupport.Text = "Sum of all outstanding pre-split commission totals due for the current year plus all future years, inclusive of the Broker, Company share, and any Co-broker, Master Broker , and Outside Broker share.";
        }
        else if (index == 8)
        {
            lblTitle.Text = "Total Pre Split Option & Contingent Receviables";
            lblMessageHelpAndSupport.Text = "Sum of all outstanding pre-split Option & Contingent lease commission totals due for the current year including any outstanding totals from previous years, inclusive of the Broker, Company share, and any Co-broker, Master Broker , and Outside Broker share.";
        }
        else if (index == 9)
        {
            lblTitle.Text = "Total Pre Split Option & Contingent Receviables";
            lblMessageHelpAndSupport.Text = "Sum of all outstanding pre-split Option & Contingent lease commission totals due for the next calendar year, inclusive of the Broker, Company share, and any Co-broker, Master Broker , and Outside Broker share.";
        }
        else if (index == 10)
        {
            lblTitle.Text = "Gross Receivables";
            lblMessageHelpAndSupport.Text = "Sum of all outstanding gross commission totals due to the company for the current year plus future years, including any outstanding balances from previous years.";
        }
        else if (index == 11)
        {
            lblTitle.Text = "Gross Receivables";
            lblMessageHelpAndSupport.Text = "Sum of all outstanding gross commission totals due to the company for the current year including any outstanding balances from previous years.";
        }
        else if (index == 12)
        {
            lblTitle.Text = "Post Split Receivables";
            lblMessageHelpAndSupport.Text = "Sum of all post-split company share commission totals due for the current year plus any outstanding balances from previous years.";
        }
        else if (index == 13)
        {
            lblTitle.Text = "Company Share Receivables";
            lblMessageHelpAndSupport.Text = "Sum of all post-split company share commission totals due for the next calendar year.";
        }
        else if (index == 14)
        {
            lblTitle.Text = "Pre Split Options & Contingent Receivables";
            lblMessageHelpAndSupport.Text = "Sum of all pre-split Option & Contingent lease commission totals for the current year plus any outstanding totals from previous years.";
        }
        else if (index == 15)
        {
            lblTitle.Text = "Total Pre Split Options & Contingent Receivables";
            lblMessageHelpAndSupport.Text = "Sum of all pre-split Option & Contingent lease commission totals for the current year plus future years, including any outstanding totals from previous years.";
        }
        else if (index == 16)
        {
            lblTitle.Text = "Collected Year to Date - Pre Split";
            lblMessageHelpAndSupport.Text = "Sum of all pre-split commission totals paid in the current year.";
        }
        else if (index == 17)
        {
            lblTitle.Text = "Collected Year to Date - Post Split";
            lblMessageHelpAndSupport.Text = "Sum of al post-split company share commission totals paid in the current year.";
        }
        else if (index == 18)
        {
            lblTitle.Text = "Pre Split Receivables";
            lblMessageHelpAndSupport.Text = "Sum of all outstanding pre-split commission totals due for the current year plus any outstanding total from previous years.";
        }
        else if (index == 19)
        {
            lblTitle.Text = "Post Split Receivables";
            lblMessageHelpAndSupport.Text = "Sum of all outstanding post-split commission totals due for the current year plus any outstanding totals from previous years (at current split level).";
        }
        else if (index == 20)
        {
            lblTitle.Text = "Pre Split Receivables";
            lblMessageHelpAndSupport.Text = "Sum of all outstanding pre-split commission totals due for the next calendar year.";
        }
        else if (index == 21)
        {
            lblTitle.Text = "Post Split Receivables";
            lblMessageHelpAndSupport.Text = "Sum of all outstanding post-split commission totals due for the next calendar year (calculated at 50:50 split level).";
        }
        else if (index == 22)
        {
            lblTitle.Text = "Pre Split Paid";
            lblMessageHelpAndSupport.Text = "Sum of all pre-split commission totals paid to the broker in the current year.";
        }
        else if (index == 23)
        {
            lblTitle.Text = "Post Split Paid";
            lblMessageHelpAndSupport.Text = "Sum of all post-split commission totals paid to the broker in the current year.";
        }
        modalHelpAndSupport.Show();
      
    }
    protected void btnClose_Click(object sender, ImageClickEventArgs e)
    {
        if (this.isAdmin)
        {
            int userId = Convert.ToInt16(drUsers.SelectedItem.Value);
            if (userId != Convert.ToInt32(Session["UserId"]))
            {
                trPreSplitRecSelect.Visible = true;
                trPostSplitSelect.Visible = true;
                trPreSplitNextYearSelect.Visible = true;
                //  trPostSplitNexSelect.Visible = true;
                trPreSplitpaidSelect.Visible = true;
                trPostSplitPaidSelect.Visible = true;
                trReachesToReachNextSelect.Visible = true;
                trPreSplitTotalRecSelect.Visible = true;
                trTotalPreSplitOptionConSelect.Visible = true;
                trTotalPreSplitOptionConNextSelect.Visible = true;
            }
            else
            {
                trPreSplitRecSelect.Visible = false;
                trPostSplitSelect.Visible = false;
                trPreSplitNextYearSelect.Visible = false;
                trPostSplitNexSelect.Visible = false;
                trPreSplitpaidSelect.Visible = false;
                trPostSplitPaidSelect.Visible = false;
                trReachesToReachNextSelect.Visible = false;
                trPreSplitTotalRecSelect.Visible = false;
                trTotalPreSplitOptionConSelect.Visible = false;
                trTotalPreSplitOptionConNextSelect.Visible = false;
            }
        }
        ScriptManager.RegisterStartupScript(updsnapsht, updsnapsht.GetType(), "locpopup2", " applyscrollable(); ", true);
    }
    protected void grdPayment_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ViewChargeslip")
        {
            hdnPopupFromGrid.Value = "1";
            //Session["ChargeSlipId"] = Convert.ToInt32(e.CommandArgument);
            //ScriptManager.RegisterStartupScript(updsnapsht, updsnapsht.GetType(), "chargeSlipIdNavigate", " Navigate(); ", true);
            ucChargeDetailCard1.ChargeSlipId = Convert.ToInt32(e.CommandArgument);
            ucChargeDetailCard1.Bindata();
            hdnPopup.Value = "1";
            if (this.isAdmin)
            {
                int userId = Convert.ToInt16(drUsers.SelectedItem.Value);
                if (userId != Convert.ToInt32(Session["UserId"]))
                {
                    trPreSplitRecSelect.Visible = true;
                    trPostSplitSelect.Visible = true;
                    trPreSplitNextYearSelect.Visible = true;
                    //  trPostSplitNexSelect.Visible = true;
                    trPreSplitpaidSelect.Visible = true;
                    trPostSplitPaidSelect.Visible = true;
                    trReachesToReachNextSelect.Visible = true;
                    trPreSplitTotalRecSelect.Visible = true;
                    trTotalPreSplitOptionConSelect.Visible = true;
                    trTotalPreSplitOptionConNextSelect.Visible = true;
                }
                else
                {
                    trPreSplitRecSelect.Visible = false;
                    trPostSplitSelect.Visible = false;
                    trPreSplitNextYearSelect.Visible = false;
                    trPostSplitNexSelect.Visible = false;
                    trPreSplitpaidSelect.Visible = false;
                    trPostSplitPaidSelect.Visible = false;
                    trReachesToReachNextSelect.Visible = false;
                    trPreSplitTotalRecSelect.Visible = false;
                    trTotalPreSplitOptionConSelect.Visible = false;
                    trTotalPreSplitOptionConNextSelect.Visible = false;
                }
            }
        }
    }  
    protected void btnExporttoPDF_Click(object sender, EventArgs e)
    {
        try
        {
            int index = Convert.ToInt32(hdnModalPopup.Value);
          
            GridView grdList=null;
            if (index == 1 || index == 2 || index == 7 || index == 10 || index == 11 || index == 18 || index == 20 || index == 25)
            {
                grdList = grdReceivables;
            }
            else if (index == 3 || index == 19)
            {
                grdList = grdEarning;
            }
            else if (index == 4 || index == 5 || index == 16 || index == 17 || index == 22 || index == 23 || index == 28 || index == 29)            
            {
                grdList = grdPayment;
                grdList.Columns[4].HeaderText = "Payment Date";
            }
            else if (index == 6 || index == 24 || index == 8 || index == 9 || index == 12 || index == 13 || index == 14 || index == 15 ||index == 26 || index == 27)
            {
                grdList = grdFutureRec;
            }           
            if (grdList.Rows.Count > 0)
            {
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                foreach (GridViewRow row in grdList.Rows)
                {
                    row.Style.Add("font-size", "8px");
                }

                GridViewRow HeaderRow = grdList.HeaderRow;
                HeaderRow.Height = 20;
                HeaderRow.Style.Add("font-size", "9px");
                GridViewRow FooterRow = grdList.FooterRow;
                FooterRow.Height = 20;
                FooterRow.Style.Add("font-size", "9px");
                if (index == 1 || index == 2 || index == 7 || index == 18 || index == 20 || index == 25)
                {
                    FooterRow.Cells[2].Style.Add("text-align", "right");
                    FooterRow.Cells[4].Style.Add("text-align", "right");
                    FooterRow.Cells[5].Style.Add("text-align", "right");
                    FooterRow.Cells[6].Style.Add("text-align", "right");
                    FooterRow.Cells[7].Style.Add("text-align", "right");
                }
                else if (index == 3 || index == 19)
                {
                    FooterRow.Cells[3].Style.Add("text-align", "right");
                    FooterRow.Cells[4].Style.Add("text-align", "right");
                    FooterRow.Cells[5].Style.Add("text-align", "right");
                }
                else if (index == 4 || index == 5 || index == 22 || index == 23)
                {
                    FooterRow.Cells[5].Style.Add("text-align", "right");
                    FooterRow.Cells[7].Style.Add("text-align", "right");
                    grdList.Columns[10].Visible = false;
                }
                else if (index == 6 || index == 24 || index == 8 || index == 9 || index == 26 || index == 27)
                {
                    FooterRow.Cells[2].Style.Add("text-align", "right");
                    FooterRow.Cells[4].Style.Add("text-align", "right");
                    FooterRow.Cells[5].Style.Add("text-align", "right");
                    FooterRow.Cells[6].Style.Add("text-align", "right");
                }
                else if (index == 28 || index == 29)
                {
                    grdList.Columns[3].Visible = false;
                    grdList.Columns[4].Visible = false;
                    grdList.Columns[5].Visible = false;
                    grdList.Columns[6].Visible = false;
                    grdList.Columns[7].Visible = false;
                    grdList.Columns[8].Visible = false;
                    grdList.Columns[9].Visible = false;
                    grdList.Columns[10].Visible = false;
                }
                else if (index == 10 || index == 11)
                {
                    grdList.Columns[3].Visible = false;
                    grdList.Columns[4].Visible = false;
                    grdList.Columns[5].Visible = false;
                    grdList.Columns[6].Visible = false;
                    grdList.Columns[7].Visible = false;
                    FooterRow.Cells[2].Style.Add("text-align", "right");  
                }
                else if ((index == 12) || (index == 13))
                {
                    grdList.Columns[5].Visible = false;
                    FooterRow.Cells[2].Style.Add("text-align", "right");
                    FooterRow.Cells[4].Style.Add("text-align", "right");                   
                    FooterRow.Cells[6].Style.Add("text-align", "right");
                }
                else if ((index == 16) || (index == 17))
                {
                    FooterRow.Cells[5].Style.Add("text-align", "right");
                    FooterRow.Cells[7].Style.Add("text-align", "right");
                    grdList.Columns[4].HeaderText = "DueDate";
                }
                else if (index == 14 || index == 15)
                {
                    grdList.Columns[6].HeaderText = "Net Amount Due";
                    FooterRow.Cells[2].Style.Add("text-align", "right");
                    FooterRow.Cells[4].Style.Add("text-align", "right");
                    FooterRow.Cells[5].Style.Add("text-align", "right");
                    FooterRow.Cells[6].Style.Add("text-align", "right");
                }      
                Response.AddHeader("content-disposition", "attachment;filename=DealTableList.pdf");
                grdList.RenderControl(hw);
                Document pdfDoc = new Document(new Rectangle(612f, 792f), 36f, 36f, 36f, 36f);
                pdfDoc.HtmlStyleClass = "SetWidth";
                //pdfDoc.SetPageSize("8.5x11");
                //Document pdfDoc = new Document(PageSize._8.5*11, 10f, 10f, 10f, 0f);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                StringReader sr = new StringReader(sw.ToString());
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                //tdNotes.Visible = false;
                Response.End();
            }
            else
            {
                ModalDetail.Show();
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucSnapshot.ascx", "btnExporttoPDF_Click", ex.Message);
        }
    }
    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        try
        {
            int index = Convert.ToInt32(hdnModalPopup.Value);
            GridView grdList=null;
            if (index == 1 || index == 2 || index == 7 || index == 10 || index == 11 || index == 18 || index == 20 || index == 25)
            {
                grdList = grdReceivables;
            }
            else if (index == 3 || index == 19)
            {
                grdList = grdEarning;
            }
            else if (index == 4 || index == 5 || index == 16 || index == 17 || index == 22 || index == 23 || index == 28 || index == 29)
            {
                grdList = grdPayment;
            }
            else if (index == 6 || index == 24 || index == 8 || index == 9 || index == 12 || index == 13 || index == 14 || index == 15 || index == 26 || index == 27)
            {
                grdList = grdFutureRec;
            }           
            if (grdList.Rows.Count > 0)
            {
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                foreach (GridViewRow row in grdList.Rows)
                {
                    row.Style.Add("font-size", "10px");
                }
                GridViewRow HeaderRow = grdList.HeaderRow;
                HeaderRow.Height = 20;
                HeaderRow.Style.Add("font-size", "11px");
                GridViewRow FooterRow = grdList.FooterRow;
                FooterRow.Height = 20;
                FooterRow.Style.Add("font-size", "11px");
                if (index == 1 || index == 2 || index == 7 || index == 18 || index == 20 || index == 25)
                {
                    FooterRow.Cells[2].Style.Add("text-align", "right");
                    FooterRow.Cells[4].Style.Add("text-align", "right");
                    FooterRow.Cells[5].Style.Add("text-align", "right");
                    FooterRow.Cells[6].Style.Add("text-align", "right");
                    FooterRow.Cells[7].Style.Add("text-align", "right");
                }
                else if (index == 3 || index == 19)
                {
                    FooterRow.Cells[3].Style.Add("text-align", "right");
                    FooterRow.Cells[4].Style.Add("text-align", "right");
                    FooterRow.Cells[5].Style.Add("text-align", "right");
                }
                else if (index == 4 || index == 5 || index == 22 || index == 23)
                {
                    FooterRow.Cells[5].Style.Add("text-align", "right");
                    FooterRow.Cells[7].Style.Add("text-align", "right");
                    grdList.Columns[10].Visible = false;
                }
                else if (index == 6 || index == 24 || index == 8 || index == 9 || index == 26 || index == 27)
                {
                    FooterRow.Cells[2].Style.Add("text-align", "right");
                    FooterRow.Cells[4].Style.Add("text-align", "right");
                    FooterRow.Cells[5].Style.Add("text-align", "right");
                    FooterRow.Cells[6].Style.Add("text-align", "right");
                }
                else if (index == 28 || index == 29)
                {
                    grdList.Columns[3].Visible = false;
                    grdList.Columns[4].Visible = false;
                    grdList.Columns[5].Visible = false;
                    grdList.Columns[6].Visible = false;
                    grdList.Columns[7].Visible = false;
                    grdList.Columns[8].Visible = false;
                    grdList.Columns[9].Visible = false;
                    grdList.Columns[10].Visible = false;
                }
                else if (index == 10 || index == 11)
                {
                    grdList.Columns[3].Visible = false;
                    grdList.Columns[4].Visible = false;
                    grdList.Columns[5].Visible = false;
                    grdList.Columns[6].Visible = false;
                    grdList.Columns[7].Visible = false;
                    FooterRow.Cells[2].Style.Add("text-align", "right");
                }
                else if ((index == 12) || (index == 13))
                {
                    grdList.Columns[5].Visible = false;
                    FooterRow.Cells[2].Style.Add("text-align", "right");
                    FooterRow.Cells[4].Style.Add("text-align", "right");
                    FooterRow.Cells[6].Style.Add("text-align", "right");
                }
                else if ((index == 16) || (index == 17))
                {
                    FooterRow.Cells[5].Style.Add("text-align", "right");
                    FooterRow.Cells[7].Style.Add("text-align", "right");
                    grdList.Columns[4].HeaderText = "DueDate";
                }
                else if (index == 14 || index == 15)
                {
                    grdList.Columns[6].HeaderText = "Net Amount Due";
                    FooterRow.Cells[2].Style.Add("text-align", "right");
                    FooterRow.Cells[4].Style.Add("text-align", "right");
                    FooterRow.Cells[5].Style.Add("text-align", "right");
                    FooterRow.Cells[6].Style.Add("text-align", "right");
                }      
                Response.AddHeader("content-disposition", "attachment;filename=DealTableList.xls");
                grdList.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
            else
            {
                ModalDetail.Show();
            }
        }
        catch (Exception ex)
        {
            
            ErrorLog.WriteLog("ucSnapshot.ascx", "btnExporttoPDF_Click", ex.Message);
        }
    }
    protected void btnClose_Click1(object sender, ImageClickEventArgs e)
    {
        if (this.isAdmin)
        {
            int userId = Convert.ToInt16(drUsers.SelectedItem.Value);
            if (userId != Convert.ToInt32(Session["UserId"]))
            {
                trPreSplitRecSelect.Visible = true;
                trPostSplitSelect.Visible = true;
                trPreSplitNextYearSelect.Visible = true;
                //  trPostSplitNexSelect.Visible = true;
                trPreSplitpaidSelect.Visible = true;
                trPostSplitPaidSelect.Visible = true;
                trReachesToReachNextSelect.Visible = true;
                trPreSplitTotalRecSelect.Visible = true;
                trTotalPreSplitOptionConSelect.Visible = true;
                trTotalPreSplitOptionConNextSelect.Visible = true;
            }
            else
            {
                trPreSplitRecSelect.Visible = false;
                trPostSplitSelect.Visible = false;
                trPreSplitNextYearSelect.Visible = false;
                trPostSplitNexSelect.Visible = false;
                trPreSplitpaidSelect.Visible = false;
                trPostSplitPaidSelect.Visible = false;
                trReachesToReachNextSelect.Visible = false;
                trPreSplitTotalRecSelect.Visible = false;
                trTotalPreSplitOptionConSelect.Visible = false;
                trTotalPreSplitOptionConNextSelect.Visible = false;
            }
        }
        ScriptManager.RegisterStartupScript(updsnapsht, updsnapsht.GetType(), "locpopup2", " applyscrollable(); ", true);
    }
}