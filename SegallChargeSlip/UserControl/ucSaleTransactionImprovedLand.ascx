﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSaleTransactionImprovedLand.ascx.cs" Inherits="UserControl_ucSaleTransactionImprovedLand" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/UserControl/ucAddNewBuyerTenant.ascx" TagName="NewBuyer" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucFileUpload.ascx" TagName="FileUploadImproved" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucDueDtWiseCommission.ascx" TagName="DueDateWiseCommission" TagPrefix="uc" %>




<style>
    .wordbreak {
        word-break: break-all;
    }

</style>

<script type="text/javascript">
    function AutoFillCommisionImpLand(Type, parent) {
        debugger;


        var SalePrice = parseFloat(document.getElementById("<%=txtSalePrice.ClientID%>").value);

        if (!isNaN(SalePrice)) {
            var CommPer = document.getElementById("<%=txtCommissionPer.ClientID %>").value


            if (parseInt(CommPer) > 100) {

                alert("Commision percentage can not be 100");
                return false;
            }
            var SetFee = document.getElementById("<%=txtSetFee.ClientID %>").value;

            if (Type == "CP" && CommPer != "") {
                var CalSetFee = (parseFloat(SalePrice) * parseFloat(CommPer)) / (100);
                document.getElementById("<%=txtSetFee.ClientID %>").value = SetFee = CalSetFee;
            }
            else if (Type == "CP" && CommPer == "") {
                document.getElementById("<%=txtSetFee.ClientID %>").value = SetFee = "";
                }



            if (Type == "SF" && SetFee != "") {
                var CalCommPer = (parseFloat(SetFee) * 100) / (parseFloat(SalePrice));
                document.getElementById("<%=txtCommissionPer.ClientID %>").value = CommPer = CalCommPer;
        }
        else if (Type == "SF" && SetFee == "") {
            document.getElementById("<%=txtCommissionPer.ClientID %>").value = CommPer = "";
        }


    if (Type == "SP") {
        if (CommPer != "") {
            var CalSetFee = (parseFloat(SalePrice) * parseFloat(CommPer)) / (100);
            document.getElementById("<%=txtSetFee.ClientID %>").value = SetFee = CalSetFee;
            }
            else if (SetFee != "") {
                var CalCommPer = (parseFloat(SetFee) * 100) / (parseFloat(SalePrice));
                document.getElementById("<%=txtCommissionPer.ClientID %>").value = CommPer = CalCommPer;
            }
            else {
                document.getElementById("<%=txtSetFee.ClientID %>").value = SetFee = "";
                document.getElementById("<%=txtCommissionPer.ClientID %>").value = CommPer = "";
            }
    }
            // JAY 
            // document.getElementById("hdnis1st.ClientID %>").value = "1";
    var Comsetfee = document.getElementById("<%=txtSetFee.ClientID %>");
            Comsetfee.onchange();
        }

        return false;
    }
</script>

<script type="text/javascript">
    function ShowIcon(sender, e) {
        sender._element.className = "loadingUC";
    }

    function hideIcon(sender, e) {
        sender._element.className = "textwidthUC";
    }

    function OnBuyerSelected_Impr(source, eventArgs) {
        document.getElementById('<%=hdnBuyerId.ClientID %>').value = eventArgs.get_value();
        //   alert(eventArgs.get_value());
        return false;
    }
</script>
<link href="../Styles/AutoCompleteStyle.css" rel="stylesheet" />

<script src="../Js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="../Js/CommonValidations.js"></script>
<table style=" width: 100%;">
    <tr>
        <td colspan="3" class="ucheadings">
            SALE TRANSACTION: IMPROVED LAND
            &nbsp;
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">LAND SIZE
        </td>
        <td colspan="2">
            <asp:TextBox ID="txtLandSize" runat="server" CssClass="uctextboxMed " TabIndex="23"></asp:TextBox>
            &nbsp;
            <asp:DropDownList ID="ddlSizeMeasure" runat="server" CssClass="ucddlSmall" TabIndex="24" >
                <asp:ListItem Text="Sq Ft" Value="1"></asp:ListItem>
                <asp:ListItem Text="Acres" Value="2"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">BLDG SIZE
        </td>
        <td colspan="2">
            <asp:TextBox ID="txtBldgsize" runat="server" CssClass="uctextboxMed" TabIndex="25"></asp:TextBox>
            &nbsp;
            <b>SF</b>
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">BUYER
        </td>
        <td colspan="2">
            <asp:TextBox runat="server" ID="txtBuyer" AutoPostBack="true" CssClass="uctextboxMed" OnTextChanged="txtBuyer_TextChanged" TabIndex="26" />
            <ajax:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server"
                TargetControlID="txtBuyer"
                DelimiterCharacters=";, :"
                MinimumPrefixLength="1"
                EnableCaching="true"
                CompletionSetCount="10"
                CompletionInterval="300"
                ServiceMethod="SearchGetDealPartiesList"
                OnClientPopulating="ShowIcon"
                OnClientPopulated="hideIcon"
                OnClientItemSelected="OnBuyerSelected_Impr"
                ShowOnlyCurrentWordInCompletionListItem="true"
                CompletionListCssClass="AutoExtender"
                CompletionListItemCssClass="AutoExtenderList"
                CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                CompletionListElementID="divwidth" />
            <asp:HiddenField ID="hdnBuyerId" runat="server" />
            <ajax:ModalPopupExtender ID="mpeAddNewBuyer" TargetControlID="btnAddNewBuyer" CancelControlID="ucNwBuyer$ImgbtnCancel"
                PopupControlID="dvNwBuyer" runat="server" BackgroundCssClass="modalBackground">
            </ajax:ModalPopupExtender>
            &nbsp;
             <asp:ImageButton ID="btnAddNewBuyer" runat="server" Height="16px" Width="16px" ImageUrl="~/Images/Add-Icon.gif"  />
            <%-- // <asp:Button ID="btnAddNewBuyer" runat="server" Text="Add New" CssClass="SqureButton" Width="90px" />--%>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>
    <tr>
        <td  class="uclabels">USE
        </td>
        <td colspan="2">
            <asp:DropDownList ID="ddlUse" runat="server" CssClass="ddlSmall" TabIndex="27">
               <%-- <asp:ListItem Text="-Select-" Value="Select"></asp:ListItem>
                <asp:ListItem Text="RETAIL" Value="Retail"></asp:ListItem>
                <asp:ListItem Text="RESTAURANT" Value="Restaurant"></asp:ListItem>--%>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels" >SALE PRICE
        </td>
        <td colspan="2">
            <asp:TextBox ID="txtSalePrice" runat="server"  CssClass="uctextboxMed onlyDecimals DollarTextBox" Width="140px" onchange="AutoFillCommisionImpLand('SP',''); return false;" TabIndex="28" ></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>
    <%--  <tr>
        <td style="vertical-align: top; width: 25%; padding-left: 15px; height: 18px;"><b>DUE DATE</b>
        </td>
        <td style="vertical-align: baseline;">
            <asp:TextBox ID="txtDueDate" runat="server" Width="130px" Height="18px"></asp:TextBox>
            <asp:ImageButton ID="btnCalender" runat="server" Width="16px" Height="16px" ImageUrl="~/Images/Calander.jpeg" />
            <ajax:CalendarExtender ID="ajaxCal" runat="server" TargetControlID="txtDueDate" PopupButtonID="btnCalender" Format="dd/MM/yyyy"></ajax:CalendarExtender>
        </td>
        <td style="width: 45%;padding-left:58px;height:18px;">
            <asp:Button ID="btnAddDueDate" runat="server" Text="ADD" CssClass="SqureButton" Width ="90px" />
        </td>
    </tr>--%>

    <tr>

        <td class="uclabels">COMMISSION
        </td>
        <td colspan="2">
            <table>
                <tr>
                    <td><b>%</b></td>
                    <td style="padding-left: 12px;">

                        <asp:TextBox ID="txtCommissionPer" runat="server" CssClass="uctextboxSmall onlyDecimals" onchange="return AutoFillCommisionImpLand('CP','<%= this.ClientID %>')" TabIndex="29" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td><b>SET FEE</b></td>
                    <td style="padding-left: 12px;">
                        <asp:TextBox ID="txtSetFee"  runat="server" CssClass="uctextboxSmall onlyDecimals DollarTextBox "  Width="69px" onchange="return AutoFillCommisionImpLand('SF','<%= this.ClientID %>')"  TabIndex="30"></asp:TextBox>
                        <%--      <asp:HiddenField ID="hdnis1st" Value="0" runat="server" />--%>
                    </td>
                </tr>
            </table>
        </td>
        <%-- <td colspan="2" style="width: 25%;">
            <b>%</b>
            <br />
            <b>SET FEE  </b>
        </td>
        <td>
            <asp:TextBox ID="txtCommissionPer" runat="server" Width="80px" onchange="return AutoFillCommisionImpLand('CP')" CssClass="onlyDecimals"></asp:TextBox><br />
            <asp:TextBox ID="txtSetFee" runat="server" Width="80px" onchange="return AutoFillCommisionImpLand('SF')" CssClass="onlyDecimals"></asp:TextBox>
        </td>--%>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>

    <tr> <%--width: 25%;--%>
        <td class="uclabels">DUE DATE
        </td>
        <td colspan="2">
            <asp:UpdatePanel ID="upDueDate" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <uc:DueDateWiseCommission ID="DueDateCommission" runat="server" />

                </ContentTemplate>
            </asp:UpdatePanel>

        </td>

    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>



    <%--<tr>
        <td colspan ="3">
            <div id="divImproved" >
            <uc:FileUploadImproved ID="ucFileUploadImproved" runat ="server" />
                </div>
        </td>
      </tr>  --%>
    <tr>
        <td class="uclabels">NOTES
        </td>
        <td colspan="2">

            <asp:TextBox Rows="10" runat="server" TextMode="MultiLine" style="font-family:Verdana;" Columns="35"  ID="txtNotes" TabIndex="35"></asp:TextBox>
           
                  <div style="float: right; padding-right: 0px;">
                <asp:ImageButton ID="imgAttach" ImageUrl="~/Images/Attach-Icon.gif" CssClass="imgSizeBig" runat="server" />


                <ajax:ModalPopupExtender ID="mpExtendr" runat="server" PopupControlID="dvFileUpload" TargetControlID="imgAttach" CancelControlID="btnCancelPopup" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>

            </div>
        </td>
    </tr>
    <tr>
         <td class="uclabels"></td>
        <td colspan="2">
            <asp:UpdatePanel ID="upAtttachment" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnAttachment" runat="server" Width="100%" BorderColor="Transparent" BorderStyle="Solid" BorderWidth="1px">
                        <asp:GridView ID="gvAttach" GridLines="None" runat="server" AutoGenerateColumns="false" Width="90%" ShowHeader="false">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Image ID="imgDot" runat="server" ImageUrl="~/Images/bullet.gif" BackColor="Gray" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="AttachmentDescription" HeaderText="Attachment" ItemStyle-CssClass="wordbreak" />
                                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtDelete" OnClientClick="return confirm('Are you sure want to delete')" ImageUrl="~/Images/delete.gif" runat="server" OnClick="imgbtDelete_Click" CommandArgument='<%# Eval("UniqueAttachmentDescription") %>'></asp:ImageButton>

                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </ContentTemplate>

            </asp:UpdatePanel>
            <%-- <hr style="width: 100%" />--%>
        </td>
    </tr>
</table>
<div class="PopupDivBodyStyle" id="dvFileUpload" style="display: block; height: 150px;">
    <asp:UpdatePanel ID="upAttachment" runat="server" UpdateMode="Conditional">

        <ContentTemplate>
            <table style="width: 100%" class="spacing">
                <tr style="background-color: gray; border-bottom: solid;">
                    <td style="text-align: left;">
                        <h3>&nbsp; &nbsp;File Upload</h3>
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="btnCancelPopup" CssClass="SqureButton" Height="15px" Width="15px" ImageUrl="~/Images/close.png" runat="server"  />

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:FileUpload ID="upFileUpload" runat="server" /></td>
                    <td style="padding-right:10px">
                        <asp:Button ID="btnFileUpload" runat="server" CssClass="SqureButton" OnClientClick="SetPageLeave();" OnClick="btnFileUpload_Click" Text="Attach" />
                    </td>
                </tr>

            </table>
        </ContentTemplate>
        <Triggers>

            <asp:PostBackTrigger ControlID="btnFileUpload" />
        </Triggers>
    </asp:UpdatePanel>
</div>
<div style="display: none; height: 380px" class="PopupDivBodyStyle" id="dvNwBuyer">
    <uc:NewBuyer ID="ucNwBuyer" runat="server" />
</div>
<%--<div style="display: none;height:auto;width:auto;" class="PopupDivBodyStyle" id="dvFileUpload">
     <uc:FileUpload ID="ucFileUpload" runat ="server" />
    </div> --%>
<%--<div id="dvFileUpload" style ="border:solid;height :200px;width:400px;border-color:gray;">
    <uc:FileUpload ID="ucFileUpload" runat ="server" />
</div>--%>



