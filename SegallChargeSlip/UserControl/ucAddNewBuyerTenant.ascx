﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAddNewBuyerTenant.ascx.cs" Inherits="UserControl_ucAddNewBuyerTenant" %>
<%--<link href="../Styles/SiteTEST.css" rel="stylesheet" />--%>
<%--< %@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>--%>
<script type="text/javascript">
    function OnPartyCompanySelected(source, eventArgs) {
        document.getElementById('<%=hdnPartyComapnyName.ClientID %>').value = eventArgs.get_value();
        //   alert(eventArgs.get_value());
        return false;
    }

    //function EmailValidation() {
    //    var Input = $('#txtEmail').val();

    //    var regExp = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;

    //    if (!regExp.test(Input)) {
    //        $('#txtEmail').val('');
    //    }
    //    else {

    //        return true;
    //    }

    //    return false;
    //}
    function Validate(event) {
        var regex = new RegExp("^[0-9 \-]+$");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    }
    function ValidateAll(Parent) {

        var txtComapanyName = $('[id^=' + Parent + '_txtComapanyName]')
        //var txtName = $('[id^=' + Parent + '_txtName]');
        //var txtAddress1 = $('[id^=' + Parent + '_txtAdd1]');
        //var txtCity = $('[id^=' + Parent + '_txtCity]');
        //var ddlState = $('[id^=' + Parent + '_ddlState]');
        var txtEmail = $('[id^=' + Parent + '_txtEmail]');
        var txtZipCode = $('[id^=' + Parent + '_txtZipCode]');
        var lbErrorMsg = $('[id^=' + Parent + '_lbErrorMsg]');
        var regExp = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
        debugger;
        if ($.trim(txtComapanyName.val()) == "") {
            lbErrorMsg.text("Please Enter Company");
            txtComapanyName.focus();
            return false;
        }
        //if ($.trim(txtName.val()) == "") {
        //    lbErrorMsg.text("Please Enter Name");
        //    txtName.focus();
        //    return false;
        //}
        if ($.trim(txtZipCode.val()) != "") {

            if (!validateZipLen($.trim(txtZipCode.val()))) {
                lbErrorMsg.text("Please Enter valid Zipcode");
                txtZipCode.focus();
                return false;
            }

        }
        if ($.trim(txtEmail.val()) != "")
        {
            if (!regExp.test(txtEmail.val())) {
                    lbErrorMsg.text("Enter Email In Proper Format");
                     txtEmail.val('');
                        return false;
                    }
        }
        //{
        //    lbErrorMsg.text("Please Enter Email");
        //    txtEmail.focus();
        //    return false;
        //}
        //else if (!regExp.test(txtEmail.val())) {
        //    lbErrorMsg.text("Enter Email In Proper Format");
        //     txtEmail.val('');
        //        return false;
        //    }
        //else if (txtAddress1.val() == "") {
        //    lbErrorMsg.text("Please Enter Address");
        //    txtAddress1.focus();
        //    return false;
        //}
        //else if (txtCity.val() == "") {
        //    lbErrorMsg.text("Please Enter City");
        //    txtCity.focus();
        //    return false;
        //}
        //else if (ddlState.val() == "0" || ddlState.val() == "") {
        //    lbErrorMsg.text("Please Enter State");
        //    ddlState.focus();
        //    return false;
        //}
        //else if (txtZipCode.val() == "") {
        //    lbErrorMsg.text("Please Enter Zipcode");
        //    txtZipCode.focus();
        //    return false;
        //}

        lbErrorMsg.text("");
        window.close();
        return true;
    }
    
    function ClearAll(Parent) {
        $(Parent + "_lbErrorMsg").text('');
        $('[id^=' + Parent + '_txtComapanyName]').val('');
        $('[id^=' + Parent + '_txtName]').val('');
        $('[id^=' + Parent + '_txtAdd1]').val('');
        $('[id^=' + Parent + '_txtCity]').val('');
        $('[id^=' + Parent + '_ddlState]').val('');
        $('[id^=' + Parent + '_txtEmail]').val('');
        $('[id^=' + Parent + '_txtZipCode]').val('');
        return true;
    }
</script>
<table cellspacing="0" cellpadding="4">
    <tr class="trBorder">
        <td colspan="4">
            <h3>
                <asp:Label ID="lbHaeding" runat="server"></asp:Label>
            </h3>
        </td>
        <td align="right" style="padding-right: 10px"><%--OnClientClick="this.close();return false;"--%>
            <asp:ImageButton ImageUrl="~/Images/close.png" ID="ImgbtnCancel" runat="server" ClientIDMode="Static" />
        </td>
    </tr>
    <%--   <tr>
        <td colspan="5">&nbsp;</td>
    </tr>--%>
    <tr>
        <td colspan="5">
            <asp:Label runat="server" ID="lbErrorMsg" CssClass="errorMsg"></asp:Label>&nbsp;
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>COMPANY NAME
        </td>
        <td>
            <asp:TextBox ID="txtComapanyName" runat="server" CssClass="modaltextboxlarge" Width="250px" Height="16px"></asp:TextBox>
            <ajax:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server"
                TargetControlID="txtComapanyName"
                DelimiterCharacters=";, :"
                MinimumPrefixLength="1"
                EnableCaching="true"
                CompletionSetCount="10"
                CompletionInterval="300"
                ServiceMethod="SearchGetPartyComapanyList"
                OnClientPopulating="ShowIcon"
                OnClientPopulated="hideIcon"
                OnClientItemSelected="OnPartyCompanySelected"
                ShowOnlyCurrentWordInCompletionListItem="true"
                CompletionListCssClass="AutoExtender"
                CompletionListItemCssClass="AutoExtenderList"
                CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                CompletionListElementID="divwidth" />
            <asp:HiddenField ID="hdnPartyComapnyName" runat="server" />
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>NAME
        </td>
        <td>
            <asp:TextBox ID="txtName" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>EMAIL
        </td>
        <td>
            <asp:TextBox ID="txtEmail" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>ADDRESS LINE 1
        </td>
        <td>
            <asp:TextBox ID="txtAdd1" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>ADDRESS LINE 2
        </td>
        <td>
            <asp:TextBox ID="txtAdd2" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>CITY
        </td>
        <td>
            <asp:TextBox ID="txtCity" runat="server" CssClass="modaltextboxsmall"></asp:TextBox>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>STATE
        </td>
        <td>
            <asp:DropDownList ID="ddlState" runat="server" CssClass="modalddlSmall"></asp:DropDownList>
        </td>
    </tr>

    <tr>
        <td>&nbsp;</td>
        <td>ZIPCODE
        </td>
        <td>
            <asp:TextBox ID="txtZipCode" runat="server" CssClass="modaltextboxsmall" onkeypress="return Validate(event);"></asp:TextBox>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>

    <tr>
        <td style="text-align: right;" colspan="4">
            <asp:Button ID="btnAddNew" runat="server" Text="ADD" OnClick="AddNew" CssClass="SqureGrayButtonSmall" />
            <%--onmouseover="this.style.backgroundColor='#E0E0E0';"
                onmouseout="this.style.backgroundColor='#B8B8B8';"--%>
        </td>
        <td>&nbsp;</td>
        <%-- <td style="text-align: right;">
            <asp:Button runat="server" ID="btnCancel" OnClick="Cancel_Click" Style="background-color: gray;" Text="Cancel" />
        </td>--%>
    </tr>
    <tr>
        <td colspan="5">&nbsp;</td>
    </tr>
</table>
