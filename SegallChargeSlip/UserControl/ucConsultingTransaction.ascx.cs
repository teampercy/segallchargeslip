﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;
using System.IO;

public partial class UserControl_ucConsultingTransaction : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        { 
        if (!Page.IsPostBack)
        {

            SetAttributes();
            if (Session[GlobleData.NewChargeSlipObj] != null)
            {
                ChargeSlip objChargeSlip = new ChargeSlip();
                objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];

                txtNotes.Text = objChargeSlip.DealTransactionsProperty.Notes;
                txtSetFee.Text = objChargeSlip.DealTransactionsProperty.SetFee.ToString();


                Attachments objAttachments = new Attachments();
                objAttachments.ChargeSlipID = objChargeSlip.ChargeSlipID;
                DataTable dt = objAttachments.GetDetailsByChargeSlip();
                //Remove repeted code

                DataTable dtAttachment = new DataTable();
                DataColumn dtcol = new DataColumn("AttachmentDescription", typeof(string));
                dtAttachment.Columns.Add(dtcol);
                dtcol = new DataColumn("FilePath", typeof(string));
                dtAttachment.Columns.Add(dtcol);
                dtcol = new DataColumn("UniqueAttachmentDescription", typeof(string));
                dtAttachment.Columns.Add(dtcol);
                DataRow dr;
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dRow in dt.Rows)
                        {
                            dr = dtAttachment.NewRow();
                            dr[0] = dRow["FileName"];
                            dr[1] = dRow["FilePath"];
                            dr[2] = dRow["AttachmentDescription"];
                            dtAttachment.Rows.Add(dr);
                        }
                    }
                }
                GetLatestDueDates();
                //if (Session[GlobleData.Attachments] != null)
                //{
                //    dtAttachment.Rows.Clear();
                //}
                //if (dt.Rows.Count > 0)
                //{
                //    foreach (DataRow dRow in dt.Rows)
                //    {
                //        dr = dtAttachment.NewRow();
                //        dr[0] = dRow["FileName"];
                //        dr[1] = dRow["FilePath"];
                //        dr[2] = dRow["AttachmentDescription"];
                //        dtAttachment.Rows.Add(dr);
                //    }
                //}

                Session[GlobleData.Attachments] = dtAttachment;
                BindGrid();
                upAtttachment.Update();

                CommissionDueDates objCommissionDueDates = new CommissionDueDates();
                objCommissionDueDates.ChargeSlipId = objChargeSlip.ChargeSlipID;
                Session[GlobleData.CommissionTb] = objCommissionDueDates.GetListAsPerChargeSlipId();

            }
            else
            {
                ResetControls();
            }
            // pnAttachment.Visible = false;
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucConsultingTransaction.ascx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #region Custom Action
    public void SetProperties()
    {
        try
        { 
        DealTransactions objDealTransactions = new DealTransactions();
        objDealTransactions.Notes = txtNotes.Text;
        objDealTransactions.SetFee = txtSetFee.Text.Trim() == "" ? 0 : Convert.ToDecimal(txtSetFee.Text);
        objDealTransactions.DealType = 3;
        Session.Add("DealTransactionObj", objDealTransactions);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucConsultingTransaction.ascx", "SetProperties", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void SetPropertiesDueDate()
    {
        DueDateCommission.strParName = this.ClientID;
        DueDateCommission.SetAttributesDueDtCommission();
        ViewState[GlobleData.cns_DueDate] = this.ClientID;
    }
    private void ResetControls()
    {
        txtNotes.Text = "";
        txtSetFee.Text = "";
        SetAttributes();
    }
    private void SetAttributes()
    {
        txtSetFee.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtSetFee.ClientID + "')");
    }

    public void GetLatestDueDates()
    {
        try
        { 
        if (Session[GlobleData.cns_IsFullCommPaid] != null)
        {
            if (Convert.ToBoolean(Session[GlobleData.cns_IsFullCommPaid].ToString()))
            {
                DueDateCommission.IsFullCommPaid = true;
            }
        }
      
        DueDateCommission.SetPropDueDateWiseCommission();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucConsultingTransaction.ascx", "GetLatestDueDates", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }


    #endregion
    protected void btnFileUpload_Click(object sender, EventArgs e)
    {
        try
        { 
        // pnAttachment.Visible = false;
        if (upFileUpload.HasFile)
        {
            HiddenField hdnPageLeave = (HiddenField)this.Parent.FindControl("hdnPageLeave");
            hdnPageLeave.Value = "0";
            //pnAttachment.Visible = true ;
            string strGuid = string.Empty;
            strGuid = System.Guid.NewGuid().ToString();
            string ServerPath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
            string fileName = Path.GetFileName(upFileUpload.PostedFile.FileName);


            if (!Directory.Exists(Server.MapPath(ServerPath)))
            { Directory.CreateDirectory(Server.MapPath(ServerPath)); }

            upFileUpload.PostedFile.SaveAs(Server.MapPath(ServerPath) + strGuid + "_" + fileName);


            DataTable dtAttachment = new DataTable();
            DataColumn dtcol = new DataColumn("AttachmentDescription", typeof(string));
            dtAttachment.Columns.Add(dtcol);
            dtcol = new DataColumn("FilePath", typeof(string));
            dtAttachment.Columns.Add(dtcol);
            dtcol = new DataColumn("UniqueAttachmentDescription", typeof(string));
            dtAttachment.Columns.Add(dtcol);
            DataRow dr;
            if (Session[GlobleData.Attachments] != null)
            {
                dtAttachment = (DataTable)Session[GlobleData.Attachments];
            }
            dr = dtAttachment.NewRow();
            dr[0] = fileName;
            dr[1] = Server.MapPath(ServerPath) + strGuid + "_" + fileName;
            dr[2] = strGuid + "_" + fileName;
            dtAttachment.Rows.Add(dr);
            Session[GlobleData.Attachments] = dtAttachment;
            BindGrid();
            upAtttachment.Update();
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucConsultingTransaction.ascx", "btnFileUpload_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    protected void imgbtDelete_Click(object sender, ImageClickEventArgs e)
    {
        try
        { 
        string id = (sender as ImageButton).CommandArgument;
        //Attachments objAttachment = new Attachments();
        //objAttachment.AttachmentID = Convert.ToInt32(id);
        //objAttachment.Load();



        string Serverpath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
        string pathfile = Serverpath + id;

        string fullpath = Server.MapPath(pathfile);

        if (File.Exists(fullpath))
        {
            File.Delete(fullpath);
        }
        // objAttachment.Delete();
        // Session[GlobleData.Attachments] = null;
        // pnAttachment.Visible = false;

        DataTable dt = new DataTable();
        dt = (DataTable)Session[GlobleData.Attachments];
        DataRow[] r = dt.Select();
        for (int i = 0; i < r.Length; i++)
        {
            if (id == r[i]["UniqueAttachmentDescription"].ToString())
            {
                r[i].Delete();
            }

        }
        Session[GlobleData.Attachments] = dt;
        BindGrid();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucConsultingTransaction.ascx", "imgbtDelete_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    private void BindGrid()
    {
        try
        { 
        DataTable dtGrid = (DataTable)Session[GlobleData.Attachments];


        if (dtGrid != null && dtGrid.Rows.Count > 0)
        {
            //string attachment = dtGrid.Rows[0]["AttachmentDescription"].ToString();
            //int count = attachment.IndexOf('_');
            // dtGrid.Rows[0]["AttachmentDescription"] = attachment.Substring(count + 1).ToString();
            gvAttach.DataSource = dtGrid;
            gvAttach.DataBind();
            pnAttachment.Visible = true;
            upAtttachment.Update();
        }
        else
        {
            gvAttach.DataSource = null;
            gvAttach.DataBind();
            pnAttachment.Visible = false;
            upAtttachment.Update();
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucConsultingTransaction.ascx", "BindGrid", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    protected void imgAttach_Click(object sender, ImageClickEventArgs e)
    {

    }
}