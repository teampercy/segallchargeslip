﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucConsultingTransaction.ascx.cs" Inherits="UserControl_ucConsultingTransaction" %>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>--%>
<%@ Register Src="~/UserControl/ucDueDtWiseCommission.ascx" TagName="DueDateWiseCommission" TagPrefix="uc" %>
<style>
    .wordbreak
    {
        word-break: break-all;
    }
</style>
<table style="width: 100%">
    <%-- class="ucbackground" --%>
    <tr>
        <td colspan="3" class="ucheadings">CONSULTING
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">FEE
        </td>
        <td colspan="2">
            <asp:TextBox ID="txtSetFee" runat="server" CssClass="uctextboxMed DollarTextBox" TabIndex="23"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">DUE DATE
        </td>
        <td colspan="2">
            <asp:UpdatePanel ID="upDueDate" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <uc:DueDateWiseCommission ID="DueDateCommission" runat="server" />

                </ContentTemplate>
            </asp:UpdatePanel>

        </td>

    </tr>
    <tr>
        <td class="uclabels">DESCRIPTION
        </td>
        <td colspan="2">
            <asp:TextBox Rows="10" runat="server" CssClass="uctextArea" TextMode="MultiLine" Columns="30" ID="txtNotes" TabIndex="24"></asp:TextBox>
            <div style="float: right; padding-right: 0px;">

                <asp:ImageButton ID="imgAttach" ImageUrl="~/Images/Attach-Icon.gif" Height="20px" Width="20px" runat="server" />


                <ajax:ModalPopupExtender ID="mpExtendr" runat="server" PopupControlID="dvFileUploadConsult" TargetControlID="imgAttach" CancelControlID="btnCancelPopup" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>

            </div>
        </td>
    </tr>

    <tr>
        <td class="uclabels"></td>
        <td colspan="2">
            <asp:UpdatePanel ID="upAtttachment" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnAttachment" runat="server" Width="100%" BorderColor="Transparent" BorderStyle="Solid" BorderWidth="1px">
                        <asp:GridView ID="gvAttach" GridLines="None" runat="server" AutoGenerateColumns="false" Width="90%" ShowHeader="false">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Image ID="imgDot" runat="server" ImageUrl="~/Images/bullet.gif" BackColor="Gray" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="AttachmentDescription" HeaderText="Attachment" ItemStyle-CssClass="wordbreak" />
                                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtDelete" OnClientClick="return confirm('Are you sure want to delete')" ImageUrl="~/Images/delete.gif" runat="server" OnClick="imgbtDelete_Click" CommandArgument='<%# Eval("UniqueAttachmentDescription") %>'></asp:ImageButton>

                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </ContentTemplate>

            </asp:UpdatePanel>
            <%-- <hr style="width: 100%" />--%>
        </td>
    </tr>
</table>
<div class="PopupDivBodyStyle" id="dvFileUploadConsult" style="display: none; height: 150px;">

    <table style="width: 100%" class="spacing">
        <asp:UpdatePanel ID="upAttachment" runat="server" UpdateMode="Conditional">

            <ContentTemplate>
                <tr style="background-color: gray; border-bottom: solid;">
                    <td style="text-align: left;">
                        <h3>&nbsp; &nbsp; File Upload</h3>
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="btnCancelPopup" Height="15px" Width="15px" ImageUrl="~/Images/close.png" runat="server" />

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:FileUpload ID="upFileUpload" runat="server" /></td>
                    <td>
                        <asp:Button ID="btnFileUpload" runat="server" CssClass="SqureButton" OnClientClick="SetPageLeave();" OnClick="btnFileUpload_Click" Text="Attach" />
                    </td>
                </tr>
            </ContentTemplate>
            <Triggers>

                <asp:PostBackTrigger ControlID="btnFileUpload" />
            </Triggers>
        </asp:UpdatePanel>
    </table>

</div>
