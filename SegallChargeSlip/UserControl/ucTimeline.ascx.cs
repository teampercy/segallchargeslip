﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_ucTimeline : System.Web.UI.UserControl
{
    #region Properties
    public Boolean isAdmin
    {
        get
        {
            return Convert.ToBoolean(ViewState["isuseradmin"]);
        }
        set
        {
            ViewState["isuseradmin"] = value;
            //dvBrokers.Visible = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if(Session[GlobleData.User]!=null)
            {
                if(!IsPostBack)
                {
                    FillUserDropDown();
                    BindGridItem();
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx", false);
            }
        }
        catch(Exception)
        {
            throw;
        }
    }    

    #region Bind Dropdown
    public void FillUserDropDown()
    {
        if (Session[GlobleData.User] != null)
        {
            if (isAdmin)
            {
                Brokers brokers = new Brokers();

                DataSet dsBrokers = brokers.usp_getCoborkerslistComp(Convert.ToInt64(Session[GlobleData.User]));
                if (dsBrokers != null && dsBrokers.Tables.Count > 0 && dsBrokers.Tables[0].Rows.Count > 0)
                {
                    drUsers.DataSource = dsBrokers;
                    drUsers.DataTextField = "BrokerName";
                    drUsers.DataValueField = "UserID";
                    drUsers.DataBind();                   
                }
            }
            else
            {
                if (Session[GlobleData.User] != null)
                {
                    drUsers.Items.Insert(0, new ListItem("", Convert.ToString(Session[GlobleData.User])));
                    dvBrokers.Style.Add("display", "none");
                }
            }
        }
    }

    private void BindGridItem()
    {
        DataTable dtGridData = new DataTable();
        dtGridData.Columns.Add("DealName");
        dtGridData.Columns.Add("DueDateShort");
        dtGridData.Columns.Add("CommAmountDue");
        dtGridData.Columns.Add("PaymentDateShort");
        dtGridData.Columns.Add("PaidAmount");
        dtGridData.Columns.Add("UnApplied");
        dtGridData.Rows.Add();

        grdChargeslipList.DataSource = dtGridData;
        grdChargeslipList.DataBind();
    }
    #endregion
}