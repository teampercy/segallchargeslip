﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucUploadAttacment.ascx.cs" Inherits="UserControl_ucUploadAttacment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>


<style type="text/css">
    .auto-style1 {
        height: 35px;
    }

    .wordbreak {
        word-break: break-all;
    }



    /*::-webkit-scrollbar {
        width: 9px;
        height: 3px;
        background: #FFFFFF;
    }

    ::-webkit-scrollbar-thumb {
        background-color: #ffffff;
        border: 1px solid black;
    }*/
</style>

<asp:UpdatePanel ID="upAttachment" runat="server" UpdateMode="Conditional">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpload" />
    </Triggers>
    <ContentTemplate>
        <%--<table style="width: 100%; height: 100%; background-color: gray;">--%>
        <table style="width: 100%; font-size: smaller;" class="ucbackgroundborder">
            <tr>

                <td class="ucheadings">COMMENTS

                </td>
                <td style="width: 40%" align="right">

                    <asp:ImageButton ID="btnAttachment" AlternateText="Attach" runat="server" Height="20px" Width="20px" ImageUrl="~/Images/Attach-Icon.gif" />
                    <%--  <asp:Button ID="btnAttachment" Text="Attach" runat="server" CssClass="SqureButton"></asp:Button>--%>
                    <ajax:ModalPopupExtender ID="mpExtendr" runat="server" PopupControlID="dvFileUpload" TargetControlID="btnAttachment" CancelControlID="btnCancelPopup" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-left: 15px;">
                    <hr style="width: 100%" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <%--<div id="dvMainAttach" style="overflow-y: scroll;">--%>
                    <div id="dvMainAttach" style="overflow-y: scroll; margin: 0px 10px 0px 10px; background-color: white;">

                        <table style="width: 100%; font-size: x-small;">
                            <tr>
                                <td>
                                    <table id="tbSummary" style="width: 100%;" cellspacing="0">
                                        <%--   <tr>
                                            <td colspan="3">COMMENTS
                                            </td>
                                        </tr>--%>
                                      <%--  <tr visible="false">
                                            <td style="width: 2.5%"></td>
                                            <td style="background-color: white;">
                                                <asp:TextBox ID="txtOldComment" runat="server" TextMode="MultiLine" Width="95%" Rows="6" ReadOnly="true"></asp:TextBox>
                                            </td>
                                            <td style="width: 2.5%"></td>
                                        </tr>--%>
                                        <tr>
                                            <td style="width: 2.5%"></td>
                                            <td style="background-color: white;">
                                                <asp:TextBox ID="txtNewComments" runat="server" TextMode="MultiLine" Width="95%" Rows="12"></asp:TextBox>
                                            </td>
                                            <td style="width: 2.5%"></td>
                                        </tr>


                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="height: 15px;"></td>
            </tr>
            <%--  <tr>
                                            <td colspan="3" style ="padding-left :30px;">Attachment List</td>
                                        </tr>--%>
            <tr>

                <td colspan="2" style="padding-left: 25px;">
                    <asp:UpdatePanel ID="upAttachemntList" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnAttachmentList" runat="server" Width="100%" BorderColor="Transparent" BorderStyle="Solid" BorderWidth="1px">
                                <asp:GridView ID="gvAttachment" runat="server" Width="100%" GridLines="None" AutoGenerateColumns="false" ShowHeader="false">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Image ID="imgDot" runat="server" ImageUrl="~/Images/bullet.gif" BackColor="Gray" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Text" HeaderText="Attachment" ItemStyle-Wrap="true" ItemStyle-CssClass="wordbreak" />
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtDelete" ImageUrl="~/Images/delete.gif" runat="server" OnClick="imgbtDelete_Click" CommandArgument='<%# Eval("Value") %>'></asp:ImageButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>

            </tr>
        </table>
        <div class="PopupDivBodyStyle" style="display: none; height: 150px;" id="dvFileUpload">
            <table style="width: 100%" class="spacing">

                <tr style="background-color: gray; border-bottom: solid;">
                    <td style="text-align: left;">
                        <h3>&nbsp; &nbsp;File Upload</h3>
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="btnCancelPopup" Height="15px" Width="15px" ImageUrl="~/Images/close.png" runat="server"  /><%-- OnClientClick="javascript:window.close();return false;"--%>

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:FileUpload ID="fupAttachment" runat="server" />
                    </td>
                    <td>
                        <asp:ImageButton ID="btnUpload" AlternateText="Attach" OnClick="FileUpload_Click" runat="server" Height="20px" Width="20px" ImageUrl="~/Images/Attach-Icon.gif" />
                        <%--<asp:Button ID="btnUpload" Text="Attach" runat="server" OnClick="FileUpload_Click" CssClass="SqureButton" />--%>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
