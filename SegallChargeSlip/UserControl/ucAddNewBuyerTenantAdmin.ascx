﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAddNewBuyerTenantAdmin.ascx.cs" Inherits="UserControl_ucAddNewBuyerTenantAdmin" %>
 <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script src="../Js/CommonValidations.js"></script>
<script type="text/javascript" src="../Js/ChargeSlipValidation.js"></script>
<script type="text/javascript">
    function EmailValidation() {
        var Input = $('#txtEmail').val();

        var regExp = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;

        if (!regExp.test(Input)) {
            $('#txtEmail').val('');
        }
        else {

            return true;
        }

        return false;
    }
    //$(document).ready(function () {
    //    $('#btnAddNew').click(function () {
    //        if ($("#txtComapanyName").val() != "") {
    //            return true;
    //        }
    //        else {
    //            alert('Please enter the text')
    //            return false;
    //        }
    //    })
    //});
    function ValidateAll() {
        debugger;
        var txtCompany = document.getElementById("<%=txtComapanyName.ClientID%>").value;
      //  var txtName = document.getElementById("<%=txtName.ClientID%>").value;
        var txtZipcode = document.getElementById("<%=txtZipCode.ClientID%>").value;
           // var txtAddress1 = document.getElementById("<%=txtAdd1.ClientID%>").value;
           // var txtCity = document.getElementById("<%=txtCity.ClientID%>").value;
           // var ddlState = document.getElementById("<%=ddlState.ClientID%>").value;
         var txtEmail = document.getElementById("<%=txtEmail.ClientID%>").value;
        var lbErrorMsg = document.getElementById("<%=lbErrorMsg.ClientID%>");
        var regExp = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
        if ($.trim(txtCompany) == "") {
            lbErrorMsg.innerHTML = "Please Enter Company Name";
            document.getElementById("<%=txtComapanyName.ClientID%>").focus();
                return false;
            }
       
        if ($.trim(txtZipcode) != "") {
      
            if (!validateZipLen(txtZipcode)) {
                lbErrorMsg.innerHTML = "Please Enter between 5 and 10 digit Valid ZipCode.";
                //lbErrorMsg.innerHTML = "Please Enter  valid Zipcode";
                return false;
            }
            
        }
        
        if ($.trim(txtEmail) != "") {
            if (!regExp.test(txtEmail)) {
                    lbErrorMsg.innerHTML="Enter Email In Proper Format";
                //$('#txtEmail').val('');
                    document.getElementById("<%=txtEmail.ClientID%>").focus();
                    return false;
                }
        }
        //    lbErrorMsg.innerHTML ="Please Enter Email";
          //  document.getElementById("<%=txtEmail.ClientID%>").focus();
        //    return false;
       // }
        //else if (!regExp.test(txtEmail)) {
        //    lbErrorMsg.innerHTML="Enter Email In Proper Format";
        //    $('#txtEmail').val('');
        //    return false;
        //}
         //if (txtAddress1 == "") {
         //   lbErrorMsg.innerHTML = "Please Enter Address";
         //   document.getElementById("<%=txtAdd1.ClientID%>").focus();
        //    return false;
        //}
        //else if (txtCity == "") {
        //    lbErrorMsg.innerHTML = "Please Enter City";
         //   document.getElementById("<%=txtCity.ClientID%>").focus();
        //    return false;
        //}
        //else if (ddlState == "0" || ddlState == "") {
        //    lbErrorMsg.innerHTML = "Please Enter State";
          //  document.getElementById("<%=ddlState.ClientID%>").focus();
          //  return false;
       // }
    lbErrorMsg.innerHTML = "";

    return true;
}
    </script>
 
     <table cellspacing="0" cellpadding="4">
                       
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lbErrorMsg" ForeColor="Red" Font-Bold="true" Font-Names="Vardena"></asp:Label><%--OnClientClick="javascript:return ValidateAll();"--%>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>COMPANY NAME
                            </td>
                            <td>
                                <asp:TextBox ID="txtComapanyName" runat="server" CssClass="modaltextboxlarge" Width="250px" Height="16px"></asp:TextBox>

                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>NAME
                            </td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>EMAIL
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>ADDRESS LINE 1
                            </td>
                            <td>
                                <asp:TextBox ID="txtAdd1" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>ADDRESS LINE 2
                            </td>
                            <td>
                                <asp:TextBox ID="txtAdd2" runat="server" CssClass="modaltextboxlarge"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>CITY
                            </td>
                            <td>
                                <asp:TextBox ID="txtCity" runat="server" CssClass="modaltextboxsmall"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>STATE
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlState" runat="server" CssClass="modalddlSmall"></asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>ZIPCODE
                            </td>
                            <td>
                                <asp:TextBox ID="txtZipCode" runat="server" CssClass="modaltextboxsmall" onkeypress="return Validate(event);"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Is Active
                            </td>
                            <td>
                                 <asp:CheckBox runat="server" ID="chkIsactive" Checked="true" />
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td style="text-align: left;" >
                                  <asp:HiddenField ID="hdnPartyId" Value="" runat="server" />
                                 <asp:HiddenField ID="hdnPartyTypeId" Value="" runat="server" />
                                <asp:Button ID="btnAddNew" runat="server" Text="ADD" OnClientClick="javascript:return ValidateAll();" CssClass="SqureGrayButtonSmall" onmouseover="this.style.backgroundColor='#E0E0E0';"
                                    onmouseout="this.style.backgroundColor='#B8B8B8';" OnClick="btnAddNew_Click" />
                            </td>
                         <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>
                    </table>