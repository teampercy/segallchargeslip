﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucMapShoppingLocation.ascx.cs" Inherits="UserControl_ucMapShoppingLocation" %>


    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<%--    <script src="http ://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>--%>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&Place=true&libraries=places"></script>
    <script src="../Js/GoogleMap/infobox.js"></script>
  
    <script type="text/javascript">
        var overlay;
        var mapgoogle;
        var markersArray = [];
        var strLocationDesc;

        //$(function () {

        //     loadShowMap();
        //});
        google.maps.event.addDomListener(window, 'load', loadShowMap);

        function loadShowMap() {
            var input = document.getElementById('txtSearch');
            var autocomplete = new google.maps.places.Autocomplete(input);
            var latlng = new google.maps.LatLng(41.4925374, -99.9018);
            var myOptions = {
                zoom: 6,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            geocoder = new google.maps.Geocoder();
            mapgoogle = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            NewMapLoad(mapgoogle, geocoder, '');

            $('#btnMapSearch').click(function () {
                debugger;

                //var parentOffset = $('#map_canvas').parent().offset();
                //var point = new google.maps.Point(e.pageX - parentOffset.left, e.pageY - parentOffset.top);
                //var ll = overlay.getProjection().fromContainerPixelToLatLng(point);
                geocoder = new google.maps.Geocoder();
                var address = $('#txtSearch').val();
                deleteOverlays();
                if (address != '') {
                    // $('#hdnlocation').val(address);
                    NewMapLoad(mapgoogle, geocoder, $('#txtSearch').val());
                }
                else { alert('Please enter a Location to Search'); }
            });

            $('#btnCloneLocation').click(function () {
                debugger;

                //var parentOffset = $('#map_canvas').parent().offset();
                //var point = new google.maps.Point(e.pageX - parentOffset.left, e.pageY - parentOffset.top);
                //var ll = overlay.getProjection().fromContainerPixelToLatLng(point);
                geocoder = new google.maps.Geocoder();
                var address = $('#txtSearch').val();
                deleteOverlays();
                if (address != '') {
                    // $('#hdnlocation').val(address);
                    NewMapLoad(mapgoogle, geocoder, 'clone');
                }
                else { alert('Please enter a Location to Search'); }
            });


            $("#draggable").draggable({
                helper: 'clone',
                stop: function (e) {

                    var parentOffset = $('#map_canvas').parent().offset();
                    var point = new google.maps.Point(e.pageX - parentOffset.left, e.pageY - parentOffset.top);
                    var ll = overlay.getProjection().fromContainerPixelToLatLng(point);
                    var address;
                    // deleteOverlays();
                    geocoder = new google.maps.Geocoder();

                    geocoder.geocode({ 'address': "", 'latLng': ll, 'region': 'US' }, function (results, status) {

                        if (status == google.maps.GeocoderStatus.OK) {
                            $('#txtSearch').val("");
                            deleteOverlays();
                            address = results;
                            $('#hdnlocation').val(address[0].address_components[address[0].address_components.length - 1].long_name);
                            //alert($('#hdnlocation').val());
                            mapgoogle.setCenter(results[0].geometry.location);
                            var marker = new google.maps.Marker({
                                map: mapgoogle,
                                position: results[0].geometry.location,
                                title: "Rectified position",
                                draggable: true,
                                icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
                            });
                            markersArray.push(marker);
                            var infowindow = new google.maps.InfoWindow({
                                content: "Search Location",
                                maxWidth: 300,
                                maxHeight: 200
                            });


                            google.maps.event.addListener(marker, 'click', function () {
                                infowindow.open(mapgoogle, marker);
                            });
                            google.maps.event.addListener(mapgoogle, 'click', function () {
                                // alert('Info CLoase');
                                infowindow.close();
                            });
                            setSearchmarker(marker);

                            NewMapLoad(mapgoogle, geocoder, '');
                        } else {
                            alert("Geocode was not successful for the following reason: " + status);
                        }
                    });
                }
            });
        }



        function NewMapLoad(Nmapgoogle, geocoder, searchplace) {
            //   AddMappoints("", "", "", strPopupDiv, geocoder, mapgoogle, PointColor, strAddr);
            var strAddr = "Hollywood, LA"
            var strPopupDiv = "<table cellPadding='0' cellSpacing='0' style='width:100%;'> <tr><td>" + "Initial Point" + "</td></tr></table> ";
            var strSearchDiv = "<table cellPadding='0' cellSpacing='0' style='width:100%;'> <tr><td>" + "Search Point" + "</td></tr></table> ";
            var PointColor = "RED";

            var OrgLocation = document.getElementById('hdnactlocation');
            var OrglatLon = document.getElementById('hdnActLatLon');
            var Orglat = "", OrgLon = "";

            if (OrglatLon != null && OrglatLon.value.split('#').length > 0) {
                orglat = OrglatLon.value.split('#')[0];
                orglon = OrglatLon.value.split('#')[1];
            }
            else if (OrgLocation != null && OrgLocation.value.trim() != "") {
                strAddr = OrgLocation.value;
            }

            AddMappointsNew(orglat, orglon, strPopupDiv, geocoder, mapgoogle, PointColor, strAddr, "Initial Point");

            if (searchplace != '' && searchplace.toLowerCase() != 'clone') {
                AddMappointsNew('', '', strSearchDiv, geocoder, mapgoogle, "GREEN", searchplace, "Search Point");
            }
            if (searchplace.toLowerCase() == 'clone') {
                AddMappointsNew(orglat, orglon, strSearchDiv, geocoder, mapgoogle, "GREEN", strAddr, "Search Point");
            }

            overlay = new google.maps.OverlayView();
            overlay.draw = function () { };
            overlay.setMap(mapgoogle);
        }

        function AddMappointsNew(lat, lon, strpopup, geocoder, mapgoogle, pointcolor, address, title) {
            var point;
            var iconlnk = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
            // debugger;

            if (pointcolor != "" && pointcolor == "GREEN") {
                iconlnk = "http://maps.google.com/mapfiles/ms/icons/green-dot.png";
            }

            geocoder = new google.maps.Geocoder();
            //var addr = strpopup;
            var Latitude = lat;
            var longtitude = lon;
            var addressdiv = $('<div/>').html(strpopup).html();

            if (Latitude != '' && longtitude != '') {
                //add point using lat lan

                var marker = new google.maps.Marker({
                    map: mapgoogle,
                    icon: iconlnk,
                    position: new google.maps.LatLng(Latitude, longtitude),
                    title: title
                });

                marker.setDraggable(true);

                //google.maps.event.addListener(marker, "dragend", function (event) {
                //    var point = marker.getPosition();
                //    map.panTo(point);

                //});

                var infowindow = new google.maps.InfoWindow({
                    content: addressdiv,
                    maxWidth: 370,
                    maxHeight: 230
                });
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(mapgoogle, marker);
                });
                if (pointcolor != "" && pointcolor == "GREEN") {
                    setSearchmarker(marker);
                }

                markersArray.push(marker);
            }
            else {
                geocoder.geocode({ 'address': address }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        mapgoogle.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: mapgoogle,
                            icon: iconlnk,
                            position: results[0].geometry.location,
                            title: title
                        });
                        var infowindow = new google.maps.InfoWindow({
                            content: addressdiv,
                            maxWidth: 300,
                            maxHeight: 200
                        });
                        google.maps.event.addListener(marker, 'click', function () {
                            infowindow.open(mapgoogle, marker);
                        });
                        if (pointcolor != "" && pointcolor == "GREEN") {
                            setSearchmarker(marker);
                        }

                        markersArray.push(marker);
                    } else {
                        alert("Geocode was not successful for the following reason: " + status);
                    }
                });
                //add point using geo code
            }

        }

         

        function setSearchmarker(marker) {
            google.maps.event.addListener(marker, 'dragend', function () {
                document.getElementById('hdnNewlocationLon').value = marker.position.lng();
                document.getElementById('hdnNewlocationLat').value = marker.position.lat();
            });

            marker.setDraggable(true);
            document.getElementById('hdnNewlocationLon').value = marker.position.lng();
            document.getElementById('hdnNewlocationLat').value = marker.position.lat();
            //alert(document.getElementById('hdnNewlocationLon').value + " ____" + document.getElementById('hdnNewlocationLat').value);
            //alert(marker.position.lat() + "_________" + marker.position.lng());
        }
 
        // Deletes all markers in the array by removing references to them
        function deleteOverlays() {
            document.getElementById('hdnNewlocationLon').value = "";
            document.getElementById('hdnNewlocationLat').value = "";
            if (markersArray) {
                for (i in markersArray) {
                    markersArray[i].setMap(null);
                }
                markersArray.length = 0;
            }
        }


        function getLaton() {

            if (document.getElementById('hdnNewlocationLon').value.trim() != "" && document.getElementById('hdnNewlocationLat').value.trim() != "") {

                //set values and //close pop up 
            }
            else {
                alert('Please select New location first');
                //alert(document.getElementById('hdnNewlocationLon').value.trim() + "____" + document.getElementById('hdnNewlocationLat').value.trim());
                return false;
            }
        }
    </script>
    <style type="text/css">
        #draggable {
            /*position: absolute;
        width: 30px;
        height: 30px;*/
            z-index: 1000000000;
        }
    </style>

<body>

    <table style="text-align: center; width:100%; height:100%;">
        <tr>
            <td>
                <input id="searchTextField" type="text" size="50">

                <input id="txtSearch" type="text" placeholder="search address" style="width: 200px; height: 30px;" />
                <input type="hidden" id="hdnNewlocationLon" value="" runat="server"/>
                <input type="hidden" id="hdnNewlocationLat" value="" runat="server" />
                <input type="hidden" id="hdnActLatLon" value=""  runat="server"/>
                <input type="hidden" value=" Hollywood,LA " id="hdnactlocation" />
            </td>
            <td>
                <a href="#" id="btnMapSearch" style="border: none; color: black; cursor: pointer;">
                    <img src="../MasterPage/Images/Map%20Search-Button.gif" title="Search" width="35" height="30" style="border: none; vertical-align: middle;" />
                </a>
            </td>
            <td>
                <a href="#" style="border: none; color: black; cursor: pointer;">
                    <img src="../MasterPage/Images/Map-Set-Location-Button.gif" id="draggable" title="Search" width="35" height="30" style="border: none; vertical-align: middle;" />
                </a>
                <a onclick="return deleteOverlays();">Clear all points</a>
                <asp:Button ID="btnNExt" runat="server" OnClientClick="return getLaton();" Text="NEXT" OnClick="btnNExt_Click" />
                <a onclick="return getLaton();">NEXT</a>
                 <a id="btnCloneLocation" > Clone </a>

            </td>
        </tr>
        <tr>
            <td colspan="3" style="width:700px; height:700px">
                <div id="map_canvas" style="width:100%; height:100%; background-color: GrayText"></div>
            </td>
        </tr>
    </table>
</body>



