﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucNewCharegeReviewSummary.ascx.cs"
    Inherits="UserControl_ucNewCharegeReviewSummary" %>
<%--<link href="../Styles/SiteTEST.css" rel="stylesheet" />--%>
<style type="text/css">
    .tblfixayout
    {
        width: 100%;
        color: black;
        table-layout: fixed;
        padding: 0px 0px 0px 0px;
        border-spacing: 0;
        text-align: center;
        /*cellspacing:0px;
        cellpadding:opx;*/
        padding: 0px 5px 0px 5px;
    }

    /*.tblfixayout tr td {
         height:8px;
        }*/
    .tbOnverFlow
    {
        overflow: hidden;
        width: 50px;
        text-align: left;
        padding: 0px 0px 0px 0px;
    }

    .tbNormal
    {
        overflow: hidden;
        width: 42%;
        padding: 0px 0px 0px 0px;
        white-space: nowrap;
    }

    .tdmain
    {
        /*width: 120px;*/
        width: 43%;
        text-align: left;
        /*text-wrap: initial;*/
    }

    .tdspceing
    {
        /*width: 20px;*/
        width: 9%;
        text-align: right;
    }

    .tdNuneric
    {
        /*width: 60px;*/
        width: 21%;
        text-align: right;
    }

    .auto-style1
    {
        width: 89%;
    }

    .tdBorderBottom
    {
        border-bottom: solid 1px black;
    }

    .tdBorderLeft
    {
        border-left: solid 1px black;
    }

    .tdBorderRight
    {
        border-right: solid 1px black;
    }

    .tdBorderTop
    {
        border-top: solid 1px black;
    }
</style>

<script type="text/javascript">
    function ClearTable() {
        var tbl = document.getElementById("tbSummary");
        if (tbl != null) {
            while (tbl.firstChild) {
                tbl.removeChild(tbl.firstChild);
            }
        }
    }
    window.onload = ClearTable();
</script>
<table id="tbSummary" style="width: 100%; font-size: smaller; background-color: gray;">
    <tr id="tr_BaseHeading" style="display: none; background-color: gray;">

        <td style="display: inline-block;" class="auto-style1 ucheadings" colspan="3">BASE COMMISSION
            <hr />
        </td>

    </tr>
    <tr id="tr_BaseDetails" style="display: none;">
        <%--   <td style="width: 6%"></td> class="Visibility"   style="width: 100%"--%>
        <td style="width: 2%"></td>
        <td style="background-color: white; border: solid 1px; border-color: black; width: 96%; text-align: center;">
            <table style="width: 100%" cellspacing="0" cellpadding="0">
                <tr style="background-color: white;">
                    <td class="OrangeTitle" style="text-align: left;">Commission Split</td>
                </tr>
                <tr>
                    <td id="td_CommSplitSumm">
                        <table id="tb_CommSplitSummBaseTerm" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="background-color: white;">
                    <td class="OrangeTitle" style="text-align: left;">Company Split</td>
                </tr>
                <tr>
                    <td id="td_CompanySplitSumm" style="background-color: white;">
                        <table id="tb_CompSplitSummBaseTerm" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </td>
        <td style="width: 2%"></td>
        <%-- <td style="width: 6%"></td>--%>
    </tr>
    <tr style="display: none;" id="tr_OptFooter">

        <td style="height: 20px; border: none;" colspan="3">
            <br />

        </td>

    </tr>
    <tr style="display: none; width: 100%" id="tr_OptFooter1">

        <td style="background-color: white; display: block; height: 20px;" colspan="3">
            <br />
        </td>

    </tr>
    <tr id="tr_OptHeading" style="display: none">
        <td style="display: inline-block;" class="auto-style1 ucheadings" colspan="3">OPTION 1 COMMISSION
            <hr />
        </td>

    </tr>
    <tr id="tr_OptDetails" style="display: none;">
        <td style="width: 2%"></td>
        <td style="width: 96%; background-color: white; border: solid 1px; border-color: black; text-align: center;">
            <table style="width: 100%">
                <tr style="background-color: white;">
                    <td class="OrangeTitle" style="text-align: left;">Due Date(s) </td>
                </tr>
                
                <tr>
                    <td id="">
                        <table id="tb_DueDatesOpt" style="background-color: white;" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                        <br /> 
                    </td>
                </tr>
                <tr style="background-color: white;">
                    <td class="OrangeTitle" style="text-align: left;">Commission Split</td>
                </tr>
                <tr>
                    <td id="td1" style="background-color: white;">
                        <table id="tb_CommSplitSummOptTerm" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="background-color: white; display: none;">
                    <td class="OrangeTitle" style="text-align: left;">Company Split</td>
                </tr>
                <tr style="display: none;">
                    <td id="td_CompanySplitSummOpt" style="background-color: white;">
                        <table id="tb_CompSplitSummOptTerm" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </td>
        <td style="width: 2%"></td>
    </tr>
   
     <tr style="display: none;" id="tr_Opt1Footer">

        <td style="height: 20px; border: none;" colspan="3">
            <br />

        </td>

    </tr>
    <tr style="display: none; width: 100%" id="tr_Opt1Footer1">

        <td style="background-color: white; display: block; height: 20px;" colspan="3">
            <br />
        </td>

    </tr>
     <tr id="tr_Opt1Heading" style="display: none">
        <td style="display: inline-block;" class="auto-style1 ucheadings" colspan="3">OPTION 2 COMMISSION
            <hr />
        </td>

    </tr>
    <tr id="tr_Opt1Details" style="display: none;">
        <td style="width: 2%"></td>
        <td style="width: 96%; background-color: white; border: solid 1px; border-color: black; text-align: center;">
            <table style="width: 100%">
                <tr style="background-color: white;">
                    <td class="OrangeTitle" style="text-align: left;">Due Date(s) </td>
                </tr>
                
                <tr>
                    <td id="">
                        <table id="tb_DueDatesOpt1" style="background-color: white;" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                        <br /> 
                    </td>
                </tr>
                <tr style="background-color: white;">
                    <td class="OrangeTitle" style="text-align: left;">Commission Split</td>
                </tr>
                <tr>
                    <td id="td4" style="background-color: white;">
                        <table id="tb_CommSplitSummOptTerm1" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="background-color: white; display: none;">
                    <td class="OrangeTitle" style="text-align: left;">Company Split</td>
                </tr>
                <tr style="display: none;">
                    <td id="td_CompanySplitSummOpt1" style="background-color: white;">
                        <table id="tb_CompSplitSummOptTerm1" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </td>
        <td style="width: 2%"></td>
    </tr>

     <tr style="display: none;" id="tr_Opt2Footer">

        <td style="height: 20px; border: none;" colspan="3">
            <br />

        </td>

    </tr>
    <tr style="display: none; width: 100%" id="tr_Opt2Footer1">

        <td style="background-color: white; display: block; height: 20px;" colspan="3">
            <br />
        </td>

    </tr>
     <tr id="tr_Opt2Heading" style="display: none">
        <td style="display: inline-block;" class="auto-style1 ucheadings" colspan="3">OPTION 3 COMMISSION
            <hr />
        </td>

    </tr>
    <tr id="tr_Opt2Details" style="display: none;">
        <td style="width: 2%"></td>
        <td style="width: 96%; background-color: white; border: solid 1px; border-color: black; text-align: center;">
            <table style="width: 100%">
                <tr style="background-color: white;">
                    <td class="OrangeTitle" style="text-align: left;">Due Date(s) </td>
                </tr>
                
                <tr>
                    <td id="">
                        <table id="tb_DueDatesOpt2" style="background-color: white;" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                        <br /> 
                    </td>
                </tr>
                <tr style="background-color: white;">
                    <td class="OrangeTitle" style="text-align: left;">Commission Split</td>
                </tr>
                <tr>
                    <td id="td5" style="background-color: white;">
                        <table id="tb_CommSplitSummOptTerm2" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="background-color: white; display: none;">
                    <td class="OrangeTitle" style="text-align: left;">Company Split</td>
                </tr>
                <tr style="display: none;">
                    <td id="td_CompanySplitSummOpt2" style="background-color: white;">
                        <table id="tb_CompSplitSummOptTerm2" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </td>
        <td style="width: 2%"></td>
    </tr>

     <tr style="display: none;" id="tr_Opt3Footer">

        <td style="height: 20px; border: none;" colspan="3">
            <br />

        </td>

    </tr>
    <tr style="display: none; width: 100%" id="tr_Opt3Footer1">

        <td style="background-color: white; display: block; height: 20px;" colspan="3">
            <br />
        </td>

    </tr>
    <tr id="tr_Opt3Heading" style="display: none">
        <td style="display: inline-block;" class="auto-style1 ucheadings" colspan="3">OPTION 4 COMMISSION
            <hr />
        </td>

    </tr>
    <tr id="tr_Opt3Details" style="display: none;">
        <td style="width: 2%"></td>
        <td style="width: 96%; background-color: white; border: solid 1px; border-color: black; text-align: center;">
            <table style="width: 100%">
                <tr style="background-color: white;">
                    <td class="OrangeTitle" style="text-align: left;">Due Date(s) </td>
                </tr>
                
                <tr>
                    <td id="">
                        <table id="tb_DueDatesOpt3" style="background-color: white;" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                        <br /> 
                    </td>
                </tr>
                <tr style="background-color: white;">
                    <td class="OrangeTitle" style="text-align: left;">Commission Split</td>
                </tr>
                <tr>
                    <td id="td6" style="background-color: white;">
                        <table id="tb_CommSplitSummOptTerm3" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="background-color: white; display: none;">
                    <td class="OrangeTitle" style="text-align: left;">Company Split</td>
                </tr>
                <tr style="display: none;">
                    <td id="td_CompanySplitSummOpt3" style="background-color: white;">
                        <table id="tb_CompSplitSummOptTerm3" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </td>
        <td style="width: 2%"></td>
    </tr>

     <tr style="display: none;" id="tr_ConFooter1">

        <td style="height: 20px;" colspan="3">
            <br />

        </td>

    </tr>
    <tr style="display: none; width: 100%" id="tr_ConFooter">

        <td style="height: 20px; background-color: white; width: 100%; display: inline-block; height: 20px;" colspan="3">
            <br />

        </td>

    </tr>
    <tr id="tr_ConHeading" style="display: none;">

        <td style="display: inline-block;" class="auto-style1 ucheadings" colspan="3">
            <label>CONTINGENT COMMISSION </label>
            <hr />
        </td>

    </tr>
    <tr id="tr_ConDetails" style="display: none;">
        <td style="width: 2%"></td>
        <td style="width: 96%; background-color: white; border: solid 1px; border-color: black; text-align: center;">
            <table style="width: 100%">
                <tr style="background-color: white;">
                    <td class="OrangeTitle" style="text-align: left;">Due Date(s)</td>
                </tr>
                <tr>
                    <td id="Td2">
                        <table id="tb_DueDatesCon" style="background-color: white;" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                        <br /> 
                    </td>
                </tr>
                <tr style="background-color: white;">
                    <td class="OrangeTitle" style="text-align: left;">Commission Split</td>
                </tr>
                <tr>
                    <td id="td3">
                        <table id="tb_CommSplitSummCntTerm" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="background-color: white;">
                    <td class="OrangeTitle" style="text-align: left; display: none;">Company Split</td>
                </tr>
                <tr style="display: none;">
                    <td id="td_CompanySplitSummCon" style="background-color: white;">
                        <table id="tb_CompSplitSummConTerm" class="tblfixayout">
                            <tr>
                                <td class="tdmain"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                                <td class="tdspceing"></td>
                                <td class="tdNuneric"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>

        </td>
        <td style="width: 2%"></td>

    </tr>
    <tr style="display: none; width: 100%" id="tr_ConFooter2">

        <td style="height: 20px; background-color: gray; width: 100%; display: inline-block;" colspan="3">
            <br />

        </td>

    </tr>

</table>
