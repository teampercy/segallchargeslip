﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucGridsPaymentHistory.ascx.cs"
    Inherits="UserControl_ucGridsPaymentHistory" %>


<style>
    .popup {
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin:auto;
        position: fixed;
        width: 500px;
        height: 250px;
        border: solid 2px black;
        background-color: white;
        z-index: 1002;
     
        /*overflow: auto;*/
        text-align: left;
    }
  
</style>


<table id="tblPaymentDistribution" runat="server" style="width: 100%; font-size: 12px;">
    <tr>
        <td colspan="2">
            <br />
        </td>
    </tr>
    <tr>
        <td>Payment Distribution:<asp:Label ID="lblPaymentDistribution" runat="server" Text=""></asp:Label>
            <asp:Label ID="lblUserName" runat="server" Text=""></asp:Label>
        </td>
        <td style="text-align: right;">Commission:<asp:Label ID="lblCommissionAmount" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:GridView ID="gvPaymentDistribution" CssClass="gv" ShowFooter="True" Font-Size="12px" font-family="Verdana"
                Width="100%" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvPaymentDistribution_RowDataBound">
                <Columns>

                   <%-- <asp:TemplateField ControlStyle-BackColor="White" ItemStyle-Width="1px">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPayemntHistoryID" Text='<%#Eval("PaymentHistoryId")%>' Style="display: none"></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>--%>
                    <asp:BoundField DataField="PaymentHistoryId" HeaderText="PaymentHistoryId" ItemStyle-HorizontalAlign="Center" Visible="false" />
                    <asp:BoundField DataField="BrokerRole" HeaderText="ROLE" ItemStyle-HorizontalAlign="Center" />
                    <asp:TemplateField HeaderText="CHECK AMT" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            $<asp:Label ID="lblCHECKAMT" runat="server" Text='<%# String.Format("{0:f2}",Eval("CheckAmount"))%>'></asp:Label>
                         </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lbltotalCHECKAMT" runat="server" Text=""></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="% DUE" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lblPercent" runat="server" Text='<%# String.Format("{0:n2}",Eval("BrokerPercent"))%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--                    <asp:BoundField DataField="BrokerPercent" HeaderText="% DUE" ItemStyle-HorizontalAlign="Right" />--%>
                    <asp:TemplateField HeaderText="AMOUNT DUE" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label ID="lbldollar" runat ="server" Text="$"></asp:Label>
                          <%-- <span style="color:#7853C9;font-weight:bold;"> $</span>--%>
                            <asp:LinkButton  ID="lnkAmountDue" runat ="server" Text='<%# String.Format("{0:f2}",Eval("AmountDue"))%>' OnClientClick=<%#"javascript:DisplayPaymentMessage('" + Eval("PaymentHistoryId") + "');return false;" %>></asp:LinkButton>
                            <%--<asp:Label ID="lblAMOUNTDUE" runat="server" Text='<%# String.Format("{0:f2}",Eval("AmountDue"))%>'></asp:Label>--%>
                            
                            <asp:ImageButton ID="btnPaymentAmount" runat ="server" ImageUrl="~/Images/edit.jpg" 
                                OnClientClick=<%# "javascript:ShowPopUpPayment('" + Eval("PaymentHistoryId") +"','"+Eval("ChargeSlipID")+"','"+Eval("AmountDue")+ "');return false;" %>
                                Height="15px" Width="15px"/>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lbltotalAMOUNTDUE" runat="server" Text=""></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                  <%--   <asp:TemplateField HeaderText="AMOUNT DUE" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtAmountDue" Style="display: none"></asp:TextBox>
                            <asp:Label ID="lblAMOUNTDUE" runat="server" Text='<%# String.Format("{0:f2}",Eval("AmountDue"))%>'></asp:Label>
                            <asp:ImageButton runat="server" ID="imgEditbtnBroker" ImageUrl="~/Images/edit.jpg" AlternateText="Edit" OnClientClick="imgEditbtn_ClientBrokerAmountClick(this);return false;"></asp:ImageButton>
                            <asp:ImageButton runat="server" ID="imgSavebtnBroker" ImageUrl="~/Images/iconSave.png" AlternateText="Save" Style="display: none" text="save" OnClientClick="imgSavebtn_ClientBrokerAmountClick(this);return false;"></asp:ImageButton>
                            <asp:Label runat="server" ID="lblPayemntHistorysID" Text='<%#Eval("PaymentHistoryId")%>' Style="display: none"></asp:Label>
                            <asp:Label runat="server" ID="lblChargeslipid" Text='<%#Eval("ChargeslipId")%>' Style="display: none"></asp:Label>

                           
                        </ItemTemplate>
                              <FooterTemplate>
                            <asp:Label ID="lbltotalAMOUNTDUE" runat="server" Text=""></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="SPLIT" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblSplit" runat="server" Text='<%# Eval("Split")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="SHARE" ItemStyle-HorizontalAlign="right">
                        <ItemTemplate>
                            $<asp:Label ID="lblShare" runat="server" Text='<%# String.Format("{0:f2}",Eval("PaymentReceived"))%>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lbltotalShare" runat="server" Text=""></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PaymentDate" HeaderText="PAYMENT DATE" ItemStyle-HorizontalAlign="Center" />

                    <asp:BoundField DataField="CheckNo" ControlStyle-Width="20px" Visible="false" HeaderText="CHECK/WIRE NUMBER" ItemStyle-HorizontalAlign="Center" />
                    <asp:TemplateField HeaderText="CHECK/WIRE NUMBER" ItemStyle-HorizontalAlign="right">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtCheckNo" Style="display: none"></asp:TextBox>
                            <asp:Label ID="lblTestCheckNo" runat="server" Text='<%# String.Format("{0:f2}",Eval("CheckNo"))%>'></asp:Label>
                            <asp:ImageButton runat="server" ID="imgEditbtn" ImageUrl="~/Images/edit.jpg" AlternateText="Edit" OnClientClick="imgEditbtn_ClientClick(this);return false;"></asp:ImageButton>
                            <asp:ImageButton runat="server" ID="imgSavebtn" ImageUrl="~/Images/iconSave.png" Width="13px" Height="13px" AlternateText="Save" Style="display: none;height:13px;width:13px;" text="save" OnClientClick="imgSavebtn_ClientClick(this);return false;"></asp:ImageButton>
                              <asp:ImageButton ID="imgCancel" runat="server" ImageUrl="~/Images/close.png" Width="12px" Height="12px" Style="display: none;width:12px;height:12px;" AlternateText="Cancel" OnClientClick="imgCancelClick(this);return false;" />
                            <asp:Label runat="server" ID="lblPayemntHistoryID" Text='<%#Eval("PaymentHistoryId")%>' Style="display: none"></asp:Label>
                            <%-- <input type="image" onclick="imgEditbtn_ClientClick" value="Edit" style="background-color:yellowgreen" />--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PAYMENT" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            $<asp:Label ID="lblPaymentReceived" runat="server" Text='<%# String.Format("{0:f2}",Eval("PaymentReceived"))%>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblPaymentReceived" runat="server" Text=""></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
                <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                <HeaderStyle CssClass="HeaderGridView2" />
                <RowStyle CssClass="RowGridView2" />
                <SelectedRowStyle CssClass="SelectedRowGridView2" />
                <FooterStyle CssClass="FooterGridView2" />
                <%--  <AlternatingRowStyle Height="10px" BackColor="#F5F6CE" />
                <HeaderStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />
                <RowStyle BackColor="#ffffff" Height="10px" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#F7BE81" />
                <FooterStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />--%>
            </asp:GridView>
        </td>
    </tr>
</table>
