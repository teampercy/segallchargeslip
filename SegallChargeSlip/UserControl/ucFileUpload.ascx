﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucFileUpload.ascx.cs" Inherits="UserControl_ucFileUpload" %>
<%--<link href="../Styles/SiteTEST.css" rel="stylesheet" />--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<style type="text/css">
    .auto-style1 {
        height: 18px;
        width: 42%;
    }
</style>
<table style="background-color: gray; width: 100%">
    <tr>
        <td style="vertical-align: top; width: 25%; padding-left: 15px; height: 18px;"><b>NOTES</b>
        </td>
        <td>
            <asp:TextBox Rows="10" runat="server" TextMode="MultiLine" Columns="35" ID="txtNotes"></asp:TextBox>
            <%-- <div style ="float:right;padding-right:55px;">--%>
            <%--<asp:ImageButton  ID="imgAttach" ImageUrl ="~/Images/attachment.jpg" Height="20px" Width ="20px" runat ="server" />
               
               
               <ajax:ModalPopupExtender ID="mpExtendr" runat="server" PopupControlID="dvFileUpload" TargetControlID="imgAttach" CancelControlID="btnCancelPopup" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>--%>

            <%-- </div>--%>
        </td>
        <td style="padding-bottom: 160px; padding-right: 60px;">
            <asp:ImageButton ID="imgAttach" ImageUrl="~/Images/attachment.jpg" Height="20px" Width="20px" runat="server" />
             
            <ajax:ModalPopupExtender ID="mpExtendr" runat="server" PopupControlID="dvFileUpload1" TargetControlID="imgAttach" CancelControlID="btnCancelPopup" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top; width: 25%; padding-left: 15px; height: 18px;"></td>
        <td colspan="2">
            <asp:UpdatePanel ID="upAtttachment" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnAttachment" runat="server" Height="100px" ScrollBars="Auto" Width="300px">
                        <asp:GridView ID="gvAttach" GridLines="None" runat="server" AutoGenerateColumns="false" Width="300px" ShowHeader="false">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Image ID="imgDot" runat="server" ImageUrl="~/Images/bullet.gif" BackColor="Gray" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="AttachmentDescription" HeaderText="Attachment" />
                                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtDelete" OnClientClick="return confirm('Are you sure want to delete')" ImageUrl="~/Images/delete.gif" runat="server" OnClick="imgbtDelete_Click" CommandArgument='<%# Eval("UniqueAttachmentDescription") %>'></asp:ImageButton>

                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </ContentTemplate>

            </asp:UpdatePanel>
            <%-- <hr style="width: 100%" />--%>
        </td>
    </tr>
</table>
<div class="PopupDivBodyStyle" id="dvFileUpload1" style="display: none; border: 5px solid rgb(255, 255, 255);">
    <asp:UpdatePanel ID="upAttachment" runat="server" UpdateMode="Conditional">

        <ContentTemplate>
            <table align="centre" style="padding-top: 20px;">
                <tr style="background-color: gray;">
                    <td><b>File Upload</b></td>
                    <td style="padding-left: 39px;">
                        <asp:ImageButton ID="btnCancelPopup" Height="15px" Width="15px" ImageUrl="~/Images/close.png" runat="server" />

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:FileUpload ID="upFileUpload" runat="server" /></td>
                    <td>
                        <asp:Button ID="btnFileUpload" runat="server" CssClass="SqureButton" OnClick="btnFileUpload_Click" Text="ATTACH" />
                    </td>
                </tr>

            </table>
        </ContentTemplate>
        <Triggers>

            <asp:PostBackTrigger ControlID="btnFileUpload" />
        </Triggers>
    </asp:UpdatePanel>
</div>
<%--<div  id="divAttachment" runat ="server"  >
    <table>
       
    </table>
</div>--%>
