﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;

public partial class UserControl_ucChargeDetailCard : System.Web.UI.UserControl
{

    public int DealTransactionID
    {
        get;
        set;
    }
    public int ChargeSlipId
    {
        get;
        set;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Page.Form.Enctype = "multipart/form-data";
        //Bindata();
        if (Session["FileAttachment"] != null)
        {
            DataTable dt = (DataTable)Session["FileAttachment"];
            PlaceHolder place = (PlaceHolder)FormView1.FindControl("phFileAttach") as PlaceHolder;

            Label lblAttachments = (Label)FormView1.FindControl("lblAttachments");
            if (place != null && lblAttachments != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    lblAttachments.Text = "Attachments";
                    LinkButton btn = new LinkButton();

                    string fileToSelect = "../Uploads1/" + dt.Rows[i]["AttachmentDescription"].ToString();
                    btn.OnClientClick = "javascript:return OpenWindow('" + fileToSelect + "');  ";
                    btn.CommandArgument = dt.Rows[i]["AttachmentDescription"].ToString();
                    string attachment = dt.Rows[i]["AttachmentDescription"].ToString();
                    int count = attachment.IndexOf('_');
                    btn.Text = attachment.Substring(count + 1).ToString() + "<br />";
                    place.Controls.Add(btn);
                }
            }
        }
    }

    public void Bindata()
    {
        try
        {
            DealTransactions objDealTransactions = new DealTransactions();
            objDealTransactions.ChargeSlipID = ChargeSlipId;
            Session["ChargeSlipId"] = ChargeSlipId;
            Int64 UserId = Convert.ToInt64(Session["UserID"].ToString());
            DataSet ds = objDealTransactions.GetChargeSlipDetails(UserId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                lblDealName.Text = ds.Tables[0].Rows[0]["DealName"].ToString();
               
                DateTime createdon = Convert.ToDateTime(ds.Tables[0].Rows[0]["CreatedOn"]);
                DateTime LastModifiedBy = Convert.ToDateTime(ds.Tables[0].Rows[0]["LastModifiedBy"]);
                if (LastModifiedBy > createdon)
                    lblDealDate.Text = "Created: " + ds.Tables[0].Rows[0]["CreatedOn"].ToString() + "  Last Updated: " + ds.Tables[0].Rows[0]["LastModifiedBy"].ToString();
                else
                    lblDealDate.Text = "Created: " + ds.Tables[0].Rows[0]["CreatedOn"].ToString();
                // Label lblBuyer1 = (Label)FormView2.FindControl("lblBuyer1");
                // Label lblBuyer = (Label)FormView2.FindControl("lblBuyer");

            }

            FormView2.DataSource = ds.Tables[0];
            FormView2.DataBind();
            FormView3.DataSource = ds.Tables[0];
            FormView3.DataBind();
            FormView4.DataSource = ds.Tables[0];
            FormView4.DataBind();
            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["DeleteReason"].ToString()))
            signin.Style.Add("height", "740px");
            
            if (ds.Tables[0].Rows.Count > 0)
            {
                HtmlTableRow trEmails = new HtmlTableRow();
                trEmails = (HtmlTableRow)FormView3.FindControl("trEmails");
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["Email"].ToString()))
                {
                    trEmails.Visible = true;
                }
                else
                {
                    trEmails.Visible = false;
                }
            }

            if (ds.Tables[1].Rows.Count > 0)
            {
                ImageButton1.Visible = true;
                gvCharge.DataSource = ds.Tables[1];
                gvCharge.DataBind();
                //ImageButton1.Visible = true;
                // LineCharge.Visible = true;
            }
            else
            {
                gvCharge.DataSource = null;
                gvCharge.DataBind();
                ImageButton1.Visible = false;
                // LineCharge.Visible = false;
            }

            FormView1.DataSource = ds.Tables[0];
            FormView1.DataBind();
           
            Label lblTenant1 = (Label)FormView1.FindControl("lblTenantLabel");
            Label lblTenant = (Label)FormView1.FindControl("lblTenantText");
            Label lblParcelSize = (Label)FormView1.FindControl("lblParcelSize");
            HtmlTableRow trSize = new HtmlTableRow();
            trSize = (HtmlTableRow)FormView1.FindControl("trSize");
            
            HtmlTableRow trUse = new HtmlTableRow();
            trUse = (HtmlTableRow)FormView1.FindControl("trUse");
            HtmlTableRow trNets = new HtmlTableRow();
            trNets = (HtmlTableRow)FormView1.FindControl("trNets");
            HtmlTableRow trTI = new HtmlTableRow();
            trTI = (HtmlTableRow)FormView1.FindControl("trTI");
            HtmlTableRow trFreeRent = new HtmlTableRow();
            trFreeRent = (HtmlTableRow)FormView1.FindControl("trFreeRent");
            HtmlTableRow trTerm = new HtmlTableRow();
            trTerm = (HtmlTableRow)FormView1.FindControl("trTerm");
            HtmlTableRow trTenantBuyer = new HtmlTableRow();
            trTenantBuyer = (HtmlTableRow)FormView1.FindControl("trTenantBuyer");
            HtmlTableRow trRentPSF = new HtmlTableRow();
            trRentPSF = (HtmlTableRow)FormView1.FindControl("trRentPSF");
            HtmlTableRow trAggregateRent = new HtmlTableRow();
            trAggregateRent = (HtmlTableRow)FormView1.FindControl("trAggregateRent");
            HtmlTableRow trCommissionable = new HtmlTableRow();
            trCommissionable = (HtmlTableRow)FormView1.FindControl("trCommissionable");
            HtmlTableRow trCommissionper = new HtmlTableRow();
            trCommissionper = (HtmlTableRow)FormView1.FindControl("trCommissionper");
             HtmlTableRow trBuildingSize = new HtmlTableRow();
            trBuildingSize = (HtmlTableRow)FormView1.FindControl("trBuildingSize");
            HtmlTableRow trSalePrice = new HtmlTableRow();
            trSalePrice = (HtmlTableRow)FormView1.FindControl("trSalePrice");



            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["DealType"].ToString().ToLower() == "sale")
                    {
                        if (trTenantBuyer != null)
                        {
                            trTenantBuyer.Visible = true;
                        }
                        if (trSize != null)
                        {
                            trSize.Visible = true;
                        }
                        if (trUse != null)
                        {
                            trUse.Visible = true;
                        }
                        trSalePrice.Visible = true;
                        if (ds.Tables[0].Rows[0]["DealSubtype"].ToString().ToLower() == "improved")
                        {
                            trBuildingSize.Visible = true;
                            lblParcelSize.Text = "Land Size:";
                        }
                    }
                    else if (ds.Tables[0].Rows[0]["DealType"].ToString().ToLower() == "lease")
                    {
                        if (ds.Tables[0].Rows[0]["DealSubtype"].ToString().ToLower() == "ground")
                        {
                           
                            lblParcelSize.Text = "Parcel Size:";
                        }
                        if (trTenantBuyer != null)
                        {
                            trTenantBuyer.Visible = true;
                        }
                        if (trSize != null)
                        {
                            trSize.Visible = true;
                        }
                        if (trUse != null)
                        {
                            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["DealUse"].ToString()))
                            {
                                trUse.Visible = true;
                            }
                        }
                        if (trNets != null)
                        {
                            trNets.Visible = true;
                        }
                        if (trTI != null)
                        {
                            trTI.Visible = true;
                        }

                        if (trFreeRent != null)
                        {
                            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["FreeRent"].ToString()))
                            {
                                Label lblFreeRentUnit = (Label)FormView1.FindControl("lblFreeRentUnit");
                                if (lblFreeRentUnit != null)
                                {
                                    if (ds.Tables[0].Rows[0]["FreeRentUnitTypeValue"].ToString() == "1")
                                    {
                                        lblFreeRentUnit.Text = "Days";
                                    }
                                    else if (ds.Tables[0].Rows[0]["FreeRentUnitTypeValue"].ToString() == "2")
                                    {
                                        lblFreeRentUnit.Text = "Months";
                                    }
                                    else if (ds.Tables[0].Rows[0]["FreeRentUnitTypeValue"].ToString() == "3")
                                    {
                                        lblFreeRentUnit.Text = "Years";
                                    }


                                }
                                trFreeRent.Visible = true;
                            }
                        }
                        if (trTerm != null)
                        {
                            trTerm.Visible = true;
                        }
                        if (trRentPSF != null)
                        {
                            trRentPSF.Visible = true;
                        }
                        if (trAggregateRent != null)
                        {
                            trAggregateRent.Visible = true;
                        }
                        if (trCommissionable != null)
                        {
                            trCommissionable.Visible = true;
                        }
                        if (trCommissionper != null)
                        {
                            trCommissionper.Visible = true;
                        }

                    }
                    else if (ds.Tables[0].Rows[0]["DealType"].ToString().ToLower() == "consulting")
                    {

                    }
                }
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["Buyer"].ToString()))
                {
                    lblTenant1.Text = "Buyer:";
                    lblTenant.Text = ds.Tables[0].Rows[0]["PartyName"].ToString();
                }
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["Tenant"].ToString()))
                {
                    lblTenant1.Text = "Tenant:";
                    lblTenant.Text = ds.Tables[0].Rows[0]["PartyName"].ToString();
                }
            }
            Label lblCommissionDate = (Label)FormView1.FindControl("lblCommissionDate");
            Label lblpercent = (Label)FormView1.FindControl("lblpercent");
            Label lblamount = (Label)FormView1.FindControl("lblamount");
            for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
            {

                lblCommissionDate.Text += " " + ds.Tables[2].Rows[i]["DueDate"].ToString() + "<br />";
                lblpercent.Text += "" + ds.Tables[2].Rows[i]["PercentComm"].ToString() + "%" + "<br />";
                lblamount.Text += "$" + String.Format("{0:n2}", Convert.ToDecimal(ds.Tables[2].Rows[i]["CommAmountDue"].ToString())) + "<br />";

                //lblCommissionDate.Text += " " + ds.Tables[2].Rows[i]["DueDate"].ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + ds.Tables[2].Rows[i]["PercentComm"].ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$" + String.Format("{0:n2}", Convert.ToDecimal(ds.Tables[2].Rows[i]["CommAmountDue"].ToString())) + "<br />";
            }
            Label lblCobrokers = (Label)FormView1.FindControl("lblCobrokers");
            Label lblCoBroker = (Label)FormView1.FindControl("lblCoBroker");
            if (ds.Tables[3].Rows.Count > 0)
            {
                lblCoBroker.Visible = true;
                for (int i = 0; i < ds.Tables[3].Rows.Count; i++)
                {
                    lblCobrokers.Text += " " + ds.Tables[3].Rows[i]["CoBrokerName"].ToString() + "<br />";
                }
            }
            else
            {
                lblCoBroker.Visible = false;
            }
            Label lblAttachments = (Label)FormView1.FindControl("lblAttachments");
            PlaceHolder place = (PlaceHolder)FormView1.FindControl("phFileAttach") as PlaceHolder;
            
            for (int i = 0; i < ds.Tables[4].Rows.Count; i++)
            {
                Session["FileAttachment"] = ds.Tables[4];
                lblAttachments.Text = "Attachments";
                LinkButton btn = new LinkButton();

                string fileToSelect = "../Uploads1/" + ds.Tables[4].Rows[i]["AttachmentDescription"].ToString();
                btn.OnClientClick = "javascript:return OpenWindow('" + fileToSelect + "');  ";
                btn.CommandArgument = ds.Tables[4].Rows[i]["AttachmentDescription"].ToString();
                string attachment = ds.Tables[4].Rows[i]["AttachmentDescription"].ToString();
                int count = attachment.IndexOf('_');
                btn.Text = attachment.Substring(count + 1).ToString() + "<br />";
                place.Controls.Add(btn);
            }

            if (ds.Tables[5].Rows.Count > 0)
            {
                gvBaseCommision.DataSource = ds.Tables[5];
                gvBaseCommision.DataBind();
                LineBaseCommision.Visible = true;
                imgBaseCommision.Visible = true;
                lblBaseCommision.Visible = true;
                tblBaseCommision.Visible = true;
            }
            else
            {
                gvBaseCommision.DataSource = null;
                gvBaseCommision.DataBind();
                LineBaseCommision.Visible = false;
                imgBaseCommision.Visible = false;
                lblBaseCommision.Visible = false;
                tblBaseCommision.Visible = false;
            }
            gvContingentTerm.DataSource = null;
            gvContingentTerm.DataBind();
            gvOptionTermPayable.DataSource = null;
            gvOptionTermPayable.DataBind();
            gvOPTIONCOMMISSION.DataSource = null;
            gvOPTIONCOMMISSION.DataBind();
            if (ds.Tables[6] != null)
            {
                if (ds.Tables[6].Rows.Count > 0)
                {
                    HtmlTableRow tr;
                    HtmlTableCell tc;
                    Label lblUserName;
                    Label lblCompanySplit;
                    Label lblCompanySplitAmount;
                    Label lblUserNames;
                    Label lblUserAmount;
                    Label lblSegallGroups;
                    Label lblSegallGroupAmount;
                    for (int i = 0; i < ds.Tables[6].Rows.Count; i++)
                    {
                        decimal splitAmount = 0;
                        splitAmount = Convert.ToDecimal(ds.Tables[6].Rows[i]["CommSplitPercent"].ToString());


                        lblUserName = new Label();
                        lblUserName.ID = "lblUserName" + i;
                        lblUserName.Text = ds.Tables[6].Rows[i]["BrokerName"].ToString() + ":";
                        lblUserName.CssClass = "FontWeight";
                        tr = new HtmlTableRow();
                        tc = new HtmlTableCell();
                        tc.Controls.Add(lblUserName);
                        tr.Cells.Add(tc);
                        tblBaseCommision.Rows.Add(tr);

                        lblCompanySplit = new Label();
                        lblCompanySplit.ID = "lblCompanySplit" + i;
                        lblCompanySplit.Text = "Company Split:";
                        tr = new HtmlTableRow();
                        tc = new HtmlTableCell();
                        tc.Controls.Add(lblCompanySplit);
                        tr.Cells.Add(tc);
                        lblCompanySplitAmount = new Label();
                        lblCompanySplitAmount.ID = "lblCompanySplitAmount" + i;
                        lblCompanySplitAmount.Text = String.Format("{0:n2}", splitAmount) + ":" + String.Format("{0:n2}", (Convert.ToDecimal(100) - splitAmount));
                        tc.Controls.Add(lblCompanySplitAmount);
                        tr.Cells.Add(tc);

                        lblUserNames = new Label();
                        lblUserNames.ID = "lblUserNames" + i;
                        //lblUserNames.Text = "User Name:";
                        lblUserNames.Text = ds.Tables[6].Rows[i]["BrokerName"].ToString() + ":"; 
                        tc = new HtmlTableCell();
                        tc.Controls.Add(lblUserNames);
                        tr.Cells.Add(tc);
                        lblUserAmount = new Label();
                        lblUserAmount.ID = "lblUserAmount" + i;
                        lblUserAmount.Text = "$" + String.Format("{0:n2}", Convert.ToDecimal(ds.Tables[6].Rows[i]["UserSplitAmount"].ToString()));
                        tc.Controls.Add(lblUserAmount);
                        tr.Cells.Add(tc);

                        lblSegallGroups = new Label();
                        lblSegallGroups.ID = "lblSegallGroups" + i;
                        lblSegallGroups.Text = "Segall Group:";
                        tc = new HtmlTableCell();
                        tc.Controls.Add(lblSegallGroups);
                        tr.Cells.Add(tc);
                        lblSegallGroupAmount = new Label();
                        lblSegallGroupAmount.ID = "lblSegallGroupAmount" + i;
                        lblSegallGroupAmount.Text = "$" + String.Format("{0:n2}", (Convert.ToDecimal(ds.Tables[6].Rows[i]["BrokerCommission"].ToString()) - Convert.ToDecimal(ds.Tables[6].Rows[i]["UserSplitAmount"].ToString())));
                        tc.Controls.Add(lblSegallGroupAmount);
                        tc.Style.Add("text-align", "left");
                        tc.Style.Add("padding-left", "20px;");
                        tr.Cells.Add(tc);
                        tblBaseCommision.Rows.Add(tr);

                    }
                }

            }
            if (ds.Tables[7].Rows.Count > 0)
            {
                lblContingentTerm.Visible = false;
                lblOptionTermPayable.Visible = false;
                lblOPTIONCOMMISSION.Visible = false;
                tdOPTIONCOMMISSION.Visible = false;
                tdOptionTermPayable.Visible = false;
                tblOPTIONCOMMISSION.Visible = false;
                tdContingentTerm.Visible = false;
                if (Convert.ToInt32(ds.Tables[7].Rows[0]["OptionLeaseTerm"].ToString()) > 0)
                {
                    if (ds.Tables[8].Rows.Count > 0)
                    {
                        lblOPTIONCOMMISSION.Visible = true;
                        tdOPTIONCOMMISSION.Visible = true;
                        tblOPTIONCOMMISSION.Visible = true;
                        gvOPTIONCOMMISSION.DataSource = ds.Tables[8];
                        gvOPTIONCOMMISSION.DataBind();

                        //diptee
                        decimal totOptComm = 0;
                        lblCommissionDateOption.Text = string.Empty;
                        lblpercentOption.Text = string.Empty;
                        lblamountOption.Text = string.Empty;
                        if (ds.Tables[9].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[9].Rows.Count; i++)
                            {
                                lblCommissionDateOption.Text += " " + ds.Tables[9].Rows[i]["DueDate"].ToString() + "<br />";
                                lblpercentOption.Text += "" + ds.Tables[9].Rows[i]["PercentComm"].ToString() + "%" + "<br />";
                                lblamountOption.Text += "$" + String.Format("{0:n2}", Convert.ToDecimal(ds.Tables[9].Rows[i]["CommAmountDue"].ToString())) + "<br />";


                                totOptComm += Convert.ToDecimal(ds.Tables[9].Rows[i]["CommAmountDue"].ToString());

                                //lblCommissionDate.Text += " " + ds.Tables[2].Rows[i]["DueDate"].ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + ds.Tables[2].Rows[i]["PercentComm"].ToString() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$" + String.Format("{0:n2}", Convert.ToDecimal(ds.Tables[2].Rows[i]["CommAmountDue"].ToString())) + "<br />";
                                //lblCommissionDateOption
                                // lblCommissionDue.Text += " " + ds.Tables[8].Rows[i]["DueDate"].ToString() + "        " + ds.Tables[8].Rows[i]["PercentComm"].ToString() + "     $" + String.Format("{0:n2}", Convert.ToDecimal(ds.Tables[8].Rows[i]["CommAmountDue"].ToString())) + "<br />";

                            }
                            Session[GlobleData.cns_totOptComm] = totOptComm;
                        }
                        else
                        {
                            lblCommissionDateOption.Text = string.Empty;
                            lblpercentOption.Text = string.Empty;
                            lblamountOption.Text = string.Empty;
                        }
                    }
                    else
                    {
                        gvOPTIONCOMMISSION.DataSource = null;
                        gvOPTIONCOMMISSION.DataBind();
                        lblCommissionDateOption.Text = string.Empty;
                        lblpercentOption.Text = string.Empty;
                        lblamountOption.Text = string.Empty;
                    }
                }
                if (Convert.ToInt32(ds.Tables[7].Rows[0]["OptionPayableLeaseTerm"].ToString()) > 0)
                {
                    if (Convert.ToInt32(ds.Tables[7].Rows[0]["OptionLeaseTerm"].ToString()) > 0)
                    {
                        if (ds.Tables[10].Rows.Count > 0)
                        {
                            lblOptionTermPayable.Visible = true;
                            tdOptionTermPayable.Visible = true;
                            gvOptionTermPayable.DataSource = ds.Tables[10];
                            gvOptionTermPayable.DataBind();
                        }
                        else
                        {
                            gvOptionTermPayable.DataSource = null;
                            gvOptionTermPayable.DataBind();
                        }
                    }
                    else
                    {
                        if (ds.Tables[8].Rows.Count > 0)
                        {
                            lblOptionTermPayable.Visible = true;
                            tdOptionTermPayable.Visible = true;
                            gvOptionTermPayable.DataSource = ds.Tables[8];
                            gvOptionTermPayable.DataBind();
                        }
                        else
                        {
                            gvOptionTermPayable.DataSource = null;
                            gvOptionTermPayable.DataBind();
                        }
                    }
                }
                if (Convert.ToInt32(ds.Tables[7].Rows[0]["ContingentLeaseTerm"].ToString()) > 0)
                {
                    if (Convert.ToInt32(ds.Tables[7].Rows[0]["OptionLeaseTerm"].ToString()) > 0 && Convert.ToInt32(ds.Tables[7].Rows[0]["OptionPayableLeaseTerm"].ToString()) > 0)
                    {
                        if (ds.Tables[11].Rows.Count > 0)
                        {
                            lblContingentTerm.Visible = true;
                            tdContingentTerm.Visible = true;
                            gvContingentTerm.DataSource = ds.Tables[11];
                            gvContingentTerm.DataBind();
                        }
                        else
                        {
                            gvContingentTerm.DataSource = null;
                            gvContingentTerm.DataBind();
                        }
                    }
                    else if (Convert.ToInt32(ds.Tables[7].Rows[0]["OptionLeaseTerm"].ToString()) > 0 || Convert.ToInt32(ds.Tables[7].Rows[0]["OptionPayableLeaseTerm"].ToString()) > 0)
                    {
                        if (ds.Tables[10].Rows.Count > 0)
                        {
                            lblContingentTerm.Visible = true;
                            tdContingentTerm.Visible = true;
                            gvContingentTerm.DataSource = ds.Tables[10];
                            gvContingentTerm.DataBind();
                        }
                        else
                        {
                            gvContingentTerm.DataSource = null;
                            gvContingentTerm.DataBind();
                        }
                    }
                    else
                    {
                        if (ds.Tables[8].Rows.Count > 0)
                        {
                            lblContingentTerm.Visible = true;
                            tdContingentTerm.Visible = true;
                            gvContingentTerm.DataSource = ds.Tables[8];
                            gvContingentTerm.DataBind();
                        }
                        else
                        {
                            gvContingentTerm.DataSource = null;
                            gvContingentTerm.DataBind();
                        }
                    }

                }
            }
            //if (ds.Tables[4].Rows.Count > 0)
            //{
            //    gvOPTIONCOMMISSION.DataSource = ds.Tables[4];
            //    gvOPTIONCOMMISSION.DataBind();
            //    lblOPTIONCOMMISSION.Visible = true;
            //    tblOPTIONCOMMISSION.Visible = true;
            //}
            //else
            //{
            //    lblOPTIONCOMMISSION.Visible = false;
            //    tblOPTIONCOMMISSION.Visible = false;
            //}

            //if (ds.Tables[5].Rows.Count > 0)
            //{
            //    gvOptionTermPayable.DataSource = ds.Tables[5];
            //    gvOptionTermPayable.DataBind();
            //    lblOptionTermPayable.Visible = true;
            //    tblOptionTermPayable.Visible = true;
            //}
            //else
            //{
            //    lblOptionTermPayable.Visible = false;
            //    tblOptionTermPayable.Visible = false;
            //}

            //if (ds.Tables[6].Rows.Count > 0)
            //{lblContingentTerm
            //    gvContingentTerm.DataSource = ds.Tables[6];
            //    gvContingentTerm.DataBind();
            //    lblContingentTerm.Visible = true;
            //    tblContingentTerm.Visible = true;
            //}
            //else
            //{
            //    lblContingentTerm.Visible = false;
            //    tblContingentTerm.Visible = false;
            //}
            ///////this.Parent.FindControl("divChargeDetailCard").Visible = true;
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "displaysdsd", "display();", true);
         
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucChargeDetailCard.ascx", "Bindata", ex.Message);
        }
    }
    //protected void imgClose_Click(object sender, ImageClickEventArgs e)
    //{
    //    //this.Parent.FindControl("divChargeDetailCard").Visible = false;
    //    ScriptManager.RegisterStartupScript(this, Page.GetType(), "Hide", "Hide();", true);
    //}
    decimal totalPaymentAmount = 0;
    protected void gvCharge_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.Eval(e.Row.DataItem, "AmountPaid") != DBNull.Value)
                    totalPaymentAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmountPaid"));
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbltotalAMOUNTPaid = (Label)e.Row.FindControl("lbltotalAMOUNTPaid");
                lbltotalAMOUNTPaid.Text = "$" + String.Format("{0:n2}", totalPaymentAmount);
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucChargeDetailCard.ascx", "gvCharge_RowDataBound", ex.Message);
        }
    }
    protected void FormView1_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "FileUpload")
            {
                string fileToSelect = "../Uploads1/" + e.CommandArgument.ToString();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "display", "javascript:return OpenWindow('" + fileToSelect + "')", true);
                // lnk.Attributes.Add("onclick", "javascript:return OpenWindow('" + fileToSelect + "')");
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucChargeDetailCard.ascx", "FormView1_ItemCommand", ex.Message);
        }
    }


    protected void btn_Command(object sender, CommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "FileUpload")
            {
                string fileToSelect = "../Uploads1/" + e.CommandArgument.ToString();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "display", "javascript:return OpenWindow('" + fileToSelect + "')", true);
                // lnk.Attributes.Add("onclick", "javascript:return OpenWindow('" + fileToSelect + "')");
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucChargeDetailCard.ascx", "btn_Command", ex.Message);
        }
    }

    decimal totalTransGross = 0, totalTransUnapplied = 0, commissionpaid = 0,TotalOverPaymentAmountBase=0;
    protected void gvBaseCommision_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.Eval(e.Row.DataItem, "AmountDue") != DBNull.Value)
                    totalTransGross += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmountDue"));
                if (DataBinder.Eval(e.Row.DataItem, "PaymentReceived") != DBNull.Value)
                    commissionpaid += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PaymentReceived"));
                if (DataBinder.Eval(e.Row.DataItem, "AmountUpaid") != DBNull.Value)
                    totalTransUnapplied += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmountUpaid"));
                if (DataBinder.Eval(e.Row.DataItem, "OverPaymentAmount") != DBNull.Value)
                    TotalOverPaymentAmountBase += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OverPaymentAmount"));
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbltotalCOMMISSIONDUE = (Label)e.Row.FindControl("lbltotalCOMMISSIONDUE");
                lbltotalCOMMISSIONDUE.Text = "$" + String.Format("{0:n2}", totalTransGross);
                Label lbltotalAMOUNTUNPAID = (Label)e.Row.FindControl("lbltotalAMOUNTUNPAID");
                lbltotalAMOUNTUNPAID.Text = "$" + String.Format("{0:n2}", totalTransUnapplied);
                Label lbltotalCOMMISSIONPAID = (Label)e.Row.FindControl("lbltotalCOMMISSIONPAID");
                lbltotalCOMMISSIONPAID.Text = "$" + String.Format("{0:n2}", commissionpaid);
                Label lbltotalOverPayAmount = (Label)e.Row.FindControl("lbltotalOverpayAmount");
                lbltotalOverPayAmount.Text = "$" + String.Format("{0:n2}", TotalOverPaymentAmountBase);
                //diptee
                Session[GlobleData.cns_totBaseLeaseComm] = totalTransGross;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucChargeDetailCard.ascx", "gvBaseCommision_RowDataBound", ex.Message);
        }
    }
    decimal optcomtotalTransGross = 0, optcomtotalTransUnapplied = 0, optcomcommissionpaid = 0, TotalOverPaymentAmountOptCom = 0;
    protected void gvOPTIONCOMMISSION_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.Eval(e.Row.DataItem, "AmountDue") != DBNull.Value)
                    optcomtotalTransGross += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmountDue"));
                if (DataBinder.Eval(e.Row.DataItem, "PaymentReceived") != DBNull.Value)
                    optcomcommissionpaid += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PaymentReceived"));
                if (DataBinder.Eval(e.Row.DataItem, "AmountUpaid") != DBNull.Value)
                    optcomtotalTransUnapplied += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmountUpaid"));
                if (DataBinder.Eval(e.Row.DataItem, "OverPaymentAmount") != DBNull.Value)
                    TotalOverPaymentAmountOptCom += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OverPaymentAmount"));
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbltotalCOMMISSIONDUE = (Label)e.Row.FindControl("lbltotalCOMMISSIONDUE");
                lbltotalCOMMISSIONDUE.Text = "$" + String.Format("{0:n2}", optcomtotalTransGross);
                Label lbltotalAMOUNTUNPAID = (Label)e.Row.FindControl("lbltotalAMOUNTUNPAID");
                lbltotalAMOUNTUNPAID.Text = "$" + String.Format("{0:n2}", optcomtotalTransUnapplied);
                Label lbltotalCOMMISSIONPAID = (Label)e.Row.FindControl("lbltotalCOMMISSIONPAID");
                lbltotalCOMMISSIONPAID.Text = "$" + String.Format("{0:n2}", optcomcommissionpaid);
                Label lbltotalOverPayAmount = (Label)e.Row.FindControl("lbltotalOverpayAmount");
                lbltotalOverPayAmount.Text = "$" + String.Format("{0:n2}", TotalOverPaymentAmountOptCom);
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucChargeDetailCard.ascx", "gvOPTIONCOMMISSION_RowDataBound", ex.Message);
        }
    }
    decimal optpaytotalTransGross = 0, optpaytotalTransUnapplied = 0, optpaycommissionpaid = 0, TotalOverPayAmountOptPay = 0;
    protected void gvOptionTermPayable_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.Eval(e.Row.DataItem, "AmountDue") != DBNull.Value)
                    optpaytotalTransGross += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmountDue"));
                if (DataBinder.Eval(e.Row.DataItem, "PaymentReceived") != DBNull.Value)
                    optpaycommissionpaid += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PaymentReceived"));
                if (DataBinder.Eval(e.Row.DataItem, "AmountUpaid") != DBNull.Value)
                    optpaytotalTransUnapplied += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmountUpaid"));
                if (DataBinder.Eval(e.Row.DataItem, "OverPaymentAmount") != DBNull.Value)
                    TotalOverPayAmountOptPay += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OverPaymentAmount"));
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbltotalCOMMISSIONDUE = (Label)e.Row.FindControl("lbltotalCOMMISSIONDUE");
                lbltotalCOMMISSIONDUE.Text = "$" + String.Format("{0:n2}", optpaytotalTransGross);
                Label lbltotalAMOUNTUNPAID = (Label)e.Row.FindControl("lbltotalAMOUNTUNPAID");
                lbltotalAMOUNTUNPAID.Text = "$" + String.Format("{0:n2}", optpaytotalTransUnapplied);
                Label lbltotalCOMMISSIONPAID = (Label)e.Row.FindControl("lbltotalCOMMISSIONPAID");
                lbltotalCOMMISSIONPAID.Text = "$" + String.Format("{0:n2}", optpaycommissionpaid);
                Label lbltotalOverPayAmount = (Label)e.Row.FindControl("lbltotalOverpayAmount");
                lbltotalOverPayAmount.Text = "$" + String.Format("{0:n2}", TotalOverPayAmountOptPay);
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucChargeDetailCard.ascx", "gvOptionTermPayable_RowDataBound", ex.Message);
        }
    }
    decimal contotalTransGross = 0, contotalTransUnapplied = 0, concommissionpaid = 0, TotalOverPayAmountContin = 0;
    protected void gvContingentTerm_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (DataBinder.Eval(e.Row.DataItem, "AmountDue") != DBNull.Value)
                    contotalTransGross += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmountDue"));
                if (DataBinder.Eval(e.Row.DataItem, "PaymentReceived") != DBNull.Value)
                    concommissionpaid += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PaymentReceived"));
                if (DataBinder.Eval(e.Row.DataItem, "AmountUpaid") != DBNull.Value)
                    contotalTransUnapplied += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmountUpaid"));
                if (DataBinder.Eval(e.Row.DataItem, "OverPaymentAmount") != DBNull.Value)
                    TotalOverPayAmountContin += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "OverPaymentAmount"));
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lbltotalCOMMISSIONDUE = (Label)e.Row.FindControl("lbltotalCOMMISSIONDUE");
                lbltotalCOMMISSIONDUE.Text = "$" + String.Format("{0:n2}", contotalTransGross);
                Label lbltotalAMOUNTUNPAID = (Label)e.Row.FindControl("lbltotalAMOUNTUNPAID");
                lbltotalAMOUNTUNPAID.Text = "$" + String.Format("{0:n2}", contotalTransUnapplied);
                Label lbltotalCOMMISSIONPAID = (Label)e.Row.FindControl("lbltotalCOMMISSIONPAID");
                lbltotalCOMMISSIONPAID.Text = "$" + String.Format("{0:n2}", concommissionpaid);
                Label lbltotalOverPayAmount = (Label)e.Row.FindControl("lbltotalOverpayAmount");
                lbltotalOverPayAmount.Text = "$" + String.Format("{0:n2}", TotalOverPayAmountContin);
                //diptee
                Session[GlobleData.cns_totContingentComm] = contotalTransGross;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucChargeDetailCard.ascx", "gvContingentTerm_RowDataBound", ex.Message);
        }
    }
    protected void gvBaseCommision_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "PayHistory")
            {
                if (this.Parent.FindControl("ucPaymentHistoryDetail1") != null)
                {
                    ucPaymentHistoryDetail ucb = (ucPaymentHistoryDetail)this.Parent.FindControl("ucPaymentHistoryDetail1");
                    ucb.ChargeSlipId = Convert.ToInt32(e.CommandArgument);
                    ucb.Bindata();
                    if (this.Parent.FindControl("hdnPopup") != null)
                    {
                        HiddenField hdn = (HiddenField)this.Parent.FindControl("hdnPopup");
                        hdn.Value = "1";
                    }
                }

            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucChargeDetailCard.ascx", "gvBaseCommision_RowCommand", ex.Message);
        }
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (this.Parent.FindControl("ucPaymentHistoryDetail1") != null)
            {
                ucPaymentHistoryDetail ucb = (ucPaymentHistoryDetail)this.Parent.FindControl("ucPaymentHistoryDetail1");
                ucb.ChargeSlipId = Convert.ToInt32(Session["ChargeSlipId"].ToString());
                ucb.Bindata();
                if (this.Parent.FindControl("hdnPopup") != null)
                {
                    HiddenField hdn = (HiddenField)this.Parent.FindControl("hdnPopup");
                    hdn.Value = "1";
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucChargeDetailCard.ascx", "ImageButton1_Click", ex.Message);
        }
    }
}