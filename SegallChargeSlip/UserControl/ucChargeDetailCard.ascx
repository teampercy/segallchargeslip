﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucChargeDetailCard.ascx.cs"
    Inherits="UserControl_ucChargeDetailCard" %>
<%@ Register Src="~/UserControl/ucPaymentHistoryDetail.ascx" TagName="ucPaymentHistoryDetail" TagPrefix="uc1" %>

<script type="text/javascript">
    function OpenWindow(Path) {
        var width = 650;
        var height = 450;
        var left = (screen.width - width) / 2;
        var top = (screen.height - height) / 2;
        var params = 'width=' + width + ', height=' + height;
        params += ', top=' + top + ', left=' + left;
        params += ', directories=no';
        params += ', location=no';
        params += ', menubar=no';
        params += ', resizable=no';
        params += ', scrollbars=no';
        params += ', status=no';
        params += ', toolbar=no';
        window.open(Path, 'mywindow', params);

        return false;
    }

    function SaveAlert(Result) {
        if (Result == "True") {
            alert("New Charege Slip Saved Successfully");
            window.location.href = "AddOrEditNewChargeSlip.aspx";
            return true;
        }
        else {
            alert("Error Occured While Save");
            window.location.href = "AddOrEditNewChargeSlip.aspx";
            return true;
        }
    }

</script>
<style type="text/css">
    .bordergrid {
        border: 1px solid black;
    }

    .black_overlay {
        position: fixed;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: black;
        z-index: 100;
        opacity: 0.4;
        filter: alpha(opacity=40);
    }

    .white_content {
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        position: fixed;
        width: 650px;
        height: 550px;
        border: solid 2px black;
        background-color: white;
        z-index: 1002;
        /*overflow: auto;*/
        text-align: left;
    }
</style>
<style type="text/css">
    .FontWeight {
        font-weight: bold;
    }
</style>
<script type="text/javascript">
    function ClearTable() {
        //alert('gii');
        debugger;
        $("#ContentMain_ucBilledTab1_ucChargeDetailCard1_tblBaseCommision").empty();
    }
</script>
<div id="fade" class="black_overlay" style="display: block; height: 100%">
</div>

<div id="signin" class="white_content" style="display: block; width: 750px; height: 700px;" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr id="divheader" style="background-color: gray; height: 35px;">
            <td style="border-bottom: solid 2px black;">&nbsp;&nbsp;<asp:Label ID="lblCHARGEDETAIL" Style="vertical-align: bottom; font-size: medium; font-weight: bold; color: Black;"
                runat="server" Text="CHARGE DETAIL"></asp:Label>
            </td>
            <td width="30px" style="text-align: center; border-bottom: solid 2px black;">
                <asp:ImageButton ID="imgClose" src="../Images/close.png" runat="server"
                    OnClientClick=" return Hide(); " /><%--OnClick="imgClose_Click"--%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table cellpadding="0" cellspacing="0" width="100%" style="padding: 10px;">
                    <tr>
                        <td style="padding-top: 5px;" colspan="2">
                            <asp:Label ID="lblDealName" Style="vertical-align: bottom; font-size: large; font-weight: bold; color: Orange;"
                                runat="server" Text=""></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblDealDate" Style="vertical-align: bottom; font-size: small; color: black;"
                                runat="server" Text=""></asp:Label>
                            <div style="width: 40px; float: right;">
                                <asp:ImageButton ID="imgEditForm2" ToolTip="Edit" Style="text-align: right;" Width="15px" Height="15px" runat="server" ImageUrl="~/Images/Edit-Icon.gif" OnClientClick="javascript:return Navigate();" />
                                <asp:ImageButton ID="imgReprint" ToolTip="Reprint" Style="text-align: right;" Width="15px" Height="15px" runat="server" ImageUrl="~/Images/Reprint.png" OnClientClick="javascript:return Reprint();" />
                            </div>
                        </td>
                    </tr>
                    <tr height="4px">
                        <td colspan="2" height="4px">
                            <hr style="width: 100%; padding: 0px 0px 0px 0px;" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:FormView ID="FormView4" Width="100%" runat="server" Visible="false">
                                <HeaderTemplate>
                                    <table cellspacing="2" cellpadding="2" style="width: 100%; font-weight: normal; font-size: 12px; font-family: Verdana;">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="vertical-align: top; width: 130px;">
                                            <asp:Label ID="lblhdrDelReason" runat="server"  Text="Reason of Deletion:"></asp:Label>
                                        </td>
                                        <td>
                                            <div style="overflow-y: auto; height: 44px; width: 100%; min-width: 100%; border: solid 1px black; word-wrap: normal; vertical-align: top;" class="LabelStyle">
                                                <asp:Label ID="lbldelreasontxt" runat="server" Text='<%#Eval("DeleteReason")%>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:FormView>
                        </td>
                    </tr>
                    <tr style="height: 100px;">
                        <td width="50%">
                            <asp:FormView ID="FormView2" Height="100px" Width="100%" runat="server">
                                <HeaderTemplate>
                                    <table cellspacing="2" cellpadding="2" style="width: 100%; font-weight: normal; font-size: 12px; font-family: Verdana;">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%-- <tr><td style="vertical-align: top; width: 130px;"><asp:Label ID="lblhdrDelReason" runat="server" Visible="false" Text="Reason of Deletion:"></asp:Label></td>
                                        <td><asp:Label ID="lbldelreasontxt" Visible="false" runat="server" Text='<%#Eval("DeleteReason")%>' ></asp:Label></td>
                                    </tr>--%>
                                    <tr>
                                        <td style="vertical-align: top; width: 130px;">Address:
                                        </td>
                                        <td colspan="2">
                                            <%# !String.IsNullOrEmpty(Eval("Company").ToString()) ? Eval("Company")+"<br />":""%>

                                            <%-- <%if(!String.IsNullOrEmpty(Eval("Address1").ToString()) && !String.IsNullOrEmpty(Eval("Address2").ToString())) 
                                            {
    
                                            }%>--%>
                                            <%--  <%if(!string.IsNullOrEmpty((Eval("Company") as string)))
                                            { %>
                                                   <asp:Label ID="lblShoppingCenter" runat="server" Text='<%#Eval("Company")%>'></asp:Label><br />
                                            <%} %>--%>
                                            <%--if((<%#Eval("Company")%>)!=null)
                                            {
                                           
                                            }--%>
                                            <%--    <asp:Label ID="lblBillingAddress1" runat="server" Text='<%#Eval("Address1")%>'></asp:Label>
                                            <asp:Label ID="lblBillingAddress2" runat="server" Text='<%#Eval("Address2")%>'></asp:Label>--%>
                                            <%# (!String.IsNullOrEmpty(Eval("Address1").ToString()) || !String.IsNullOrEmpty(Eval("Address2").ToString())) ? Eval("Address1")+" "+Eval("Address2")+"<br />":""%>
                                            <asp:Label ID="lblBillingCity" runat="server" Text='<%#Eval("City")%>'></asp:Label>
                                            <asp:Label ID="lblBillingState" runat="server" Text='<%#Eval("State")%>'></asp:Label>
                                            <asp:Label ID="lblBillingZip" runat="server" Text='<%#Eval("ZipCode")%>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; width: 120px;">Charge To:
                                        </td>
                                        <td colspan="2">
                                            <%# !String.IsNullOrEmpty(Eval("ChargeToName").ToString()) ? Eval("ChargeToName")+"<br />":""%>
                                            <%# !String.IsNullOrEmpty(Eval("OwnershipEntity").ToString()) ? Eval("OwnershipEntity")+"<br />":""%>
                                            <%# (!String.IsNullOrEmpty(Eval("ToAddress1").ToString()) || !String.IsNullOrEmpty(Eval("ToAddress2").ToString())) ? Eval("ToAddress1")+" "+Eval("ToAddress2")+"<br />":""%>
                                            <%-- <asp:Label ID="lblChargeToCompany" runat="server" Text='<%#Eval("ChargeToName")%>'></asp:Label><br />--%>
                                            <%--                                    <asp:Label ID="lblOwnershipEntity" runat="server" Text='<%#Eval("OwnershipEntity")%>'></asp:Label><br />--%>

                                            <%--<asp:Label ID="lblChargeToAddress1" runat="server" Text='<%#Eval("ToAddress1")%>'></asp:Label>
                                            <asp:Label ID="lblChargeToAddress2" runat="server" Text='<%#Eval("ToAddress2")%>'></asp:Label><br />--%>
                                            <asp:Label ID="lblChargeToCity" runat="server" Text='<%#Eval("ToCity")%>'></asp:Label>
                                            <asp:Label ID="lblChargeToState" runat="server" Text='<%#Eval("ToState")%>'></asp:Label>
                                            <asp:Label ID="lblChargeToZip" runat="server" Text='<%#Eval("ToZipCode")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:FormView>
                        </td>
                        <td width="50%" style="vertical-align: top;">
                            <asp:FormView ID="FormView3" Height="100px" Width="100%" runat="server">
                                <HeaderTemplate>
                                    <table cellspacing="2" cellpadding="2" style="width: 100%; height: 100%; font-weight: normal; vertical-align: top; font-size: 12px; font-family: Verdana;">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td width="100px;">Contact Name:
                                        </td>
                                        <td width="60%">
                                            <asp:Label ID="lblContact" runat="server" Text='<%#Eval("Name")%>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="trEmails" runat="server">
                                        <td>Email:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email")%>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Deal Type:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDealType" runat="server" Text='<%#Eval("DealType")%>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Deal Subtype:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDealSubtype" runat="server" Text='<%#Eval("DealSubtype")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:FormView>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <hr style="width: 100%;" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="details" style="width: 98%; overflow-y: auto; height: 420px;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvCharge" ShowFooter="True" Font-Size="12px" font-family="Verdana" Width="100%"
                                                runat="server" AutoGenerateColumns="False" OnRowDataBound="gvCharge_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField DataField="CHARGETO" HeaderText="CHARGE TO" ItemStyle-HorizontalAlign="left"
                                                        FooterText="Total" />
                                                    <%-- <asp:BoundField DataField="PaymentDate" HeaderText="DUE DATE" />
                                                    <asp:TemplateField HeaderText="AMOUNT" ItemStyle-HorizontalAlign="right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblAMOUNTDue" runat="server" Text='<%# String.Format("{0:f2}",Eval("AMOUNTDue"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalAMOUNTDue" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>--%>
                                                    <asp:BoundField DataField="PaymentDate" HeaderText="PAID DATE" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderText="AMOUNT" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblAMOUNTPaid" runat="server" Text='<%# String.Format("{0:n2}",Eval("AmountPaid"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalAMOUNTPaid" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Method" HeaderText="METHOD" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:BoundField DataField="CheckNo" HeaderText="CHECK NO" ItemStyle-HorizontalAlign="Center" />
                                                   
                                                    <%-- <asp:TemplateField HeaderText="UNAPPLIED" ItemStyle-HorizontalAlign="right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblUnapplied" runat="server" Text='<%# String.Format("{0:f2}",Eval("Unapplied"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalUnapplied" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>--%>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                                <HeaderStyle CssClass="HeaderGridView2" />
                                                <RowStyle CssClass="RowGridView2" />
                                                <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                                <FooterStyle CssClass="FooterGridView2" />
                                                <%--<AlternatingRowStyle Height="10px" BackColor="#F5F6CE" />
                                                <HeaderStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />
                                                <RowStyle BackColor="#ffffff" Height="10px" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#F7BE81" />
                                                <FooterStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />--%>
                                            </asp:GridView>
                                        </td>
                                        <td width="20px" style="vertical-align: top;">
                                            <asp:ImageButton ID="ImageButton1" Width="15px" Height="15px" runat="server" ImageUrl="~/Images/Payment-Icon.gif" OnClick="ImageButton1_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:FormView ID="FormView1" Width="100%" runat="server" OnItemCommand="FormView1_ItemCommand">
                                                <HeaderTemplate>
                                                    <table cellspacing="2" cellpadding="2" style="width: 100%; font-weight: normal; font-size: 12px; font-family: Verdana;">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td width="100px">Charge Date:
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblDate" runat="server" Text='<%#Eval("CreatedOn")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trSize" runat="server" visible="false">
                                                        <td>
                                                            <asp:Label ID="lblParcelSize" runat="server" Text="Size:"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblSize" runat="server" Text='<%# String.Format("{0:n2}",Eval("Size"))%>'></asp:Label>
                                                            <asp:Label ID="lblSizeSign" runat="server" Text='<%#Eval("UnitTypeDesc")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trBuildingSize" runat="server" visible="false">
                                                        <td>Bldg Size:</td>
                                                        <td>
                                                            <asp:Label ID="lblBuildingSize" runat="server" Text='<%#Eval("BldgSize")%>'></asp:Label>
                                                            <asp:Label ID="lblUnit" runat="server" Text="Sq ft"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trTenantBuyer" runat="server" visible="false">
                                                        <td>
                                                            <asp:Label ID="lblTenantLabel" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblTenantText" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%-- <tr id="trBuyer" runat ="server" >
                                                        <td>
                                                           <asp:Label ID="lblBuyer1" runat ="server" ></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblBuyer" runat="server" Text='<%#Eval("PartyName")%>'></asp:Label>
                                                        </td>
                                                    </tr>--%>
                                                    <tr id="trUse" runat="server" visible="false">
                                                        <td>Use:
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblUse" runat="server" Text='<%#Eval("DealUse")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trSalePrice" runat="server" visible="false">
                                                        <td>Sale Price:
                                                        </td>
                                                        <td colspan="2">$
                                                            <asp:Label ID="lblSalePrice" runat="server" Text='<%# String.Format("{0:n2}",Eval("SalePrice"))%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trNets" runat="server" visible="false">
                                                        <td>Nets:
                                                        </td>
                                                        <td colspan="2">$<asp:Label ID="lblNets" runat="server" Text='<%#String.Format("{0:n2}",Eval("Nets")) %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trTI" runat="server" visible="false">
                                                        <td>TI:
                                                        </td>
                                                        <td colspan="2">$<asp:Label ID="lblTI" runat="server" Text='<%#String.Format("{0:n2}",Eval("TI")) %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trFreeRent" runat="server" visible="false">
                                                        <td>Free Rent:
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblFreeRent" runat="server" Text='<%#Eval("FreeRent")%>'></asp:Label>
                                                            <asp:Label ID="lblFreeRentUnit" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trRentPSF" runat="server" visible="false">
                                                        <td>Rent PSF:
                                                        </td>
                                                        <td colspan="2">$<asp:Label ID="Label3" runat="server" Text='<%#String.Format("{0:n2}",Eval("NNN")) %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trAggregateRent" runat="server" visible="false">
                                                        <td>Aggregate Rent:
                                                        </td>
                                                        <td colspan="2">$<asp:Label ID="Label4" runat="server" Text='<%#String.Format("{0:n2}",Eval("AggregateRent")) %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trCommissionable" runat="server" visible="false">
                                                        <td>Commissionable:
                                                        </td>
                                                        <td colspan="2">$<asp:Label ID="Label5" runat="server" Text='<%#String.Format("{0:n2}",Eval("CommissionablePeriodRent")) %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trCommissionper" runat="server" visible="false">
                                                        <td>Commission %:
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="Label6" runat="server" Text='<%#Eval("CommissionPer")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total Due:
                                                        </td>
                                                        <td colspan="2">$
                                                            <asp:Label ID="lblTotalDue" runat="server" Text='<%#String.Format("{0:n2}",Eval("TotalDue")) %>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trTerm" runat="server" visible="false">
                                                        <td style="vertical-align: top;">Term:
                                                        </td>
                                                        <td colspan="2">
                                                            <table style="font-size: 12px; font-family: Verdana; font-weight: normal;">
                                                                <tr>
                                                                    <td>Base:&nbsp;&nbsp;<asp:Label ID="lblTermBase" runat="server" Text='<%#Eval("BaseLeaseTerm")%>'></asp:Label>&nbsp;Months<br />
                                                                        Options:&nbsp;&nbsp;<asp:Label ID="lblTermOptions" runat="server" Text='<%#Eval("OptionLeaseTerm")%>'></asp:Label>&nbsp;Months
                                                                    </td>
                                                                    <td style="width: 120px;"></td>
                                                                    <td width="50%">Commissionable?&nbsp;<asp:Label ID="Label1" runat="server" Text='<%#Eval("BaseCommissionableTerm")%>'></asp:Label><br />
                                                                        Commissionable?&nbsp;<asp:Label ID="Label2" runat="server" Text='<%#Eval("OptionCommissionableTerm")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>


                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: top; width: 19%">Commission Due:
                                                        </td>
                                                        <td style="width: 70%;" colspan="2">
                                                            <table style="font-size: 12px; font-family: Verdana; font-weight: normal;">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblCommissionDate" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                    <td width="30px"></td>
                                                                    <td align="right">
                                                                        <asp:Label ID="lblpercent" runat="server"></asp:Label></td>
                                                                    <td width="30px"></td>
                                                                    <td align="right">
                                                                        <asp:Label ID="lblamount" runat="server"></asp:Label></td>
                                                                </tr>
                                                            </table>
                                                            <%--<asp:Label ID="lblCommissionDate" runat="server" Text=""></asp:Label>--%>
                                                            <%-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <asp:Label ID="lblCommissionPercent" runat="server" Text='<%#Eval("CommissionPercent")%>'></asp:Label><br />
                                                            <asp:Label ID="lblCommissionDate2" runat="server" Text='<%#Eval("CommissionDate")%>'></asp:Label>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <asp:Label ID="lblCommissionPercent2" runat="server" Text='<%#Eval("CommissionPercent")%>'></asp:Label>--%>
                                                        </td>
                                                        <td></td>
                                                        <%-- <td>$<asp:Label ID="lblCommissionDue" runat="server" Text='<%#Eval("CommissionDue")%>'></asp:Label><br />
                                                            $<asp:Label ID="lblCommissionDue2" runat="server" Text='<%#Eval("CommissionDue")%>'></asp:Label>
                                                        </td>--%>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: top;">
                                                            <asp:Label ID="lblCoBroker" runat="server" Text="Co-brokers:"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblCobrokers" runat="server" Text=""></asp:Label>
                                                            <%--'<%#Eval("Cobrokers")%>'--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: top;">Notes:
                                                        </td>
                                                        <td colspan="2" style="width: 100%;">
                                                            <div style="overflow-y: auto; height: 44px; width: 100%; min-width: 100%; border: solid 1px black; word-wrap: normal; vertical-align: top;" class="LabelStyle">


                                                                <asp:Label ID="lblNotes" runat="server" Text='<%#Eval("Notes")%>'></asp:Label>
                                                            </div>



                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblAttachments" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <%--  <asp:Label ID="lblAttachments" runat="server" Text=""></asp:Label>--%>
                                                            <asp:PlaceHolder ID="phFileAttach" runat="server" />
                                                            <%--  <asp:Button ID="btnAttachments" BackColor="Gray" Font-Bold="true"
                                                                Style="height: 10px; font-size: 4px;" runat="server" Text="Open" />--%>
                                                  
                                                        </td>


                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:FormView>
                                        </td>
                                    </tr>

                                    <%--  
                                        <tr>
                                        <td colspan="2" id="LineCharge" runat="server">
                                            <hr style="width: 100%;" />
                                        </td>
                                    </tr>--%>

                                    <tr>
                                        <td colspan="2">
                                            <hr style="width: 100%;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: lightgray;">
                                            <asp:Label BackColor="#D8D8D8" Width="99.7%" Font-Bold="True" ForeColor="Black"
                                                ID="lblBaseCommision" Style="height: 15px; vertical-align: top; font-size: 10px; border: solid 1px black; border-bottom: none; text-align: center;" runat="server" Text="BASE COMMISSION TERMS"></asp:Label>
                                            <asp:GridView ID="gvBaseCommision" ShowFooter="True" Font-Size="12px" font-family="Verdana" CssClass="OuterBorder"
                                                Width="100%" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvBaseCommision_RowDataBound" OnRowCommand="gvBaseCommision_RowCommand">
                                                <Columns>
                                                    <asp:BoundField DataField="BrokerName" HeaderText="VENDOR" ItemStyle-HorizontalAlign="left"
                                                        FooterText="Total" FooterStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="BrokerRole" HeaderText="ROLE" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderText="% DUE" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPercent" runat="server" Text='<%# String.Format("{0:n2}",Eval("BrokerPercent"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--  <asp:BoundField DataField="BrokerPercent" HeaderText="% DUE" />--%>
                                                    <asp:TemplateField HeaderText="COMMISSION DUE" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblCOMMISSIONDUE" runat="server" Text='<%# String.Format("{0:n2}",Eval("AmountDue"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalCOMMISSIONDUE" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="COMMISSION PAID" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblCOMMISSIONPAID" runat="server" Text='<%# String.Format("{0:n2}",Eval("PaymentReceived"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalCOMMISSIONPAID" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ControlStyle-Width="10px">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgPayHistory" CommandName="PayHistory" CommandArgument='<%#Eval("ChargeSlipID") %>' Height="10px" Width="10px" ImageUrl="~/Images/Payment-Icon.gif" runat="server" OnClick="ImageButton1_Click" />
                                                        </ItemTemplate>
                                                        <ControlStyle Width="10px" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="AMOUNT UNPAID" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblAMOUNTUNPAID" runat="server" Text='<%# String.Format("{0:n2}",Eval("AmountUpaid"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalAMOUNTUNPAID" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="OverPayment Amount" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblOverPayAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("OverPaymentAmount"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalOverpayAmount" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                                <HeaderStyle CssClass="HeaderGridView2" />
                                                <RowStyle CssClass="RowGridView2" />
                                                <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                                <FooterStyle CssClass="FooterGridView2" />
                                                <%--                                                <AlternatingRowStyle Height="10px" BackColor="#F5F6CE" />
                                                <HeaderStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />
                                                <RowStyle BackColor="#ffffff" Height="10px" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#F7BE81" />
                                                <FooterStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />--%>
                                            </asp:GridView>
                                        </td>
                                        <td width="20px" style="vertical-align: top;">
                                            <asp:ImageButton ID="imgBaseCommision" Width="15px" Height="15px" runat="server" ImageUrl="~/Images/Edit-Icon.gif" OnClientClick="javascript:return NavigateSplitCalculator();" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <table id="tblBaseCommision" runat="server" width="100%" style="font-weight: normal; font-size: 12px; font-family: Verdana;">
                                                <%-- <tr><td></td></tr>--%>
                                                <%-- <tr><asp:Label ID="lblVendorName" runat ="server" Text=""></asp:Label></tr>
                                                 <tr>
                                                    <td>Company Split:<asp:Label ID="lblCompanySplit" runat="server" Text='<%#Eval("CompanySplit")%>'></asp:Label>
                                                    </td>
                                                    <td>User Name:<asp:Label ID="lblUserName" runat="server" Text='<%#Eval("UserName")%>'></asp:Label>
                                                    </td>
                                                    <td>Segall Group:<asp:Label ID="lblSegallGroup" runat="server" Text='<%#Eval("SegallGroup")%>'></asp:Label>
                                                    </td>
                                                </tr>--%>
                                            </table>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td runat="server" id="LineBaseCommision" colspan="2">
                                            <hr style="width: 100%;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: lightgray;" id="tdOPTIONCOMMISSION" runat="server">
                                            <asp:Label BackColor="#D8D8D8" Width="99.7%" Font-Bold="True" ForeColor="Black"
                                                ID="lblOPTIONCOMMISSION" Style="height: 15px; vertical-align: top; font-size: 12px; border: solid 1px black; border-bottom: none; text-align: center;" runat="server" Text="OPTION COMMISSION TERMS"></asp:Label>
                                            <asp:GridView ID="gvOPTIONCOMMISSION" ShowFooter="True" Font-Size="12px" font-family="Verdana"
                                                Width="100%" runat="server" CssClass="OuterBorder" AutoGenerateColumns="False" OnRowDataBound="gvOPTIONCOMMISSION_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField DataField="BrokerName" HeaderText="VENDOR" ItemStyle-HorizontalAlign="left"
                                                        FooterText="Total" FooterStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="BrokerRole" HeaderText="ROLE" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderText="% DUE" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPercent" runat="server" Text='<%# String.Format("{0:n2}",Eval("BrokerPercent"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:BoundField DataField="BrokerPercent" HeaderText="% DUE" />--%>
                                                    <asp:TemplateField HeaderText="COMMISSION DUE" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblCOMMISSIONDUE" runat="server" Text='<%# String.Format("{0:n2}",Eval("AmountDue"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalCOMMISSIONDUE" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="COMMISSION PAID" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblCOMMISSIONPAID" runat="server" Text='<%# String.Format("{0:n2}",Eval("PaymentReceived"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalCOMMISSIONPAID" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="AMOUNT UNPAID" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblAMOUNTUNPAID" runat="server" Text='<%# String.Format("{0:n2}",Eval("AmountUpaid"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalAMOUNTUNPAID" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="OverPayment Amount" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblOverPayAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("OverPaymentAmount"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalOverpayAmount" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                                <HeaderStyle CssClass="HeaderGridView2" />
                                                <RowStyle CssClass="RowGridView2" />
                                                <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                                <FooterStyle CssClass="FooterGridView2" />
                                            </asp:GridView>
                                        </td>
                                        <td width="20px" style="vertical-align: top;">&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table id="tblOPTIONCOMMISSION" runat="server" width="100%" style="font-weight: normal; font-size: 12px;">
                                                <tr>
                                                    <td width="22%">Commission Due:
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblCommissionDateOption" runat="server" Text=""></asp:Label>
                                                                </td>
                                                                <td width="30px"></td>
                                                                <td>
                                                                    <asp:Label ID="lblpercentOption" runat="server"></asp:Label></td>
                                                                <td width="30px"></td>
                                                                <td>
                                                                    <asp:Label ID="lblamountOption" runat="server"></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                        <%-- <asp:Label ID="lblCommissionDue" runat="server" Text=""></asp:Label>--%>
                                                        <%-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:Label ID="lblCommissionPercent" runat="server" Text=""></asp:Label><br />
                                                        <asp:Label ID="lblCommissionDue2" runat="server" Text=""></asp:Label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:Label ID="lblCommissionPercent2" runat="server" Text=""></asp:Label>--%>
                                                    </td>
                                                    <%-- <td width="50%">
                                                        <asp:Label ID="lblCommission" runat="server" Text=""></asp:Label><br />
                                                        <asp:Label ID="lblCommission2" runat="server" Text=""></asp:Label>
                                                    </td>--%>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: lightgray;" id="tdOptionTermPayable" runat="server">
                                            <asp:Label BackColor="#D8D8D8" Width="99.7%" Font-Bold="True" ForeColor="Black"
                                                ID="lblOptionTermPayable" Style="height: 15px; vertical-align: top; font-size: 12px; border: solid 1px black; border-bottom: none; text-align: center;" runat="server" Text="OPTION TERM PAYABLE"></asp:Label>
                                            <asp:GridView ID="gvOptionTermPayable" ShowFooter="True" Font-Size="12px" font-family="Verdana"
                                                Width="100%" runat="server" CssClass="OuterBorder" AutoGenerateColumns="False" OnRowDataBound="gvOptionTermPayable_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField DataField="BrokerName" HeaderText="VENDOR" ItemStyle-HorizontalAlign="left"
                                                        FooterText="Total" FooterStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="BrokerRole" HeaderText="ROLE" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderText="% DUE" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPercent" runat="server" Text='<%# String.Format("{0:n2}",Eval("BrokerPercent"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--  <asp:BoundField DataField="BrokerPercent" HeaderText="% DUE" />--%>
                                                    <asp:TemplateField HeaderText="COMMISSION DUE" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblCOMMISSIONDUE" runat="server" Text='<%# String.Format("{0:n2}",Eval("AmountDue"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalCOMMISSIONDUE" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="COMMISSION PAID" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblCOMMISSIONPAID" runat="server" Text='<%# String.Format("{0:n2}",Eval("PaymentReceived"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalCOMMISSIONPAID" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="AMOUNT UNPAID" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblAMOUNTUNPAID" runat="server" Text='<%# String.Format("{0:n2}",Eval("AmountUpaid"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalAMOUNTUNPAID" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="OverPayment Amount" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblOverPayAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("OverPaymentAmount"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalOverpayAmount" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                                <HeaderStyle CssClass="HeaderGridView2" />
                                                <RowStyle CssClass="RowGridView2" />
                                                <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                                <FooterStyle CssClass="FooterGridView2" />
                                                <%-- <AlternatingRowStyle Height="10px" BackColor="#F5F6CE" />
                                                <HeaderStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />
                                                <RowStyle BackColor="#ffffff" Height="10px" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#F7BE81" />
                                                <FooterStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />--%>
                                            </asp:GridView>
                                        </td>
                                        <td width="20px" style="vertical-align: top;">&nbsp;
                                        </td>
                                    </tr>
                                    <%--       <tr>
                                        <td colspan="2">
                                            <table id="tblOptionTermPayable" runat="server" width="100%" style="font-weight: normal; font-size: 10px;">
                                                <tr>
                                                    <td width="15%">Commission Due:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:Label ID="Label4" runat="server" Text=""></asp:Label><br />
                                                        <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td width="50%">
                                                        <asp:Label ID="Label7" runat="server" Text=""></asp:Label><br />
                                                        <asp:Label ID="Label8" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td style="background-color: lightgray;" id="tdContingentTerm" runat="server">
                                            <asp:Label BackColor="#D8D8D8" Width="99.7%" Font-Bold="True" ForeColor="Black"
                                                ID="lblContingentTerm" Style="height: 15px; vertical-align: top; font-size: 12px; border: solid 1px black; border-bottom: none; text-align: center;" runat="server" Text="CONTINGENT TERM"></asp:Label>
                                            <asp:GridView ID="gvContingentTerm" ShowFooter="True" Font-Size="12px" font-family="Verdana"
                                                Width="100%" CssClass="OuterBorder" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvContingentTerm_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField DataField="BrokerName" HeaderText="VENDOR" ItemStyle-HorizontalAlign="left"
                                                        FooterText="Total" FooterStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="BrokerRole" HeaderText="ROLE" ItemStyle-HorizontalAlign="Center" />
                                                    <asp:TemplateField HeaderText="% DUE" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPercent" runat="server" Text='<%# String.Format("{0:n2}",Eval("BrokerPercent"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%-- <asp:BoundField DataField="BrokerPercent" HeaderText="% DUE" />--%>
                                                    <asp:TemplateField HeaderText="COMMISSION DUE" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblCOMMISSIONDUE" runat="server" Text='<%# String.Format("{0:n2}",Eval("AmountDue"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalCOMMISSIONDUE" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="COMMISSION PAID" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblCOMMISSIONPAID" runat="server" Text='<%# String.Format("{0:n2}",Eval("PaymentReceived"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalCOMMISSIONPAID" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="AMOUNT UNPAID" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblAMOUNTUNPAID" runat="server" Text='<%# String.Format("{0:n2}",Eval("AmountUpaid"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalAMOUNTUNPAID" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="OverPayment Amount" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            $<asp:Label ID="lblOverPayAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("OverPaymentAmount"))%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lbltotalOverpayAmount" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                                <HeaderStyle CssClass="HeaderGridView2" />
                                                <RowStyle CssClass="RowGridView2" />
                                                <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                                <FooterStyle CssClass="FooterGridView2" />
                                                <%--  <AlternatingRowStyle Height="10px" BackColor="#F5F6CE" />
                                                <HeaderStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />
                                                <RowStyle BackColor="#ffffff" Height="10px" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#F7BE81" />
                                                <FooterStyle BackColor="#D8D8D8" Height="25px" Font-Bold="True" ForeColor="Black" />--%>
                                            </asp:GridView>
                                        </td>
                                        <td width="20px" style="vertical-align: top;">&nbsp;
                                        </td>
                                    </tr>
                                    <%--     <tr>
                                        <td colspan="2">
                                            <table id="tblContingentTerm" runat="server" width="100%" style="font-weight: normal; font-size: 10px;">
                                                <tr>
                                                    <td width="15%">Commission Due:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label9" runat="server" Text=""></asp:Label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:Label ID="Label10" runat="server" Text=""></asp:Label><br />
                                                        <asp:Label ID="Label11" runat="server" Text=""></asp:Label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:Label ID="Label12" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td width="50%">
                                                        <asp:Label ID="Label13" runat="server" Text=""></asp:Label><br />
                                                        <asp:Label ID="Label14" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>--%>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<%--<asp:UpdatePanel ID="UpdatePanel6" runat="server">
    <ContentTemplate>
        <div id="divPaymentHistoryDetail" runat="server" style=" display:none;" >
                <uc1:ucPaymentHistoryDetail ID="ucPaymentHistoryDetail1" runat="server" /> 
        </div>
    </ContentTemplate>
</asp:UpdatePanel> --%>
