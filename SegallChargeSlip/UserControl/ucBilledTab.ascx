﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucBilledTab.ascx.cs" Inherits="UserControl_ucBilledTab" %>
<%@ Register Src="~/UserControl/ucChargeDetailCard.ascx" TagName="ucChargeDetailCard" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/ucPaymentHistoryDetail.ascx" TagName="ucPaymentHistoryDetail" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/ucAddNewShoppingCenterOrCompany.ascx" TagName="AddNewShoppingCenterOrCompany" TagPrefix="uc" %>
<link href="../Styles/SiteTEST.css" rel="stylesheet" type="text/css" />
<link href="../Styles/AutoCompleteStyle.css" rel="stylesheet" type="text/css" />
<script src="../Js/CommonValidations.js"></script>
<%--<script src="../Js/ScrollableGrid.js" type="text/javascript"></script>--%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv='X-UA-Compatible' content='IE=7' />
<link rel='stylesheet' type='text/css' href='Styles/StaticHeader.css' />

<script type='text/javascript' src='Styles/x.js'></script>
<%--<script src="../Js/Scrollable.js"></script>--%>

<%--<script src="../Js/jquery-1.4.1.min.js"></script>--%>
<%--<script src="../Js/Scrollable.js"></script>--%>

<%--<script src="../Js/ScrollableGridPlugin.js"></script>--%>
<script language="javascript" type="text/javascript">
  
    function MakeStaticHeader(gridId, height, width, headerHeight, isFooter) {
        debugger;
        //alert('hi');
        var div_position = document.getElementById("div_position");
        var position = parseInt(div_position.value);
        if (isNaN(position)) {
            position = 0;
        }    
        //alert(position);
        var tbl = document.getElementById(gridId);
        width = tbl.offsetWidth;
        if (parseInt(tbl.offsetHeight) > 368) {
          
            if (tbl) {
                //   alert(width);
                //   alert('inside');
                //   alert('call');
                var DivHR = document.getElementById('DivHeaderRow');
                var DivMC = document.getElementById('DivMainContent');
                var DivFR = document.getElementById('DivFooterRow');

                //*** Set divheaderRow Properties ****
                DivHR.style.height = (parseInt(headerHeight)) + 'px';
                DivHR.style.width = (parseInt(width)) -16 + 'px';
                //alert(width);
                //alert(DivHR.style.width);
                DivHR.style.position = 'relative';
                DivHR.style.top = '0px';
                DivHR.style.zIndex = '10';
                DivHR.style.verticalAlign = 'top';

                //*** Set divMainContent Properties ****
                DivMC.style.width = width + 'px';
                DivMC.style.height = height + 'px';
                DivMC.style.position = 'relative';
                DivMC.style.top = -(headerHeight) + 'px';
                DivMC.scrollTop=div_position.value;
                DivMC.style.zIndex = '1';

                //*** Set divFooterRow Properties ****
                DivFR.style.width = (parseInt(width) - 16) + 'px';
                DivFR.style.position = 'relative';
                DivFR.style.top = -headerHeight + 'px';
                DivFR.style.verticalAlign = 'top';
                DivFR.style.paddingtop = '2px';
                //DivFR.style.zIndex = '10';

                if (isFooter) {
                    var tblfr = tbl.cloneNode(true);
                    tblfr.removeChild(tblfr.getElementsByTagName('tbody')[0]);
                    var tblBody = document.createElement('tbody');
                    tblfr.style.width = '100%';
                    tblfr.cellSpacing = "0";
                    tblfr.border = "0px";
                    tblfr.rules = "all";
                    //*****In the case of Footer Row *******
                    tblBody.appendChild(tbl.rows[tbl.rows.length - 1]);
                    tblfr.appendChild(tblBody);
                    DivFR.appendChild(tblfr);
                }
                //****Copy Header in divHeaderRow****
                DivHR.appendChild(tbl.cloneNode(true));
               
            }
        }
    }



    function OnScrollDiv(Scrollablediv) {
        document.getElementById('DivHeaderRow').scrollLeft = Scrollablediv.scrollLeft;
        document.getElementById('DivFooterRow').scrollLeft = Scrollablediv.scrollLeft;

        var div_position = document.getElementById("div_position");
        var position = parseInt('<%=Request.Form["div_position"] %>');
        if (isNaN(position)) {
            position = 0;
        }       
        div_position.value = Scrollablediv.scrollTop;
        //document.getElementById('DivHeaderRow').scrollTop = Scrollablediv.scrollTop;
        //document.getElementById('DivFooterRow').scrollTop = Scrollablediv.scrollTop;
    }


</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        // applyscrollable();
        FixedCustomerGrid();
        FixedVendorGrid();
        FixedTransGrid();
        MakeStaticHeader();
    });
    function applyscrollable() {
        $('#<%=gvDealInfo.ClientID %>').Scrollable101({
            ScrollHeight: 300
        });
    }
</script>

<style type="text/css">
    .HeaderGridViewDealInfo {
        height: 25px;
        background-color: #D8D8D8;
        font-weight: bold;
        color: Black;
        text-align: center;
        position: absolute;
        word-wrap: break-word;
        /*text-wrap:normal;
   
position: absolute;
top:expression(this.parentNode.parentNode.parentNode.scrollTop-2);
left:expression(this.parentNode.parentNode.parentNode.scrollLeft-1);
right:1px;*/
    }

    .SetWidth {
        width: 8.5in;
        height: 11in;
    }

    .Notes {
        vertical-align: top;
        vertical-align: top;
    }

    .Paddingtd {
        padding-left: 20px;
    }

    .tabs {
        font-weight: bold;
        color: Black;
        font-size: small;
        text-decoration: none;
        text-align: center;
        vertical-align: middle;
        line-height: 50px;
        border: solid 1px black;
    }

    .ddl {
        color: Black;
        font-size: small;
    }

    th a {
        text-decoration: none;
    }

    .g-search-button {
        display: inline-block;
        width: 16px;
        height: 10px;
        position: relative;
        /*  background-color: black;  Replace with your own image */
    }

    .popup {
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        position: fixed;
        width: 500px;
        height: 250px;
        border: solid 2px black;
        background-color: white;
        z-index: 1002;
        /*overflow: auto;*/
        text-align: left;
    }

    .modalBackground {
        background-color: gray;
        filter: alpha(opacity=90);
        opacity: 0.8;
    }

    .modalPopup {
        background-color: #FFFFFF;
        border-width: 1px;
        border-style: solid;
        border-color: black;
        /*padding-top: 10px;
        padding-left: 10px;*/
        /*width: 300px;
        height: 140px;*/
    }


    /*.AlternatingRowGridView2 {
        height: 10px;
        background-color: #F5F6CE;
    }

    .HeaderGridView2 {
        height: 25px;
        background-color: #D8D8D8;
        font-weight: bold;
        color: Black;
        text-align: center;
    }

    .RowGridView2 {
        background-color: #ffffff;
        height: 10px;
        color: Black;
    }

    .SelectedRowGridView2 {
        background-color: #F7BE81;
    }

    .FooterGridView2 {
        background-color: #D8D8D8;
        height: 25px;
        color: Black;
        font-weight: bold;
        text-align: right;
    }*/


    .LabelStyle {
        font-weight: bold;
    }
</style>
<script type="text/javascript">


    $(window).load(function () {
        //FixedVendorGrid();
        //FixedCustomerGrid();
        //// FixedDealInfoGrid();
        //FixedTransGrid();
    });
    function FixedTransGrid() {
        // alert('dealinfo');
        var GridIdData = "<%=gvTransInfo.ClientID %>";
        var ScrollHeightData = 520;
        var GridId = GridIdData;
        var ScrollHeight = ScrollHeightData;
        var grid = document.getElementById(GridId);
        var gridWidth = grid.offsetWidth;
        var gridHeight = grid.offsetHeight;
        var headerCellWidths = new Array();
        for (var i = 0; i < grid.getElementsByTagName("TH").length; i++) {
            headerCellWidths[i] = grid.getElementsByTagName("TH")[i].offsetWidth;
        }
        grid.parentNode.appendChild(document.createElement("div"));
        var parentDiv = grid.parentNode;

        var table = document.createElement("table");
        for (i = 0; i < grid.attributes.length; i++) {
            if (grid.attributes[i].specified && grid.attributes[i].name != "id") {
                table.setAttribute(grid.attributes[i].name, grid.attributes[i].value);
            }
        }
        table.style.cssText = grid.style.cssText;
        table.style.width = gridWidth + "px";
        table.appendChild(document.createElement("tbody"));
        table.getElementsByTagName("tbody")[0].appendChild(grid.getElementsByTagName("TR")[0]);
        var cells = table.getElementsByTagName("TH");

        var gridRow = grid.getElementsByTagName("TR")[0];
        for (var i = 0; i < cells.length; i++) {
            var width;
            if (headerCellWidths[i] > gridRow.getElementsByTagName("TD")[i].offsetWidth) {
                width = headerCellWidths[i];
            }
            else {
                width = gridRow.getElementsByTagName("TD")[i].offsetWidth;
            }
            cells[i].style.width = parseInt(width - 3) + "px";
            gridRow.getElementsByTagName("TD")[i].style.width = parseInt(width - 3) + "px";
        }
        parentDiv.removeChild(grid);

        var dummyHeader = document.createElement("div");
        dummyHeader.appendChild(table);
        parentDiv.appendChild(dummyHeader);
        var scrollableDiv = document.createElement("div");
        if (parseInt(gridHeight) > ScrollHeight) {
            gridWidth = parseInt(gridWidth) + 17;
        }
        scrollableDiv.style.cssText = "overflow:auto;height:" + ScrollHeight + "px;width:" + gridWidth + "px";
        scrollableDiv.appendChild(grid);
        parentDiv.appendChild(scrollableDiv);
        // FixedGridHeader(GridIdData, ScrollHeightData);
    }
    function FixedDealInfoGrid() {
        // alert('dealinfo');
        var GridIdData = "<%=gvDealInfo.ClientID %>";
        var ScrollHeightData = 343;
        var GridId = GridIdData;
        var ScrollHeight = ScrollHeightData;
        var grid = document.getElementById(GridId);
        var gridWidth = grid.offsetWidth;
        alert(grid.offsetWidth);
        alert(grid.offsetHeight);
        var gridHeight = grid.offsetHeight;
        var headerCellWidths = new Array();
        for (var i = 0; i < grid.getElementsByTagName("TH").length; i++) {
            headerCellWidths[i] = grid.getElementsByTagName("TH")[i].offsetWidth;
        }
        grid.parentNode.appendChild(document.createElement("div"));
        var parentDiv = grid.parentNode;

        var table = document.createElement("table");
        for (i = 0; i < grid.attributes.length; i++) {
            if (grid.attributes[i].specified && grid.attributes[i].name != "id") {
                table.setAttribute(grid.attributes[i].name, grid.attributes[i].value);
            }
        }
        table.style.cssText = grid.style.cssText;
        table.style.width = gridWidth + "px";
        table.appendChild(document.createElement("tbody"));
        table.getElementsByTagName("tbody")[0].appendChild(grid.getElementsByTagName("TR")[0]);
        var cells = table.getElementsByTagName("TH");

        var gridRow = grid.getElementsByTagName("TR")[0];
        for (var i = 0; i < cells.length; i++) {
            var width;
            if (headerCellWidths[i] > gridRow.getElementsByTagName("TD")[i].offsetWidth) {
                width = headerCellWidths[i];
            }
            else {
                width = gridRow.getElementsByTagName("TD")[i].offsetWidth;
            }
            cells[i].style.width = parseInt(width - 3) + "px";
            gridRow.getElementsByTagName("TD")[i].style.width = parseInt(width - 3) + "px";
        }
        parentDiv.removeChild(grid);

        var dummyHeader = document.createElement("div");
        dummyHeader.appendChild(table);
        parentDiv.appendChild(dummyHeader);
        var scrollableDiv = document.createElement("div");
        if (parseInt(gridHeight) > ScrollHeight) {
            gridWidth = parseInt(gridWidth) + 17;
        }
        else {
            //gridWidth = parseInt(gridWidth) - 17;
        }
        scrollableDiv.style.cssText = "overflow:auto;height:" + ScrollHeight + "px;width:" + gridWidth + "px";
        scrollableDiv.appendChild(grid);
        parentDiv.appendChild(scrollableDiv);
        // FixedGridHeader(GridIdData, ScrollHeightData);
    }
    function FixedVendorGrid() {
        var div_Vendor_Postion = document.getElementById("div_Vendor_Postion");    
        //debugger;
        // alert(div_Vendor_Postion.value);
        var GridIdData = "<%=gvVendor.ClientID %>";
        var ScrollHeightData = 475;
        var GridId = GridIdData;
        var ScrollHeight = ScrollHeightData;
        var grid = document.getElementById(GridId);
        var gridWidth = grid.offsetWidth;
        var gridHeight = grid.offsetHeight;
        var headerCellWidths = new Array();
        for (var i = 0; i < grid.getElementsByTagName("TH").length; i++) {
            headerCellWidths[i] = grid.getElementsByTagName("TH")[i].offsetWidth;
        }
        grid.parentNode.appendChild(document.createElement("div"));
        var parentDiv = grid.parentNode;

        var table = document.createElement("table");
        for (i = 0; i < grid.attributes.length; i++) {
            if (grid.attributes[i].specified && grid.attributes[i].name != "id") {
                table.setAttribute(grid.attributes[i].name, grid.attributes[i].value);
            }
        }
        table.style.cssText = grid.style.cssText;
        table.style.width = gridWidth + "px";
        table.appendChild(document.createElement("tbody"));
        table.getElementsByTagName("tbody")[0].appendChild(grid.getElementsByTagName("TR")[0]);
        var cells = table.getElementsByTagName("TH");

        var gridRow = grid.getElementsByTagName("TR")[0];
        for (var i = 0; i < cells.length; i++) {
            var width;
            if (headerCellWidths[i] > gridRow.getElementsByTagName("TD")[i].offsetWidth) {
                width = headerCellWidths[i];
            }
            else {
                width = gridRow.getElementsByTagName("TD")[i].offsetWidth;
            }
            cells[i].style.width = parseInt(width - 3) + "px";
            gridRow.getElementsByTagName("TD")[i].style.width = parseInt(width - 3) + "px";
        }
        parentDiv.removeChild(grid);

        var dummyHeader = document.createElement("div");
        dummyHeader.appendChild(table);
        parentDiv.appendChild(dummyHeader);
        var scrollableDiv = document.createElement("div");
        if (parseInt(gridHeight) > ScrollHeight) {
            gridWidth = parseInt(gridWidth) + 17;
        }
        var windowWidth = document.documentElement.clientWidth;
        var windowHeight = document.documentElement.clientHeight;
        var popupHeight = ScrollHeight;
        var popupWidth = gridWidth;
       
        scrollableDiv.style.cssText = "overflow:auto;height:" + ScrollHeight + "px;width:" + gridWidth + "px;top:" + (windowHeight / 2 - popupHeight / 2) + $(scrollableDiv).scrollTop() + "px;";
       
        scrollableDiv.id="grdVendor";       
        scrollableDiv.appendChild(grid);       
        

        parentDiv.appendChild(scrollableDiv);
        var grdVendor = document.getElementById("grdVendor");      
        grdVendor.onscroll=function(){  
            //alert(div_Vendor_Postion.value);
            debugger;
            div_Vendor_Postion.value=grdVendor.scrollTop;
            //alert(div_Vendor_Postion.value);
        };
        // alert(div_Vendor_Postion.value);
        grdVendor.scrollTop=div_Vendor_Postion.value;
      
    }
    function FixedCustomerGrid() {
        //alert('cust');
        var div_Customer_Postion = document.getElementById("div_Customer_Postion");         
        var GridIdData = "<%=gvCustInfo.ClientID %>";
        var ScrollHeightData = 475;
        var GridId = GridIdData;
        var ScrollHeight = ScrollHeightData;
        var grid = document.getElementById('<%=gvCustInfo.ClientID %>');
        var gridWidth = grid.offsetWidth;
        var gridHeight = grid.offsetHeight;
        var headerCellWidths = new Array();
        for (var i = 0; i < grid.getElementsByTagName("TH").length; i++) {
            headerCellWidths[i] = grid.getElementsByTagName("TH")[i].offsetWidth;
        }
        grid.parentNode.appendChild(document.createElement("div"));
        var parentDiv = grid.parentNode;

        var table = document.createElement("table");
        for (i = 0; i < grid.attributes.length; i++) {
            if (grid.attributes[i].specified && grid.attributes[i].name != "id") {
                table.setAttribute(grid.attributes[i].name, grid.attributes[i].value);
            }
        }
        table.style.cssText = grid.style.cssText;
        table.style.width = gridWidth + "px";
        table.appendChild(document.createElement("tbody"));
        table.getElementsByTagName("tbody")[0].appendChild(grid.getElementsByTagName("TR")[0]);
        var cells = table.getElementsByTagName("TH");

        var gridRow = grid.getElementsByTagName("TR")[0];
        for (var i = 0; i < cells.length; i++) {
            var width;
            if (headerCellWidths[i] > gridRow.getElementsByTagName("TD")[i].offsetWidth) {
                width = headerCellWidths[i];
            }
            else {
                width = gridRow.getElementsByTagName("TD")[i].offsetWidth;
            }
            cells[i].style.width = parseInt(width - 3) + "px";
            gridRow.getElementsByTagName("TD")[i].style.width = parseInt(width - 3) + "px";
        }
        parentDiv.removeChild(grid);

        var dummyHeader = document.createElement("div");
        dummyHeader.appendChild(table);
        parentDiv.appendChild(dummyHeader);
        var scrollableDiv = document.createElement("div");
        if (parseInt(gridHeight) > ScrollHeight) {
            gridWidth = parseInt(gridWidth) + 17;
        }
        scrollableDiv.style.cssText = "overflow:auto;height:" + ScrollHeight + "px;width:" + gridWidth + "px";
        scrollableDiv.id="gvCustInfo";   
        scrollableDiv.appendChild(grid);
      
        parentDiv.appendChild(scrollableDiv);
        var gvCustInfo = document.getElementById("gvCustInfo"); 
        gvCustInfo.onscroll=function(){                
            div_Customer_Postion.value=gvCustInfo.scrollTop;
        };
        gvCustInfo.scrollTop=div_Customer_Postion.value;
        //FixedGridHeader(GridIdData1, ScrollHeightData1);
    }
    function FixedGridHeader(GridIdData, ScrollHeightData) {
        //alert('hi');
        var GridId = GridIdData;
        var ScrollHeight = ScrollHeightData;
        var grid = document.getElementById(GridId);
        var gridWidth = grid.offsetWidth;
        var gridHeight = grid.offsetHeight;
        var headerCellWidths = new Array();
        for (var i = 0; i < grid.getElementsByTagName("TH").length; i++) {
            headerCellWidths[i] = grid.getElementsByTagName("TH")[i].offsetWidth;
        }
        grid.parentNode.appendChild(document.createElement("div"));
        var parentDiv = grid.parentNode;

        var table = document.createElement("table");
        for (i = 0; i < grid.attributes.length; i++) {
            if (grid.attributes[i].specified && grid.attributes[i].name != "id") {
                table.setAttribute(grid.attributes[i].name, grid.attributes[i].value);
            }
        }
        table.style.cssText = grid.style.cssText;
        table.style.width = gridWidth + "px";
        table.appendChild(document.createElement("tbody"));
        table.getElementsByTagName("tbody")[0].appendChild(grid.getElementsByTagName("TR")[0]);
        var cells = table.getElementsByTagName("TH");

        var gridRow = grid.getElementsByTagName("TR")[0];
        for (var i = 0; i < cells.length; i++) {
            var width;
            if (headerCellWidths[i] > gridRow.getElementsByTagName("TD")[i].offsetWidth) {
                width = headerCellWidths[i];
            }
            else {
                width = gridRow.getElementsByTagName("TD")[i].offsetWidth;
            }
            cells[i].style.width = parseInt(width - 3) + "px";
            gridRow.getElementsByTagName("TD")[i].style.width = parseInt(width - 3) + "px";
        }
        parentDiv.removeChild(grid);

        var dummyHeader = document.createElement("div");
        dummyHeader.appendChild(table);
        parentDiv.appendChild(dummyHeader);
        var scrollableDiv = document.createElement("div");
        if (parseInt(gridHeight) > ScrollHeight) {
            gridWidth = parseInt(gridWidth) + 17;
        }
        scrollableDiv.style.cssText = "overflow:auto;height:" + ScrollHeight + "px;width:" + gridWidth + "px";
        scrollableDiv.appendChild(grid);
        parentDiv.appendChild(scrollableDiv);
    }
</script>
<%--script from payment individual broker--%>

<script type="text/javascript">
    function UpdateCreditAmount() {
        debugger;
        
        var x;
        if (confirm("Do you want to Credit the Amount?") == true) {
            $.ajax({
                async: true,
                type: "POST",
                url: "Customers.aspx/UpdateCreditAmount",
                data: '{CommissionDueId: "' + document.getElementById('<%=hdnCommissionDueDateId.ClientID%>').value + '",OutstandingAmount: "' + document.getElementById('<%=hdnOutstanding.ClientID%>').value + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //alert('p');
                    if (response.d == "") {



                    }
                    else {
                        debugger;
                        var sessionid=<%=Session["IsVendor"] %>;
                        if(sessionid!=null)
                        {
                            if(sessionid=="1")
                            {
                                document.getElementById('<%=btnFillGVDealInfo.ClientID%>').click();
                            }
                        }
                        document.getElementById('<%=trCreditAmount.ClientID%>').style.display = "";
                        
                        debugger;
                       
                       


                    }
                },
                failure: function (response) {

                    alert(response.d);
                    return false;
                }
            });
            x = "You pressed OK!";
        } else {
            x = "You pressed Cancel!";
        }

        return false;
    }
    function PaymentLink(strChargeslipId) {
        //alert('hi');
        //alert(str);
        window.open("../Admin/Payments.aspx?ChargeslipId=" + strChargeslipId,"_self");
        return false;
    }
    function ValidateNumber(event) {

        var regex = new RegExp("^[0-9 \-]+$");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();

            return false;
        }
    }
    function ClearControl() {

        document.getElementById('<%=txtStartDate.ClientID%>').value = '';
        document.getElementById('<%=txtEndDate.ClientID%>').value = '';
        document.getElementById('<%=ddlViewDealGV.ClientID%>').value = 0;

    }
    function ClearControlTran() {

        document.getElementById('<%=txtTransForm.ClientID%>').value = '';
        document.getElementById('<%=txtTransTo.ClientID%>').value = '';
        document.getElementById('<%=ddlTransaction.ClientID%>').value = 0;
    }
</script>
<script type="text/javascript">
    function ValidateDate() {

        var fromdate = document.getElementById('<%=txtStartDate.ClientID %>');
        var todate = document.getElementById('<%=txtEndDate.ClientID %>');
        if ($.trim(fromdate.value) != "") {
            if (!isDate(fromdate)) {
                return false;
            }
        }
        if ($.trim(todate.value) != "") {
            if (!isDate(todate)) {
                return false;
            }
        }
        if ($.trim(fromdate.value) != "" && $.trim(todate.value) != "") {
            if (Date.parse($.trim(fromdate.value)) > Date.parse($.trim(todate.value))) {
                alert('FromDate cannot be greater than To Date');
                return false;
            }
        }
    }
    function ValidateDateTran() {
        var fromdate = document.getElementById('<%=txtTransForm.ClientID %>');
        var todate = document.getElementById('<%=txtTransTo.ClientID %>');
        if ($.trim(fromdate.value) != "") {
            if (!isDate(fromdate)) {
                return false;
            }
        }
        if ($.trim(todate.value) != "") {
            if (!isDate(todate)) {
                return false;
            }
        }
        if ($.trim(fromdate.value) != "" && $.trim(todate.value) != "") {
            if (Date.parse($.trim(fromdate.value)) > Date.parse($.trim(todate.value))) {
                fromdate.value =
                alert('FromDate cannot be greater than To Date');
                return false;
            }
        }
    }
</script>
<script type="text/javascript">

    var oldgridcolor;
    function SetMouseOver(element) {
        oldgridcolor = element.style.backgroundColor;
        element.style.backgroundColor = '#ffeb95';
        element.style.cursor = 'pointer';
    }
    function SetMouseOut(element) {
        element.style.backgroundColor = oldgridcolor;
        element.style.textDecoration = 'none';
    }

    function ClickTrCust(id, rowid) {
        // //debugger;
        document.getElementById('<%=hdnCustRowID.ClientID%>').value = rowid;
        document.getElementById('<%=hdnCompanyID.ClientID%>').value = id;
        document.getElementById('<%=btnFillCustForm.ClientID%>').click();
    }
    function ClickTrVendor(id, rowid) {
        // //debugger;
        document.getElementById('<%=hdnVendorRowId.ClientID%>').value = rowid;
        document.getElementById('<%=hdnUserId.ClientID%>').value = id;
        document.getElementById('<%=btnFillVendorForm.ClientID%>').click();

    }

    function ClickTrDeal(id, DueDateId, rowid) {
        debugger;
       
        document.getElementById('<%=hdnDealRowID.ClientID%>').value = rowid;
        document.getElementById('<%=hdnDealTransactionID.ClientID%>').value = id;
        document.getElementById('<%=hdnCommissionDueDateId.ClientID%>').value = DueDateId;
        document.getElementById('<%=btnFillDealForm.ClientID%>').click();
        
    }

    function display() {
        //debugger;
        //alert('hi');
        document.getElementById('<%=divChargeDetailCard.ClientID%>').style.display = "block";
        return false;
    }

    function Hide() {
        //debugger;
        document.getElementById('<%=divChargeDetailCard.ClientID%>').style.display = "none";
        document.getElementById('<%=hdnPopup.ClientID %>').value = "0";
        <% Session["FileAttachment"] = null; %>
        return false;
    }

    function displayPayHistory() {
        //debugger;
        document.getElementById('<%=divPaymentHistoryDetail.ClientID%>').style.display = "block";

        //alert(document.getElementById('<%=divPaymentHistoryDetail.ClientID%>'));

    }

    function HidePayHistory() {
        //debugger;
        document.getElementById('<%=divPaymentHistoryDetail.ClientID%>').style.display = "none";


    }

    function test() {
        //debugger;

        return false;
    }

    function hidepopup() {

        var vhdnpopup = document.getElementById('<%=hdnPopup.ClientID %>');

        if (vhdnpopup.value == "1") {
            HidePayHistory();
            vhdnpopup.value = "0";
            return display();
        }
        else {
            HidePayHistory();
        }
    }
   

</script>
<script type="text/javascript">
    function validateZipLen(strzip) {
        debugger;
        var strziptrm = $.trim(strzip);
        if (strziptrm.length < 5 || strziptrm.length > 10) {
            return false;
        }
        return true;
    }
    function ValidateControl() {
        var Company = document.getElementById('<%=txtCompany.ClientID %>');
        var Address = document.getElementById('<%=txtAddress.ClientID %>');
        var City = document.getElementById('<%=txtCity.ClientID %>');
        var State = document.getElementById('<%=ddlState.ClientID %>');
        var ZipCode = document.getElementById('<%=txtZipCode.ClientID %>');
        var Name = document.getElementById('<%=txtName.ClientID %>');
        var Email = document.getElementById('<%=txtEmail.ClientID %>');
        var Ein = document.getElementById('<%=txtEin.ClientID %>');
        var Share = document.getElementById('<%=txtShare.ClientID %>');
        var SetAmount = document.getElementById('<%=txtSetAmount.ClientID %>');
        var ErrorMsg = document.getElementById('<%=lblErrorMsg.ClientID %>');
        if (Company.value == "") {
            ErrorMsg.innerHTML = "Please Enter Company";
            Company.focus();
            return false;
        }
        if (Address.value == "") {
            ErrorMsg.innerHTML = "Please Enter Address";
            Address.focus();
            return false;
        }
        if (City.value == "") {
            ErrorMsg.innerHTML = "Please Enter City";
            City.focus();
            return false;
        }
        if (State.value == "") {
            ErrorMsg.innerHTML = "Please Select State";
            State.focus();
            return false;
        }
        if (ZipCode.value == "") {
            ErrorMsg.innerHTML = "Please Enter ZipCode";
            ZipCode.focus();
            return false;
        }
        if (ZipCode.value != "") {
            if (!validateZipLen(ZipCode.value)) {
                ErrorMsg.innerHTML = "Please Enter between 5 and 10 digit valid Zip Code.";
                ZipCode.focus();
                return false;
                //     $("#ContentMain_txtLocZipCode").css({ "border": "3px red groove", "border-bottom-style": "double", "border-top-style": "double" });
            }
        }
        if (Name.value == "") {
            ErrorMsg.innerHTML = "Please Enter Name";
            Name.focus();
            return false;
        }
        if (Email.value == "") {
            ErrorMsg.innerHTML = "Please Enter Email";
            Email.focus();
            return false;
        }
        if (Ein.value == "") {
            ErrorMsg.innerHTML = "Please Enter Ein";
            Ein.focus();
            return false;
        }
        if (Share.value == "") {
            ErrorMsg.innerHTML = "Please Enter Share";
            Share.focus();
            return false;
        }
        if (SetAmount.value == "") {
            ErrorMsg.innerHTML = "Please Enter SetAmount";
            SetAmount.focus();
            return false;
        }
        return true;
    }
</script>
<script type="text/javascript">
    function ShowIcon(sender, e) {
        sender._element.className = "loading";
    }

    function hideIcon(sender, e) {
        sender._element.className = "textwidth";
    }
    function OnCenterSelected(source, eventArgs) {
        //alert(eventArgs.get_value());
        document.getElementById('<%=hdnBrokerId.ClientID %>').value = eventArgs.get_value();
        //hdnLocId JAY//   alert(eventArgs.get_value());
        return false;
    }
</script>
<%--<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function (sender, args) {
        if (args.get_error() && args.get_error().name === 'Sys.WebForms.PageRequestManagerTimeoutException') {
            args.set_errorHandled(true);
        }
    });
</script>--%>
<script type="text/javascript">
    //Add for Payment Void on 15-07-2016
    function ClearPaymentDetail(chargeid,GrossAmountDue)
    {
        alert('hi');
        if(chargeid>0)
        {
            alert('hi12');
            if(confirm('Are you sure you want to void the Payment for this Chargeslip.?'))
            {
                $.ajax({
                    type: "POST",
                    url: "../Pages/Billed.aspx/UpdateChargeslipPaymentVoid",
                    data: '{ChargeslipId:' + chargeid + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if(result.d=="success")
                        {
                            alert('The Payment is voided successfully for this Chargeslip');
                            document.getElementById('<%=btnVoidPayment.ClientID%>').click();
                            document.getElementById('<%=lblAmountDue.ClientID%>').innerText="$"+GrossAmountDue;
                            document.getElementById('<%=lblOutstanding.ClientID%>').innerText="$"+GrossAmountDue;
                            document.getElementById('<%=hdnOutstanding.ClientID%>').value=GrossAmountDue;
                            document.getElementById('<%=lblCreditAmount.ClientID%>').innerText="$0.00";
                            document.getElementById('<%=lblOverPaymentAmount.ClientID%>').innerText="$0.00";
                        }
                        else
                        {
                            alert('The Payment is fail to void for this Chargeslip due to some error occurred.');
                        }
                  
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
            
        }
        return false;
    }
    function ShowPopUp(dealname, chargeid) {
        debugger

        $('#<%=dvChargeSlipDelete.ClientID %>').show();
        $('#<%=lblDeleteChargeslipDealName.ClientID %>').text(dealname);
        //  $('#<%=txtDeleteCharslipReason.ClientID %>').text("");
        $('#<%=hdnChargeSlipidDel.ClientID %>').val(chargeid);

    }
    function checkDeleteReason() {

        var reason = $('#<%=txtDeleteCharslipReason.ClientID %>').val();
        // alert(reason);
        if (reason == '' || reason == null) {
            $('#<%=lblDelerror.ClientID %>').text("Please enter valid reason");
            return false;
        }
        else {

            $('#<%=lblDelerror.ClientID %>').text('');
            return true;
        }

    }
    function closepopup() {
        $('#<%=txtDeleteCharslipReason.ClientID %>').val('');
        $('#<%=dvChargeSlipDelete.ClientID %>').hide();
    }
    function BindGridAfterDelete() {
        debugger;
        var isvendor = '<%= Session["IsVendor"]%>'
        //alert(isvendor);
        if (isvendor == "0") {

            document.getElementById('<%=btnFillCustForm.ClientID%>').click();
        }
        else if (isvendor == "1") {

            document.getElementById('<%=btnFillVendorForm.ClientID%>').click();
        }
}
</script>
<input type="hidden" id="div_position" name="div_position" value="0" />
<input type="hidden" id="div_Vendor_Postion" name="div_Vendor_Postion" value="0" />
<input type="hidden" id="div_Customer_Postion" name="div_Customer_Postion" value="0" />
<asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360000"></asp:ScriptManager>
<table cellpadding="0" cellspacing="0" style="width: 100%; height: 90%; padding: 3px 3px 3px 3px;">
    <%--   font-family:Trebuchet MS;--%>
    <tr style="width: 100%; height: auto;">
        <td>
            <table cellpadding="0" cellspacing="3" style="width: 100%; height: 100%;">
                <tr>
                    <td style="width: 30%; vertical-align: top;">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div id="divTabs" style="height: 50px; margin: 5px 7px 0px 5px;">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="50%">
                                                <asp:LinkButton ID="lnkCust" CssClass="tabs" Height="50px" Width="100%"
                                                    runat="server" OnClick="lnkCust_Click" Text="CUSTOMERS & DEALS" OnClientClick="ClearControlTran();" />
                                            </td>
                                            <td width="50%">
                                                <asp:LinkButton ID="lnkTrans" CssClass="tabs" Height="50px" Width="100%"
                                                    runat="server" Text="TRANSACTIONS" OnClick="lnkTrans_Click" OnClientClick="ClearControl();" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="divTabsCust" runat="server" style="border: solid 1px black; height: 100px; margin: 0px 5px 5px 5px;">
                                    <table cellpadding="5" cellspacing="5" style="font-weight: bold; font-size: small; width: 100%; height: 100%; text-align: left;">
                                        <tr>
                                            <td width="20%">SHOW
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCustShow" CssClass="ddl" Width="100px" runat="server" Height="24px" AutoPostBack="true" OnSelectedIndexChanged="ddlCustShow_SelectedIndexChanged">
                                                    <asp:ListItem Value="0">ALL</asp:ListItem>
                                                    <asp:ListItem Value="1">OPEN</asp:ListItem>
                                                    <asp:ListItem Value="2">OVERDUE</asp:ListItem>
                                                    <%-- Selected="True"  modified by Shishir 04-05-2016--%>
                                                    <asp:ListItem Value="3">ACTIVE</asp:ListItem>
                                                    <asp:ListItem Value="4">INACTIVE</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>FIND
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCustFind" Style="width: 150px; font-size: small" runat="server" Height="18px"></asp:TextBox>
                                                <%--   <span style="padding-top :10px;" >
                                                   </span>--%>
                                            </td>
                                            <td>
                                                <span style="padding-right: 50px;">
                                                    <asp:ImageButton ID="imgCustSearch"
                                                        Style="height: 20px; width: 20px; margin-top: 0px;"
                                                        src="../Images/Search-Icon.gif" runat="server"
                                                        OnClick="imgCustSearch_Click" /></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="divTabsTrans" runat="server" style="border: solid 1px black; height: 604px; margin: 0px 5px 5px 5px;">
                                    <table cellpadding="5" cellspacing="5" style="width: 100%; font-weight: bold; font-size: small; height: 100px; text-align: left;">
                                        <tr>
                                            <td width="20%">SHOW
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlTransShow" CssClass="ddl" Width="100px" runat="server" OnSelectedIndexChanged="ddlTransShow_SelectedIndexChanged" AutoPostBack="true" Height="24px">
                                                    <asp:ListItem Value="1">ALL</asp:ListItem>
                                                    <asp:ListItem Value="2">OPEN</asp:ListItem>
                                                    <%--  <asp:ListItem Value="2">INVOICES</asp:ListItem>--%><%--Commented by Jaywanti on 24-08-2015--%>
                                                    <asp:ListItem Value="3">RECEIVED</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>FIND
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTransFind" Style="width: 150px; font-size: small" runat="server" Height="18px"></asp:TextBox>

                                                <%--<span class="g-search-button" style ="bottom :4px;">
                                                    <asp:ImageButton ID="imgTransSearch" Style="height: 20px; width: 20px; margin-top: 0px;" src="../Images/Search-Icon.gif" runat="server" OnClick ="imgTransSearch_Click"/></span>--%>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="ImageButton1" Style="height: 20px; width: 20px; margin-top: 0px;" src="../Images/Search-Icon.gif" runat="server" OnClick="imgTransSearch_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="divCustomers" runat="server">
                                    <div style="border: solid 1px black; margin: 5px;">
                                        <asp:GridView ID="gvCustInfo" Font-Size="Small" Width="100%" runat="server" AutoGenerateColumns="false"
                                            OnRowDataBound="gvCustInfo_RowDataBound" ShowFooter="True" OnSorting="gvCustInfo_Sorting" AllowSorting="true">
                                            <Columns>
                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ControlStyle-Width="10px">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnIsActive" runat="server" Value='<%#Eval("IsActive")%>' />
                                                        <asp:ImageButton ID="imgStatus" Height="8px" Width="5px" runat="server" />
                                                        <asp:Label ID="lblCompanyID" runat="server" Visible="false" Text='<%#Eval("CompanyID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField SortExpression="Customers" DataField="Customers" HeaderText="CUSTOMERS" ItemStyle-HorizontalAlign="Left" FooterText="Total" FooterStyle-HorizontalAlign="Left" />
                                                <asp:TemplateField SortExpression="Unapplied" HeaderText="UNAPPLIED" ItemStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        $<asp:Label ID="lblUnapplied" runat="server" Text='<%# String.Format("{0:n2}",Eval("Unapplied"))%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblCusUnapplied" runat="server" Text=""></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                            <HeaderStyle CssClass="HeaderGridView2" />
                                            <RowStyle CssClass="RowGridView2" />
                                            <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                            <FooterStyle CssClass="FooterGridView2" />
                                        </asp:GridView>
                                        <asp:HiddenField ID="hdnCompanyID" runat="server" />
                                        <asp:HiddenField ID="hdnCustRowID" runat="server" />
                                        <asp:Button ID="btnFillCustForm" runat="server" OnClick="btnFillCustForm_Click" Style="display: none;" OnClientClick="ClearControl();ClearControlTran();" />
                                    </div>
                                </div>
                                <div id="divVendorGrid" runat="server">
                                    <div style="border: solid 1px black; margin: 5px;">
                                        <asp:GridView ID="gvVendor" runat="server" AutoGenerateColumns="false" Font-Size="Small" Width="100%"
                                            OnRowDataBound="gvVendor_RowDataBound" ShowFooter="True" AllowSorting="true" OnSorting="gvVendor_Sorting">
                                            <Columns>
                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ControlStyle-Width="10px">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdntable" runat="server" Value='<%#Eval("tblNO")%>' />
                                                        <asp:HiddenField ID="hdnIsActive" runat="server" Value='<%#Eval("IsActive")%>' />
                                                        <asp:ImageButton ID="imgStatus" Height="8px" Width="5px" runat="server" />
                                                        <asp:Label ID="lblUserId" runat="server" Visible="false" Text='<%#Eval("UserID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField SortExpression="VendorName" DataField="VendorName" HeaderText="VENDORS" ItemStyle-HorizontalAlign="Left" FooterText="Total" FooterStyle-HorizontalAlign="Left" />
                                                <asp:TemplateField SortExpression="Unapplied" HeaderText="DUE" ItemStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUnapplied" runat="server" Text='<%# String.Format("{0:n2}",Eval("Unapplied"))%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblVenUnapplied" runat="server" Text=""></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                            <HeaderStyle CssClass="HeaderGridView2" />
                                            <RowStyle CssClass="RowGridView2" />
                                            <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                            <FooterStyle CssClass="FooterGridView2" />
                                        </asp:GridView>
                                        <asp:HiddenField ID="hdnUserId" runat="server" />
                                        <asp:HiddenField ID="hdnVendorRowId" runat="server" />
                                        <asp:Button ID="btnFillVendorForm" runat="server" OnClick="btnFillVendorForm_Click" Style="display: none;" OnClientClick="ClearControl();ClearControlTran();" />
                                    </div>
                                </div>
                                <div id="divTransactions" style="display: none;">
                                    <div style="border: solid 1px black; height: 510px; margin: 5px;">
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnExporttoPDF" />
                                <asp:PostBackTrigger ControlID="btnExportToExcel" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                    <td style="width: 70%; vertical-align: top;">
                        <div style="border: solid 1px black; margin: 5px;">
                            <table style="width: 100%; font-weight: bold; color: Black; font-size: small">
                                <tr style="height: 45px;">
                                    <td style="width: 84%; font-size: 14px;">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <table id="CustAndDealInfo" runat="server" style="border-bottom: solid 1px black;" width="100%">
                                                    <tr>
                                                        <%--<td id="tdCustAndDealInfo" runat ="server" >CUSTOMER AND DEAL INFORMATION WINDOW
                                                        </td>--%><td>
                                                            <asp:Label ID="lblCustAndDealInfo" runat="server" Text="CUSTOMER AND DEAL INFORMATION WINDOW"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div>
                                                    <table id="CustInfo" runat="server" style="border-bottom: solid 1px black;" width="100%">
                                                        <tr>
                                                            <td>CUSTOMER INFORMATION
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table id="DealInfo" style="border-bottom: solid 1px black;" width="100%" runat="server">
                                                        <tr>
                                                            <td>DEAL INFORMATION
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table id="VendorInfo" style="border-bottom: solid 1px black; width: 100%;" runat="server" visible="false">
                                                        <tr>
                                                            <td>VENDOR INFORMATION</td>
                                                        </tr>
                                                    </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td style="width: 16%; text-align: right; font-size: 12px; font-weight: 500;">EXPORT&nbsp;&nbsp;
                                    <asp:ImageButton ID="imgExport" Width="15px" Height="15px" runat="server"
                                        OnClick="imgExport_Click" ImageUrl="~/Images/excel.png" />&nbsp;&nbsp;
                                    <asp:ImageButton ID="imgPDF" Width="15px" Height="15px" runat="server"
                                        OnClick="imgPDF_Click" ImageUrl="~/Images/pdf.png" />
                                    </td>
                                </tr>
                                <tr id="CustDealInfo" runat="server">
                                    <td colspan="2">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <div id="divStart" style="height: 195px;" runat="server">
                                                </div>
                                                <div id="divCust" style="height: 195px;" runat="server">
                                                    <table id="Form1" runat="server" style="width: 70%; font-weight: normal; font-size: small;">
                                                        <tr>
                                                            <td width="35%" class="LabelStyle">Customer Name: 
                                                            </td>
                                                            <td width="60%">
                                                                <asp:Label ID="lblCustName" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td width="5%">

                                                                <asp:ImageButton ID="imgEdit" Width="15px" Height="15px" runat="server"
                                                                    ImageUrl="~/Images/Edit-Icon.gif" OnClick="imgEdit_Click" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LabelStyle">Contact:
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:Label ID="lblContact" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LabelStyle" id="trcompanyemail" runat="server">Email:
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: top;" class="LabelStyle">Billing Address:
                                                            </td>
                                                            <td colspan="2" style="text-align: left;">
                                                                <asp:Label ID="lblBillingAddress1" runat="server" Text=""></asp:Label>
                                                                <asp:Label ID="lblBillingAddress2" runat="server" Text=""></asp:Label>
                                                                <asp:Label ID="lblBillingCity" runat="server" Text=""></asp:Label>
                                                                <asp:Label ID="lblBillingState" runat="server" Text=""></asp:Label>
                                                                <asp:Label ID="lblBillingZip" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <div style="overflow-y: auto; height: 70px; word-wrap: normal; border: solid 1px black; vertical-align: top;" class="LabelStyle">

                                                                    <asp:Label ID="lblNotes" runat="server" Text=""></asp:Label>
                                                                </div>
                                                                <%-- <div style="overflow: scroll; height: 5px; border: solid 1px black; vertical-align: top;" class="LabelStyle">--%>
                                                                <%--  <table style="width: 100%;  >
                                                                    <tr>
                                                                        <td style="vertical-align: top;" class="LabelStyle">--%>




                                                                <%--   </td>
                                                                    </tr>
                                                                </table>--%>
                                                                <%--  </div>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="divDeal" style="height: 230px;" runat="server">
                                                    <table style="width: 100%; font-size: small; font-weight: normal;">
                                                        <tr>
                                                            <td style="width: 60%; vertical-align: top;">

                                                                <table id="Form2" cellspacing="2" cellpadding="2" runat="server" style="width: 100%; font-weight: normal; font-size: small; vertical-align: top;">
                                                                    <tr>
                                                                        <td style="width: 30%;" class="LabelStyle">Deal Name: 
                                                                        </td>
                                                                        <td style="width: 60%;">
                                                                            <asp:Label ID="lblDealName" runat="server" Text=""></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;">
                                                                            <asp:ImageButton ID="imgEditForm2" Width="15px" Height="15px" runat="server"
                                                                                ImageUrl="~/Images/Edit-Icon.gif" OnClick="imgEditForm2_Click" OnClientClick="javascript:return Navigate();" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top;">
                                                                        <td style="width: 40%; vertical-align: top;" class="LabelStyle">Location Address:
                                                                        </td>
                                                                        <td colspan="2" style="text-align: left; vertical-align: top;">
                                                                            <asp:Label ID="lblDealNames" runat="server" Text=""></asp:Label>
                                                                            <asp:Label ID="lblBillingAddress1Form2" runat="server" Text=""></asp:Label>
                                                                            <asp:Label ID="lblBillingAddress2Form2" runat="server" Text=""></asp:Label>
                                                                            <asp:Label ID="lblBillingCityForm2" runat="server" Text=""></asp:Label>
                                                                            <asp:Label ID="lblBillingStateForm2" runat="server" Text=""></asp:Label>
                                                                            <asp:Label ID="lblBillingZipForm2" runat="server" Text=""></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="vertical-align: top;">
                                                                        <td style="vertical-align: top;" valign="top" class="LabelStyle">Charge To:
                                                                        </td>
                                                                        <td colspan="2" style="text-align: left;">
                                                                            <%--  <asp:Label ID="lblChargeToName" runat="server" Text=""></asp:Label><br />--%>
                                                                            <asp:Label ID="lblChargeToCompany" runat="server" Text=""></asp:Label>
                                                                            <asp:Label ID="lblOwnerShipEntity" runat="server" Text=""></asp:Label>
                                                                            <asp:Label ID="lblChargeToAddress1" runat="server" Text=""></asp:Label>
                                                                            <asp:Label ID="lblChargeToAddress2" runat="server" Text=""></asp:Label>
                                                                            <asp:Label ID="lblChargeToCity" runat="server" Text=""></asp:Label>
                                                                            <asp:Label ID="lblChargeToState" runat="server" Text=""></asp:Label>
                                                                            <asp:Label ID="lblChargeToZip" runat="server" Text=""></asp:Label>
                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                            </td>
                                                            <td style="width: 40%; vertical-align: top;">
                                                                <table id="Form3" runat="server" cellspacing="2" cellpadding="2" style="width: 100%; height: 100%; font-weight: normal; vertical-align: top; font-size: small;">
                                                                    <tr>
                                                                        <td style="width: 40%; vertical-align: top;" class="LabelStyle">Contact: 
                                                                        </td>
                                                                        <td style="width: 50%; text-align: left;">
                                                                            <asp:Label ID="lblContactForm2" runat="server" Text=""></asp:Label><br />
                                                                            <asp:Label ID="lblCompanyEmail" runat="server" Text=""></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;"></td>
                                                                    </tr>
                                                                    <tr id="trDealUse" runat="server" visible="false">
                                                                        <td class="LabelStyle">Use:
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblUse" runat="server" Text=""></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;"></td>
                                                                    </tr>
                                                                    <tr id="trTenant" runat="server" visible="false">
                                                                        <td class="LabelStyle">Tenant:
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblTenant" runat="server" Text=""></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;"></td>
                                                                    </tr>
                                                                    <tr id="trBuyer" runat="server" visible="false">
                                                                        <td class="LabelStyle">Buyer:
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblBuyer" runat="server" Text=""></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="LabelStyle" style="vertical-align: top;">Due Date:
                                                                        </td>
                                                                        <td style="text-align: left;">
                                                                            <asp:Label ID="lblDueDate" runat="server" Text=""></asp:Label>
                                                                            <%--<table  id="tableDueDate" runat="server" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="lblDueDate1" runat="server" Text=""></asp:Label></td>
                                                                                    <td id="tdDueDate2" runat="server">
                                                                                        <asp:Label ID="lblDueDate2" runat="server" Text=""></asp:Label></td>
                                                                                </tr>
                                                                            </table>--%>

                                                                        </td>
                                                                        <td style="width: 10%;"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="LabelStyle">Amount Due:
                                                                        </td>
                                                                        <td style="text-align: left;">
                                                                            <asp:Label ID="lblAmountDue" runat="server" Text=""></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="LabelStyle">Outstanding:
                                                                        </td>
                                                                        <td style="text-align: left;">
                                                                            <asp:Label ID="lblOutstanding" runat="server" Text=""></asp:Label>
                                                                            <asp:HiddenField ID="hdnOutstanding" runat="server" Value="" />
                                                                            <%if (Session["UserType"] != null)
                                                                              {

                                                                                  if (Session["UserType"].ToString() == "1")
                                                                                  {
                                                                                      if (Session["IsVendor"] != null)
                                                                                      {
                                                                                          if (Session["IsVendor"].ToString() == "1")
                                                                                          {  %>
                                                                            <asp:LinkButton Style="text-decoration: none;" ID="lnkCreditAmount" runat="server" Text="Credit" OnClientClick="UpdateCreditAmount();return false;"></asp:LinkButton>

                                                                            <%}
                                                                                      }
                                                                                  }
                                                                              } %>
                                                                              
                                                                        </td>
                                                                        <td style="width: 10%;"></td>
                                                                    </tr>
                                                                    <tr id="trCreditAmount" runat="server">
                                                                        <td class="LabelStyle">Credit:
                                                                        </td>
                                                                        <td style="text-align: left;">
                                                                            <asp:Label ID="lblCreditAmount" runat="server" Text=""></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;"></td>
                                                                    </tr>
                                                                    <tr id="trOverPaymentAmount" runat="server">
                                                                        <td class="LabelStyle">Over Payment Amount:
                                                                        </td>
                                                                        <td style="text-align: left;">
                                                                            <asp:Label ID="lblOverPaymentAmount" runat="server" Text=""></asp:Label>
                                                                        </td>
                                                                        <td style="width: 10%;"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>

                                                            <td style="width: 100%;" colspan="2">
                                                                <table id="trNotes" width="100%" runat="server" style="width: 100%; font-size: small; font-weight: normal;">
                                                                    <tr>
                                                                        <%--<td id="tdNotes" runat="server" visible="false" style="width: 30%;  vertical-align: top;" valign="top">Notes:</td>--%>
                                                                        <td style="width: 100%; vertical-align: top;" align="left" valign="top">
                                                                            <%-- <div valign="top" style="word-wrap:normal;overflow-y: auto; vertical-align: top; padding-top: 0px; margin-top: 0px; height: 44px; width: 95%; min-width: 95%; border: solid 1px black; white-space:normal; vertical-align: top;" class="LabelStyle">


                                                                                <asp:Label ID="lblNotesForm2" runat="server" Text="" ></asp:Label>
                                                                            </div>--%>
                                                                            <div style="overflow-y: auto; height: 45px; word-wrap: normal; width: 98%; border: solid 1px black; vertical-align: top; min-width: 95%;" class="LabelStyle">

                                                                                <asp:Label ID="lblNotesForm2" runat="server" Text=""></asp:Label>
                                                                            </div>
                                                                        </td>

                                                                    </tr>
                                                                </table>




                                                            </td>

                                                        </tr>
                                                    </table>
                                                    <%--     <table style="width:100%;">
                                                        <tr>
                                                          
                                                                    <td  style="width: 100%;">
                                                                        <table id="trNotes" width="100%" runat="server">
                                                                            <tr>
                                                                                <td id="tdNotes" runat ="server" visible="false" style="vertical-align:top;" valign="top">Notes</td>
                                                                                <td style="width: 100%;">
                                                                                    <div style="overflow-y: auto; height: 44px; width: 95%; min-width: 95%; border: solid 1px black; word-break: break-all; vertical-align: top;" class="LabelStyle">


                                                                                        <asp:Label ID="lblNotesForm2" runat="server" Text="Notes" Width="90%"></asp:Label>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>




                                                                    </td>
                                                             
                                                        </tr>
                                                    </table>--%>
                                                </div>

                                                <div id="divVendor" style="height: 195px;" runat="server">
                                                    <%--   <table>
                                                        <tr>
                                                            <td width="500px">--%>
                                                    <table id="Form5" runat="server" style="width: 100%; font-weight: normal; font-size: small;">
                                                        <tr>
                                                            <%-- <td colspan="2">
                                                                            <table style="width:100%;">
                                                                                <tr>--%>
                                                            <td class="LabelStyle" style="width: 120px;">Company: 
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCompany" runat="server" Text=""></asp:Label>

                                                            </td>
                                                            <td colspan="2">
                                                                <div>
                                                                    <asp:ImageButton ID="imgVendorEdit" Width="15px" Height="15px" runat="server"
                                                                        ImageUrl="~/Images/Edit-Icon.gif" OnClick="imgVendorEdit_Click" />
                                                                    <asp:Button ID="btnPostSplitPaid" runat="server" Text="Post Split Paid" Style="margin-left: 20px;" OnClick="btnPostSplitPaid_Click" CssClass="SqureButton" />
                                                                    <asp:Button ID="btnViewBrokerSnapshotDetails" runat="server" Text="View Snapshot Detail" Style="margin-left: 20px;" CssClass="SqureButton" OnClick="btnViewBrokerSnapshotDetails_Click" />
                                                                </div>
                                                            </td>
                                                            <%--        </tr>
                                                                            </table>
                                                                        </td>--%>
                                                        </tr>
                                                        <tr>
                                                            <td class="LabelStyle" style="width: 120px;">Name:
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td class="LabelStyle">
                                                                <asp:Label ID="lblYTD" runat="server" Text="YTD Collected:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblYTDCollected" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="LabelStyle" style="width: 120px;">Email:
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblEmails" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td class="LabelStyle">
                                                                <asp:Label ID="lblSplit" runat="server" Text="Current Split:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCurrentSplit" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="vertical-align: top; width: 120px;" class="LabelStyle">Address:
                                                            </td>
                                                            <td style="text-align: left;">
                                                                <asp:Label ID="lblAddress1" runat="server" Text=""></asp:Label>
                                                                <asp:Label ID="lblAddress2" runat="server" Text=""></asp:Label>
                                                                <asp:Label ID="lblCity" runat="server" Text=""></asp:Label>
                                                                <asp:Label ID="lblState" runat="server" Text=""></asp:Label>
                                                                <asp:Label ID="lblZipCode" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td class="LabelStyle">
                                                                <asp:Label ID="lblRemaining" runat="server" Text="Remaining Needed till Next Tier:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRemainingNeeded" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <%--<table style="width: 100%; height: 100%; border: solid 1px black; vertical-align: top;">
                                                                                <tr>
                                                                                    <td style="vertical-align: top;" class="LabelStyle">
                                                                                        <asp:Label ID="lblVendorNotes" runat="server" Text="Notes"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>--%>

                                                                <div style="overflow-y: auto; height: 80px; word-wrap: normal; border: solid 1px black; vertical-align: top;" class="LabelStyle">
                                                                    <asp:Label ID="lblVendorNotes" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <ajax:ModalPopupExtender ID="modalEdit" runat="server"
                                                        PopupControlID="divpopup" BackgroundCssClass="modalBackground" TargetControlID="lnbPopUp1" CancelControlID="imgbtnClose">
                                                    </ajax:ModalPopupExtender>
                                                    <asp:LinkButton ID="lnbPopUp1" runat="server"></asp:LinkButton>
                                                    <%--</td>
                                                            <td width="50%" style="vertical-align: top;">--%>

                                                    <%--        <table>
                                                                    <tr>

                                                                        <td width="10%" style ="vertical-align :top;">

                                                                           
                                                                        </td>
                                                                    </tr>
                                                                    <tr><td></td></tr>
                                                                    <tr><td></td></tr>
                                                                    <tr><td></td></tr>
                                                                    <tr><td></td></tr>
                                                 <%--               </table>--%>
                                                    <%--       </td>
                                                        </tr>
                                                    </table>--%>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div id="divDealCustgv" runat="server" style="border: solid 1px black; height: 400px; width: 100%; margin: 5px 0px 5px 5px;">
                                    <table width="100%">
                                        <tr bgcolor="#A4A4A4" style="font-weight: bold; color: Black; font-size: small; height: 25px;">
                                            <td width="10%">View
                                            </td>
                                            <td width="20%">
                                                <asp:DropDownList ID="ddlViewDealGV" CssClass="ddl" runat="server">
                                                    <asp:ListItem Value="1">ALL</asp:ListItem>
                                                    <asp:ListItem Value="2">CURRENT YEAR</asp:ListItem>
                                                    <asp:ListItem Value="3">UNPAID</asp:ListItem>
                                                    <%--Modified by Shishir 05-05-2016--%>
                                                    <asp:ListItem Value="4">PAID</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>Date&nbsp&nbsp;&nbsp;
                                    <asp:TextBox ID="txtStartDate" Width="60px"
                                        Style="height: 12px; font-size: 8px;" runat="server"></asp:TextBox>
                                                &nbsp;&nbsp;To&nbsp;&nbsp;
                                    <asp:TextBox ID="txtEndDate" Width="60px" Style="height: 12px; font-size: 8px;" runat="server"></asp:TextBox>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnDisplay" CssClass="SqureButton"
                                        runat="server" Text="DISPLAY" OnClick="btnDisplay_Click" BackColor="Gray" OnClientClick="return ValidateDate();" />

                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnClear" BackColor="Gray" CssClass="SqureButton"
                                        runat="server" Text="CLEAR" OnClick="btnClear_Click" OnClientClick="ClearControl()" />
                                                <ajax:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtStartDate"></ajax:CalendarExtender>
                                                <ajax:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtEndDate"></ajax:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <div id="DivRoot" align="left">
                                                    <div style="overflow: hidden;" id="DivHeaderRow">
                                                    </div>

                                                    <div style="overflow: auto;" onscroll="OnScrollDiv(this)" id="DivMainContent">
                                                        <%-- <div style="overflow-y: scroll; height: 368px;">--%>
                                                        <asp:GridView ID="gvDealInfo" ShowFooter="True" Font-Size="small" Width="100%"
                                                            CellPadding="0" CellSpacing="0" OnSorting="gvDealInfo_Sorting" AllowSorting="true" runat="server" AutoGenerateColumns="False"
                                                            OnRowDataBound="gvDealInfo_RowDataBound" OnRowCommand="gvDealInfo_RowCommand">
                                                            <%--Height="15px"--%>
                                                            <Columns>
                                                                <asp:BoundField SortExpression="DealName" DataField="DealName" HeaderText="DEAL NAME" ItemStyle-HorizontalAlign="left" FooterText="Total" FooterStyle-HorizontalAlign="Left" />
                                                                <%-- <asp:BoundField SortExpression="DealType" DataField="DealType" HeaderText="DEAL TYPE" ItemStyle-HorizontalAlign="Center" />--%>
                                                                <asp:BoundField DataFormatString="{0:d}" SortExpression="DueDates" DataField="DueDate" HeaderText="DUE DATE" ItemStyle-HorizontalAlign="Center" FooterStyle-Width="80px" />
                                                                <asp:TemplateField SortExpression="Gross" HeaderText="GROSS" ItemStyle-HorizontalAlign="right">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hdnCreatedBy" runat="server" Value='<%#Eval("CreatedBy") %>' />
                                                                        $<asp:Label ID="lblGross" runat="server" Text='<%# String.Format("{0:n2}",Eval("Gross"))%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lbltotalGross" runat="server" Text=""></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField SortExpression="PaidDates" DataField="PaidDate" HeaderText="PAID DATE" ItemStyle-HorizontalAlign="Center" />
                                                                <asp:TemplateField SortExpression="PaidAmount" HeaderText="PAID AMT" ItemStyle-HorizontalAlign="Right">
                                                                    <ItemTemplate>
                                                                        $<asp:Label ID="lblPaidAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("PaidAmount"))%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblFooPaidAmount" runat="server" Text=""></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton runat="server" ToolTip="View the Payment History" ID="imgPaysHistory" CommandName="PayHistory1" CommandArgument='<%#Eval("ChargeSlipID") %>' Height="10px" Width="10px" ImageUrl="~/Images/Attach-Icon.gif" />

                                                                        <%-- <asp:ImageButton ID="imgPaymentHistory" CommandName="PayHistory1" CommandArgument='<%#Eval("ChargeSlipID") %>' Height="10px" Width="10px" ImageUrl="~/Images/Attach-Icon.gif"   runat="server" OnClientClick ="javascript:alert('hi');" />--%>
                                                                    </ItemTemplate>
                                                                    <ControlStyle Width="10px" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField SortExpression="Unapplied" HeaderText="UNAPPLIED" ItemStyle-HorizontalAlign="right">
                                                                    <ItemTemplate>
                                                                        $<asp:Label ID="lblUnapplied" runat="server" Text='<%# String.Format("{0:n2}",Eval("Unapplied"))%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lbltotalUnapplied" runat="server" Text=""></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField SortExpression="[My Share]" HeaderText="MY SHARE <br/> ESTIMATED" ItemStyle-Width="110px" ItemStyle-HorizontalAlign="right">
                                                                    <ItemTemplate>
                                                                        $<asp:Label ID="lblShare" runat="server" Text='<%# String.Format("{0:n2}",Eval("CompanyEstimatedShare"))%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lbltotalShare" runat="server" Text=""></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField SortExpression="MyShareActual" HeaderText="MY SHARE <br/> ACTUAL" ItemStyle-Width="110px" ItemStyle-HorizontalAlign="right">
                                                                    <ItemTemplate>
                                                                        $<asp:Label ID="lblMyShareActual" runat="server" Text='<%# String.Format("{0:n2}",Eval("CompanyActualShare"))%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblMyShareTotalShare" runat="server" Text=""></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgView" CommandName="View" ToolTip="View the Charge Detail" CommandArgument='<%#Eval("ChargeSlipID") %>' Height="12px" Width="12px" ImageUrl="~/Images/Info-Card-Icon.gif" runat="server" />
                                                                        <asp:Label ID="lblDealTransactionID" runat="server" Visible="false" Text='<%#Eval("DealTransactionID") %>'></asp:Label>
                                                                        <asp:Label ID="lblCommissionDueDateId" runat="server" Visible="false" Text='<%#Eval("CommissionDueId") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <%--<ControlStyle Width="15px" />--%>
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgPayment" ToolTip="Apply the Payment" OnClientClick=<%#"javascript:PaymentLink('"+Eval("ChargeSlipID")+"')" %> Height="12px" Width="12px" ImageUrl="~/Images/money.png" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--OnClientClick='<%# Eval("this.ClientID","return ShowPopUp({0})") %>'--%> <%--'<%# String.Format("confirm_ticket({0},{1});return false;",Eval("idAgir"), Eval("idAgir"))%> '--%>
                                                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgDelete" ToolTip="Delete the Chargeslip" OnClientClick=<%# "javascript:ShowPopUp('" + Eval("DealName").ToString().Replace("'", "\\'") +"','"+Eval("ChargeSlipID")+ "')" %> CommandArgument='<%#Eval("ChargeSlipID") %>' Height="12px" Width="12px" ImageUrl="~/Images/delete.gif" runat="server" />
                                                                    </ItemTemplate>
                                                                    <%--<ControlStyle Width="15px" />--%>
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <%-- //Add for Payment Void on 15-07-2016--%>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton AlternateText="Void the Payment" ToolTip="Void the Payment" Style="vertical-align: middle;" ID="imgVoidPayment" OnClientClick=<%#"javascript:ClearPaymentDetail('"+Eval("ChargeSlipID")+"','"+String.Format("{0:n2}",Eval("Gross"))+"')"%> Height="15px" Width="15px" ImageUrl="~/Images/Cancelpayment.png" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <%--<AlternatingRowStyle CssClass="AlternatingRowGridView2" />'<%# Eval("DealName","return ShowPopUp({0})") %>'
                                                        <HeaderStyle CssClass="HeaderGridView2" />
                                                        <RowStyle CssClass="RowGridView2" />
                                                        <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                                        <FooterStyle CssClass="FooterGridView2" />--%>


                                                            <%--<AlternatingRowStyle  Height="10px" BackColor="#F5F6CE" />
                                           <HeaderStyle CssClass="HeaderGridView2" />
                                            <RowStyle Height="16px" Font-Size="Small" />
                                            <SelectedRowStyle BackColor="#F7BE81" />
                                            <FooterStyle BackColor="#D8D8D8" Height="25px" ForeColor="Black" Font-Bold="true" />
                                                         <FooterStyle CssClass="FooterGridView2" />--%>
                                                            <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                                            <HeaderStyle CssClass="HeaderGridView2" Height="40px" Font-Size="10px" />
                                                            <RowStyle CssClass="RowGridView2" />
                                                            <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                                            <FooterStyle CssClass="FooterGridView2" />
                                                        </asp:GridView>
                                                        <asp:HiddenField ID="hdnDealTransactionID" runat="server" />
                                                        <asp:HiddenField ID="hdnCommissionDueDateId" runat="server" />
                                                        <asp:HiddenField ID="hdnDealRowID" runat="server" />
                                                        <asp:Button ID="btnFillDealForm" runat="server" OnClick="btnFillDealForm_Click" Style="display: none;" />
                                                        <asp:Button ID="btnFillGVDealInfo" runat="server" OnClick="btnFillGVDealInfo_Click" Style="display: none;" />
                                                        <asp:Button ID="btnVoidPayment" runat="server" OnClick="btnVoidPayment_Click" Style="display: none;" />
                                                        <asp:Button ID="btnFillDealFormGVDealInfo" runat="server" OnClick="btnFillDealFormGVDealInfo_Click" Style="display: none;" />
                                                        <%--</div>--%>
                                                    </div>

                                                    <div id="DivFooterRow" style="overflow: hidden">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="divTransgv" runat="server" style="border: solid 1px black; margin: 5px; width: 100%;">
                                    <table width="100%">
                                        <tr bgcolor="#A4A4A4" style="font-weight: bold; color: Black; font-size: small; height: 25px;">
                                            <td width="10%">View
                                            </td>
                                            <td width="20%">
                                                <asp:DropDownList ID="ddlTransaction" CssClass="ddl" runat="server">
                                                    <asp:ListItem Value="1">ALL</asp:ListItem>
                                                    <asp:ListItem Value="2">CURRENT YEAR</asp:ListItem>
                                                    <asp:ListItem Value="3">UNPAID</asp:ListItem>
                                                    <asp:ListItem Value="4">PAID</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>Date&nbsp&nbsp;&nbsp;
                                    <asp:TextBox ID="txtTransForm" Width="60px" Style="height: 10px; font-size: 8px;" runat="server"></asp:TextBox>
                                                &nbsp;&nbsp;To&nbsp;&nbsp;
                                    <asp:TextBox ID="txtTransTo" Width="60px" Style="height: 10px; font-size: 8px;" runat="server"></asp:TextBox>
                                                <ajax:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtTransForm"></ajax:CalendarExtender>
                                                <ajax:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtTransTo"></ajax:CalendarExtender>
                                                <asp:Button ID="btnTransDisplay" Text="DISPLAY" CssClass="SqureButton" BackColor="Gray" runat="server" OnClick="btnTransDisplay_Click" OnClientClick="return ValidateDateTran();" />
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnTranClear" CssClass="SqureButton" BackColor="Gray" runat="server" Text="CLEAR" OnClientClick="ClearControlTran()" OnClick="btnTranClear_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <%--<div style="overflow-y: scroll; height: 560px; width: 100%;">--%>
                                                <asp:GridView ID="gvTransInfo" ShowFooter="true" Font-Size="small" Width="100%" runat="server" AutoGenerateColumns="False"
                                                    AllowSorting="true" OnSorting="gvTransInfo_Sorting" OnRowDataBound="gvTransInfo_RowDataBound"
                                                    OnRowCommand="gvTransInfo_RowCommand">
                                                    <Columns>
                                                        <asp:BoundField SortExpression="UserName" DataField="UserName" HeaderText="VENDORS" ItemStyle-HorizontalAlign="left" FooterText="Total" FooterStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField SortExpression="Company" DataField="Company" HeaderText="CUSTOMERS" ItemStyle-HorizontalAlign="left" FooterText="Total" FooterStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField SortExpression="DealName" DataField="DealName" HeaderText="DEAL NAME" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField SortExpression="DueDates" DataField="DueDate" HeaderText="DUE DATE" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField SortExpression="Gross" HeaderText="Gross" ItemStyle-HorizontalAlign="Right">
                                                            <ItemTemplate>
                                                                $<asp:Label ID="lblGross" runat="server" Text='<%# String.Format("{0:n2}",Eval("Gross"))%>'></asp:Label>
                                                                <asp:HiddenField ID="hdnTotalGrossAmount" runat="server" Value='<%# String.Format("{0:n2}",Eval("TotalGrossAmount"))%>' />
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="lbltotalGross" runat="server" Text=""></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField SortExpression="PaidDates" DataField="PaidDate" HeaderText="PAID DATE" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField SortExpression="VendorShareEstimated" HeaderText="Vendor Share Estimated" ItemStyle-HorizontalAlign="Right">
                                                            <ItemTemplate>
                                                                $<asp:Label ID="lblVendorShareEstimated" runat="server" Text='<%# String.Format("{0:n2}",Eval("VendorEstimatedShare"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="lblFooVendorShareEstimated" runat="server" Text=""></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField SortExpression="PaidAmount" HeaderText="PAID AMT" ItemStyle-HorizontalAlign="Right">
                                                            <ItemTemplate>
                                                                $<asp:Label ID="lblPaidAmounts" runat="server" Text='<%# String.Format("{0:n2}",Eval("PaidAmount"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="lblFooPaidAmount" runat="server" Text=""></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ControlStyle-Width="10px">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgPaymentHistory" ToolTip="View the Payment History" CommandName="PayHistory" CommandArgument='<%#Eval("ChargeSlipID") %>' Height="10px" Width="10px" ImageUrl="~/Images/Attach-Icon.gif" runat="server" />
                                                            </ItemTemplate>
                                                            <ControlStyle Width="10px" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField SortExpression="UnApplied" HeaderText="UNAPPLIED" ItemStyle-HorizontalAlign="Right">
                                                            <ItemTemplate>
                                                                $<asp:Label ID="lblUnapplied" runat="server" Text='<%# String.Format("{0:n2}",Eval("UnApplied"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="lbltotalUnapplied" runat="server" Text=""></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField SortExpression="MY SHARE" HeaderText="MY SHARE <br/> ESTIMATED" ItemStyle-HorizontalAlign="Right">
                                                            <ItemTemplate>
                                                                $<asp:Label ID="lblShare" runat="server" Text='<%# String.Format("{0:n2}",Eval("CompanyEstimatedShare"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="lbltotalShare" runat="server" Text=""></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField SortExpression="MyShareActual" HeaderText="MY SHARE <br/> ACTUAL" ItemStyle-HorizontalAlign="right">
                                                            <ItemTemplate>
                                                                $<asp:Label ID="lblMyShareActual" runat="server" Text='<%# String.Format("{0:n2}",Eval("CompanyActualShare"))%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <asp:Label ID="lblMyShareTotalShare" runat="server" Text=""></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ControlStyle-Width="10px">
                                                            <ItemTemplate>
                                                                <%--  <asp:ImageButton ID="imgView" Height="10px" Width="10px" ImageUrl="~/Images/Info-Card-Icon.gif" runat="server" />--%>
                                                                <asp:ImageButton ID="imgView" ToolTip="View the Charge Detail" CommandName="View" CommandArgument='<%#Eval("ChargeSlipID") %>' Height="15px" Width="15px" ImageUrl="~/Images/Info-Card-Icon.gif" runat="server" />
                                                                <asp:Label ID="lblDealTransactionID" runat="server" Visible="false" Text='<%#Eval("DealTransactionID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgPayment" ToolTip="Apply the Payment" OnClientClick=<%#"javascript:PaymentLink('"+Eval("ChargeSlipID")+"')" %> Height="15px" Width="15px" ImageUrl="~/Images/money.png" runat="server" />
                                                                <%-- CommandName="Payment" CommandArgument='<%#Eval("ChargeSlipID") %>'--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                                    <HeaderStyle CssClass="HeaderGridView2" />
                                                    <RowStyle CssClass="RowGridView2" />
                                                    <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                                    <FooterStyle CssClass="FooterGridView2" />
                                                </asp:GridView>
                                                <%--   </div>--%>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>

<%--Added by Shishir 13-06-2016 Starts--%>

<ajax:ModalPopupExtender ID="ModalShowPostSplitPaid" runat="server" PopupControlID="divPostSplitPaid" TargetControlID="btnShow" CancelControlID="btnClose" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>
<asp:Button runat="server" Style="display: none" ID="btnShow" Text="duummy" />
<div id="divPostSplitPaid" class="PopupDivBodyStyle" style="height: auto;">

    <table width="100%" cellpadding="0" cellspacing="0">
        <tr id="divheader" style="background-color: gray; height: 35px;">
            <td style="border-bottom: solid 2px black;">&nbsp;&nbsp;<asp:Label ID="lblCHARGEDETAIL" Style="vertical-align: bottom; font-size: medium; font-weight: bold; color: Black;"
                runat="server" Text="POST SPLIT PAID"></asp:Label>
            </td>
            <td width="30px" style="text-align: center; border-bottom: solid 2px black;">
                <asp:ImageButton ID="btnClose" src="../Images/close.png" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">

                <asp:UpdatePanel ID="updPostSplitPaid" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td style="width: 100%;">
                                    <div style="overflow: auto; max-width: 750px; max-height: 650px; min-width: 500px; min-height: 450px; margin-left: 20px; margin-right: 20px;">
                                        <asp:GridView ID="grdPayment" runat="server" AutoGenerateColumns="false" ShowFooter="True" Font-Size="12px" font-family="Verdana" Width="95%" OnRowDataBound="grdPayment_RowDataBound">
                                            <%--OnRowCommand="grdPayment_RowCommand"--%>
                                            <Columns>
                                                <asp:BoundField DataField="ChargeSlipId" HeaderText="ChargeSlipId" Visible="false" />
                                                <asp:BoundField DataField="CompanyName" HeaderText="Charge To" ItemStyle-Width="120px" />
                                                <asp:BoundField DataField="Broker" HeaderText="Broker" />
                                                <asp:BoundField DataFormatString="{0:d}" DataField="PaymentDate" HeaderText="Payment Date" ItemStyle-HorizontalAlign="Center" FooterStyle-Width="80px" />

                                                <asp:TemplateField HeaderText="Paid Amount" ItemStyle-HorizontalAlign="Right">

                                                    <ItemTemplate>
                                                        $<asp:Label ID="lblPaidAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("GrossPaid"))%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblFooPaidAmount" runat="server" Text=""></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="SplitPercent1" HeaderText="Broker Split%" ItemStyle-HorizontalAlign="Center" />
                                                <asp:TemplateField HeaderText="Net Paid" ItemStyle-HorizontalAlign="Right">

                                                    <ItemTemplate>
                                                        $<asp:Label ID="lblNetPaid" runat="server" Text='<%# String.Format("{0:n2}",Eval("NetPaid"))%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblFooNetPaid" runat="server" Text=""></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="BrokerCheckNo" HeaderText="Broker Check No." ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField DataField="CompanyCheckNo" HeaderText="Company Check No." ItemStyle-HorizontalAlign="Center" />
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <b style="padding: 2px 5px  2px 25px; color: #000000; background-color: #FFFFFF; height: 26px; vertical-align: middle; cursor: pointer;">No records found.</b>
                                            </EmptyDataTemplate>
                                            <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                            <HeaderStyle CssClass="HeaderGridView2" />
                                            <RowStyle CssClass="RowGridView2" />
                                            <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                            <FooterStyle CssClass="FooterGridView2" />
                                        </asp:GridView>
                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td style="text-align: right;">
                                    <asp:Button ID="btnExportToExcel" runat="server" Text="Export To Excel" CssClass="SqureButton" OnClick="btnExportToExcel_Click" />
                                    <asp:Button ID="btnExporttoPDF" runat="server" Text="Export To PDF" CssClass="SqureButton" OnClick="btnExporttoPDF_Click" />
                                </td>
                            </tr>
                        </table>


                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>

            <td align="right" style="padding-right: 33px;" colspan="2"></td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
    </table>

</div>
<%--Ends Here--%>
<%--Added by Jaywanti on 18-11-2016--%>
<ajax:ModalPopupExtender ID="ModalShowBrokerSnapshotDetail" runat="server" PopupControlID="divBrokerSnapshotDetail" BackgroundCssClass="modalBackground" TargetControlID="btnShowSummary"
    CancelControlID="btnCloseSummary">
</ajax:ModalPopupExtender>
<asp:Button runat="server" Style="display: none" ID="btnShowSummary" Text="duummy" />
<div id="divBrokerSnapshotDetail" class="PopupDivBodyStyle" style="width: 550px; height: auto;">

    <table width="100%" cellpadding="0" cellspacing="0">
        <tr id="Tr1" style="background-color: gray; height: 35px;">
            <td style="border-bottom: solid 2px black;">&nbsp;&nbsp;<asp:Label ID="Label1" Style="vertical-align: bottom; font-size: medium; font-weight: bold; color: Black;"
                runat="server" Text="SNAPSHOT DETAIL SUMMARY"></asp:Label>
            </td>
            <td width="30px" style="text-align: center; border-bottom: solid 2px black;">
                <asp:ImageButton ID="btnCloseSummary" src="../Images/close.png" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatepanelBrokerSummaryDetail" runat="server">
                    <ContentTemplate>

                        <table>
                            <tr>
                                <td class="Paddingtd">
                                    <asp:Label ID="labelPreSplitRecCur" runat="server" Text="Pre Split Receivables:" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPreSplitRecCur" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td class="Paddingtd">
                                    <asp:Label ID="labelPostSplitRecCur" runat="server" Text="Post Split Receivables:" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPostSplitRecCur" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td class="Paddingtd">
                                    <asp:Label ID="labelPreSplitRecNex" runat="server" Text="Pre Split Receivables:" Font-Bold="true"> </asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPreSplitRecNex" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td class="Paddingtd">
                                    <asp:Label ID="labelPreSplitPaid" runat="server" Text="Pre Split Paid:" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPreSplitPaid" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td class="Paddingtd">
                                    <asp:Label ID="labelPostSplitPaid" runat="server" Text="Post Split Paid:" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPostSplitPaid" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td class="Paddingtd">
                                    <asp:Label ID="labelPreSplitTotalRec" runat="server" Text="Pre Split Total Receivables:" Font-Bold="true"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblPreSplitTotalRec" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td class="Paddingtd">
                                    <asp:Label ID="labelTotalPreSplitOptConCur" runat="server" Text="Total Pre Split Option & Contingent Receivables:" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblTotalPreSplitOptConCur" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td class="Paddingtd">
                                    <asp:Label ID="labelTotalPreSplitOptConNext" runat="server" Text="Total Pre Split Option & Contingent Receivables:" Font-Bold="true"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblTotalPreSplitOptConNext" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

        <tr>
            <td>
                <br />
            </td>
        </tr>
    </table>

</div>
<%--End here--%>
<div id="divNwCenterOrCompany" class="PopupDivBodyStyle" style="height: auto; display: none;">
    <table style="width: 100%" class="spacing">
        <tr style="background-color: gray; border-bottom: solid;">
            <td style="text-align: left;">
                <asp:UpdatePanel ID="upTitile" runat="server">
                    <ContentTemplate>
                        <h3>&nbsp; &nbsp;      
                                <asp:Label ID="lbTitle" runat="server"></asp:Label></h3>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td align="right">
                <asp:ImageButton ImageUrl="~/Images/close.png" ID="btnCancel" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc:AddNewShoppingCenterOrCompany ID="ucNewCenterOrCompany" runat="server" />

            </td>
        </tr>
    </table>

</div>
<asp:HiddenField ID="hdnChargeSlip" runat="server" />
<%--<asp:UpdatePanel ID="UpdatePanel6" runat="server">
    <ContentTemplate>
     
    </ContentTemplate>
</asp:UpdatePanel> --%>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnPopup" runat="server" />
        <div id="divPaymentHistoryDetail" runat="server" style="display: none;">
            <uc1:ucPaymentHistoryDetail ID="ucPaymentHistoryDetail1" runat="server" />
        </div>
        <div id="divChargeDetailCard" runat="server" style="display: none;">
            <uc1:ucChargeDetailCard ID="ucChargeDetailCard1" runat="server" />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<ajax:ModalPopupExtender ID="mpeAddNewComapny" TargetControlID="lbDummy" CancelControlID="btnCancel"
    PopupControlID="divNwCenterOrCompany" runat="server" BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Label ID="lbDummy" runat="server"></asp:Label>
<asp:UpdatePanel ID="updatepanel6" runat="server">
    <ContentTemplate>
        <div id="divpopup" class="PopupDivBodyStyle" runat="server" style="width: 450px; height: 550px; overflow-y: auto; background-color: white; z-index: 111; position: absolute; display: none;">
            <table style="width: 100%" class="spacing">
                <tr style="background-color: gray; border-bottom: solid;">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="text-align: left; width: 490px;">
                                    <asp:Label ID="lblHeader" runat="server" Text="VENDOR DETAILS" ForeColor="Black" Style="padding-left: 10px; vertical-align: middle; font-weight: bold;"></asp:Label>
                                </td>
                                <td style="text-align: right;">
                                    <asp:ImageButton ID="imgbtnClose" runat="server" ImageUrl="~/Images/Close.png" AlternateText="Close"
                                        ForeColor="Red" ImageAlign="Right" Style="margin-top: 4px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="Paddingtd">
                        <asp:Label ID="lblErrorMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%" class="Paddingtd">COMPANY
                    </td>
                    <td>
                        <asp:TextBox ID="txtCompany" runat="server" Width="250px" Height="18px"></asp:TextBox>
                        <ajax:AutoCompleteExtender ID="acBrokerInfo" runat="server"
                            TargetControlID="txtCompany"
                            DelimiterCharacters=";, :"
                            MinimumPrefixLength="1"
                            EnableCaching="true"
                            CompletionSetCount="10"
                            CompletionInterval="300"
                            ServiceMethod="SearchGetComapnyList"
                            OnClientItemSelected="OnCenterSelected"
                            OnClientPopulating="ShowIcon"
                            OnClientPopulated="hideIcon"
                            ShowOnlyCurrentWordInCompletionListItem="true"
                            CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList"
                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                            CompletionListElementID="divwidth" />
                        <asp:HiddenField ID="hdnBrokerId" runat="server" Value="" />
                        <%-- <asp:HiddenField ID="hdnDealBrokerCommissionId" runat="server" Value="" />--%>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%" class="Paddingtd">ADDRESS
                    </td>
                    <td>
                        <asp:TextBox ID="txtAddress" runat="server" Width="250px" Height="18px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%" class="Paddingtd">CITY
                    </td>
                    <td>
                        <asp:TextBox ID="txtCity" runat="server" Width="170px" Height="18px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%" class="Paddingtd">STATE
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlState" runat="server" Width="170px" Font-Size="Small"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%" class="Paddingtd">ZIPCODE
                    </td>
                    <td>
                        <asp:TextBox ID="txtZipCode" CssClass="onlyNumbers" runat="server" Width="170px" Height="18px" onkeypress="return ValidateNumber(event);"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%" class="Paddingtd">NAME
                    </td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" Width="250px" Height="18px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <%-- Onblur= "javascript:EmailValidation();"--%>
                    <td style="width: 25%" class="Paddingtd">EMAIL
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" Width="250px" Height="18px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%; vertical-align: top" class="Paddingtd">EIN
                    </td>
                    <td>
                        <asp:TextBox ID="txtEin" runat="server" Width="170px" Height="18px"></asp:TextBox>



                    </td>
                </tr>

                <tr>
                    <td style="width: 25%" class="Paddingtd">SHARE (%)
                    </td>
                    <td>
                        <asp:TextBox ID="txtShare" runat="server" Width="100px" CssClass="onlyNumbers" Height="18px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%" class="Paddingtd">SET AMOUNT
                    </td>
                    <td>
                        <asp:TextBox ID="txtSetAmount" runat="server" Width="100px" Height="18px" CssClass="DollarTextBoxWithBorder"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Label ID="lblNotesModifiedby" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%" class="Paddingtd" valign="top">NOTES</td>
                    <td>
                        <asp:TextBox ID="txtUserNOTES" runat="server" TextMode="MultiLine" Rows="12" Columns="35"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%"></td>
                    <td style="text-align: right; padding-right: 30px;">
                        <asp:Button ID="btnAddBroker" runat="server" Text="OK" CssClass="SqureButton" Width="100px" OnClick="btnAddBroker_Click" OnClientClick="return ValidateControl();" />
                        <%-- <button id="b" onclick="btnAddExternalBroker_Save();return false;" class="btnAddBrokertype">Add Broker</button> OnClientClick="btnAddExternalBroker_Save();return true;"--%> 
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div id="dvChargeSlipDelete" title="Delete Chargeslip" class="popup" runat="server" style="display: none">
    <table style="width: 100%" cellpadding="0">
        <%--   <tr style="background-color: gray;height: 15px;">
            <td style=" border-bottom:solid 2px black; width:100%; " colsapn="2"  >
                &nbsp;&nbsp;<asp:Label ID="lblheading" Style="vertical-align: bottom; font-size: small;
                    font-weight: bold; color: Black;" runat="server" Text=" DELETE CHARGESLIP"></asp:Label>
           <%-- </td>
            <td style="text-align: right; width:20%; border-bottom:solid 2px black;">--%>
        <%--    <asp:ImageButton ID="imgClose" src="../Images/close.png" ImageAlign="Right" runat="server" 
                     OnClientClick ="return closepopup();" />
            </td>
        </tr>--%>
        <tr style="height: 25px">
            <td colspan="2" style="background-color: grey; vertical-align: bottom; font-weight: bold; font-size: medium;">
                <asp:ImageButton ID="imgClose" src="../Images/close.png" ImageAlign="Right" runat="server" OnClientClick="return closepopup();" />
                DELETE CHARGESLIP
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width: 40%">
                <asp:Label runat="server" ID="lblDelerror" ForeColor="Red" Font-Bold="true" Font-Names="Vardena" Font-Size="Small" Text=""></asp:Label>
            </td>
        </tr>

        <tr>
            <td>Deal Name:</td>
            <td>
                <asp:Label ID="lblDeleteChargeslipDealName" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>Reason to Delete:</td>
            <td>
                <asp:TextBox TextMode="MultiLine" ID="txtDeleteCharslipReason" runat="server" Rows="8" Columns="45"></asp:TextBox></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="btnDeleteOK" Text="OK" CssClass="SqureButton" runat="server" OnClientClick="javascript:return checkDeleteReason();" OnClick="btnDeleteOK_Click" />
                <asp:HiddenField ID="hdnChargeSlipidDel" runat="server" />
            </td>
    </table>
</div>

