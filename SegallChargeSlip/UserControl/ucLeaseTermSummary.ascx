﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucLeaseTermSummary.ascx.cs"
    Inherits="UserControl_ucLeaseTermSummary" EnableViewState="true" %>


<style type="text/css">
    .tblfixayout {
        width: 100%;
        color: black;
       /* table-layout: initial; */

        table-layout: fixed;
        padding: 0px 0px 0px 0px;
        border-spacing: 0;
        text-align: center;
        /*cellspacing:0px;
        cellpadding:opx;*/
    }

    /*.tblfixayout tr td {
         height:8px;
        }*/
    .tbOnverFlow {
        overflow: hidden;
        width: 50px;
        padding: 0px 0px 0px 0px;
    }

    .tbNormal {
        overflow: hidden;
        width: 40%;
        padding: 0px 0px 0px 0px;
        white-space: nowrap;
        text-align: left;
    }

    .tdmain {
        /*width: 120px;*/
        width: 40%;
        text-align: left;
        /*text-wrap: initial;*/
    }

    .tdspceing {
        /*width: 20px;*/
        width: 8%;
        text-align: right;
    }

    .tdNuneric {
        /*width: 60px;*/
        width: 22%;
        text-align: right;
    }

    .tdalignright {
        text-align: right;
        /*.auto-style1 {
            width: 89%;
        }

        .tdBorderBottom {
            border-bottom: solid 1px black;
        }

        .tdBorderLeft {
            border-left: solid 1px black;
        }

        .tdBorderRight {
            border-right: solid 1px black;
        }

        .tdBorderTop {
            border-top: solid 1px black;*/
    }
</style>


 
<%--style="font-size: small; font: bold;"--%>
<table style="width: 100%; font-size: smaller;" class="ucbackgroundborder">
    <tr>
        <td class="ucheadings">SUMMARY
            <hr style="width: 100%" />
        </td>
    </tr>
    <tr>
        <td>
            <%--<td style="width: 93%; border: 1px solid black;">--%>
            <asp:UpdatePanel ID="upMain" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="dvMainSumm" style="overflow-y: scroll; margin: 0px 10px 0px 10px; background-color: white;">
                        <table id="tbSummary" style="width: 100%; font-size: x-small;">
                            <tr>
                                <td colspan="2">
                                    <h4>BASE TERM:</h4>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%">LEASE</td>
                                <td style="width: 85%" id="baseSumm" class="tdalignright"></td>
                            </tr>
                            <tr>
                                <td>COMMISSIONABLE</td>
                                <td id="commSumm" class="tdalignright"></td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <h4>BASE TERM DUE DATE(S)</h4>
                                </td>
                            </tr>

                            <tr>
                                <td id="dueDateSumm" colspan="2" style="text-align: center;">
                                    <div id="dvDueDtSummary">
                                        <%--style="height: 100px; overflow: initial;"--%>
                                        <table id="tbdueDateSumm" runat="server" class="tblfixayout">
                                            <tr>
                                                <td class="tdmain"></td>
                                                <td class="tdspceing"></td>
                                                <td class="tdNuneric"></td>
                                                <td class="tdspceing"></td>
                                                <td class="tdNuneric"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h4>OPTION TERM 1:</h4>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%">LEASE</td>
                                <td style="width: 85%" id="optnSumm" class="tdalignright"></td>
                            </tr>
                            <tr>
                                <td>COMMISSIONABLE</td>
                                <td id="optnCommSumm" class="tdalignright"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h4>OPTION TERM 1 DUE DATE(S)</h4>
                                </td>
                            </tr>
                            <tr>
                                <td id="Td1" colspan="2" style="text-align: center;">
                                    <div id="dvOptdueDateSumm">
                                        <%--style="height: 100px; overflow: initial;"--%>
                                        <table id="tbOptdueDateSumm" runat="server" class="tblfixayout">
                                            <tr>
                                                <td class="tdmain"></td>
                                                <td class="tdspceing"></td>
                                                <td class="tdNuneric"></td>
                                                <td class="tdspceing"></td>
                                                <td class="tdNuneric"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h4>CONTINGENT:</h4>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%">LEASE</td>
                                <td style="width: 85%" id="contengentSumm" class="tdalignright"></td>
                            </tr>
                            <tr>
                                <td>COMMISSIONABLE</td>
                                <td id="contengentCommSumm" class="tdalignright"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h4>CONTINGENT TERM DUE DATE(S)</h4>
                                </td>
                            </tr>
                            <tr>
                                <td id="Td2" colspan="2" style="text-align: center;">
                                    <div id="dvContendueDateSumm">
                                        <%--style="height: 100px; overflow: initial;"--%>
                                        <table id="tbContendueDateSumm" runat="server" class="tblfixayout">
                                            <tr>
                                                <td class="tdmain"></td>
                                                <td class="tdspceing"></td>
                                                <td class="tdNuneric"></td>
                                                <td class="tdspceing"></td>
                                                <td class="tdNuneric"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <h4>OPTION TERM 2:</h4>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%">LEASE</td>
                                <td style="width: 85%" id="optnSumm1" class="tdalignright"></td>
                            </tr>
                            <tr>
                                <td>COMMISSIONABLE</td>
                                <td id="optnCommSumm1" class="tdalignright"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h4>OPTION TERM 2 DUE DATE(S)</h4>
                                </td>
                            </tr>
                            <tr>
                                <td id="Td3" colspan="2" style="text-align: center;">
                                    <div id="dvOptdueDateSumm1">
                                        <%--style="height: 100px; overflow: initial;"--%>
                                        <table id="tbOptdueDateSumm1" runat="server" class="tblfixayout">
                                            <tr>
                                                <td class="tdmain"></td>
                                                <td class="tdspceing"></td>
                                                <td class="tdNuneric"></td>
                                                <td class="tdspceing"></td>
                                                <td class="tdNuneric"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <h4>OPTION TERM 3:</h4>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%">LEASE</td>
                                <td style="width: 85%" id="optnSumm2" class="tdalignright"></td>
                            </tr>
                            <tr>
                                <td>COMMISSIONABLE</td>
                                <td id="optnCommSumm2" class="tdalignright"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h4>OPTION TERM 3 DUE DATE(S)</h4>
                                </td>
                            </tr>
                            <tr>
                                <td id="Td4" colspan="2" style="text-align: center;">
                                    <div id="dvOptdueDateSumm2">
                                        <%--style="height: 100px; overflow: initial;"--%>
                                        <table id="tbOptdueDateSumm2" runat="server" class="tblfixayout">
                                            <tr>
                                                <td class="tdmain"></td>
                                                <td class="tdspceing"></td>
                                                <td class="tdNuneric"></td>
                                                <td class="tdspceing"></td>
                                                <td class="tdNuneric"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <h4>OPTION TERM 4:</h4>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%">LEASE</td>
                                <td style="width: 85%" id="optnSumm3" class="tdalignright"></td>
                            </tr>
                            <tr>
                                <td>COMMISSIONABLE</td>
                                <td id="optnCommSumm3" class="tdalignright"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h4>OPTION TERM 4 DUE DATE(S)</h4>
                                </td>
                            </tr>
                            <tr>
                                <td id="Td5" colspan="2" style="text-align: center;">
                                    <div id="dvOptdueDateSumm3">
                                        <%--style="height: 100px; overflow: initial;"--%>
                                        <table id="tbOptdueDateSumm3" runat="server" class="tblfixayout">
                                            <tr>
                                                <td class="tdmain"></td>
                                                <td class="tdspceing"></td>
                                                <td class="tdNuneric"></td>
                                                <td class="tdspceing"></td>
                                                <td class="tdNuneric"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                        </table>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:HiddenField ID="test" runat="server" EnableViewState="true" />



            <%--</td><table style="width: 300px; background-color: white;">
                <tr>
                   
                    
                   
                </tr>
            </table>--%>
        
        </td>
    </tr>
    <tr>
        <td style="height: 20px;">
            <br />
        </td>
    </tr>
    <%--  <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>--%>
</table>



