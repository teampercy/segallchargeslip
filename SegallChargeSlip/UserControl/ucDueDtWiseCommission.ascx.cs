﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AjaxControlToolkit;
using System.Web.Script.Services;
using System.Web.Services;
using SegallChargeSlipBLL;

public partial class UserControl_ucDueDtWiseCommission : System.Web.UI.UserControl
{
    DataTable dtDueDtComm = null;
    DataRow dRow;

    TextBox txt;
    ImageButton btn;
    Button btnNtfy;
    TableCell tc;
    TableRow tr;
    Label lbl;
    int cnt = 0;
    int iCount = 0;

    AjaxControlToolkit.CalendarExtender ajxCal;
    ModalPopupExtender mpe;
    
    public string strParName { get; set; }
    public Boolean IsFullCommPaid
    {
        get { return Convert.ToBoolean(ViewState["IsFullCommPaid"]); }
        set { ViewState["IsFullCommPaid"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (hdnupdate.Value.Trim() != "true")
            {
                GenerateTableDynamically();
            }
            if (!IsPostBack)
            {
                CalendarExtender1.SelectedDate = DateTime.Now.Date;
                //txtAddDuedt.Attributes.Add("onkeyup", "javascript:return validateDate('" + txtAddDuedt.Text + "')");
            
                Session["NotifyDays"] = txtNotify.Value;
                BindNotificationDaysByChargeSlip();
            }
            
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucDueDtWiseCommission.ascx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    public void CreatedtDueDtCommTable()
    {
        dtDueDtComm = new DataTable();
        dtDueDtComm.Columns.Clear();
        dtDueDtComm.Columns.Add("CommPer", typeof(decimal));
        dtDueDtComm.Columns.Add("CommAmount", typeof(decimal));
        dtDueDtComm.Columns.Add("DueDate", typeof(string));
        //dtDueDtComm.Columns.Add("RowId", typeof(Int32));
        Session.Add(GlobleData.CommissionTb, dtDueDtComm);
        //Session[strParName] = dtDueDtComm;
    }

    public void SetPropDueDateWiseCommission()
    {
        try
        {
            dtDueDtComm = (DataTable)Session[GlobleData.CommissionTb];
            if (tbDueWiseCommission.Rows.Count > 0)
            {
                DataTable dtTemp = new DataTable();
                dtTemp = dtDueDtComm.Clone();
                int cnt = tbDueWiseCommission.Rows.Count - 2;
                // int start = 1;
                for (int i = 0; i <= cnt; i = i + 2)
                {
                    dRow = dtTemp.NewRow();
                    dRow["DueDate"] = Convert.ToString(((TextBox)tbDueWiseCommission.Rows[i].Cells[0].FindControl("txtAddDuedt_" + i)).Text);
                    dRow["CommPer"] = Convert.ToDecimal(((Label)tbDueWiseCommission.Rows[i + 1].Cells[0].FindControl("lblAddPer_" + i)).Text);
                    dRow["CommAmount"] = Convert.ToDecimal(((Label)tbDueWiseCommission.Rows[i + 1].Cells[1].FindControl("lblAddAmount_" + i)).Text);
                    //dRow["RowId"] = start;//dtDueDtComm.Rows[i]["RowId"];
                    dtTemp.Rows.Add(dRow);
                    // start += 1;
                }
                // DataRow[] dRowFil = dtDueDtComm.Select("[CommissionDueId] is null");
                // dtDueDtComm = dRowFil.CopyToDataTable();
                ////dtDueDtComm.Rows.Clear();

                Session.Add(GlobleData.CommissionTb, dtTemp);
                // Session[strParName] = dtTemp;

               
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucDueDtWiseCommission.ascx", "SetPropDueDateWiseCommission", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    public void GenerateTableDynamically(bool isAddRow = false)
    {
        try
        {
            dtDueDtComm = (DataTable)Session[GlobleData.CommissionTb];
            int RN = 0;
            if (dtDueDtComm != null)
            {
                tbDueWiseCommission.Rows.Clear();
                iCount = 0; // added by jay 
                if (dtDueDtComm.Rows.Count != 0)
                {

                    for (int i = 0; i <= dtDueDtComm.Rows.Count - 1; i++)
                    {
                        txt = new TextBox();
                        txt.ID = "txtAddDuedt_" + iCount;
                        txt.Text = dtDueDtComm.Rows[i]["DueDate"].ToString();
                        txt.Width = 85;
                        txt.Height = 18;
                        //if (IsFullCommPaid) { txt.Enabled = false; }//Comment By Jaywanti on 23-11-2015

                        //txt.Attributes.Add("onchange", "javascript:return DateRangeValidation('" + this.ClientID + "','" + txt.ID + "')");
                        txt.Attributes.Add("onchange", "javascript:return DueDateRangeUpdate('" + this.ClientID + "','" + txt.ID + "','" + i + "')");
                        //    txt.TextChanged += new EventHandler(txtAddDuedt_TextChanged);
                        tc = new TableCell();
                        tc.Controls.Add(txt);

                        ajxCal = new CalendarExtender();
                        ajxCal.ID = "Cal_" + iCount;
                        ajxCal.TargetControlID = txt.ID;
                        ajxCal.Format = "MM/dd/yyyy";

                        //if (IsFullCommPaid) { ajxCal.Enabled = false; }//Comment By Jaywanti on 23-11-2015
                        tc.Controls.Add(ajxCal);

                        tr = new TableRow();
                        tr.Cells.Add(tc);
                        tbDueWiseCommission.Controls.AddAt(iCount, tr);
                        //upTable.Update();
                        // tbDueWiseCommission.Rows.Add(tr);

                        lbl = new Label();
                        lbl.ID = "lblAddPer_" + iCount;
                        lbl.Text = dtDueDtComm.Rows[i]["CommPer"].ToString();
                        tc = new TableCell();
                        tc.Controls.Add(lbl);

                        lbl = new Label();
                        lbl.ID = "syb" + iCount;
                        lbl.Text = "%";
                        tc.Controls.Add(lbl);

                        tr = new TableRow();

                        tr.Cells.Add(tc);

                        decimal CommAmount = Convert.ToDecimal(dtDueDtComm.Rows[i]["CommAmount"].ToString());

                        lbl = new Label();
                        lbl.ID = "lblAddAmount_" + iCount;
                        lbl.Text = CommAmount.ToString("#.##");
                        lbl.Attributes.Add("Class", "DollarLabel"); //JAX                        
                        tc = new TableCell();
                        tc.Controls.Add(lbl);
                        tr.Cells.Add(tc);

                        btn = new ImageButton();
                        btn.ID = "imgBtn" + iCount;
                        btn.ImageUrl = "../Images/close.png";
                        btn.Attributes.Add("onclick", "javascript:DeleteDueDate('" + (iCount) + "','" + i + "','" + this.ClientID + "');return false;");
                        //if (IsFullCommPaid) { btn.Enabled = false; }//Comment By Jaywanti on 23-11-2015
                        tc = new TableCell();
                        tc.Controls.Add(btn);
                        tr.Cells.Add(tc);

                        tbDueWiseCommission.Controls.AddAt(iCount + 1, tr);

                        //txt = new TextBox();
                        //txt.ID = "txtNotify_" + iCount;
                        //btnNtfy = new Button();
                        //btnNtfy.ID = "btnNtfy" + iCount;
                        //btnNtfy.Text = "Notify Me";
                        ////btnNtfy.OnClientClick = "javascript:$get('" + hdnPopup.ClientID + "').click(); return false;";
                        //btnNtfy.Attributes.Add("onclick", "javascript:FillNotifyDaysPopup('" + this.ClientID + "','" + txt.ID + "');");
                        //btnNtfy.Attributes.Add("class", "SqureButton");
                        //tc = new TableCell();
                        //tc.Controls.Add(btnNtfy);

                        //mpe = new ModalPopupExtender();
                        //mpe.ID = "mpe_" + iCount;
                        //mpe.PopupControlID = dvPopupNotifyDetails.ID;
                        //mpe.TargetControlID = btnNtfy.ID;
                        //mpe.CancelControlID = btnCancelPopup.ID;
                        //mpe.BackgroundCssClass = "modalBackground";
                        //tc.Controls.Add(mpe);

                        //tr = new TableRow();
                        //tr.Cells.Add(tc);
                                                
                        //txt.Text = "15,30,60,90";                        
                        //txt.Width = 130;
                        //txt.Height = 18;                        
                        //tc = new TableCell();
                        //tc.Controls.Add(txt);
                        //tr.Cells.Add(tc);

                        //tbDueWiseCommission.Controls.AddAt(iCount + 2, tr);

                        //Added by Jaywanti on 23-11-2015
                        DealTransactions obj = new DealTransactions();
                        DataSet ds = new DataSet();
                        ds = obj.CheckCommissiondueDatePayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 1, dtDueDtComm.Rows[i]["DueDate"].ToString());
                        string Ispayment = "False";
                        int PaymentCount = 0;
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            PaymentCount = Convert.ToInt32(ds.Tables[0].Rows[0]["RowCountResult"].ToString());
                            if (PaymentCount > 0)
                            {
                                Ispayment = "True";
                                txt.Enabled = true;
                                ajxCal.Enabled = true;
                                btn.Enabled = true;

                                for (int p = 0; p < tbDueWiseCommission.Rows.Count; p++)
                                {
                                    ((TextBox)tbDueWiseCommission.Rows[p].Cells[0].FindControl("txtAddDuedt_" + p)).Enabled = true;
                                    ((CalendarExtender)tbDueWiseCommission.Rows[p].Cells[0].FindControl("Cal_" + p)).Enabled = true;
                                    ((ImageButton)tbDueWiseCommission.Rows[p + 1].Cells[0].FindControl("imgBtn" + p)).Enabled = true;
                                    p = p + 1;
                                }
                            }

                        }
                        iCount = iCount + 2; // replaved i by icount by jay 


                    }
                    cnt = Convert.ToInt32(dtDueDtComm.Rows.Count) + 1;
                }

                //RN = dtDueDtComm.Rows.Count == 0 ? 0 : (tbDueWiseCommission.Rows.Count); //ADDEd inside by JAY
                RN = iCount;
            }
            //for (int i = 2; i <= tbDueWiseCommission.Rows.Count; i = i + 2)
            //{
            if (isAddRow)
            {
                addnewdaterow(RN);
            }

            if (tbDueWiseCommission.Rows.Count > 0)
            {
                btnNotifyMe.Style.Add("display", "block");
            }
            else
            {
                btnNotifyMe.Style.Add("display", "none");
            }
            //upTable.Update();
            //UpdatePanel uc = (UpdatePanel)this.Parent.FindControl("upDueDate");
            //uc.Update(); 
            SetPropDueDateWiseCommission();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucDueDtWiseCommission.ascx", "GenerateTableDynamically", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            //// throw;
        }
        //  tbDueWiseCommission.Rows[RandomNumber].Controls.Add(tc);

    }

    protected void addnewdaterow(int iCOunt)
    {
        try
        {
            if (txtAddPer.Text != "")
            {
                txt = new TextBox();
                txt.ID = "txtAddDuedt_" + iCOunt;
                txt.Text = txtAddDuedt.Text;
                txt.Width = 85;
                txt.Height = 18;
                txt.Attributes.Add("onchange", "javascript:return DateRangeValidation('" + this.ClientID + "','" + txt.ID + "')");
                //    txt.TextChanged += new EventHandler(txtAddDuedt_TextChanged);
                tc = new TableCell();
                tc.Controls.Add(txt);

                ajxCal = new CalendarExtender();
                //ajxCal.ID = "Cal_" + iCOunt + 1;
                ajxCal.ID = "Cal_" + iCOunt;
                ajxCal.TargetControlID = txt.ID;
                ajxCal.Format = "MM/dd/yyyy";
                tc.Controls.Add(ajxCal);

                tr = new TableRow();
                tr.Cells.Add(tc);
                tbDueWiseCommission.Controls.AddAt(iCOunt, tr);//rn - 1
                                                               //upTable.Update();
                                                               // tbDueWiseCommission.Rows.Add(tr);

                lbl = new Label();
                lbl.ID = "lblAddPer_" + iCOunt;
                lbl.Text = txtAddPer.Text;
                tc = new TableCell();
                tc.Controls.Add(lbl);

                lbl = new Label();
                lbl.ID = "syb" + iCOunt;
                lbl.Text = "%";
                tc.Controls.Add(lbl);

                tr = new TableRow();

                tr.Cells.Add(tc);

                lbl = new Label();
                lbl.ID = "lblAddAmount_" + iCOunt;
                lbl.Text = txtAddAmount.Text;
                lbl.Attributes.Add("Class", "DollarLabel"); //JAX
                tc = new TableCell();
                tc.Controls.Add(lbl);
                tr.Cells.Add(tc);

                btn = new ImageButton();
                btn.ID = "imgBtn" + iCOunt;
                btn.ImageUrl = "../Images/close.png";
                btn.Attributes.Add("onclick", "javascript:DeleteDueDate('" + (iCOunt) + "','" + dtDueDtComm.Rows.Count + "','" + this.ClientID + "');return false;");
                tc = new TableCell();
                tc.Controls.Add(btn);
                tr.Cells.Add(tc);

                tbDueWiseCommission.Controls.AddAt(iCOunt + 1, tr);

                //txt = new TextBox();
                //txt.ID = "txtNotify_" + iCOunt;
                //btnNtfy = new Button();
                //btnNtfy.ID = "btnNtfy" + iCOunt;
                //btnNtfy.Text = "Notify Me";
                ////btnNtfy.OnClientClick = "javascript:$get('" + hdnPopup.ClientID + "').click(); return false;";
                //btnNtfy.Attributes.Add("onclick", "javascript:FillNotifyDaysPopup('" + this.ClientID + "','" + txt.ID + "');");
                //btnNtfy.Attributes.Add("class", "SqureButton");
                ////btnNtfy.Attributes.Add("onclick", "javascript:return false;");
                //tc = new TableCell();
                //tc.Controls.Add(btnNtfy);

                //mpe = new ModalPopupExtender();
                //mpe.ID = "mpe_" + iCOunt;
                //mpe.PopupControlID = dvPopupNotifyDetails.ID;                
                //mpe.TargetControlID = btnNtfy.ID;
                //mpe.CancelControlID = btnCancelPopup.ID;
                //mpe.BackgroundCssClass = "modalBackground";                          
                //tc.Controls.Add(mpe);

                //tr = new TableRow();
                //tr.Cells.Add(tc);
                                
                //txt.Text = "15,30,60,90";
                //txt.Width = 130;
                //txt.Height = 18;
                //tc = new TableCell();
                //tc.Controls.Add(txt);
                //tr.Cells.Add(tc);

                //tbDueWiseCommission.Controls.AddAt(iCount + 2, tr);                               
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucDueDtWiseCommission.ascx", "addnewdaterow", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }
    //protected void DeleteRecord((object sender, EventArgs e)
    //{
    //    dtDueDtComm = (DataTable)Session[GlobleData.CommissionTb];
    //    dtDueDtComm.Rows[RowIndex-1].Delete();
    //    dtDueDtComm.Rows[RowIndex - 1].Delete();
    //    Session[GlobleData.CommissionTb] = dtDueDtComm;
    //}

    protected void btnAddDueDtComm_Click(object sender, EventArgs e)
    {
        GenerateTableDynamically(true);
        hdnupdate.Value = "";
    }

    public void SetAttributesDueDtCommission()
    {
        try
        {
            if (IsFullCommPaid) { btnAddDueDate.Enabled = true; }
            hdnDueDatetbCount.Value = "1";
            btnAddDueDate.Attributes.Add("onclick", "javascript:return ScrollToggle('" + dvNewDuedtwiseComm.ClientID + "')"); //+ "','" + this.ClientID +
            TextBox SalePriceParentTB = ((TextBox)this.Parent.FindControl("txtSetFee"));
            btnAddDueDtComm.Attributes.Add("onclick", "javascript:return ChkEntry('" + SalePriceParentTB.ClientID + "','" + this.ClientID + "')");
            txtAddPer.Attributes.Add("onchange", "javascript:return CalCommissionAmount('" + SalePriceParentTB.ClientID + "','" + this.ClientID + "')");
            txtAddAmount.Attributes.Add("onchange", "javascript:return CalCommissionPercentage('" + SalePriceParentTB.ClientID + "','" + this.ClientID + "')");


            //jay //SalePriceParentTB.Attributes.Add("onchange", "javascript:return CalCommissionAmountOrPer('" + SalePriceParentTB.ClientID + "','" + this.ClientID + "')");
            TextBox txtsaleprice = ((TextBox)this.Parent.FindControl("txtSalePrice"));
            if (txtsaleprice != null)
            {
                SalePriceParentTB.Attributes.Add("onchange", "javascript:return CalCommissionAmountFEE('" + txtsaleprice.ClientID + "','" + this.ClientID + "')");
            }
            else
            {
                SalePriceParentTB.Attributes.Add("onchange", "javascript:return CalCommissionAmountFEEForCon('" + this.ClientID + "')");
            }

            txtAddDuedt.Attributes.Add("onchange", "javascript:return DateRangeValidation('" + this.ClientID + "','txtAddDuedt')");
            hdnParentId.Value = SalePriceParentTB.ClientID;

            CreatedtDueDtCommTable();
            dtDueDtComm.Rows.Clear();
            Session[GlobleData.CommissionTb] = dtDueDtComm;
            //Session[strParName] = dtDueDtComm;
         

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucDueDtWiseCommission.ascx", "SetAttributesDueDtCommission", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }


    protected void rngDate_Init(object sender, EventArgs e)
    {
        ((RangeValidator)sender).MinimumValue = DateTime.Now.Date.ToString("MM/dd/yyyy");
    }


    //protected void txtAddDuedt_TextChanged(object sender, EventArgs e)
    //{
    //    string u;
    //}

    //protected void btnAddDays_Click(object sender, EventArgs e)
    //{
    //    iCount = 0;

    //    lbl = new Label();
    //    lbl.ID = "lblCustomNotifyDays_" + iCount;
    //    lbl.Text = txtAddDays.Text;
    //    tc = new TableCell();
    //    tc.Controls.Add(lbl);

    //    btn = new ImageButton();
    //    btn.ID = "imgBtnClose" + iCount;
    //    btn.ImageUrl = "../Images/delete.gif";

    //    tc.Controls.Add(btn);
    //    tr = new TableRow();
    //    tr.Cells.Add(tc);

    //    tbCustomNotificatoDays.Controls.AddAt(iCount, tr);
    //}

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Session["NotifyDays"] = txtNotify.Value;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucDueDtWiseCommission.ascx", "btnSave_Click", ex.Message);
        }
    }


    public void BindNotificationDaysByChargeSlip()
    {
        try
        {
            if (Session[GlobleData.NewChargeSlipId] != null)
            {
                CommissionDueDatesNotification objCommissionDueDatesNotification = new CommissionDueDatesNotification();
                //long ChargeSlipId = 182085;
                objCommissionDueDatesNotification.ChargeSlipId = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                DataSet ds = objCommissionDueDatesNotification.BindNotificationDaysByChargeSlip(objCommissionDueDatesNotification.ChargeSlipId);
                string IsNotifyOn = string.Empty;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        IsNotifyOn = string.Empty;
                        IsNotifyOn = Convert.ToString(row["IsNotifyOn"]);
                        if (!(string.IsNullOrEmpty(IsNotifyOn)))
                        {
                            txtNotify.Value = IsNotifyOn;
                        }
                        else
                        {
                            txtNotify.Value = Session["NotifyDays"].ToString();
                        }
                        Session["NotifyDays"] = txtNotify.Value;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucDueDtWiseCommission.ascx", "BindNotificationDaysByChargeSlip", ex.Message);
        }
    }
}