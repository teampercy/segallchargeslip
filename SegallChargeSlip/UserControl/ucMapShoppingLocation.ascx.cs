﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_ucMapShoppingLocation : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string NewLatitude
    { get { return ViewState["NewLatitude"] == null ? "0" : Convert.ToString(ViewState["NewLatitude"]); } set { ViewState["NewLatitude"] = value; } }



    public string NewLongtitude
    { get { return ViewState["NewLongtitude"] == null ? "0" : Convert.ToString(ViewState["NewLongtitude"]); } set { ViewState["NewLongtitude"] = value; } }

     
    protected void btnNExt_Click(object sender, EventArgs e)
    {
        NewLatitude = hdnNewlocationLon.Value.Trim();
        NewLongtitude = hdnNewlocationLon.Value.Trim();
        this.Page.GetType().InvokeMember("setMapLocation", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
    }
}