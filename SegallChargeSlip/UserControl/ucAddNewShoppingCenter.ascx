﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAddNewShoppingCenter.ascx.cs" Inherits="UserControl_ucAddNewShoppingCenter" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&Place=true&libraries=places"></script>
<script type="text/javascript" src="../Js/CommonValidations.js"></script>
<style type="text/css">
    .SetMapMarker {
        width:25px;
        height:20px;
        border: none;
        vertical-align: middle;
        display:inline-block;
        
    }
</style>
<script type="text/javascript">
    function SelectSingleRadiobutton(rdbtnid) {
        var rdBtn = document.getElementById(rdbtnid);
        var rdBtnList = document.getElementsByTagName("input");
        for (i = 0; i < rdBtnList.length; i++) {
            if (rdBtnList[i].type == "radio" && rdBtnList[i].id != rdBtn.id) {
                rdBtnList[i].checked = false;
            }
        }
        return false;
    }
    function GetLatitudeLongitude() {
        debugger;

        var state = $('[id^=ContentMain_ucAddNewShoppingCenter_ddlState] option:selected').val();
        // var state = $('#ddlState option:selected').val();
        // var address = $('#txtName').val() + ' ' + $('#txtAddress1').val() + ' ' + $('#txtAddress2').val() + ' ' + $('#txtCity').val() + ' ' + state + ' ' + $('[id^=ContentMain_ucAddNewShoppingCenter_txtZipCode]').val()
        var address = $('[id^=ContentMain_ucAddNewShoppingCenter_txtZipCode]').val(); //$('[id^=ContentMain_ucAddNewShoppingCenter_txtAddress1]').val() + ' ' + $('[id^=ContentMain_ucAddNewShoppingCenter_txtAddress2]').val() + ' ' + $('[id^=ContentMain_ucAddNewShoppingCenter_txtCity]').val() + ' ' + state + ' ' + 
        showAddress(address);
    }
    function Validate(event) {
        var regex = new RegExp("^[0-9 \-]+$");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    }
    function showAddress(address) {

        var geocoder = new google.maps.Geocoder();

        if (address != '' && address != 0 && address != null) {
            geocoder.geocode({ 'address': address }, function (results, status) {

                debugger;
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    $('[id^=ContentMain_ucAddNewShoppingCenter_txtLatitude]').val(latitude);
                    $('[id^=ContentMain_ucAddNewShoppingCenter_txtLongitude]').val(longitude);
                    //$('#txtLatitude').val(latitude);
                    //$('#txtLongitude').val(longitude);
                    //alert(latitude +' '+ longitude );
                } else {
                    alert(address + " not found");
                }
            });
        }
        else { alert('Please enter the address'); }
    };
    function ValidateAll() {
        debugger;
        var rdlocationtype = $("#<%= rdbLocationType.ClientID %> input:checked");
        
       // var rdlocationtype = document.getElementById("<%= rdbLocationType.ClientID %>");
        var txtName = document.getElementById("<%=txtName.ClientID%>").value;
        var txtAddress1 = document.getElementById("<%=txtAddress1.ClientID%>").value;
        var txtCity = document.getElementById("<%=txtCity.ClientID%>").value;
        var ddlState = document.getElementById("<%=ddlState.ClientID%>").value;
        var txtCounty = document.getElementById("<%=txtCounty.ClientID%>").value;
        var txtZip = document.getElementById("<%=txtZipCode.ClientID%>").value;
        var lblerr = document.getElementById("<%=lbErrorMsg.ClientID%>");
        if (rdlocationtype.val() == "0"){
            if (txtName == "") {
                lblerr.innerHTML = "Please Enter Name";
                document.getElementById("<%=txtName.ClientID%>").focus();
                return false;
            }
        }else{
            if (txtAddress1 == "") {
                lblerr.innerHTML = "Please Enter Address";
                document.getElementById("<%=txtAddress1.ClientID%>").focus();
                return false;
            }
        }
         if (txtCity == "") {
            lblerr.innerHTML = "Please Enter City";
            document.getElementById("<%=txtCity.ClientID%>").focus();
            return false;
        }
        else if (txtCounty == "") {
            lblerr.innerHTML = "Please Enter County";
            document.getElementById("<%=txtCounty.ClientID%>").focus();
            return false;
        }
        else if (ddlState == "0" || ddlState == "") {
            lblerr.innerHTML = "Please Enter State";
            document.getElementById("<%=ddlState.ClientID%>").focus();
            return false;
        }
        else if (txtZip == "") {
            lblerr.innerHTML = "Please Enter ZipCode";
            document.getElementById("<%=txtZipCode.ClientID%>").focus();
            return false;
        }


    return true;
    }

    function toggleLocation() {
        
        var rblSelectedValue = $("#<%= rdbLocationType.ClientID %> input:checked");
        var lblerr = document.getElementById("<%=lbErrorMsg.ClientID%>");
        lblerr.innerHTML = "";
        //alert('1');
        if (rblSelectedValue.val() == "0") {//shopping center=0
          //  alert('2');
            //document.getElementById('<%= trShoppingCenter.ClientID %>').visible = true;
            $('#<%= trShoppingCenter.ClientID %>').show();
         
            document.getElementById('<%= trAdd2.ClientID %>').className = 'hide';
            document.getElementById('<%= trAddr.ClientID %>').className = 'hide';
            document.getElementById('<%=hdnLocationSelType.ClientID %>').value = "0";
            
            document.getElementById('<%= imgMapMarker.ClientID %>').style.display = "none";
            
        }
        else if (rblSelectedValue.val() == "1") {//not a shopping center=1
          //  alert('3');
            //alert(document.getElementById('<%= imgMapMarker.ClientID %>').style.visibility);
            document.getElementById('<%=hdnLocationSelType.ClientID %>').value = "1";
            document.getElementById('<%= trAdd2.ClientID %>').className = '';
            document.getElementById('<%= trAddr.ClientID %>').className = '';
            $('#<%= trShoppingCenter.ClientID %>').hide();
           
            document.getElementById('<%= imgMapMarker.ClientID %>').style.display = "block";
            
        }
        return false;
    }
    $(document).ready(function () {
        $('#<%= trShoppingCenter.ClientID %>').show();
        $('#<%= trAdd2.ClientID %>').hide();
        $('#<%= trAddr.ClientID %>').hide();
          });
       
</script>
<style>
    .hide {
 display:none;
    
    }
     .visible {
 visibility:hidden;
    
    }
</style>
<asp:UpdatePanel ID="upAddNewShoppingCenterOrCompany" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
<table style="width: 100%; padding-left: 5px; padding-right: 5px;">
    <tr>
        <%--style="background-color: gray;"--%>
        <td colspan="2" style="text-align: left;">
            <%-- <h3>ADD NEW SHOPPING CENTER</h3>--%>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label runat="server" ID="lbErrorMsg" ForeColor="Red" Font-Bold="true" Font-Names="Vardena"></asp:Label>
        </td>
    </tr>
    <tr >
        <td colspan="2"><asp:HiddenField ID="hdnLocationSelType" runat ="server" />
            <asp:RadioButtonList ID="rdbLocationType" runat="server" onchange="return toggleLocation();" RepeatDirection="Horizontal" AutoPostBack="false">
                <asp:ListItem Text="Shopping Center" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="No Shopping Center" Value="1"></asp:ListItem>
            </asp:RadioButtonList></td>
    </tr>
     <tr id="trShoppingCenter" runat="server">
        <td style="width: 40%">SHOPPING CENTER
        </td>
        <td>
            <asp:textbox id="txtName" runat="server" width="200px"></asp:textbox>
        </td>
    </tr>
 <%--     <tr id="trShoppingCenter" runat="server"><td colspan="2">
          <div></div>
          <table><tr>  <td style="width: 40%"  >SHOPPING CENTER
        </td>
        <td>
            <asp:TextBox ID="TextBox1" runat="server" Width="200px"></asp:TextBox>
        </td></tr></table></td>
    </tr>--%>
     <tr id="trAddr" class="hide" runat="server">
        <td style="width: 40%">ADDRESS LINE 1
        </td>
        <td>
            <asp:TextBox ID="txtAddress1" runat="server" Width="200px"></asp:TextBox>
        </td>
    </tr>
     <tr id="trAdd2" class="hide" runat="server" >
        <td style="width: 40%">ADDRESS LINE 2
        </td>
        <td>
            <asp:TextBox ID="txtAddress2" runat="server" Width="200px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width: 40%">CITY
        </td>
        <td>
            <asp:TextBox ID="txtCity" runat="server" Width="200px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width: 40%">COUNTY
        </td>
        <td>
            <asp:TextBox ID="txtCounty" runat="server" Width="200px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width: 40%">STATE
        </td>
        <td>
            <asp:DropDownList ID="ddlState" runat="server" Width="200px" Font-Size="Small"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="width: 40%">ZIPCODE
        </td>
        <td>
            <asp:TextBox ID="txtZipCode" runat="server" Width="200px" onkeypress="return Validate(event);"></asp:TextBox>
        </td>
    </tr>
    <tr> 

        <td style="width: 40%;">Latitude<a href="#" onclick="GetLatitudeLongitude()" style="cursor: pointer; border: none; color: black; padding-left: 5px;display:inline-block;vertical-align:middle;line-height:20px;" >
            <asp:Image ImageUrl="../Images/map_marker.png" ToolTip="Get Latitude and Longitude" ID="imgMapMarker" runat ="server"  CssClass="SetMapMarker"/>
            <%--<img src="../Images/map_marker.png" title="Get Latitude and Longitude"  width="25" height="25" style="border: none; vertical-align: middle;" />--%>
        </a>
        </td>
        <td>
            <asp:Label ID="lbllat" runat="server" Visible="false"></asp:Label>
            <asp:TextBox ID="txtLatitude" runat="server" Width="200px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width: 40%">Longitude
        </td>
        <td>
            <asp:Label ID="lbllon" runat="server" Visible="false"></asp:Label>
            <asp:TextBox ID="txtLongitude" runat="server" Width="200px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width: 40%">Is Active
        </td>
        <td>
            <asp:CheckBox runat="server" ID="chkIsactive" Checked="true" />
        </td>
    </tr>
    <tr>
        <td style="width: 40%"></td>
        <td>
            <asp:HiddenField ID="hdnLocationId" Value="" runat="server" />
            <asp:Button ID="btnAddNew" runat="server" Text="ADD" OnClick="btnAdd_Click" OnClientClick="javascript:return ValidateAll();" CssClass="SqureGrayButtonSmall" />
              <asp:HiddenField ID="hdnClassRef" runat="server" />
                    <asp:Label ID="lbDummy1" runat="server"></asp:Label>
                    <ajax:ModalPopupExtender ID="mpDuplicateShoppingCenter" TargetControlID="lbDummy1" CancelControlID="btnCancelPopup"
                        PopupControlID="dvOnDuplication" runat="server" BackgroundCssClass="modalBackground">
                    </ajax:ModalPopupExtender>
        </td>
       
    </tr>
</table>
        <div id="dvOnDuplication" class="PopupDivBodyStyle" style="height: auto;">
            <table>
                <tr align="right" style="background-color: gray;">
                    <td colspan="2">
                        <%-- <asp:Button ID="btnCancelPopup" Height="20px" Width="20px" Text="X" runat="server" />--%>
                        <asp:ImageButton ID="btnCancelPopup" Height="15px" Width="15px" ImageUrl="~/Images/close.png" runat="server" />
                    </td>
                </tr>
                <tr align="Center">
                    <td colspan="2">
                        <asp:Panel ScrollBars="Both" Width="400px" Height="150px" ID="pnGvDupLoc" runat="server">
                            <asp:UpdatePanel ID="upDupLoc" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="gvDupLocations" runat="server" AutoGenerateColumns="false" Width="99%" OnRowDataBound="gvDupLocations_RowDataBound">
                                        <HeaderStyle BackColor="#cccccc" ForeColor="Black" Font-Bold="true" />
                                        <Columns>
                                            <asp:BoundField DataField="LocationID" />
                                            <asp:BoundField DataField="Address" HeaderText="DB Address" />
                                            <%--      <asp:BoundField DataField="EnteredAddress" HeaderText="New Address" />--%>
                                            <asp:TemplateField HeaderText="Select">
                                                <%--   <HeaderTemplate>
                                    Select
                                </HeaderTemplate>--%>
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="rbSelLocForUpdate" runat="server" OnClick="javascript:SelectSingleRadiobutton(this.id)" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        <asp:Label ID="lbAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="font-family: Verdana; font-size: smaller;">THE Location ADDRESS ENTERED DOES NOT MATCH
                <br />
                        THE ADDRESS IN THE DATABASE FOR "<b>
                            <asp:Label ID="lblShpName" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="LblActShpAddr" runat="server"></asp:Label>
                        </b> <br />

                        " WOULD YOU LIKE TO UPDATE THE ADDRESS OR SAVE THIS AS<br /> 
                        A NEW Location?
                    </td>

                </tr>
                <tr align="right">
                    <td>
                        <asp:Button ID="btnupShoppingCenter" runat="server" Text="UPDATE" CssClass="SqureButton" BackColor="Gray" OnClick="btnUpdate_ShoppingCenter" Height="18px" />
                    </td>
                    <td>
                        <asp:Button ID="btnCreateNewShoppingCenter" runat="server" Text="SAVE AS NEW" CssClass="SqureButton" BackColor="Gray" OnClick="btnAddNew_ShoppingCenter" Height="18px" />
                    </td>
                </tr>
            </table>
        </div>
           <asp:Label ID="lblSame" runat="server"></asp:Label>
        <ajax:ModalPopupExtender ID="mpSameLocation" TargetControlID="lblSame" CancelControlID="btnClosePopup"
            PopupControlID="dvOnsameLocation" runat="server" BackgroundCssClass="modalBackground">
        </ajax:ModalPopupExtender>
        <div id="dvOnsameLocation" class="PopupDivBodyStyle" style="height: 100px;">
             <table>
                <tr align="right" style="background-color: gray;">
                    <td colspan="2">
                        <asp:ImageButton ID="btnClosePopup" Height="15px" Width="15px" ImageUrl="~/Images/close.png" runat="server" />
                    </td>
                </tr>
                <tr align="Center">
                    <td colspan="2">
                        Location Already Exist in the Database.
                    </td>
                </tr>
                 <tr><td colspan="2" align="Center"><asp:Button runat="server" Text="OK" CssClass="SqureButton" BackColor="Gray" /></td></tr>
                </table>
        </div>
        </ContentTemplate>
        </asp:UpdatePanel>