﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;

public partial class UserControl_ucAddDealTypes : System.Web.UI.UserControl
{

    #region Events
    private int PageSize = GlobleData.Pagesize;

    public string DealTypeMode
    {
        get { return String.IsNullOrEmpty(hdnDealTypeMode.Value) ? "L" :  Convert.ToString(hdnDealTypeMode.Value); }
        set
        {
            switch (value)
            {
                case "E":  // Edit                    
                    divListDealTypes.Style.Add("display", "none");// List
                    divEditDealTypes.Style.Remove("display");//.Add("display", "");
                    ((Label)this.Parent.FindControl("lbDealTypesTitle")).Text = "EDIT DEAL TYPE";
                    break;
                case "A": // Add
                    divListDealTypes.Style.Add("display", "none"); // List
                    txtDealType.Text = "";
                    hdnDealTypeId.Value = "0";
                    divEditDealTypes.Style.Add("display", "");
                  //  ((Label)this.Parent.FindControl("lbDealTypesTitle")).Text = "ADD DEAL TYPE";
                    break;
                case "M":  // Manage 
                    divListDealTypes.Style.Add("display", "");// List
                    divEditDealTypes.Style.Add("display", "none");
                    hdnDealTypeId.Value = "0";
                    ((Label)this.Parent.FindControl("lbDealTypesTitle")).Text = "DEAL TYPES";
                    break;
                default:
                    divListDealTypes.Style.Add("display", "");// List
                    divEditDealTypes.Style.Add("display", "none");
                    break;
            }            
            hdnDealTypeMode.Value = value;
        }
    }
    public int DealTypeID
    {
        get { return String.IsNullOrEmpty(hdnDealTypeId.Value) ? 0 : Convert.ToInt32(hdnDealTypeId.Value); }
        set { hdnDealTypeId.Value = value.ToString(); }
    }


    protected void gvDealTypes_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                int TotalRecords = Convert.ToInt32(Session["ucDealTypesTotalRecords"]);
                double dblPageCount = (double)((decimal)TotalRecords / (decimal)PageSize);
                int PageCount = (int)Math.Ceiling(dblPageCount);

                for (int i = 1; i <= PageCount; i++)
                {
                    LinkButton btn = new LinkButton();
                    btn.Width = Unit.Pixel(20);
                    btn.CommandName = "Page";
                    btn.CommandArgument = i.ToString();

                    btn.Text = i.ToString();

                    PlaceHolder place = e.Row.FindControl("phPageNo") as PlaceHolder;
                    place.Controls.Add(btn);
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("UserControl_ucAddDealTypes", "gvDealTypes_RowCreated", ex.Message);
        }
    }

    protected void gvDealTypes_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Del")
            {
                DealTypeID = Convert.ToInt32(e.CommandArgument);
                DealTypes dealTypes = new DealTypes();
                // dealTypes.UserID = UserID;
                //dealTypes.IsActive = false;
                dealTypes.Delete();
                BindGrid(0);


            }
            if (e.CommandName == "EditDealtype")
            {
                DealTypeMode = "E";
                DealTypeID = Convert.ToInt32(e.CommandArgument);
                DealTypes dealTypes = new DealTypes();
                dealTypes.DealTypeID = DealTypeID;

                Boolean isloaded = dealTypes.Load();
                if (isloaded)
                {
                    txtDealType.Text = dealTypes.DealType;
                }
                divListDealTypes.Style.Add("display", "none");// List
                divEditDealTypes.Style.Remove("display");//.Add("display","block");
                ((Label)this.Parent.FindControl("lbDealTypesTitle")).Text = "EDIT DEAL TYPE";
                lbDealTypeErrorMsg.Text = "";
               // upAddNewDealTypes.Update();
            }


        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("UserControl_ucAddDealTypes", "gvDealTypes_RowCommand", ex.Message);
        }
    }
    protected void gvDealTypes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            int i = e.NewPageIndex + 1;
            ViewState["PageNo"] = i;
            BindGrid(i);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("UserControl_ucAddDealTypes", "gvDealTypes_PageIndexChanging", ex.Message);
        }
    }

    public void BindGrid(int PageIndex)
    {
        try
        {
            DealTypes dealTypes = new DealTypes();
            dealTypes.CurrentPageNo = Convert.ToInt32(ViewState["PageNo"]);

            dealTypes.PageSize = PageSize;

            DataSet ds = dealTypes.GetList();
            int TotalRecords = dealTypes.TotalRecords;
            Session["ucDealTypesTotalRecords"] = TotalRecords;
            gvDealTypes.DataSource = ds;
            gvDealTypes.DataBind();
            if (gvDealTypes.Rows.Count > 0)
            {
                gvDealTypes.BottomPagerRow.Visible = true;
            }

            // upDealTypes.Update();

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("UserControl_ucAddDealTypes", "BindGrid", ex.Message);
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            DealTypes dealType = new DealTypes();
            dealType.DealTypeID = DealTypeID;
            dealType.Load();
            dealType.DealType = txtDealType.Text;
            if (!dealType.Save())
            {
                lbDealTypeErrorMsg.Text = "Error: Deal Type could not be saved.";
                DealTypeMode = "A";
            }
            else
            {
                DealTypeMode = "M";
                BindGrid(0);
                this.Page.GetType().InvokeMember("BindDealTypes", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
            }
         
            upAddNewDealTypes.Update();
        }
        catch (Exception ex)
        {
          ErrorLog.WriteLog("UserControl_ucAddDealTypes", "btnSave_Click", ex.Message);
        }
    }
  
  
}