﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSnapshot.ascx.cs" Inherits="UserControl_ucSnapshot" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/UserControl/ucChargeDetailCard.ascx" TagName="ucChargeDetailCard" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/ucPaymentHistoryDetail.ascx" TagName="ucPaymentHistoryDetail" TagPrefix="uc1" %>

<%--<link href="../Styles/SiteTEST.css" rel="stylesheet" />--%>
<script src="../Js/pqgrid.min.js"></script>
<%-- <link href="../Styles/Style.css" rel="stylesheet" />--%>
<style type="text/css">
    .tooltip {
        display: none;
        position: absolute;
        border: 1px solid #333;
        background-color: #161616;
        border-radius: 5px;
        padding: 10px;
        color: #fff;
        font-size: 12px Arial;
    }

    a {
        text-decoration: none;
    }

    .SnpstMaintbl tr td {
        /*padding: 5px;
        width: 45%;*/
        vertical-align: top;
        /*border: 1px solid black;*/
        table-layout: fixed;
    }

    .dateHeader {
        text-align: left;
        padding: 0px 0px 0px 5px;
    }

    .divconttainersp {
        width: 100%;
        border: 1px solid black;
    }

    .divHeadersp {
        width: 100%;
        /*  background-color: gray */
        background-color: #949599;
        height: 20px;
        border-bottom: 1px solid black;
    }

    .divContentsp {
        /*width:%;*/
    }

    .divfootersp {
        width: 100%;
        background-color: #949599;
        float: left;
        padding-top: 4px;
        font: bold;
        height: 20px;
        border: 1px groove black;
    }

    .contLeftAlgn {
        float: left;
        padding-left: 10px;
        width: 55%;
        position: relative;
    }

    .contRightAlgn {
        position: relative;
        float: right;
        width: 35%;
        padding-right: 37px;
        text-align: right;
    }

    .recevables tr td {
        width: 100%;
        border: 0px none;
        padding: 0px;
    }

    .recevables {
        padding: 7px 5px 5px 5px;
    }

    .recevableslefttr {
        text-align: right;
        padding-right: 2px;
        width: 35%;
    }

    .RemoveUnderline {
        text-decoration: none;
    }

    .FixedTables {
        /*border: 0 none !important;*/
        background-color: white;
        border-top: 0px solid black;
        border-right: 0px solid black;
        border-bottom: 0px solid black;
        border-left: 0px solid black;
    }

        .FixedTables tr td {
            border: 0 none !important;
            padding: 5px;
            height: 17px;
        }

        .FixedTables tr {
            table-layout: fixed;
            height: 17px;
        }

            .FixedTables tr th {
                border: 0 none !important;
            }

    .headerspan {
        font-size: Small;
        height: 16px;
        font-weight: bold;
        vertical-align: bottom;
        margin: 4px 0px 0px 4px;
    }

    .gvdatecss {
        width: 15% !important;
        white-space: nowrap;
    }

    .modalBackground {
        background-color: gray;
        filter: alpha(opacity=90);
        opacity: 0.8;
    }

    .modalPopup { 
        background-color: #FFFFFF;
        border-width: 1px;
        border-style: solid;
        border-color: black;
        /*padding-top: 10px;
        padding-left: 10px;*/
        /*width: 300px;
        height: 140px;*/
    }
</style>

   <style type="text/css">
        .divcss {
            overflow-y: scroll;
            height: 285px;
        }
    </style>
<link href="../Js/FixedHeader/css/fixedHeader.dataTables.css" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script src="../Js/DataTables/media/js/jquery.js"></script>
<script src="../Js/Scrollable.js"></script>
<script src="../Js/DataTables/media/js/jquery.dataTables.js"></script>
<script src="../Js/FixedHeader/js/dataTables.fixedHeader.js"></script>
<%--
<asp:UpdatePanel ID="updsnapst" runat="server">
    <ContentTemplate>--%>
<script type="text/javascript" lang="javascript">
    var DT;
    function pageLoad() {
        //alert('call');
        $('.masterTooltip').hover(function () {
            // Hover over code
            //alert('hello');
            var title = $(this).attr('title');
            $(this).data('tipText', title).removeAttr('title');
            $('<p class="tooltip"></p>')
            .text(title)
            .appendTo('body')
            .fadeIn('slow');
        }, function () {
            // Hover out code
            $(this).attr('title', $(this).data('tipText'));
            $('.tooltip').remove();
        }).mousemove(function (e) {
            var mousex = e.pageX + 20; //Get X coordinates
            var mousey = e.pageY + 10; //Get Y coordinates
            $('.tooltip')
            .css({ top: mousey, left: mousex })
        });

    }
    function CallPopupClose() {
        document.getElementById('<%=btnClose.ClientID%>').click();
        return false;
    }
    $(document).ready(function () {
        applyscrollable();
    });

    
    
    function BindDataTable(targetGrid) {
        //This is added because of the header not initially set as per column width in tbody.
        $('#' + '<%= Panel1.ClientID %>').css('display', 'block')

        var sortableGrid = $(targetGrid).attr('id');
        if (($('#' + sortableGrid + ' thead').length) > 0)
        {
            DT = $('#' + sortableGrid).DataTable({
                "destroy": true,
                "fnInitComplete": function () {
                    $('.dataTables_scrollHeadInner > table > thead > tr').addClass("HeaderGridView2").css('cursor', 'pointer');
                    $('.dataTables_scrollFootInner > table > tfoot > tr').addClass("FooterGridView2");
                },
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": false,
                "bSort": true,
                scrollY: '500px',
                "bInfo": false,
                "bScrollCollapse": true,
                "bAutoWidth": true,
                "asStripClasses": null, //To remove "odd"/"event" zebra classes
                "fnDrawCallback": function (settings) {
                    for (var i = 0; i < settings.aoColumns.length; i++) {
                        if (settings.aoColumns[i].sTitle.toLowerCase() == "due date" || settings.aoColumns[i].sTitle.toLowerCase() == "payment date" || settings.aoColumns[i].sTitle.toLowerCase() == "charge date") {
                            settings.aoColumns[i].sType = "date";
                        }
                    }
                    $('#' + sortableGrid + ' tr').removeClass();
                    $('#' + sortableGrid + ' tr:even').addClass("RowGridView2 even");
                    $('#' + sortableGrid + ' tr:odd').addClass("AlternatingRowGridView2 odd");
                    $('.dataTables_scrollHeadInner > table > thead > tr').addClass("HeaderGridView2").css('cursor', 'pointer');
                    $('.dataTables_scrollFootInner > table > tfoot > tr').addClass("FooterGridView2");
                }
            });
        }

    }
    function SetModalPopup(Index) {
        $('#' + '<%=hdnModalPopup.ClientID%>').val(Index);
        return true;
    }

    var gridWidthTotal = 0;
    var gridoffsetWidth_global = 0;
    var headerCellWidths_global;
    //function changeToTable(that) {
    //    var tbl = $("table.fortune500");
    //    tbl.css("display", "");
    //    $("#grid_table").pqGrid("destroy");
    //    $(that).val("Change Table to Grid");
    ////}

    //$(document).ready(function () {
    //    debugger;

    //    var tbl = $("#ContentMain_grdData");
    //    var gridWidth = tbl.offsetWidth;
    //    var obj = $.paramquery.tableToArray(tbl);
    //    var newObj = { width: gridWidth, height: 400,  resizable: false };
    //    newObj.dataModel = { data: obj.data, rPP: 20,  paging: "local" };
    //    newObj.colModel = obj.colModel;
    // $("#grid_table").pqGrid(newObj);
    ////    $(that).val("Change Grid back to Table");
    //    tbl.css("display", "none");
    //});

    function applyscrollable() {
        var ht = $(window).height();
        //  alert($(window).height());

        $('#<%=dvComingdue.ClientID %>').Scrollable();

      <%--  $('#<%=gvPastdue.ClientID %>').Scrollable(); --%>

        $('#<%=dvRecentCharge.ClientID %>').Scrollable();
    }

    function Navigate() {

        var Index = 1;
        $.ajax({
            type: "POST",
            url: "../Pages/Billed.aspx/GetChargeSlipDetails",
            data: '{RowIndx: "' + Index + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                window.open("../Pages/NewCharge.aspx", "_self");
            },
            failure: function (response) {
                alert(response.d);
            }
        });

        return false;
    }
    function NavigateSplitCalculator() {

        var Index = 1;
        $.ajax({
            type: "POST",
            url: "../Pages/Billed.aspx/GetChargeSlipDetails",
            data: '{RowIndx: "' + Index + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                window.open("../Pages/NewChargeCommissionSpitCalCulator.aspx", "_self");
            },
            failure: function (response) {
                alert(response.d);
            }
        });

        return false;
    }
    function CloseMessageDetail() {

        $('#' + '<%=modalHelpAndSupport.ClientID%>').Hide();
        return false;
    }
    function display() {
        //debugger;
        applyscrollable(); document.getElementById('<%=divChargeDetailCard.ClientID%>').style.display = "block";
        return false;
    }

    function Hide() {
      
        document.getElementById('<%=divChargeDetailCard.ClientID%>').style.display = "none";
        document.getElementById('<%=hdnPopup.ClientID %>').value = "0";
        if ($('#' + '<%=hdnPopupFromGrid.ClientID%>').val() == "1") {
            $find("mpe").show();
        } 
        return false;
    }

    function displayPayHistory() {
        //debugger;
        applyscrollable(); document.getElementById('<%=divPaymentHistoryDetail.ClientID%>').style.display = "block";

        //alert(document.getElementById('<%=divPaymentHistoryDetail.ClientID%>'));

    }

    function HidePayHistory() {
        //debugger;
        document.getElementById('<%=divPaymentHistoryDetail.ClientID%>').style.display = "none";


    }

    function hidepopup() {

        var vhdnpopup = document.getElementById('<%=hdnPopup.ClientID %>');

        if (vhdnpopup.value == "1") {
            HidePayHistory();
            vhdnpopup.value = "0";
            return display();
        }
        else {
            HidePayHistory();
        }
    }


    //Maintain scroll position of Gridview inside div on postbacks for Past Due grid.
     $(document).ready(function () {
            maintainScrollPosition();
        });
        function pageLoad() {
            maintainScrollPosition();
        }
        function maintainScrollPosition() {
            $("#dvgvPastdueScroll").scrollTop($('#<%=hfScrollPosition.ClientID%>').val());
        }   
        function setScrollPosition(scrollValue) {
            $('#<%=hfScrollPosition.ClientID%>').val(scrollValue);
        }               
</script>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<asp:UpdatePanel ID="updsnapsht" runat="server">
    <ContentTemplate>
        <style type="text/css">
            .errtext {
                color: red;
                font: bold;
                float: right;
                padding-right: 20px;
            }
        </style>
        <asp:HiddenField ID="hdnPopupFromGrid" Value="1" runat="server"/>
        <table style="width: 100%;" cellspacing="5" class="SnpstMaintbl">
            <%--  <tr>
            <td></td>
            <td></td>
        </tr>--%>
            <tr>
                <td style="width: 49%">
                    <div class="divconttainersp">
                        <div class="divHeadersp">
                            <span class="headerspan">RECEIVABLES SUMMARY</span>
                        </div>
                        <div class="divContentsp">
                            <div id="dvdateselection">
                                <div style="height: 35px; border-top: 1px solid black; padding: 7px 5px 5px 5px; vertical-align: central">
                                    <span style="width: 100PX">DATE</span>

                                    <asp:TextBox ID="txtdtFrom" runat="server" ValidationGroup="validatedate" Width="80px" Height="18px"></asp:TextBox>
                                    <asp:ImageButton ID="imgdtfrom" runat="server" Width="16px" Height="16px" ImageUrl="~/Images/Calander.jpeg" />
                                    <ajax:CalendarExtender ID="calfrom" runat="server" TargetControlID="txtdtFrom" PopupButtonID="imgdtfrom" Format="MM/dd/yyyy"></ajax:CalendarExtender>
                                    <span>TO</span>

                                    <asp:TextBox ID="txtdtTo" runat="server" ValidationGroup="validatedate" Width="80px" Height="18px"></asp:TextBox>
                                    <asp:ImageButton ID="imgtxtdtTo" runat="server" Width="16px" Height="16px" ImageUrl="~/Images/Calander.jpeg" />
                                    <ajax:CalendarExtender ID="caltxtdtTo" runat="server" TargetControlID="txtdtTo" PopupButtonID="imgtxtdtTo" Format="MM/dd/yyyy"></ajax:CalendarExtender>

                                    <span style="float: right">
                                        <asp:Button ID="btnclear" runat="server" CssClass="SqureButton" Text="CLEAR" OnClick="btnclear_Click" /></span>
                                    <span style="float: right">
                                        <asp:Button ID="btnRefresh" ValidationGroup="validatedate" runat="server" CssClass="SqureButton" Text="Refresh" OnClick="btnRefresh_Click" /></span>
                                    <br />
                                    <asp:CompareValidator ID="CompareValidator1" ControlToCompare="txtdtTo" ValidationGroup="validatedate" Operator="LessThan"
                                        ControlToValidate="txtdtFrom" Type="Date" CssClass="errtext" runat="server" Display="Dynamic" ErrorMessage="Invalid Date Range"></asp:CompareValidator>
                                    <br />
                                    <%--<asp:CompareValidator ID="cmpdates" runat="server" ControlToCompare=""></asp:CompareValidator>--%>
                                </div>
                                <div id="divRecevableBox" style="border: 0px solid black; border-top-width: 1px; vertical-align: central;">
                                    <div id="dvBrokers" runat="server" style="height: 30px; padding: 7px 0px 0px 5px; border-bottom: 1px groove black; vertical-align: central">
                                        <span style="width: 100PX">BROKER</span>
                                        <asp:DropDownList ID="drUsers" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drUsers_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                    <div id="divSummaryReceivables" runat="server" style="height: 230px; overflow-y: auto;">
                                        <table class="recevables">
                                            <tr id="trPresplitBrk" runat="server">
                                                <td>
                                                    <%--<asp:Label ID="lblhdgross" runat="server"> Gross Commission Due </asp:Label>--%>
                                                    <asp:LinkButton ID="lblPreSplitCommDueBrk" runat="server" Text="Pre Split Commission Due" title="Sum of all pre-split commissions due to the broker for the current year (1/1 to 12/31) plus any outstanding commissions due from previous years." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('1');"></asp:LinkButton>
                                                    <%-- <asp:ImageButton ID="imgHelpAndSupport" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('1');"/>--%>
                                                    
                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lbllblPreSplitCommDuesBrk" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPreSplitNextBrk" runat="server">
                                                <td style="width: 60%">
                                                    <%--   <asp:Label ID="lblhdrecevables" runat="server">Gross Receivables </asp:Label>--%>
                                                    <asp:LinkButton ID="lblPreSplitCommDueNextYearBrk" runat="server" title="Sum of all pre-split commissions due to the broker in the next calendar year (1/2 to 12/31)." Text="Pre Split Commission Due" OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('2');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton1" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('2');"/>--%>
                                                </td>
                                                <td class="recevableslefttr">

                                                    <asp:Label ID="lblPreSplitCommDuesNextYearBrk" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trEstimatedPostSplitBrk" runat="server">
                                                <td style="width: 60%">
                                                    <asp:LinkButton ID="lblEstimatedPostSplitCommBrk" runat="server" title="Estimated total of all outstanding post-split commissions due in the current year for the broker calculated using the split threshold tiers entered by Admin." Text="Estimated Post Split Commission Due" OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('3');"></asp:LinkButton>
                                                    <%-- <asp:ImageButton ID="ImageButton2" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('3');"/>--%>
                                                    <%--  <asp:Label ID="lblhdEstimatedNetRec" runat="server">Estimated Net Receivables </asp:Label>--%>
                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblEstimatedPostSplitCommsBrk" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPreSplitpaidBrk" runat="server">
                                                <td>
                                                    <asp:LinkButton ID="lblPreSplitpaidBrk" runat="server" Text="Pre Split Paid" title="Sum of all pre-split commissions paid to the broker in the current year." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('4');"></asp:LinkButton>
                                                    <%-- <asp:ImageButton ID="ImageButton3" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('4');"/>--%>
                                                    <%--  <asp:Label ID="lblhdPaid" runat="server">Gross Paid </asp:Label>--%>
                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblPreSplitpaidsBrk" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPostSplitBrk" runat="server">
                                                <td>
                                                    <%--<asp:Label ID="lblhdNetPaid" runat="server">Net Paid </asp:Label>--%>
                                                    <asp:LinkButton ID="lnkPostSplitPaidBrk" runat="server" Text="Post Split Paid" title="Sum of all post-split commissions paid to the broker in the current year (regardless of when the payment was stated to be due in the charge slip; can include charges from previous years or future years)." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('5');"></asp:LinkButton>
                                                    <%--   <asp:ImageButton ID="ImageButton4" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('5');"/>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblPostSplitPaidsBrk" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trAmountneededBrk" runat="server">
                                                <td>
                                                    <%--<asp:Label ID="lblAmountneededBrk" runat="server" Text="Gross Commission needed to Reach Next Tier"  title="Pre-split commission amount needed to reach the next split threshold tier [on 1/1 split automatically resets to 50:50]." CssClass="masterTooltip" ></asp:Label>--%><%--Commented by shishir 13-05-2016--%>
                                                    <asp:LinkButton ID="lnkAmountneededBrk" runat="server" Text="Gross Commission needed to Reach Next Tier" title="Pre-split commission amount needed to reach the next split threshold tier [on 1/1 split automatically resets to 50:50]." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('6');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton8" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('6');"/>--%>
                                                    <%--     <asp:Label ID="lbloptiontdNcontgent" runat="server">Option & Contingent Gross Receivables </asp:Label>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblAmountneededsBrk" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPreSplitTotalBrk" runat="server">
                                                <td>
                                                    <asp:LinkButton ID="lnkPreSplitTotalRecBrk" runat="server" Text="Pre Split Total Receivables" title="Sum of all outstanding pre-split commission totals due for the current year plus all future years, inclusive of the Broker, Company share, and any Co-broker, Master Broker , and Outside Broker share." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('7');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton5" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('7');"/>--%>
                                                    <%--  <span>Total Gross Receivables</span>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblPreSplitTotalRecsBrk" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trTotalPreSplitBrk" runat="server">
                                                <td>

                                                    <asp:LinkButton ID="lnkTotalPreSplitOptConBrk" runat="server" title="Sum of all outstanding pre-split Option & Contingent lease commission totals due for the current year including any outstanding totals from previous years, inclusive of the Broker, Company share, and any Co-broker, Master Broker , and Outside Broker share." Text="Total Pre Split Option & Contingent Receivables" OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('8');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton6" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('8');"/>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblTotalPreSplitOptConsBrk" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trTotalPreSplitOptBrk" runat="server">
                                                <td>
                                                    <asp:LinkButton ID="lnkTotalPreSplitOptConNextYearBrk" runat="server" title="Sum of all outstanding pre-split Option & Contingent lease commission totals due for the next calendar year, inclusive of the Broker, Company share, and any Co-broker, Master Broker , and Outside Broker share." Text="Total Pre Split Option & Contingent Receivables" OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('9');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton7" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('9');"/>--%>


                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lnkTotalPreSplitOptConNextYearsBrk" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trGrossRecAdm" runat="server">
                                                <td>
                                                      <asp:LinkButton ID="lnkGrossRecAdm" runat="server" Text="Gross Receivables" title="Sum of all outstanding gross commission totals due to the company for the current year plus future years, including any outstanding balances from previous years." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('10');"></asp:LinkButton>
                                                    <%--<asp:Label ID="lnkGrossRecAdm" runat="server" Text="Gross Receivables" title="Sum of all outstanding gross commission totals due to the company for the current year plus future years, including any outstanding balances from previous years." CssClass="masterTooltip"></asp:Label>--%>
                                                    <%--<asp:ImageButton ID="ImageButton9" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('10');"/>--%>
                                                    <%-- <asp:LinkButton ID="lnkGrossRecAdm" runat="server" Text="Gross Receivables" OnClick="lblhdgross_Click" CssClass="RemoveUnderline" OnClientClick="return SetModalPopup('10');"></asp:LinkButton>--%>


                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lnkGrossRecsAdm" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>

                                            <tr id="trGrossRecNextYearAdm" runat="server">
                                                <td>
                                                    <asp:LinkButton ID="lnkGrossRecNextYearAdm" runat="server" Text="Gross Receivables" title="Sum of all outstanding gross commission totals due to the company for the current year including any outstanding balances from previous years." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('11');"></asp:LinkButton>
                                                    <%--<asp:Label ID="lnkGrossRecNextYearAdm" runat="server" Text="Gross Receivables" title="Sum of all outstanding gross commission totals due to the company for the current year including any outstanding balances from previous years." CssClass="masterTooltip"></asp:Label>--%>
                                                    <%--<asp:ImageButton ID="ImageButton10" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('11');"/>--%>
                                                    <%-- <asp:LinkButton ID="lnkGrossRecNextYearAdm" runat="server" Text="Gross Receivables" OnClick="lblhdgross_Click" CssClass="RemoveUnderline" OnClientClick="return SetModalPopup('11');"></asp:LinkButton>--%>


                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblGrossRecsNextYearAdm" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPostSplitRecAdm" runat="server">

                                                <td>
                                                      <asp:LinkButton ID="lnkPostSplitRecAdm" runat="server" Text="Post Split Receivables" title="Sum of all post-split company share commission totals due for the current year plus any outstanding balances from previous years." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('12');"></asp:LinkButton>
                                                    <%--<asp:Label ID="lnkPostSplitRecAdm" runat="server" Text="Post Split Receivables" title="Sum of all post-split company share commission totals due for the current year plus any outstanding balances from previous years." CssClass="masterTooltip"></asp:Label>--%>
                                                    <%--<asp:ImageButton ID="ImageButton11" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('12');"/>--%>
                                                    <%--    <asp:LinkButton ID="lnkPostSplitRecAdm" runat="server" Text="Post Split Receivables" OnClick="lblhdgross_Click" CssClass="RemoveUnderline" OnClientClick="return SetModalPopup('12');"></asp:LinkButton>--%>


                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblPostSplitRecsAdm" runat="server" Width="100px" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trComShareAdm" runat="server">
                                                <td>
                                                     <asp:LinkButton ID="lnkComShareRecAdm" runat="server" Text="Company Share Receivables" title="Sum of all post-split company share commission totals due for the next calendar year." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('13');"></asp:LinkButton>
                                                    <%--<asp:Label ID="lnkComShareRecAdm" runat="server" Text="Company Share Receivables" title="Sum of all post-split company share commission totals due for the next calendar year." CssClass="masterTooltip"></asp:Label>--%>
                                                    <%--<asp:ImageButton ID="ImageButton12" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('13');"/>--%>
                                                    <%--<asp:LinkButton ID="lnkComShareRecAdm" runat="server" Text="Company Share Receivables" OnClick="lblhdgross_Click" CssClass="RemoveUnderline" OnClientClick="return SetModalPopup('13');"></asp:LinkButton>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblComShareRecsAdm" runat="server" Width="100px" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPreSplitOptAdm" runat="server">
                                                <td>
                                                      <asp:LinkButton ID="lnkPreSplitOptConAdm" runat="server" Text="Pre Split Options & Contingent Receivables" title="Sum of all pre-split Option & Contingent lease commission totals for the current year plus any outstanding totals from previous years." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('14');"></asp:LinkButton>
                                                    <%--<asp:Label ID="lnkPreSplitOptConAdm" runat="server" Text="Pre Split Options & Contingent Receivables" title="Sum of all pre-split Option & Contingent lease commission totals for the current year plus any outstanding totals from previous years." CssClass="masterTooltip"></asp:Label>--%>
                                                    <%--<asp:ImageButton ID="ImageButton13" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('14');"/>--%>
                                                    <%--   <asp:LinkButton ID="lnkPreSplitOptConAdm" runat="server" Text="Pre Split Options & Contingent Receivables" OnClick="lblhdgross_Click" CssClass="RemoveUnderline" OnClientClick="return SetModalPopup('14');"></asp:LinkButton>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblPreSplitOptConsAdm" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trTotalPreSplitOptAdm" runat="server">
                                                <td>
                                                    <asp:LinkButton ID="lnkTotalPreSplitOptConAdm" runat="server" Text="Total Pre Split Options & Contingent Receivables" title="Sum of all pre-split Option & Contingent lease commission totals for the current year plus future years, including any outstanding totals from previous years." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('15');"></asp:LinkButton>
                                                    <%--<asp:Label ID="lnkTotalPreSplitOptConAdm" runat="server" title="Sum of all pre-split Option & Contingent lease commission totals for the current year plus future years, including any outstanding totals from previous years." Text="Total Pre Split Options & Contingent Receivables" CssClass="masterTooltip"></asp:Label>--%>
                                                    <%--<asp:ImageButton ID="ImageButton14" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('15');"/>--%>
                                                    <%-- <asp:LinkButton ID="lnkTotalPreSplitOptConAdm" runat="server" Text="Total Pre Split Options & Contingent Receivables" OnClick="lblhdgross_Click" CssClass="RemoveUnderline" OnClientClick="return SetModalPopup('15');"></asp:LinkButton>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lnkTotalPreSplitOptConsAdm" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trCollectedAdm" runat="server">
                                                <td>
                                                     <asp:LinkButton ID="lnkCollectedYearPreSplitAdm" runat="server" Text="Collected Year to Date - Pre Split" title="Sum of all pre-split commission totals paid in the current year." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('16');"></asp:LinkButton>
                                                    <%--<asp:Label ID="lnkCollectedYearPreSplitAdm" runat="server" title="Sum of all pre-split commission totals paid in the current year." Text="Collected Year to Date - Pre Split" CssClass="masterTooltip"></asp:Label>--%>
                                                    <%--<asp:ImageButton ID="ImageButton15" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('16');"/>--%>
                                                    <%--  <asp:LinkButton ID="lnkCollectedYearPreSplitAdm" runat="server" Text="Collected Year to Date - Pre Split" OnClick="lblhdgross_Click" CssClass="RemoveUnderline" OnClientClick="return SetModalPopup('16');"></asp:LinkButton>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblCollectedYearPreSplitsAdm" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trCollectYearPostAdm" runat="server">
                                                <td>
                                                    <asp:LinkButton ID="lnkCollecYearPostSplitAdm" runat="server" Text="Collected Year to Date - Post Split" title="Sum of al post-split company share commission totals paid in the current year." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('17');"></asp:LinkButton>
                                                    <%--<asp:Label ID="lnkCollecYearPostSplitAdm" runat="server" title="Sum of al post-split company share commission totals paid in the current year." Text="Collected Year to Date - Post Split" CssClass="masterTooltip"></asp:Label>--%>
                                                    <%--<asp:ImageButton ID="ImageButton16" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('17');"/>--%>
                                                    <%-- <asp:LinkButton ID="lnkCollecYearPostSplitAdm" runat="server" Text="Collected Year to Date - Post Split" OnClick="lblhdgross_Click" CssClass="RemoveUnderline" OnClientClick="return SetModalPopup('17');"></asp:LinkButton>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblCollecYearPostSplitsAdm" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trYearTodateDeal" runat="server">
                                                <td>
                                                    <asp:LinkButton ID="lblYearToDate" runat="server"  Text="Total Transactions Submitted (Current Year)" OnClientClick="return SetModalPopup('28');" OnClick="lblhdgross_Click" CssClass="masterTooltip"
                                                         title="Total number of ChargeSlips submitted for the current year (Jan 1 to Dec 31) by the Brokers."></asp:LinkButton>
                                                  <%--  <asp:Label ID="lblYearToDate" runat="server" title="Total number of ChargeSlips submitted for the current year (Jan 1 to Dec 31) by the Brokers."
                                                        Text="Total Transactions Submitted (Current Year)" CssClass="masterTooltip"></asp:Label>--%>
                                                    <%--<asp:ImageButton ID="ImageButton16" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('17');"/>--%>
                                                    <%-- <asp:LinkButton ID="lnkCollecYearPostSplitAdm" runat="server" Text="Collected Year to Date - Post Split" OnClick="lblhdgross_Click" CssClass="RemoveUnderline" OnClientClick="return SetModalPopup('17');"></asp:LinkButton>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblYearToDateDeals" runat="server" Text="0"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPreviousYearDeal" runat="server">
                                                <td>
                                                     <asp:LinkButton ID="lblPreviousYear" runat="server"  Text="Total Transactions Submitted (Previous Year)" OnClientClick="return SetModalPopup('29');" OnClick="lblhdgross_Click" CssClass="masterTooltip"
                                                        title="Total number of ChargeSlips submitted for the previous year by the Brokers."></asp:LinkButton>
                                                    <%--<asp:Label ID="lblPreviousYear" runat="server" title="Total number of ChargeSlips submitted for the previous year by the Brokers."
                                                        Text="Total Transactions Submitted (Previous Year)" CssClass="masterTooltip"></asp:Label>--%>
                                                    <%--<asp:ImageButton ID="ImageButton16" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('17');"/>--%>
                                                    <%-- <asp:LinkButton ID="lnkCollecYearPostSplitAdm" runat="server" Text="Collected Year to Date - Post Split" OnClick="lblhdgross_Click" CssClass="RemoveUnderline" OnClientClick="return SetModalPopup('17');"></asp:LinkButton>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblPreviousYearDeal" runat="server" Text="0"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPreSplitRecSelect" runat="server">
                                                <td>

                                                    <asp:LinkButton ID="lnkPreSplitRecSelect" runat="server" title="Sum of all outstanding pre-split commission totals due for the current year plus any outstanding total from previous years." Text="Pre Split Receivables" OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('18');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton17" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('18');"/>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lnkPreSplitRecSelects" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPostSplitSelect" runat="server">
                                                <td>

                                                    <asp:LinkButton ID="lnkPostSplitRecSelect" runat="server" title="Sum of all outstanding post-split commission totals due for the current year plus any outstanding totals from previous years (at current split level)." Text="Post Split Receivables" OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('19');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton18" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('19');"/>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblPostSplitRecSelects" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPreSplitNextYearSelect" runat="server">
                                                <td>

                                                    <asp:LinkButton ID="lnkPreSplitRecNextYearSelect" runat="server" title="Sum of all outstanding pre-split commission totals due for the next calendar year." Text="Pre Split Receivables" OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('20');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton19" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('20');"/>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lnkPreSplitRecNextYearSelects" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPostSplitNexSelect" runat="server">
                                                <td>

                                                    <asp:LinkButton ID="lnkPostSplitRecNextYearSelect" runat="server" title="Sum of all outstanding post-split commission totals due for the next calendar year (calculated at 50:50 split level)." Text="Post Split Receivables" OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('21');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton20" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('21');"/>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblPostSplitRecNextYearSelects" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPreSplitpaidSelect" runat="server">
                                                <td>

                                                    <asp:LinkButton ID="lnkPreSplitpaidSelect" runat="server" title="Sum of all pre-split commission totals paid to the broker in the current year." Text="Pre Split Paid" OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('22');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton21" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('22');"/>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lnkPreSplitpaidSelects" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPostSplitPaidSelect" runat="server">
                                                <td>

                                                    <asp:LinkButton ID="lnkPostSplitpaidSelect" runat="server" title="Sum of all post-split commission totals paid to the broker in the current year." Text="Post Split Paid" OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('23');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton22" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('23');"/>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lnkPostSplitpaidSelects" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trReachesToReachNextSelect" runat="server">
                                                <td>

                                                    <asp:LinkButton ID="lnlReachesToReachNextSelect" runat="server" Text="Gross Commission needed to Reach Next Tier" title="Pre-split commission amount needed to reach the next split threshold tier [on 1/1 split automatically resets to 50:50]." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('24');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton22" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('23');"/>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblReachesToReachNextSelect" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPreSplitTotalRecSelect" runat="server">
                                                <td>

                                                    <asp:LinkButton ID="lnkPreSplitTotalRecSelect" runat="server" Text="Pre Split Total Receivables" title="Sum of all outstanding pre-split commission totals due for the current year plus all future years, inclusive of the Broker, Company share, and any Co-broker, Master Broker , and Outside Broker share." OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('25');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton22" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('23');"/>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblPreSplitTotalRecSelect" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trTotalPreSplitOptionConSelect" runat="server">
                                                <td>

                                                    <asp:LinkButton ID="lnkTotalPreSplitOptionConSelect" runat="server" title="Sum of all outstanding pre-split Option & Contingent lease commission totals due for the current year including any outstanding totals from previous years, inclusive of the Broker, Company share, and any Co-broker, Master Broker , and Outside Broker share." Text="Total Pre Split Option & Contingent Receivables" OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('26');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton22" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('23');"/>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblTotalPreSplitOptionConSelect" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>
                                             <tr id="trTotalPreSplitOptionConNextSelect" runat="server">
                                                <td>

                                                    <asp:LinkButton ID="lnkTotalPreSplitOptionConNextSelect" runat="server" title="Sum of all outstanding pre-split Option & Contingent lease commission totals due for the next calendar year, inclusive of the Broker, Company share, and any Co-broker, Master Broker , and Outside Broker share." Text="Total Pre Split Option & Contingent Receivables" OnClick="lblhdgross_Click" CssClass="masterTooltip" OnClientClick="return SetModalPopup('27');"></asp:LinkButton>
                                                    <%--<asp:ImageButton ID="ImageButton22" runat ="server" ImageUrl="~/Images/Questionmark.png" Width="20px" Height="20px" OnClick="imgHelpAndSupport_Click" OnClientClick="return SetModalPopup('23');"/>--%>

                                                </td>
                                                <td class="recevableslefttr">
                                                    <asp:Label ID="lblTotalPreSplitOptionConNextSelect" runat="server" Text="$0.00"></asp:Label>
                                                </td>
                                            </tr>

                                        </table>
                                    </div>


                                    <%--   <table style="border: 0 none;">
                                <tr>
                                    <td>DATE</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                </tr>

                            </table>--%>
                                </div>
                            </div>
                            <%--   <div class="divfootersp">
                        <table class="recevables">
                            <tr>
                                <td>TOTAL

                                </td>
                                <td class="recevableslefttr">
                                    <asp:Label ID="gvtotal" runat="server" Text="$ 12345.00"></asp:Label>
                                </td>
                            </tr>

                        </table>--%>
                            <%-- <div class="contLeftAlgn">Total</div>
                        <div class="contRightAlgn">
                        </div>--%>
                        </div>
                    </div>
                </td>
                <td style="width: 49%">
                    <div class="divconttainersp">
                        <div class="divHeadersp">
                            <span class="headerspan">PAST DUE</span>
                        </div>
                             <asp:HiddenField ID="hfScrollPosition" Value="0" runat="server" />
        <%--                <div class="divContentsp">--%>
                            <div id="dvgvPastdueScroll" class="divcss" onscroll="setScrollPosition(this.scrollTop);">
                            <asp:GridView ID="gvPastdue" runat="server" Width="100%" AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true"
                                class="FixedTables" OnRowCommand="dvComingdue_RowCommand">

                                <Columns>
                                    <asp:BoundField DataField="ChargeDate" HeaderText="Date" HeaderStyle-CssClass="dateHeader" ItemStyle-CssClass="gvdatecss" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="OwnershipEntity" HeaderText="Customer" HeaderStyle-CssClass="dateHeader" ItemStyle-Width="55%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="duedate" HeaderText="Due Date" ItemStyle-Width="15%" HeaderStyle-CssClass="dateHeader" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="AmountDue" DataFormatString="{0:c}" ItemStyle-CssClass="ItemStyleAlignRight" HeaderText="Amount Due" ItemStyle-Width="18%" ItemStyle-Wrap="true" />
                                    <asp:TemplateField ItemStyle-Width="10%" HeaderText="">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgView" CommandName="View" CommandArgument='<%#Eval("ChargeSlipID") %>' Height="10px" Width="10px" ImageUrl="~/Images/Info-Card-Icon.gif" runat="server" />
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <div>

                                        <span>No chargeslip data avilable </span>

                                    </div>

                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <div class="divfootersp">
                            <div class="contLeftAlgn">TOTAL  </div>
                            <div class="contRightAlgn">
                                <asp:Label ID="lblpastDueTotal" runat="server" Text="$ 0.00"></asp:Label>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 49%">
                    <div class="divconttainersp">
                        <div class="divHeadersp">
                            <span class="headerspan">RECENT CHARGE (90 DAYS)</span>
                        </div>
                        <div class="divContentsp">
                            <asp:GridView ID="dvRecentCharge" runat="server" Width="100%" AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true"
                                class="FixedTables" OnRowCommand="dvComingdue_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="ChargeDate" HeaderStyle-CssClass="dateHeader" HeaderText="Date" ItemStyle-CssClass="gvdatecss" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="OwnershipEntity" HeaderText="Customer" HeaderStyle-CssClass="dateHeader" ItemStyle-Width="55%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="duedate" HeaderText="Due Date" ItemStyle-Width="15%" HeaderStyle-CssClass="dateHeader" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="AmountDue" DataFormatString="{0:c}" ItemStyle-CssClass="ItemStyleAlignRight" HeaderText="Amount Due" ItemStyle-Width="18%" ItemStyle-Wrap="true" />
                                    <asp:TemplateField ItemStyle-Width="10%" HeaderText="">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgView" CommandName="View" CommandArgument='<%#Eval("ChargeSlipID") %>' Height="10px" Width="10px" ImageUrl="~/Images/Info-Card-Icon.gif" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <div>

                                        <span>No chargeslip data avilable </span>

                                    </div>

                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <div class="divfootersp">
                            <div class="contLeftAlgn">TOTAL  </div>
                            <div class="contRightAlgn">
                                <asp:Label ID="lblrecntTotal" runat="server" Text="$ 0.00" />
                            </div>
                        </div>
                    </div>
                </td>
                <td style="width: 49%">
                    <div class="divconttainersp">
                        <div class="divHeadersp">
                            <span class="headerspan">COMING DUE (30 DAYS) </span>
                        </div>
                        <div class="divContentsp">
                            <asp:GridView ID="dvComingdue" runat="server" Width="100%" AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true"
                                class="FixedTables" OnRowCommand="dvComingdue_RowCommand">

                                <Columns>
                                    <asp:BoundField DataField="ChargeDate" HeaderStyle-CssClass="dateHeader" HeaderText="Date" ItemStyle-CssClass="gvdatecss" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="OwnershipEntity" HeaderText="Customer" HeaderStyle-CssClass="dateHeader" ItemStyle-Width="55%" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="duedate" HeaderText="Due Date" ItemStyle-Width="15%" HeaderStyle-CssClass="dateHeader" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="AmountDue" DataFormatString="{0:c}" ItemStyle-CssClass="ItemStyleAlignRight" HeaderText="Amount Due" ItemStyle-Width="18%" ItemStyle-Wrap="true" />
                                    <asp:TemplateField ItemStyle-Width="10%" HeaderText="">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgView" CommandName="View" CommandArgument='<%#Eval("ChargeSlipID") %>' Height="10px" Width="10px" ImageUrl="~/Images/Info-Card-Icon.gif" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>

                                    <span>No chargeslip data avilable </span>

                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <div class="divfootersp">
                            <div class="contLeftAlgn">TOTAL  </div>
                            <div class="contRightAlgn">
                                <asp:Label ID="lblComingDue" runat="server" Text="$ 0.00" />
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hdnModalPopup" Value="1" runat="server" />
        <asp:Button ID="btnShow" runat="server" Style="display: none;" />
        <asp:LinkButton ID="lnk12" runat ="server" Style="display: none;" />
        <ajax:ModalPopupExtender ID="ModalDetail" runat="server" BehaviorID="mpe" PopupControlID="Panel1" TargetControlID="btnShow" CancelControlID="lnk12" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>
        <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" align="center" Style="display: none">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr id="divheader" style="background-color: gray; height: 35px;">
                    <td style="border-bottom: solid 2px black;">&nbsp;&nbsp;<asp:Label ID="lblCHARGEDETAIL" Style="vertical-align: bottom; font-size: medium; font-weight: bold; color: Black;"
                        runat="server" Text="Gross Commission Amount"></asp:Label>
                    </td>
                    <td width="30px" style="text-align: center; border-bottom: solid 2px black;">
                        <asp:ImageButton ID="btnClose" src="../Images/close.png" runat="server" OnClick="btnClose_Click1"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <div style="overflow:auto;max-width: 750px; max-height: 650px; min-width: 500px; min-height: 450px; margin-left: 20px; margin-right: 20px;">


                            <asp:GridView ID="grdReceivables" runat="server" AutoGenerateColumns="false"
                                ShowFooter="True" Font-Size="12px" font-family="Verdana" Width="90%" OnRowDataBound="grdReceivables_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="DealName" HeaderText="DealName" />
                                    <asp:BoundField DataFormatString="{0:d}" DataField="DueDate" HeaderText="DUE DATE" ItemStyle-HorizontalAlign="Center" FooterStyle-Width="80px" />
                                    <asp:TemplateField HeaderText="Gross Commission Due" ItemStyle-HorizontalAlign="Right">

                                        <ItemTemplate>
                                            $<asp:Label ID="lblCommAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("CommAmountDue"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooCommAmount" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="BrokerSubPercent" HeaderText="Broker %" />

                                    <asp:TemplateField HeaderText="Pre Split Amount" ItemStyle-HorizontalAlign="Right">

                                        <ItemTemplate>
                                            $<asp:Label ID="lblPreSplitAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("PreSplitComm"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooPreSplitAmount" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paid Amount" ItemStyle-HorizontalAlign="Right">

                                        <ItemTemplate>
                                            $<asp:Label ID="lblPaidAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("GrossPaid"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooPaidAmount" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Post Split" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            $<asp:Label ID="lblPostSplit" runat="server" Text='<%# String.Format("{0:n2}",Eval("EstimatedPostSplitReceivables"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooPostSplit" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pre Split Outstanding" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            $<asp:Label ID="lblPreSplitOutstanding" runat="server" Text='<%# String.Format("{0:n2}",Eval("PreSplitOutstanding"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooPreSplitOutstanding" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <EmptyDataTemplate>
                                    <b style="padding: 2px 5px  2px 25px; color: #000000; background-color: #FFFFFF; height: 26px; vertical-align: middle; cursor: pointer;">No records found.</b>
                                </EmptyDataTemplate>
                                <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                <HeaderStyle CssClass="HeaderGridView2" />
                                <RowStyle CssClass="RowGridView2" />
                                <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                <FooterStyle CssClass="FooterGridView2" />
                            </asp:GridView>

                            <asp:GridView ID="grdFutureRec" runat="server" AutoGenerateColumns="false"
                                ShowFooter="True" Font-Size="12px" font-family="Verdana" Width="95%" OnRowDataBound="grdFutureRec_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="DealName" HeaderText="DealName" />
                                    <asp:BoundField DataFormatString="{0:d}" DataField="DueDate" HeaderText="DUE DATE" ItemStyle-HorizontalAlign="Center" FooterStyle-Width="80px" />
                                    <asp:TemplateField HeaderText="Gross Commission Due" ItemStyle-HorizontalAlign="Right">

                                        <ItemTemplate>
                                            $<asp:Label ID="lblCommAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("CommAmountDue"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooCommAmount" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="BrokerSubPercent" ItemStyle-HorizontalAlign="Center" HeaderText="Broker %" />

                                    <asp:TemplateField HeaderText="Pre Split Amount" ItemStyle-HorizontalAlign="Right">

                                        <ItemTemplate>
                                            $<asp:Label ID="lblPreSplitAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("PreSplitComm"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooPreSplitAmount" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paid Amount" ItemStyle-HorizontalAlign="Right">

                                        <ItemTemplate>
                                            $<asp:Label ID="lblPaidAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("GrossPaid"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooPaidAmount" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Post Split" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            $<asp:Label ID="lblPostSplit" runat="server" Text='<%# String.Format("{0:n2}",Eval("EstimatedPostSplitReceivables"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooPostSplit" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <b style="padding: 2px 5px  2px 25px; color: #000000; background-color: #FFFFFF; height: 26px; vertical-align: middle; cursor: pointer;">No records found.</b>
                                </EmptyDataTemplate>
                                <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                <HeaderStyle CssClass="HeaderGridView2" />
                                <RowStyle CssClass="RowGridView2" />
                                <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                <FooterStyle CssClass="FooterGridView2" />
                            </asp:GridView>
                            <%--<form runat="server" id="frmPayment">--%>
                            <asp:GridView ID="grdPayment" runat="server" AutoGenerateColumns="false" ShowHeader="true"
                                ShowFooter="True" Font-Size="12px" font-family="Verdana" Width="95%" OnRowDataBound="grdPayment_RowDataBound" OnRowCommand="grdPayment_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="ChargeSlipId" HeaderText="ChargeSlipId" Visible="false" />
                                    <asp:BoundField DataField="CompanyName" HeaderText="Charge To" ItemStyle-Width="120px" />
                                    <asp:BoundField DataField="DealName" HeaderText="Deal Name" ItemStyle-Width="120px" />
                                    <asp:BoundField DataField="Broker" HeaderText="Broker" />
                                    <asp:BoundField DataFormatString="{0:d}" DataField="PaymentDate" HeaderText="Payment Date" ItemStyle-HorizontalAlign="Center" FooterStyle-Width="80px" />

                                    <asp:TemplateField HeaderText="Paid Amount" ItemStyle-HorizontalAlign="Right">

                                        <ItemTemplate>
                                            $<asp:Label ID="lblPaidAmount" runat="server" Text='<%# String.Format("{0:n2}",Eval("GrossPaid"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooPaidAmount" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SplitPercent1" HeaderText="Broker Split%" ItemStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="Net Paid" ItemStyle-HorizontalAlign="Right">

                                        <ItemTemplate>
                                            $<asp:Label ID="lblNetPaid" runat="server" Text='<%# String.Format("{0:n2}",Eval("NetPaid"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooNetPaid" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="BrokerCheckNo" HeaderText="Broker Check No." ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="CompanyCheckNo" HeaderText="Company Check No." ItemStyle-HorizontalAlign="Center" />

                                    <asp:TemplateField ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                                        <ItemTemplate>
                                            <%--<asp:ImageButton ID="imgChargeslip"  Style="text-align: center;" ToolTip="View Chargeslip Detail" Width="15px" CommandName="ViewChargeslip" CommandArgument='<%#Eval("ChargeSlipId")%>' Height="15px" runat="server" ImageUrl="~/Images/Edit-Icon.gif"  />--%>
                                            <asp:ImageButton ID="imgEditForm2" Style="text-align: right;" Width="15px" Height="15px" runat="server" ImageUrl="~/Images/Edit-Icon.gif" CommandName="ViewChargeslip" CommandArgument='<%#Eval("ChargeSlipId")%>' />
                                            <%--OnClientClick="javascript:return Navigate();"--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <b style="padding: 2px 5px  2px 25px; color: #000000; background-color: #FFFFFF; height: 26px; vertical-align: middle; cursor: pointer;">No records found.</b>
                                </EmptyDataTemplate>
                                <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                <HeaderStyle CssClass="HeaderGridView2" />
                                <RowStyle CssClass="RowGridView2" />
                                <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                <FooterStyle CssClass="FooterGridView2" />
                            </asp:GridView>
                            <asp:GridView ID="grdEarning" runat="server" AutoGenerateColumns="false"
                                ShowFooter="True" Font-Size="12px" font-family="Verdana" Width="95%" OnRowDataBound="grdEarning_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="EarningsFrom" HeaderText="Earnings From" />
                                    <asp:BoundField DataField="EarningsTo" HeaderText="Earnings To" />
                                    <asp:BoundField DataField="PercentBroker" HeaderText="Broker %" />

                                    <asp:TemplateField HeaderText="Pre Split Paid" ItemStyle-HorizontalAlign="Right">

                                        <ItemTemplate>
                                            $<asp:Label ID="lblPreSplipaid" runat="server" Text='<%# String.Format("{0:n2}",Eval("PreSplitPaid"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooPreSplipaid" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pre Split Outstanding" ItemStyle-HorizontalAlign="Right">

                                        <ItemTemplate>
                                            $<asp:Label ID="lblPreSplitOut" runat="server" Text='<%# String.Format("{0:n2}",Eval("PreSplitOutstanding"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooPreSplitOut" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Estimated Post Split Due" ItemStyle-HorizontalAlign="Right">

                                        <ItemTemplate>
                                            $<asp:Label ID="lblEstPostSplitDue" runat="server" Text='<%# String.Format("{0:n2}",Eval("EstimatedPostSplitDue"))%>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooEstPostSplitDue" runat="server" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <b style="padding: 2px 5px  2px 25px; color: #000000; background-color: #FFFFFF; height: 26px; vertical-align: middle; cursor: pointer;">No records found.</b>
                                </EmptyDataTemplate>
                                <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                <HeaderStyle CssClass="HeaderGridView2" />
                                <RowStyle CssClass="RowGridView2" />
                                <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                <FooterStyle CssClass="FooterGridView2" />
                            </asp:GridView>
                            <br />
                            <br />
                            <div id="divNextSplitDetailSummary" runat="server" style="border: 1px solid black;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td class="label"><b>Current Split Range :</b></td>
                                        <td align="right">
                                            <asp:Label ID="lblCurrentSplitRange" runat="server" Text="12"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="label"><b>Current Split Percent:</b></td>
                                        <td align="right">
                                            <asp:Label ID="lblCurrentSplitPercent" runat="server" Text="50"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="label"><b>Next Split Range :</b></td>
                                        <td align="right">
                                            <asp:Label ID="lblEarningNextRange" runat="server" Text="12"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td class="label"><b>Next Split Percent:</b></td>
                                        <td align="right">
                                            <asp:Label ID="lblEarningNextSplitPercent" runat="server" Text="50"></asp:Label></td>
                                    </tr>


                                </table>
                            </div>


                        </div>
                    </td>
                </tr>
                <tr>
               <%--       <td align="right" style="padding-right: 33px;">
                     
                    </td>--%>
                    <td  align="right" style="padding-right: 33px;" colspan="2">
                           <asp:Button ID="btnExportToExcel" runat="server" Text="Export To Excel" CssClass="SqureButton" OnClick="btnExportToExcel_Click" /> 
                        <asp:Button ID="btnExporttoPDF" runat="server" Text="Export To PDF" CssClass="SqureButton" OnClick="btnExporttoPDF_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <%--   <tr>
                    <td colspan="2">
                     
                    </td>
                </tr>--%>
                <%--<tr>
                    <td colspan="2" align="right">
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="SqureButton" Width="100px" Style="margin-right: 10px;" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>--%>
            </table>
        </asp:Panel>
        <div style="margin: 0; width: 100%;">

            <ajax:ModalPopupExtender ID="modalHelpAndSupport" runat="server" CancelControlID="btnCancel"
                TargetControlID="lbDummy" PopupControlID="pnlHelpAndSupport" BackgroundCssClass="modalBackground">
            </ajax:ModalPopupExtender>
            <asp:Label ID="lbDummy" runat="server"></asp:Label>
            <asp:LinkButton ID="btnCancel" runat="server" Style="display: none" />
            <asp:Panel ID="pnlHelpAndSupport" runat="server" Style="display: none; height: 100%; width: 100%; z-index: 100000000;">

                <div style="width: 700px; height: auto; overflow: auto; margin: auto; margin-top: 250px;">
                    <table border="0" cellpadding="0" cellspacing="0" style="vertical-align: top; border-bottom-color: #728AA3; border-bottom-style: solid; border-bottom-width: 1px; border-right-color: #728AA3; border-right-style: solid; border-right-width: 1px; border-left-color: #728AA3; border-left-style: solid; border-left-width: 1px; background-color: White;"
                        width="100%"
                        align="center">
                        <tr>
                            <td valign="top" style="background-color: gray; height: 25px; width: 590px; padding-left: 10px; vertical-align: middle;" align="center">
                                <asp:Label ID="lblTitle" runat="server" Text="Details" Font-Bold="true"
                                    ForeColor="White">
                                </asp:Label>
                            </td>
                            <td valign="middle" align="right" style="background-color: gray; height: 21px; width: 20px;">
                                <asp:ImageButton ID="btnCancel1" runat="server" ImageUrl="~/Images/close.png" OnClick="btnClose_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 20px;">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 50px; padding-left: 50px; padding-right: 50px;">
                                <asp:Label ID="lblMessageHelpAndSupport" runat="server" Text=""></asp:Label></td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <br />
                            </td>
                        </tr>

                    </table>

                </div>
            </asp:Panel>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExporttoPDF" />
         <asp:PostBackTrigger ControlID="btnExportToExcel" />
    </Triggers>
</asp:UpdatePanel>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hdnPopup" runat="server" />
        <div id="divPaymentHistoryDetail" runat="server" style="display: none;">
            <uc1:ucPaymentHistoryDetail ID="ucPaymentHistoryDetail1" runat="server" />
        </div>
        <div id="divChargeDetailCard" runat="server" style="display: none;">
            <uc1:ucChargeDetailCard ID="ucChargeDetailCard1" runat="server" />
        </div>
    </ContentTemplate>

</asp:UpdatePanel>
