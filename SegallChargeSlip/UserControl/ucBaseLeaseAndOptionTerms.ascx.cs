﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SegallChargeSlipBll;
using System.Web.UI.HtmlControls;
using SegallChargeSlipBLL;

public partial class UserControl_ucBaseLeaseAndOptionTerms : System.Web.UI.UserControl
{
    #region Variables
    Random rnd = new Random();
    DataTable dtBasicLeaseTerm, dtCommTerm, dtOptTerm, dtOptCommTerm, dt, dtDueDtComm, dtAttachment;
    DataTable dtOptTerm1, dtOptCommTerm1, dtOptTerm2, dtOptCommTerm2, dtOptTerm3, dtOptCommTerm3;
    DataRow dRow;
    const string cns_dtBasicLeaseTerm = GlobleData.cns_dtBasicLeaseTerm;
    const string cns_dtCommTerm = GlobleData.cns_dtCommTerm;

    const string cns_dtOptTerm = "dtOptTerm";
    const string cns_dtOptCommTerm = "dtOptCommTerm";

    const string cns_dtOptTerm1 = "dtOptTerm1";
    const string cns_dtOptCommTerm1 = "dtOptCommTerm1";
    const string cns_dtOptTerm2 = "dtOptTerm2";
    const string cns_dtOptCommTerm2 = "dtOptCommTerm2";
    const string cns_dtOptTerm3 = "dtOptTerm3";
    const string cns_dtOptCommTerm3 = "dtOptCommTerm3";

    decimal RentPSF;
    decimal SizeSF = 0;
    decimal totPeriodRent = 0;
    public int totLeaseTermPeriod = 0;
    public int totComission = 0;
    public int totCommTermPeriod = 0;
    public decimal totCommFee = 0;
    public decimal totRent = 0;

    string chkParent;
    public string userCntr;
    const string cns_ucBaseLeaseTerms = "ucBaseLeaseTerms";
    const string cns_ucOptionalTerms = "ucOptionalTerms";
    const string cns_ucContingentCommission = "ucContingentCommission";

    const string cns_ucOptional1Terms = "ucOptional1Terms";
    const string cns_ucOptional2Terms = "ucOptional2Terms";
    const string cns_ucOptional3Terms = "ucOptional3Terms";

    HiddenField hdnCommissionTypeId = new HiddenField();
    public decimal TotalSquareFoot = 0;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //Disable past date in ajax control
                //ajaxCal.SelectedDate = DateTime.Now.Date;
                SetAttributes();
                SetCSData();
                if (this.ClientID.Contains(cns_ucBaseLeaseTerms))
                {
                    txtLeaseTermMonths.Focus();
                }

                Session["NotifyDays"] = txtNotify.Value;
                BindNotificationDaysByChargeSlip();
            }
            else
            {
                string ucName = this.ClientID;
                ReCreateDueDatesOnPostBack(ucName);
                GenerateSummary(ucName);

                if (ViewState["LeaseTermPeriod"] != null)
                {
                    int x = rnd.Next(701, 900);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "SetSumm" + x, "javascript:SetSummary1('" + ViewState["LeaseTermPeriod"].ToString() + "','" + this.ID + "','Term');", true);
                }
                if (ViewState["CommTermPeriod"] != null)
                {
                    int x = rnd.Next(901, 1100);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ScriptCoom" + x, "javascript:SetSummary('" + ViewState["CommTermPeriod"].ToString() + "','" + this.ID + "','Comm');", true);
                }

                int y = rnd.Next(200, 300);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "SetSumm" + y, "javascript:VisibleNotifyButton();", true);
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #region DataTables

    public void CreateBasicLeaseTermTable()
    {
        dtBasicLeaseTerm = new DataTable();
        dtBasicLeaseTerm.Columns.Clear();
        // dtBasicLeaseTerm.Columns.Add("LeaseTermYears", typeof(decimal));
        dtBasicLeaseTerm.Columns.Add("RentFromPeriod", typeof(int));
        dtBasicLeaseTerm.Columns.Add("RentToPeriod", typeof(int));
        dtBasicLeaseTerm.Columns.Add("TotalMonths", typeof(decimal));
        dtBasicLeaseTerm.Columns.Add("Rent_PSF", typeof(decimal));
        dtBasicLeaseTerm.Columns.Add("AnnualRent", typeof(decimal));
        dtBasicLeaseTerm.Columns.Add("PerIncrease", typeof(decimal));
        dtBasicLeaseTerm.Columns.Add("PeriodRent", typeof(decimal));
        dtBasicLeaseTerm.Columns.Add("TotalSF", typeof(decimal));
        dtBasicLeaseTerm.Columns.Add("LeaseTermId", typeof(int));
        dtBasicLeaseTerm.Columns.Add("TermPeriod", typeof(int));
        Session.Add(cns_dtBasicLeaseTerm + userCntr, dtBasicLeaseTerm);
    }

    public void CreateCommissionableTermTable()
    {
        dtCommTerm = new DataTable();
        dtCommTerm.Columns.Clear();
        dtCommTerm.Columns.Add("LeaseTermYears", typeof(decimal));
        dtCommTerm.Columns.Add("RentFromPeriod", typeof(int));
        dtCommTerm.Columns.Add("RentToPeriod", typeof(int));
        dtCommTerm.Columns.Add("TotalMonths", typeof(int));
        dtCommTerm.Columns.Add("BrokerCommission", typeof(decimal));
        dtCommTerm.Columns.Add("Fee", typeof(decimal));
        dtCommTerm.Columns.Add("CommissionableTermId", typeof(int));
        dtCommTerm.Columns.Add("CommTermPeriod", typeof(int));
        Session.Add(cns_dtCommTerm + userCntr, dtCommTerm);
    }

    #endregion

    #region GridView

    protected void gvBaseLeaseTerm_Bind(object sender, GridViewRowEventArgs e)
    {
        try
        { 
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[9].Visible = false;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[9].Visible = false;
            Label PeriodRent = ((Label)e.Row.FindControl("lbItemPeriodRent"));

            if (PeriodRent != null)
            {
                if (!string.IsNullOrEmpty(PeriodRent.Text))
                {
                    totPeriodRent += Convert.ToDecimal(PeriodRent.Text);
                    totLeaseTermPeriod += Convert.ToInt32(((Label)e.Row.FindControl("lbItemTotMonths")).Text);
                    if (e.Row.RowIndex != 0)
                    {
                        Label PrevPeriodRent = (Label)gvBaseLeaseTerm.Rows[e.Row.RowIndex - 1].FindControl("lbItemAnnualRent");
                        if (PrevPeriodRent != null)
                        {
                            decimal CurrRent = Convert.ToDecimal(((Label)e.Row.FindControl("lbItemAnnualRent")).Text);
                            decimal PrevRent = Convert.ToDecimal((PrevPeriodRent).Text);
                            if (CurrRent != PrevRent)
                            {
                                //Added by Jaywanti on 02-06-2016
                                decimal DiffPer = 0;
                                if (PrevRent > 0)
                                {
                                    DiffPer = Convert.ToDecimal((((CurrRent - PrevRent) / PrevRent) * 100).ToString("#.##"));
                                    ((Label)e.Row.FindControl("lbItemPerIncrese")).Text = DiffPer.ToString();
                                }
                                else
                                {
                                    DiffPer = 0;
                                    ((Label)e.Row.FindControl("lbItemPerIncrese")).Text = "0.00";
                                }
                            }
                        }

                    }
                    else if (e.Row.RowIndex == 0 && ((System.Web.UI.WebControls.CompositeDataBoundControl)(sender)).ClientID != "ContentMain_ucBaseLeaseTerms_gvBaseLeaseTerm")
                    //&& check id if it is not base lease terms then use the hidden field ((System.Web.UI.WebControls.CompositeDataBoundControl)(sender)).ID
                    {
                        if (this.Parent.FindControl("hdnleaseLast") != null)
                        {
                            HiddenField hdnleaseLast1 = (HiddenField)this.Parent.FindControl("hdnleaseLast");
                            string strPrevPeriodRent = hdnleaseLast1.Value; // ControlValue("ContentMain_hdnleaseLast");// hdnleaseLast1.Value;
                            if (strPrevPeriodRent != null && strPrevPeriodRent != "")
                            {
                                decimal CurrRent = Convert.ToDecimal(((Label)e.Row.FindControl("lbItemAnnualRent")).Text);
                                decimal PrevRent = Convert.ToDecimal((strPrevPeriodRent));
                                if (CurrRent != PrevRent)
                                {
                                    //Added by Jaywanti on 02-06-2016
                                    decimal DiffPer = 0;
                                    if (PrevRent > 0)
                                    {
                                        DiffPer = Convert.ToDecimal((((CurrRent - PrevRent) / PrevRent) * 100).ToString("#.##"));
                                        ((Label)e.Row.FindControl("lbItemPerIncrese")).Text = DiffPer.ToString();
                                    }
                                    else
                                    {
                                        DiffPer = 0;
                                        ((Label)e.Row.FindControl("lbItemPerIncrese")).Text = "0.00";
                                    }
                                }
                                else
                                {
                                    ((Label)e.Row.FindControl("lbItemPerIncrese")).Text = "0.00";
                                }
                            }
                        }
                    }
                }
                if (this.ClientID.Contains(cns_ucBaseLeaseTerms))
                {
                    totRent += Convert.ToDecimal(((Label)e.Row.FindControl("lbItemAnnualRent")).Text);
                }
            }
            txtFromRentPeriod.Attributes.Add("onchange", "javascript:ValidateMonthRange('" + this.ClientID + "','" + e.Row.RowIndex + "');");
            txtToRentPeriod.Attributes.Add("onchange", "javascript:ValidateMonthRange('" + this.ClientID + "','" + e.Row.RowIndex + "');");
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[8].Visible = false;
            Label PeriodRent = ((Label)e.Row.FindControl("lblPeriodRent"));
            if (PeriodRent != null)
            {
                PeriodRent.Text = totPeriodRent.ToString("N2");

                if (totLeaseTermPeriod > 0)
                {
                    int x = 0;
                    string ParentUc = this.ClientID;
                    if (ParentUc.Contains(cns_ucBaseLeaseTerms))
                    {
                        x = rnd.Next(100, 199);
                    }
                    else if (ParentUc.Contains(cns_ucOptionalTerms))
                    {
                        x = rnd.Next(200, 299);
                    }
                    else if (ParentUc.Contains(cns_ucContingentCommission))
                    {
                        x = rnd.Next(300, 399);
                    }
                    else if (ParentUc.Contains(cns_ucOptional1Terms))
                    {
                        x = rnd.Next(400, 499);
                    }
                    else if (ParentUc.Contains(cns_ucOptional2Terms))
                    {
                        x = rnd.Next(500, 599);
                    }
                    else if (ParentUc.Contains(cns_ucOptional3Terms))
                    {
                        x = rnd.Next(600, 699);
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ScriptSumm" + x, "javascript:SetSummary1('" + totLeaseTermPeriod.ToString() + "','" + this.ID + "','Term');", true);
                    ViewState["LeaseTermPeriod"] = totLeaseTermPeriod;
                    Session[GlobleData.TotalRent] = totRent;


                    //PeriodRent.Attributes.Add("onchange", "javascript:setsummary('" + totLeaseTermPeriod.ToString() + "','" + this.ID + "','Term')");
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "isactive", "javascript:setsummary('" + totLeaseTermPeriod.ToString() + "','" + this.ID + "','Term');", true);
                    //periodrent.attributes.add("onchange", "javascript:setsummary('" + totcommtermperiod.tostring() + "','" + this.id + "','term')");
                    // scriptmanager.registerstartupscript(this, this.gettype(), "isactive", "javascript:setsummary('" + totleasetermperiod.tostring() + "','" + this.id + "','term');", true);
                }
            }
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "gvBaseLeaseTerm_Bind", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    protected void gvBaseLeaseTerm_DataBound(object sender, EventArgs e)
    {
        try
        { 
        if (gvBaseLeaseTerm.EditIndex != -1)
        {
            GridViewRow editRow = gvBaseLeaseTerm.Rows[gvBaseLeaseTerm.EditIndex];

            //TextBox txtEditFromRentPeriod = (TextBox)editRow.FindControl("txtEditFromRentPeriod");
            //TextBox txtEditToRentPeriod = (TextBox)editRow.FindControl("txtEditToRentPeriod");
            //txtEditFromRentPeriod.Attributes.Add("onchange", "javascript:return SetRent('" + gvBaseLeaseTerm.ClientID + "')");
            //txtEditToRentPeriod.Attributes.Add("onchange", "javascript:return SetRent('" + gvBaseLeaseTerm.ClientID + "')");


            TextBox txtEditRentPSP = (TextBox)editRow.FindControl("txtEditRentPSP");
            TextBox txtEditAnnualRent = (TextBox)editRow.FindControl("txtEditAnnualRent");
            TextBox txtItemFromRentPeriod = (TextBox)editRow.FindControl("txtItemFromRentPeriod");
            TextBox txtItemToRentPeriod = (TextBox)editRow.FindControl("txtItemToRentPeriod");

            //JAY
            //txtEditRentPSP.Attributes.Add("onchange", "javascript:return CalCulateRentPSPChange('" + gvBaseLeaseTerm.ClientID + "','" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.EditIndex + "','" + this.ClientID + "')");
            //txtEditAnnualRent.Attributes.Add("onchange", "javascript:return CalCulateRentAnnualRentChange('" + gvBaseLeaseTerm.ClientID + "','" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.EditIndex + "','" + this.ClientID + "')");
            int CommissionType = ddlCommissionType.SelectedValue == "" ? 1 : Convert.ToInt32(ddlCommissionType.SelectedValue);
            txtEditRentPSP.Attributes.Add("onchange", "javascript:return CalCulateRentPSPChange('" + gvBaseLeaseTerm.ClientID + "','" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.EditIndex + "','" + this.ClientID + "'," + CommissionType + ")");
            txtEditAnnualRent.Attributes.Add("onchange", "javascript:return CalCulateRentAnnualRentChange('" + gvBaseLeaseTerm.ClientID + "','" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.EditIndex + "','" + this.ClientID + "')");
            txtItemFromRentPeriod.Attributes.Add("onchange", "javascript:return CalCulateRentMonthPeriodChange('" + gvBaseLeaseTerm.ClientID + "','" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.EditIndex + "','" + this.ClientID + "','" + CommissionType + "')");
            txtItemToRentPeriod.Attributes.Add("onchange", "javascript:return CalCulateRentMonthPeriodChange('" + gvBaseLeaseTerm.ClientID + "','" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.EditIndex + "','" + this.ClientID + "','" + CommissionType + "')");

        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "gvBaseLeaseTerm_DataBound", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    protected void gvBaseLeaseTerm_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        { 
        chkParent = Convert.ToString(this.ClientID);
        gvBaseLeaseTerm.EditIndex = e.NewEditIndex;
        BindBasicLeaseTermTable(chkParent);
            //gvBaseCommTerms_RowEditing(sender, e); //JAY
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "gvBaseLeaseTerm_RowEditing", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    protected void gvBaseLeaseTerm_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        { 
        chkParent = Convert.ToString(this.ClientID);
        gvBaseLeaseTerm.EditIndex = -1;
        BindBasicLeaseTermTable(chkParent);
        BindCommissionableTermTable(chkParent);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "gvBaseLeaseTerm_RowCancelingEdit", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    protected void gvBaseLeaseTerm_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        { 
        Savechanges();

        chkParent = Convert.ToString(this.ClientID);
        GridViewRow gRow = gvBaseLeaseTerm.Rows[e.RowIndex];
        gvBaseLeaseTerm.EditIndex = -1;

        if (gRow != null)
        {
            //TextBox txtEditLeaseTermYears = ((TextBox)gRow.FindControl("txtEditLeaseTermYears"));
            //TextBox txtEditFromRentPeriod = ((TextBox)gRow.FindControl("txtEditFromRentPeriod"));
            //TextBox txtEditToRentPeriod = ((TextBox)gRow.FindControl("txtEditToRentPeriod"));
            //    Label lbEditTotMonths = 

            // Label lbItemLeaseTermYears = ((Label)gRow.FindControl("lbItemLeaseTermYears"));
            TextBox lbItemFromRentPeriod = ((TextBox)gRow.FindControl("lbItemFromRentPeriod"));
            TextBox lbItemToRentPeriod = ((TextBox)gRow.FindControl("lbItemToRentPeriod"));
            TextBox txtItemFromRentPeriod = ((TextBox)gRow.FindControl("txtItemFromRentPeriod"));
            TextBox txtItemToRentPeriod = ((TextBox)gRow.FindControl("txtItemToRentPeriod"));
            TextBox txtEditRentPSP = ((TextBox)gRow.FindControl("txtEditRentPSP"));
            TextBox txtEditAnnualRent = ((TextBox)gRow.FindControl("txtEditAnnualRent"));
            Label lbPerIncrese = ((Label)gRow.FindControl("lbPerIncrese"));
            Label lbPeriodRent = ((Label)gRow.FindControl("lbPeriodRent"));
            Label lbTotSF = ((Label)gRow.FindControl("lbTotSF"));
            Int16 TotalMonths = 0;

            dtBasicLeaseTerm = GetRelativeDataTable(chkParent, cns_dtBasicLeaseTerm);

            //for (int i = 0; i < dtBasicLeaseTerm.Rows.Count; i++)
            //{
            //    if (e.RowIndex == e.RowIndex)
            //    {
            dRow = dtBasicLeaseTerm.Rows[e.RowIndex];
            dRow.BeginEdit();
            // dRow["LeaseTermYears"] = Convert.ToDecimal(lbItemLeaseTermYears.Text);
            if (txtItemFromRentPeriod != null && txtItemFromRentPeriod.ToString() != "")
            {
                dRow["RentFromPeriod"] = Convert.ToInt32(txtItemFromRentPeriod.Text);
            }
            else
            {
                dRow["RentFromPeriod"] = Convert.ToInt32(lbItemFromRentPeriod.Text);
            }

            if (txtItemToRentPeriod != null && txtItemToRentPeriod.ToString() != "")
            {
                dRow["RentToPeriod"] = Convert.ToInt32(txtItemToRentPeriod.Text);
            }
            else
            {
                dRow["RentToPeriod"] = Convert.ToInt32(lbItemToRentPeriod.Text);
            }

            //TotalMonths = Convert.ToInt16(Convert.ToInt16(lbItemToRentPeriod.Text) - Convert.ToInt16(lbItemFromRentPeriod.Text) + 1);
            TotalMonths = Convert.ToInt16(Convert.ToInt16(dRow["RentToPeriod"]) - Convert.ToInt16(dRow["RentFromPeriod"]) + 1);
            dRow["TotalMonths"] = TotalMonths;
            dRow["Rent_PSF"] = txtEditRentPSP.Text;
            dRow["AnnualRent"] = txtEditAnnualRent.Text;
            dRow["PerIncrease"] = lbPerIncrese.Text;
            dRow["PeriodRent"] = TotalMonths * Convert.ToDecimal(txtEditAnnualRent.Text) / 12;
            //  dRow["PeriodRent"] = Convert.ToDecimal(lbItemLeaseTermYears.Text) * Convert.ToDecimal(txtEditAnnualRent.Text);
            dRow["TotalSF"] = lbTotSF.Text;
            dRow["TermPeriod"] = txtLeaseTermMonths.Text == "" ? 0 : Convert.ToInt32(txtLeaseTermMonths.Text);
            //   dRow["LeaseTermId"] = 0;
            decimal RentDiff = 0;

            if (e.RowIndex > 0)
            {
                DataRow drowprev;
                drowprev = dtBasicLeaseTerm.Rows[e.RowIndex - 1];
                decimal PrevRent = Convert.ToDecimal(drowprev["AnnualRent"].ToString());
                decimal CurRent = Convert.ToDecimal(dRow["AnnualRent"].ToString());
                RentDiff = Convert.ToDecimal(((CurRent - PrevRent) / (PrevRent)) * 100);
                RentDiff = Convert.ToDecimal(String.Format("{0:n2}", RentDiff));
                dRow["PerIncrease"] = RentDiff;
            }
            dRow.AcceptChanges();
            //        break;
            //    }
            //}
            gvBaseLeaseTerm.DataSource = dtBasicLeaseTerm;
            gvBaseLeaseTerm.DataBind();
            Session.Add(cns_dtBasicLeaseTerm + SetRelativeTable(chkParent), dtBasicLeaseTerm);

            if (e.RowIndex == (gvBaseCommTerms.Rows.Count - 1) && ((System.Web.UI.WebControls.CompositeDataBoundControl)(sender)).ClientID == "ContentMain_ucBaseLeaseTerms_gvBaseLeaseTerm")
            {
                if (this.Parent.FindControl("hdnleaseLast") != null)
                {
                    HiddenField hdnleaseLast2 = (HiddenField)this.Parent.FindControl("hdnleaseLast");
                    hdnleaseLast2.Value = txtEditAnnualRent.Text;
                    ((UpdatePanel)this.Parent.FindControl("upBaseLeaseTerms")).Update();
                    //  this.Page.GetType().InvokeMember("UpdateContANdOptnGrid", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);

                }

            }

        }

        dtBasicLeaseTerm = GetRelativeDataTable(chkParent, cns_dtBasicLeaseTerm);

            //chkParent = Convert.ToString(this.ClientID);
            ////GridViewRow gRow = gvBaseCommTerms.Rows[e.RowIndex];
            //gRow = gvBaseCommTerms.Rows[e.RowIndex];
            //if (gRow != null)
            //{
            //    TextBox txtEditBrokerCommission = (TextBox)gRow.FindControl("txtEditBrokerCommission");
            //    TextBox txtEditFee = (TextBox)gRow.FindControl("txtEditFee");
            //    dtCommTerm = GetRelativeDataTable(chkParent, cns_dtCommTerm);
            //    dRow = dtCommTerm.Rows[e.RowIndex];
            //    dRow.BeginEdit();
            //    dRow["BrokerCommission"] = txtEditBrokerCommission.Text;
            //    dRow["Fee"] = txtEditFee.Text;
            //    dRow["CommTermPeriod"] = txtBaseCommTermMonths.Text == "" ? 0 : Convert.ToInt32(txtBaseCommTermMonths.Text);
            //    dRow.AcceptChanges();
            //    Session.Add(cns_dtCommTerm + SetRelativeTable(chkParent), dtCommTerm);
            //    BindCommissionableTermTable(chkParent);
            //}
            ////gvBaseCommTerms_RowUpdating(sender, e);
            //// gvBaseCommTerms_RowUpdatingOnLeaseChange(chkParent, e.RowIndex);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "gvBaseLeaseTerm_RowUpdating", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }



    //protected void gvBaseCommTerms_RowUpdatingOnLeaseChange(string chkParent, int RowIndex)
    //{ 
    //    //chkParent = Convert.ToString(this.ClientID);
    //    GridViewRow gRow = gvBaseCommTerms.Rows[RowIndex];
    //    // gvBaseCommTerms.EditIndex = -1;
    //    if (gRow != null)
    //    {
    //        Label lbItemBrokerCommission = (Label)gRow.FindControl("lbItemBrokerCommission");
    //        Label lbItemFee = (Label)gRow.FindControl("lbItemFee");

    //        dtCommTerm = GetRelativeDataTable(chkParent, cns_dtCommTerm);
    //        dRow = dtCommTerm.Rows[RowIndex];
    //        dRow.BeginEdit();
    //        dRow["BrokerCommission"] = lbItemBrokerCommission.Text;
    //        dRow["Fee"] = lbItemFee.Text;
    //        // dRow["CommTermPeriod"] = txtBaseCommTermMonths.Text == "" ? 0 : Convert.ToInt32(txtBaseCommTermMonths.Text);
    //        dRow.AcceptChanges();
    //        Session.Add(cns_dtCommTerm + SetRelativeTable(chkParent), dtCommTerm);
    //        BindCommissionableTermTable(chkParent);
    //    }
    //}


    //JAY COMMENTED
    //protected void gvBaseCommTerms_RowEditing(object snder, GridViewEditEventArgs e)
    //{
    //    chkParent = Convert.ToString(this.ClientID);
    //    gvBaseCommTerms.EditIndex = e.NewEditIndex;
    //    BindCommissionableTermTable(chkParent);
    //}

    //protected void gvBaseCommTerms_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    //{
    //    chkParent = Convert.ToString(this.ClientID);
    //    gvBaseCommTerms.EditIndex = -1;
    //    BindCommissionableTermTable(chkParent);
    //}

    //protected void gvBaseCommTerms_RowUpdating(object sender, GridViewUpdateEventArgs e)
    //{
    //    chkParent = Convert.ToString(this.ClientID);
    //    GridViewRow gRow = gvBaseCommTerms.Rows[e.RowIndex];
    //    gvBaseCommTerms.EditIndex = -1;
    //    if (gRow != null)
    //    {
    //        TextBox txtEditBrokerCommission = (TextBox)gRow.FindControl("txtEditBrokerCommission");
    //        TextBox txtEditFee = (TextBox)gRow.FindControl("txtEditFee");

    //        dtCommTerm = GetRelativeDataTable(chkParent, cns_dtCommTerm);
    //        dRow = dtCommTerm.Rows[e.RowIndex];
    //        dRow.BeginEdit();
    //        dRow["BrokerCommission"] = txtEditBrokerCommission.Text;
    //        dRow["Fee"] = txtEditFee.Text;
    //        dRow["CommTermPeriod"] = txtBaseCommTermMonths.Text == "" ? 0 : Convert.ToInt32(txtBaseCommTermMonths.Text);
    //        dRow.AcceptChanges();
    //        Session.Add(cns_dtCommTerm + SetRelativeTable(chkParent), dtCommTerm);
    //        BindCommissionableTermTable(chkParent);
    //    }
    //}

    //protected void gvBaseCommTerms_Bind(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.Header)
    //    {
    //        e.Row.Cells[5].Visible = false;
    //    }
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        e.Row.Cells[5].Visible = false;
    //        if ((Label)e.Row.FindControl("lbItemFee") != null)
    //        {
    //            totCommFee += Convert.ToDecimal(((Label)e.Row.FindControl("lbItemFee")).Text);
    //            totCommTermPeriod += Convert.ToInt32(((Label)e.Row.FindControl("lbItemTotMonths")).Text);
    //        }
    //        //TextBox txtBrokerCommission = (TextBox)e.Row.FindControl("lbItemBrokerCommission");
    //        //TextBox txtFee = (TextBox)e.Row.FindControl("lbItemFee");
    //        //if (txtBrokerCommission != null && txtFee != null)
    //        //{
    //        //    txtBrokerCommission.Attributes.Add("onchange", "javascript:return CalBrokerPer('" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.ClientID + "')");
    //        //    txtFee.Attributes.Add("onchange", "javascript:return CalBrokerFee('" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.ClientID + "')");
    //        //}


    //    }
    //    if (e.Row.RowType == DataControlRowType.Footer)
    //    {
    //        e.Row.Cells[5].Visible = false;
    //        Label lb = (Label)e.Row.FindControl("lblTotFee");
    //        ((Label)e.Row.FindControl("lblTotFee")).Text = totCommFee.ToString("N2");
    //        int x = rnd.Next(100, 200);
    //        //lb.Attributes.Add("onchange", "javascript:test1();");
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "ScriptCoom" + x, "javascript:SetSummary('" + totCommTermPeriod.ToString() + "','" + this.ID + "','Comm');", true);
    //        ViewState["CommTermPeriod"] = totCommTermPeriod;
    //        Session[GlobleData.TotalRent] = totRent;
    //    }
    //}

    //protected void gvBaseCommTerms_DataBound(object sender, EventArgs e)
    //{
    //    if (gvBaseCommTerms.EditIndex != -1)
    //    {
    //        GridViewRow editRow = gvBaseCommTerms.Rows[gvBaseCommTerms.EditIndex];

    //        //TextBox txtEditFromRentPeriod = (TextBox)editRow.FindControl("txtEditFromRentPeriod");
    //        //TextBox txtEditToRentPeriod = (TextBox)editRow.FindControl("txtEditToRentPeriod");

    //        //txtEditFromRentPeriod.Attributes.Add("onchange", "javascript:return SetRent('" + gvBaseCommTerms.ClientID + "')");
    //        //txtEditToRentPeriod.Attributes.Add("onchange", "javascript:return SetRent('" + gvBaseCommTerms.ClientID + "')");

    //        TextBox txtBrokerCommission = (TextBox)editRow.FindControl("txtEditBrokerCommission");
    //        TextBox txtFee = (TextBox)editRow.FindControl("txtEditFee");

    //        txtBrokerCommission.Attributes.Add("onchange", "javascript:return CalBrokerPer('" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.ClientID + "','" + gvBaseCommTerms.EditIndex + "','" + this.ClientID + "')");
    //        txtFee.Attributes.Add("onchange", "javascript:return CalBrokerFee('" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.ClientID + "','" + gvBaseCommTerms.EditIndex + "','" + this.ClientID + "')");
    //    }
    //}




    #region New Grid

    protected void gvbasecommtermsedit_Bind(object sender, GridViewRowEventArgs e)
    {
        try
        { 
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[4].Visible = false;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[4].Visible = false;
            if ((TextBox)e.Row.FindControl("txtEditFee") != null)
            {
                totCommFee += Convert.ToDecimal(((TextBox)e.Row.FindControl("txtEditFee")).Text);
                totCommTermPeriod += Convert.ToInt32(((Label)e.Row.FindControl("lbItemTotMonths")).Text);
            }
            //if (hdnCommissionTypeId != null)
            //{
            //    if (hdnCommissionTypeId.Value == "2" || hdnCommissionTypeId.Value == "3")
            //    {
            //        if ((TextBox)e.Row.FindControl("txtEditBrokerCommission") != null)
            //        {
            //            ((TextBox)e.Row.FindControl("txtEditBrokerCommission")).Text = "0";
            //        }
            //    }
            //}

            //TextBox txtBrokerCommission = (TextBox)e.Row.FindControl("lbItemBrokerCommission");
            //TextBox txtFee = (TextBox)e.Row.FindControl("lbItemFee");
            //if (txtBrokerCommission != null && txtFee != null)
            //{
            //    txtBrokerCommission.Attributes.Add("onchange", "javascript:return CalBrokerPer('" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.ClientID + "')");
            //    txtFee.Attributes.Add("onchange", "javascript:return CalBrokerFee('" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.ClientID + "')");
            //}
            int CommissionType = ddlCommissionType.SelectedValue == "" ? 1 : Convert.ToInt32(ddlCommissionType.SelectedValue);
            if (e.Row.FindControl("txtEditBrokerCommission") != null && e.Row.FindControl("txtEditFee") != null)
            {
                TextBox txtBrokerCommission = (TextBox)e.Row.FindControl("txtEditBrokerCommission");
                TextBox txtFee = (TextBox)e.Row.FindControl("txtEditFee");

                txtBrokerCommission.Attributes.Add("onchange", "javascript:return CalBrokerPer('" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.ClientID + "','" + e.Row.RowIndex + "','" + this.ClientID + "'," + CommissionType + ")");
                txtFee.Attributes.Add("onchange", "javascript:return CalBrokerFee('" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.ClientID + "','" + e.Row.RowIndex + "','" + this.ClientID + "'," + CommissionType + ")");
            }
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[4].Visible = false;
            Label lb = (Label)e.Row.FindControl("lblTotFee");
            ((Label)e.Row.FindControl("lblTotFee")).Text = totCommFee.ToString("N2");
            int x = rnd.Next(100, 200);
            //lb.Attributes.Add("onchange", "javascript:test1();");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ScriptCoom" + x, "javascript:SetSummary('" + totCommTermPeriod.ToString() + "','" + this.ID + "','Comm');", true);
            ViewState["CommTermPeriod"] = totCommTermPeriod;
            Session[GlobleData.TotalRent] = totRent;
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "gvbasecommtermsedit_Bind", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    protected void gvbasecommtermsedit_DataBound(object sender, EventArgs e)
    {
        try
        { 
        if (gvBaseCommTerms.EditIndex != -1)
        {
            GridViewRow editRow = gvBaseCommTerms.Rows[gvBaseCommTerms.EditIndex];

            //TextBox txtEditFromRentPeriod = (TextBox)editRow.FindControl("txtEditFromRentPeriod");
            //TextBox txtEditToRentPeriod = (TextBox)editRow.FindControl("txtEditToRentPeriod");

            //txtEditFromRentPeriod.Attributes.Add("onchange", "javascript:return SetRent('" + gvBaseCommTerms.ClientID + "')");
            //txtEditToRentPeriod.Attributes.Add("onchange", "javascript:return SetRent('" + gvBaseCommTerms.ClientID + "')");

            TextBox txtBrokerCommission = (TextBox)editRow.FindControl("txtEditBrokerCommission");
            TextBox txtFee = (TextBox)editRow.FindControl("txtEditFee");

            txtBrokerCommission.Attributes.Add("onchange", "javascript:return CalBrokerPer('" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.ClientID + "','" + gvBaseCommTerms.EditIndex + "','" + this.ClientID + "')");
            txtFee.Attributes.Add("onchange", "javascript:return CalBrokerFee('" + gvBaseCommTerms.ClientID + "','" + gvBaseLeaseTerm.ClientID + "','" + gvBaseCommTerms.EditIndex + "','" + this.ClientID + "')");
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "gvbasecommtermsedit_DataBound", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }


    #endregion
    #endregion

    #region Button Events

    protected void btnAddLeaseTerm_Click(object sender, EventArgs e)
    {
        try
        { 
        chkParent = Convert.ToString(btnAddLeaseTerm.ClientID);
        AddNewRecordToBasicLeaseTerm(chkParent);

        //if (chkParent.Contains("ucOptionalTerms"))
        //{
        //    CheckBox chkOptPayable = (CheckBox)(this.Parent.FindControl("chkOptionTermPayable"));
        //    if (chkOptPayable.Checked)
        //    {
        //        AddNewRecordToCommissionableTermSameAsLeaseTerm(chkParent);
        //    }
        //}
        //else
        //{
        //    AddNewRecordToCommissionableTermSameAsLeaseTerm(chkParent);
        //}
        AddNewRecordToCommissionableTermSameAsLeaseTerm(chkParent);
        ResetNewFields();
        //   Page.ClientScript.RegisterStartupScript(Page.GetType(), "Chk", "javascript:test1();", true);
        ScriptManager.RegisterStartupScript(this, Page.GetType(), "display", "EnableDisableOnCheckChange1();", true);
        txtBaseCommTermMonths.Focus();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "btnAddLeaseTerm_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    protected void btnAddCommissionableTerm_Click(object sender, EventArgs e)
    {
        try
        { 
        chkParent = Convert.ToString(btnAddLeaseTerm.ClientID);
        if (chkSameCommTerm.Checked == true)
        {
            AddNewRecordToCommissionableTermSameAsLeaseTerm(chkParent);
        }
        else
        {
            AddNewRecordToCommissionableTerm(chkParent);
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "btnAddCommissionableTerm_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    protected void gvBaseLeaseTerm_DelbuttonClick(object sender, EventArgs e)
    {
        try
        { 
        chkParent = Convert.ToString(this.ClientID);
        ImageButton lnkbtn = sender as ImageButton;
        //getting particular row linkbutton
        GridViewRow gvRow = lnkbtn.NamingContainer as GridViewRow;
        dtBasicLeaseTerm = GetRelativeDataTable(chkParent, cns_dtBasicLeaseTerm);
        dtBasicLeaseTerm.Rows[gvRow.RowIndex].Delete();
        dtBasicLeaseTerm.AcceptChanges();
        Session.Add(cns_dtBasicLeaseTerm + SetRelativeTable(chkParent), dtBasicLeaseTerm);
        BindBasicLeaseTermTable(chkParent);

        // 
        // Delete respective row from Base Commissionable Grid

        dtCommTerm = GetRelativeDataTable(chkParent, cns_dtCommTerm);
        if (dtCommTerm.Rows.Count != 0 && (dtCommTerm.Rows.Count - 1) >= gvRow.RowIndex)
        {
            dtCommTerm.Rows[gvRow.RowIndex].ToString();
            dtCommTerm.Rows[gvRow.RowIndex].Delete();
            dtCommTerm.AcceptChanges();
            Session.Add(cns_dtCommTerm + SetRelativeTable(chkParent), dtCommTerm);
            BindCommissionableTermTable(chkParent);

            int RowIndx = gvBaseLeaseTerm.Rows.Count - 1;

            // Add New Month Range
            //int NewFromMonth = Convert.ToInt32(((TextBox)gvBaseLeaseTerm.Rows[RowIndx].Cells[0].FindControl("lbItemToRentPeriod")).Text) + 1;
            //int LeaseMonthRange = txtLeaseTermMonths.Text == "" ? 0 : Convert.ToInt32(txtLeaseTermMonths.Text);
            //if (LeaseMonthRange >= NewFromMonth)
            //{
            //    hdnNewFromMonth.Value = NewFromMonth.ToString();
            //}
            //else
            //{
            //    hdnNewFromMonth .Value= "";
            //}
        }

        //JAY
        //int gvCnt = gvBaseCommTerms.Rows.Count;
        //Page.ClientScript.RegisterStartupScript(GetType(), "UpdateDueDate", "javascript:UpdateDueDate('" + this.ClientID + "','" + 0 + "');", true);
        ScriptManager.RegisterStartupScript(upBaseLeaseTerm, this.GetType(), "ScriptSumm", "javascript:UpdateDueDate('" + this.ClientID + "','" + 0 + "');", true);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "gvBaseLeaseTerm_DelbuttonClick", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    //protected void gvBaseCommTerms_DelbuttonClick(object sender, EventArgs e)
    //{
    //    if (delRecodrs.Text != "" || hdnRowInx.Value != "")
    //    {
    //        int RowIndex = Convert.ToInt32(hdnRowInx.Value) - 1;
    //        chkParent = this.ClientID;
    //        dtCommTerm = GetRelativeDataTable(chkParent, cns_dtCommTerm);
    //        dtCommTerm.Rows[RowIndex].Delete();
    //        Session.Add(cns_dtCommTerm + SetRelativeTable(chkParent), dtCommTerm);
    //        AddNewRecordToCommissionableTermSameAsLeaseTerm(chkParent);
    //    }
    //}

    //protected void btnFillBaseCommTermGrid_Click(object sender, EventArgs e)
    //{
    //    gvBaseCommTerms.DataSource = null;
    //    gvBaseCommTerms.DataBind();
    //    chkParent = Convert.ToString(btnAddLeaseTerm.ClientID);
    //    AddNewRecordToCommissionableTermSameAsLeaseTerm(chkParent);
    //}

    #endregion

    #region Check Box Events
    protected void chkBlankValue(object sender, EventArgs e)
    {
        try
        { 
        chkParent = Convert.ToString(btnAddLeaseTerm.ClientID);
        TextBox tb = sender as TextBox;
        ScriptManager.RegisterStartupScript(txtBaseCommPer, this.GetType(), "ScriptSumm", "javascript:CheckCommissionTypeValue('" + tb.ClientID + "');", true);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "chkBlankValue", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }


    protected void chkSameCommTerm_CheckChanged(object sender, EventArgs e)
    {
        try
        { 
        //    gvBaseCommTerms.DataSource = null;
        //    gvBaseCommTerms.DataBind();

        //JAY
        gvBaseCommTerms.DataSource = null;
        gvBaseCommTerms.DataBind();
        // HiddenField hdnCommissionType = new HiddenField();
        chkParent = Convert.ToString(btnAddLeaseTerm.ClientID);
        if (chkParent.Contains("ContentMain_ucBaseLeaseTerms"))
        {
            if (this.Parent.FindControl("hdnCommissionTypeBaseTerm") != null)
            {
                ((HiddenField)this.Parent.FindControl("hdnCommissionTypeBaseTerm")).Value = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
                Session["CommissionTypeBaseTerm"] = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
                if (ddlCommissionType.SelectedValue == "2" || ddlCommissionType.SelectedValue == "3")
                {
                    gvBaseCommTerms.Attributes.Add("style", "display:none");
                }
                else
                {
                    gvBaseCommTerms.Attributes.Add("style", "display:block");
                }

                //hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeBaseTerm");
                //hdnCommissionType.Value = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
            }
        }
        else if (chkParent.Contains("ContentMain_ucOptionalTerms"))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm") != null)
            {
                ((HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm")).Value = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
                Session["CommissionTypeOptionTerm"] = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
                if (ddlCommissionType.SelectedValue == "2" || ddlCommissionType.SelectedValue == "3")
                {
                    gvBaseCommTerms.Attributes.Add("style", "display:none");
                }
                else
                {
                    gvBaseCommTerms.Attributes.Add("style", "display:block");
                }

                //hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm");
                //hdnCommissionType.Value = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
            }
        }
        else if (chkParent.Contains("ContentMain_ucContingentCommission"))
        {
            if (this.Parent.FindControl("hdnCommissionTypeContingent") != null)
            {
                ((HiddenField)this.Parent.FindControl("hdnCommissionTypeContingent")).Value = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
                Session["CommissionTypeContingent"] = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
                if (ddlCommissionType.SelectedValue == "2" || ddlCommissionType.SelectedValue == "3")
                {
                    gvBaseCommTerms.Attributes.Add("style", "display:none");
                }
                else
                {
                    gvBaseCommTerms.Attributes.Add("style", "display:block");
                }

                //hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeContingent");
                //hdnCommissionType.Value = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
            }
        }
        else if (chkParent.Contains("ContentMain_ucOptional1Terms"))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm1") != null)
            {
                ((HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm1")).Value = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
                Session["CommissionTypeOptionTerm1"] = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
                if (ddlCommissionType.SelectedValue == "2" || ddlCommissionType.SelectedValue == "3")
                {
                    gvBaseCommTerms.Attributes.Add("style", "display:none");
                }
                else
                {
                    gvBaseCommTerms.Attributes.Add("style", "display:block");
                }

            }
        }
        else if (chkParent.Contains("ContentMain_ucOptional2Terms"))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm2") != null)
            {
                ((HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm2")).Value = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
                Session["CommissionTypeOptionTerm2"] = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
                if (ddlCommissionType.SelectedValue == "2" || ddlCommissionType.SelectedValue == "3")
                {
                    gvBaseCommTerms.Attributes.Add("style", "display:none");
                }
                else
                {
                    gvBaseCommTerms.Attributes.Add("style", "display:block");
                }

            }
        }
        else if (chkParent.Contains("ContentMain_ucOptional3Terms"))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm3") != null)
            {
                ((HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm3")).Value = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
                Session["CommissionTypeOptionTerm3"] = ddlCommissionType.SelectedValue == "" ? "1" : ddlCommissionType.SelectedValue;
                if (ddlCommissionType.SelectedValue == "2" || ddlCommissionType.SelectedValue == "3")
                {
                    gvBaseCommTerms.Attributes.Add("style", "display:none");
                }
                else
                {
                    gvBaseCommTerms.Attributes.Add("style", "display:block");
                }

            }
        }

        AddNewRecordToCommissionableTermSameAsLeaseTerm(chkParent);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "chkSameCommTerm_CheckChanged", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

        //gvBaseCommTerms.Attributes.Add("style", "display:none");
    }

    #endregion

    #region Custom Actions

    private void AddNewRecordToBasicLeaseTerm(string Parent)
    {
        try
        { 
        // Perform Validations
        //txtRentPSP.Text = Convert.ToString(ViewState["Rent_PSF"]);
        //txtAnnualRent.Text = Convert.ToString(ViewState["AnnualRent"]);
        //To Perfor Calulations Of Other GridView Columns
        if (!(string.IsNullOrEmpty(txtFromRentPeriod.Text) && string.IsNullOrEmpty(txtToRentPeriod.Text) && string.IsNullOrEmpty(txtRentPSP.Text) && string.IsNullOrEmpty(txtAnnualRent.Text)))
        {
            // To Calculate Total Months
            //If Rent Period Entered In Months 
            decimal TOTALMONTHS = 0;
            if (Convert.ToDecimal(txtToRentPeriod.Text) > Convert.ToDecimal(txtFromRentPeriod.Text))
            {
                TOTALMONTHS = (Convert.ToDecimal(txtToRentPeriod.Text) - Convert.ToDecimal(txtFromRentPeriod.Text) + 1);
            }
            else if (Convert.ToDecimal(txtToRentPeriod.Text) == Convert.ToDecimal(txtFromRentPeriod.Text))
            {
                TOTALMONTHS = 1;
            }
            dtBasicLeaseTerm = GetRelativeDataTable(Parent, cns_dtBasicLeaseTerm);
            int rowcount = dtBasicLeaseTerm.Rows.Count;
            dRow = dtBasicLeaseTerm.NewRow();
            // dRow["LeaseTermYears"] = Convert.ToDecimal(txtLeaseTermYears.Text);
            dRow["RentFromPeriod"] = Convert.ToInt32(txtFromRentPeriod.Text);
            dRow["RentToPeriod"] = Convert.ToInt32(txtToRentPeriod.Text);
            dRow["TotalMonths"] = TOTALMONTHS;
            dRow["Rent_PSF"] = txtRentPSP.Text;
            dRow["AnnualRent"] = Convert.ToDecimal(txtAnnualRent.Text);// Convert.ToDecimal(txtRentPSP.Text) * Convert.ToDecimal(ViewState["TotalSF"]);                                     // txtAnnualRent.Text;
            if (Parent.Contains(cns_ucBaseLeaseTerms))
            {
                if (rowcount > 0)
                    dRow["PerIncrease"] = txtPerIncrese.Text;
                else
                    dRow["PerIncrease"] = 0.0;
            }
            else
            {
                dRow["PerIncrease"] = txtPerIncrese.Text;
            }
            //dRow["PeriodRent"] = (Convert.ToInt32(txtToRentPeriod.Text) * Convert.ToDecimal(txtAnnualRent.Text)) / 12;
            dRow["PeriodRent"] = TOTALMONTHS * Convert.ToDecimal(txtAnnualRent.Text) / 12;
            dRow["TotalSF"] = ViewState["TotalSF"];

            dRow["LeaseTermId"] = 0;
            dRow["TermPeriod"] = txtLeaseTermMonths.Text == "" ? 0 : Convert.ToInt32(txtLeaseTermMonths.Text);
            //dRow["RowID"] =Convert.ToString(dtBasicLeaseTerm.Rows.Count - 1);
            dtBasicLeaseTerm.Rows.Add(dRow);
            if (dtBasicLeaseTerm.Rows.Count > 0)
            {
                gvBaseLeaseTerm.DataSource = dtBasicLeaseTerm;
                gvBaseLeaseTerm.DataBind();
                string cns = cns_dtBasicLeaseTerm + SetRelativeTable(Parent);
                Session.Add(cns, dtBasicLeaseTerm);

                // Add New Month Range
                //int NewFromMonth = Convert.ToInt32(txtToRentPeriod.Text) + 1;
                //int LeaseMonthRange = txtLeaseTermMonths.Text == "" ? 0 : Convert.ToInt32(txtLeaseTermMonths.Text);
                //if (LeaseMonthRange >= NewFromMonth)
                //{
                //    hdnNewFromMonth.Value = NewFromMonth.ToString();
                //}
                //else
                //{
                //    hdnNewFromMonth.Value = "";
                //}
                // txtFromRentPeriod.Text = 
            }
        }
        if (Parent.Contains(cns_ucBaseLeaseTerms))
        {
            if (this.Parent.FindControl("hdnleaseLast") != null)
            {
                HiddenField hdnleaseLast1 = (HiddenField)this.Parent.FindControl("hdnleaseLast");
                hdnleaseLast1.Value = txtAnnualRent.Text;
            }
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "AddNewRecordToBasicLeaseTerm", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private void AddNewRecordToCommissionableTermSameAsLeaseTerm(string Parent)
    {
        try
        { 
        decimal totamount = 0;
        // If Chaecked=true then BASE COMMISSIONABLE TERM are same as that of BASE LEASE TERM
        dtBasicLeaseTerm = GetRelativeDataTable(Parent, cns_dtBasicLeaseTerm);
        if (dtBasicLeaseTerm.Rows.Count != 0)
        {
            if (Session[cns_dtCommTerm] != null)
            {
                dtCommTerm = (DataTable)Session[cns_dtCommTerm];
            }
            else
            {
                CreateCommissionableTermTable();
            }
            //test 
            dtCommTerm = GetRelativeDataTable(Parent, cns_dtCommTerm);
            //dtCommTerm.Rows.Clear();
            int i;
            for (i = 0; i < dtBasicLeaseTerm.Rows.Count; i++)
            {
                int CountList = dtBasicLeaseTerm.Rows.Count;
                DataRow dr = dtBasicLeaseTerm.Rows[i];
                int ToRentPeriod = Convert.ToInt32(dr["RentToPeriod"]);
                int FromRentPeriod = Convert.ToInt32(dr["RentFromPeriod"]);

                int ToRentPeriodComm = 0;
                if (i < dtCommTerm.Rows.Count)
                {
                    ToRentPeriodComm = Convert.ToInt32(dtCommTerm.Rows[i]["RentToPeriod"]);
                }
                //else
                //{
                //    ToRentPeriodComm = ToRentPeriod;
                //}

                int BaseCommTermMonths = txtBaseCommTermMonths.Text == "" ? 0 : Convert.ToInt32(txtBaseCommTermMonths.Text);
                decimal CommomBaseComissionPer = txtBaseCommPer.Text == "" ? 0 : Convert.ToDecimal(txtBaseCommPer.Text);
                int BaseTermMonths = txtLeaseTermMonths.Text == "" ? 0 : Convert.ToInt32(txtLeaseTermMonths.Text);
                int CommissionType = ddlCommissionType.SelectedValue == "" ? 1 : Convert.ToInt32(ddlCommissionType.SelectedValue);
                if (ddlCommissionType.SelectedValue == "2")
                {
                    lblComAmountSquareFoot.Visible = true;
                    lblCommissionAmountSquareFoot.Visible = true;
                    lblComAmountSquareFoot.Text = "$" + String.Format("{0:n2}", (CommomBaseComissionPer * Convert.ToDecimal(dr["TotalSF"].ToString())));
                    hdnTotalSquareFoot.Value = dr["TotalSF"].ToString();

                }
                else
                {
                    lblComAmountSquareFoot.Visible = false;
                    lblCommissionAmountSquareFoot.Visible = false;
                }
                if (BaseCommTermMonths <= ToRentPeriod && BaseTermMonths > BaseCommTermMonths && BaseCommTermMonths != 0)
                {
                    dtCommTerm.Rows.Clear();
                    if (ViewState["tempComm"] != null)
                    {
                        dtCommTerm = (DataTable)ViewState["tempComm"];
                    }
                    if (FromRentPeriod <= BaseCommTermMonths)
                    {
                        dRow = dtCommTerm.NewRow();
                        dRow["RentFromPeriod"] = dr["RentFromPeriod"];
                        dRow["RentToPeriod"] = Convert.ToInt32(txtBaseCommTermMonths.Text);
                        dRow["TotalMonths"] = BaseCommTermMonths - Convert.ToInt32(dr["RentFromPeriod"]) + 1;
                        dRow["BrokerCommission"] = CommomBaseComissionPer;
                        //dRow["Fee"] = CommomBaseComissionPer * Convert.ToDecimal(dr["PeriodRent"].ToString()) / 100;
                        if (CommissionType == 1)
                        {
                            dRow["BrokerCommission"] = CommomBaseComissionPer;
                            dRow["Fee"] = CommomBaseComissionPer * Convert.ToDecimal(dr["PeriodRent"].ToString()) / 100;
                        }
                        else if (CommissionType == 2)
                        {
                            dRow["Fee"] = (CommomBaseComissionPer * Convert.ToDecimal(dr["TotalSF"].ToString())) / CountList;
                        }
                        else if (CommissionType == 3)
                        {
                            dRow["Fee"] = CommomBaseComissionPer / CountList;
                        }

                        dRow["CommissionableTermId"] = 0;
                        dRow["CommTermPeriod"] = txtBaseCommTermMonths.Text == "" ? 0 : Convert.ToInt32(txtBaseCommTermMonths.Text);
                        dtCommTerm.Rows.Add(dRow);
                        totamount = totamount + Convert.ToDecimal(dRow["Fee"].ToString());
                    }

                    break;
                }
                else if (ToRentPeriod != ToRentPeriodComm && BaseTermMonths >= BaseCommTermMonths && BaseCommTermMonths != 0)
                {
                    if (FromRentPeriod <= BaseCommTermMonths)
                    {
                        DataTable tempComm;
                        dtCommTerm.Rows.Clear();
                        if (ViewState["tempComm"] != null)
                        {
                            tempComm = (DataTable)ViewState["tempComm"];
                        }
                        else
                        {
                            tempComm = dtCommTerm.Clone();
                        }

                        dRow = tempComm.NewRow();
                        dRow["RentFromPeriod"] = dr["RentFromPeriod"];
                        dRow["RentToPeriod"] = dr["RentToPeriod"];
                        dRow["TotalMonths"] = dr["TotalMonths"];
                        dRow["BrokerCommission"] = CommomBaseComissionPer;
                        // dRow["Fee"] = CommomBaseComissionPer * Convert.ToDecimal(dr["PeriodRent"].ToString()) / 100;
                        if (CommissionType == 1)
                        {
                            dRow["Fee"] = CommomBaseComissionPer * Convert.ToDecimal(dr["PeriodRent"].ToString()) / 100;
                        }
                        else if (CommissionType == 2)
                        {
                            dRow["Fee"] = (CommomBaseComissionPer * Convert.ToDecimal(dr["TotalSF"].ToString())) / CountList;

                        }
                        else if (CommissionType == 3)
                        {
                            dRow["Fee"] = CommomBaseComissionPer / CountList;
                        }
                        dRow["CommissionableTermId"] = 0;
                        dRow["CommTermPeriod"] = txtBaseCommTermMonths.Text == "" ? 0 : Convert.ToInt32(txtBaseCommTermMonths.Text);
                        totamount = totamount + Convert.ToDecimal(dRow["Fee"].ToString());
                        tempComm.Rows.Add(dRow);
                        ViewState["tempComm"] = tempComm;
                        foreach (DataRow dr1 in tempComm.Rows)
                        {

                            dtCommTerm.Rows.Add(dr1.ItemArray);
                        }
                    }
                }
                else if (ToRentPeriod == ToRentPeriodComm)
                {
                    DataTable tempComm;

                    if (ViewState["tempComm"] != null)
                    {
                        tempComm = (DataTable)ViewState["tempComm"];
                    }
                    else
                    {
                        tempComm = dtCommTerm.Clone();
                    }
                    dRow = tempComm.NewRow();
                    dRow["RentFromPeriod"] = dr["RentFromPeriod"];
                    dRow["RentToPeriod"] = dr["RentToPeriod"];
                    dRow["TotalMonths"] = dr["TotalMonths"];
                    dRow["BrokerCommission"] = CommomBaseComissionPer;
                    //dRow["Fee"] = CommomBaseComissionPer * Convert.ToDecimal(dr["PeriodRent"].ToString()) / 100;
                    if (CommissionType == 1)
                    {
                        dRow["Fee"] = CommomBaseComissionPer * Convert.ToDecimal(dr["PeriodRent"].ToString()) / 100;
                    }
                    else if (CommissionType == 2)
                    {
                        dRow["Fee"] = (CommomBaseComissionPer * Convert.ToDecimal(dr["TotalSF"].ToString())) / CountList;
                    }
                    else if (CommissionType == 3)
                    {
                        dRow["Fee"] = CommomBaseComissionPer / CountList;
                    }

                    dRow["CommissionableTermId"] = dtCommTerm.Rows[i]["CommissionableTermId"].ToString();
                    dRow["CommTermPeriod"] = dtCommTerm.Rows[i]["CommTermPeriod"].ToString();
                    totamount = totamount + Convert.ToDecimal(dRow["Fee"].ToString());

                    tempComm.Rows.Add(dRow);
                    ViewState["tempComm"] = tempComm;
                    dtCommTerm.Rows.Clear();

                    foreach (DataRow dr1 in tempComm.Rows)
                    {
                        // totamount = totamount+Convert.ToDecimal(dr1["Fee"].ToString());
                        dtCommTerm.Rows.Add(dr1.ItemArray);
                    }

                }
                //if (BaseCommTermMonths < ToRentPeriod || BaseCommTermMonths == 0)
                //{
                //    dRow = dtCommTerm.NewRow();
                //    // dRow.BeginEdit();
                //    //dRow["LeaseTermYears"] = dr["LeaseTermYears"];
                //    dRow["RentFromPeriod"] = dr["RentFromPeriod"];
                //    dRow["RentToPeriod"] = dr["RentToPeriod"];
                //    dRow["TotalMonths"] = dr["TotalMonths"];
                //    dRow["BrokerCommission"] = 0.0;
                //    dRow["Fee"] = 0.0;
                //    dRow["CommissionableTermId"] = 0;
                //    dRow["CommTermPeriod"] = txtBaseCommTermMonths.Text == "" ? 0 : Convert.ToInt32(txtBaseCommTermMonths.Text);
                //    //dtCommTerm.AcceptChanges();
                //    dtCommTerm.Rows.Add(dRow);
                //}
                //else
                //{
                //    dRow = dtCommTerm.Rows[i];
                //    dRow.BeginEdit();
                //    //dRow["LeaseTermYears"] = dr["LeaseTermYears"];
                //    dRow["RentFromPeriod"] = dr["RentFromPeriod"];
                //    dRow["RentToPeriod"] = BaseCommTermMonths;
                //    dRow["TotalMonths"] = BaseCommTermMonths - Convert.ToInt32(dr["RentFromPeriod"]) + 1;
                //    //dRow["BrokerCommission"] = 0.0;
                //    //dRow["Fee"] = 0.0;
                //    //dRow["CommissionableTermId"] = 0;
                //    dRow["CommTermPeriod"] = txtBaseCommTermMonths.Text == "" ? 0 : Convert.ToInt32(txtBaseCommTermMonths.Text);
                //    dtCommTerm.AcceptChanges();
                //    //dtCommTerm.Rows.Add(dRow);
                //    //break;
                //}UpdateDueDate('" + Parent + "','" + totamount + "');

            }
            ViewState["tempComm"] = null;
            Session.Add(cns_dtCommTerm + SetRelativeTable(Parent), dtCommTerm);
            BindCommissionableTermTable(Parent);
            //System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "UpdateDueDate('" + Parent + "','" + totamount + "');", true);
            //Page.ClientScript.RegisterStartupScript(GetType(), "myScript", "alert();", true);
            ScriptManager.RegisterStartupScript(upBaseLeaseTerm, this.GetType(), "ScriptSumm", "javascript:UpdateDueDate('" + this.ClientID + "','" + totamount + "');", true);
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "AddNewRecordToCommissionableTermSameAsLeaseTerm", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private void AddNewRecordToCommissionableTerm(string Parent)
    {
        try
        { 
        // If NOT Chaecked=true then BASE COMMISSIONABLE TERM are same as that of BASE LEASE TERM

        //if (Session[cns_dtCommTerm] != null)
        //{
        //    dtCommTerm = (DataTable)Session[cns_dtCommTerm];
        //}
        //else
        //{
        //    CreateCommissionableTermTable();
        //}

        dtCommTerm = GetRelativeDataTable(Parent, cns_dtCommTerm);

        dRow = dtCommTerm.NewRow();
        // dRow["LeaseTermYears"] = Convert.ToDecimal(txtCommLeaseTerms.Text);
        dRow["RentFromPeriod"] = Convert.ToInt32(txtCommFromRentPeriod.Text);
        dRow["RentToPeriod"] = txtCommToRentPeriod.Text;
        dRow["TotalMonths"] = (Convert.ToInt32(txtCommToRentPeriod.Text) - Convert.ToInt32(txtCommFromRentPeriod.Text) + 1);
        dRow["BrokerCommission"] = Convert.ToDecimal(txtBrokerPer.Text);
        dRow["Fee"] = Convert.ToDecimal(txtBrokerFee.Text);
        dRow["CommissionableTermId"] = 0;
        dRow["CommTermPeriod"] = txtBaseCommTermMonths.Text == "" ? 0 : Convert.ToInt32(txtBaseCommTermMonths.Text);
        dtCommTerm.Rows.Add(dRow);
        Session.Add(cns_dtCommTerm + SetRelativeTable(Parent), dtCommTerm);
        BindCommissionableTermTable(Parent);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "AddNewRecordToCommissionableTerm", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private void BindBasicLeaseTermTable(string Parent)
    {
        try
        { 
        dtBasicLeaseTerm = GetRelativeDataTable(Parent, cns_dtBasicLeaseTerm);
        gvBaseLeaseTerm.DataSource = dtBasicLeaseTerm;
        gvBaseLeaseTerm.DataBind();
            //  Session.Add(cns_dtBasicLeaseTerm, dtBasicLeaseTerm);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "BindBasicLeaseTermTable", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private void BindCommissionableTermTable(string Parent)
    {
        try
        { 
        dtCommTerm = GetRelativeDataTable(Parent, cns_dtCommTerm);

        if (Parent.Contains(cns_ucBaseLeaseTerms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeBaseTerm") != null)
            {
                HiddenField hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeBaseTerm");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (Parent.Contains(cns_ucOptionalTerms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm") != null)
            {
                HiddenField hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (Parent.Contains(cns_ucContingentCommission))
        {
            if (this.Parent.FindControl("hdnCommissionTypeContingent") != null)
            {
                HiddenField hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeContingent");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (Parent.Contains(cns_ucOptional1Terms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm1") != null)
            {
                HiddenField hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm1");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (Parent.Contains(cns_ucOptional2Terms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm2") != null)
            {
                HiddenField hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm2");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (Parent.Contains(cns_ucOptional3Terms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm3") != null)
            {
                HiddenField hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm3");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        //if (hdnCommissionType != null)
        //{
        //    if (hdnCommissionType.Value == "2" || hdnCommissionType.Value == "3")
        //    {
        //        for (int i = 0; i < dtCommTerm.Rows.Count; i++)
        //        {
        //            dtCommTerm.Rows[i]["BrokerCommission"] = "0";
        //        }
        //    }
        //}
        //dtCommTerm.AcceptChanges();
        //gvBaseCommTerms.DataSource = dtCommTerm;
        //gvBaseCommTerms.DataBind();

        //JAY
        gvBaseCommTerms.DataSource = dtCommTerm;
        gvBaseCommTerms.DataBind();
            //  Session.Add(cns_dtBasicLeaseTerm, dtBasicLeaseTerm);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "BindCommissionableTermTable", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private DataTable GetRelativeDataTable(string Container, string tbName) // To get relative data table from session (for more than two user control of same type on single aspx page
    {
        try
        { 
        if (Container.Contains(cns_ucBaseLeaseTerms))
        {
            if (Session[tbName + cns_ucBaseLeaseTerms] != null)
            {
                dt = (DataTable)Session[tbName + cns_ucBaseLeaseTerms];
            }
            else
            {
                dt = null;
            }

        }
        else if (Container.Contains(cns_ucOptionalTerms))
        {
            if (Session[tbName + cns_ucOptionalTerms] != null)
            {
                dt = (DataTable)Session[tbName + cns_ucOptionalTerms];
            }
            else
            {
                dt = null;
            }
        }
        else if (Container.Contains(cns_ucContingentCommission))
        {
            if (Session[tbName + cns_ucContingentCommission] != null)
            {
                dt = (DataTable)Session[tbName + cns_ucContingentCommission];
            }
            else
            {
                dt = null;
            }
        }
        else if (Container.Contains(cns_ucOptional1Terms))
        {
            if (Session[tbName + cns_ucOptional1Terms] != null)
            {
                dt = (DataTable)Session[tbName + cns_ucOptional1Terms];
            }
            else
            {
                dt = null;
            }
        }
        else if (Container.Contains(cns_ucOptional2Terms))
        {
            if (Session[tbName + cns_ucOptional2Terms] != null)
            {
                dt = (DataTable)Session[tbName + cns_ucOptional2Terms];
            }
            else
            {
                dt = null;
            }
        }
        else if (Container.Contains(cns_ucOptional3Terms))
        {
            if (Session[tbName + cns_ucOptional3Terms] != null)
            {
                dt = (DataTable)Session[tbName + cns_ucOptional3Terms];
            }
            else
            {
                dt = null;
            }
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "GetRelativeDataTable", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            dt = null;
        }
        return dt;
    }

    private string SetRelativeTable(string Container)
    {
        if (Container.Contains(cns_ucBaseLeaseTerms))
        {
            return cns_ucBaseLeaseTerms;
        }
        else if (Container.Contains(cns_ucOptionalTerms))
        {
            return cns_ucOptionalTerms;
        }
        else if (Container.Contains(cns_ucContingentCommission))
        {
            return cns_ucContingentCommission;
        }
        else if (Container.Contains(cns_ucOptional1Terms))
        {
            return cns_ucOptional1Terms;
        }
        else if (Container.Contains(cns_ucOptional2Terms))
        {
            return cns_ucOptional2Terms;
        }
        else if (Container.Contains(cns_ucOptional3Terms))
        {
            return cns_ucOptional3Terms;
        }
        else
        {
            return "";
        }
    }

    private void SetAttributes()
    {
        try
        { 
        txtLeaseTermMonths.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtLeaseTermMonths.ClientID + "')");
        txtLeaseTermMonths.Attributes.Add("onchange", "javascript:return SetBaseCommTermMonths('" + this.ClientID + "')");


        //   txtBaseCommTermMonths.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtBaseCommTermMonths.ClientID + "')");
        txtBaseCommTermMonths.Attributes.Add("onchange", "javascript:SetToPeriodBaseCommTerm('" + this.ClientID + "');return true;");
        //  Page.ClientScript.RegisterStartupScript(this.GetType(), "click", "HideDelBtn('" + delRecodrs.ClientID + "');", true);
        btnDueDtAdd.Attributes.Add("onclick", "javascript:return addDueDateRecords('" + btnDueDates.ClientID + "','" + pnDueDt.ClientID + "','" + this.ID + "')");
        btnAddLeaseTerm.Attributes.Add("onclick", "javascript:return CheckLeaseMonths('" + this.ClientID + "','" + pnAddBaseLeaseTerm.ClientID + "')");
        btnClosePanel.Attributes.Add("onclick", "javascript:return CheckLeaseMonths('" + this.ClientID + "','" + pnAddBaseLeaseTerm.ClientID + "')");
        //  btnAddLeaseTerm.Attributes.Add("onclick", "javascript:return CheckLeaseMonths('" + pnAddBaseLeaseTerm.ClientID + "')");
        //btnAddCommTerm.Attributes.Add("onclick", "javascript:return ScrollToggle('" + pnAddBaseCommissionableTerm.ClientID + "')");

        btnAddLeaseTerm_done.Attributes.Add("onclick", "javascript:return ValidateBaseLeasePeriod('" + this.ClientID + "')");
        //    btnAddLeaseTerm_done.Attributes.Add("onclick", "javascript:return ScrollUp('" + pnAddBaseLeaseTerm.ClientID + "')return true;return ValidateBaseLeasePeriod('" + pnAddBaseLeaseTerm.ClientID + "');");
        btnAddBaseCommissionableTerm.Attributes.Add("onclick", "javascript:return ScrollUp('" + pnAddBaseCommissionableTerm.ClientID + "')");
        txtAddCommPerOnDueDt.Attributes.Add("onchange", "javascript:return SetCommissionAmount('" + this.ClientID + "')");

        txtRentPSP.Attributes.Add("onchange", "javascript:return CalculateRent('" + this.ClientID + "',1)");
        txtAnnualRent.Attributes.Add("onchange", "javascript:return CalculateRent('" + this.ClientID + "',2)");
        txtPerIncrese.Attributes.Add("onchange", "javascript:return CalAnnualRentDiffFromPercentage('" + this.ClientID + "')");
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "SetAttributes", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void SetTitles(string Title1, string Title2)
    {
        lbTermTitle.Text = Title1;
        lbCommTitle.Text = Title2;
    }

    private void FindDueDateTableControls(int Count, string Parent, int TermType)
    {
        try
        { 
        dtDueDtComm = (DataTable)Session[GlobleData.CommissionTb];
        DataRow dRow;
        Count = Count++;
        //if (hdnDueDateCount.Value != "")
        //{
        //    Count = Convert.ToInt16(hdnDueDateCount.Value) + 1;
        //}
        for (int i = 0; i <= Count; i++)
        {
            if (ControlValue(Parent + "_txtCommPerOnDueDt_" + i).Replace("%", "") != "")
            {
                dRow = dtDueDtComm.NewRow();
                dRow["PercentComm"] = Convert.ToDecimal(ControlValue(Parent + "_txtCommPerOnDueDt_" + i).Replace("%", ""));
                dRow["CommAmountDue"] = Convert.ToDecimal(ControlValue(Parent + "_txtCommAmtOnDueDt_" + i).Replace("%", ""));
                dRow["DueDate"] = ControlValue(Parent + "_lbDueDate_" + i);
                dRow["TermTypeId"] = TermType;
                dtDueDtComm.Rows.Add(dRow);
            }
        }
        Session[GlobleData.CommissionTb] = dtDueDtComm;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "FindDueDateTableControls", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private string ControlValue(string ctrlPrefix)
    {
        try
        { 
        string[] ctrls = Request.Form.ToString().Replace("%24", "_").Split('&');
        int cnt = FindOccurence(ctrlPrefix);
        if (cnt > 0)
        {
            for (int k = 1; k <= cnt; k++)
            {
                for (int i = 0; i < ctrls.Length; i++)
                {
                    if (ctrls[i].Contains(ctrlPrefix))
                    {
                        string ctrlName = ctrls[i].Split('=')[0];
                        string ctrlValue = ctrls[i].Split('=')[1];

                        //Decode the Value
                        ctrlValue = Server.UrlDecode(ctrlValue);
                        return ctrlValue;
                    }
                }
            }
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "ControlValue", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return "";
        }
        return "";
    }

    private int FindOccurence(string substr)
    {
        string reqstr = Request.Form.ToString().Replace("%24", "_");
        return ((reqstr.Length - reqstr.Replace(substr, "").Length)
                / substr.Length);
    }

    private void CreatedtDueDtCommTable(Boolean FinalSave)
    {
        if (FinalSave == true)//Session[GlobleData.CommissionTb] == null || 
        {
            dtDueDtComm = new DataTable();
            dtDueDtComm.Columns.Clear();
            dtDueDtComm.Columns.Add("PercentComm", typeof(decimal));
            dtDueDtComm.Columns.Add("CommAmountDue", typeof(decimal));
            dtDueDtComm.Columns.Add("DueDate", typeof(string));
            dtDueDtComm.Columns.Add("TermTypeId", typeof(int));
            Session.Add(GlobleData.CommissionTb, dtDueDtComm);

        }
    }

    private void SaveAllDueDateRecords()
    {
        try
        { 
        if (Session[GlobleData.CommissionTb] != null)
        {
            DataTable dt = (DataTable)Session[GlobleData.CommissionTb];

            Boolean OptChk = ViewState["OptPayable"] == null ? false : Convert.ToBoolean(ViewState["OptPayable"]);
            CommissionDueDates objCommissionDueDates = new CommissionDueDates();
            objCommissionDueDates.DealTransactionId = Convert.ToInt32(Session[GlobleData.DealTransactionId]);
            objCommissionDueDates.ChargeSlipId = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);

            //ADDED BY Jaywanti on 14-02-2014
            //Check CommissionDueDate already exist for particular chargeslip ..
            DataTable dtCommExist = new DataTable();
            dtCommExist.Columns.Add("ChargeSlipId", typeof(Int64));
            dtCommExist.Columns.Add("DueDate", typeof(DateTime));
            dtCommExist.Columns.Add("PercentComm", typeof(decimal));
            dtCommExist.Columns.Add("CommAmountDue", typeof(decimal));
            dtCommExist.Columns.Add("TermTypeId", typeof(Int16));
            //dtCommExist.Columns.Add("RowId", typeof(Int16));
            DataRow dr;
            if (dt.Rows.Count != 0)
            {
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    dr = dtCommExist.NewRow();
                    dr[0] = objCommissionDueDates.ChargeSlipId;
                    dr[1] = DateTime.ParseExact(dt.Rows[i]["DueDate"].ToString(), "MM/dd/yyyy", null);
                    dr[2] = Convert.ToDouble(dt.Rows[i]["PercentComm"].ToString());
                    dr[3] = Convert.ToDecimal(dt.Rows[i]["CommAmountDue"].ToString());
                    dr[4] = Convert.ToInt16(dt.Rows[i]["TermTypeId"]);
                    //dr[5] = Convert.ToInt16(dt.Rows[i]["RowId"].ToString());
                    dtCommExist.Rows.Add(dr);
                }
            }
            objCommissionDueDates.CommissiionDueDateCheckExist(dtCommExist);
            //if (dt.Rows.Count != 0)
            //{
            //    for (int i = 0; i <= dt.Rows.Count - 1; i++)
            //    {
            //        objCommissionDueDates.PercentComm = Convert.ToDouble(dt.Rows[i]["PercentComm"].ToString());
            //        objCommissionDueDates.CommAmountDue = Convert.ToDecimal(dt.Rows[i]["CommAmountDue"].ToString());
            //        objCommissionDueDates.DueDate = DateTime.ParseExact(dt.Rows[i]["DueDate"].ToString(), "MM/dd/yyyy", null);
            //        objCommissionDueDates.CommissionDueDateExist();
            //    }
            //}
            //END
            //if (Session[GlobleData.cns_IsFullCommPaid] != null)
            //{
            //    if (Convert.ToBoolean(Session[GlobleData.cns_IsFullCommPaid].ToString()) == false)
            //    {
            //        objCommissionDueDates.DeleteByChargeSlip();
            //        if (dt.Rows.Count != 0)
            //        {
            //            // CommissionDueDates objCommissionDueDates = new CommissionDueDates();
            //            // objCommissionDueDates.DealTransactionId = Convert.ToInt32(Session[GlobleData.DealTransactionId]);
            //            // objCommissionDueDates.ChargeSlipId = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
            //            // objCommissionDueDates.DeleteByChargeSlip();
            //            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            //            {
            //                if (Convert.ToInt16(dt.Rows[i]["TermTypeId"]) == 2)
            //                {
            //                    if (OptChk == true)
            //                    {
            //                        objCommissionDueDates.PercentComm = Convert.ToDouble(dt.Rows[i]["PercentComm"].ToString());
            //                        objCommissionDueDates.CommAmountDue = Convert.ToDecimal(dt.Rows[i]["CommAmountDue"].ToString());
            //                        objCommissionDueDates.DueDate = DateTime.ParseExact(dt.Rows[i]["DueDate"].ToString(), "MM/dd/yyyy", null);
            //                        objCommissionDueDates.TermTypeId = Convert.ToInt16(dt.Rows[i]["TermTypeId"]);
            //                        objCommissionDueDates.Save();
            //                        objCommissionDueDates.CommissionDueId = 0;
            //                    }
            //                }
            //                else
            //                {
            //                    objCommissionDueDates.PercentComm = Convert.ToDouble(dt.Rows[i]["PercentComm"].ToString());
            //                    objCommissionDueDates.CommAmountDue = Convert.ToDecimal(dt.Rows[i]["CommAmountDue"].ToString());
            //                    objCommissionDueDates.DueDate = DateTime.ParseExact(dt.Rows[i]["DueDate"].ToString(), "MM/dd/yyyy", null);
            //                    objCommissionDueDates.TermTypeId = Convert.ToInt16(dt.Rows[i]["TermTypeId"]);
            //                    objCommissionDueDates.Save();
            //                    objCommissionDueDates.CommissionDueId = 0;
            //                }
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    objCommissionDueDates.DeleteByChargeSlip();
            //    if (dt.Rows.Count != 0)
            //    {
            //        // CommissionDueDates objCommissionDueDates = new CommissionDueDates();
            //        // objCommissionDueDates.DealTransactionId = Convert.ToInt32(Session[GlobleData.DealTransactionId]);
            //        // objCommissionDueDates.ChargeSlipId = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
            //        // objCommissionDueDates.DeleteByChargeSlip();
            //        for (int i = 0; i <= dt.Rows.Count - 1; i++)
            //        {
            //            if (Convert.ToInt16(dt.Rows[i]["TermTypeId"]) == 2)
            //            {
            //                if (OptChk == true)
            //                {
            //                    objCommissionDueDates.PercentComm = Convert.ToDouble(dt.Rows[i]["PercentComm"].ToString());
            //                    objCommissionDueDates.CommAmountDue = Convert.ToDecimal(dt.Rows[i]["CommAmountDue"].ToString());
            //                    objCommissionDueDates.DueDate = DateTime.ParseExact(dt.Rows[i]["DueDate"].ToString(), "MM/dd/yyyy", null);
            //                    objCommissionDueDates.TermTypeId = Convert.ToInt16(dt.Rows[i]["TermTypeId"]);
            //                    objCommissionDueDates.Save();
            //                    objCommissionDueDates.CommissionDueId = 0;
            //                }
            //            }
            //            else
            //            {
            //                objCommissionDueDates.PercentComm = Convert.ToDouble(dt.Rows[i]["PercentComm"].ToString());
            //                objCommissionDueDates.CommAmountDue = Convert.ToDecimal(dt.Rows[i]["CommAmountDue"].ToString());
            //                objCommissionDueDates.DueDate = DateTime.ParseExact(dt.Rows[i]["DueDate"].ToString(), "MM/dd/yyyy", null);
            //                objCommissionDueDates.TermTypeId = Convert.ToInt16(dt.Rows[i]["TermTypeId"]);
            //                objCommissionDueDates.Save();
            //                objCommissionDueDates.CommissionDueId = 0;
            //            }
            //        }
            //    }
            //}
        }
        dtDueDtComm.Rows.Clear();
        Session[GlobleData.CommissionTb] = dtDueDtComm;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "SaveAllDueDateRecords", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void GetDueDates(Boolean FinalSave)
    {
        try
        { 
        int LeaseCount = Convert.ToInt32(ControlValue("ucBaseLeaseTerms_hdnDueDateCount"));
        int OptnCount = Convert.ToInt32(ControlValue("ucOptionalTerms_hdnDueDateCount"));
        int ContingentCount = Convert.ToInt32(ControlValue("ucContingentCommission_hdnDueDateCount"));

        int OptnCount1 = Convert.ToInt32(ControlValue("ucOptional1Terms_hdnDueDateCount"));
        int OptnCount2 = Convert.ToInt32(ControlValue("ucOptional2Terms_hdnDueDateCount"));
        int OptnCount3 = Convert.ToInt32(ControlValue("ucOptional3Terms_hdnDueDateCount"));

        int duedtCount = 0;
        string Parent = "";
        int TermType = 0;

        CreatedtDueDtCommTable(FinalSave);
        DataTable dt = (DataTable)Session[GlobleData.CommissionTb];
        if (dt.Rows.Count == 0 || FinalSave == true)
        {

            if (LeaseCount > 0)
            {
                duedtCount = LeaseCount;
                Parent = cns_ucBaseLeaseTerms;
                TermType = GlobleData.cns_TertType_BaseLease;
                FindDueDateTableControls(duedtCount, Parent, TermType);
            }
            if (OptnCount > 0)
            {
                duedtCount = OptnCount;
                Parent = cns_ucOptionalTerms;
                TermType = GlobleData.cns_TertType_OptionTerm;
                FindDueDateTableControls(duedtCount, Parent, TermType);
            }
            if (ContingentCount > 0)
            {
                duedtCount = ContingentCount;
                Parent = cns_ucContingentCommission;
                TermType = GlobleData.cns_TertType_ContingentTerm;
                FindDueDateTableControls(duedtCount, Parent, TermType);
            }

            if (OptnCount1 > 0)
            {
                duedtCount = OptnCount1;
                Parent = cns_ucOptional1Terms;
                TermType = GlobleData.cns_TertType_OptionTerm1;
                FindDueDateTableControls(duedtCount, Parent, TermType);
            }
            if (OptnCount2 > 0)
            {
                duedtCount = OptnCount2;
                Parent = cns_ucOptional2Terms;
                TermType = GlobleData.cns_TertType_OptionTerm2;
                FindDueDateTableControls(duedtCount, Parent, TermType);
            }
            if (OptnCount3 > 0)
            {
                duedtCount = OptnCount3;
                Parent = cns_ucOptional3Terms;
                TermType = GlobleData.cns_TertType_OptionTerm3;
                FindDueDateTableControls(duedtCount, Parent, TermType);
            }
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "GetDueDates", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void SaveDueDates(Boolean ChkOpt)
    {
        try
        { 
        //  string hdnDueDateCountStr = ContentMain_ucOptionalTerms_hdnDueDateCount.value;
        ViewState["OptPayable"] = ChkOpt;
        GetDueDates(true);
        SaveAllDueDateRecords();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "SaveDueDates", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void ReCreateDueDatesOnPostBack(string ucName)
    {
        try
        { 
        if (hdnSetTrue.Value == "true") //if we add the uc name here we can skip addition of all due dates while reseting the controls 
        {
            GetDueDates(true);
        }
        else
        {
            GetDueDates(false);

        }
        TextBox txt;
        HtmlTableRow tr;
        HtmlTableCell tc;
        Label lbl, lblspace;
        ImageButton ImgBtn;


        dtDueDtComm = (DataTable)Session[GlobleData.CommissionTb];
        DataRow[] dRow1;
        dRow1 = dtDueDtComm.Select("[TermTypeId]=1");
        DataTable dtBase = dRow1.Length != 0 ? dRow1.CopyToDataTable() : null;
        dRow1 = null;
        dRow1 = dtDueDtComm.Select("[TermTypeId]=2");
        DataTable dtOptCommTerm = dRow1.Length != 0 ? dRow1.CopyToDataTable() : null;
        dRow1 = null;
        dRow1 = dtDueDtComm.Select("[TermTypeId]=3");
        DataTable dtCon = dRow1.Length != 0 ? dRow1.CopyToDataTable() : null;

        dRow1 = null;
        dRow1 = dtDueDtComm.Select("[TermTypeId]=4");
        DataTable dtOptCommTerm1 = dRow1.Length != 0 ? dRow1.CopyToDataTable() : null;
        dRow1 = null;
        dRow1 = dtDueDtComm.Select("[TermTypeId]=5");
        DataTable dtOptCommTerm2 = dRow1.Length != 0 ? dRow1.CopyToDataTable() : null;
        dRow1 = null;
        dRow1 = dtDueDtComm.Select("[TermTypeId]=6");
        DataTable dtOptCommTerm3 = dRow1.Length != 0 ? dRow1.CopyToDataTable() : null;

        string ucSel = this.ClientID;

        DataTable dt = null;

        if (dtBase != null && this.ClientID.Contains(cns_ucBaseLeaseTerms))
        {
            dt = dtBase;
        }
        else if (dtOptCommTerm != null && this.ClientID.Contains(cns_ucOptionalTerms))
        {
            dt = dtOptCommTerm;
        }
        else if (dtCon != null && this.ClientID.Contains(cns_ucContingentCommission))
        {
            dt = dtCon;
        }
        else if (dtOptCommTerm1 != null && this.ClientID.Contains(cns_ucOptional1Terms))
        {
            dt = dtOptCommTerm1;
        }
        else if (dtOptCommTerm2 != null && this.ClientID.Contains(cns_ucOptional2Terms))
        {
            dt = dtOptCommTerm2;
        }
        else if (dtOptCommTerm3 != null && this.ClientID.Contains(cns_ucOptional3Terms))
        {
            dt = dtOptCommTerm3;
        }

        int rowCount = tbDueDates.Rows.Count;
        if (dt != null)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                tr = new HtmlTableRow();

                txt = new TextBox();
                txt.ID = "lbDueDate_" + (i);
                txt.Text = dt.Rows[i]["DueDate"].ToString();
                txt.Width = 90;
                txt.Height = 16;
                txt.ReadOnly = true;
                //txt.Style.Add("textAlign","left");
                tc = new HtmlTableCell();
                tc.Controls.Add(txt);
                //tc.Style.Add("Width", "20%");
                tr.Cells.Add(tc);

                txt = new TextBox();
                txt.ID = "txtCommPerOnDueDt_" + (i);
                txt.Text = Convert.ToDecimal(dt.Rows[i]["PercentComm"]).ToString("#.##");
                txt.Width = 40;
                txt.Height = 16;
                //txt.Style.Add("textAlign", "left");

                lbl = new Label();
                lbl.ID = this.ClientID + "per" + (i);
                lbl.Text = " % ";


                //lbl.Width = 80;
                lbl.Height = 16;
                txt.ReadOnly = true;
                tc = new HtmlTableCell();
                tc.Controls.Add(txt);
                tc.Controls.Add(lbl);
                tc.Attributes.Add("class", "Padding");
                // tc.Style.Add("Width", "18%");
                tr.Cells.Add(tc);

                txt = new TextBox();
                txt.ID = "txtCommAmtOnDueDt_" + (i);
                txt.Text = Convert.ToDecimal(dt.Rows[i]["CommAmountDue"]).ToString("#.##");
                txt.Width = 100;
                txt.Height = 16;
                txt.ReadOnly = true;
                txt.CssClass = "DollarTextBoxWithBorder";
                tc = new HtmlTableCell();
                // tc.Style.Add("Width", "15%");
                tc.Controls.Add(txt);
                tr.Cells.Add(tc);


                ImgBtn = new ImageButton();
                ImgBtn.ID = "btnDelDueDate_" + (i);
                ImgBtn.ImageUrl = "../Images/delete.gif";
                ImgBtn.Attributes.Add("onclick", "javascript:DeleteDueDate('" + dt.Rows[i]["DueDate"].ToString() + "','" + Convert.ToDecimal(dt.Rows[i]["PercentComm"]).ToString("#.##") + "','" + tbDueDates.ClientID + "','" + this.ClientID + "');");
                tc = new HtmlTableCell();
                if (Session[GlobleData.cns_IsFullCommPaid] != null && this.ClientID.Contains(cns_ucBaseLeaseTerms))
                {
                    if (Convert.ToBoolean(Session[GlobleData.cns_IsFullCommPaid]))
                    {
                        // ImgBtn.Visible = false;
                    }
                }
                if (this.ClientID.Contains(cns_ucOptionalTerms))
                {
                    DealTransactions objdealtrans = new DealTransactions();
                    DataSet dtIsPayment = new DataSet();
                    dtIsPayment = objdealtrans.IsChargeslipPayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 2);
                    if (dtIsPayment != null && dtIsPayment.Tables.Count > 0)
                    {
                        if (dtIsPayment.Tables[0].Rows[0]["Ispayment"].ToString() != "0")
                        {
                            ImgBtn.Visible = true;
                        }
                    }
                }
                if (this.ClientID.Contains(cns_ucContingentCommission))
                {
                    DealTransactions objdealtrans = new DealTransactions();
                    DataSet dtIsPayment = new DataSet();
                    dtIsPayment = objdealtrans.IsChargeslipPayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 3);
                    if (dtIsPayment != null && dtIsPayment.Tables.Count > 0)
                    {
                        if (dtIsPayment.Tables[0].Rows[0]["Ispayment"].ToString() != "0")
                        {
                            ImgBtn.Visible = true;
                        }
                    }
                }

                if (this.ClientID.Contains(cns_ucOptional1Terms))
                {
                    DealTransactions objdealtrans = new DealTransactions();
                    DataSet dtIsPayment = new DataSet();
                    dtIsPayment = objdealtrans.IsChargeslipPayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 4);
                    if (dtIsPayment != null && dtIsPayment.Tables.Count > 0)
                    {
                        if (dtIsPayment.Tables[0].Rows[0]["Ispayment"].ToString() != "0")
                        {
                            ImgBtn.Visible = true;
                        }
                    }
                }
                if (this.ClientID.Contains(cns_ucOptional2Terms))
                {
                    DealTransactions objdealtrans = new DealTransactions();
                    DataSet dtIsPayment = new DataSet();
                    dtIsPayment = objdealtrans.IsChargeslipPayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 5);
                    if (dtIsPayment != null && dtIsPayment.Tables.Count > 0)
                    {
                        if (dtIsPayment.Tables[0].Rows[0]["Ispayment"].ToString() != "0")
                        {
                            ImgBtn.Visible = true;
                        }
                    }
                }
                if (this.ClientID.Contains(cns_ucOptional3Terms))
                {
                    DealTransactions objdealtrans = new DealTransactions();
                    DataSet dtIsPayment = new DataSet();
                    dtIsPayment = objdealtrans.IsChargeslipPayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 6);
                    if (dtIsPayment != null && dtIsPayment.Tables.Count > 0)
                    {
                        if (dtIsPayment.Tables[0].Rows[0]["Ispayment"].ToString() != "0")
                        {
                            ImgBtn.Visible = true;
                        }
                    }
                }
                tc.Controls.Add(ImgBtn);
                //tc.Style.Add("Width", "47%");
                tr.Cells.Add(tc);

                tbDueDates.Controls.AddAt(i, tr);
            }
            hdnDueDateCount.Value = dt.Rows.Count.ToString();//new 
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "ReCreateDueDatesOnPostBack", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    private void ResetNewFields()
    {
        // txtFromRentPeriod.Text = "";
        txtToRentPeriod.Text = "";
        txtRentPSP.Text = "";
        txtAnnualRent.Text = "";
        txtPerIncrese.Text = "";
    }


    public void BindGrid(string UcName)
    {
        try
        { 
        if (Session["dtBasicLeaseTerm"] != null)
        {
            dtBasicLeaseTerm = null;
            dtBasicLeaseTerm = (DataTable)Session["dtBasicLeaseTerm"];
        }
        DataRow[] dRow1; DataTable tempTB = null, tbDest = null; string tbName = "";

        dRow1 = dtBasicLeaseTerm.Select("[TermTypeId]=1");//For BaseLease  Grid

        if (UcName.Contains(cns_ucOptionalTerms))
        {
            dRow1 = dtBasicLeaseTerm.Select("[TermTypeId]=2");//For OptionalLease Grid
            if (dRow1.Length > 0)
            {
                tempTB = dRow1.CopyToDataTable();
                tbName = cns_dtBasicLeaseTerm + cns_ucOptionalTerms;

                //chk Opt CheckBox
                CheckBox Chk = (CheckBox)this.Parent.FindControl("chkOptionTerm");
                Chk.Checked = true;
            }
        }
        else if (UcName.Contains(cns_ucContingentCommission))
        {
            dRow1 = dtBasicLeaseTerm.Select("[TermTypeId]=3");//For ContingentLease Grid
            if (dRow1.Length > 0)
            {
                tempTB = dRow1.CopyToDataTable();
                tbName = cns_dtBasicLeaseTerm + cns_ucContingentCommission;
                CheckBox Chk = (CheckBox)this.Parent.FindControl("chkContingentComm");
                Chk.Checked = true;
            }
        }
        else if (UcName.Contains(cns_ucOptional1Terms))
        {
            dRow1 = dtBasicLeaseTerm.Select("[TermTypeId]=4");//For OptionalLease Grid 1
            if (dRow1.Length > 0)
            {
                tempTB = dRow1.CopyToDataTable();
                tbName = cns_dtBasicLeaseTerm + cns_ucOptional1Terms;

                //chk Opt CheckBox
                CheckBox Chk = (CheckBox)this.Parent.FindControl("chkOptionTerm1");
                Chk.Checked = true;
            }
        }
        else if (UcName.Contains(cns_ucOptional2Terms))
        {
            dRow1 = dtBasicLeaseTerm.Select("[TermTypeId]=5");//For OptionalLease Grid 2
            if (dRow1.Length > 0)
            {
                tempTB = dRow1.CopyToDataTable();
                tbName = cns_dtBasicLeaseTerm + cns_ucOptional2Terms;

                //chk Opt CheckBox
                CheckBox Chk = (CheckBox)this.Parent.FindControl("chkOptionTerm2");
                Chk.Checked = true;
            }
        }
        else if (UcName.Contains(cns_ucOptional3Terms))
        {
            dRow1 = dtBasicLeaseTerm.Select("[TermTypeId]=6");//For OptionalLease Grid 3
            if (dRow1.Length > 0)
            {
                tempTB = dRow1.CopyToDataTable();
                tbName = cns_dtBasicLeaseTerm + cns_ucOptional3Terms;

                //chk Opt CheckBox
                CheckBox Chk = (CheckBox)this.Parent.FindControl("chkOptionTerm3");
                Chk.Checked = true;
            }
        }

        tbDest = (DataTable)Session[tbName];
        if (tbDest != null)
        {
            SetLeaseTermGrid(tempTB, tbDest);
            Session[tbName] = tbDest;
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "BindGrid", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    // For Edit Purpose
    public void GetLeaseTermRecordsForSelChargeSlip(String UcName)
    {
        try
        { 
        if (Session[GlobleData.NewChargeSlipId] != null)
        {
            LeaseTerms objLeaseTerms = new LeaseTerms();
            objLeaseTerms.ChargeSlipId = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);

            if (Session["dtBasicLeaseTerm"] != null)
            {
                dtBasicLeaseTerm = (DataTable)Session["dtBasicLeaseTerm"];
            }

            //if (dtBasicLeaseTerm.Rows.Count <= 0 || Session["dtBasicLeaseTerm"] == null)
            //{
            DataSet ds = objLeaseTerms.GetListByChargeSlip();
            // DataTable dtLeaseTerms = null, dtCommTerms = null;
            if (ds != null)
            {
                dtBasicLeaseTerm = ds.Tables[0];
                dtCommTerm = ds.Tables[1];
                dtDueDtComm = ds.Tables[2];
                dtAttachment = ds.Tables[3];

                Session["dtBasicLeaseTerm"] = dtBasicLeaseTerm;
                Session["dtCommTerm"] = dtCommTerm;
                Session["dtDueDtComm"] = dtDueDtComm;
                Session["dtAttachment"] = dtAttachment;
            }
            //}
            //else
            //{
            // dtBasicLeaseTerm = (DataTable)Session["dtBasicLeaseTerm"];
            dtCommTerm = (DataTable)Session["dtCommTerm"];
            DataRow[] dRow1; DataTable tempTB = null, tbDest = null; string tbName = "";

            if (dtBasicLeaseTerm.Rows.Count > 0)
            {
                if (UcName.Contains(cns_ucBaseLeaseTerms))
                {
                    DealTransactions obj = new DealTransactions();
                    DataSet dsCommissionType = new DataSet();
                    ds = obj.GetCommissionType(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]));
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                        {
                            ddlCommissionType.SelectedValue = ds.Tables[0].Rows[0]["CommissionTypeBaseTerm"].ToString();
                            ((HiddenField)(Parent.FindControl("hdnCommissionTypeBaseTerm"))).Value = ds.Tables[0].Rows[0]["CommissionTypeBaseTerm"].ToString();
                            Session["CommissionTypeBaseTerm"] = ds.Tables[0].Rows[0]["CommissionTypeBaseTerm"].ToString();
                            if (ddlCommissionType.SelectedValue == "2" || ddlCommissionType.SelectedValue == "3")
                            {
                                gvBaseCommTerms.Attributes.Add("style", "display:none");

                            }
                            else
                            {
                                gvBaseCommTerms.Attributes.Add("style", "display:block");
                            }
                            if (ddlCommissionType.SelectedValue == "2")
                            {
                                lblComAmountSquareFoot.Visible = true;
                                lblCommissionAmountSquareFoot.Visible = true;
                            }
                            else
                            {
                                lblComAmountSquareFoot.Visible = false;
                                lblCommissionAmountSquareFoot.Visible = false;
                            }
                        }
                    }
                    dRow1 = dtBasicLeaseTerm.Select("[TermTypeId]=1");//For BaseLease  Grid
                    if (dRow1.Length > 0)
                    {
                        tempTB = dRow1.CopyToDataTable();
                        TotalSquareFoot = Convert.ToDecimal(tempTB.Rows[0]["TotalSqFt"].ToString());
                        tbName = cns_dtBasicLeaseTerm + cns_ucBaseLeaseTerms;
                        if (Parent.FindControl("hdnleaseLast") != null)
                        {
                            ((HiddenField)(Parent.FindControl("hdnleaseLast"))).Value = tempTB.Rows[tempTB.Rows.Count - 1]["AnnualRent"].ToString();
                        }
                    }
                    //Commented by Jaywanti on 09-11-2015
                    //if (Session[GlobleData.cns_IsFullCommPaid] != null)
                    //{
                    //    if (Convert.ToBoolean(Session[GlobleData.cns_IsFullCommPaid].ToString()))
                    //    {
                    //        btnDueDtAdd.Disabled = true;

                    //    }
                    //}
                }
                else if (UcName.Contains(cns_ucOptionalTerms))
                {
                    DealTransactions obj = new DealTransactions();
                    DataSet dsCommissionType = new DataSet();
                    ds = obj.GetCommissionType(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]));
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                        {
                            ddlCommissionType.SelectedValue = ds.Tables[0].Rows[0]["CommissionTypeOptionTerm"].ToString();
                            ((HiddenField)(Parent.FindControl("hdnCommissionTypeOptionTerm"))).Value = ds.Tables[0].Rows[0]["CommissionTypeOptionTerm"].ToString();
                            Session["CommissionTypeOptionTerm"] = ds.Tables[0].Rows[0]["CommissionTypeOptionTerm"].ToString();
                            if (ddlCommissionType.SelectedValue == "2" || ddlCommissionType.SelectedValue == "3")
                            {
                                gvBaseCommTerms.Attributes.Add("style", "display:none");
                            }
                            else
                            {
                                gvBaseCommTerms.Attributes.Add("style", "display:block");
                            }
                            if (ddlCommissionType.SelectedValue == "2")
                            {
                                lblComAmountSquareFoot.Visible = true;
                                lblCommissionAmountSquareFoot.Visible = true;
                            }
                            else
                            {
                                lblComAmountSquareFoot.Visible = false;
                                lblCommissionAmountSquareFoot.Visible = false;
                            }
                        }
                    }
                    dRow1 = dtBasicLeaseTerm.Select("[TermTypeId]=2");//For OptionalLease Grid
                    if (dRow1.Length > 0)
                    {
                        tempTB = dRow1.CopyToDataTable();
                        TotalSquareFoot = Convert.ToDecimal(tempTB.Rows[0]["TotalSqFt"].ToString());
                        tbName = cns_dtBasicLeaseTerm + cns_ucOptionalTerms;

                        //chk Opt CheckBox
                        CheckBox Chk = (CheckBox)this.Parent.FindControl("chkOptionTerm");
                        Chk.Checked = true;
                    }
                    //Commented by Jaywanti on 09-11-2015
                    //DataSet dtIsPayment = new DataSet();
                    //dtIsPayment = obj.IsChargeslipPayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 2);
                    //if (dtIsPayment != null && dtIsPayment.Tables.Count > 0)
                    //{
                    //    if (dtIsPayment.Tables[0].Rows[0]["Ispayment"].ToString() != "0")
                    //    {
                    //        btnDueDtAdd.Disabled = true;
                    //        hdnIsPayment.Value = dtIsPayment.Tables[0].Rows[0]["Ispayment"].ToString();
                    //    }
                    //}


                }
                else if (UcName.Contains(cns_ucContingentCommission))
                {
                    DealTransactions obj = new DealTransactions();
                    DataSet dsCommissionType = new DataSet();
                    ds = obj.GetCommissionType(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]));
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                        {
                            ddlCommissionType.SelectedValue = ds.Tables[0].Rows[0]["CommissionTypeContingentTerm"].ToString();
                            ((HiddenField)(Parent.FindControl("hdnCommissionTypeContingent"))).Value = ds.Tables[0].Rows[0]["CommissionTypeContingentTerm"].ToString();
                            Session["CommissionTypeContingent"] = ds.Tables[0].Rows[0]["CommissionTypeContingentTerm"].ToString();
                            if (ddlCommissionType.SelectedValue == "2" || ddlCommissionType.SelectedValue == "3")
                            {
                                gvBaseCommTerms.Attributes.Add("style", "display:none");
                            }
                            else
                            {
                                gvBaseCommTerms.Attributes.Add("style", "display:block");
                            }
                            if (ddlCommissionType.SelectedValue == "2")
                            {
                                lblComAmountSquareFoot.Visible = true;
                                lblCommissionAmountSquareFoot.Visible = true;
                            }
                            else
                            {
                                lblComAmountSquareFoot.Visible = false;
                                lblCommissionAmountSquareFoot.Visible = false;
                            }
                        }
                    }
                    dRow1 = dtBasicLeaseTerm.Select("[TermTypeId]=3");//For ContingentLease Grid
                    if (dRow1.Length > 0)
                    {
                        tempTB = dRow1.CopyToDataTable();
                        TotalSquareFoot = Convert.ToDecimal(tempTB.Rows[0]["TotalSqFt"].ToString());
                        tbName = cns_dtBasicLeaseTerm + cns_ucContingentCommission;
                        CheckBox Chk = (CheckBox)this.Parent.FindControl("chkContingentComm");
                        Chk.Checked = true;
                    }
                    //Commented by Jaywanti on 09-11-2015
                    //DataSet dtIsPayment = new DataSet();
                    //dtIsPayment = obj.IsChargeslipPayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 2);
                    //if (dtIsPayment != null && dtIsPayment.Tables.Count > 0)
                    //{
                    //    if (dtIsPayment.Tables[0].Rows[0]["Ispayment"].ToString() != "0")
                    //    {
                    //        btnDueDtAdd.Disabled = true;
                    //        hdnIsPayment.Value = dtIsPayment.Tables[0].Rows[0]["Ispayment"].ToString();
                    //    }
                    //}
                }

                else if (UcName.Contains(cns_ucOptional1Terms))
                {
                    DealTransactions obj = new DealTransactions();
                    DataSet dsCommissionType = new DataSet();
                    ds = obj.GetCommissionType(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]));
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                        {
                            ddlCommissionType.SelectedValue = ds.Tables[0].Rows[0]["CommissionTypeOptionTerm1"].ToString();
                            ((HiddenField)(Parent.FindControl("hdnCommissionTypeOptionTerm1"))).Value = ds.Tables[0].Rows[0]["CommissionTypeOptionTerm1"].ToString();
                            Session["CommissionTypeOptionTerm1"] = ds.Tables[0].Rows[0]["CommissionTypeOptionTerm1"].ToString();
                            if (ddlCommissionType.SelectedValue == "2" || ddlCommissionType.SelectedValue == "3")
                            {
                                gvBaseCommTerms.Attributes.Add("style", "display:none");
                            }
                            else
                            {
                                gvBaseCommTerms.Attributes.Add("style", "display:block");
                            }
                            if (ddlCommissionType.SelectedValue == "2")
                            {
                                lblComAmountSquareFoot.Visible = true;
                                lblCommissionAmountSquareFoot.Visible = true;
                            }
                            else
                            {
                                lblComAmountSquareFoot.Visible = false;
                                lblCommissionAmountSquareFoot.Visible = false;
                            }
                        }
                    }
                    dRow1 = dtBasicLeaseTerm.Select("[TermTypeId]=4");//For OptionalLease Grid 1
                    if (dRow1.Length > 0)
                    {
                        tempTB = dRow1.CopyToDataTable();
                        TotalSquareFoot = Convert.ToDecimal(tempTB.Rows[0]["TotalSqFt"].ToString());
                        tbName = cns_dtBasicLeaseTerm + cns_ucOptional1Terms;

                        //chk Opt CheckBox 1
                        CheckBox Chk = (CheckBox)this.Parent.FindControl("chkOptionTerm1");
                        Chk.Checked = true;
                    }
                }
                else if (UcName.Contains(cns_ucOptional2Terms))
                {
                    DealTransactions obj = new DealTransactions();
                    DataSet dsCommissionType = new DataSet();
                    ds = obj.GetCommissionType(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]));
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                        {
                            ddlCommissionType.SelectedValue = ds.Tables[0].Rows[0]["CommissionTypeOptionTerm2"].ToString();
                            ((HiddenField)(Parent.FindControl("hdnCommissionTypeOptionTerm2"))).Value = ds.Tables[0].Rows[0]["CommissionTypeOptionTerm2"].ToString();
                            Session["CommissionTypeOptionTerm2"] = ds.Tables[0].Rows[0]["CommissionTypeOptionTerm2"].ToString();
                            if (ddlCommissionType.SelectedValue == "2" || ddlCommissionType.SelectedValue == "3")
                            {
                                gvBaseCommTerms.Attributes.Add("style", "display:none");
                            }
                            else
                            {
                                gvBaseCommTerms.Attributes.Add("style", "display:block");
                            }
                            if (ddlCommissionType.SelectedValue == "2")
                            {
                                lblComAmountSquareFoot.Visible = true;
                                lblCommissionAmountSquareFoot.Visible = true;
                            }
                            else
                            {
                                lblComAmountSquareFoot.Visible = false;
                                lblCommissionAmountSquareFoot.Visible = false;
                            }
                        }
                    }
                    dRow1 = dtBasicLeaseTerm.Select("[TermTypeId]=5");//For OptionalLease Grid 2
                    if (dRow1.Length > 0)
                    {
                        tempTB = dRow1.CopyToDataTable();
                        TotalSquareFoot = Convert.ToDecimal(tempTB.Rows[0]["TotalSqFt"].ToString());
                        tbName = cns_dtBasicLeaseTerm + cns_ucOptional2Terms;

                        //chk Opt CheckBox 2
                        CheckBox Chk = (CheckBox)this.Parent.FindControl("chkOptionTerm2");
                        Chk.Checked = true;
                    }
                }
                else if (UcName.Contains(cns_ucOptional3Terms))
                {
                    DealTransactions obj = new DealTransactions();
                    DataSet dsCommissionType = new DataSet();
                    ds = obj.GetCommissionType(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]));
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                        {
                            ddlCommissionType.SelectedValue = ds.Tables[0].Rows[0]["CommissionTypeOptionTerm3"].ToString();
                            ((HiddenField)(Parent.FindControl("hdnCommissionTypeOptionTerm3"))).Value = ds.Tables[0].Rows[0]["CommissionTypeOptionTerm3"].ToString();
                            Session["CommissionTypeOptionTerm3"] = ds.Tables[0].Rows[0]["CommissionTypeOptionTerm3"].ToString();
                            if (ddlCommissionType.SelectedValue == "2" || ddlCommissionType.SelectedValue == "3")
                            {
                                gvBaseCommTerms.Attributes.Add("style", "display:none");
                            }
                            else
                            {
                                gvBaseCommTerms.Attributes.Add("style", "display:block");
                            }
                            if (ddlCommissionType.SelectedValue == "2")
                            {
                                lblComAmountSquareFoot.Visible = true;
                                lblCommissionAmountSquareFoot.Visible = true;
                            }
                            else
                            {
                                lblComAmountSquareFoot.Visible = false;
                                lblCommissionAmountSquareFoot.Visible = false;
                            }
                        }
                    }
                    dRow1 = dtBasicLeaseTerm.Select("[TermTypeId]=6");//For OptionalLease Grid 3
                    if (dRow1.Length > 0)
                    {
                        tempTB = dRow1.CopyToDataTable();
                        TotalSquareFoot = Convert.ToDecimal(tempTB.Rows[0]["TotalSqFt"].ToString());
                        tbName = cns_dtBasicLeaseTerm + cns_ucOptional3Terms;

                        //chk Opt CheckBox 3
                        CheckBox Chk = (CheckBox)this.Parent.FindControl("chkOptionTerm3");
                        Chk.Checked = true;
                    }
                }


                tbDest = (DataTable)Session[tbName];
                if (tbDest != null)
                {
                    SetLeaseTermGrid(tempTB, tbDest);
                    Session[tbName] = tbDest;
                }


            }

            if (dtCommTerm.Rows.Count > 0)
            {
                tempTB = null;
                if (UcName.Contains(cns_ucBaseLeaseTerms))
                {
                    dRow1 = dtCommTerm.Select("[TermTypeId]=1");//For BaseLease  Grid
                    if (dRow1.Length > 0)
                    {
                        tempTB = dRow1.CopyToDataTable();
                        tbName = cns_dtCommTerm + cns_ucBaseLeaseTerms;
                        hdnCommissionTypeId.Value = ((HiddenField)(Parent.FindControl("hdnCommissionTypeBaseTerm"))).Value;
                        if (ddlCommissionType.SelectedValue == "2")
                        {
                            lblComAmountSquareFoot.Visible = true;
                            lblCommissionAmountSquareFoot.Visible = true;
                            if (tempTB != null && tempTB.Rows.Count > 0)
                            {
                                lblComAmountSquareFoot.Text = "$" + String.Format("{0:n2}", TotalSquareFoot * Convert.ToDecimal(tempTB.Rows[0]["PercentBrokerComm"].ToString()));
                                hdnTotalSquareFoot.Value = TotalSquareFoot.ToString();
                            }
                        }
                        else
                        {
                            lblComAmountSquareFoot.Visible = false;
                            lblCommissionAmountSquareFoot.Visible = false;
                        }

                    }



                }
                else if (UcName.Contains(cns_ucOptionalTerms))
                {
                    dRow1 = dtCommTerm.Select("[TermTypeId]=2");//For OptionalLease Grid
                    if (dRow1.Length > 0)
                    {
                        tempTB = dRow1.CopyToDataTable();
                        tbName = cns_dtCommTerm + cns_ucOptionalTerms;
                        hdnCommissionTypeId.Value = ((HiddenField)(Parent.FindControl("hdnCommissionTypeOptionTerm"))).Value;
                        if (ddlCommissionType.SelectedValue == "2")
                        {
                            lblComAmountSquareFoot.Visible = true;
                            lblCommissionAmountSquareFoot.Visible = true;
                            if (tempTB != null && tempTB.Rows.Count > 0)
                            {
                                lblComAmountSquareFoot.Text = "$" + String.Format("{0:n2}", TotalSquareFoot * Convert.ToDecimal(tempTB.Rows[0]["PercentBrokerComm"].ToString()));
                                hdnTotalSquareFoot.Value = TotalSquareFoot.ToString();
                            }
                        }
                        else
                        {
                            lblComAmountSquareFoot.Visible = false;
                            lblCommissionAmountSquareFoot.Visible = false;
                        }
                    }
                }
                else if (UcName.Contains(cns_ucContingentCommission))
                {
                    dRow1 = dtCommTerm.Select("[TermTypeId]=3");//For ContingentLease Grid
                    if (dRow1.Length > 0)
                    {
                        tempTB = dRow1.CopyToDataTable();
                        tbName = cns_dtCommTerm + cns_ucContingentCommission;
                        hdnCommissionTypeId.Value = ((HiddenField)(Parent.FindControl("hdnCommissionTypeContingent"))).Value;
                        if (ddlCommissionType.SelectedValue == "2")
                        {
                            lblComAmountSquareFoot.Visible = true;
                            lblCommissionAmountSquareFoot.Visible = true;
                            if (tempTB != null && tempTB.Rows.Count > 0)
                            {
                                lblComAmountSquareFoot.Text = "$" + String.Format("{0:n2}", TotalSquareFoot * Convert.ToDecimal(tempTB.Rows[0]["PercentBrokerComm"].ToString()));
                                hdnTotalSquareFoot.Value = TotalSquareFoot.ToString();
                            }
                        }
                        else
                        {
                            lblComAmountSquareFoot.Visible = false;
                            lblCommissionAmountSquareFoot.Visible = false;
                        }
                    }
                }

                else if (UcName.Contains(cns_ucOptional1Terms))
                {
                    dRow1 = dtCommTerm.Select("[TermTypeId]=4");//For OptionalLease Grid 1
                    if (dRow1.Length > 0)
                    {
                        tempTB = dRow1.CopyToDataTable();
                        tbName = cns_dtCommTerm + cns_ucOptional1Terms;
                        hdnCommissionTypeId.Value = ((HiddenField)(Parent.FindControl("hdnCommissionTypeOptionTerm1"))).Value;
                        if (ddlCommissionType.SelectedValue == "2")
                        {
                            lblComAmountSquareFoot.Visible = true;
                            lblCommissionAmountSquareFoot.Visible = true;
                            if (tempTB != null && tempTB.Rows.Count > 0)
                            {
                                lblComAmountSquareFoot.Text = "$" + String.Format("{0:n2}", TotalSquareFoot * Convert.ToDecimal(tempTB.Rows[0]["PercentBrokerComm"].ToString()));
                                hdnTotalSquareFoot.Value = TotalSquareFoot.ToString();
                            }
                        }
                        else
                        {
                            lblComAmountSquareFoot.Visible = false;
                            lblCommissionAmountSquareFoot.Visible = false;
                        }
                    }
                }
                else if (UcName.Contains(cns_ucOptional2Terms))
                {
                    dRow1 = dtCommTerm.Select("[TermTypeId]=5");//For OptionalLease Grid 2
                    if (dRow1.Length > 0)
                    {
                        tempTB = dRow1.CopyToDataTable();
                        tbName = cns_dtCommTerm + cns_ucOptional2Terms;
                        hdnCommissionTypeId.Value = ((HiddenField)(Parent.FindControl("hdnCommissionTypeOptionTerm2"))).Value;
                        if (ddlCommissionType.SelectedValue == "2")
                        {
                            lblComAmountSquareFoot.Visible = true;
                            lblCommissionAmountSquareFoot.Visible = true;
                            if (tempTB != null && tempTB.Rows.Count > 0)
                            {
                                lblComAmountSquareFoot.Text = "$" + String.Format("{0:n2}", TotalSquareFoot * Convert.ToDecimal(tempTB.Rows[0]["PercentBrokerComm"].ToString()));
                                hdnTotalSquareFoot.Value = TotalSquareFoot.ToString();
                            }
                        }
                        else
                        {
                            lblComAmountSquareFoot.Visible = false;
                            lblCommissionAmountSquareFoot.Visible = false;
                        }
                    }
                }
                else if (UcName.Contains(cns_ucOptional3Terms))
                {
                    dRow1 = dtCommTerm.Select("[TermTypeId]=6");//For OptionalLease Grid 3
                    if (dRow1.Length > 0)
                    {
                        tempTB = dRow1.CopyToDataTable();
                        tbName = cns_dtCommTerm + cns_ucOptional3Terms;
                        hdnCommissionTypeId.Value = ((HiddenField)(Parent.FindControl("hdnCommissionTypeOptionTerm3"))).Value;
                        if (ddlCommissionType.SelectedValue == "2")
                        {
                            lblComAmountSquareFoot.Visible = true;
                            lblCommissionAmountSquareFoot.Visible = true;
                            if (tempTB != null && tempTB.Rows.Count > 0)
                            {
                                lblComAmountSquareFoot.Text = "$" + String.Format("{0:n2}", TotalSquareFoot * Convert.ToDecimal(tempTB.Rows[0]["PercentBrokerComm"].ToString()));
                                hdnTotalSquareFoot.Value = TotalSquareFoot.ToString();
                            }
                        }
                        else
                        {
                            lblComAmountSquareFoot.Visible = false;
                            lblCommissionAmountSquareFoot.Visible = false;
                        }
                    }
                }

                tbDest = (DataTable)Session[tbName];
                if (tbDest != null)
                {
                    SetCommTermGrid(tempTB, tbDest);
                    if (tbName == cns_dtCommTerm + cns_ucOptionalTerms && Session[tbName] != null)
                    {
                        if (Convert.ToInt32(tbDest.Rows[0]["BrokerCommission"]) > 0)
                        {
                            //chk Opt CheckBox
                            CheckBox Chk = (CheckBox)this.Parent.FindControl("chkOptionTermPayable");
                            Chk.Checked = true;
                        }
                    }
                    else if (tbName == cns_dtCommTerm + cns_ucOptional1Terms && Session[tbName] != null)
                    {
                        if (Convert.ToInt32(tbDest.Rows[0]["BrokerCommission"]) > 0)
                        {
                            //chk Opt CheckBox
                            CheckBox Chk = (CheckBox)this.Parent.FindControl("chkOptionTermPayable1");
                            Chk.Checked = true;
                        }
                    }
                    else if (tbName == cns_dtCommTerm + cns_ucOptional2Terms && Session[tbName] != null)
                    {
                        if (Convert.ToInt32(tbDest.Rows[0]["BrokerCommission"]) > 0)
                        {
                            //chk Opt CheckBox
                            CheckBox Chk = (CheckBox)this.Parent.FindControl("chkOptionTermPayable2");
                            Chk.Checked = true;
                        }
                    }
                    else if (tbName == cns_dtCommTerm + cns_ucOptional3Terms && Session[tbName] != null)
                    {
                        if (Convert.ToInt32(tbDest.Rows[0]["BrokerCommission"]) > 0)
                        {
                            //chk Opt CheckBox
                            CheckBox Chk = (CheckBox)this.Parent.FindControl("chkOptionTermPayable3");
                            Chk.Checked = true;
                        }
                    }
                    Session[tbName] = tbDest;
                }
            }

            // test due dates
            GenerateSummary(UcName);
            //disabled when paid only for base lease term
            //if (Session[GlobleData.cns_IsFullCommPaid] != null)
            //{
            //    if (Convert.ToBoolean(Session[GlobleData.cns_IsFullCommPaid].ToString()))
            //    {
            //        btnDueDtAdd.Disabled = true;
            //    }
            //}

            //}
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "GetLeaseTermRecordsForSelChargeSlip", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }


    public void GenerateSummary(String UcName)
    {
        try
        { 
        dtDueDtComm = (DataTable)Session["dtDueDtComm"] == null ? (DataTable)Session[GlobleData.CommissionTb] : (DataTable)Session["dtDueDtComm"];
        if (dtDueDtComm.Rows.Count > 0)
        {
            Page.ClientScript.RegisterStartupScript(GetType(), "clearTb", "javascript:ClearTable();", true);
            int i;
            DataRow[] dRow2 = dtDueDtComm.Select("[TermTypeId]=1");
            if (UcName.Contains(cns_ucBaseLeaseTerms))
            {
                for (i = 0; i < dRow2.Length; i++)
                {
                    int No = rnd.Next(200, 300);
                    DealTransactions obj = new DealTransactions();
                    DataSet ds = new DataSet();
                    ds = obj.CheckCommissiondueDatePayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 1, dRow2[i]["DueDate"].ToString());
                    string Ispayment = "False";
                    int PaymentCount = 0;
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        PaymentCount = Convert.ToInt32(ds.Tables[0].Rows[0]["RowCountResult"].ToString());
                        if (PaymentCount > 0)
                        {
                            Ispayment = "True";
                        }

                    }
                    // Page.ClientScript.RegisterStartupScript(GetType(), "myScript" + i, "javascript:addDueDateRecordsEditMode('" + this.ClientID + "','" + dtDueDtComm.Rows[i]["DueDate"].ToString() + "','" + dtDueDtComm.Rows[i]["PercentComm"].ToString() + "','" + dtDueDtComm.Rows[i]["CommAmountDue"].ToString() + "')", true);
                    Page.ClientScript.RegisterStartupScript(GetType(), "myScript" + No, "javascript:addDueDateRecordsEditMode('" + this.ClientID + "','" + dRow2[i]["DueDate"].ToString() + "','" + dRow2[i]["PercentComm"].ToString() + "','" + dRow2[i]["CommAmountDue"].ToString() + "','" + i + "','" + Ispayment + "');", true);

                }
            }
            dRow2 = null;
            dRow2 = dtDueDtComm.Select("[TermTypeId]=2");
            if (UcName.Contains(cns_ucOptionalTerms))
            {
                for (i = 0; i < dRow2.Length; i++)
                {
                    int No = rnd.Next(300, 400);
                    // Page.ClientScript.RegisterStartupScript(GetType(), "myScript" + i, "javascript:addDueDateRecordsEditMode('" + this.ClientID + "','" + dtDueDtComm.Rows[i]["DueDate"].ToString() + "','" + dtDueDtComm.Rows[i]["PercentComm"].ToString() + "','" + dtDueDtComm.Rows[i]["CommAmountDue"].ToString() + "')", true);
                    DealTransactions obj = new DealTransactions();
                    DataSet ds = new DataSet();
                    ds = obj.CheckCommissiondueDatePayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 2, dRow2[i]["DueDate"].ToString());
                    string Ispayment = "False";
                    int PaymentCount = 0;
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        PaymentCount = Convert.ToInt32(ds.Tables[0].Rows[0]["RowCountResult"].ToString());
                        if (PaymentCount > 0)
                        {
                            Ispayment = "True";
                        }

                    }
                    Page.ClientScript.RegisterStartupScript(GetType(), "myScript" + No, "javascript:addDueDateRecordsEditMode('" + this.ClientID + "','" + dRow2[i]["DueDate"].ToString() + "','" + dRow2[i]["PercentComm"].ToString() + "','" + dRow2[i]["CommAmountDue"].ToString() + "','" + i + "','" + Ispayment + "');", true);

                }
            }
            dRow2 = null;
            dRow2 = dtDueDtComm.Select("[TermTypeId]=3");
            if (UcName.Contains(cns_ucContingentCommission))
            {
                for (i = 0; i < dRow2.Length; i++)
                {
                    int No = rnd.Next(400, 500);
                    // Page.ClientScript.RegisterStartupScript(GetType(), "myScript" + i, "javascript:addDueDateRecordsEditMode('" + this.ClientID + "','" + dtDueDtComm.Rows[i]["DueDate"].ToString() + "','" + dtDueDtComm.Rows[i]["PercentComm"].ToString() + "','" + dtDueDtComm.Rows[i]["CommAmountDue"].ToString() + "')", true);
                    DealTransactions obj = new DealTransactions();
                    DataSet ds = new DataSet();
                    ds = obj.CheckCommissiondueDatePayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 3, dRow2[i]["DueDate"].ToString());
                    string Ispayment = "False";
                    int PaymentCount = 0;
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        PaymentCount = Convert.ToInt32(ds.Tables[0].Rows[0]["RowCountResult"].ToString());
                        if (PaymentCount > 0)
                        {
                            Ispayment = "True";
                        }

                    }
                    Page.ClientScript.RegisterStartupScript(GetType(), "myScript" + No, "javascript:addDueDateRecordsEditMode('" + this.ClientID + "','" + dRow2[i]["DueDate"].ToString() + "','" + dRow2[i]["PercentComm"].ToString() + "','" + dRow2[i]["CommAmountDue"].ToString() + "','" + i + "','" + Ispayment + "');", true);

                }
            }

            dRow2 = null;
            dRow2 = dtDueDtComm.Select("[TermTypeId]=4");
            if (UcName.Contains(cns_ucOptional1Terms))
            {
                for (i = 0; i < dRow2.Length; i++)
                {
                    int No = rnd.Next(500, 600);
                    // Page.ClientScript.RegisterStartupScript(GetType(), "myScript" + i, "javascript:addDueDateRecordsEditMode('" + this.ClientID + "','" + dtDueDtComm.Rows[i]["DueDate"].ToString() + "','" + dtDueDtComm.Rows[i]["PercentComm"].ToString() + "','" + dtDueDtComm.Rows[i]["CommAmountDue"].ToString() + "')", true);
                    DealTransactions obj = new DealTransactions();
                    DataSet ds = new DataSet();
                    ds = obj.CheckCommissiondueDatePayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 4, dRow2[i]["DueDate"].ToString());
                    string Ispayment = "False";
                    int PaymentCount = 0;
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        PaymentCount = Convert.ToInt32(ds.Tables[0].Rows[0]["RowCountResult"].ToString());
                        if (PaymentCount > 0)
                        {
                            Ispayment = "True";
                        }

                    }
                    Page.ClientScript.RegisterStartupScript(GetType(), "myScript" + No, "javascript:addDueDateRecordsEditMode('" + this.ClientID + "','" + dRow2[i]["DueDate"].ToString() + "','" + dRow2[i]["PercentComm"].ToString() + "','" + dRow2[i]["CommAmountDue"].ToString() + "','" + i + "','" + Ispayment + "');", true);

                }
            }
            dRow2 = null;
            dRow2 = dtDueDtComm.Select("[TermTypeId]=5");
            if (UcName.Contains(cns_ucOptional2Terms))
            {
                for (i = 0; i < dRow2.Length; i++)
                {
                    int No = rnd.Next(600, 700);
                    // Page.ClientScript.RegisterStartupScript(GetType(), "myScript" + i, "javascript:addDueDateRecordsEditMode('" + this.ClientID + "','" + dtDueDtComm.Rows[i]["DueDate"].ToString() + "','" + dtDueDtComm.Rows[i]["PercentComm"].ToString() + "','" + dtDueDtComm.Rows[i]["CommAmountDue"].ToString() + "')", true);
                    DealTransactions obj = new DealTransactions();
                    DataSet ds = new DataSet();
                    ds = obj.CheckCommissiondueDatePayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 5, dRow2[i]["DueDate"].ToString());
                    string Ispayment = "False";
                    int PaymentCount = 0;
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        PaymentCount = Convert.ToInt32(ds.Tables[0].Rows[0]["RowCountResult"].ToString());
                        if (PaymentCount > 0)
                        {
                            Ispayment = "True";
                        }

                    }
                    Page.ClientScript.RegisterStartupScript(GetType(), "myScript" + No, "javascript:addDueDateRecordsEditMode('" + this.ClientID + "','" + dRow2[i]["DueDate"].ToString() + "','" + dRow2[i]["PercentComm"].ToString() + "','" + dRow2[i]["CommAmountDue"].ToString() + "','" + i + "','" + Ispayment + "');", true);

                }
            }
            dRow2 = null;
            dRow2 = dtDueDtComm.Select("[TermTypeId]=6");
            if (UcName.Contains(cns_ucOptional3Terms))
            {
                for (i = 0; i < dRow2.Length; i++)
                {
                    int No = rnd.Next(700, 800);
                    // Page.ClientScript.RegisterStartupScript(GetType(), "myScript" + i, "javascript:addDueDateRecordsEditMode('" + this.ClientID + "','" + dtDueDtComm.Rows[i]["DueDate"].ToString() + "','" + dtDueDtComm.Rows[i]["PercentComm"].ToString() + "','" + dtDueDtComm.Rows[i]["CommAmountDue"].ToString() + "')", true);
                    DealTransactions obj = new DealTransactions();
                    DataSet ds = new DataSet();
                    ds = obj.CheckCommissiondueDatePayment(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), 6, dRow2[i]["DueDate"].ToString());
                    string Ispayment = "False";
                    int PaymentCount = 0;
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        PaymentCount = Convert.ToInt32(ds.Tables[0].Rows[0]["RowCountResult"].ToString());
                        if (PaymentCount > 0)
                        {
                            Ispayment = "True";
                        }

                    }
                    Page.ClientScript.RegisterStartupScript(GetType(), "myScript" + No, "javascript:addDueDateRecordsEditMode('" + this.ClientID + "','" + dRow2[i]["DueDate"].ToString() + "','" + dRow2[i]["PercentComm"].ToString() + "','" + dRow2[i]["CommAmountDue"].ToString() + "','" + i + "','" + Ispayment + "');", true);

                }
            }

        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "GenerateSummary", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private void SetLeaseTermGrid(DataTable tbSource, DataTable tbDest)
    {
        try
        { 
        foreach (DataRow dr in tbSource.Rows)
        {
            dRow = tbDest.NewRow();
            dRow["RentFromPeriod"] = Convert.ToInt32(dr["RentPeriodFrom"].ToString());
            dRow["RentToPeriod"] = Convert.ToInt32(dr["RentPeriodTo"].ToString());
            dRow["TotalMonths"] = dr["TotalMonths"].ToString();
            dRow["Rent_PSF"] = dr["RentPerSqFt"].ToString();
            dRow["AnnualRent"] = dr["AnnualRent"].ToString();
            dRow["PerIncrease"] = dr["PercentIncrease"].ToString();
            dRow["PeriodRent"] = Convert.ToDecimal(dr["PeriodRent"].ToString());
            dRow["TotalSF"] = dr["TotalSqFt"].ToString();
            dRow["LeaseTermId"] = Convert.ToInt32(dr["LeaseTermId"].ToString());
            dRow["TermPeriod"] = Convert.ToInt32(dr["TermPeriod"].ToString());
            tbDest.Rows.Add(dRow);
            txtLeaseTermMonths.Text = "";
            txtLeaseTermMonths.Text = dr["TermPeriod"].ToString();
        }
        //05/09/2013-------
        gvBaseLeaseTerm.DataSource = null;
        gvBaseLeaseTerm.DataBind();

        //05/09/2013-------
        gvBaseLeaseTerm.DataSource = tbDest;
        gvBaseLeaseTerm.DataBind();

            //JAY
            //gvbasecommtermsedit.DataSource = tbDest;
            //gvbasecommtermsedit.DataBind();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "SetLeaseTermGrid", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private void SetCommTermGrid(DataTable tbSource, DataTable tbDest)
    {
        try
        { 
        if (tbSource != null)
        {
            var firstrow = tbSource.Rows[0]["PercentBrokerComm"].ToString();
            var flag = 0;
            foreach (DataRow dr in tbSource.Rows)
            {
                dRow = tbDest.NewRow();
                dRow["RentFromPeriod"] = dr["RentPeriodFrom"];
                dRow["RentToPeriod"] = dr["RentPeriodTo"];
                dRow["TotalMonths"] = dr["TotalMonths"];
                dRow["BrokerCommission"] = dr["PercentBrokerComm"];
                dRow["Fee"] = dr["Fee"];
                dRow["CommissionableTermId"] = Convert.ToInt32(dr["CommissionableTermId"].ToString());
                dRow["CommTermPeriod"] = Convert.ToInt32(dr["CommTermPeriod"].ToString());
                txtBaseCommTermMonths.Text = "";
                txtBaseCommTermMonths.Text = dr["CommTermPeriod"].ToString();
                tbDest.Rows.Add(dRow);
                if (firstrow == dr["PercentBrokerComm"].ToString())
                {
                    txtBaseCommPer.Text = dr["PercentBrokerComm"].ToString();
                }
                else
                {
                    flag += 1;
                }

            }
            if (flag != 0)
            {
                txtBaseCommPer.Text = "";
            }


            //05/09/2013------
            //gvBaseCommTerms.DataSource = null;
            //gvBaseCommTerms.DataBind();
            ////05/09/2013------
            //gvBaseCommTerms.DataSource = tbDest;
            //gvBaseCommTerms.DataBind();


            //JAY
            gvBaseCommTerms.DataSource = tbDest;
            gvBaseCommTerms.DataBind();
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "SetCommTermGrid", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void UpdateDataTableForTermPerid(string UcName)
    {
        try
        { 
        DataRow dR;
        int i;
        dtBasicLeaseTerm = GetRelativeDataTable(UcName, cns_dtBasicLeaseTerm);
        if (dtBasicLeaseTerm.Rows.Count > 0)
        {
            for (i = 0; i < dtBasicLeaseTerm.Rows.Count; i++)
            {
                dR = dtBasicLeaseTerm.Rows[i];
                dR.BeginEdit();
                dR["TermPeriod"] = txtLeaseTermMonths.Text == "" ? 0 : Convert.ToInt32(txtLeaseTermMonths.Text);
                dR.AcceptChanges();
            }
        }
        dtCommTerm = GetRelativeDataTable(UcName, cns_dtCommTerm);
        if (dtCommTerm != null)
        {
            if (dtCommTerm.Rows.Count > 0)
            {
                for (i = 0; i < dtCommTerm.Rows.Count; i++)
                {
                    dR = dtCommTerm.Rows[i];
                    dR.BeginEdit();
                    dR["CommTermPeriod"] = txtBaseCommTermMonths.Text == "" ? 0 : Convert.ToInt32(txtBaseCommTermMonths.Text);
                    dR.AcceptChanges();
                }
            }
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "UpdateDataTableForTermPerid", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void SetCSData()
    {
        try
        { 
        if (Session[GlobleData.NewChargeSlipObj] != null)
        {
            ChargeSlip objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
            //Convert Size From Acres To SQ Feets For  Size
            if (objChargeSlip.DealTransactionsProperty.DealSubType == 3 && (objChargeSlip.DealTransactionsProperty.UnitValue == 1 && objChargeSlip.DealTransactionsProperty.UnitTypeValue == 2))
            {
                Utility.ConvertToSF(ref SizeSF, objChargeSlip.DealTransactionsProperty.Size);
            }
            //Cal Rent Per SQF
            if (SizeSF == 0)
            {
                //RentPSF = objChargeSlip.DealTransactionsProperty.Nets / objChargeSlip.DealTransactionsProperty.Size;
                ViewState.Add("TotalSF", objChargeSlip.DealTransactionsProperty.Size);
                hdnSizeSF.Value = objChargeSlip.DealTransactionsProperty.Size.ToString();
                lblTotalSF.Text = objChargeSlip.DealTransactionsProperty.Size.ToString();
            }
            else
            {
                ViewState.Add("TotalSF", SizeSF);
                hdnSizeSF.Value = SizeSF.ToString();
                lblTotalSF.Text = SizeSF.ToString();
            }


            dtBasicLeaseTerm = (DataTable)Session[cns_dtBasicLeaseTerm];

            //set status
            if (GlobleData.cns_Status_Complete == objChargeSlip.Status)
            {
                Session[GlobleData.cns_Is_Complete] = true;
                if (this.Parent.FindControl("ContentFooter") != null)
                {
                    if (this.Parent.FindControl("ContentFooter").FindControl("btnSaveForLater") != null)
                    {
                        Button btnSaveForLater = (Button)this.Parent.FindControl("ContentFooter").FindControl("btnSaveForLater");
                        btnSaveForLater.Visible = false;
                    }
                }
            }
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "SetCSData", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void CheckForNewChages()
    {

    }
    #endregion

    #region TextBox Event
    protected void txtBaseCommTermMonths_TextChanged(object sender, EventArgs e)
    {
        try
        { 
        //if (hdnPrevMonth.Value != "")
        //{
        int LeaseMonths = txtLeaseTermMonths.Text == "" ? 0 : Convert.ToInt32(txtLeaseTermMonths.Text);
        int BaseCommMonths = txtBaseCommTermMonths.Text == "" ? 0 : Convert.ToInt32(txtBaseCommTermMonths.Text);

        if (LeaseMonths != 0 && BaseCommMonths != 0 && LeaseMonths >= BaseCommMonths)
        {
            chkParent = this.ClientID;
            AddNewRecordToCommissionableTermSameAsLeaseTerm(chkParent);
        }
        else if (BaseCommMonths == 0)
        {
            chkParent = this.ClientID;
            AddNewRecordToCommissionableTermSameAsLeaseTerm(chkParent);
        }
        hdnSetTrue.Value = "";
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "txtBaseCommTermMonths_TextChanged", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #endregion

    //    hdnPrevMonth.Value = txtBaseCommTermMonths.Text;
    //}

    //protected void btnDueDtAdd_Click(object sender, EventArgs e)
    //{
    //    GetDueDates(true);
    //}
    //protected void btnUPDgvBaseCommTerms_Click(object sender, EventArgs e)
    //{
    // }

    public void Savechanges()
    {
        try
        { 
        decimal totamount = 0;
        chkParent = Convert.ToString(this.ClientID);
        HiddenField hdnCommissionType = new HiddenField();
        if (chkParent.Contains(cns_ucBaseLeaseTerms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeBaseTerm") != null)
            {
                hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeBaseTerm");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (chkParent.Contains(cns_ucOptionalTerms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm") != null)
            {
                hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (chkParent.Contains(cns_ucContingentCommission))
        {
            if (this.Parent.FindControl("hdnCommissionTypeContingent") != null)
            {
                hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeContingent");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (chkParent.Contains(cns_ucOptional1Terms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm1") != null)
            {
                hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm1");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (chkParent.Contains(cns_ucOptional2Terms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm2") != null)
            {
                hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm2");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (chkParent.Contains(cns_ucOptional3Terms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm3") != null)
            {
                hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm3");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        foreach (GridViewRow gRow in gvBaseCommTerms.Rows)
        {
            if (gRow.RowType == DataControlRowType.DataRow)
            {

                //GridViewRow gRow = gvBaseCommTerms.Rows[gRow.RowIndex];
                //gvBaseCommTerms.EditIndex = -1;
                if (gRow != null)
                {
                    TextBox txtEditBrokerCommission = (TextBox)gRow.FindControl("txtEditBrokerCommission");
                    TextBox txtEditFee = (TextBox)gRow.FindControl("txtEditFee");

                    TextBox lbItemFromRentPeriod = (TextBox)gRow.FindControl("lbItemFromRentPeriod");
                    TextBox lbItemToRentPeriod = (TextBox)gRow.FindControl("lbItemToRentPeriod");
                                       
                    dtCommTerm = GetRelativeDataTable(chkParent, cns_dtCommTerm);
                    dRow = dtCommTerm.Rows[gRow.RowIndex];
                    dRow.BeginEdit();
                    if (hdnCommissionTypeId.Value == "1")
                    {
                        dRow["BrokerCommission"] = txtEditBrokerCommission.Text;
                    }

                    dRow["Fee"] = txtEditFee.Text;
                    totamount = totamount + Convert.ToDecimal(dRow["Fee"].ToString());
                    dRow["CommTermPeriod"] = txtBaseCommTermMonths.Text == "" ? 0 : Convert.ToInt32(txtBaseCommTermMonths.Text);
                    if (lbItemFromRentPeriod != null && lbItemToRentPeriod != null)
                    {
                        dRow["RentFromPeriod"] = lbItemFromRentPeriod.Text;
                        dRow["RentToPeriod"] = lbItemToRentPeriod.Text;
                        dRow["TotalMonths"] = Convert.ToInt32(dRow["RentToPeriod"]) - Convert.ToInt32(dRow["RentFromPeriod"])+1;
                    }
                    dRow.AcceptChanges();
                    
                }
            }
        }
        ScriptManager.RegisterStartupScript(upBaseLeaseTerm, this.GetType(), "ScriptSumm", "javascript:UpdateDueDate('" + this.ClientID + "','" + totamount + "');", true);
        Session.Add(cns_dtCommTerm + SetRelativeTable(chkParent), dtCommTerm);
        BindCommissionableTermTable(chkParent);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "Savechanges", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void btnsavechanges_Click(object sender, EventArgs e)
    {
        try
        { 
        chkParent = Convert.ToString(this.ClientID);
        HiddenField hdnCommissionType = new HiddenField();
        if (chkParent.Contains(cns_ucBaseLeaseTerms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeBaseTerm") != null)
            {
                hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeBaseTerm");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (chkParent.Contains(cns_ucOptionalTerms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm") != null)
            {
                hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (chkParent.Contains(cns_ucContingentCommission))
        {
            if (this.Parent.FindControl("hdnCommissionTypeContingent") != null)
            {
                hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeContingent");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (chkParent.Contains(cns_ucOptional1Terms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm1") != null)
            {
                hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm1");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (chkParent.Contains(cns_ucOptional2Terms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm2") != null)
            {
                hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm2");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        else if (chkParent.Contains(cns_ucOptional3Terms))
        {
            if (this.Parent.FindControl("hdnCommissionTypeOptionTerm3") != null)
            {
                hdnCommissionType = (HiddenField)this.Parent.FindControl("hdnCommissionTypeOptionTerm3");
                hdnCommissionTypeId.Value = hdnCommissionType.Value;
            }
        }
        foreach (GridViewRow gRow in gvBaseCommTerms.Rows)
        {
            if (gRow.RowType == DataControlRowType.DataRow)
            {

                //GridViewRow gRow = gvBaseCommTerms.Rows[gRow.RowIndex];
                //gvBaseCommTerms.EditIndex = -1;
                if (gRow != null)
                {
                    TextBox txtEditBrokerCommission = (TextBox)gRow.FindControl("txtEditBrokerCommission");
                    TextBox txtEditFee = (TextBox)gRow.FindControl("txtEditFee");

                    dtCommTerm = GetRelativeDataTable(chkParent, cns_dtCommTerm);
                    dRow = dtCommTerm.Rows[gRow.RowIndex];
                    dRow.BeginEdit();
                    if (hdnCommissionTypeId.Value == "1")
                    {
                        dRow["BrokerCommission"] = txtEditBrokerCommission.Text;
                    }
                    //dRow["BrokerCommission"] = txtEditBrokerCommission.Text;
                    dRow["Fee"] = txtEditFee.Text;
                    dRow["CommTermPeriod"] = txtBaseCommTermMonths.Text == "" ? 0 : Convert.ToInt32(txtBaseCommTermMonths.Text);
                    dRow.AcceptChanges();

                }
            }
        }
        Session.Add(cns_dtCommTerm + SetRelativeTable(chkParent), dtCommTerm);
        BindCommissionableTermTable(chkParent);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "btnsavechanges_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    protected void ddlCommissionType_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Session["NotifyDays"] = txtNotify.Value;
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "SetSumm", "javascript:VisibleNotifyButton();", true);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "btnSave_Click", ex.Message);
        }
    }


    public void BindNotificationDaysByChargeSlip()
    {
        try
        {
            if (Session[GlobleData.NewChargeSlipId] != null)
            {
                CommissionDueDatesNotification objCommissionDueDatesNotification = new CommissionDueDatesNotification();
                //long ChargeSlipId = 182085;
                objCommissionDueDatesNotification.ChargeSlipId = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                DataSet ds = objCommissionDueDatesNotification.BindNotificationDaysByChargeSlip(objCommissionDueDatesNotification.ChargeSlipId);
                string IsNotifyOn = string.Empty;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        IsNotifyOn = string.Empty;
                        IsNotifyOn = Convert.ToString(row["IsNotifyOn"]);
                        if(!(string.IsNullOrEmpty(IsNotifyOn)))
                        {
                            txtNotify.Value = IsNotifyOn;
                        }
                       else
                        {
                            txtNotify.Value = Session["NotifyDays"].ToString();
                        }
                        Session["NotifyDays"] = txtNotify.Value;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucBaseLeaseAndOptionTerms.ascx", "BindNotificationDaysByChargeSlip", ex.Message);
        }
    }
}