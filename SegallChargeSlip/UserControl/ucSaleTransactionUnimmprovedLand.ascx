﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucSaleTransactionUnimmprovedLand.ascx.cs" Inherits="UserControl_ucSaleTransactionUnimmprovedLand" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/UserControl/ucAddNewBuyerTenant.ascx" TagName="NewBuyerUnImpr" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucDueDtWiseCommission.ascx" TagName="DueDateWiseCommission" TagPrefix="uc" %>
<%--<link href="../Styles/SiteTEST.css" rel="stylesheet" />--%>
<script src="../Js/calendar.js"></script>
<link href="../Styles/calendar.css" rel="stylesheet" />
<style>
    .wordbreak {
        word-break: break-all;
    }
</style>
<script type="text/javascript">
 

    function AutoFillCommision(Type) {
        debugger;
        var SalePrice = parseFloat(document.getElementById("<%=txtSalePrice.ClientID%>").value);
        
        var CommPer = document.getElementById("<%=txtCommissionPer.ClientID %>").value;
        var SetFee = document.getElementById("<%=txtSetFee.ClientID %>").value;

        if (SalePrice != "") {

            if (Type == "CP" && CommPer != "") {
                var CalSetFee = (parseFloat(SalePrice) * parseFloat(CommPer)) / (100);
                document.getElementById("<%=txtSetFee.ClientID %>").value = SetFee = CalSetFee;
            }
            else if (Type == "CP" && CommPer == "") {
                document.getElementById("<%=txtSetFee.ClientID %>").value = SetFee = "";
            }

        if (Type == "SF" && SetFee != "") {
            var CalCommPer = (parseFloat(SetFee) * 100) / (parseFloat(SalePrice));
            document.getElementById("<%=txtCommissionPer.ClientID %>").value = CommPer = CalCommPer;
            return false;
        }
        else if (Type == "SF" && SetFee == "") {
            document.getElementById("<%=txtCommissionPer.ClientID %>").value = CommPer = "";
                return false;
            }
    }

    if (Type == "SP") {
        debugger;
        if (CommPer != "") {
            var CalSetFee = (parseFloat(SalePrice) * parseFloat(CommPer)) / (100);
            document.getElementById("<%=txtSetFee.ClientID %>").value = SetFee = CalSetFee;
        }
        else if (SetFee != "") {
            var CalCommPer = (parseFloat(SetFee) * 100) / (parseFloat(SalePrice));
            document.getElementById("<%=txtCommissionPer.ClientID %>").value = CommPer = CalCommPer;
        }
        else {
            document.getElementById("<%=txtSetFee.ClientID %>").value = SetFee = "";
            document.getElementById("<%=txtCommissionPer.ClientID %>").value = CommPer = "";
        }
}
        // JAY 
        // document.getElementById("hdnis1st.ClientID %>").value = "1";
        var Comsetfee = document.getElementById("<%=txtSetFee.ClientID %>");
        Comsetfee.onchange();
    return false;
}




function addDueDateRecords(Parent, PanelId) {
    debugger;
    ScrollUp(PanelId);
    var btn = Parent;
    btn = btn.replace("btnAddDueDtComm", "");

    var DueDtStr = btn + "txtAddDuedt";
    var CommPerOnDueDtStr = btn + "txtAddPer";
    var CommAmountDueDtStr = btn + "txtAddAmount";
    var randomnumberStr = btn + "hdnDueDatetbCount";
    var opTableStr = btn + "tbDueWiseCommission";
    var NwRow;


    var DueDate = document.getElementById(DueDtStr).value;
    var CommPerOnDueDt = document.getElementById(CommPerOnDueDtStr).value;

    if (CommPerOnDueDt > 100) {
        alert("Commission Percentage Cannot Be Greater Than 100 %");
        return false;
    }
    else {
        if (DueDate != "" && CommPerOnDueDt != "") {
            var randomnumber = parseFloat(document.getElementById(randomnumberStr).value);
            var PerCommissionTot;
            PerCommissionTot = 0;

            var opTable = document.getElementById(opTableStr);

            //for (var r = 1, n = opTable.rows.length; r < n; r = r + 2) {
            //    PerCommissionTot += parseFloat(opTable.rows.item(r).cells.item(1).firstChild.innerText);
            //}

            //if (PerCommissionTot + parseFloat(CommPerOnDueDt) > 100) {
            //    alert("Total Commission Percentage is: " + PerCommissionTot + "  Commission Percentage Cannot Be Greater Than 100 %");
            //    return false;
            //}

            var rowCount = opTable.rows.length;
            var cellCount = opTable.rows[0].cells.length;
            var row = opTable.insertRow(rowCount);
            var cell_1 = row.insertCell(0);
            rowCount = rowCount;

            var element_1 = document.createElement("input");
            element_1.type = "text";
            element_1.name = 'txtCommPerDuedt_' + rowCount;
            element_1.id = 'txtCommPerDuedt_' + rowCount;
            element_1.value = DueDate;
            cell_1.appendChild(element_1);
            calendar.set('txtCommPerDuedt_' + rowCount);

            var row = opTable.insertRow(rowCount + 1);
            var cell_2 = row.insertCell(0);
            var element_2 = document.createElement("input");
            element_2.type = "text";
            element_2.name = 'txtCommAmtDuedt_' + rowCount;
            element_2.id = 'txtCommAmtDuedt_' + rowCount;
            element_2.value = CommPerOnDueDt + '%';
            cell_2.appendChild(element_2);

            var cell_3 = row.insertCell(1);
            var element_3 = document.createElement("input");
            element_3.src = " ../Images/delete.gif";
            element_3.type = "image";
            element_3.name = 'btnDelDueDate_' + rowCount;
            element_3.id = 'btnDelDueDate_' + rowCount;
            element_3.setAttribute('onclick', 'javascript:DeleteDueDate("' + rowCount + '","' + opTableStr + '");return false;');
            cell_3.appendChild(element_3);

            randomnumber = randomnumber + 2;
            document.getElementById(randomnumberStr).value = randomnumber;
        }
    }
    return false;
}


//function DeleteDueDate(Row, TableStr) {
//    var DetTable = document.getElementById(TableStr);
//    //  var SummTable = document.getElementById("ContentMain_ucSummery_tbdueDateSumm");
//    DetTable.deleteRow(Row - 1);
//    SummTable.deleteRow(Row - 2);
//    return false;
//}

//function ScrollDown(parent) {
//    $("#" + parent).slideDown('fast');
//    return false;
//}

//function ScrollUp(parent) {
//    $("#" + parent).slideUp('fast');
//    return true;
//}

//function ScrollToggle(parent) {
//    $("#" + parent).slideToggle('fast');
//    return false;
//}

//$(document).ready(function () {

//    calendar.set("ContentMain_ucSaleUnImprovedLand_txtDate");
//});

</script>

<script type="text/javascript">
    function ShowIcon(sender, e) {
        sender._element.className = "loadingUC";
    }

    function hideIcon(sender, e) {
        sender._element.className = "textwidthUC";
    }

    function OnBuyerSelected(source, eventArgs) {
        document.getElementById('<%=hdnBuyerId.ClientID %>').value = eventArgs.get_value();
        //   alert(eventArgs.get_value());
        return false;
    }
   
</script>


<table class="ucbackground" style="width: 100%">
    <tr>
        <td colspan="3" class="ucheadings">SALE TRANSACTION: UNIMPROVED LAND
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">SIZE
        </td>
        <td colspan="2">
            <asp:TextBox ID="txtSize" runat="server" CssClass="uctextboxMed" TabIndex="23"></asp:TextBox>
            &nbsp;
            <asp:DropDownList ID="ddlSizeMeasure" runat="server" CssClass="ucddlSmall">
                <asp:ListItem Text="Sq Ft" Value="1"></asp:ListItem>
                <asp:ListItem Text="Acres" Value="2"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">BUYER
        </td>
        <td colspan="2">
            <%--     <ajax:ComboBox ID="ajaxCombBuyer" runat="server" AutoCompleteMode="SuggestAppend" RenderMode="Block" CssClass="ddlHeight"  Height="18px">
            </ajax:ComboBox>--%>
            <asp:TextBox runat="server" ID="txtBuyer" CssClass="uctextboxMed" AutoPostBack="true"  OnTextChanged="txtBuyer_TextChanged" TabIndex="24"/>
            <ajax:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server"
                TargetControlID="txtBuyer"
                DelimiterCharacters=";, :"
                MinimumPrefixLength="1"
                EnableCaching="true"
                CompletionSetCount="10"
                CompletionInterval="300"
                ServiceMethod="SearchGetDealPartiesList"
                OnClientPopulating="ShowIcon"
                OnClientPopulated="hideIcon"
                OnClientItemSelected="OnBuyerSelected"
                ShowOnlyCurrentWordInCompletionListItem="true"
                CompletionListCssClass="AutoExtender"
                CompletionListItemCssClass="AutoExtenderList"
                CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                CompletionListElementID="divwidth" />
            <asp:HiddenField ID="hdnBuyerId" runat="server" />

            <ajax:ModalPopupExtender ID="mpeAddNewBuyerUnImpr" TargetControlID="btnAddNewBuyerUnImpr" CancelControlID="ucNwBuyer$ImgbtnCancel"
                PopupControlID="dvNwBuyerUnImpr" runat="server" BackgroundCssClass="modalBackground">
            </ajax:ModalPopupExtender>
            &nbsp;
             <asp:ImageButton ID="btnAddNewBuyerUnImpr" runat="server" CssClass="imgSizeSmall" ImageUrl="~/Images/Add-Icon.gif" />
            <%--  <asp:Button ID="btnAddNewBuyerUnImpr" runat="server" Text="Add New" CssClass="SqureButton" Width="90px" />OnClientClick="ClearLabel();"--%>
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">USE
        </td>
        <td colspan="2">
            <asp:DropDownList ID="ddlUse" runat="server" CssClass="ddlSmall" TabIndex="25">
                <%-- <asp:ListItem Text="-Select-" Value="Select"></asp:ListItem>
               <asp:ListItem Text="RETAIL" Value="Retail"></asp:ListItem>
                <asp:ListItem Text="RESTAURANT" Value="Restaurant"></asp:ListItem>--%>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td class="uclabels">SALE PRICE
        </td>
        <td colspan="2">
            <asp:TextBox ID="txtSalePrice" runat="server" CssClass="uctextboxMed DollarTextBox" Width="140px" onchange="return AutoFillCommision('SP')" TabIndex="26"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>

    <tr>
        <td class="uclabels">COMMISSION
        </td>
        <td colspan="2">
            <table>
                <tr>
                    <td><b>%</b></td>
                    <td style="padding-left: 12px;">
                        <asp:TextBox ID="txtCommissionPer" runat="server" CssClass="uctextboxSmall" onchange="return AutoFillCommision('CP')" TabIndex="27"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td><b>SET FEE</b></td>
                    <td style="padding-left: 12px;">
                        <asp:TextBox ID="txtSetFee" Height="15px" CssClass="DollarTextBox" runat="server" Width="69px" onchange="return AutoFillCommision('SF')" TabIndex="28"></asp:TextBox></td>
                </tr>
            </table>
        </td>
        <%--<td style="width: 25%">%<br />
            SET FEE
        </td>
        <td style="width: 25%">
            <asp:TextBox ID="txtCommissionPer" runat="server" Width="80px" onchange="return AutoFillCommision('CP')"></asp:TextBox><br />
            <asp:TextBox ID="txtSetFee" runat="server" Width="80px" onchange="return AutoFillCommision('SF')"></asp:TextBox>
        </td>--%>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <%--  <tr>
        <td style="vertical-align: top; width: 25%; padding-left: 15px; height: 18px;"><b>DUE DATE</b>
        </td>
        <td>
            <div style="max-height: 200px; overflow: scroll;">
                <asp:Table ID="tbDueWiseCommission" runat="server">
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">
                            <asp:TextBox ID="txtDueDate" runat="server" Width="130px" Height="18px"></asp:TextBox>
                            <asp:ImageButton ID="btnCalender" runat="server" Width="16px" Height="16px" ImageUrl="~/Images/Calander.jpeg" />
                            <ajax:CalendarExtender ID="ajaxCal" runat="server" TargetControlID="txtDueDate" PopupButtonID="btnCalender" Format="dd/MM/yyyy"></ajax:CalendarExtender>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCommPerDuedt" runat="server" Width="40px" Height="18px"></asp:TextBox>
                            &nbsp; % &nbsp;
                        <asp:TextBox ID="txtCommAmtDuedt" runat="server" Width="100px" Height="18px"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:ImageButton ID="imgBtnDelDuedt" ImageUrl="~/Images/close.png" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </td>
        <td style="width: 45%; padding-left: 58px; height: 18px; vertical-align: top;">
            <asp:Button ID="btnAddDueDate" runat="server" Text="ADD" CssClass="SqureButton" Width="90px" />
        </td>
    </tr>--%>

    <tr>
        <td class="uclabels">DUE DATE
        </td>
        <td colspan="2">
            <asp:UpdatePanel ID="updDuedate" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <uc:DueDateWiseCommission ID="DueDateCommission" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>

    </tr>
    <tr>
        <td class="uclabels">NOTES
        </td>
        <td colspan="2" style="vertical-align: top;">
            <asp:TextBox ID="txtNotes" Rows="10" runat="server" CssClass="uctextArea" TextMode="MultiLine" Columns="35" TabIndex="35"></asp:TextBox>
            <div style="float: right; padding-right: 0px;">

                <asp:ImageButton ID="imgAttach" ImageUrl="~/Images/Attach-Icon.gif" CssClass="imgSizeBig" runat="server" />


                <ajax:ModalPopupExtender ID="mpExtendr" runat="server" PopupControlID="dvFileUploadUnImprove" TargetControlID="imgAttach" CancelControlID="btnCancelPopup" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>

            </div>
        </td>
    </tr>
    <tr>
        <td class="uclabels"></td>
        <td colspan="2">
            <asp:UpdatePanel ID="upAtttachment" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnAttachment" runat="server" Width="100%" BorderColor="Transparent" BorderStyle="Solid" BorderWidth="1px">
                        <asp:GridView ID="gvAttach" GridLines="None" runat="server" AutoGenerateColumns="false" Width="90%" ShowHeader="false">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Image ID="imgDot" runat="server" ImageUrl="~/Images/bullet.gif" BackColor="Gray" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="AttachmentDescription" HeaderText="Attachment" ItemStyle-CssClass="wordbreak" />
                                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtDelete" OnClientClick="return confirm('Are you sure want to delete')" ImageUrl="~/Images/delete.gif" runat="server" OnClick="imgbtDelete_Click" CommandArgument='<%# Eval("UniqueAttachmentDescription") %>'></asp:ImageButton>

                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </ContentTemplate>

            </asp:UpdatePanel>
            <%-- <hr style="width: 100%" />--%>
        </td>
    </tr>
</table>

<div style="display: none; height: 380px;" class="PopupDivBodyStyle" id="dvNwBuyerUnImpr">
    <uc:NewBuyerUnImpr ID="ucNwBuyer" runat="server" />
</div>
<div class="PopupDivBodyStyle" id="dvFileUploadUnImprove" style="display: none; height: 150px;">
    <asp:UpdatePanel ID="upAttachment" runat="server" UpdateMode="Conditional">

        <ContentTemplate>
            <table style="width: 100%" class="spacing">
                <tr style="background-color: gray; border-bottom: solid;">
                    <td style="text-align: left;">
                        <h3>&nbsp; &nbsp;File Upload</h3>
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="btnCancelPopup" Height="15px" Width="15px" ImageUrl="~/Images/close.png" runat="server" />

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:FileUpload ID="upFileUpload" runat="server" /></td>
                    <td style="padding-right: 10px">
                        <asp:Button ID="btnFileUpload" runat="server" CssClass="SqureButton" OnClientClick="SetPageLeave();" OnClick="btnFileUpload_Click" Text="Attach" />
                    </td>
                </tr>

            </table>
        </ContentTemplate>
        <Triggers>

            <asp:PostBackTrigger ControlID="btnFileUpload" />
        </Triggers>
    </asp:UpdatePanel>
</div>
