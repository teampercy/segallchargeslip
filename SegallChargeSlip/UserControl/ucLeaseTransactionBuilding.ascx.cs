﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SegallChargeSlipBll;
using System.IO;
public partial class UserControl_ucLeaseTransactionBuilding : System.Web.UI.UserControl
{
    const string cns_ucBaseLeaseTerms = "ucBaseLeaseTerms";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        { 
        if (!IsPostBack)
        {
            BindDropDown();
            SetAttributes();
            if (Session[GlobleData.NewChargeSlipObj] != null)
            {
                SetDataInControls();

                //Convert.ToString(DateTime.ParseExact(Convert.ToString(objDealTransactions.LeaseStartDate), "dd/MM/yyyy", null));
            }
            else
            {
                ResetControls();
            }
        }
        //else
        //{
        ucNwTenant.Title = "ADD NEW TENANT";
        ucNwTenant.ClassRef = "T";
            //}
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucLeaseTransactionBuilding.ascx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    #region Custom Action
    private void BindDropDown()
    {
        CustomList objCL = new CustomList();
        Utility.FillDropDown(ref ddlUse, objCL.GetDealUseTypes(), "DealUseType", "DealUseId");
    }
    public void SetProperties()
    {
        try
        { 
        DealTransactions objDealTransactions = new DealTransactions();
        objDealTransactions.Size = txtSize.Text.Trim() == "" ? 0 : Convert.ToDecimal(txtSize.Text);
        objDealTransactions.Tenant = hdnTenantID.Value == "" ? 0 : Convert.ToInt32(hdnTenantID.Value);
        if (objDealTransactions.Tenant == 0)
            objDealTransactions.DealPartyCompanyName = txtTenant.Text.Trim();
        objDealTransactions.DealUse = ddlUse.SelectedValue;
        objDealTransactions.DealUseText = ddlUse.SelectedItem.Text;
        if (!string.IsNullOrEmpty(txtLeaseStart.Text))
            objDealTransactions.LeaseStartDate = DateTime.ParseExact(txtLeaseStart.Text, "MM/dd/yyyy", null);

        if (!string.IsNullOrEmpty(txtCAM.Text))
            objDealTransactions.CAM = Convert.ToDecimal(txtCAM.Text);
        if (!string.IsNullOrEmpty(txtINS.Text))
            objDealTransactions.INS = Convert.ToDecimal(txtINS.Text);
        if (!string.IsNullOrEmpty(txtTAX.Text))
            objDealTransactions.TAX = Convert.ToDecimal(txtTAX.Text);
        if (!string.IsNullOrEmpty(txtNNN.Text))
            objDealTransactions.NNN = Convert.ToDecimal(txtNNN.Text);
        if (!string.IsNullOrEmpty(txtNNN.Text))
            objDealTransactions.Nets = Convert.ToDecimal(txtNNN.Text); //nets
        if (!string.IsNullOrEmpty(txtTI.Text))
            objDealTransactions.TI = Convert.ToDecimal(txtTI.Text);
        if (!string.IsNullOrEmpty(txtFreeRent.Text))
            objDealTransactions.FreeRent = Convert.ToDecimal(txtFreeRent.Text);

       
          
        objDealTransactions.Notes = txtNotes.Text;
        objDealTransactions.UnitValue = 1;
        objDealTransactions.UnitTypeValue = 1;//Always SF for size
        objDealTransactions.TIAllowanceType = Convert.ToInt32(ddlTIAllowanceType.SelectedItem.Value);// Added by Shishir 17-05-2016
        objDealTransactions.FreeRentUnitValue = 2;
        objDealTransactions.FreeRentUnitTypeValue = Convert.ToInt16(ddlFreeRentTime.SelectedValue);
        objDealTransactions.DealType = 2;
        objDealTransactions.DealSubType = 4;
        objDealTransactions.AdditionalNotes = ViewState["AddNotes"] == null ? "" : ViewState["AddNotes"].ToString();
        Session.Add(GlobleData.DealTransactionObj, objDealTransactions);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucLeaseTransactionBuilding.ascx", "SetProperties", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    private void ResetControls()
    {
        txtSize.Text = "";
        txtLeaseStart.Text = "";
        txtNotes.Text = "";
        ddlUse.SelectedIndex = 0;
        txtCAM.Text = "";
        txtINS.Text = "";
        txtTAX.Text = "";
        txtNNN.Text = "";
        txtFreeRent.Text = "";
        ddlFreeRentTime.SelectedIndex = 0;
        ddlTIAllowanceType.SelectedValue = "1";
        //Tenant objTenant = new Tenant();
        //DataSet ds = objTenant.GetList();

        SetAttributes();
        // Utility.FillDropDown(ref ajaxCombTenant, ds.Tables[0], "TenantName", "TenantID");
    }

    private void SetDataInControls()
    {
        try
        { 
        ChargeSlip objChargeSlip = new ChargeSlip();
        objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
        if (objChargeSlip.DealTransactionsProperty.DealSubType == 4)
        {
            DealParties objTenant = new DealParties();
            objTenant.PartyID = objChargeSlip.DealTransactionsProperty.Tenant;
            hdnTenantID.Value = objChargeSlip.DealTransactionsProperty.Tenant.ToString();
            objTenant.Load();

            txtSize.Text = Convert.ToString(Convert.ToInt32(objChargeSlip.DealTransactionsProperty.Size));
            HiddenField hdnSize = null, hdnOldUnitValue = null, hdnOldUnitTypeValue=null;
            Control ctl = this.Parent;
            while (true)
            {
                hdnSize = (HiddenField)ctl.FindControl("hdnSize");
                if (hdnSize != null)
                {
                    hdnSize.Value = Convert.ToString(Convert.ToInt32(objChargeSlip.DealTransactionsProperty.Size));
                }
                hdnOldUnitValue = (HiddenField)ctl.FindControl("hdnOldUnitValue");
                if (hdnOldUnitValue != null)
                {
                    hdnOldUnitValue.Value = Convert.ToString(Convert.ToInt32(objChargeSlip.DealTransactionsProperty.UnitValue));
                }
                hdnOldUnitTypeValue = (HiddenField)ctl.FindControl("hdnOldUnitTypeValue");
                if (hdnOldUnitTypeValue != null)
                {
                    hdnOldUnitTypeValue.Value = Convert.ToString(Convert.ToInt32(objChargeSlip.DealTransactionsProperty.UnitTypeValue));
                }
                break;
            }

            if (!string.IsNullOrEmpty(txtSize.Text))
            {
                //Comment by Jaywanti on 31-08-2015
                //LeaseTerms objLeaseTerm = new LeaseTerms();
                //objLeaseTerm.ChargeSlipId = objChargeSlip.ChargeSlipID;
                //DataSet dslease = null;
                //dslease = objLeaseTerm.GetListByChargeSlip();
                //if (dslease.Tables[0] != null)
                //{
                //    if (dslease.Tables[0].Rows.Count > 0)
                //    {
                //        txtSize.ReadOnly = true;
                //    }
                //    else
                //    {
                //        txtSize.ReadOnly = false;
                //    }
                //}
                //else
                //    txtSize.ReadOnly = false;
                //Added by Jaywanti on 31-08-2015
                CommissionDueDates objCommissionDueDates = new CommissionDueDates();
                objCommissionDueDates.ChargeSlipId = objChargeSlip.ChargeSlipID;
                DataTable dueDt = objCommissionDueDates.CheckPaymentpaid();
                Boolean IsPaymentComp;
                if (dueDt != null)
                {
                    if (dueDt.Rows.Count > 0)
                    {
                        //Session[GlobleData.cns_IsFullCommPaid] = true;
                        for (int i = 0; i <= dueDt.Rows.Count - 1; i++)
                        {
                            IsPaymentComp = Convert.ToBoolean(dueDt.Rows[i]["IsPayment"].ToString());
                            if (IsPaymentComp == true)
                            {
                                txtSize.ReadOnly = false;
                                break;
                            }
                            else
                            {
                                txtSize.ReadOnly = false;
                                break;
                            }
                        }
                    }
                }
                //----   vv------- marker : 12/06/2013 : Added to make building size readonly only if there is data in lease terms table, else allow editing.
                //DataTable dtBasicLeaseTerm = null;
                //if (Session["dtSaveTerms"] != null || dslease.Tables[0] != null)
                //{
                //    dtBasicLeaseTerm = (DataTable)Session["dtSaveTerms"];
                //    if (dtBasicLeaseTerm.Rows.Count > 0)
                //    {
                //        txtSize.ReadOnly = true;
                //    }
                //    else
                //    {
                //        txtSize.ReadOnly = false;
                //    }
                //}
                //----   ^^------- marker : 12/06/2013 : Added to make building size readonly only if there is data in lease terms table, else allow editing.
            }
            else
            {
                txtSize.ReadOnly = false;
            }
            txtLeaseStart.Text = objChargeSlip.DealTransactionsProperty.LeaseStartDate.ToString("MM/dd/yyyy");
            txtNotes.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.Notes);
            ddlUse.SelectedValue = objChargeSlip.DealTransactionsProperty.DealUse;
            txtCAM.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.CAM);
            txtINS.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.INS);
            txtTAX.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.TAX);
            txtNNN.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.NNN);
            txtFreeRent.Text = Convert.ToString(Convert.ToInt32(objChargeSlip.DealTransactionsProperty.FreeRent));
            ddlFreeRentTime.SelectedValue = Convert.ToString(objChargeSlip.DealTransactionsProperty.FreeRentUnitTypeValue);
            hdnTenantID.Value = Convert.ToString(objChargeSlip.DealTransactionsProperty.Tenant);
            //txtTenant.Text = objTenant.PartyName;
            txtTenant.Text = objTenant.PartyCompanyName;
            txtTI.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.TI);
            ddlTIAllowanceType.SelectedValue =objChargeSlip.DealTransactionsProperty.TIAllowanceType==null?"0":Convert.ToString(objChargeSlip.DealTransactionsProperty.TIAllowanceType);
            // For Additional Notes
            ViewState["AddNotes"] = Convert.ToString(objChargeSlip.DealTransactionsProperty.AdditionalNotes);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "RebindTenantAllowanceBuilding", "CalculateAmount();", true);

        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucLeaseTransactionBuilding.ascx", "SetDataInControls", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    private void SetAttributes()
    {
        txtSize.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtSize.ClientID + "')");
        //txtCAM.Attributes.Add("onkeyup", "javascript:return AllowDecimalsNet('" + txtCAM.ClientID + "')");
        //txtINS.Attributes.Add("onkeyup", "javascript:return AllowDecimalsNet('" + txtINS.ClientID + "')");
        //txtTAX.Attributes.Add("onkeyup", "javascript:return AllowDecimalsNet('" + txtTAX.ClientID + "')");
        //txtNNN.Attributes.Add("onkeyup", "javascript:return AllowDecimalsNet('" + txtNNN.ClientID + "')");
        txtCAM.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtCAM.ClientID + "')");
        txtINS.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtINS.ClientID + "')");
        txtTAX.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtTAX.ClientID + "')");
        txtNNN.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtNNN.ClientID + "')");
        txtTI.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtTI.ClientID + "')");
        txtFreeRent.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtFreeRent.ClientID + "')");
        //txtFreeRent.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtFreeRent.ClientID + "')");//Commented by Jaywanti on 24-08-2015
    }


    #endregion
    protected void txtTenant_TextChanged(object sender, EventArgs e)
    {
        try
        { 
        DealParties objdealParties = new DealParties();
        if (hdnTenantID.Value != "")
        {
            objdealParties.PartyID = Convert.ToInt32(hdnTenantID.Value);
            objdealParties.Load();
            if (objdealParties.PartyCompanyName != txtTenant.Text)
                hdnTenantID.Value = "";
        }
        txtTenant.Focus();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucLeaseTransactionBuilding.ascx", "txtTenant_TextChanged", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }
}