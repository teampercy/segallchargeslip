﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;

public partial class UserControl_ucAddNewBuyerTenant : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        { 
        if (!IsPostBack)
        {
            ResetControls();
        }
        //else
        //{
            lbHaeding.Text = Title;
            //}
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewBuyerTenant.ascx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #region Properties

    public string Title
    {
        get;
        set;
    }


    public string ClassRef
    {
        get;
        set;
    }

    #endregion

    #region Button Events
    protected void AddNew(object sender, EventArgs e)
    {
        try
        { 
        //if (ValidateBeforeInsert() == false)
        //{
        //    return;
        //}
        lbErrorMsg.Text = "";

        //Choose obj of which class to be invoked

        //Perform validation before save
        DealParties objDp = new DealParties();
        objDp.PartyCompanyName = txtComapanyName.Text.Trim();
        objDp.Email = txtEmail.Text.Trim();
        objDp.PartyName = txtName.Text.Trim();
        objDp.Address1 = txtAdd1.Text.Trim();
        objDp.Address2 = txtAdd2.Text.Trim();
        objDp.City = txtCity.Text.Trim();
        if(!string.IsNullOrEmpty(ddlState.SelectedValue))
            objDp.State = Convert.ToString(ddlState.SelectedValue);
        if (!string.IsNullOrEmpty(txtZipCode.Text.Trim()))
            objDp.ZipCode = Convert.ToString(txtZipCode.Text.Trim());
        objDp.CreatedBy = Convert.ToInt32(Session["UserId"]);
        objDp.CreatedOn = DateTime.Now;
        objDp.IsActive = true;
        if (ClassRef == "B")
        {
            objDp.PartyTypeId = 1;
            bool rslt = objDp.Save();
            if (rslt == true)
            {
                TextBox txtBuyer = (TextBox)this.Parent.FindControl("txtBuyer");
                HiddenField hdnBuyerID = (HiddenField)this.Parent.FindControl("hdnBuyerId");

                txtBuyer.Text = txtComapanyName.Text.Trim();
                hdnBuyerID.Value = Convert.ToString(objDp.PartyID);

            }
        }
        else if (ClassRef == "T")
        {
            objDp.PartyTypeId = 2;
            bool rslt = objDp.Save();
            if (rslt == true)
            {
                TextBox txtTenant = (TextBox)this.Parent.FindControl("txtTenant");
                HiddenField hdnTenantID = (HiddenField)this.Parent.FindControl("hdnTenantID");

                txtTenant.Text = txtComapanyName.Text.Trim();
                hdnTenantID.Value = Convert.ToString(objDp.PartyID);
            }

        }
        ResetControls();
            //if (ClassRef == "B")
            //{
            //    Buyer objB = new Buyer();
            //    objB.BuyerName = txtName.Text;
            //    objB.Address1 = txtAdd1.Text;
            //    objB.Address2 = txtAdd2.Text;
            //    objB.City = txtCity.Text;
            //    objB.State = Convert.ToInt32(ddlState.SelectedValue);
            //    objB.ZipCode = Convert.ToInt32(txtZipCode.Text);
            //    objB.CreatedBy = Convert.ToInt32(Session["UserId"]);
            //    objB.CreatedOn = DateTime.Now;
            //    bool rslt = objB.Save();
            //    if (rslt == true)
            //    {
            //        TextBox txtBuyer = (TextBox)this.Parent.FindControl("txtBuyer");
            //        HiddenField hdnBuyerID = (HiddenField)this.Parent.FindControl("hdnBuyerId");

            //        txtBuyer.Text = txtName.Text;
            //        hdnBuyerID.Value = Convert.ToString(objB.BuyerID);

            //    }
            //}
            //else if (ClassRef == "T")
            //{
            //    Tenant objT = new Tenant();
            //    objT.TenantName = txtName.Text;
            //    objT.Address1 = txtAdd1.Text;
            //    objT.Address2 = txtAdd2.Text;
            //    objT.City = txtCity.Text;
            //    objT.State = Convert.ToInt32(ddlState.SelectedValue);
            //    objT.ZipCode = Convert.ToInt32(txtZipCode.Text);
            //    objT.CreatedBy = Convert.ToInt32(Session["UserId"]);
            //    objT.CreatedOn = DateTime.Now;
            //    bool rslt = objT.Save();
            //    if (rslt == true)
            //    {
            //        TextBox txtTenant = (TextBox)this.Parent.FindControl("txtTenant");
            //        HiddenField hdnTenantID = (HiddenField)this.Parent.FindControl("hdnTenantID");

            //        txtTenant.Text = txtName.Text;
            //        hdnTenantID.Value = Convert.ToString(objT.TenantID);

            //    }
            //}
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucAddNewBuyerTenant.ascx", "AddNew", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Close_Window", "myWindow.Close();", true);
    }
    #endregion

    #region Custom Methods
    public void ResetControls()
    {
        Utility.FillStates(ref ddlState);
        txtComapanyName.Text = "";
        txtAdd1.Text = "";
        txtAdd2.Text = "";
        txtCity.Text = "";
        txtName.Text = "";
        txtZipCode.Text = "";
        lbErrorMsg.Text = "";
        txtEmail.Text = "";
        SetAttributes();

    }
    private void SetAttributes()
    {
        //txtZipCode.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtZipCode.ClientID + "')");
        btnAddNew.Attributes.Add("onclick", "javascript:return ValidateAll('" + this.ClientID + "')");
        ImgbtnCancel.Attributes.Add("onclick", "javascript:return ClearAll('" + this.ClientID + "')");
    }

    private Boolean ValidateBeforeInsert()
    {
        if (txtName.Text == String.Empty)
        {
            lbErrorMsg.Text = "Please Enter Name";
            txtName.Focus();
            return false;
        }
        if (txtAdd1.Text == String.Empty)
        {
            lbErrorMsg.Text = "Please Enter Address Line 1";
            txtAdd1.Focus();
            return false;
        }
        if (txtCity.Text == String.Empty)
        {
            lbErrorMsg.Text = "Please Enter City";
            txtCity.Focus();
            return false;
        }
        if (ddlState.SelectedIndex == 0)
        {
            lbErrorMsg.Text = "Please Select State";
            ddlState.Focus();
            return false;
        }
        if (txtZipCode.Text == String.Empty)
        {
            lbErrorMsg.Text = "Please Enter Zip Code";
            txtZipCode.Focus();
            return false;
        }
        return true;
    }

    #endregion

}