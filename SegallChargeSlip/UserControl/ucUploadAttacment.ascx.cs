﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using SegallChargeSlipBll;
using System.Data;

public partial class UserControl_ucUploadAttacment : System.Web.UI.UserControl
{
    DataTable dtAttachment;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        { 
        if (!IsPostBack)
        {
            ChargeSlip objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
            if (objChargeSlip != null)
            {
               // txtOldComment.Text = objChargeSlip.DealTransactionsProperty.Notes;
                txtNewComments.Text = objChargeSlip.DealTransactionsProperty.AdditionalNotes;
            }
            BindGrid();
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucUploadAttacment.ascx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void FileUpload_Click(object sender, EventArgs e)
    {
        try
        { 
        if (fupAttachment.HasFile)
        {

            string strGuid = string.Empty;
            strGuid = System.Guid.NewGuid().ToString();
            string ServerPath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
            string fileName = Path.GetFileName(fupAttachment.PostedFile.FileName);
            Attachments objAttachments = new Attachments();
            objAttachments.AttachmentDescription = strGuid + "_" + fileName;
            objAttachments.ChargeSlipID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
            objAttachments.CreatedBy = Convert.ToInt32(Session[GlobleData.User]);
            objAttachments.CreatedOn = DateTime.Now;
            objAttachments.FilePath = Server.MapPath(ServerPath) + strGuid + "_" + fileName;
            Boolean rslt = objAttachments.Save();
            //  Boolean rslt = objAttachments.Insert1();

            if (rslt == true)
            {
                if (!Directory.Exists(Server.MapPath(ServerPath)))
                { Directory.CreateDirectory(Server.MapPath(ServerPath)); }

                fupAttachment.PostedFile.SaveAs(Server.MapPath(ServerPath) + strGuid + "_" + fileName);
                BindGrid();
                upAttachment.Update();
            }
            else
            {
                string err = "Show Error";
            }

            //    Response.Redirect(Request.Url.AbsoluteUri);
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucUploadAttacment.ascx", "FileUpload_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void imgbtDelete_Click(object sender, ImageClickEventArgs e)
    {
        try
        { 
        string id = (sender as ImageButton).CommandArgument;
        Attachments objAttachment = new Attachments();
        objAttachment.AttachmentID = Convert.ToInt32(id);
        objAttachment.Load();



        string Serverpath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
        string pathfile = Serverpath + objAttachment.AttachmentDescription;

        string fullpath = Server.MapPath(pathfile);

        if (File.Exists(fullpath))
        {
            File.Delete(fullpath);
        }


        objAttachment.Delete();

        BindGrid();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucUploadAttacment.ascx", "imgbtDelete_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #region CustomActions

    public void BindGrid()
    {
        try
        { 
        // string[] filePaths = Directory.GetFiles(Server.MapPath("~/Uploads/"));

        //   string[] fileNames = Directory.GetFiles(Server.MapPath("~/Uploads/")).Select(path => Path.GetFileName(path))  .ToArray();

        List<ListItem> files = new List<ListItem>();
        Attachments objAttachments = new Attachments();
        objAttachments.ChargeSlipID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
        objAttachments.CreatedBy = Convert.ToInt32(Session[GlobleData.User]);
        dtAttachment = objAttachments.GetDetailsByUserAndChargeSlip();
        if (dtAttachment != null)
        {
            for (int i = 0; i <= dtAttachment.Rows.Count - 1; i++)
            {

                //string filePath = dtAttachment.Rows[i]["FilePath"].ToString();
                //files.Add(new ListItem(Path.GetFileName(filePath)));

                string filePath = dtAttachment.Rows[i]["FilePath"].ToString();
                int id = Convert.ToInt32(dtAttachment.Rows[i]["AttachmentID"].ToString());
                int count = Path.GetFileName(filePath).IndexOf('_');
                string filename = Path.GetFileName(filePath).Substring(count + 1);
                files.Add(new ListItem(filename, Convert.ToString(id)));

            }
            gvAttachment.DataSource = files;
            gvAttachment.DataBind();
            upAttachemntList.Update();
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucUploadAttacment.ascx", "BindGrid", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void InsertAddNotes()
    {
        try
        { 
        //if (!String.IsNullOrEmpty(txtNewComments.Text))
        //{
            DealTransactions objDealTransactions = new DealTransactions();
            objDealTransactions.ChargeSlipID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
            objDealTransactions.AdditionalNotes = txtNewComments.Text;
            objDealTransactions.AddNotesInsert();
            ChargeSlip objChargeSlip = new ChargeSlip();
            objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
            objChargeSlip.DealTransactionsProperty.AdditionalNotes = objDealTransactions.AdditionalNotes;

            //}
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ucUploadAttacment.ascx", "InsertAddNotes", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    #endregion

}