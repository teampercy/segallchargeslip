﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucBaseLeaseAndOptionTerms.ascx.cs"
    Inherits="UserControl_ucBaseLeaseAndOptionTerms" %>
<%--<% @ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<link href="../Styles/GridView.css" rel="stylesheet" />
<%--<script src="../Js/jquery-1.9.1.js"></script>
<script src="../Js/CommonValidations.js"></script>

<link href="../Styles/SiteTEST.css" rel="stylesheet" />
<script src="../Js/CommonValidations.js"></script>
--%>
<style>
    /*.padding {
        padding-left: 20px;
    }*/
    .wdthAll {
        width: 95%;
    }

    .tdDuePer {
        /*padding-left: 27px;
        width: 65px;*/
    }

    .tdDueDate {
        /*padding-left:15px;*/
    }

    input[type=text]:focus, textarea:focus {
        background-color: #F0F0F0;
        border: 2px solid #ccc;
    }

    .colGray {
        /*background-color: #D8D8D8;*/
    }

    .colyellow {
        /*background-color: #FFFF99;*/
    }

    .DollarTextBoxWithBorder {
        background-position-y: 2px;
        background-repeat: no-repeat;
        padding-left: 12px;
    }
</style>
<%--<script src="../Js/jquery-ui-1.10.3.custom%20(1)/Extension.min.js"></script>--%>
<script type="text/javascript">

    // window.onload = ClearTable();
    function UpdateCommissionTerm() {
        //alert('hi');
        document.getElementById('<%=btnUpdateCommTerm.ClientID%>').click();
        return false;
    }
    function ClearTable() {

        $("#ContentMain_ucBaseLeaseTerms_tbDueDates").empty();
        $("#ContentMain_ucOptionalTerms_tbDueDates").empty();
        $("#ContentMain_ucContingentCommission_tbDueDates").empty();
        $("#ContentMain_ucOptional1Terms_tbDueDates").empty();
        $("#ContentMain_ucOptional2Terms_tbDueDates").empty();
        $("#ContentMain_ucOptional3Terms_tbDueDates").empty();
        //var tb1 = document.getElementById("ContentMain_ucBaseLeaseTerms_tbDueDates");
        //var tb2 = document.getElementById("ContentMain_ucOptionalTerms_tbDueDates");
        //var tb3 = document.getElementById("ContentMain_ucContingentCommission_tbDueDates");

        //if (tb1 != null) {
        //    for (var r = 1, n = tb1.rows.length; r < n; r++) {
        //        tb1.deleteRow(r);
        //    }
        //}
        //if (tb2 != null) {
        //    for (var r = 1, n = tb2.rows.length; r < n; r++) {
        //        tb2.deleteRow(r);
        //    }
        //}
        //if (tb3 != null) {
        //    for (var r = 1, n = tb3.rows.length; r < n; r++) {
        //        tb3.deleteRow(r);
        //    }
        //}
    }

    function test1() {
        alert("test");
    }

    //function CheckDateEalier(sender, args) {

    //    var Parent = sender._body.id.replace('ajaxCal_body', '');

    //    var ToDay = new Date();
    //    var Month = ToDay.getMonth() + 1;
    //    var Day = ToDay.getDate();
    //    var Year = ToDay.getFullYear();
    //    var CurrDate = new Date(Month + "/" + Day + "/" + Year);



    //    if (Date.parse(sender._selectedDate) < Date.parse(CurrDate)) {
    //        alert("You Cannot Select A Day Before Today!");
    //        sender._selectedDate = new Date();
    //        // set the date back to the today
    //        sender._textbox.set_Value(sender._selectedDate.format(sender._format));
    //    }

    //    if (CheckForDuplicateDueDateEntry(sender._selectedDate.format(sender._format), Parent) == false) {
    //        sender._textbox.set_Value('');
    //    }
    //    //
    //}


    function CheckForDuplicateDueDateEntry(SelectedDate, Parent) {

        var opTableStr = Parent + "tbDueDates";
        var opTable = document.getElementById(opTableStr);
        var DueDate;
        if (opTable != null) {
            for (var r = 0; r < opTable.rows.length; r++) {
                if (opTable.rows[r].cells[1].firstChild != null) {
                    DueDate = opTable.rows[r].cells[0].children[0].value;
                    if (Date.parse(SelectedDate) == Date.parse(DueDate)) {
                        alert("Selected Date Entry Exist Already !");
                        return false;
                    }
                }
            }
        }
        return true;
    }

    function EnableDisableOnCheckChange1() {

        var SelChkOpt = $("#ContentMain_chkOptionTerm");
        var SelChkOptPayable = $("#ContentMain_chkOptionTermPayable");
        var SelChkContingent = $("#ContentMain_chkContingentComm");

        var SelChkOpt1 = $("#ContentMain_chkOptionTerm1");
        var SelChkOptPayable1 = $("#ContentMain_chkOptionTermPayable1");
        var SelChkOpt2 = $("#ContentMain_chkOptionTerm2");
        var SelChkOptPayable2 = $("#ContentMain_chkOptionTermPayable2");
        var SelChkOpt3 = $("#ContentMain_chkOptionTerm3");
        var SelChkOptPayable3 = $("#ContentMain_chkOptionTermPayable3");

        //ContentMain_ucContingentCommission_optPayable_2
        //ContentMain_ucBaseLeaseTerms_optPayable_3 ContentMain_chkOptionTerm  chkOptionTermPayable chkContingentComm


        if (SelChkOptPayable.is(':checked') && SelChkOpt.is(':checked')) {

            //optPayable 
            $("#ContentMain_ucOptionalTerms_optPayable").show();
            $("#ContentMain_ucOptionalTerms_optPayable_1").show();
            $("#ContentMain_ucOptionalTerms_optPayable_2").show();
            $("#ContentMain_ucOptionalTerms_optPayable_3").show();
            $("#ContentMain_ucOptionalTerms_optPayable_4").show();
            $("#ContentMain_ucOptionalTerms_optPayable_5").show();
            $("#ContentMain_ucOptionalTerms_optPayable_6").show();
        }
        else {
            $("#ContentMain_ucOptionalTerms_optPayable").hide();
            $("#ContentMain_ucOptionalTerms_optPayable_1").hide();
            $("#ContentMain_ucOptionalTerms_optPayable_2").hide();
            $("#ContentMain_ucOptionalTerms_optPayable_3").hide();
            $("#ContentMain_ucOptionalTerms_optPayable_4").hide();
            $("#ContentMain_ucOptionalTerms_optPayable_5").hide();
            $("#ContentMain_ucOptionalTerms_optPayable_6").hide();
        }

        if (SelChkOptPayable1.is(':checked') && SelChkOpt1.is(':checked')) {

            //optPayable 
            $("#ContentMain_ucOptional1Terms_optPayable").show();
            $("#ContentMain_ucOptional1Terms_optPayable_1").show();
            $("#ContentMain_ucOptional1Terms_optPayable_2").show();
            $("#ContentMain_ucOptional1Terms_optPayable_3").show();
            $("#ContentMain_ucOptional1Terms_optPayable_4").show();
            $("#ContentMain_ucOptional1Terms_optPayable_5").show();
            $("#ContentMain_ucOptional1Terms_optPayable_6").show();
        }
        else {
            $("#ContentMain_ucOptional1Terms_optPayable").hide();
            $("#ContentMain_ucOptional1Terms_optPayable_1").hide();
            $("#ContentMain_ucOptional1Terms_optPayable_2").hide();
            $("#ContentMain_ucOptional1Terms_optPayable_3").hide();
            $("#ContentMain_ucOptional1Terms_optPayable_4").hide();
            $("#ContentMain_ucOptional1Terms_optPayable_5").hide();
            $("#ContentMain_ucOptional1Terms_optPayable_6").hide();
        }

        if (SelChkOptPayable2.is(':checked') && SelChkOpt2.is(':checked')) {

            //optPayable 
            $("#ContentMain_ucOptional2Terms_optPayable").show();
            $("#ContentMain_ucOptional2Terms_optPayable_1").show();
            $("#ContentMain_ucOptional2Terms_optPayable_2").show();
            $("#ContentMain_ucOptional2Terms_optPayable_3").show();
            $("#ContentMain_ucOptional2Terms_optPayable_4").show();
            $("#ContentMain_ucOptional2Terms_optPayable_5").show();
            $("#ContentMain_ucOptional2Terms_optPayable_6").show();
        }
        else {
            $("#ContentMain_ucOptional2Terms_optPayable").hide();
            $("#ContentMain_ucOptional2Terms_optPayable_1").hide();
            $("#ContentMain_ucOptional2Terms_optPayable_2").hide();
            $("#ContentMain_ucOptional2Terms_optPayable_3").hide();
            $("#ContentMain_ucOptional2Terms_optPayable_4").hide();
            $("#ContentMain_ucOptional2Terms_optPayable_5").hide();
            $("#ContentMain_ucOptional2Terms_optPayable_6").hide();
        }

        if (SelChkOptPayable3.is(':checked') && SelChkOpt3.is(':checked')) {

            //optPayable 
            $("#ContentMain_ucOptional3Terms_optPayable").show();
            $("#ContentMain_ucOptional3Terms_optPayable_1").show();
            $("#ContentMain_ucOptional3Terms_optPayable_2").show();
            $("#ContentMain_ucOptional3Terms_optPayable_3").show();
            $("#ContentMain_ucOptional3Terms_optPayable_4").show();
            $("#ContentMain_ucOptional3Terms_optPayable_5").show();
            $("#ContentMain_ucOptional3Terms_optPayable_6").show();
        }
        else {
            $("#ContentMain_ucOptional3Terms_optPayable").hide();
            $("#ContentMain_ucOptional3Terms_optPayable_1").hide();
            $("#ContentMain_ucOptional3Terms_optPayable_2").hide();
            $("#ContentMain_ucOptional3Terms_optPayable_3").hide();
            $("#ContentMain_ucOptional3Terms_optPayable_4").hide();
            $("#ContentMain_ucOptional3Terms_optPayable_5").hide();
            $("#ContentMain_ucOptional3Terms_optPayable_6").hide();
        }

        return false;
    }

    function testAlert(ucName, DueDate, CommPerOnDueDt, CommAmtOnDueDt) {
        // alert(CommAmtOnDueDt);
        return false;
    }

    function addDueDateRecordsEditMode(ucName, DueDate, CommPerOnDueDt, CommAmtOnDueDt, Count, IsPayment) {
        debugger;
        // alert('hi');
        // alert(IsPayment);
        var IsFullCommPaid = '<%=Session["IsFullCommPaid"]%>';

        var opTableStr = ucName + "_tbDueDates";
        if (CommPerOnDueDt > 100) {
            alert("Commission Percentage Cannot Be Greater Than 100 %");
            return false;
        }
        else {
            if (DueDate != "" && CommPerOnDueDt != "") {
                //  var randomnumber = parseFloat(document.getElementById(randomnumberStr).value);
                var PerCommissionTot;
                PerCommissionTot = 0;

                var opTable = document.getElementById(opTableStr);


                //if (PerCommissionTot + parseFloat(CommPerOnDueDt) > 100) {
                //    alert("Total Commission Percentage is: " + PerCommissionTot + "  Commission Percentage Cannot Be Greater Than 100 %");
                //    document.getElementById(CommPerOnDueDtStr).focus();
                //    return false;
                //}

                var rowCount = opTable.rows.length;
                //var cellCount = opTable.rows[0].cells.length;
                var row = opTable.insertRow(rowCount);
                var cell_1 = row.insertCell(0);
                //rowCount = rowCount + 1;

                var element_1 = document.createElement("input");
                element_1.type = "text";
                element_1.name = ucName + '_lbDueDate_' + rowCount;
                element_1.id = ucName + '_lbDueDate_' + rowCount;
                element_1.value = DueDate;
                element_1.style.width = "90px";
                element_1.style.height = "16px";
                //element_1.style.textAlign = "left";
                element_1.readOnly = true;
                // cell_1.className = "tdDueDate";
                cell_1.style.width = "20%";
                cell_1.appendChild(element_1);


                var cell_2 = row.insertCell(1);
                var element_2 = document.createElement("input");
                element_2.type = "text";
                element_2.name = ucName + '_txtCommPerOnDueDt_' + rowCount;
                element_2.id = ucName + '_txtCommPerOnDueDt_' + rowCount;
                element_2.value = parseFloat(CommPerOnDueDt).toFixed(2);
                element_2.style.width = '40px';
                element_2.style.height = "16px";
                //element_2.style.textAlign = "left";
                //element_2.setAttribute("style", "paddingleft:50px");
                // cell_2.className = "tdDuePer";
                //  cell_2.style.width = "85px";
                //element_2.className = "padding";
                element_2.readOnly = true;

                var element_syb = document.createElement("Lable");
                element_syb.type = "lable";
                element_syb.name = ucName + 'per' + rowCount;
                element_syb.id = ucName + 'per' + rowCount;
                element_syb.style.height = "16px";
                element_syb.innerHTML = " %";

                cell_2.style.width = "14%";
                cell_2.setAttribute("class", "Padding");
                cell_2.appendChild(element_2);
                cell_2.appendChild(element_syb);


                var cell_3 = row.insertCell(2);
                var element_3 = document.createElement("input");
                element_3.type = "text";
                element_3.name = ucName + '_txtCommAmtOnDueDt_' + rowCount;
                element_3.id = ucName + '_txtCommAmtOnDueDt_' + rowCount;
                element_3.value = parseFloat(CommAmtOnDueDt).toFixed(2);
                element_3.style.width = "100px";
                element_3.style.height = "16px";
                element_3.className = "DollarTextBoxWithBorder";
                //element_2.style.textAlign = "left";
                element_3.readOnly = true;
                //cell_3.style.paddingLeft = "13px";
                cell_3.style.width = "15%";



                cell_3.appendChild(element_3);

                var cell_4 = row.insertCell(3);
                var element_4 = document.createElement("input");
                element_4.src = " ../Images/delete.gif";
                element_4.type = "image";
                element_4.name = ucName + '_btnDelDueDate_' + rowCount;
                element_4.id = ucName + '_btnDelDueDate_' + rowCount;

                //if (ucName.indexOf("ucBaseLeaseTerms") != -1) {
                //    if (IsFullCommPaid == "True") {
                //        element_4.style.display = 'none';

                //    }
                //}
                //else if (ucName.indexOf("ucOptionalTerms") != -1) {

                //   // alert(document.getElementById("ContentMain_ucOptionalTerms_hdnOptionIsPayment").value);
                //    if (document.getElementById("ContentMain_ucOptionalTerms_hdnIsPayment").value != "0")
                //    {
                //        element_4.style.display = 'none';
                //    }

                //}
                //else if (ucName.indexOf("ucContingentCommission") != -1) {
                //    //alert('1');
                //    debugger;
                //    if (document.getElementById("ContentMain_ucContingentCommission_hdnIsPayment").value != "0") {
                //        element_4.style.display = 'none';
                //    }

                //}
                //cell_4.style.paddingLeft = "40px";
                element_4.setAttribute('onclick', 'javascript:DeleteDueDate("' + DueDate + '","' + CommPerOnDueDt + '","' + opTableStr + '","' + ucName + '");return false;');
                //   element_4.setAttribute('onclick', 'javascript:DeleteDueDate("' + rowCount + '","' + opTableStr + '","' + ucName + '");return false;');
                cell_4.style.width = "47%";
                if (IsPayment == "True") {
                    element_4.style.display = 'block';
                }
                cell_4.appendChild(element_4);

                //Added by Jaywanti on 23-11-2015:-Do not display delete button till last last commissionduedates payment applied.
                for (var i = 0; i < rowCount; i++) {

                    // var j = i++;

                    var btnDelete = document.getElementById(ucName + "_btnDelDueDate_" + i);
                    if (IsPayment == "True") {
                        btnDelete.style.display = 'block';
                    }
                }
                //
                var randomnumberStr = ucName + "_hdnDueDateCount";
                // var randomnumber = parseInt(document.getElementById(randomnumberStr).value) + 1;
                var randomnumber = opTable.rows.length;
                document.getElementById(randomnumberStr).value = randomnumber;
                if (randomnumber >= 1) {
                    CreateDueDateSummarytb(randomnumberStr, opTableStr, ucName.replace("ContentMain_", ""));
                }
                // randomnumber = randomnumber + 1;
                //document.getElementById(randomnumberStr).value = randomnumber;

                VisibleNotifyButton();
            }
            return false;
        }

    }

    function addDueDateRecords(Parent, PanelId, ucName) {
        debugger;


        ScrollDown(PanelId);
        var btn = Parent;
        btn = btn.replace("btnDueDates", "");

        var DueDtStr = btn + "txtDueDates";
        var CommPerOnDueDtStr = btn + "txtAddCommPerOnDueDt";
        var CommAmtOnDueDtStr = btn + "txtAddCommAmountOnDueDt";
        var randomnumberStr = btn + "hdnDueDateCount";
        var opTableStr = btn + "tbDueDates";


        var CreateNewTbVal = btn + "hdnSetTrue";

        //   var hdnDueDateCountStr = btn + "hdnDueDateCoun";  

        var DueDate = document.getElementById(DueDtStr).value;

        if (!isDate(document.getElementById(DueDtStr))) {
            return false;
        }
        var CommPerOnDueDt = document.getElementById(CommPerOnDueDtStr).value;
        var CommAmtOnDueDt = document.getElementById(CommAmtOnDueDtStr).value;

        if (CheckForDuplicateDueDateEntry(DueDate, btn) == true) {
            if (CommAmtOnDueDt != "") {
                if (CommPerOnDueDt > 100) {
                    alert("Commission Percentage Cannot Be Greater Than 100 %");
                    return false;
                }
                else {
                    if (DueDate != "" && CommPerOnDueDt != "") {
                        var randomnumber = parseFloat(document.getElementById(randomnumberStr).value);
                        var PerCommissionTot;
                        PerCommissionTot = 0;
                        // PerCommAmt = 0;

                        var opTable = document.getElementById(opTableStr);

                        for (var r = 0, n = opTable.rows.length; r < n; r++) {
                            if (opTable.rows[r].cells[1].firstChild != null) {
                                PerCommissionTot += parseFloat(opTable.rows[r].cells[1].firstChild.value);

                            }
                        }

                        if (PerCommissionTot + parseFloat(CommPerOnDueDt) > 100) {
                            alert("Total Commission Percentage is: " + PerCommissionTot + "  Commission Percentage Cannot Be Greater Than 100 %");
                            document.getElementById(CommPerOnDueDtStr).focus();
                            return false;
                        }

                        // var rowCount = opTable.rows.length;
                        if (randomnumber == "") {
                            randomnumber = opTable.rows.length;
                        }
                        var InsertRowAt = opTable.rows.length;

                        var rowCount = parseInt(randomnumber);// + 1;

                        var row = opTable.insertRow(InsertRowAt);
                        var cell_1 = row.insertCell(0);
                        // rowCount = rowCount + 1;

                        var element_1 = document.createElement("input");
                        element_1.type = "text";
                        element_1.name = btn + 'lbDueDate_' + rowCount;
                        element_1.id = btn + 'lbDueDate_' + rowCount;
                        element_1.value = DueDate;
                        element_1.style.width = "90px";
                        element_1.style.height = "18px";
                        //element_1.style.textAlign = "left";
                        element_1.readOnly = true;
                        //cell_1.style.paddingLeft = "115px";
                        cell_1.style.width = "20%";
                        cell_1.appendChild(element_1);


                        var cell_2 = row.insertCell(1);
                        var element_2 = document.createElement("input");
                        element_2.type = "text";
                        element_2.name = btn + 'txtCommPerOnDueDt_' + rowCount;
                        element_2.id = btn + 'txtCommPerOnDueDt_' + rowCount;
                        element_2.value = CommPerOnDueDt;
                        element_2.style.width = '40px';
                        element_2.style.height = "18px";
                        //element_2.style.textAlign = "left";
                        //element_2.setAttribute("style", "paddingleft:50px");
                        //cell_2.style.paddingLeft = "45px";
                        cell_2.style.width = "85px";
                        cell_2.style.width = "14%";
                        //element_2.className = "padding";
                        element_2.readOnly = true;

                        var element_syb = document.createElement("Lable");
                        element_syb.type = "lable";
                        element_syb.name = btn + 'per' + rowCount;
                        element_syb.id = btn + 'per' + rowCount;
                        element_syb.innerHTML = " % ";


                        cell_2.appendChild(element_2);
                        cell_2.appendChild(element_syb);

                        var cell_3 = row.insertCell(2);
                        var element_3 = document.createElement("input");
                        element_3.type = "text";
                        element_3.name = btn + 'txtCommAmtOnDueDt_' + rowCount;
                        element_3.id = btn + 'txtCommAmtOnDueDt_' + rowCount;
                        element_3.value = CommAmtOnDueDt;
                        element_3.style.width = "100px";
                        element_3.style.height = "18px";
                        element_3.className = "DollarTextBoxWithBorder";
                        //element_2.style.textAlign = "left";
                        element_3.readOnly = true;
                        //cell_3.style.paddingLeft = "13px";
                        cell_3.style.width = "15%";
                        cell_3.appendChild(element_3);

                        var cell_4 = row.insertCell(3);
                        var element_4 = document.createElement("input");
                        element_4.src = " ../Images/delete.gif";
                        element_4.type = "image";
                        element_4.name = btn + 'btnDelDueDate_' + rowCount;
                        element_4.id = btn + 'btnDelDueDate_' + rowCount;
                        //cell_4.style.paddingLeft = "40px";
                        element_4.setAttribute('onclick', 'javascript:DeleteDueDate("' + DueDate + '","' + CommPerOnDueDt + '","' + opTableStr + '","' + ucName + '");return false;');
                        cell_4.style.width = "47%";
                        cell_4.appendChild(element_4);


                        randomnumber = randomnumber + 1;
                        document.getElementById(randomnumberStr).value = randomnumber; var randomnumber =
                        // document.getElementById(randomnumberStr).value = opTable.rows.length;

                         document.getElementById(CreateNewTbVal).value = true;

                        if (randomnumber >= 1) {
                            CreateDueDateSummarytb(randomnumberStr, opTableStr, ucName);
                        }

                        VisibleNotifyButton();
                    }
                }
                //document.getElementById(DueDtStr).value = "";
                document.getElementById(CommPerOnDueDtStr).value = "";
                document.getElementById(CommAmtOnDueDtStr).value = "";
                return true;
            }
        }
        else {
            //document.getElementById(DueDtStr).value = "";
            document.getElementById(CommPerOnDueDtStr).value = "";
            document.getElementById(CommAmtOnDueDtStr).value = "";
        }
        return true;

    }

    function DeleteDueDate(DueDate, Percentage, TableStr, ucName) {

        debugger;
        var RowIndex, RowIndexComm;
        var uc = ucName.replace("ContentMain_", "");
        var DetTable = document.getElementById(TableStr);
        var SummTable;

        if (uc == 'ucBaseLeaseTerms') {
            SummTable = document.getElementById("ContentMain_ucSummery_tbdueDateSumm");
        } else if (uc == 'ucOptionalTerms') {
            SummTable = document.getElementById("ContentMain_ucSummery_tbOptdueDateSumm");
        } else if (uc == 'ucContingentCommission') {
            SummTable = document.getElementById("ContentMain_ucSummery_tbContendueDateSumm");
        } else if (uc == 'ucOptional1Terms') {
            SummTable = document.getElementById("ContentMain_ucSummery_tbOptdueDateSumm1");
        } else if (uc == 'ucOptional2Terms') {
            SummTable = document.getElementById("ContentMain_ucSummery_tbOptdueDateSumm2");
        } else if (uc == 'ucOptional3Terms') {
            SummTable = document.getElementById("ContentMain_ucSummery_tbOptdueDateSumm3");
        }

        var randomnumberStr = "ContentMain_" + uc + "_hdnDueDateCount";
        var randomnumber;
        if (document.getElementById(randomnumberStr).value != "") {
            randomnumber = parseInt(document.getElementById(randomnumberStr).value);// - 1;
        }
        else {
            randomnumber = 0;
        }
        document.getElementById(randomnumberStr).value = randomnumber;
        var i;

        for (i = 1; i < SummTable.rows.length; i++) {
            var DueDateSumm = SummTable.rows[i].cells[0].children[0].innerHTML;
            var PerSumm = SummTable.rows[i].cells[2].children[0].innerHTML.replace("%", "");

            if (DetTable.rows[i - 1].cells[0].firstChild != null) {
                var DueDateMain = DetTable.rows[i - 1].cells[0].firstChild.value;
                var PerMain = DetTable.rows[i - 1].cells[1].firstChild.value;

                if (DueDateSumm == DueDate && parseFloat(PerSumm) == parseFloat(Percentage)) {
                    RowIndexComm = i;
                }
                if (DueDateMain == DueDate && parseFloat(PerMain) == parseFloat(Percentage)) {
                    RowIndex = i - 1;
                }
            }
            else {
                var DueDateMain = DetTable.rows[i].cells[0].firstChild.value;
                var PerMain = DetTable.rows[i].cells[1].firstChild.value;

                if (DueDateSumm == DueDate && parseFloat(PerSumm) == parseFloat(Percentage)) {
                    RowIndexComm = i + 1;
                }
                if (DueDateMain == DueDate && parseFloat(PerMain) == parseFloat(Percentage)) {
                    RowIndex = i;
                }
            }
        }


        DetTable.deleteRow(RowIndex);
        var len = SummTable.rows.length;
        if (len > 0) {
            SummTable.deleteRow(RowIndexComm-1);
        }
       // SummTable.deleteRow(RowIndexComm);
        DeleteDatatbRecord(DueDate, Percentage);
        debugger;
        VisibleNotifyButton();
        return false;
    }

    function DeleteDatatbRecord(DueDate, Percentage) {

        $.ajax({
            type: "POST",
            url: "NewChargeLeaseTerms.aspx/DeleteDueDateFromSession",
            data: '{DueDate: "' + DueDate + '",Percentage:"' + Percentage + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            failure: function (response) {
                alert(response.d);
            }
        });
    }


    function CreateDueDateSummarytb(randomnumberStr, opTableStr, ucName) {
        //   alert(ucName);

        var SourceTable = document.getElementById(opTableStr);
        var DestTable;
        var TotCommission;

        if (ucName == 'ucBaseLeaseTerms') {
            DestTable = document.getElementById("ContentMain_ucSummery_tbdueDateSumm");
            TotCommission = parseFloat(replaceAll(",", "", $("#ContentMain_ucBaseLeaseTerms_gvBaseCommTerms").find('span[id*="lblTotFee"]').text()));
        } else if (ucName == 'ucOptionalTerms') {
            DestTable = document.getElementById("ContentMain_ucSummery_tbOptdueDateSumm");
            TotCommission = parseFloat(replaceAll(",", "", $("#ContentMain_ucOptionalTerms_gvBaseCommTerms").find('span[id*="lblTotFee"]').text()));
        }
        else if (ucName == 'ucContingentCommission') {
            DestTable = document.getElementById("ContentMain_ucSummery_tbContendueDateSumm");
            TotCommission = parseFloat(replaceAll(",", "", $("#ContentMain_ucContingentCommission_gvBaseCommTerms").find('span[id*="lblTotFee"]').text()));
        } else if (ucName == 'ucOptional1Terms') {
            DestTable = document.getElementById("ContentMain_ucSummery_tbOptdueDateSumm1");
            TotCommission = parseFloat(replaceAll(",", "", $("#ContentMain_ucOptional1Terms_gvBaseCommTerms").find('span[id*="lblTotFee"]').text()));
        } else if (ucName == 'ucOptional2Terms') {
            DestTable = document.getElementById("ContentMain_ucSummery_tbOptdueDateSumm2");
            TotCommission = parseFloat(replaceAll(",", "", $("#ContentMain_ucOptional2Terms_gvBaseCommTerms").find('span[id*="lblTotFee"]').text()));
        } else if (ucName == 'ucOptional3Terms') {
            DestTable = document.getElementById("ContentMain_ucSummery_tbOptdueDateSumm3");
            TotCommission = parseFloat(replaceAll(",", "", $("#ContentMain_ucOptional3Terms_gvBaseCommTerms").find('span[id*="lblTotFee"]').text()));
        }

        var Commission;
        var DueDate;
        var rowCount;
        var Srow = SourceTable.rows.length - 1;

        DueDate = SourceTable.rows[Srow].cells[0].firstChild.value;


        Commission = parseFloat(SourceTable.rows[Srow].cells[1].firstChild.value);

        var CommAmount = 0;
        if (TotCommission != '') {
            CommAmount = (parseFloat(TotCommission) * parseFloat(Commission)) / 100;
        }

        rowCount = Srow - 1;
        var rowCount = DestTable.rows.length;
        //var cellCount = DestTable.rows[0].cells.length;
        var row = DestTable.insertRow(rowCount);
        // var cell_1 = row.insertCell(0);
        rowCount = rowCount + 1;



        var i;
        for (i = 1; i <= 5; i++) {
            var cell_1 = row.insertCell(i - 1);
            if (i % 2 == 0) {
                cell_1.style.width = "10%";
                var element_1 = document.createElement("span");
                element_1.name = 'lbSummary_' + rowCount + i;
                element_1.id = 'lbSummary_' + rowCount + i;
                element_1.type = "span";
                element_1.innerHTML = "--";
            }
            else {
                var element_1 = document.createElement("span");
                element_1.type = "span";
                element_1.name = 'lbDueDate_' + rowCount;

                switch (i) {
                    case 1:

                        cell_1.style.width = "30%";
                        cell_1.id = "tdBrokerSummary_" + ucName + "_" + rowCount + i;
                        $("#spPixelWidth").text(DueDate);
                        var StrWidth = $("#spPixelWidth").width();

                        if (StrWidth < 60) {
                            element_1.innerHTML = DueDate + "";
                            $("#" + cell_1.id).attr('class', 'tbNormal');
                        }
                        else {
                            element_1.innerHTML = DueDate;
                            $("#" + cell_1.id).attr('class', 'tbOnverFlow');
                        }
                        element_1.style.textAlign = "left";
                        break;
                    case 3:
                        cell_1.style.width = "25%";
                        element_1.innerHTML = parseFloat(Commission).toFixed(2) + '%';
                        element_1.style.textAlign = "right";
                        break;
                    case 5:
                        cell_1.style.width = "25%";
                        element_1.innerHTML = "$ " + parseFloat(CommAmount).toFixed(2);
                        element_1.style.textAlign = "right";
                        break;
                }
                cell_1.appendChild(element_1);
            }
            cell_1.appendChild(element_1);
        }

        //var element_1 = document.createElement("Lable");
        //element_1.type = "lable";
        //element_1.name = 'lbDueDate_' + rowCount;
        //element_1.id = 'lbDueDate_' + rowCount;
        //element_1.innerHTML = DueDate + '...' + Commission + '% ....' + CommAmount.toFixed(2);
        //cell_1.appendChild(element_1);

    }

    function ScrollToggle(parent) {
        $("#" + parent).slideToggle('fast');
        return false;
    }

    function ScrollDown(parent) {
        $("#" + parent).slideDown('fast');
        return false;
    }

    function ScrollUp(parent) {

        $("#" + parent).slideUp('fast');
        return true;
    }
    function SetSummary1(Summ, ucName, type) {

        var SetSumm = "";
        if (Summ != "") {
            SetSumm = Summ + " MONTHS";
        }
        if (ucName == 'ucBaseLeaseTerms') {
            if (type == "Term") {

                $("#baseSumm").html(SetSumm);

            }
            else if (type == "Comm") {

                $("#commSumm").html(SetSumm);

            }
        }
        else if (ucName == 'ucOptionalTerms') {
            if (type == "Term") {
                $("#optnSumm").html(SetSumm);
            }
            else if (type == "Comm") {
                $("#optnCommSumm").html(SetSumm);
            }
        }
        else if (ucName == 'ucContingentCommission') {
            if (type == "Term") {
                $("#contengentSumm").html(SetSumm);
            }
            else if (type == "Comm") {
                $("#contengentCommSumm").html(SetSumm);
            }
        }
        else if (ucName == 'ucOptional1Terms') {
            if (type == "Term") {
                $("#optnSumm1").html(SetSumm);
            }
            else if (type == "Comm") {
                $("#optnCommSumm1").html(SetSumm);
            }
        }
        else if (ucName == 'ucOptional2Terms') {
            if (type == "Term") {
                $("#optnSumm2").html(SetSumm);
            }
            else if (type == "Comm") {
                $("#optnCommSumm2").html(SetSumm);
            }
        }
        else if (ucName == 'ucOptional3Terms') {
            if (type == "Term") {
                $("#optnSumm3").html(SetSumm);
            }
            else if (type == "Comm") {
                $("#optnCommSumm3").html(SetSumm);
            }
        }

        return false;
    }
    function SetSummary(Summ, ucName, type) {

        var SetSumm = "";
        if (Summ != "") {
            SetSumm = Summ + " MONTHS";
        }

        if (ucName == 'ucBaseLeaseTerms') {
            //alert(type);
            if (type == "Term") {

                $("#baseSumm").html(SetSumm);
            }
            else if (type == "Comm") {

                $("#commSumm").html(SetSumm);
            }
        }
        else if (ucName == 'ucOptionalTerms') {
            if (type == "Term") {
                $("#optnSumm").html(SetSumm);
            }
            else if (type == "Comm") {
                $("#optnCommSumm").html(SetSumm);
            }
        }
        else if (ucName == 'ucContingentCommission') {
            if (type == "Term") {
                $("#contengentSumm").html(SetSumm);
            }
            else if (type == "Comm") {
                $("#contengentCommSumm").html(SetSumm);
            }
        }
        else if (ucName == 'ucOptional1Terms') {
            if (type == "Term") {
                $("#optnSumm1").html(SetSumm);
            }
            else if (type == "Comm") {
                $("#optnCommSumm1").html(SetSumm);
            }
        }
        else if (ucName == 'ucOptional2Terms') {
            if (type == "Term") {
                $("#optnSumm2").html(SetSumm);
            }
            else if (type == "Comm") {
                $("#optnCommSumm2").html(SetSumm);
            }
        }
        else if (ucName == 'ucOptional3Terms') {
            if (type == "Term") {
                $("#optnSumm3").html(SetSumm);
            }
            else if (type == "Comm") {
                $("#optnCommSumm3").html(SetSumm);
            }
        }

        return false;
    }


    function SetRent(Parent) {
        //;
        // var PIndx = Parent.indexOf("txtEditFromRentPeriod");
        //  var PLenght = Parent.length;
        //  var subStrParent = Parent.substr(0, PIndx - 1);
        var toPeriod = $("table[id*='" + Parent + "'] input[type=text][id*=txtEditToRentPeriod]");
        var frmPeriod = $("table[id*=" + Parent + "] input[type=text][id*=txtEditFromRentPeriod]");
        var Period = parseInt(toPeriod.val()) - parseInt(frmPeriod.val()) + 1;
        $("#" + Parent).find('span[id*="lbEditTotMonths"]').text(Period);
    }

    function CalCulateRentPSPChange(Parent, Parent1, EditIndex, Container, CommissionType) {
        // alert('1');
        // alert(CommissionType);

        var hdnSizeSF = parseFloat(document.getElementById("<%=hdnSizeSF.ClientID%>").value);
        var rslt = hdnSizeSF * replaceAll(",", "", $("table[id*='" + Parent + "'] input[type=text][id*=txtEditRentPSP]").val());
        $("table[id*='" + Parent + "'] input[type=text][id*=txtEditAnnualRent]").val(rslt);

        CalBrokerPerOnBaseAmtChange(Parent, Parent1, EditIndex, Container, rslt, CommissionType);
    }

    function CalCulateRentAnnualRentChange(Parent, Parent1, EditIndex, Container) {

        //alert('2');
        // debugger;
        var hdnSizeSF = parseFloat(document.getElementById("<%=hdnSizeSF.ClientID%>").value);
        var rslt = (replaceAll(",", "", $("table[id*='" + Parent + "'] input[type=text][id*=txtEditAnnualRent]").val()) / hdnSizeSF);
        $("table[id*='" + Parent + "'] input[type=text][id*=txtEditRentPSP]").val(rslt.toFixed(2));
        CalBrokerPerOnBaseAmtChange(Parent, Parent1, EditIndex, Container, rslt, CommissionType);

    }

    function CalCulateRentMonthPeriodChange(Parent, Parent1, EditIndex, Container, CommissionType) {
        debugger;
        var PeriodFrom = $("table[id*='" + Parent + "'] input[type=text][id*=txtItemFromRentPeriod]").val();
        var PeriodTo = $("table[id*='" + Parent + "'] input[type=text][id*=txtItemToRentPeriod]").val();
        if (PeriodFrom != null && PeriodTo != null && (parseInt(PeriodTo) > parseInt(PeriodFrom))) {
            var TotPeriod = PeriodTo - PeriodFrom + 1;
            CalBrokerPerOnBaseAmtMonthChange(Parent, Parent1, EditIndex, Container, PeriodFrom, PeriodTo, TotPeriod, CommissionType);
        }
        else {
            return false;
        }
    }

    //used when COMMISSION TERM SAME AS LEASE TERM?
    function CalBrokerPer(Parent, Parent1, RowIndx, Container, CommissionType) {
        //debugger;
        // alert('3');
        //var rslt = (parseFloat($("#" + Parent1).find('span[id*="lbItemPeriodRent"]').text()) * parseFloat($("table[id*='" + Parent + "'] input[type=text][id*=txtEditBrokerCommission]").val())) / 100;
        if (CommissionType == 1) {
            var Row = parseInt(RowIndx) + 1;
            var GidView = document.getElementById(Parent1);
            var lbItemPeriodRent = parseFloat(replaceAll(",", "", GidView.rows[Row].cells[5].children[0].innerHTML));
            var rslt = (parseFloat(lbItemPeriodRent) * parseFloat(replaceAll(",", "", $("table[id*='" + Parent + "'] input[type=text][id*=txtEditBrokerCommission_" + RowIndx + "]").val())));
            rslt = rslt / 100;
            $("table[id*='" + Parent + "'] input[type=text][id*=txtEditFee_" + RowIndx + "]").val(rslt.toFixed(2));
        }

        //  $("table[id*='" + Parent + "'] input[type=text][id*=txtEditFee]").val(rslt);


        var TotalFee = 0;
        for (var i = 0; i < GidView.rows.length - 2 ; i++) {
            if ($("table[id*='" + Parent + "'] input[type=text][id*=txtEditFee_" + i + "]").length != 0) {

                TotalFee += parseFloat(replaceAll(",", "", $("table[id*='" + Parent + "'] input[type=text][id*=txtEditFee_" + i + "]").val()));

            }
        }
        // $("table[id*='" + Parent + "'] input[type=text][id*=lblTotFee]").val(TotalFee.toFixed(2));
        $("#" + Parent).find('span[id*="lblTotFee"]').text(TotalFee.toFixed(2));
        UpdateDueDate(Container, TotalFee.toFixed(2));
    }

    //This function is same as CalBrokerPerOnBaseAmtChange() just Period duration included
    function CalBrokerPerOnBaseAmtMonthChange(Parent, Parent1, RowIndx, Container, PeriodFrom, PeriodTo, TotPeriod, CommissionType) {

        var CommissionTypeId = CommissionType;
        var Row = parseInt(RowIndx) + 1;
        var GidView = document.getElementById(Parent1);
        var GidView1 = document.getElementById(Parent);

        var lbItemBrokerCommission = parseFloat(replaceAll(",", "", $("table[id*='" + Parent1 + "'] input[type=text][id*=txtEditBrokerCommission_" + RowIndx + "]").val()));
        var RentPeriod = 0; var PeriodRent = 0; var rslt = 0.0; var AnnualRent = 0.0;
        var hdnSizeSF = parseFloat(document.getElementById("<%=hdnSizeSF.ClientID%>").value);

        if (CommissionType == 1) {
            RentPeriod = parseInt(TotPeriod);
            AnnualRent = hdnSizeSF * replaceAll(",", "", $("table[id*='" + Parent + "'] input[type=text][id*=txtEditRentPSP]").val());
            PeriodRent = ((RentPeriod * parseFloat(AnnualRent)) / 12)
            rslt = (parseFloat(PeriodRent) * parseFloat(lbItemBrokerCommission));
            rslt = rslt / 100;
            $("table[id*='" + Parent1 + "'] input[type=text][id*=txtEditFee_" + RowIndx + "]").val(rslt.toFixed(2));

            $("table[id*='" + Parent1 + "'] input[type=text][id*=lbItemFromRentPeriod_" + RowIndx + "]").val(PeriodFrom);
            $("table[id*='" + Parent1 + "'] input[type=text][id*=lbItemToRentPeriod_" + RowIndx + "]").val(PeriodTo);
            var TotMonths = PeriodTo - PeriodFrom;
            $("table[id*='" + Parent + "'] input[type=text][id*=lbEditTotMonths_" + RowIndx + "]").val(TotMonths);
            $("table[id*='" + Parent1 + "'] input[type=text][id*=lbItemTotMonths_" + RowIndx + "]").val(TotMonths);
        }
    }

    function CalBrokerPerOnBaseAmtChange(Parent, Parent1, RowIndx, Container, AnnualRent, CommissionType) {

        debugger;

        //var rslt = (parseFloat($("#" + Parent1).find('span[id*="lbItemPeriodRent"]').text()) * parseFloat($("table[id*='" + Parent + "'] input[type=text][id*=txtEditBrokerCommission]").val())) / 100;
        var CommissionTypeId = CommissionType;
        var Row = parseInt(RowIndx) + 1;
        var GidView = document.getElementById(Parent1);

        // var lbItemBrokerCommission = parseFloat(replaceAll(",", "", GidView.rows[Row].cells[2].children[0].innerHTML));
        var lbItemBrokerCommission = parseFloat(replaceAll(",", "", $("table[id*='" + Parent1 + "'] input[type=text][id*=txtEditBrokerCommission_" + RowIndx + "]").val()));

        var GidView1 = document.getElementById(Parent);
        var RentPeriod = 0; var PeriodRent = 0; var rslt = 0.0;
        if (CommissionType == 1) {
            if (GidView1 != null) {
                RentPeriod = parseInt(GidView1.rows[Row].cells[1].children[0].innerHTML);
                PeriodRent = ((RentPeriod * AnnualRent) / 12);
                rslt = (parseFloat(PeriodRent) * parseFloat(lbItemBrokerCommission));
                rslt = rslt / 100;
                $("table[id*='" + Parent1 + "'] input[type=text][id*=txtEditFee_" + RowIndx + "]").val(rslt.toFixed(2));

                //$("#" + Parent1).find('span[id*="lbItemFee_' + RowIndx + '"]').text(rslt.toFixed(2));
                //$("table[id*='" + Parent1 + "'][id*=hdnFeeBaseAmt_" + RowIndx + "]").val(PeriodRent);
            }
            else {
                //var RentPeriod = parseInt(GidView1.rows[Row].cells[1].children[0].innerHTML);
                RentPeriod = $("table[id*='" + Parent + "'] input[type=text][id*=lbItemToRentPeriod_" + RowIndx).val();
                PeriodRent = ((RentPeriod * AnnualRent) / 12);
                rslt = (parseFloat(PeriodRent) * parseFloat(lbItemBrokerCommission));
                rslt = rslt / 100;
                $("table[id*='" + Parent1 + "'] input[type=text][id*=txtEditFee_" + RowIndx + "]").val(rslt.toFixed(2));

                //$("#" + Parent1).find('span[id*="lbItemFee_' + RowIndx + '"]').text(rslt.toFixed(2));
                //$("table[id*='" + Parent1 + "'][id*=hdnFeeBaseAmt_" + RowIndx + "]").val(PeriodRent);

            }
        }

        var TotalFee = 0;
        for (var i = 0; i < GidView.rows.length - 2 ; i++) {
            if ($("table[id*='" + Parent1 + "'] input[type=text][id*=txtEditFee_" + i + "]").length() != 0) {
                TotalFee += parseFloat(replaceAll(",", "", $("table[id*='" + Parent1 + "'] input[type=text][id*=txtEditFee_" + i + "]").val()));
            }
        }
        // $("table[id*='" + Parent + "'] input[type=text][id*=lblTotFee]").val(TotalFee.toFixed(2));
        $("#" + Parent1).find('span[id*="lblTotFee"]').text(TotalFee.toFixed(2));
        //alert(TotalFee);
        UpdateDueDate(Container, TotalFee);
    }


    function CalBrokerFee(Parent, Parent1, RowIndx, Container, CommissionType) {

        if (CommissionType == 1) {
            var Row = parseInt(RowIndx) + 1;
            var GidView = document.getElementById(Parent1);
            var lbItemPeriodRent = parseFloat(replaceAll(",", "", GidView.rows[Row].cells[5].children[0].innerHTML));

            var rslt = (replaceAll(",", "", $("table[id*='" + Parent + "'] input[type=text][id*=txtEditFee_" + RowIndx + "]").val()) / (parseFloat(lbItemPeriodRent))) * 100;
            // rslt = rslt / 1000;
            // $("table[id*='" + Parent + "'] input[type=text][id*=txtEditBrokerCommission]").val(rslt);
            $("table[id*='" + Parent + "'] input[type=text][id*=txtEditBrokerCommission_" + RowIndx + "]").val(rslt.toFixed(2));
        }


        var TotalFee = 0;
        for (var i = 0; i < GidView.rows.length - 2 ; i++) {
            if ($("table[id*='" + Parent + "'] input[type=text][id*=txtEditFee_" + i + "]").length != 0) {

                TotalFee += parseFloat(replaceAll(",", "", $("table[id*='" + Parent + "'] input[type=text][id*=txtEditFee_" + i + "]").val()));

            }
        }
        // $("table[id*='" + Parent + "'] input[type=text][id*=lblTotFee]").val(TotalFee.toFixed(2));
        $("#" + Parent).find('span[id*="lblTotFee"]').text(TotalFee.toFixed(2));

        UpdateDueDate(Container, replaceAll(",", "", TotalFee.toFixed(2)));

    }

    function UpdateDueDate(Parent, totAmt) {


        var PerCommissionTot = 0;
        var GV = Parent + "_gvBaseCommTerms";
        var GidView = document.getElementById(GV);
        var Summtb;
        var CommAmt = 0;
        var uCName = Parent.replace("ContentMain_", "");

        if (uCName == 'ucBaseLeaseTerms') {
            DestTable = document.getElementById("ContentMain_ucSummery_tbdueDateSumm");
            Summtb = "ContentMain_ucSummery_tbdueDateSumm";
        } else if (uCName == 'ucOptionalTerms') {
            DestTable = document.getElementById("ContentMain_ucSummery_tbOptdueDateSumm");
            Summtb = "ContentMain_ucSummery_tbOptdueDateSumm";
        }
        else if (uCName == 'ucContingentCommission') {
            DestTable = document.getElementById("ContentMain_ucSummery_tbContendueDateSumm");
            Summtb = "ContentMain_ucSummery_tbContendueDateSumm";
        }
        else if (uCName == 'ucOptional1Terms') {
            DestTable = document.getElementById("ContentMain_ucSummery_tbOptdueDateSumm1");
            Summtb = "ContentMain_ucSummery_tbOptdueDateSumm1";
        }
        else if (uCName == 'ucOptional2Terms') {
            DestTable = document.getElementById("ContentMain_ucSummery_tbOptdueDateSumm2");
            Summtb = "ContentMain_ucSummery_tbOptdueDateSumm2";
        }
        else if (uCName == 'ucOptional3Terms') {
            DestTable = document.getElementById("ContentMain_ucSummery_tbOptdueDateSumm3");
            Summtb = "ContentMain_ucSummery_tbOptdueDateSumm3";
        }

        if (GidView != null) {
            var rCount = parseInt(GidView.rows.length) - 2;
            // var rCount = Math.round(rCount / 2) + 1;

            for (var i = 1; i <= rCount ; i++) {
                if (GidView.rows[i].cells[3].children[0].innerHTML != "") {
                    CommAmt = CommAmt + parseFloat(replaceAll(",", "", GidView.rows[i].cells[3].children[0].innerHTML));
                }
            }
            var tot = parseFloat(totAmt) + CommAmt;
            var opTable = document.getElementById(Parent + "_tbDueDates");
            for (var r = 0, n = opTable.rows.length; r < n; r++) { //changed innerText to innerHTML
                if (opTable.rows[r].cells[1].children.length > 0) {
                    var duepercentage = parseFloat(replaceAll(",", "", opTable.rows[r].cells[1].children[0].value));
                    opTable.rows[r].cells[2].children[0].value = (duepercentage * tot / 100).toFixed(2);
                }
            }


            for (var r = 0, n = DestTable.rows.length; r < n; r++) { //changed innerText to innerHTML
                if (DestTable.rows[r].cells[2].children.length > 0) {
                    var duepercentage = parseFloat((replaceAll(",", "", DestTable.rows[r].cells[2].children[0].innerHTML)).replace("%", ""));
                    DestTable.rows[r].cells[4].children[0].innerHTML = (duepercentage * tot / 100).toFixed(2);
                }
            }
        }
        else {
            $('#' + Parent + "_tbDueDates").empty();
            $('#' + Summtb).empty();

            SetSummary("", uCName, "Term");
            SetSummary1("", uCName, "Comm");
        }
    }

    function ValidateMonthRange(Parent, RowIndex) {
        // debugger;
        var txtFromRentPeriod = Parent + "_txtFromRentPeriod";
        var txtToRentPeriod = Parent + "_txtToRentPeriod";
        var txtLeaseTermMonths = Parent + "_txtLeaseTermMonths";
        var Row = parseInt(RowIndex) + 1;

        //;
        var GV = Parent + "_gvBaseLeaseTerm";
        var GidView = document.getElementById(GV);

        var FromMonth = parseInt(document.getElementById(txtFromRentPeriod).value);
        var ToMonth = parseInt(document.getElementById(txtToRentPeriod).value);
        var TotMonths = document.getElementById(txtLeaseTermMonths).value;

        var PrevFromMonth = parseInt(GidView.rows[Row].cells[0].firstElementChild.value);
        var PrevToMonth = parseInt(GidView.rows[Row].cells[0].lastElementChild.value);

        if (FromMonth <= PrevToMonth) {
            alert("Enter Valid Period");
            document.getElementById(txtFromRentPeriod).focus();
            return false;
        }
        else if (ToMonth < FromMonth) {
            alert("Enter Valid Period");
            document.getElementById(txtToRentPeriod).focus();
            return false;
        }
        else if (ToMonth > TotMonths) {
            alert("To Months Cannot Be Greater Than Lease Months");
            document.getElementById(txtToRentPeriod).focus();
            return false;
        }
        else {
            return true;
        }
    }



    function ValidateBaseLeasePeriod(Parent) {

        EnableDisableOnCheckChange1();

        var txtFromRentPeriod = Parent + "_txtFromRentPeriod";
        var txtToRentPeriod = Parent + "_txtToRentPeriod";
        var txtLeaseTermMonths = Parent + "_txtLeaseTermMonths";
        var txtRentPSP = Parent + "_txtRentPSP";
        var txtAnnualRent = Parent + "_txtAnnualRent";
        var panel = Parent + "_pnAddBaseLeaseTerm";

        var FromMonth = $.trim(document.getElementById(txtFromRentPeriod).value);
        var ToMonth = $.trim(document.getElementById(txtToRentPeriod).value);
        var TotMonths = $.trim(document.getElementById(txtLeaseTermMonths).value);
        var RentPSP = $.trim(document.getElementById(txtRentPSP).value);
        var AnnualRent = $.trim(document.getElementById(txtAnnualRent).value);


        if (FromMonth == "") {
            alert("Enter From Month");
            document.getElementById(txtFromRentPeriod).focus();
            return false;
        }
        else if (ToMonth == "") {
            alert("Enter To Month");
            document.getElementById(txtToRentPeriod).focus();
            return false;
        }
        else if (parseInt(FromMonth) > parseInt(ToMonth)) {
            alert("Enter Valid Period");
            document.getElementById(txtToRentPeriod).focus();
            return false;
        }
        else if (parseInt(TotMonths) < parseInt(ToMonth)) {
            alert("To Months Cannot Be Greater Than Lease Months");
            document.getElementById(txtToRentPeriod).focus();
            return false;
        }
        else if (RentPSP == "") {
            alert("Enter Rent (PSF)");
            document.getElementById(txtRentPSP).focus();
            return false;
        }
        else if (AnnualRent == "") {
            alert("Enter Annual Rent");
            document.getElementById(txtAnnualRent).focus();
            return false;
        }
        else {
            return true;
        }
        VisibleNotifyButton();
    }

    function SetToPeriodBaseCommTerm(Parent) {
        //;

        var txtBoxNew = Parent + "_txtBaseCommTermMonths";
        var txtLeaseTermMonths = Parent + "_txtLeaseTermMonths";

        var Months = parseInt(document.getElementById(txtBoxNew).value);
        var LeaseMonths = parseInt(document.getElementById(txtLeaseTermMonths).value);

        if (Months > LeaseMonths) {
            alert("Commissionable Term Months Cannot Be Greater Than Lease Term Months");
            document.getElementById(txtBoxNew).value = "";
            document.getElementById(txtBoxNew).focus();
            return false;
        }

        //var hdnMonths = Parent + "_hdnPrevMonth";
        //document.getElementById(hdnMonths).value = Months;

        var GV = Parent + "_gvBaseCommTerms";
        var GidView = document.getElementById(GV);
        var rCount = parseInt(GidView.rows.length) - 1;
        var delRow = 0;
        var i;
        var rslt = false;

        for (i = 1; i <= rCount - 1; i++) {

        }


        for (i = 1; i <= rCount - 1; i++) {
            var rowElement = GidView.rows[i];
            var txtBoxMonths = rowElement.cells[1].children[2].innerHTML;
            if (Months < txtBoxMonths) {
                if (delRow == 0) {
                    rowElement.cells[1].children[2].innerHTML = Months;
                    delRow = i;
                }
            }
            else {
                rsl = true;
            }
            if (delRow != 0 && delRow < i) {
                rsl = true;
                //var DelBtn = Parent + "_delRecodrs";
                //var Hdn = Parent + "_hdnRowInx";

                //document.getElementById(Hdn).value = i;
                //document.getElementById(DelBtn).innerHTML = i;
                // document.getElementById(DelBtn).click();

                //GidView.deleteRow(i);
            }
        }

        if (rsl) {
            __doPostBack(txtBoxNew, '');
        }
        else {
            return false;
        }

        return true;
    }

    function CheckLeaseMonths(parent, Container) {
        // debugger;
        var LeaseMonths = parent + "_txtLeaseTermMonths";
        var Months = document.getElementById(LeaseMonths).value;
        if (Months == "") {
            alert("Please Enter Lease Months");
            document.getElementById(LeaseMonths).focus();

        }
        else {
            document.getElementById(parent + '_txtFromRentPeriod').value = ""
            var TotMonths = document.getElementById(parent + "_txtLeaseTermMonths").value == "" ? 0 : parseInt(document.getElementById(parent + "_txtLeaseTermMonths").value);
            var GV = parent + "_gvBaseLeaseTerm";
            var GidView = document.getElementById(GV);
            if (GidView != null) {
                var RowIndex = GidView.rows.length - 2;
                var NewFromMonth = parseInt(GidView.rows[RowIndex].cells[0].lastElementChild.value) + 1;

                if (TotMonths >= NewFromMonth) {
                    document.getElementById(parent + '_txtFromRentPeriod').value = NewFromMonth;
                    var txtPerIncrese = document.getElementById(parent + '_txtPerIncrese');
                    txtPerIncrese.removeAttribute("readOnly");
                }
            }
            else if (TotMonths > 0) {


                document.getElementById(parent + '_txtFromRentPeriod').value = 1;
                var txtPerIncrese = document.getElementById(parent + '_txtPerIncrese');
                //alert(parent.toLowerCase().indexOf("base"));
                if (parent.toLowerCase().indexOf("base") > 0) {
                    txtPerIncrese.setAttribute("readOnly", true);
                }
                else {
                    txtPerIncrese.removeAttribute("readOnly");
                }
            }

            //hdnNewFromMonth.Value
            document.getElementById(parent + '_txtToRentPeriod').value = "";
            document.getElementById(parent + '_txtRentPSP').value = "";
            document.getElementById(parent + '_txtAnnualRent').value = "";
            document.getElementById(parent + '_txtPerIncrese').value = "";
            //

            ScrollToggle(Container);
            document.getElementById(parent + '_txtFromRentPeriod').focus();
            //  document.getElementById(parent + "_dvAddNewRow").style.display = 'block';


            //dvAddNewRow
        }
        return false;
    }

    function SetCommissionAmount(Parent) {
        //alert('hi');
        //debugger;
        // alert(document.getElementById(Parent + '_ddlCommissionType').value);
        var TotCommission = 0;
        TotCommission = parseFloat(replaceAll(",", "", $("#" + Parent + "_gvBaseCommTerms").find('span[id*="lblTotFee"]').text()));
        var TotalSquareFoot = 0;
        TotalSquareFoot = document.getElementById(Parent + '_hdnTotalSquareFoot').value;
        if (document.getElementById(Parent + '_ddlCommissionType').value == "3") {
            TotCommission = document.getElementById(Parent + '_txtBaseCommPer').value;
        }
        if (document.getElementById(Parent + '_ddlCommissionType').value == "2") {
            TotCommission = (parseFloat(document.getElementById(Parent + '_txtBaseCommPer').value) * parseFloat(TotalSquareFoot)).toFixed(2);
        }
        //alert(TotCommission);
        var txtAddCommPerOnDueDt = document.getElementById(Parent + '_txtAddCommPerOnDueDt').value;
        //alert(txtAddCommPerOnDueDt);
        if (TotCommission != '' && txtAddCommPerOnDueDt != '') {
            //alert('p');
            document.getElementById(Parent + '_txtAddCommAmountOnDueDt').value = ((parseFloat(TotCommission) * parseFloat(txtAddCommPerOnDueDt)) / 100).toFixed(2);
        }
        // alert(document.getElementById(Parent + '_txtAddCommAmountOnDueDt').value);
        return false;
    }


    function CalculateRent(parent, type) {
        debugger;
        var rentPSF = document.getElementById(parent + '_txtRentPSP').value;
        var AnnualRent = document.getElementById(parent + '_txtAnnualRent').value;
        var TotalSQFt = document.getElementById(parent + '_lblTotalSF').innerHTML;

        if (rentPSF != "" && type == 1) {
            document.getElementById(parent + '_txtAnnualRent').value = parseFloat(rentPSF * TotalSQFt).toFixed(2);
        }
        if (AnnualRent != "" && type == 2) {
            document.getElementById(parent + '_txtRentPSP').value = parseFloat(AnnualRent / TotalSQFt).toFixed(2);
        }
        //alert(parent.toLowerCase().indexOf("base"));
        //if (parent.toLowerCase().indexOf("base")) {
        CalAnnualRentDiffInPercentage(parent);
        //}
        //else {
        //    CalAnnualRentDiffInPercentageOPT(parent);
        //}
        //test Purpose


    }

    function SetBaseCommTermMonths(parent) {

        if (parent == "ContentMain_ucOptionalTerms") {
            var SelChkOpt = $("#ContentMain_chkOptionTerm");
            var SelChkOptPayable = $("#ContentMain_chkOptionTermPayable");

            if (SelChkOptPayable.is(':checked') && SelChkOpt.is(':checked')) {
                var LeaseTermMonths = document.getElementById(parent + '_txtLeaseTermMonths').value;
                document.getElementById(parent + '_txtBaseCommTermMonths').value = LeaseTermMonths;
            }
            else {
                document.getElementById(parent + '_txtBaseCommTermMonths').value = 0;
            }
        }
        else if (parent == "ContentMain_ucOptional1Terms") {
            var SelChkOpt1 = $("#ContentMain_chkOptionTerm1");
            var SelChkOptPayable1 = $("#ContentMain_chkOptionTermPayable1");

            if (SelChkOptPayable1.is(':checked') && SelChkOpt1.is(':checked')) {
                var LeaseTermMonths = document.getElementById(parent + '_txtLeaseTermMonths').value;
                document.getElementById(parent + '_txtBaseCommTermMonths').value = LeaseTermMonths;
            }
            else {
                document.getElementById(parent + '_txtBaseCommTermMonths').value = 0;
            }
        }
        else if (parent == "ContentMain_ucOptional2Terms") {
            var SelChkOpt2 = $("#ContentMain_chkOptionTerm2");
            var SelChkOptPayable2 = $("#ContentMain_chkOptionTermPayable2");

            if (SelChkOptPayable2.is(':checked') && SelChkOpt2.is(':checked')) {
                var LeaseTermMonths = document.getElementById(parent + '_txtLeaseTermMonths').value;
                document.getElementById(parent + '_txtBaseCommTermMonths').value = LeaseTermMonths;
            }
            else {
                document.getElementById(parent + '_txtBaseCommTermMonths').value = 0;
            }
        }
        else if (parent == "ContentMain_ucOptional3Terms") {
            var SelChkOpt3 = $("#ContentMain_chkOptionTerm3");
            var SelChkOptPayable3 = $("#ContentMain_chkOptionTermPayable3");

            if (SelChkOptPayable3.is(':checked') && SelChkOpt3.is(':checked')) {
                var LeaseTermMonths = document.getElementById(parent + '_txtLeaseTermMonths').value;
                document.getElementById(parent + '_txtBaseCommTermMonths').value = LeaseTermMonths;
            }
            else {
                document.getElementById(parent + '_txtBaseCommTermMonths').value = 0;
            }
        }
        else {
            var LeaseTermMonths = document.getElementById(parent + '_txtLeaseTermMonths').value;
            document.getElementById(parent + '_txtBaseCommTermMonths').value = LeaseTermMonths;
        }
    }

    function CalAnnualRentDiffInPercentage(parent) {

        var GV = parent + "_gvBaseLeaseTerm";

        var GidView = document.getElementById(GV);
        if (GidView == null) {
            // alert(rCount);
            if (parent.toLowerCase().indexOf("base") < 0) {
                CalAnnualRentDiffInPercentageOPT(parent);
            }
        }
        else {
            var rCount = parseInt(GidView.rows.length) - 2;
            var rowElement = GidView.rows[rCount];


            // if (parent.toLowerCase().indexOf("base")) {
            var PrevAnnualRent = rowElement.cells[3].children[0].innerHTML;
            var CurrentAnnualRent = $("#" + parent + "_txtAnnualRent").val();

            if (PrevAnnualRent != "" && CurrentAnnualRent != "") {
                var PrevRent = parseFloat(replaceAll(",", "", PrevAnnualRent));
                var CurRent = parseFloat(CurrentAnnualRent);
                //  alert(PrevRent);
                // alert(CurRent);
                var RentDiff = (((CurRent - PrevRent) / (PrevRent)) * 100).toFixed(2);
                // alert(RentDiff);
                if (PrevRent == 0) {
                    $("#" + parent + "_txtPerIncrese").val(0);
                }
                else {
                    $("#" + parent + "_txtPerIncrese").val(RentDiff);
                }
            }
            //}
            //else {
            //    CalAnnualRentDiffInPercentageOPT(parent);
            //}

        }
    }




    function CalAnnualRentDiffFromPercentage(parent) {

        var GV = parent + "_gvBaseLeaseTerm";
        var GidView = document.getElementById(GV);
        if (GidView == null) {
            // alert(rCount);
            if (parent.toLowerCase().indexOf("base") < 0) {
                CalAnnualRentDiffFromPercentageOPT(parent)
            }
        }
        else {

            var rCount = parseInt(GidView.rows.length) - 2;
            var rowElement = GidView.rows[rCount];

            var PrevAnnualRent = rowElement.cells[3].children[0].innerHTML;
            var CurPer = $("#" + parent + "_txtPerIncrese").val();

            var CurrentAnnualRent = $("#" + parent + "_txtAnnualRent").val();

            if (PrevAnnualRent != "" && CurPer != "") {
                var PrevRent = parseFloat(replaceAll(",", "", PrevAnnualRent));
                var CurPer = parseFloat(CurPer);
                var CurRent = (((CurPer / 100) * PrevRent) + PrevRent).toFixed(2);
                $("#" + parent + "_txtAnnualRent").val(CurRent);
            }
            CalculateRent(parent, 2);
        }
    }

    function CheckCommissionTypeValue(parent) {

        var chkBaseCommValue = $('#' + parent).val();
        if (chkBaseCommValue == "") {
            return false;
        }
        else {
            var btnid = parent.replace('_txtBaseCommPer', '');
            $('#' + btnid + '_btnBaseCommPer').click();
        }
    }

    //Notification Days Coding

    function isInteger(evt, id) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        var NotifiDays = $("#" + id).val()
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        if (NotifiDays.length > 2)
            return false;

        return true;
    }

    function FillNotifyDaysPopup(id) {
        debugger;
        var Parent = id.replace("_btnNotifyMe", "")
        $find(Parent + '_mpNotifyExtendr').show();
        var txtNotifyDaysVal = $('#' + Parent + '_txtNotify').val()
        var NotifyDaysArray = txtNotifyDaysVal.split(',').map(function (item) {
            return parseInt(item, 10)
        });
        var DefaultNotifyDays = [15, 30, 60, 90]

        for (var i = 0; i < DefaultNotifyDays.length; i++) {
            if (NotifyDaysArray.indexOf(DefaultNotifyDays[i]) > -1)
                document.getElementById(Parent + "_chkNotifyDays" + DefaultNotifyDays[i]).checked = true;
            else
                document.getElementById(Parent + "_chkNotifyDays" + DefaultNotifyDays[i]).checked = false;
        }

        NotifyDaysArray = NotifyDaysArray.filter(function (item) {
            return DefaultNotifyDays.indexOf(item) === -1;
        });

        $('#' + Parent + '_tbCustomNotificatonDays tr').remove();

        BindNotificationTable(NotifyDaysArray, Parent);
    }

    function BindNotificationTable(NotifyDaysArray, Parent) {
        for (var i = 0; i < NotifyDaysArray.length; i++) {
            var randomnumber = parseInt($('#' + Parent + '_hdnCount').val());
            var output = document.getElementById(Parent + '_tbCustomNotificatonDays');
            var rowCount = output.rows.length;
            var cellCount = 0;
            var row = output.insertRow(rowCount);
            var cell1 = row.insertCell(cellCount);

            var element1 = document.createElement("input");
            element1.type = "hidden";
            rowCount = rowCount + 1;
            element1.name = Parent + "hdn1_" + rowCount;
            element1.id = Parent + "hdn1_" + rowCount;
            element1.value = 0;
            cell1.appendChild(element1);

            var element2 = document.createElement("input");
            element2.type = "hidden";
            element2.name = Parent + "hdnCheck_" + rowCount;
            element2.id = Parent + "hdnCheck_" + rowCount;
            element2.value = rowCount;
            cell1.appendChild(element2);
            cellCount++;
            cell1 = row.insertCell(cellCount);

            element1 = document.createElement("Label");
            element1.name = Parent + '_lbl1_' + rowCount;
            element1.id = Parent + '_lbl1_' + rowCount;
            element1.innerText = NotifyDaysArray[i]
            element1.width = "100%";
            cell1.appendChild(element1);
            cellCount++;
            cell1 = row.insertCell(cellCount);

            var cell1 = row.insertCell(cellCount);
            var element1 = document.createElement("input");
            $(cell1).addClass("tdgray");
            $(cell1).addClass("td-DeleteRecord");
            element1.type = "image";
            element1.src = "../Images/delete.gif";
            element1.setAttribute('onclick', 'javascript:DeleteRecord("' + rowCount + '","' + Parent + '");return false;');
            cell1.appendChild(element1);

            randomnumber = randomnumber + 1;
            $('#' + Parent + '_hdnCount').val();
        }
    }

    function addCustomNotificationDayRows(id) {
        debugger;
        var Parent = id.replace("_btnAddDays", "");
        var randomnumber = parseInt($('#' + Parent + '_hdnCount').val());
        var output = document.getElementById(Parent + '_tbCustomNotificatonDays');
        var NotifyDaysval = $('#' + Parent + '_txtAddDays').val()
        var DefaultNotifyDays = [15, 30, 60, 90]

        for (var i = 0; i < output.rows.length; i++) {
            DefaultNotifyDays.push(parseInt(output.rows[i].cells[1].innerText))
        }

        if (NotifyDaysval == "" || NotifyDaysval == 0) {
            alert("Blank Notification Days value not allowed")
            return false;
        }

        if (DefaultNotifyDays.indexOf(parseInt(NotifyDaysval)) != -1) {
            alert('Entered Notification Days already Present')
            return false
        }

        var rowCount = output.rows.length;
        var cellCount = 0;
        var row = output.insertRow(rowCount);
        var cell1 = row.insertCell(cellCount);

        var element1 = document.createElement("input");
        element1.type = "hidden";
        rowCount = rowCount + 1;
        element1.name = Parent + "hdn1_" + rowCount;
        element1.id = Parent + "hdn1_" + rowCount;
        element1.value = 0;
        cell1.appendChild(element1);

        var element2 = document.createElement("input");
        element2.type = "hidden";
        element2.name = Parent + "hdnCheck_" + rowCount;
        element2.id = Parent + "hdnCheck_" + rowCount;
        element2.value = rowCount;
        cell1.appendChild(element2);
        cellCount++;
        cell1 = row.insertCell(cellCount);

        element1 = document.createElement("Label");
        element1.name = Parent + '_lbl1_' + rowCount;
        element1.id = Parent + '_lbl1_' + rowCount;
        element1.innerText = NotifyDaysval
        element1.width = "100%";
        element1.style.paddingTop = "5%";
        cell1.appendChild(element1);
        cellCount++;
        cell1 = row.insertCell(cellCount);

        var cell1 = row.insertCell(cellCount);
        var element1 = document.createElement("input");
        $(cell1).addClass("tdgray");
        $(cell1).addClass("td-DeleteRecord");
        element1.style.paddingTop = "5%";
        element1.type = "image";
        element1.src = "../Images/delete.gif";
        element1.setAttribute('onclick', 'javascript:DeleteRecord("' + rowCount + '","' + Parent + '");return false;');
        cell1.appendChild(element1);

        $('#' + Parent + '_txtAddDays').val("");
        randomnumber = randomnumber + 1;
        $('#' + Parent + '_hdnCount').val(randomnumber);

        return false;
    }

    function DeleteRecord(RowIndex, Parent) {
        debugger;
        var Row;
        var table = document.getElementById(Parent + '_tbCustomNotificatonDays');
        var NotifyDaysArray = [];
        for (var i = 0; i < table.rows.length; i++) {
            if (typeof table.rows[i].cells[0].children[1] != 'undefined') {
                if (parseInt(RowIndex) == parseInt(table.rows[i].cells[0].children[1].value)) {
                    Row = i;
                    break;
                }
            }
        }
        $('#' + table.id + " tr:eq(" + Row + ")").remove();

        for (var i = 0; i < table.rows.length; i++) {
            NotifyDaysArray.push(table.rows[i].cells[1].innerText)
        }
        $('#' + Parent + '_tbCustomNotificatonDays tr').remove();
        BindNotificationTable(NotifyDaysArray, Parent);
        return false;
    }

    function SaveNotoficationDays(id) {
        debugger;
        var Parent = id.replace("_btnSave", "")
        var SelectedNotificatonDays = [];
        var DefaultNotifyDays = [15, 30, 60, 90]
        var table = document.getElementById(Parent + '_tbCustomNotificatonDays')

        for (var i = 0; i < DefaultNotifyDays.length; i++) {
            $("input:checkbox[id=" + Parent + "_chkNotifyDays" + DefaultNotifyDays[i] + "]:checked").each(function () {
                var label = $('label[for="' + this.id + '"]').text();
                SelectedNotificatonDays.push(label);
            });
        }

        for (var i = 0; i < table.rows.length; i++) {
            SelectedNotificatonDays.push(table.rows[i].cells[1].innerText)
        }
        $('#' + Parent + '_txtNotify').val(SelectedNotificatonDays.sort());

        //VisibleNotifyButton();
    }    

    function VisibleNotifyButton() {
        debugger;
        var ndTable = document.getElementById("ContentMain_ucBaseLeaseTerms_tbDueDates");
        var len = ndTable.rows.length;
        if (len > 0)
        {
            var objCells = ndTable.rows.item(len-1).cells;
            for (var j = 0; j < objCells.length; j++) {
                var x = x + ' ' + objCells.item(j).innerHTML;
            }
            //alert('value' + x);
            if (x != "") {
                $("#ContentMain_ucBaseLeaseTerms_btnNotifyMe").css("display", "block");
            }
            if (x == "undefined    ") {
                $("#ContentMain_ucBaseLeaseTerms_btnNotifyMe").css("display", "none");
            }
        }
        else {
            $("#ContentMain_ucBaseLeaseTerms_btnNotifyMe").css("display", "none");
        }

        //var ndTable = document.getElementById("ContentMain_ucBaseLeaseTerms_tbDueDates");
        //if(ndTable.rows.length > 0) {
        //        $("#ContentMain_ucBaseLeaseTerms_btnNotifyMe").css("display", "block");
        //    }
        //    else {
        //        $("#ContentMain_ucBaseLeaseTerms_btnNotifyMe").css("display", "none");
        //    }
    }

    //End of Notification Days Coding
</script>

<table cellspacing="0" class="OuterBorder" style="width: 100%">
    <tr style="background-color: gray;" class="BottomBorder">
        <td style="text-align: left;">
            <%-- BASE LEASE TERMS--%>
            <asp:Label ID="lbTermTitle" runat="server" Font-Bold="true" Font-Size="Small"></asp:Label>
            <br />
            <%--        <asp:Label ID="lbltest" runat="server" Text="TestJAY" ></asp:Label>--%>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnAddLeaseTerm">
                <table cellspacing="0">
                    <tr>
                        <td>
                            <h5>LEASE TERM </h5>
                        </td>
                        <td style="padding-left: 10px;">
                            <asp:TextBox ID="txtLeaseTermMonths" runat="server" Width="140px" Height="18px"></asp:TextBox></td>
                        <td style="padding-left: 10px;">
                            <h5>MONTHS</h5>
                        </td>
                        <td style="padding-left: 50px;">
                            <asp:Button ID="btnAddLeaseTerm" runat="server" Text="Add Rent Period" CssClass="SqureButton" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <%-- <table cellspacing="0">
                <tr>
                    <td>--%>
            <asp:UpdatePanel ID="upBaseLeaseTerm" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnBaseLeaseTerm" runat="server" Width="100%">
                        <asp:GridView ID="gvBaseLeaseTerm" runat="server" ShowFooter="true" Width="95%" CssClass="AllBorder"
                            HeaderStyle-CssClass="HeaderGridView"
                            RowStyle-CssClass="RowGridView"
                            FooterStyle-CssClass="FooterGridView" RowStyle-Height="16px"
                            AutoGenerateColumns="false"
                            OnRowDataBound="gvBaseLeaseTerm_Bind"
                            OnRowEditing="gvBaseLeaseTerm_RowEditing"
                            OnRowCancelingEdit="gvBaseLeaseTerm_RowCancelingEdit"
                            OnRowUpdating="gvBaseLeaseTerm_RowUpdating"
                            OnDataBound="gvBaseLeaseTerm_DataBound">
                            <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                            <%--  <HeaderStyle BackColor="LightGray" ForeColor="Black" Font-Bold="true" Font-Size="Small" Font-Names="Verdena" HorizontalAlign="Center" />--%>
                            <%--<FooterStyle BackColor="LightGray" ForeColor="Black" Font-Bold="true" Font-Size="Small" Font-Names="Verdena" Wrap="false" />
                            <RowStyle Font-Size="Small" Font-Names="Verdena" />Height="100%"--%>
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="12%" HeaderText="RENT PERIOD" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtItemFromRentPeriod" runat="server" Text='<%#Eval("RentFromPeriod") %>' Width="40px" Style="text-align: right;"></asp:TextBox>

                                        <asp:TextBox ID="txtItemToRentPeriod" runat="server" Text='<%#Eval("RentToPeriod") %>' Width="40px" Style="text-align: right;"></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="lbItemFromRentPeriod" runat="server" Text='<%#Eval("RentFromPeriod")%>' Width="40px" ReadOnly="true" Style="text-align: right;" Height="16px"></asp:TextBox>

                                        <asp:TextBox ID="lbItemToRentPeriod" runat="server" Text='<%#Eval("RentToPeriod")%>' Width="40px" ReadOnly="true" Style="text-align: right;" Height="16px"></asp:TextBox>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="12%" HeaderText="TOTAL MONTHS" HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="Center">
                                    <EditItemTemplate>
                                        <asp:Label ID="lbEditTotMonths" runat="server" Text='<%#Eval("TotalMonths")%>' Style="text-align: right;"></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbItemTotMonths" runat="server" Text='<%#Eval("TotalMonths")%>' Style="text-align: right;"></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="12%" HeaderText="RENT(PSF)" HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="Center">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtEditRentPSP" runat="server" Text='<%#String.Format("{0:n2}",Eval("Rent_PSF")) %>' Width="70px" Style="text-align: right;"></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <%--  <asp:Label ID="lbItemRentPSP" runat="server" Text='<%#Eval("Rent_PSF")%>' Style="text-align: right;"></asp:Label>--%>
                                       $<asp:Label ID="lbItemRentPSP" runat="server" Text='<%#String.Format("{0:n2}",Eval("Rent_PSF")) %>' Style="text-align: right;"></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="12%" HeaderText="ANNUAL RENT" HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="Right">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtEditAnnualRent" runat="server" Text='<%#String.Format("{0:n2}",Eval("AnnualRent")) %>' Width="100px" Style="text-align: right;"></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <%-- <asp:Label ID="lbItemAnnualRent" runat="server" Text='<%#Eval("AnnualRent")%>' Style="text-align: right;"></asp:Label>--%>
                                       $<asp:Label ID="lbItemAnnualRent" runat="server" Text='<%#String.Format("{0:n2}",Eval("AnnualRent")) %>' Style="text-align: right;"></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="12%" HeaderText="% INCREASE" HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="Right">
                                    <EditItemTemplate>
                                        <asp:Label ID="lbPerIncrese" runat="server" Text='<%#String.Format("{0:n2}",Eval("PerIncrease")) %>' Style="text-align: right;"></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <%--  <asp:Label ID="lbItemPerIncrese" runat="server" Text='<%#Eval("PerIncrease")%>' Style="text-align: right;"></asp:Label>--%>
                                        <asp:Label ID="lbItemPerIncrese" runat="server" Text='<%#String.Format("{0:n2}",Eval("PerIncrease")) %>' Style="text-align: right;"></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="18%" HeaderText="PERIOD RENT" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                    <EditItemTemplate>
                                        <%-- <asp:Label ID="lbPeriodRent" runat="server" Text='<%#Eval("PeriodRent")%>'  Style="text-align: right;"></asp:Label>--%>
                                        <asp:Label ID="lbPeriodRent" runat="server" Text='<%#String.Format("{0:n2}",Eval("PeriodRent")) %>' Style="text-align: right;"></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <%-- <asp:Label ID="lbItemPeriodRent" runat="server" Text='<%#Eval("PeriodRent")%>' Style="text-align: right;"></asp:Label>--%>
                                      $<asp:Label ID="lbItemPeriodRent" runat="server" Text='<%#String.Format("{0:n2}",Eval("PeriodRent")) %>' Style="text-align: right;"></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <%-- <asp:Label ID="lblPeriodRent" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>--%>
                                      $<asp:Label ID="lblPeriodRent" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="12%" HeaderText="TOTAL SF" HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                    <EditItemTemplate>
                                        <%-- <asp:Label ID="lbTotSF" runat="server" Text='<%#Eval("TotalSF")%>' Style="text-align: right;"></asp:Label>--%>
                                        <asp:Label ID="lbTotSF" runat="server" Text='<%#String.Format("{0:n2}",Eval("TotalSF")) %>' Style="text-align: right;"></asp:Label>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <%--<asp:Label ID="lbItemTotSF" runat="server" Text='<%#Eval("TotalSF")%>' Style="text-align: right;"></asp:Label>--%>

                                        <asp:Label ID="lbItemTotSF" runat="server" Text='<%#String.Format("{0:n2}",Eval("TotalSF")) %>' Style="text-align: right;"></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>

                                        <%-- <asp:Label ID="txtTotSF" runat="server" Style="text-align: right;"></asp:Label>--%>

                                        <asp:Label ID="txtTotSF" runat="server" Style="text-align: right;"></asp:Label>

                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="4%" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                    <EditItemTemplate>
                                        <asp:LinkButton runat="server" ID="btnUpdate" Text="Update" CommandName="Update" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnEdit" CommandName="Edit" ImageUrl="~/Images/edit.jpg" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="4%" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                    <EditItemTemplate>
                                        <asp:LinkButton runat="server" ID="btnCancel" Text="Cancel" CommandName="Cancel" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnDelete" CommandName="Delete" OnClientClick='javascript:return confirm("Do You Want To Delete This Record");' OnClick="gvBaseLeaseTerm_DelbuttonClick" ImageUrl="~/Images/delete.gif" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ItemStyle-Width="5%" DataField="LeaseTermId" />
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <%-- </td>
                </tr>
            </table>--%>
        </td>
    </tr>
    <tr>
        <td>
            <%--  <asp:UpdatePanel ID="upt" runat="server">
                <ContentTemplate>--%>
            <div id="pnAddBaseLeaseTerm" width="100%" style="display: none;" runat="server">
                <%-- --%>
                <table class="OuterBorder" cellspacing="12" cellpadding="2">
                    <tr style="background-color: gray; font-weight: 600;">

                        <%-- <tr> <td colspan="6">
                                <table>
                                    <tr>--%>
                        <td>RENT PERIOD </td>
                        <td>RENT(PSF) </td>
                        <td>ANNUAL RENT </td>
                        <td>% INCREASE</td>
                        <td>TOTAL SF </td>
                        <td colspan="2" align="center">
                            <asp:ImageButton runat="server" ID="btnClosePanel" ImageUrl="~/Images/delete.gif" />
                        </td>
                        <%--</tr>     </table>
                            </td>
                        </tr>--%>
                    </tr>
                    <tr id="dvAddNewRow" runat="server" style="border: 1px solid black">
                        <td style="vertical-align: middle;" class="colGray">
                            <asp:TextBox ID="txtFromRentPeriod" runat="server" Width="32px" Height="12px"></asp:TextBox>
                            <label>-</label>
                            <asp:TextBox ID="txtToRentPeriod" runat="server" Width="32px" Height="12px"></asp:TextBox>
                        </td>
                        <td class="colyellow">
                            <asp:TextBox ID="txtRentPSP" runat="server" Height="16px" Width="80px" CssClass="DollarTextBoxWithBorder"></asp:TextBox>
                        </td>
                        <td class="colGray">
                            <asp:TextBox ID="txtAnnualRent" runat="server" Height="16px" Width="100px" CssClass="DollarTextBoxWithBorder"></asp:TextBox>
                        </td>
                        <td style="text-align: center" class="colyellow">
                            <asp:TextBox ID="txtPerIncrese" runat="server" Height="16px" Width="80px"></asp:TextBox>
                        </td>
                        <td class="colGray">
                            <asp:Label ID="lblTotalSF" runat="server" Height="16px"></asp:Label>
                        </td>
                        <td class="colyellow">
                            <asp:Button ID="btnAddLeaseTerm_done" runat="server" Text="+" OnClick="btnAddLeaseTerm_Click" CssClass="SqureButton" />
                        </td>
                    </tr>
                </table>
            </div>
            <%-- </ContentTemplate>
            </asp:UpdatePanel>--%>
        </td>
    </tr>
    <tr runat="server" id="optPayable">
        <td style="background-color: gray">
            <%--   BASE COMMISSIONABLE TERM--%>
            <asp:Label ID="lbCommTitle" runat="server" Font-Bold="true" Font-Size="Small"> </asp:Label>
        </td>
    </tr>
    <tr runat="server" id="optPayable_1">
        <td>
            <table cellspacing="0">
                <tr>
                    <td>
                        <h5>LEASE TERM </h5>
                    </td>
                    <td>
                        <asp:TextBox ID="txtBaseCommTermMonths" runat="server" Width="140px" Height="18px" AutoPostBack="true"></asp:TextBox></td>
                    <td>
                        <h5>MONTHS</h5>

                    </td>
                    <%--<td style="padding-left: 40px">
                        <asp:Button ID="btnFillBaseCommTermGrid" runat="server" Text="Fill Grid" CssClass="SqureButton" Visible="false" /> OnTextChanged="txtBaseCommTermMonths_TextChanged"
                    </td>--%>
                    <td style="padding-left: 35px;">
                        <asp:Button ID="btnUpdateCommTerm" runat="server" OnClick="chkSameCommTerm_CheckChanged" Text="UPDATE COMMISSION TERM" CssClass="SqureButton" />
                        <%-- <span style="padding-left: 15px">
                            <asp:Button ID="btnUPDgvBaseCommTerms" runat="server" Text="Update Commision term" OnClick="btnUPDgvBaseCommTerms_Click" />
                        </span>--%>
                        <asp:CheckBox ID="chkSameCommTerm" runat="server" Text="COMMISSION TERM SAME AS LEASE TERM?" Font-Size="Smaller" AutoPostBack="true" OnCheckedChanged="chkSameCommTerm_CheckChanged" Visible="false" />
                    </td>
                    <%-- <td style="padding-top: 40px; padding-left: 5px">
                        <asp:Label ID="Label2" runat="server" Text="COMMISSION TERM SAME AS LEASE TERM?"></asp:Label></td>AutoPostBack="true"--%>
                </tr>
                <tr>
                    <td>
                        <h5>COMMISSION TYPE</h5>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCommissionType" runat="server" OnSelectedIndexChanged="chkSameCommTerm_CheckChanged" AutoPostBack="true">
                            <asp:ListItem Text="Rent Percentage" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Per Square Foot" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Flat Fee" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox runat="server" ID="txtBaseCommPer" Width="140px" Height="18px" onchange="return CheckCommissionTypeValue(this.id);" AutoPostBack="true"></asp:TextBox>
                        <asp:Button runat="server" ID="btnBaseCommPer" OnClick="chkSameCommTerm_CheckChanged" Style="display: none;" />
                    </td>
                    <td></td>
                    <td>&nbsp;&nbsp;&nbsp;<asp:Label ID="lblCommissionAmountSquareFoot" runat="server" Text="Commission Amount: " Font-Bold="true"></asp:Label><asp:Label ID="lblComAmountSquareFoot" runat="server"></asp:Label>
                        <asp:HiddenField ID="hdnTotalSquareFoot" runat="server" Value="0" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr runat="server" id="optPayable_2">
        <td>
            <%--<div id="dvOptCommTerm" runat="server">--%>

            <%--       <table cellspacing="0" class="wdthAll">
                    <tr>
                        <td style="width: 80%" colspan="2">--%>
            <asp:UpdatePanel ID="upBaseCommTerms" runat="server">
                <ContentTemplate>

                    <asp:Panel ID="pnBaseCommTerms" runat="server" Width="56%">



                        <br />

                        <asp:GridView ID="gvBaseCommTerms" runat="server" Width="95%" AutoGenerateColumns="false" ShowFooter="true" OnRowDataBound="gvbasecommtermsedit_Bind"
                            OnDataBound="gvbasecommtermsedit_DataBound" RowStyle-Height="16px"
                            CssClass="AllBorder" HeaderStyle-CssClass="HeaderGridView" RowStyle-CssClass="RowGridView" FooterStyle-CssClass="FooterGridView">
                            <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                            <Columns>
                                <asp:TemplateField HeaderText="RENT PERIOD" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="center" ItemStyle-Width="7%">
                                    <ItemTemplate>
                                        <asp:TextBox ID="lbItemFromRentPeriod" runat="server" Text='<%#Eval("RentFromPeriod")%>' Width="40px" onkeydown="javascript:return false" Style="text-align: right" Height="16px"></asp:TextBox>

                                        <asp:TextBox ID="lbItemToRentPeriod" runat="server" Text='<%#Eval("RentToPeriod")%>' Width="40px" onkeydown="javascript:return false" Style="text-align: right" Height="16px"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText=" TOTAL MONTHS " HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                                    <ItemTemplate>
                                        <asp:Label ID="lbItemTotMonths" runat="server" Text='<%#Eval("TotalMonths")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText=" % BROKER  COMMISSION " HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="25%">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtEditBrokerCommission" runat="server" Width="70px" Style="text-align: right" Height="16px" Text='<%#String.Format("{0:n2}",Eval("BrokerCommission")) %>'></asp:TextBox>
                                        <%----%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="FEE" HeaderStyle-Wrap="true" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtEditFee" runat="server" Text='<%#String.Format("{0:n2}",Eval("Fee")) %>' Style="text-align: right" Height="16px"></asp:TextBox>
                                        <asp:HiddenField ID="hdnFeeBaseAmt" runat="server" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        $
                                        <asp:Label ID="lblTotFee" runat="server" Style="text-align: right"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="CommissionableTermId" ItemStyle-Width="5%" />
                            </Columns>
                        </asp:GridView>
                        <asp:Button ID="btnsavechanges" Text="Update Changes" runat="server" Visible="false" CssClass="SqureButton" OnClick="btnsavechanges_Click" />

                        <br />
                    </asp:Panel>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
            <%--</td>
            
                    </tr>
                </table>--%>
            <%-- </div>--%>
        </td>

    </tr>
    <tr runat="server" id="optPayable_3">
        <td>
            <div id="pnAddBaseCommissionableTerm" width="80%" style="display: none;" runat="server">
                <table border="1">
                    <thead style="background-color: gray;">
                        <tr>
                            <%--  <td>LEASE TERM
                                                                <br />
                                (YEARS) </td>--%>
                            <td>RENT PERIOD </td>
                            <td>% BROKER COMMISSION </td>
                            <td colspan="2">FEE</td>
                        </tr>
                    </thead>
                    <tr>
                        <%-- <td>
                            <asp:TextBox ID="txtCommLeaseTerms" runat="server" Width="50px" style="text-align:right"></asp:TextBox>
                        </td>--%>
                        <td style="vertical-align: middle;">
                            <asp:TextBox ID="txtCommFromRentPeriod" runat="server" Width="30px" Style="text-align: right"></asp:TextBox>
                            <label>-</label>
                            <asp:TextBox ID="txtCommToRentPeriod" runat="server" Width="30px" Style="text-align: right"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtBrokerPer" runat="server" Width="80px" Style="text-align: right"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtBrokerFee" runat="server" Width="120px" Style="text-align: right"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnAddBaseCommissionableTerm" runat="server" Text="+" OnClick="btnAddCommissionableTerm_Click" CssClass="SqureButton" />
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <%--  <tr runat="server" id="optPayable_4">
        <td style="width: 30%;" colspan="3">COMMISSION DaDATE(S) 
              &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp; &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp; 
            <asp:TextBox ID="txtDueDates" runat="server" Width="80px"></asp:TextBox>
            <asp:ImageButton ID="btnDueDates" runat="server" Width="16px" Height="16px" ImageUrl="~/Images/Calander.jpeg" />
            <ajax:CalendarExtender ID="ajaxCal" runat="server" TargetControlID="txtDueDates" PopupButtonID="btnDueDates" Format="dd/MM/yyyy"></ajax:CalendarExtender>
            &nbsp;&nbsp;&nbsp;&nbsp;     
             <asp:TextBox ID="txtAddCommPerOnDueDt" runat="server" Width="40px"></asp:TextBox>%
        &nbsp;&nbsp;&nbsp;&nbsp;    
             <asp:TextBox ID="txtAddCommAmountOnDueDt" runat="server" Width="80px" ReadOnly="true"></asp:TextBox></td>
        <td style="width: 20%; vertical-align: bottom;" colspan="2">
            <asp:Button ID="btnDueDtAdd" runat="server" Text="+ADD" CssClass="SqureButton"></asp:Button>
        </td>

    </tr>--%>
    <tr runat="server" id="optPayable_4">
        <td style="background-color: gray">
            <%--   BASE COMMISSIONABLE TERM--%>
            <asp:Label ID="Label1" runat="server" Font-Bold="true" Font-Size="Small" Text="COMMISSION"> </asp:Label>
        </td>
    </tr>
    <tr runat="server" id="optPayable_6">
        <td>
            <table cellspacing="0" class="wdthAll">
                <%--class="wdthAll"--%>
                <br />

                <tr>
                    <td style="width: 15%"></td>
                    <td style="width: 85%;"><%--style="text-align: left;"padding-left: 50px--%>
                        <div id="pnDueDt" runat="server" style="width: 100%;">
                            <table id="tbDueDates" runat="server" width="100%">
                                <tr>
                                    <td style="width: 20%"></td>
                                    <td style="width: 18%"></td>
                                    <td style="width: 15%"></td>
                                    <td style="width: 47%"></td>
                                </tr>
                            </table>

                            <%-- <asp:Table ID="tbDueDates" runat="server" Width="100%" BorderWidth="2" BorderColor="Transparent">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right">
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" CssClass="Padding">
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right">
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right">
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>--%>
                            <asp:HiddenField ID="hdnDueDateCount" runat="server" Value="1" />
                              </div>
                    </td>
                </tr>
            </table>
        </td>
        <%-- <td colspan="4"></td>--%>
    </tr>
    <tr runat="server" id="optPayable_5">
        <td>

            <table cellspacing="0" class="wdthAll">
                <tr>
                    <td style="width: 15%;">
                        <h5>DUE DATE(S)</h5>
                    </td>
                    <td style="width: 18%;">
                        <asp:TextBox ID="txtDueDates" runat="server" Width="90px" Height="16px"></asp:TextBox>
                        <asp:ImageButton ID="btnDueDates" runat="server" Width="16px" Height="16px" ImageUrl="~/Images/Calander.jpeg" />
                        <ajax:CalendarExtender ID="ajaxCal" runat="server" TargetControlID="txtDueDates" PopupButtonID="btnDueDates" Format="MM/dd/yyyy"></ajax:CalendarExtender>
                        <%--OnClientDateSelectionChanged="CheckDateEalier"--%>
                    </td>
                    <td style="width: 12%;">
                        <asp:TextBox ID="txtAddCommPerOnDueDt" runat="server" Width="40px" Height="16px"></asp:TextBox>
                        <asp:Label ID="lblPercentage" runat="server" Text="%"></asp:Label>
                    </td>
                    <td style="width: 13%;">
                        <asp:TextBox ID="txtAddCommAmountOnDueDt" runat="server" Width="100px" ReadOnly="true" Height="17px" CssClass="DollarTextBoxWithBorder"></asp:TextBox>
                    </td>
                    <td style="width: 52%">
                        <input type="button" id="btnDueDtAdd" runat="server" value="ADD" cssclass="SqureButton"></input>
                        <%--<asp:Button ID="btnDueDtAdd"  runat="server" Text="ADD" CssClass="SqureButton"></asp:Button>--%>
                        <asp:HiddenField ID="hdnSetTrue" runat="server" />
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btnNotifyMe" Text="Notify Me" CssClass="SqureButton" Width="85px" Style="display: none;" OnClientClick="javascript:FillNotifyDaysPopup(this.id);return false;" />
                        <asp:HiddenField ID="hdnPopup" runat="server" />
                        <ajax:ModalPopupExtender ID="mpNotifyExtendr" runat="server" PopupControlID="dvPopupNotifyDetails" TargetControlID="hdnPopup" CancelControlID="btnCancelPopup" BackgroundCssClass="modalBackground"></ajax:ModalPopupExtender>
                        <%--<asp:TextBox runat="server" ID="txtnotify" Text="15,30,60,90" Style="width: 75px;"></asp:TextBox>--%>
                        <asp:HiddenField runat="server" ID="txtNotify" Value="15,30,60,90" />
                    </td>

                </tr>
            </table>
        </td>
    </tr>


</table>
<asp:HiddenField ID="hdnSizeSF" runat="server" />
<asp:HiddenField ID="hdnIsPayment" runat="server" Value="0" />

<span id="spPixelWidth" style="visibility: hidden;"></span>
<%--<asp:HiddenField ID="hdnPrevMonth" runat="server" /> 
<asp:HiddenField ID="hdnRowInx" runat="server" />

<asp:Button ID="delRecodrs" runat="server" OnClick="gvBaseCommTerms_DelbuttonClick"  />--%>

<div id="dvPopupNotifyDetails" runat="server" class="PopupDivBodyStyle" style="display: none; height: 230px; width: 250px;">
    <table cellspacing="0" cellpadding="4" style="width: 100%">
        <asp:UpdatePanel ID="upNotifyDays" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <tr class="trBorder">
                    <td colspan="2">
                        <h3 style="margin: 6px;">Notify Days</h3>
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="btnCancelPopup" Height="15px" Width="15px" ImageUrl="~/Images/close.png" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:CheckBox runat="server" ID="chkNotifyDays15" Text="15" Checked="true" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="chkNotifyDays30" Text="30" Checked="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:CheckBox runat="server" ID="chkNotifyDays60" Text="60" Checked="true" />
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="chkNotifyDays90" Text="90" Checked="true" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">
                       <div Style="overflow: auto; height: 50px;">
                        <asp:Table runat="server" ID="tbCustomNotificatonDays" Style="width: 50%;">
                        </asp:Table>
         </div>
                        <asp:HiddenField ID="hdnCount" runat="server" Value="2" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center;">
                        <asp:TextBox ID="txtAddDays" runat="server"  Width="30%" onkeypress="return isInteger(event,this.id)"></asp:TextBox>&nbsp;&nbsp;
                        <asp:Button ID="btnAddDays" runat="server" Text="+" CssClass="SqureButton" OnClientClick="addCustomNotificationDayRows(this.id);return false;"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center;">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnSmallOrange" OnClientClick="SaveNotoficationDays(this.id);" Style="height: 30px; width: 65px; margin-top: 5px;" OnClick="btnSave_Click"></asp:Button>
                    </td>
                </tr>
            </ContentTemplate>
            <Triggers>
            </Triggers>
        </asp:UpdatePanel>
    </table>
</div>



