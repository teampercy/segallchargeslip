﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SegallChargeSlipBll;

public partial class Admin_SettingMaster : System.Web.UI.MasterPage
{
    #region Variables

    #endregion

    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {

        //SetSession();
        if (!IsPostBack)
        {
            if (Session[GlobleData.User] != null)
            {
                if (Convert.ToUInt32(Session["UserType"]) == 1)
                {
                    //
                    if (Session["Type"] != null)
                    {
                        if (Session["Type"].ToString() == "DealType")
                        {
                            tdDealType.Style.Add("background-color", "#F5F6CE");
                        }
                        else if (Session["Type"].ToString() == "DealSubType")
                        {
                            tdDealSubType.Style.Add("background-color", "#F5F6CE");
                        }
                        else if (Session["Type"].ToString() == "DealUse")
                        {
                            tdDealUse.Style.Add("background-color", "#F5F6CE");
                        }
                        else if (Session["Type"].ToString() == "ChargeTo")
                        {
                            tdChargeTo.Style.Add("background-color", "#F5F6CE");
                        }
                        else if (Session["Type"].ToString() == "Location")
                        {
                            tdLocation.Style.Add("background-color", "#F5F6CE");
                        }
                        else if (Session["Type"].ToString() == "Location")
                        {
                            tdLocation.Style.Add("background-color", "#F5F6CE");
                        }
                        else if (Session["Type"].ToString() == "Buyers")
                        {
                            tdBuyers.Style.Add("background-color", "#F5F6CE");
                        }
                        else if (Session["Type"].ToString() == "Tenants")
                        {
                            tdTenants.Style.Add("background-color", "#F5F6CE");
                        }
                        else if (Session["Type"].ToString() == "Vendor")
                        {
                            tdVendor.Style.Add("background-color", "#F5F6CE");
                        }
                        else if (Session["Type"].ToString() == "CSSentToEmails")
                        {
                            tdCSSentToEmails.Style.Add("background-color", "#F5F6CE");
                        }
                        
                        //Session["Type"] = null;
                    }
                    //

                    NavigationMenu.Items.Add(new MenuItem("SNAPSHOT", "SNAPSHOT", "", "~/Admin/Snapshot.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("TIMELINE", "TIMELINE", "", "~/Admin/Timeline.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("INBOX", "INBOX", "", "~/Pages/Inbox.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("VENDORS", "VENDORS", "", "~/Admin/Vendors.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("CUSTOMERS", "CUSTOMERS", "", "~/Admin/Customers.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("PAYMENTS", "PAYMENTS", "", "~/Admin/Payments.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("REPORTS", "REPORTS", "", "~/Pages/Reports.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("USERS", "USERS", "", "~/Admin/Users.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("SETTINGS", "SETTINGS", "", "~/Admin/Setting.aspx"));
                    
                    Messages obj = new Messages();
                    DataSet ds = new DataSet();
                    Label lbl = new Label();
                    lbl.ID = "lblInbox";
                    Boolean isNewMessages = false;
                    ds = obj.GetMessagesUnRead(Convert.ToInt64(Session[GlobleData.User].ToString()));
                    if (ds != null)
                    {
                        if (ds.Tables[0] != null)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                isNewMessages = true;
                            }
                        }
                    }

                    if (isNewMessages)
                    {
                        if (Request.Url.AbsoluteUri.ToLower().Contains("inbox"))
                        {
                            lbl.Text = "<img src='../Images/InboxWhite3.png' height='14px' width='14px' valign='top' border='0px'/>";
                            //lbl.Text = "!";
                            lbl.ForeColor = System.Drawing.Color.Red;
                            lbl.CssClass = "Notification";
                            //ImageButton btn = new ImageButton();"!";
                            //btn.ID = "btnNotification";
                            //btn.ImageUrl = "../Images/dollar-icon.jpg";
                            NavigationMenu.Items[2].Text = "INBOX " + lbl.Text;
                        }
                        else
                        {
                            lbl.Text = "<img src='../Images/InboxOrangeNotification.png' height='14px' width='14px' border='0px'/>";
                            //lbl.Text = "!";
                            lbl.ForeColor = System.Drawing.Color.Red;
                            lbl.CssClass = "Notification";
                            //ImageButton btn = new ImageButton();"!";
                            //btn.ID = "btnNotification";
                            //btn.ImageUrl = "../Images/dollar-icon.jpg";
                            NavigationMenu.Items[2].Text = "INBOX " + lbl.Text;
                        }
                    }
                    foreach (MenuItem item in NavigationMenu.Items)
                    {
                        //if (Request.Url.AbsoluteUri.ToLower().Contains(Page.ResolveUrl(item.NavigateUrl.ToLower())))
                        //{
                        //    item.Selected = true;
                        //}
                        string itemurl = item.NavigateUrl.ToLower();
                        int index = itemurl.LastIndexOf('/');
                        string urlpage = itemurl.Substring(index + 1);

                        if (Request.Url.AbsoluteUri.ToLower().Contains(urlpage))
                        {
                            item.Selected = true;
                            break;
                        }
                        
                    }

                    lbUser.Text = Session["UserName"].ToString();
                    if (Request.Url.AbsoluteUri.ToLower().Contains("admin/addoreditvendor.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("admin/chargeto.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("admin/dealtype.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("admin/dealsubtype.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("admin/dealuse.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("admin/tenants.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("admin/shoppingcenter.aspx"))
                    {
                        NavigationMenu.Items[8].Selected = true;
                    }

                }
                else if (Convert.ToUInt32(Session["UserType"]) == 2)
                {
                    NavigationMenu.Items.Add(new MenuItem("SNAPSHOT", "SNAPSHOT", "", "~/Pages/Snapshot.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("TIMELINE", "TIMELINE", "", "~/Pages/Timeline.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("INBOX", "INBOX", "", "~/Pages/Inbox.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("NEW CHARGE", "NEWCHARGE", "", "~/Pages/AddOrEditNewChargeSlip.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("BILLED", "BILLED", "", "~/Pages/Billed.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("REPORTS", "REPORTS", "", "~/Pages/Reports.aspx"));

                   

                    //NavigationMenu.Items.Add(new MenuItem("SNAPSHOT", "SNAPSHOT"));
                    //NavigationMenu.Items.Add(new MenuItem("INBOX", "INBOX"));
                    //NavigationMenu.Items.Add(new MenuItem("NEW CHARGE", "NEWCHARGE"));
                    //NavigationMenu.Items.Add(new MenuItem("BILLED", "BILLED"));
                    //NavigationMenu.Items.Add(new MenuItem("REPORTS", "REPORTS"));
                    Messages obj = new Messages();
                    DataSet ds = new DataSet();
                    Label lbl = new Label();
                    lbl.ID = "lblInbox";
                    Boolean isNewMessages = false;
                    ds = obj.GetMessagesUnRead(Convert.ToInt64(Session[GlobleData.User].ToString()));
                    if (ds != null)
                    {
                        if (ds.Tables[0] != null)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                isNewMessages = true;
                            }
                        }
                    }

                    if (isNewMessages)
                    {
                        if (Request.Url.AbsoluteUri.ToLower().Contains("inbox"))
                        {
                            lbl.Text = "<img src='../Images/InboxWhite3.png' height='14px' width='14px' valign='top' border='0px'/>";
                            //lbl.Text = "!";
                            lbl.ForeColor = System.Drawing.Color.Red;
                            lbl.CssClass = "Notification";
                            //ImageButton btn = new ImageButton();"!";
                            //btn.ID = "btnNotification";
                            //btn.ImageUrl = "../Images/dollar-icon.jpg";
                            NavigationMenu.Items[2].Text = "INBOX " + lbl.Text;
                        }
                        else
                        {
                            lbl.Text = "<img src='../Images/InboxOrangeNotification.png' height='14px' width='14px' border='0px' />";
                            //lbl.Text = "!";
                            lbl.ForeColor = System.Drawing.Color.Red;
                            lbl.CssClass = "Notification";
                            //ImageButton btn = new ImageButton();"!";
                            //btn.ID = "btnNotification";
                            //btn.ImageUrl = "../Images/dollar-icon.jpg";
                            NavigationMenu.Items[2].Text = "INBOX " + lbl.Text;
                        }
                    }
                    foreach (MenuItem item in NavigationMenu.Items)
                    {
                        string itemurl = item.NavigateUrl.ToLower();
                        int index = itemurl.LastIndexOf('/');
                        string urlpage = itemurl.Substring(index + 1);

                        if (Request.Url.AbsoluteUri.ToLower().Contains(urlpage))
                        {
                            item.Selected = true;
                            break;
                        }
                       

                    }

                    lbUser.Text = Session["UserName"].ToString();
                    if (Request.Url.AbsoluteUri.ToLower().Contains("pages/newcharge.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("pages/newchargeleaseterms.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("pages/newchargecommissionspitcalculator.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("pages/newchargeslipreview.aspx"))
                    {
                        NavigationMenu.Items[3].Selected = true;
                    }
                }



            }
            else
            {   // SK : 08/12/2013
                Session.Clear();
                Session.RemoveAll();
                Response.Redirect("~/Login.aspx", false);
            }
            //   SetButtonList();
            
            
            //if (Session["DealSubType"] != null)
            //{
            //    if (Session["DealSubType"].ToString() == "DealSubType")
            //    {
            //        tdDealSubType.Style.Add("background-color", "#F5F6CE");
            //    }
            //}
            //if (Session["DealUse"] != null)
            //{
            //    if (Session["DealUse"].ToString() == "DealUse")
            //    {
            //        tdDealUse.Style.Add("background-color", "#F5F6CE");
            //    }
            //}
            //if (Session["ChargeTo"] != null)
            //{
            //    if (Session["ChargeTo"].ToString() == "ChargeTo")
            //    {
            //        tdChargeTo.Style.Add("background-color", "#F5F6CE");
            //    }
            //}
            //if (Session["Location"] != null)
            //{
            //    if (Session["Location"].ToString() == "Location")
            //    {
            //        tdLocation.Style.Add("background-color", "#F5F6CE");
            //    }
            //}
          
        }
    }
    #endregion

    #region Button Events
    protected void btnLogOut_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Session.RemoveAll();
        Response.Redirect("~/Login.aspx", false);
    }
    #endregion
    private void SetSession()
    {
        Session["Type"] = null;
        //Session["DealSubType"] = null;
        //Session["DealUse"] = null;
        //Session["ChargeTo"] = null;
        //Session["Location"] = null;
    }
    protected void lnkDealType_Click(object sender, EventArgs e)
    {
        //SetSession();
        Session["Type"] = "DealType";
        Response.Redirect("DealType.aspx", false);
    }
    protected void lnkSubType_Click(object sender, EventArgs e)
    {
        //SetSession();
        Session["Type"] = "DealSubType";
        Response.Redirect("DealSubType.aspx", false);
    }
    protected void lnkDealUse_Click(object sender, EventArgs e)
    {
        //SetSession();
        Session["Type"] = "DealUse";
        Response.Redirect("DealUse.aspx", false);
    }
    protected void lnkChargeTo_Click(object sender, EventArgs e)
    {
        //SetSession();
        Session["Type"] = "ChargeTo";
        Response.Redirect("ChargeTo.aspx", false);
    }
    protected void lnkLocation_Click(object sender, EventArgs e)
    {
        //SetSession();
        Session["Type"] = "Location";
        Response.Redirect("ShoppingCenter.aspx", false);
    }
    protected void lnkTenants_Click(object sender, EventArgs e)
    {
        Session["Type"] = "Tenants";
        Response.Redirect("Tenants.aspx", false);
    }
    protected void lnkBuyers_Click(object sender, EventArgs e)
    {
        Session["Type"] = "Buyers";
        Response.Redirect("Tenants.aspx", false);
    }
    protected void lnkVendor_Click(object sender, EventArgs e)
    {
        Session["Type"] = "Vendor";
        Response.Redirect("AddorEditVendor.aspx", false);
    }
    protected void lnkCSSentToEmails_Click(object sender, EventArgs e)
    {
        Session["Type"] = "CSSentToEmails";
        Response.Redirect("CSSentToEmails.aspx", false);
    }
}
