﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using SegallChargeSlipBll;
public partial class MasterPage_SegallChargeSlip : System.Web.UI.MasterPage
{

    #region Variables

    #endregion

    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session[GlobleData.User] != null)
            {
                if (Convert.ToUInt32(Session["UserType"]) == 1) 
                {
                    Session["Type"] = null;
                    NavigationMenu.Items.Add(new MenuItem("SNAPSHOT", "SNAPSHOT", "", "~/Admin/Snapshot.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("TIMELINE", "TIMELINE", "", "~/Admin/Timeline.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("INBOX", "INBOX", "", "~/Pages/Inbox.aspx"));
                    //NavigationMenu.Items.Add(new MenuItem("", "", "~/Images/delete.gif"));
                    NavigationMenu.Items.Add(new MenuItem("VENDORS", "VENDORS", "", "~/Admin/Vendors.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("CUSTOMERS", "CUSTOMERS", "", "~/Admin/Customers.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("PAYMENTS", "PAYMENTS", "", "~/Admin/Payments.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("REPORTS", "REPORTS", "", "~/Pages/Reports.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("USERS", "USERS", "", "~/Admin/Users.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("SETTINGS", "SETTINGS", "", "~/Admin/Setting.aspx"));
                    Messages obj = new Messages();
                    DataSet ds = new DataSet();
                    Label lbl = new Label();
                    lbl.ID = "lblInbox";
                    Boolean isNewMessages = false;
                    ds = obj.GetMessagesUnRead(Convert.ToInt64(Session[GlobleData.User].ToString()));
                    if (ds != null)
                    {
                        if (ds.Tables[0] != null)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                isNewMessages = true;
                            }
                        }
                    }

                    if (isNewMessages)
                    {
                        if (Request.Url.AbsoluteUri.ToLower().Contains("inbox"))
                        {
                            lbl.Text = "<img src='../Images/InboxWhite3.png' height='14px' width='14px' valign='top' border='0px'/>";
                            //lbl.Text = "!";
                            lbl.ForeColor = System.Drawing.Color.Red;
                            lbl.CssClass = "Notification";
                            //ImageButton btn = new ImageButton();"!";
                            //btn.ID = "btnNotification";
                            //btn.ImageUrl = "../Images/dollar-icon.jpg";
                            NavigationMenu.Items[2].Text = "INBOX " + lbl.Text;
                        }
                        else
                        {
                            lbl.Text = "<img src='../Images/InboxOrangeNotification.png' height='14px' width='14px' border='0px'/>";
                            //lbl.Text = "!";
                            lbl.ForeColor = System.Drawing.Color.Red;
                            lbl.CssClass = "Notification";
                            //ImageButton btn = new ImageButton();"!";
                            //btn.ID = "btnNotification";
                            //btn.ImageUrl = "../Images/dollar-icon.jpg";
                            NavigationMenu.Items[2].Text = "INBOX " + lbl.Text;
                        }
                    }

                    foreach (MenuItem item in NavigationMenu.Items)
                    {
                        //if (Request.Url.AbsoluteUri.ToLower().Contains(Page.ResolveUrl(item.NavigateUrl.ToLower())))
                        //{
                        //    item.Selected = true;
                        //}
                        string itemurl = item.NavigateUrl.ToLower();
                        int index = itemurl.LastIndexOf('/');
                        string urlpage = itemurl.Substring(index + 1);

                        if (Request.Url.AbsoluteUri.ToLower().Contains(urlpage))
                        {
                            item.Selected = true;
                            break;
                        }
                       
                    }

                    lbUser.Text = Session["UserName"].ToString();

                }
                else if (Convert.ToUInt32(Session["UserType"]) == 2)
                {
                    NavigationMenu.Items.Add(new MenuItem("SNAPSHOT", "SNAPSHOT", "", "~/Pages/Snapshot.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("TIMELINE", "TIMELINE", "", "~/Pages/Timeline.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("INBOX", "INBOX", "", "~/Pages/Inbox.aspx"));
                    //NavigationMenu.Items.Add(new MenuItem("", "", "~/Images/delete.gif"));
                    NavigationMenu.Items.Add(new MenuItem("NEW CHARGE", "NEWCHARGE", "", "~/Pages/AddOrEditNewChargeSlip.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("BILLED", "BILLED", "", "~/Pages/Billed.aspx"));
                    NavigationMenu.Items.Add(new MenuItem("REPORTS", "REPORTS", "", "~/Pages/Reports.aspx"));

                    Messages obj = new Messages();
                    DataSet ds = new DataSet();
                    Label lbl = new Label();
                    lbl.ID = "lblInbox";


                    Boolean isNewMessages = false;
                    ds = obj.GetMessagesUnRead(Convert.ToInt64(Session[GlobleData.User].ToString())); 
                    if (ds != null)
                    {
                        if (ds.Tables[0] != null)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                isNewMessages = true;
                            }
                        }
                    }

                    if (isNewMessages)
                    {
                        if (Request.Url.AbsoluteUri.ToLower().Contains("inbox"))
                        {
                            lbl.Text = "<img src='../Images/InboxWhite3.png' height='14px' width='14px' valign='top' border='0px'/>";
                            //lbl.Text = "!";
                            lbl.ForeColor = System.Drawing.Color.Red;
                            lbl.CssClass = "Notification";
                            //ImageButton btn = new ImageButton();"!";
                            //btn.ID = "btnNotification";
                            //btn.ImageUrl = "../Images/dollar-icon.jpg";
                            NavigationMenu.Items[2].Text = "INBOX " + lbl.Text;
                        }
                        else
                        {
                            lbl.Text = "<img src='../Images/InboxOrangeNotification.png' height='14px' width='14px' border='0px' />";
                            //lbl.Text = "!";
                            lbl.ForeColor = System.Drawing.Color.Red;
                            lbl.CssClass = "Notification";
                            //ImageButton btn = new ImageButton();"!";
                            //btn.ID = "btnNotification";
                            //btn.ImageUrl = "../Images/dollar-icon.jpg";
                            NavigationMenu.Items[2].Text = "INBOX " + lbl.Text;
                        }
                    }
                     
                    foreach (MenuItem item in NavigationMenu.Items)
                    {
                        string itemurl = item.NavigateUrl.ToLower();
                        int index = itemurl.LastIndexOf('/');
                        string urlpage = itemurl.Substring(index + 1);
                        if (Request.Url.AbsoluteUri.ToLower().Contains(urlpage))
                        {
                            item.Selected = true;
                            break;
                        }

                    }

                    lbUser.Text = Session["UserName"].ToString();
                    if (Request.Url.AbsoluteUri.ToLower().Contains("pages/newcharge.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("pages/newchargeleaseterms.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("pages/newchargecommissionspitcalculator.aspx") || Request.Url.AbsoluteUri.ToLower().Contains("pages/newchargeslipreview.aspx"))
                    {
                        NavigationMenu.Items[3].Selected = true;
                    }
                }
            }
            else
            {   // SK : 08/12/2013
                //    Session.Clear();
                //    Session.RemoveAll();
                //    Response.Redirect("~/Login.aspx");
                Utility.CheckSession();


            }

            //   SetButtonList();
        }
        else
        {
            Utility.CheckSession();
            // DateTime Now = new DateTime();
            // DateTime DTLogin = Convert.ToDateTime(hdnSessionTime.Value);
            // ErrorLog.WriteLog("Utility.cs", "CheckSession", Now.Subtract(DTLogin).TotalMinutes.ToString());
        }
    }
   
    #endregion

    #region Button Events
    protected void btnLogOut_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Session.RemoveAll();
        Response.Redirect("~/Login.aspx",false);
    }
    #endregion

    #region Custom Actions
    //private void SetButtonList()
    //{
    //    switch (Convert.ToInt32(NavigationMenu.SelectedValue))
    //    {
    //        case 1:
    //            Button btnNext = new Button();
    //            btnNext.ID = "btnNext";
    //            btnNext.Text = "Next";
    //            tdButtonList.Controls.Add(btnNext);
    //            break;
    //        case 2:
    //            break;
    //        case 3:
    //            break;
    //        case 4:
    //            break;
    //        case 5:
    //            Button btnSave = new Button();
    //            btnSave.ID = "btnSave";
    //            btnSave.Text = "Save";
    //            tdButtonList.Controls.Add(btnSave);
    //            break;
    //    }
    //}
    #endregion
    //protected void NavigationMenu_MenuItemClick(object sender, MenuEventArgs e)
    //{
    // HiddenField hdn = new HiddenField();
    //// hdn.ID = "hdnComplete";
    // string selecteditem = e.Item.Value;
    // if (selecteditem == "SNAPSHOT")
    // {
    //     Panel panel = (Panel)Page.Master.FindControl("pnContainMain");
    //     ContentPlaceHolder ph = (ContentPlaceHolder)panel.FindControl("ContentMain");
    //     UpdatePanel up = (UpdatePanel)ph.FindControl("upNewCharge"); 

    // Label cotentlbl = (Label)ContentMain.FindControl("txtSearchLocShoppingCenter");
    //  UpdatePanel contenttxt = (UpdatePanel)this.Master.FindControl("ContentMain").FindControl("upNewCharge");
    //   if (contenttxt != null)
    //  {
    //if (contenttxt.Text != "")
    //{
    //    Response.Redirect("~/Pages/Snapshot.aspx");
    //}
    // }

    //  }
    // }
}