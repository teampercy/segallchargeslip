﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeFile="Page20.aspx.cs" Inherits="TEST_Page20" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">



<head id="Head1" runat="server">
    <title>jQuery UI Dialog - Default functionality</title>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css" />
     <link href="../Styles/Master.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <style type="text/css">

    .ui-widget-header {
    background-image:none;
    background-color:gray;
    }
</style>
    <script>
        $(function () {
            // $("div").hasClass("ui-dialog-titlebar").each(function () { $(this).hide();});

            $("#dialogAddExternalBroker").dialog({
                autoOpen: false,
                width: 550,
                modal: true,
                show: {
                    duration: 300
                },
                hide: {
                    duration: 300
                }
            });
        });


        function btnAddExternalBroker_Click() {
            $("#dialogAddExternalBroker").dialog("open");

        }
        function btnAddExternalBroker_Save() {

            //debugger;
            var brokers = $('[id^=txtBrokers').val();
            var sharePercentage = $('[id^=txtSharePercentage').val();
            var commisionAmount = $('[id^=txtCommisionAmount').val();

            appendExternalBrokerFromPopup(brokers, sharePercentage, commisionAmount);

            clearAddExternalBroker();

            $("#dialogAddExternalBroker").dialog("close");
        }

        function clearAddExternalBroker() {
            $('[id^=txtBrokers').val("");
            $('[id^=txtSharePercentage').val("");
            $('[id^=txtCommisionAmount').val("");
            $('[id^=txtAddress').val("");

        }
        function appendExternalBrokerFromPopup(brokers, sharePercentage, commisionAmount) {
            var table = document.getElementById('tblExternalBroker');
            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            //brokers 
            var cell0 = row.insertCell(0);
            var cell0_element0 = document.createTextNode(brokers);
            cell0.appendChild(cell0_element0);

            //sharePercentage
            var cell1 = row.insertCell(1);
            var cell1_element0 = document.createTextNode(sharePercentage);
            cell1.appendChild(cell1_element0);

            //commisionAmount
            var cell2 = row.insertCell(2);
            var cell2_element0 = document.createTextNode(commisionAmount);
            cell2.appendChild(cell2_element0);
        }
    </script>
</head>
<body>
    <form id="form2" runat="server">
        <div id="dialogAddExternalBroker" title="Add External Broker" class="PopupDivBodyStyle">
            <table style="width: 100%">

                <tr>
                    <td>Company</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtBrokers" />
                    </td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtAddress" />
                    </td>
                </tr>

                <tr>
                    <td>City</td>
                    <td></td>
                </tr>

                <tr>
                    <td>State</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Zip Code</td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>Share</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtSharePercentage" /></td>
                </tr>
                <tr>
                    <td>Set Amount</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtCommisionAmount" />
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="2">

                        <button id="btnAddExternalBrokerSave" onclick="btnAddExternalBroker_Save();" style="width: 150px; text-align: center;">OK</button>
                    </td>
                </tr>
            </table>

        </div>


        <button id="btnAddExternalBroker" onclick="btnAddExternalBroker_Click();return false;">Add Broker</button>

        <table style="width: 660px; border:1px;">
            <tr>
                <td style="width: 15%; vertical-align: top;"></td>
                <td>
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 100%;">
                                <table style="width: 100%;" id="tblExternalBroker">
                                    <thead>
                                        <tr>
                                            <td>Brokers</td>
                                            <td>Percentage</td>
                                            <td align="right">Commission</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>


</html>
