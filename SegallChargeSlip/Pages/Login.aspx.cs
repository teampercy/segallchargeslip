﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SegallChargeSlipBll;


public partial class Pages_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {

            Users user = new Users();
            user = user.UserAunthentication(txtUser.Text, txtPassword.Text);
            if (user != null)
            {
                if (user.UserID != null && user.UserID > 0)
                {
                    Session["UserID"] = user.UserID;
                    Session["User"] = user.Username;
                    Session["UserType"] = user.UserType;

                    Session["UserName"] = user.FirstName + ' ' + user.LastName;

                    if (user.UserType == 1)
                    {
                        Response.Redirect("~/Admin/Payments.aspx", false);

                    }
                    else if (user.UserType == 2)
                    {
                        Response.Redirect("~/Pages/AddOrEditNewChargeSlip.aspx", false);
                    }
                }
                else
                    LoginFailMsg();
            }
            else
                LoginFailMsg();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Pages_Login.aspx", "btnLogin_Click", ex.Message);
        }
    }

    #region CustomActions

    private void LoginFailMsg()
    {
        lblMessage.Text = "Plz enter correct username and password.";
        lblMessage.ForeColor = System.Drawing.Color.Red;
    }
    #endregion
    protected void abc_Click(object sender, EventArgs e)
    {
        // Load a Excel document.
        //String inputFilePath = Program.RootPath + "\\" + "1.xlsx";
       
        //XLSXDocument doc = new XLSXDocument(inputFilePath);

        //// Convert and output to a PDF file.
        //String outputFilePath = inputFilePath + ".pdf";
        //doc.ConvertToDocument(DocumentType.PDF, outputFilePath);
    }
}