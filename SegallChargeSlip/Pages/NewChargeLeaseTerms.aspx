﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true" EnableViewState="true" CodeFile="NewChargeLeaseTerms.aspx.cs" Inherits="Pages_NewChargeLeaseTerms" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>--%>
<%@ Register Src="~/UserControl/ucBaseLeaseAndOptionTerms.ascx" TagName="BaseLeaseAndOptionTerms" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucLeaseTermSummary.ascx" TagName="LeaseTermSummery" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucUploadAttacment.ascx" TagName="Attachments" TagPrefix="uc" %>


<asp:Content ID="contHeadCont" ContentPlaceHolderID="cntHeadTag" runat="server">

    <script src="../Js/CommonValidations.js"></script>
    <%--  <link href="../Styles/SiteTEST.css" rel="stylesheet" />
   <script src="../Js/jquery-1.9.1.js"></script>--%>
    <script src="../Js/AutoNumeric.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            // Set Height For Main Panel  dvMainAttach dvMainSumm
            var ht = $(window).height();
            var decht = (10 / 100);
            $("#SetHeightHereLeaseTerm").height(ht - 240);
            $("#SetHeightHereLeaseTerm_Summ").height(ht - 240);
            var setHeight = Math.round(((ht - 390) / 2));
            //   alert(setHeight);
            $("#dvMainAttach").height(setHeight - (setHeight * decht));
            $("#dvMainSumm").height(setHeight);
            // alert($("#dvMainAttach").height());

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SetRent);
            SetRent();
            function SetRent() {
                $("#ContentMain_ucBaseLeaseTerms_txtRentPSP").change(function () {
                    var rslt = parseFloat($("#ContentMain_ucBaseLeaseTerms_hdnSizeSF").val()) * $("#ContentMain_ucBaseLeaseTerms_txtRentPSP").val();
                    $("#ContentMain_ucBaseLeaseTerms_txtAnnualRent").val(rslt);
                    return false;
                });

                $("#ContentMain_ucBaseLeaseTerms_txtAnnualRent").change(function () {
                    var rslt = (($("#ContentMain_ucBaseLeaseTerms_txtAnnualRent").val()) / parseFloat($("#ContentMain_ucBaseLeaseTerms_hdnSizeSF").val()));
                    $("#ContentMain_ucBaseLeaseTerms_txtRentPSP").val(rslt.toFixed(2));
                    return false;
                });
            }



            $('body').on('keydown', 'input, select, textarea', function (e) {
                var self = $(this)
                  , form = self.parents('form:eq(0)')
                  , focusable
                  , next
                ;
                if (e.keyCode == 13) {
                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {
                        form.submit();
                    }
                    return false;
                }
            });
        });

        //Option Term
        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SetRent);
            SetRent();
            function SetRent() {
                $("#ContentMain_ucOptionalTerms_txtRentPSP").change(function () {
                    var rslt = parseFloat($("#ContentMain_ucOptionalTerms_hdnSizeSF").val()) * $("#ContentMain_ucOptionalTerms_txtRentPSP").val();
                    $("#ContentMain_ucOptionalTerms_txtAnnualRent").val(rslt);
                    return false;
                });

                $("#ContentMain_ucOptionalTerms_txtAnnualRent").change(function () {
                    var rslt = (($("#ContentMain_ucOptionalTerms_txtAnnualRent").val()) / parseFloat($("#ContentMain_ucOptionalTerms_hdnSizeSF").val()));
                    $("#ContentMain_ucOptionalTerms_txtRentPSP").val(rslt.toFixed(2));
                    return false;
                });
            }
        });

        //Option Term 1
        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SetRent);
            SetRent();
            function SetRent() {
                $("#ContentMain_ucOptional1Terms_txtRentPSP").change(function () {
                    var rslt = parseFloat($("#ContentMain_ucOptional1Terms_hdnSizeSF").val()) * $("#ContentMain_ucOptional1Terms_txtRentPSP").val();
                    $("#ContentMain_ucOptional1Terms_txtAnnualRent").val(rslt);
                    return false;
                });

                $("#ContentMain_ucOptional1Terms_txtAnnualRent").change(function () {
                    var rslt = (($("#ContentMain_ucOptional1Terms_txtAnnualRent").val()) / parseFloat($("#ContentMain_ucOptional1Terms_hdnSizeSF").val()));
                    $("#ContentMain_ucOptional1Terms_txtRentPSP").val(rslt.toFixed(2));
                    return false;
                });
            }
        });

        //Option Term 2
        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SetRent);
            SetRent();
            function SetRent() {
                $("#ContentMain_ucOptional2Terms_txtRentPSP").change(function () {
                    var rslt = parseFloat($("#ContentMain_ucOptional2Terms_hdnSizeSF").val()) * $("#ContentMain_ucOptional2Terms_txtRentPSP").val();
                    $("#ContentMain_ucOptional2Terms_txtAnnualRent").val(rslt);
                    return false;
                });

                $("#ContentMain_ucOptional2Terms_txtAnnualRent").change(function () {
                    var rslt = (($("#ContentMain_ucOptional2Terms_txtAnnualRent").val()) / parseFloat($("#ContentMain_ucOptional2Terms_hdnSizeSF").val()));
                    $("#ContentMain_ucOptional2Terms_txtRentPSP").val(rslt.toFixed(2));
                    return false;
                });
            }
        });

        //Option Term 3
        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SetRent);
            SetRent();
            function SetRent() {
                $("#ContentMain_ucOptional3Terms_txtRentPSP").change(function () {
                    var rslt = parseFloat($("#ContentMain_ucOptional3Terms_hdnSizeSF").val()) * $("#ContentMain_ucOptional3Terms_txtRentPSP").val();
                    $("#ContentMain_ucOptional3Terms_txtAnnualRent").val(rslt);
                    return false;
                });

                $("#ContentMain_ucOptional3Terms_txtAnnualRent").change(function () {
                    var rslt = (($("#ContentMain_ucOptional3Terms_txtAnnualRent").val()) / parseFloat($("#ContentMain_ucOptional3Terms_hdnSizeSF").val()));
                    $("#ContentMain_ucOptional3Terms_txtRentPSP").val(rslt.toFixed(2));
                    return false;
                });
            }
        });

        //Contingent Term
        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SetRent);
            SetRent();
            function SetRent() {
                $("#ContentMain_ucContingentCommission_txtRentPSP").change(function () {
                    var rslt = parseFloat($("#ContentMain_ucContingentCommission_hdnSizeSF").val()) * $("#ContentMain_ucContingentCommission_txtRentPSP").val();
                    $("#ContentMain_ucContingentCommission_txtAnnualRent").val(rslt);
                    return false;
                });

                $("#ContentMain_ucContingentCommission_txtAnnualRent").change(function () {
                    var rslt = (($("#ContentMain_ucContingentCommission_txtAnnualRent").val()) / parseFloat($("#ContentMain_ucContingentCommission_hdnSizeSF").val()));
                    $("#ContentMain_upContingentComm_txtRentPSP").val(rslt.toFixed(2));
                    return false;
                });
            }
        });

        $(document).ready(function () {

            var SelChk = document.getElementById("<%=chkOptionTermPayable.ClientID%>");
            if (SelChk.checked) {
                $("#ContentMain_ucSummery_optnPayableSumm").val('YES');
            }
            else {
                $("#ContentMain_ucSummery_optnPayableSumm").val('');
            }

            // EnableDisableOnCheckChange();
            //ValidateOnNext();
            // alert('hjh');
        });


        $(document).ready(function () {

            var inputs = $(':input').keypress(function (e) {
                if (e.which == 13) {
                    //alert("enter pressed");
                    e.preventDefault();
                    var nextInput = inputs.get(inputs.index(this) + 1);
                    if (nextInput) {
                        nextInput.focus();
                    }
                }
            });

        });

        //$("input").bind("keydown", function (event) {
        //    if (event.which === 13) {
        //        alert("enter pressed");
        //        event.stopPropagation();
        //        event.preventDefault();
        //        $(this).next("input").focus();
        //    }
        //});

        window.onload = function () {
            EnableDisableOnCheckChange();
        };



        function EnableDisableOnCheckChange() {
            debugger;

            document.getElementById("<%=trOption1.ClientID%>").style.visibility = 'hidden';           
            document.getElementById("<%=trOption2.ClientID%>").style.visibility = 'hidden';            
            document.getElementById("<%=trOption3.ClientID%>").style.visibility = 'hidden';            

            var SelChkOpt = document.getElementById("<%=chkOptionTerm.ClientID%>");
            var SelChkOptPayable = document.getElementById("<%=chkOptionTermPayable.ClientID%>");
            var SelChkContingent = document.getElementById("<%=chkContingentComm.ClientID%>");

            var SelChkOpt1 = document.getElementById("<%=chkOptionTerm1.ClientID%>");
            var SelChkOptPayable1 = document.getElementById("<%=chkOptionTermPayable1.ClientID%>");

            var SelChkOpt2 = document.getElementById("<%=chkOptionTerm2.ClientID%>");
            var SelChkOptPayable2 = document.getElementById("<%=chkOptionTermPayable2.ClientID%>");

            var SelChkOpt3 = document.getElementById("<%=chkOptionTerm3.ClientID%>");
            var SelChkOptPayable3 = document.getElementById("<%=chkOptionTermPayable3.ClientID%>");

            //ContentMain_ucContingentCommission_optPayable_2
            //ContentMain_ucBaseLeaseTerms_optPayable_3 ContentMain_chkOptionTerm

            //Option Term
            if (SelChkOpt.checked) {
                document.getElementById("<%=dvOptionTerms.ClientID %>").style.display = 'block';
                document.getElementById("<%=trOption1.ClientID%>").style.visibility = 'visible';                
            }
            else {
                document.getElementById("<%=dvOptionTerms.ClientID %>").style.display = 'none';
            }

            //Option Term 1
            if (SelChkOpt.checked && SelChkOpt1.checked) {
                document.getElementById("<%=trOption2.ClientID%>").style.visibility = 'visible';
                document.getElementById("<%=dvOptionTerms1.ClientID%>").style.display = 'block';
            }
            else
            {
                document.getElementById("<%=dvOptionTerms1.ClientID%>").style.display = 'none';
            }

            //Option Term 2
            if (SelChkOpt.checked && SelChkOpt1.checked && SelChkOpt2.checked) {
                document.getElementById("<%=trOption3.ClientID%>").style.visibility = 'visible';
                document.getElementById("<%=dvOptionTerms2.ClientID%>").style.display = 'block';
            }
            else
            {
                document.getElementById("<%=dvOptionTerms2.ClientID%>").style.display = 'none';
            }

            //Option Term 3
            if (SelChkOpt.checked && SelChkOpt1.checked && SelChkOpt2.checked && SelChkOpt3.checked) {                
                document.getElementById("<%=dvOptionTerms3.ClientID%>").style.display = 'block';
            }
            else
            {
                document.getElementById("<%=dvOptionTerms3.ClientID%>").style.display = 'none';
            }

            //Contingent Commission
            if (SelChkContingent.checked) {
                document.getElementById("<%=divContingentComm.ClientID %>").style.display = 'block';
            }
            else {
                document.getElementById("<%=divContingentComm.ClientID %>").style.display = 'none';
            }

            //to unchecked child Option Payable checkbox while parent Option Payable checkbox is unchecked
            if (!SelChkOptPayable.checked)
            {                
                $('#ContentMain_chkOptionTermPayable1').prop('checked', false);
                $('#ContentMain_chkOptionTermPayable2').prop('checked', false);
                $('#ContentMain_chkOptionTermPayable3').prop('checked', false);
            }
            else if (!SelChkOptPayable1.checked)
            {
                $('#ContentMain_chkOptionTermPayable2').prop('checked', false);
                $('#ContentMain_chkOptionTermPayable3').prop('checked', false);
            }
            else if (!SelChkOptPayable2.checked)
            {
                $('#ContentMain_chkOptionTermPayable3').prop('checked', false);
            } 

            //Option Term Payable
            if (SelChkOptPayable.checked && SelChkOpt.checked) {

                //optPayable 
                $("#ContentMain_ucOptionalTerms_optPayable").show();
                $("#ContentMain_ucOptionalTerms_optPayable_1").show();
                $("#ContentMain_ucOptionalTerms_optPayable_2").show();
                $("#ContentMain_ucOptionalTerms_optPayable_3").show();
                $("#ContentMain_ucOptionalTerms_optPayable_4").show();
                $("#ContentMain_ucOptionalTerms_optPayable_5").show();
                $("#ContentMain_ucOptionalTerms_optPayable_6").show();
            }
            else {
                $("#ContentMain_ucOptionalTerms_optPayable").hide();
                $("#ContentMain_ucOptionalTerms_optPayable_1").hide();
                $("#ContentMain_ucOptionalTerms_optPayable_2").hide();
                $("#ContentMain_ucOptionalTerms_optPayable_3").hide();
                $("#ContentMain_ucOptionalTerms_optPayable_4").hide();
                $("#ContentMain_ucOptionalTerms_optPayable_5").hide();
                $("#ContentMain_ucOptionalTerms_optPayable_6").hide();
            }

            //Option Term Payable 1
            if (SelChkOptPayable1.checked && SelChkOpt1.checked && SelChkOptPayable.checked) {

                //optPayable 
                $("#ContentMain_ucOptional1Terms_optPayable").show();
                $("#ContentMain_ucOptional1Terms_optPayable_1").show();
                $("#ContentMain_ucOptional1Terms_optPayable_2").show();
                $("#ContentMain_ucOptional1Terms_optPayable_3").show();
                $("#ContentMain_ucOptional1Terms_optPayable_4").show();
                $("#ContentMain_ucOptional1Terms_optPayable_5").show();
                $("#ContentMain_ucOptional1Terms_optPayable_6").show();
            }
            else {
                $("#ContentMain_ucOptional1Terms_optPayable").hide();
                $("#ContentMain_ucOptional1Terms_optPayable_1").hide();
                $("#ContentMain_ucOptional1Terms_optPayable_2").hide();
                $("#ContentMain_ucOptional1Terms_optPayable_3").hide();
                $("#ContentMain_ucOptional1Terms_optPayable_4").hide();
                $("#ContentMain_ucOptional1Terms_optPayable_5").hide();
                $("#ContentMain_ucOptional1Terms_optPayable_6").hide();
            }

            //Option Term Payable 2
            if (SelChkOptPayable2.checked && SelChkOpt2.checked && SelChkOptPayable1.checked) {

                //optPayable 
                $("#ContentMain_ucOptional2Terms_optPayable").show();
                $("#ContentMain_ucOptional2Terms_optPayable_1").show();
                $("#ContentMain_ucOptional2Terms_optPayable_2").show();
                $("#ContentMain_ucOptional2Terms_optPayable_3").show();
                $("#ContentMain_ucOptional2Terms_optPayable_4").show();
                $("#ContentMain_ucOptional2Terms_optPayable_5").show();
                $("#ContentMain_ucOptional2Terms_optPayable_6").show();
            }
            else {
                $("#ContentMain_ucOptional2Terms_optPayable").hide();
                $("#ContentMain_ucOptional2Terms_optPayable_1").hide();
                $("#ContentMain_ucOptional2Terms_optPayable_2").hide();
                $("#ContentMain_ucOptional2Terms_optPayable_3").hide();
                $("#ContentMain_ucOptional2Terms_optPayable_4").hide();
                $("#ContentMain_ucOptional2Terms_optPayable_5").hide();
                $("#ContentMain_ucOptional2Terms_optPayable_6").hide();
            }

            //Option Term Payable 3
            if (SelChkOptPayable3.checked && SelChkOpt3.checked && SelChkOptPayable2.checked) {

                //optPayable 
                $("#ContentMain_ucOptional3Terms_optPayable").show();
                $("#ContentMain_ucOptional3Terms_optPayable_1").show();
                $("#ContentMain_ucOptional3Terms_optPayable_2").show();
                $("#ContentMain_ucOptional3Terms_optPayable_3").show();
                $("#ContentMain_ucOptional3Terms_optPayable_4").show();
                $("#ContentMain_ucOptional3Terms_optPayable_5").show();
                $("#ContentMain_ucOptional3Terms_optPayable_6").show();
            }
            else {
                $("#ContentMain_ucOptional3Terms_optPayable").hide();
                $("#ContentMain_ucOptional3Terms_optPayable_1").hide();
                $("#ContentMain_ucOptional3Terms_optPayable_2").hide();
                $("#ContentMain_ucOptional3Terms_optPayable_3").hide();
                $("#ContentMain_ucOptional3Terms_optPayable_4").hide();
                $("#ContentMain_ucOptional3Terms_optPayable_5").hide();
                $("#ContentMain_ucOptional3Terms_optPayable_6").hide();
            }
            return false;
        }


        function SetSaveStatus(SaveStatus) {
            debugger;
            var rslt = true;
            // marker : 12/06/2013 : Added  SaveStatus == "B" to save data on Back request
            if (SaveStatus == "N" || SaveStatus == "B") {
                var ParentBase = "ContentMain_ucBaseLeaseTerms";
                var GV = ParentBase + "_gvBaseCommTerms";
                var GVTerm = ParentBase + "_gvBaseLeaseTerm";
                var opTableStr = ParentBase + "_tbDueDates";
                var txtTotMonth = ParentBase + "_txtLeaseTermMonths";
                rslt = CheckDuePercentage(GV, opTableStr, "Base Commissionable Term", GVTerm, txtTotMonth);

                //Option Term
                if (rslt == true) {
                    var ParentOpt = "ContentMain_ucOptionalTerms";
                    var GV = ParentOpt + "_gvBaseCommTerms";
                    var opTableStr = ParentOpt + "_tbDueDates";
                    var GVTerm = ParentOpt + "_gvBaseLeaseTerm";
                    var txtTotMonth = ParentOpt + "_txtLeaseTermMonths";
                    rslt = CheckDuePercentage(GV, opTableStr, "Optional Commissionable Term 1", GVTerm, txtTotMonth);
                }

                //Contingent Term
                if (rslt == true) {
                    var ParentCon = "ContentMain_ucContingentCommission";
                    var GV = ParentCon + "_gvBaseCommTerms";
                    var GVTerm = ParentCon + "_gvBaseLeaseTerm";
                    var opTableStr = ParentCon + "_tbDueDates";
                    var txtTotMonth = ParentCon + "_txtLeaseTermMonths";
                    rslt = CheckDuePercentage(GV, opTableStr, "Contingent Commissionable Term", GVTerm, txtTotMonth);
                }

                //Option Term 1
                if (rslt == true) {
                    var ParentOpt = "ContentMain_ucOptional1Terms";
                    var GV = ParentOpt + "_gvBaseCommTerms";
                    var opTableStr = ParentOpt + "_tbDueDates";
                    var GVTerm = ParentOpt + "_gvBaseLeaseTerm";
                    var txtTotMonth = ParentOpt + "_txtLeaseTermMonths";
                    rslt = CheckDuePercentage(GV, opTableStr, "Optional Commissionable Term 2", GVTerm, txtTotMonth);
                }

                //Option Term 2
                if (rslt == true) {
                    var ParentOpt = "ContentMain_ucOptional2Terms";
                    var GV = ParentOpt + "_gvBaseCommTerms";
                    var opTableStr = ParentOpt + "_tbDueDates";
                    var GVTerm = ParentOpt + "_gvBaseLeaseTerm";
                    var txtTotMonth = ParentOpt + "_txtLeaseTermMonths";
                    rslt = CheckDuePercentage(GV, opTableStr, "Optional Commissionable Term 3", GVTerm, txtTotMonth);
                }

                //Option Term 3
                if (rslt == true) {
                    var ParentOpt = "ContentMain_ucOptional3Terms";
                    var GV = ParentOpt + "_gvBaseCommTerms";
                    var opTableStr = ParentOpt + "_tbDueDates";
                    var GVTerm = ParentOpt + "_gvBaseLeaseTerm";
                    var txtTotMonth = ParentOpt + "_txtLeaseTermMonths";
                    rslt = CheckDuePercentage(GV, opTableStr, "Optional Commissionable Term 4", GVTerm, txtTotMonth);
                }
            }
            if (rslt == true) {
                document.getElementById('<%= hdnSaveStatus.ClientID %>').value = SaveStatus;
                return true;
            }
            else {
                return false;
            }
        }

        function CheckDuePercentage(GV, opTableStr, UcName, GVTerm, txtTotMonth) {
            debugger;
            var GidView = document.getElementById(GV);
            var opTable = document.getElementById(opTableStr);
            var TotLeseMonths = document.getElementById(txtTotMonth).value;
            var Parent = UcName.replace("Commissionable", "Lease");
            var rCount = 0;
            var CommNotSet;

            var GidView1 = document.getElementById(GVTerm);

            if (GidView1 != null) {
                debugger;
                var row = parseInt(GidView1.rows.length) - 2;
                var LastMonth = parseInt(GidView1.rows[row].cells[0].children[1].value);
                var LeaseTerm = TotLeseMonths == "" ? 0 : parseInt(TotLeseMonths);

                if (LastMonth != LeaseTerm) {
                    alert(Parent + " Months And Total Months For The Rent Periods Are Not Equal");
                    return false;
                }
            }

            if (GidView != null) {
                rCount = parseInt(GidView.rows.length) - 1;
                CommNotSet = false;
            }
            else {
                CommNotSet = true;
            }
            var Payable = true;
            //Option Term Payable
            if (UcName == 'Optional Commissionable Term 1') {
                var SelChkOptPayable = document.getElementById("<%=chkOptionTermPayable.ClientID%>");
                if (!SelChkOptPayable.checked) {
                    Payable = false;
                }
            }

            //Option Term Payable 1
            if (UcName == 'Optional Commissionable Term 2') {
                var SelChkOptPayable1 = document.getElementById("<%=chkOptionTermPayable1.ClientID%>");
                if (!SelChkOptPayable1.checked) {
                    Payable = false;
                }
            }

            //Option Term Payable 2
            if (UcName == 'Optional Commissionable Term 3') {
                var SelChkOptPayable2 = document.getElementById("<%=chkOptionTermPayable2.ClientID%>");
                if (!SelChkOptPayable2.checked) {
                    Payable = false;
                }
            }

            //Option Term Payable 3
            if (UcName == 'Optional Commissionable Term 4') {
                var SelChkOptPayable3 = document.getElementById("<%=chkOptionTermPayable3.ClientID%>");
                if (!SelChkOptPayable3.checked) {
                    Payable = false;
                }
            }

            if (rCount > 0 && CommNotSet == false && Payable == true) {
                var PerCommissionTot = 0, PerAmtTot = 0;
                for (var r = 0, n = opTable.rows.length; r < n; r++) {
                    if (opTable.rows[r].cells[1].childNodes.length > 0) {
                        PerCommissionTot += parseFloat(opTable.rows[r].cells[1].firstChild.value);
                        PerAmtTot += parseFloat(opTable.rows[r].cells[2].firstChild.value);
                    }
                }
                if (!(PerAmtTot > 0)) {
                    alert("Please Enter Proper Commission Amount For " + UcName)
                    return false;
                }
                if (PerCommissionTot < 100) {
                    alert("Total Commission Percentage For " + UcName + " is : " + PerCommissionTot + "  Commission Percentage Cannot Be Less Than 100 %");
                    return false;
                }
                else if (PerCommissionTot > 100) {
                    alert("Total Commission Percentage For " + UcName + " is : " + PerCommissionTot + "  Commission Percentage Cannot Be Greater Than 100 %");
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return true;
            }
        }

        function ValidateOnNext() {
            alert('Please Fill Lease Term Information');
            return false;
        }


        function CalAnnualRentDiffInPercentageOPT(parent) {
            debugger;

            //var GV = parent + "_gvBaseLeaseTerm";
            //var GidView = document.getElementById(GV);
            //var rCount = parseInt(GidView.rows.length) - 2;
            //var rowElement = GidView.rows[rCount];

            //var PrevAnnualRent = rowElement.cells[3].children[0].innerHTML;
            //alert($("#<%= hdnleaseLast.ClientID %>").val());
            var PrevAnnualRent = $("#<%= hdnleaseLast.ClientID %>").val();

            var CurrentAnnualRent = $("#" + parent + "_txtAnnualRent").val();

            if (PrevAnnualRent != "" && CurrentAnnualRent != "") {
                var PrevRent = parseFloat(replaceAll(",", "", PrevAnnualRent));
                var CurRent = parseFloat(CurrentAnnualRent);
                var RentDiff = (((CurRent - PrevRent) / (PrevRent)) * 100).toFixed(2);
                $("#" + parent + "_txtPerIncrese").val(RentDiff);
            }
        }


        function CalAnnualRentDiffFromPercentageOPT(parent) {
            debugger;
            var PrevAnnualRent = $("#<%= hdnleaseLast.ClientID %>").val();
            var CurPer = $("#" + parent + "_txtPerIncrese").val();

            var CurrentAnnualRent = $("#" + parent + "_txtAnnualRent").val();
            if (PrevAnnualRent != "" && CurPer != "") {
                var PrevRent = parseFloat(replaceAll(",", "", PrevAnnualRent));
                var CurPer = parseFloat(CurPer);
                var CurRent = (((CurPer / 100) * PrevRent) + PrevRent).toFixed(2);
                $("#" + parent + "_txtAnnualRent").val(CurRent);
                CalculateRent(parent, 2);
            }
        }

    </script>

    <style type="text/css">
        .borderspace {
            width: 5px;
        }

        .leftCont {
            width: 68%;
        }
    </style>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <table style="width: 100%">
        <tr>
            <td class="mainPageSubTitle">LEASE TERMS TABLES
            </td>
            <td class="mainPageTitle">NEW CHARGE SLIP
            </td>
        </tr>
    </table>
    <hr style="width: 100%" />
    <style>
        .Htadjust {
            height: auto !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <%-- <link href="../Styles/SiteTEST.css" rel="stylesheet" />--%>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <table style="width: 100%; overflow: scroll;">
        <tr>
            <td class="leftCont" valign="top" id="SetHeightHereLeaseTerm">
                <asp:Panel ScrollBars="Vertical" ID="pnContainMain" runat="server" class="leftCont" Width="100%" Height="98%">
                    <table style="width: 100%;">

                        <tr>
                            <td>
                                <div id="dvBaseLease" runat="server" style="height: 100%">
                                    <asp:UpdatePanel ID="upBaseLeaseTerms" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <uc:BaseLeaseAndOptionTerms ID="ucBaseLeaseTerms" runat="server" />
                                            <asp:HiddenField ID="hdnleaseLast" runat="server" Value="100" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </td>
                        </tr>
                        <tr style="background-color: #F68620;">
                            <td>
                                <table>
                                    <tr>
                                        <td style="width: 25%">
                                            <asp:CheckBox ID="chkOptionTerm" runat="server" Text="OPTION(S) TERM 1?" onclick="javascript:EnableDisableOnCheckChange();" />
                                            <asp:HiddenField ID="hdnCommissionTypeBaseTerm" runat="server" />
                                            <asp:HiddenField ID="hdnCommissionTypeOptionTerm" runat="server" />
                                            <asp:HiddenField ID="hdnCommissionTypeContingent" runat="server" />
                                            
                                            <asp:HiddenField ID="hdnCommissionTypeOptionTerm1" runat="server" />
                                            <asp:HiddenField ID="hdnCommissionTypeOptionTerm2" runat="server" />
                                            <asp:HiddenField ID="hdnCommissionTypeOptionTerm3" runat="server" />
                                        </td>
                                        <td style="width: 30%">
                                            <asp:CheckBox ID="chkOptionTermPayable" runat="server" Text="OPTION(S) TERM PAYABLE 1?" onclick="javascript:EnableDisableOnCheckChange();" />
                                        </td>
                                        <td style="width: 30%">
                                            <asp:CheckBox ID="chkContingentComm" runat="server" Text="CONTINGENT COMMISSION?" onclick="javascript:EnableDisableOnCheckChange();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>                        
                        <tr>
                            <td>
                                <div id="dvOptionTerms" runat="server">
                                    <asp:UpdatePanel ID="upOptionalTerms" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <uc:BaseLeaseAndOptionTerms ID="ucOptionalTerms" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="divContingentComm" runat="server">
                                    <asp:UpdatePanel ID="upContingentComm" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <uc:BaseLeaseAndOptionTerms ID="ucContingentCommission" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </td>
                        </tr>
                        <tr style="background-color: #F68620;" id="trOption1" runat="server" >
                            <td>
                                <table>
                                    <tr>
                                        <td style="width: 12%">
                                            <asp:CheckBox ID="chkOptionTerm1" runat="server" Text="OPTION(S) TERM 2?" onclick="javascript:EnableDisableOnCheckChange();" />
                                        </td>
                                        <td style="width: 30%">
                                            <asp:CheckBox ID="chkOptionTermPayable1" runat="server" Text="OPTION(S) TERM PAYABLE 2?" onclick="javascript:EnableDisableOnCheckChange();" />
                                        </td>                                        
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="dvOptionTerms1" runat="server">
                                    <asp:UpdatePanel ID="upOptionalTerms1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <uc:BaseLeaseAndOptionTerms ID="ucOptional1Terms" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </td>
                        </tr>
                        <tr style="background-color: #F68620;" id="trOption2" runat="server">
                            <td>
                                <table>
                                    <tr>
                                        <td style="width: 12%">
                                            <asp:CheckBox ID="chkOptionTerm2" runat="server" Text="OPTION(S) TERM 3?" onclick="javascript:EnableDisableOnCheckChange();" />
                                        </td>
                                        <td style="width: 30%">
                                            <asp:CheckBox ID="chkOptionTermPayable2" runat="server" Text="OPTION(S) TERM PAYABLE 3?" onclick="javascript:EnableDisableOnCheckChange();" />
                                        </td>                                        
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="dvOptionTerms2" runat="server">
                                    <asp:UpdatePanel ID="upOptionalTerms2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <uc:BaseLeaseAndOptionTerms ID="ucOptional2Terms" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </td>
                        </tr>
                        <tr style="background-color: #F68620;" id="trOption3" runat="server">
                            <td>
                                <table>
                                    <tr>
                                        <td style="width: 12%">
                                            <asp:CheckBox ID="chkOptionTerm3" runat="server" Text="OPTION(S) TERM 4?" onclick="javascript:EnableDisableOnCheckChange();" />
                                        </td>
                                        <td style="width: 30%">
                                            <asp:CheckBox ID="chkOptionTermPayable3" runat="server" Text="OPTION(S) TERM PAYABLE 4?" onclick="javascript:EnableDisableOnCheckChange();" />
                                        </td>                                        
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="dvOptionTerms3" runat="server">
                                    <asp:UpdatePanel ID="upOptionalTerms3" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <uc:BaseLeaseAndOptionTerms ID="ucOptional3Terms" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td style="vertical-align: top; width: 30%" id="SetHeightHereLeaseTerm_Summ">

                <table style="width: 100%;">
                    <tr>
                        <td class="borderspace"></td>
                        <td style="vertical-align: top;">
                            <asp:UpdatePanel ID="upBaseLeaseTerm" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <uc:LeaseTermSummery ID="ucSummery" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="borderspace"></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height: 10px"></td>
                    </tr>
                    <tr>
                        <td class="borderspace"></td>
                        <td>
                            <%-- <asp:UpdatePanel ID="upAttach" runat="server">
                                <ContentTemplate>--%>
                            <uc:Attachments ID="ucAttachment" runat="server" />
                            <%-- </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                        <td class="borderspace"></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height: 10px"></td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" runat="Server">
    <table style="width: 100%;">
        <tr>
            <td style="width: 60%; text-align: left;">
                <label class="whitelabel"><b>PAGE 2 OF 4</b></label>
                <asp:Label ID="lbModificationHistory" runat="server" Text=""></asp:Label>
            </td>
            <td style="width: 40%; text-align: right;">
                <asp:Button ID="btnSaveForLater" runat="server" Text="SAVE FOR LATER" OnClick="btnNext_Click" BackColor="#808080" CssClass="SqureButton" OnClientClick="SetSaveStatus('D');" />
                &nbsp; 
                <asp:Button ID="btnBack" runat="server" Text="BACK" OnClick="btnNext_Click" CssClass="btnSmallOrange" OnClientClick="return SetSaveStatus('B');" />
                <%--btnBack_Click  // marker : 12/06/2013 : Added  SaveStatus == "B" to save data on Back request--%>
                &nbsp;  
                <asp:Button ID="btnNext" runat="server" Text="NEXT" CssClass="btnSmallOrange" OnClick="btnNext_Click" OnClientClick="return SetSaveStatus('N');" />
                <asp:HiddenField ID="hdnSaveStatus" runat="server" Value="" />
            </td>
        </tr>
    </table>
</asp:Content>

