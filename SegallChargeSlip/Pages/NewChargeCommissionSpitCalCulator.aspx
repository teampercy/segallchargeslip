﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true"
    CodeFile="NewChargeCommissionSpitCalCulator.aspx.cs" Inherits="Pages_NewChargeCommissionSpitCalCulator" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>--%>
<%@ Register Src="~/UserControl/ucCommissionSplitCalculator.ascx" TagName="CommissionSplitCalculator" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucCompanySplitSummary.ascx" TagName="CompanySplitSummary" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucCommissionSplitCalculatorSummary.ascx" TagName="Summary" TagPrefix="uc" %>


<asp:Content ID="contHeadCont" ContentPlaceHolderID="cntHeadTag" runat="server">

    <%--<link href="../Styles/SiteTEST.css" rel="stylesheet" />

    <link href="../Styles/AutoCompleteStyle.css" rel="stylesheet" />--%>
    <%--  <link href="../Js/ jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../Js/ jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
    <link href="../Js/ jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />--%>
    <link href="../Js/jquery-ui.css" rel="stylesheet" />
    <script src="../Js/jquery-ui.js"></script>

    <%--  <script src="../Js/CommonValidations.js"></script>
  <script src="../Js/ChargeSlipValidation.js"></script>--%>

    <script type="text/ecmascript">

        function SetSaveStatus(SaveStatus) {
            debugger;
            // check for total per equal to 100--20/09/2013
            var base = $('[id^=ContentMain_hdnLeaseCommTot]').val() == '' ? 0 : parseFloat($('[id^=ContentMain_hdnLeaseCommTot]').val());
            var opt = $('[id^=ContentMain_hdnOptCommTot]').val() == '' ? 0 : parseFloat($('[id^=ContentMain_hdnOptCommTot]').val());
            var contingent = $('[id^=ContentMain_hdnContigentTot]').val() == '' ? 0 : parseFloat($('[id^=ContentMain_hdnContigentTot]').val());

            var opt1 = $('[id^=ContentMain_hdnOptCommTot1]').val() == '' ? 0 : parseFloat($('[id^=ContentMain_hdnOptCommTot1]').val());
            var opt2 = $('[id^=ContentMain_hdnOptCommTot2]').val() == '' ? 0 : parseFloat($('[id^=ContentMain_hdnOptCommTot2]').val());
            var opt3 = $('[id^=ContentMain_hdnOptCommTot3]').val() == '' ? 0 : parseFloat($('[id^=ContentMain_hdnOptCommTot3]').val());

            if (base > 0) {
                //this="#ContentMain_CommCalculator_td_tblBrokersPerTot";
                var ChkBase = $("#ContentMain_CommCalculator_td_tblBrokersPerTot").text() == '' ? 0 : parseFloat($("#ContentMain_CommCalculator_td_tblBrokersPerTot").text());
                //var ChkBase = $(this).text() == '' ? 0 : parseFloat($(this).text());
                if (ChkBase < 100) {
                    alert("Total Commission Percentage For Base Term Should Be 100%");
                    return false;
                }
            }

            if (opt > 0) {
                var ChkBase = $("#ContentMain_OptCommCalculator_td_tblBrokersPerTot").text() == '' ? 0 : parseFloat($("#ContentMain_OptCommCalculator_td_tblBrokersPerTot").text());
                if (ChkBase < 100) {
                    alert("Total Commission Percentage For Options Term 1 Should Be 100%");
                    return false;
                }
            }

            if (contingent > 0) {
                var ChkBase = $("#ContentMain_ContingentCommCalculator_td_tblBrokersPerTot").text() == '' ? 0 : parseFloat($("#ContentMain_ContingentCommCalculator_td_tblBrokersPerTot").text());
                if (ChkBase < 100) {
                    alert("Total Commission Percentage Contingent Term Should Be 100%");
                    return false;
                }
            }

            if (opt1 > 0) {
                var ChkBase = $("#ContentMain_Opt1CommCalculator_td_tblBrokersPerTot").text() == '' ? 0 : parseFloat($("#ContentMain_Opt1CommCalculator_td_tblBrokersPerTot").text());
                if (ChkBase < 100) {
                    alert("Total Commission Percentage For Options Term 2 Should Be 100%");
                    return false;
                }
            }

            if (opt2 > 0) {
                var ChkBase = $("#ContentMain_Opt2CommCalculator_td_tblBrokersPerTot").text() == '' ? 0 : parseFloat($("#ContentMain_Opt2CommCalculator_td_tblBrokersPerTot").text());
                if (ChkBase < 100) {
                    alert("Total Commission Percentage For Options Term 3 Should Be 100%");
                    return false;
                }
            }

            if (opt3 > 0) {
                var ChkBase = $("#ContentMain_Opt3CommCalculator_td_tblBrokersPerTot").text() == '' ? 0 : parseFloat($("#ContentMain_Opt3CommCalculator_td_tblBrokersPerTot").text());
                if (ChkBase < 100) {
                    alert("Total Commission Percentage For Options Term 4 Should Be 100%");
                    return false;
                }
            }


            document.getElementById('<%= hdnSaveStatus.ClientID %>').value = SaveStatus;
        }

        $(function () {
            $("#content").tabs();
        });

        $(document).ready(function () {
            
            var opt = $('[id^=ContentMain_hdnOptCommTot]').val();
            var contingent = $('[id^=ContentMain_hdnContigentTot]').val();

            var opt1 = $('[id^=ContentMain_hdnOptCommTot1]').val();
            var opt2 = $('[id^=ContentMain_hdnOptCommTot2]').val();
            var opt3 = $('[id^=ContentMain_hdnOptCommTot3]').val();

            //Old Code for 3 Term Types
            //if (opt == "0" || opt == "") {
            //    $("#content").tabs({ disabled: [1] });
            //    if (contingent == "0" || contingent == "") {
            //        $("#content").tabs({ disabled: [1, 2] });
            //    }
            //}
            //else if (contingent == "0" || contingent == "") {
            //    $("#content").tabs({ disabled: [2] });
            //}

            if (opt == "0" || opt == "") {                          //Option 1
                $("#content").tabs({ disabled: [1] });
                if (opt1 == "0" || opt1 == "") {
                    $("#content").tabs({ disabled: [1, 2] });
                    if (opt2 == "0" || opt2 == "") {
                        $("#content").tabs({ disabled: [1, 2, 3] });
                        if (opt3 == "0" || opt3 == "") {
                            $("#content").tabs({ disabled: [1, 2, 3, 4] });
                            if (contingent == "0" || contingent == "") {
                                $("#content").tabs({ disabled: [1, 2, 3, 4, 5] });
                            }
                        }
                        else if (contingent == "0" || contingent == "") {
                            $("#content").tabs({ disabled: [1, 2, 3, 5] });
                        }
                    }
                    else if (opt3 == "0" || opt3 == "") {
                        $("#content").tabs({ disabled: [1, 2, 4] });
                        if (contingent == "0" || contingent == "") {
                            $("#content").tabs({ disabled: [1, 2, 4, 5] });
                        }
                    }
                    else if (contingent == "0" || contingent == "") {
                        $("#content").tabs({ disabled: [1, 2, 5] });
                    }
                }
                else if(opt2 == "0" || opt2 == "") {
                    $("#content").tabs({ disabled: [1, 3] });
                    if (opt3 == "0" || opt3 == "") {
                        $("#content").tabs({ disabled: [1, 3, 4] });
                        if (contingent == "0" || contingent == "") {
                            $("#content").tabs({ disabled: [1, 3, 4, 5] });
                        }
                    }
                    else if (contingent == "0" || contingent == "") {
                        $("#content").tabs({ disabled: [1, 3, 5] });
                    }
                }
                else if (opt3 == "0" || opt3 == "") {
                    $("#content").tabs({ disabled: [1, 4] });
                    if (contingent == "0" || contingent == "") {
                        $("#content").tabs({ disabled: [1, 4, 5] });
                    }
                }
                else if (contingent == "0" || contingent == "") {
                    $("#content").tabs({ disabled: [1, 5] });
                }
            }

            else if (opt1 == "0" || opt1 == "") {           //Option 2
                $("#content").tabs({ disabled: [2] });
                if (opt2 == "0" || opt2 == "") {
                    $("#content").tabs({ disabled: [2, 3] });
                    if (opt3 == "0" || opt3 == "") {
                        $("#content").tabs({ disabled: [2, 3, 4] });
                        if (contingent == "0" || contingent == "") {
                            $("#content").tabs({ disabled: [2, 3, 4, 5] });
                        }
                    }
                    else if (contingent == "0" || contingent == "") {
                        $("#content").tabs({ disabled: [2, 3, 5] });
                    }
                }
                else if (opt3 == "0" || opt3 == "") {
                    $("#content").tabs({ disabled: [2, 4] });
                    if (contingent == "0" || contingent == "") {
                        $("#content").tabs({ disabled: [2, 4, 5] });
                    }
                }
                else if (contingent == "0" || contingent == "") {
                    $("#content").tabs({ disabled: [2, 5] });
                }
            }

            else if (opt2 == "0" || opt2 == "") {           //Option 3
                $("#content").tabs({ disabled: [3] });
                if (opt3 == "0" || opt3 == "") {
                    $("#content").tabs({ disabled: [3, 4] });
                    if (contingent == "0" || contingent == "") {
                        $("#content").tabs({ disabled: [3, 4, 5] });
                    }
                }
                else if (contingent == "0" || contingent == "") {
                    $("#content").tabs({ disabled: [3, 5] });
                }
            }

            else if (opt3 == "0" || opt3 == "") {           //Option 4
                $("#content").tabs({ disabled: [4] });
                if (contingent == "0" || contingent == "") {
                    $("#content").tabs({ disabled: [4, 5] });
                }
            }

            else if (contingent == "0" || contingent == "") {           //Contingent
                $("#content").tabs({ disabled: [5] });
            }

            
            var Tabsel = $("#content").tabs('option', 'active');
            //hdnSelTab
            if (Tabsel == 0) {
                $("#ContentMain_totCommission").text(FormatCurrency(parseFloat($('[id^=ContentMain_hdnLeaseCommTot]').val()), '$'));
                // $("#ContentMain_totCommission").text(parseFloat($('[id^=ContentMain_hdnLeaseCommTot]').val()));
                //$("#ContentMain_ucCommSummary_tbOptionsummarry").hide();
                //$("#ContentMain_ucCommSummary_tbBrokersummary").show();
                document.getElementById('troptionsummary').style.display = 'none';
                document.getElementById('trtotopt').style.display = 'none';
                document.getElementById('trcontingentsummary').style.display = 'none';
                document.getElementById('trtotcontingent').style.display = 'none';
                document.getElementById('trbrokersummary').style.display = 'block';
                document.getElementById('trtotbroker').style.display = 'block';

                document.getElementById('troptionsummary1').style.display = 'none';
                document.getElementById('trtotopt1').style.display = 'none';
                document.getElementById('troptionsummary2').style.display = 'none';
                document.getElementById('trtotopt2').style.display = 'none';
                document.getElementById('troptionsummary3').style.display = 'none';
                document.getElementById('trtotopt3').style.display = 'none';

                SetCompanySpitSummary("ContentMain_ucCommSummary_tbBrokersummary");
            }

            $("#content").tabs();
            $('#content').click('tabsselect', function (event, ui) {
                
                var SelectedTab = $("#content").tabs('option', 'active');
                //We have change the ordering of Tabs.Contingent Tab Placed at last from position of 2 so numbering of selected tab have placed randomly in below code.

                if (SelectedTab == 0) {
                    $("#ContentMain_totCommission").text(FormatCurrency(parseFloat($('[id^=ContentMain_hdnLeaseCommTot]').val()), '$'));
                    //  $("#ContentMain_totCommission").text(parseFloat($('[id^=ContentMain_hdnLeaseCommTot]').val()));
                    //$("#ContentMain_ucCommSummary_tbOptionsummarry").hide();
                    //$("#ContentMain_ucCommSummary_tbBrokersummary").show();
                    document.getElementById('troptionsummary').style.display = 'none';
                    document.getElementById('trtotopt').style.display = 'none';
                    document.getElementById('trbrokersummary').style.display = 'block';
                    document.getElementById('trtotbroker').style.display = 'block';
                    document.getElementById('trcontingentsummary').style.display = 'none';
                    document.getElementById('trtotcontingent').style.display = 'none';

                    document.getElementById('troptionsummary1').style.display = 'none';
                    document.getElementById('trtotopt1').style.display = 'none';
                    document.getElementById('troptionsummary2').style.display = 'none';
                    document.getElementById('trtotopt2').style.display = 'none';
                    document.getElementById('troptionsummary3').style.display = 'none';
                    document.getElementById('trtotopt3').style.display = 'none';

                    // for comp split cal

                    SetCompanySpitSummary("ContentMain_ucCommSummary_tbBrokersummary");
                    document.getElementById('tbCompSpliSumm').style.display = 'block';


                }
                else if (SelectedTab == 1) {
                    $("#ContentMain_totCommission").text(FormatCurrency(parseFloat($('[id^=ContentMain_hdnOptCommTot]').val()), '$'));
                    // $("#ContentMain_totCommission").text(parseFloat($('[id^=ContentMain_hdnOptCommTot]').val()));
                    document.getElementById('trbrokersummary').style.display = 'none';
                    document.getElementById('trtotbroker').style.display = 'none';
                    document.getElementById('trtotopt').style.display = 'block';
                    document.getElementById('troptionsummary').style.display = 'block';
                    document.getElementById('trcontingentsummary').style.display = 'none';
                    document.getElementById('trtotcontingent').style.display = 'none';

                    document.getElementById('troptionsummary1').style.display = 'none';
                    document.getElementById('trtotopt1').style.display = 'none';
                    document.getElementById('troptionsummary2').style.display = 'none';
                    document.getElementById('trtotopt2').style.display = 'none';
                    document.getElementById('troptionsummary3').style.display = 'none';
                    document.getElementById('trtotopt3').style.display = 'none';

                    //$("#ContentMain_ucCommSummary_tbOptionsummarry").show();

                    //$("#ContentMain_ucCommSummary_tbBrokersummary").hide();
                    // SetCompanySpitSummary("ContentMain_ucCommSummary_tbOptionsummarry");
                    document.getElementById('tbCompSpliSumm').style.display = 'none';
                    //tbCompSpliSumm
                }
                else if (SelectedTab == 5) {
                    $("#ContentMain_totCommission").text(FormatCurrency(parseFloat($('[id^=ContentMain_hdnContigentTot]').val()), '$'));
                    //  $("#ContentMain_totCommission").text(parseFloat($('[id^=ContentMain_hdnContigentTot]').val()).toFixed(2));
                    document.getElementById('trbrokersummary').style.display = 'none';
                    document.getElementById('trtotbroker').style.display = 'none';
                    document.getElementById('troptionsummary').style.display = 'none';
                    document.getElementById('trtotopt').style.display = 'none';
                    document.getElementById('trcontingentsummary').style.display = 'block';
                    document.getElementById('trtotcontingent').style.display = 'block';

                    document.getElementById('troptionsummary1').style.display = 'none';
                    document.getElementById('trtotopt1').style.display = 'none';
                    document.getElementById('troptionsummary2').style.display = 'none';
                    document.getElementById('trtotopt2').style.display = 'none';
                    document.getElementById('troptionsummary3').style.display = 'none';
                    document.getElementById('trtotopt3').style.display = 'none';

                    //SetCompanySpitSummary("ContentMain_ucCommSummary_tbcontingent");
                    document.getElementById('tbCompSpliSumm').style.display = 'none';
                }
                else if (SelectedTab == 2) {
                    $("#ContentMain_totCommission").text(FormatCurrency(parseFloat($('[id^=ContentMain_hdnOptCommTot1]').val()), '$'));

                    document.getElementById('trbrokersummary').style.display = 'none';
                    document.getElementById('trtotbroker').style.display = 'none';
                    document.getElementById('trtotopt').style.display = 'none';
                    document.getElementById('troptionsummary').style.display = 'none';
                    document.getElementById('trcontingentsummary').style.display = 'none';
                    document.getElementById('trtotcontingent').style.display = 'none';

                    document.getElementById('troptionsummary1').style.display = 'block';
                    document.getElementById('trtotopt1').style.display = 'block';
                    document.getElementById('troptionsummary2').style.display = 'none';
                    document.getElementById('trtotopt2').style.display = 'none';
                    document.getElementById('troptionsummary3').style.display = 'none';
                    document.getElementById('trtotopt3').style.display = 'none';

                    document.getElementById('tbCompSpliSumm').style.display = 'none';

                }
                else if (SelectedTab == 3) {
                    $("#ContentMain_totCommission").text(FormatCurrency(parseFloat($('[id^=ContentMain_hdnOptCommTot2]').val()), '$'));

                    document.getElementById('trbrokersummary').style.display = 'none';
                    document.getElementById('trtotbroker').style.display = 'none';
                    document.getElementById('trtotopt').style.display = 'none';
                    document.getElementById('troptionsummary').style.display = 'none';
                    document.getElementById('trcontingentsummary').style.display = 'none';
                    document.getElementById('trtotcontingent').style.display = 'none';

                    document.getElementById('troptionsummary1').style.display = 'none';
                    document.getElementById('trtotopt1').style.display = 'none';
                    document.getElementById('troptionsummary2').style.display = 'block';
                    document.getElementById('trtotopt2').style.display = 'block';
                    document.getElementById('troptionsummary3').style.display = 'none';
                    document.getElementById('trtotopt3').style.display = 'none';

                    document.getElementById('tbCompSpliSumm').style.display = 'none';

                }
                else if (SelectedTab == 4) {
                    $("#ContentMain_totCommission").text(FormatCurrency(parseFloat($('[id^=ContentMain_hdnOptCommTot3]').val()), '$'));

                    document.getElementById('trbrokersummary').style.display = 'none';
                    document.getElementById('trtotbroker').style.display = 'none';
                    document.getElementById('trtotopt').style.display = 'none';
                    document.getElementById('troptionsummary').style.display = 'none';
                    document.getElementById('trcontingentsummary').style.display = 'none';
                    document.getElementById('trtotcontingent').style.display = 'none';

                    document.getElementById('troptionsummary1').style.display = 'none';
                    document.getElementById('trtotopt1').style.display = 'none';
                    document.getElementById('troptionsummary2').style.display = 'none';
                    document.getElementById('trtotopt2').style.display = 'none';
                    document.getElementById('troptionsummary3').style.display = 'block';
                    document.getElementById('trtotopt3').style.display = 'block';

                    document.getElementById('tbCompSpliSumm').style.display = 'none';

                }
            });
        });

        function SetCompanySpitSummary(strTableName) {
            debugger;
            var DestTable = document.getElementById(strTableName);
            var lbUser = $("#lbUser").text();
            var LoggedInUser = '<%=Session["UserName"]%>';
            var loggedintype = '<%=Session["UserType"]%>';
            for (var i = 0; i <= DestTable.rows.length - 1; i++) {
                var Broker = DestTable.rows[i].cells[0].innerHTML;
                //var index = Broker.indexOf('<', 2);
                //var startindex = Broker.indexOf('>');
                //var brokername = Broker.substring(startindex+1, index);
                if (Broker == LoggedInUser) {
                    var Amount = DestTable.rows[i].cells[4].innerHTML == "" ? 0 : parseFloat(replaceAll(",", "", DestTable.rows[i].cells[4].innerHTML.replace("$", "")));
                    UpdateCompSplitSummary(Amount, LoggedInUser);
                }
            }
        }

        //function UpdateCompSplitSummary(UserCommTot,LoggedInUser) {
        //    debugger;
        //    var uid = LoggedInUser.trim().replace(/[^A-Za-z0-9]+/gi, '-');
        //    alert(uid+'new')
        //    var tdname = "td_threshold" + uid;
        //    var Comm = $('table[id$="tbCompanyCommsionSummary"] td.' + tdname + '').text();
        //    alert(Comm);
        //    if (Comm != "" && UserCommTot != '') {
        //        var index = Comm.indexOf(':');

        //        var UserPer = Comm.substring(0, index);
        //        var CompanyPer = Comm.substring(index + 1, Comm.length);

        //        var UserComm = (parseFloat(UserPer) / 100) * parseFloat(UserCommTot);
        //        var CompanyComm = (parseFloat(CompanyPer) / 100) * parseFloat(UserCommTot);

        //        var Total = parseFloat(UserComm) + parseFloat(CompanyComm);
        //        alert(Total);
        //        var usertd = "td_UserComm" + uid;
        //        var tdper = "td_CompComm" + uid;
        //        var tdtotal = "td_total" + uid;
        //        $('table[id$="tbCompanyCommsionSummary"] td.' + usertd + '').text("$" + parseFloat(UserComm).toFixed(2));
        //        $('table[id$="tbCompanyCommsionSummary"] td.' + tdper + '').text("$" + parseFloat(CompanyComm).toFixed(2));
        //        $('table[id$="tbCompanyCommsionSummary"] td.' + tdtotal + '').text("$" + parseFloat(Total).toFixed(2));
        //        //$('table[id$="tbCompanyCommsionSummary"] td.td_UserComm'+ uid + '').text("$" + parseFloat(UserComm).toFixed(2));
        //        //$('table[id$="tbCompanyCommsionSummary"] td.td_CompComm'+ uid + '').text("$" + parseFloat(CompanyComm).toFixed(2));
        //        //$('table[id$="tbCompanyCommsionSummary"] td.td_total'+ uid + '').text("$" + parseFloat(Total).toFixed(2));
        //        alert(Total);

        //    }
        //    //var Comm = $("#td_Threashold").text();
        //    //if (Comm != "" && UserCommTot != '') {
        //    //    var index = Comm.indexOf(':');

        //    //    var UserPer = Comm.substring(0, index);
        //    //    var CompanyPer = Comm.substring(index + 1, Comm.length);

        //    //    var UserComm = (parseFloat(UserPer) / 100) * parseFloat(UserCommTot);
        //    //    var CompanyComm = (parseFloat(CompanyPer) / 100) * parseFloat(UserCommTot);

        //    //    var Total = parseFloat(UserComm) + parseFloat(CompanyComm);
        //    //    $("#td_UserComm").text("$" + parseFloat(UserComm).toFixed(2));
        //    //    $("#td_CompComm").text("$" + parseFloat(CompanyComm).toFixed(2));
        //    //    $("#td_total").text("$" + parseFloat(Total).toFixed(2));
        //    //}
        //}

        function FormatCurrency(n, currency) {

            return currency + " " + n.toFixed(2).replace(/./g, function (c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
            });
        }

        //$(document).ready(function () {
        //    var $tabs = $('#tabs').tabs();
        //    var active = $("#tabs").tabs("option", "active");
        //    alert(active);
        //});

        //$(function () {
        //    var $tabs = $("#content").tabs({
        //        select: function (e, ui) {
        //            var thistab = ui.index;
        //            //$("[id$=Label1]").text(thistab);
        //            $("[id$=hdnSelectedTab]").val(thistab);

        //            alert(thistab);
        //           // $("#tab" + thistab).html(getCars(thistab));
        //        }
        //    });
        //});



    </script>
    <%-- <style type="text/css">
        .ui-widget-header {
            font-weight: bold;
            font-size: 1.3em;
            background: gray;
            font-family: Verdana;
        }

        .ui-widget-content {
            border: 2px solid black;
        }

        .ui-dialog-titlebar-close {
            border: none;
            background-color: gray;
            background-repeat: no-repeat;
        }

        .ui-icon {
            display: none;
        }

        .ui-dialog {
            padding: 0px;
        }

        .ui-corner-all {
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
            border-top-left-radius: 0px;
            border-top-right-radius: 0px;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <table style="width: 100%">
        <tr>
            <td class="mainPageSubTitle">COMMISSION SPLIT CALCULATOR
            </td>
            <td class="mainPageTitle">NEW CHARGE SLIP 
            </td>
        </tr>
    </table>
    <hr style="width: 100%" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <ajax:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true" />

    <table style="width: 100%">

        <tr>
            <td style="width: 70%; vertical-align: top;">
                <table style="text-align: right;">
                    <tr>
                        <td style="width: 60%;"></td>
                        <td style="font-weight: bold; width: 5%;" nowrap="nowrap">TOTAL COMMISSION
                        </td>
                        <%--  <td style="width: 5%">&nbsp;&nbsp;</td>--%>
                        <td style="padding-right: 20px; color: #E65C00;">
                            <asp:Label ID="totCommission" ForeColor="#E65C00" Style="text-wrap: avoid;" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>

                <div id="content" style="border: 1px solid black;">
                    <ul>
                        <li><a href="#tab1">BASE</a></li>
                        <%--<li><a href="#tab2">OPTIONS</a></li>--%>
                        <li><a href="#tab2">OT-1</a></li>                        
                        <li><a href="#tab4">OT-2</a></li>
                        <li><a href="#tab5">OT-3</a></li>
                        <li><a href="#tab6">OT-4</a></li>
                        <li><a href="#tab3">CONTINGENT</a></li>
                    </ul>
                    <div id="tab1">
                        <ol style="font-size: x-small; font-style: italic;">
                            <li>Add brokers to the deal by pressing the add button in the associated broker category.</li>
                            <li>Input their commission percentage to add them to the deal.</li>
                        </ol>
                        <uc:CommissionSplitCalculator ID="CommCalculator" runat="server" />
                    </div>
                    <div id="tab2">

                        <uc:CommissionSplitCalculator ID="OptCommCalculator" runat="server" />
                    </div>
                    <div id="tab3">

                        <uc:CommissionSplitCalculator ID="ContingentCommCalculator" runat="server" />
                    </div>
                    <div id="tab4">
                        <uc:CommissionSplitCalculator ID="Opt1CommCalculator" runat="server" />
                    </div>
                    <div id="tab5">
                        <uc:CommissionSplitCalculator ID="Opt2CommCalculator" runat="server" />
                    </div>
                    <div id="tab6">
                        <uc:CommissionSplitCalculator ID="Opt3CommCalculator" runat="server" />
                    </div>
                </div>
            </td>
            <td style="width: 30%; vertical-align: top;">
                <div>
                    <uc:Summary ID="ucCommSummary" runat="server" />
                </div>
                <br />
                <div>
                    <uc:CompanySplitSummary ID="ucCompanySumm" runat="server" />
                </div>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnLeaseCommTot" runat="server" />
    <asp:HiddenField ID="hdnOptCommTot" runat="server" />
    <asp:HiddenField ID="hdnContigentTot" runat="server" />
    <asp:HiddenField ID="hdnCoBrokerCompany" runat="server" />
    <asp:HiddenField ID="hdnOptCommTot1" runat="server" />
    <asp:HiddenField ID="hdnOptCommTot2" runat="server" />
    <asp:HiddenField ID="hdnOptCommTot3" runat="server" />
    <%-- <input type="text" id="inpborder" value="asdf"  style=""/>
    <input type="button" value="Click Me" onclick=" return checkBorder();" />
    <script>
        function checkBorder() {
            document.getElementById('inpborder');
        }
    </script>--%>
</asp:Content>
<asp:Content ID="CntFooter" ContentPlaceHolderID="ContentFooter" runat="Server">
    <table style="width: 100%;">
        <tr>
            <td style="width: 60%; text-align: left;">
                <label class="whitelabel"><b>PAGE 3 OF 4</b></label>
                <asp:Label ID="lbModificationHistory" runat="server" Text=""></asp:Label>
            </td>
            <td style="width: 40%; text-align: right;">
                <asp:Button ID="btnSaveForLater" runat="server" Text="SAVE FOR LATER" OnClick="btnNext_Click" BackColor="#808080" CssClass="SqureButton" OnClientClick=" return SetSaveStatus('D');" />
                &nbsp; 
                <asp:Button ID="btnBack" runat="server" Text="BACK" CssClass="btnSmallOrange" OnClick="btnBack_Click" />
                &nbsp;    <%-- &nbsp;--%>
                <asp:Button ID="btnNext" runat="server" Text="NEXT" CssClass="btnSmallOrange" OnClick="btnNext_Click" OnClientClick="return SetSaveStatus('N');" />
                <asp:HiddenField ID="hdnSaveStatus" runat="server" Value="" />
            </td>
        </tr>
    </table>
</asp:Content>

