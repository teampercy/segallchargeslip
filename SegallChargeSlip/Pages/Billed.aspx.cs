﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;

public partial class Pages_Billed : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["IsVendor"] = "0";
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }




    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void GetChargeSlipDetails(string RowIndx)
    {
        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {

                int ChargeSlipID = Convert.ToInt32(HttpContext.Current.Session["ChargeSlipId"]);
                Utility.ClearSessionExceptUser();
                ChargeSlip objChargeSlip = new ChargeSlip();
                objChargeSlip.ChargeSlipID = ChargeSlipID;
                objChargeSlip.Load();

                DealTransactions objDealTransactions = new DealTransactions();
                objDealTransactions.ChargeSlipID = ChargeSlipID;
                objDealTransactions.LoadByChargeSlipId();


                objChargeSlip.DealTransactionsProperty = objDealTransactions;
                HttpContext.Current.Session[GlobleData.NewChargeSlipObj] = objChargeSlip;
                HttpContext.Current.Session[GlobleData.NewChargeSlipId] = ChargeSlipID;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Billed.aspx", "GetChargeSlipDetails", ex.Message);
        }
    }
    //Add for Payment Void on 15-07-2016
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string UpdateChargeslipPaymentVoid(Int32 ChargeslipId)
    {
        try
        {
            if (HttpContext.Current.Session["UserID"] != null)
            {

                int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
                Payments objPayment = new Payments();
                int count = 0;
                count = objPayment.UpdateChargeslipPaymentVoid(ChargeslipId, UserID);
                if (count > 0)
                {
                    return "success";
                }
                else
                {
                    return "failure";
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Billed.aspx", "UpdateChargeslipPaymentVoid", ex.Message);
            return "failure";
        }
        return "success";
    }

}