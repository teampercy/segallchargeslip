﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using SegallChargeSlipBll;

public partial class Pages_ReportChargeslipReviewCard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ucChargeDetailCard1.ChargeSlipId =Convert.ToInt32(Request.QueryString["id"]);
        Session["ChargeSlipId"] = Convert.ToInt32(Request.QueryString["id"]);
        ChargeSlip objChargeslip = new ChargeSlip();
        objChargeslip.ChargeSlipID = Convert.ToInt32(Request.QueryString["id"]);
        objChargeslip.Load();
        ucChargeDetailCard1.Bindata();
        ucChargeDetailCard1.FindControl("imgClose").Visible = false;
        ucChargeDetailCard1.FindControl("imgBaseCommision").Visible = false;
        ucChargeDetailCard1.FindControl("imgEditForm2").Visible = false;
       
        ucPaymentHistoryDetail1.FindControl("imgEditForm2").Visible = false;
        if (objChargeslip.Status == 2)
        {
            //ucChargeDetailCard1.FindControl("lbldelreasontxt").Visible = true;
            //ucChargeDetailCard1.Fin
            //ucChargeDetailCard1.FindControl("lblhdrDelReason").Visible = true;
            ucChargeDetailCard1.FindControl("FormView4").Visible = true;
            HtmlControl YourDiv = (HtmlControl)ucChargeDetailCard1.FindControl("signin");
            YourDiv.Style.Add("height", "760px");
          // ucChargeDetailCard1.FindControl("signin")
        }
       //ScriptManager.RegisterStartupScript(this, Page.GetType(), "hide", "javascript:return hideEdit(), true);
        //ScriptManager.RegisterStartupScript(this, Page.GetType(), "Hide", "hideEdit();", true);
 
    }
    protected void btnRefreshPayment_Click(object sender, EventArgs e)
    {
        if (hdnChargeslipId.Value != "0")
        {
           
                ucPaymentHistoryDetail1.ChargeSlipId = Convert.ToInt32(hdnChargeslipId.Value.ToString());
                ucPaymentHistoryDetail1.Bindata();

                //if (ucBilledTab1.FindControl("hdnPopup") != null)
                //{
                    HiddenField hdn = (HiddenField)ucPaymentHistoryDetail1.Parent.FindControl("hdnPopup");
                    hdn.Value = "0";
               // }
            }
        
    }
}