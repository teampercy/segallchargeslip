﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Pages_Reports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // SK : 08/12/2013---------------- vv--------
        //if (Session["UserId"] == null)
        //{

        //    Session.Clear();
        //    Session.RemoveAll();
        //    Server.Transfer("Login.aspx");
        //    //Response.Redirect("Login.aspx");
        //}

        try
        {
            rptReport.Visible = true;
            if (!Page.IsPostBack)
            {
                rptReport.LocalReport.EnableHyperlinks = true;
                rptReport.Reset();
                rptReport.LocalReport.DataSources.Clear();
                CustomList objCL = new CustomList();
                //Utility.FillDropDown(ref ddlType, objCL.GetReportType(), "ReportType", "ReportID");
                int usertype = Convert.ToInt32(Session["UserType"]);
                Utility.FillDropDown(ref ddlType, objCL.GetReportType(usertype), "ReportType", "ReportID");//Added by Shishir 02-05-2016
                BindDropDown();

            }
            SetAttributes();
            HideDropdown();
            displayDropdown();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Reports.aspx", "Page_Load", ex.Message);
        }

    }
    private void SetAttributes()
    {
        txtvalue1.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtvalue1.ClientID + "')");
        txtvalue2.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtvalue2.ClientID + "')");
        txtFrom.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtFrom.ClientID + "')");
        txtTo.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtTo.ClientID + "')");
    }

    //Added by Shishir 29-04-2016
    //Called when a CheckNumber is entered in GridView and Saved
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SaveBrokerCheckNo(string PaymentHistoryId, string CheckNo)//, string ChargeSlipID, string PaymentID, string BrokerID)
    {
        try
        {
            if (HttpContext.Current.Session["BrokerId"] != null)
            {
                //if ((!String.IsNullOrEmpty(PaymentHistoryId)) && (!String.IsNullOrEmpty(CheckNo)))
                if ((!String.IsNullOrEmpty(PaymentHistoryId)))
                {
                    int PHID = Convert.ToInt32(PaymentHistoryId.Trim());
                    DealTransactions objDealTransactions = new DealTransactions();
                    objDealTransactions.UpdateCheckNo(PHID, CheckNo);
                    //if(objDealTransactions.UpdateCheckNo(PHID, CheckNo))
                    //{
                    //    if(!String.IsNullOrEmpty(CheckNo))
                    //    {
                    //        HttpResponse response = HttpContext.Current.Response;
                    //        response.Write("<script  type = 'text/javascript'>alert('Data Updated Successfully!!');</script>");

                    //        Pages_Reports PR = new Pages_Reports();
                    //        PR.SendEmail(ChargeSlipID, PaymentID, BrokerID);



                    //    }
                    //}
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Customer.aspx", "SaveBrokerCheckNo", ex.Message);
        }
    }


    //Added by Shishir 03-05-2016
    //public void SendEmail(string ChargeSlipID, string PaymentID, string BrokerID)
    //{
    //    //Response.Write("<script  type = 'text/javascript'>alert('Email Sent Successfully!!');</script>");

    //    CreateLogFiles Log = new CreateLogFiles();
    //    Report objPaymentReport = new Report();
    //    bool IsPayment = false;
    //    string Reportpath = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["ReportPath"]);
    //    objPaymentReport.ChargeSlipId = Convert.ToInt32(ChargeSlipID);
    //    objPaymentReport.PaymentId = Convert.ToInt32(PaymentID);
    //    objPaymentReport.UserId = Convert.ToInt32(BrokerID);
    //    DataTable dtBroker = objPaymentReport.GetCobrokerList();
    //    if (dtBroker != null && dtBroker.Rows.Count > 0)
    //    {
    //        for (int j = 0; j < dtBroker.Rows.Count; j++)
    //        {
    //            objPaymentReport.UserId = Convert.ToInt32(dtBroker.Rows[j]["BrokerId"].ToString());
    //            objPaymentReport.CreatedBy = objPaymentReport.UserId;
    //            DataTable dtAdminEmail = objPaymentReport.GetUserAndAdminEmail();
    //            DataSet dspaymentCommission = objPaymentReport.GetPaymentCommissionDetail();
    //            if (dspaymentCommission != null && dspaymentCommission.Tables.Count > 0)
    //            {
    //                Email objPaymentEmail = new Email();
    //                StringBuilder ChSlipsemailtext = new StringBuilder("<html><body><table style='font-size:14px;' width='80%'>");

    //                Log.WriteLog("Payment started",Reportpath);
    //                if (dspaymentCommission.Tables[0] != null && dspaymentCommission.Tables[0].Rows.Count > 0)
    //                {
    //                    for (int r = 0; r < dspaymentCommission.Tables[0].Rows.Count; r++)
    //                    {
    //                        objPaymentEmail.EmailSub = "Payment applied to Charge Slip " + dspaymentCommission.Tables[0].Rows[r]["DealName"].ToString();
    //                        objPaymentEmail.EmailFrom = dtAdminEmail.Rows[0]["AdminEmail"].ToString();
    //                        ChSlipsemailtext.Append("<tr><td >Payment applied to Charge Slip DealName: " + dspaymentCommission.Tables[0].Rows[r]["DealName"].ToString() + ".<td></tr>");
    //                        ChSlipsemailtext.Append("<tr><td >&nbsp;</td></tr><tr><td >A payment of <b>$" + String.Format("{0:n2}", Convert.ToDecimal(dspaymentCommission.Tables[0].Rows[r]["PaymentAmount"].ToString())) + "</b> has been applied to deal <b>" + dspaymentCommission.Tables[0].Rows[r]["DealName"].ToString() + "</b>.</td></tr>");
    //                        ChSlipsemailtext.Append("<tr><td >A commission payment of <b>$" + String.Format("{0:n2}", Convert.ToDecimal(dspaymentCommission.Tables[0].Rows[r]["SplitAmount"].ToString())) + "</b> has been applied for deal <b>" + dspaymentCommission.Tables[0].Rows[r]["DealName"].ToString() + "</b>.</td></tr>");
    //                        ChSlipsemailtext.Append("<tr><td >&nbsp;</td></tr>");
    //                        if (dspaymentCommission.Tables[1] != null && dspaymentCommission.Tables[1].Rows.Count > 0)
    //                        {
    //                            ChSlipsemailtext.Append("<tr><td ><table width='100%' style='font-size:14px;'>");
    //                            for (int p = 0; p < dspaymentCommission.Tables[1].Rows.Count; p++)
    //                            {
    //                                ChSlipsemailtext.Append("<tr><td width='10%'>" + dspaymentCommission.Tables[1].Rows[p]["BrokerSubPercent"].ToString() + "%</td>");
    //                                ChSlipsemailtext.Append("<td width='30%'>" + dspaymentCommission.Tables[1].Rows[p]["Name"].ToString() + " (" + dspaymentCommission.Tables[1].Rows[p]["BrokerTypes"].ToString() + ")</td>");
    //                                ChSlipsemailtext.Append("<td width='10%' align='right'><b>" + dspaymentCommission.Tables[1].Rows[p]["SplitAmount"].ToString() + "</b></td>");
    //                                ChSlipsemailtext.Append("<td width='50%' style='padding-left:100px;'>Check No: " + dspaymentCommission.Tables[1].Rows[p]["CheckNo"].ToString() + "</td></tr>");
    //                                if (dspaymentCommission.Tables[1].Rows[p]["CurrentLogin"].ToString() == "1")
    //                                {
    //                                    ChSlipsemailtext.Append("<tr><td></td><td>" + dspaymentCommission.Tables[1].Rows[p]["CompanyName"].ToString() + "</td>");
    //                                    ChSlipsemailtext.Append("<td align='right'><b>" + dspaymentCommission.Tables[1].Rows[p]["SplitCompany"].ToString() + "</b></td><td></td></tr>");
    //                                    ChSlipsemailtext.Append("<tr><td></td><td colspan='2'><hr/></td><td></td></tr>");
    //                                    ChSlipsemailtext.Append("<tr><td></td><td >Total</td><td align='right'><b>" + dspaymentCommission.Tables[1].Rows[p]["Total"].ToString() + "</b></td><td></td></tr>");
    //                                }
    //                                ChSlipsemailtext.Append("<tr><td >&nbsp;</td></tr>");
    //                            }

    //                            ChSlipsemailtext.Append("</table></td></tr>");
    //                        }
    //                        ChSlipsemailtext.Append("<tr><td >&nbsp;</td></tr><tr><td><b>Payment Date:</b> " + dspaymentCommission.Tables[0].Rows[r]["PaymentDate"].ToString() + "</td></tr></table></body></html>");
    //                        objPaymentEmail.EmailBody = ChSlipsemailtext.ToString();
    //                        objPaymentEmail.EmailTo = dtBroker.Rows[j]["Email"].ToString();
    //                    }
    //                }
    //                IsPayment = objPaymentEmail.SendEmail();
    //                for (int k = 0; k < dtAdminEmail.Rows.Count; k++)
    //                {
    //                    objPaymentEmail.EmailTo = dtAdminEmail.Rows[k]["AdminEmail"].ToString();

    //                    IsPayment = objPaymentEmail.SendEmail();
    //                }
    //                //string Reportpath = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["ReportPath"]);

    //                if (IsPayment)
    //                {
    //                    Log.WriteLog("Payment applied successfully to " + objPaymentEmail.EmailTo, Reportpath);
    //                }
    //                else
    //                {
    //                    Log.WriteLog("Payment failed to applied to " + objPaymentEmail.EmailTo, Reportpath);
    //                }

    //            }

    //        }
    //    }
    //    if (IsPayment)
    //    {
    //        objPaymentReport.SetMessageEmailSent();

    //    }
    //    Log.WriteLog("Payment Mail Sent",Reportpath);




    //}

    public void HideDropdown()
    {
        trLandLord.Visible = false;
        trOwner.Visible = false;
        trTenant.Visible = false;
        trBroker.Visible = false;
        trLocation.Visible = false;
        trAddress.Visible = false;
        trValue.Visible = false;
        trCity.Visible = false;
        trCounty.Visible = false;
        trState.Visible = false;
        trZipCode.Visible = false;
        trDesignationType.Visible = false;
        trMultipleBroker.Visible = false;
        trBrokerList.Visible = false;
    }
    public void BindDropDown()
    {
        try
        {
            int usertype = Convert.ToInt32(Session["UserType"]);
            CustomList objCL = new CustomList();
            Users objUser = new Users();
            objUser.UserID = Convert.ToInt32(Session["UserId"]);
            //Utility.FillDropDown(ref ddlType, objCL.GetReportType(), "ReportType", "ReportID");
            Utility.FillDropDown(ref ddlLandLord, objCL.GetCompanyList(), "Company", "CompanyID");
            Utility.FillDropDown(ref ddlOwner, objCL.GetCompanyList(), "Company", "CompanyID");
            Utility.FillDropDown(ref ddlTenant, objCL.GetTenant(), "PartyCompanyName", "PartyID");
            Utility.FillDropDown(ref ddlDesignationType, objCL.GetDealDesignationTypes(), "DealDesignationTypes", "DealDesignationTypesId");
            //if (usertype==1)//admin user type=1
            //    Utility.FillDropDownBroker(ref ddlBroker, objCL.GetBrokerList(), "Name", "UserID");
            //else
            //    Utility.FillDropDownBroker(ref ddlBroker, objUser.GetBrokerListByUsersCompany(), "BrokerName", "BrokerID");

            ////Updated by Shishir 28-04-2016
            if (usertype == 1)//admin user type=1
                if (ddlType.SelectedValue == "17" || ddlType.SelectedValue == "18" || ddlType.SelectedValue == "20")
                {
                    Utility.FillDropDownBroker(ref ddlBroker, objCL.GetBrokerListForAllBrokers(), "Name", "UserID");
                    DataTable dtinternalbroker = new DataTable();
                    dtinternalbroker = objCL.GetBrokerList();
                    if (dtinternalbroker != null && dtinternalbroker.Rows.Count > 0)
                    {
                        DropDownCheckBoxes1.DataSource = dtinternalbroker;
                        DropDownCheckBoxes1.DataValueField = "UserID";
                        DropDownCheckBoxes1.DataTextField = "Name";
                        DropDownCheckBoxes1.DataBind();
                    }
                    foreach (System.Web.UI.WebControls.ListItem item in DropDownCheckBoxes1.Items)
                    {
                        //item.Attributes.Add("style", "color:red");
                        item.Attributes.Add("class", "RedColor");

                        //if(item.Value == "someStringValue") {
                        //    item.Attributes.Add("style", "color:red");
                        //    item.Attribute
                        //}
                    }
                    DataTable dtnoninternalBroker = new DataTable();
                    dtnoninternalBroker = objCL.GetOutsideBrokerList();
                    if (dtnoninternalBroker != null && dtnoninternalBroker.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtnoninternalBroker.Rows.Count; i++)
                        {
                            System.Web.UI.WebControls.ListItem objli = new System.Web.UI.WebControls.ListItem();
                            objli.Value = dtnoninternalBroker.Rows[i]["UserID"].ToString();
                            objli.Text = dtnoninternalBroker.Rows[i]["Name"].ToString();
                            DropDownCheckBoxes1.Items.Add(objli);
                        }
                        //DropDownCheckBoxes1.DataSource = dtnoninternalBroker;
                        //DropDownCheckBoxes1.DataValueField = "UserID";
                        //DropDownCheckBoxes1.DataTextField = "Name";
                        //DropDownCheckBoxes1.DataBind();
                    }
                    //ListItem  objli=new ListItem();
                    //objli.Value="";
                    //objli.Text="---------------";
                    //DropDownCheckBoxes1.Items.Add(objli);
                    //DropDownCheckBoxes1.Items.Add().
                }
                else
                {
                    Utility.FillDropDownBroker(ref ddlBroker, objCL.GetBrokerList(), "Name", "UserID");
                }
            else
            {
                Utility.FillDropDownBroker(ref ddlBroker, objUser.GetBrokerListByUsersCompany(), "BrokerName", "BrokerID");
            }
            Utility.FillDropDown(ref ddlState, objCL.GetStateList(), "StateName", "State");
            Utility.FillDropDown(ref ddlLocation, objCL.GetLocationList(), "Name", "LocationID");
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Report.aspx", "BindDropDown", ex.Message);
        }



    }
    protected void Resetcontrols()
    {
        Session["MultipleBrokerId"] = null;
        BindDropDown();
        txtTo.Text = "";
        txtFrom.Text = "";
        txtvalue1.Text = "";
        txtvalue2.Text = "";
        txtCity.Text = "";
        txtCounty.Text = "";
        txtZipCode.Text = "";
        txtLocation.Text = "";
        cleanlistbox();
        if (ddlType.SelectedValue == "12")
        {
            CustomList objCL = new CustomList();
            Utility.FillDropDown(ref ddlBroker, objCL.GetOutsideBrokerList(), "Name", "UserID");
        }
        gvBrokerReport.DataSource = null;
        gvBrokerReport.DataBind();
        pnlBrokerReport.Visible = false;
        ////Broker Report
        txtCritera1From.Text = "";
        txtCritera1To.Text = "";
        txtCritera2From.Text = "";
        txtCritera2To.Text = "";
        txtCritera3From.Text = "";
        txtCritera3To.Text = "";
    }
    protected void displayDropdown()
    {
        try
        {
            HideDropdown();
            rptReport.LocalReport.EnableHyperlinks = true;
            int usertype = Convert.ToInt32(Session["UserType"]);
            if (usertype == 1)
            {
                trBroker.Visible = true;
                if (ddlType.SelectedValue == "18" || ddlType.SelectedValue == "20")
                {
                    trBroker.Visible = false;
                    trMultipleBroker.Visible = true;
                }
            }
            else
                trBroker.Visible = false;
            if (ddlType.SelectedValue == "1" || ddlType.SelectedValue == "2" || ddlType.SelectedValue == "9" || ddlType.SelectedValue == "10" || ddlType.SelectedValue == "11" || ddlType.SelectedValue == "14" || ddlType.SelectedValue == "16" || ddlType.SelectedValue == "18"||ddlType.SelectedValue=="21")
            {
                //  trBroker.Visible = true;
                trDate.Visible = true;
            }
            else if (ddlType.SelectedValue == "3")
            {
                //  trBroker.Visible = true;
                trTenant.Visible = true;
                trDate.Visible = true;
            }
            else if (ddlType.SelectedValue == "4")
            {
                //  trBroker.Visible = true;
                trOwner.Visible = true;
                trDate.Visible = true;
            }
            else if (ddlType.SelectedValue == "5")
            {
                // trBroker.Visible = true;
                trLandLord.Visible = true;
                trDate.Visible = true;
            }
            else if (ddlType.SelectedValue == "6")
            {
                // trBroker.Visible = true;
                trOwner.Visible = true;
                trTenant.Visible = true;
                trDate.Visible = true;
            }
            else if (ddlType.SelectedValue == "7")
            {
                // trBroker.Visible = true;
                trOwner.Visible = true;
                trValue.Visible = true;
                trLocation.Visible = true;
                lblLOCATION.Text = string.Empty;
                trDate.Visible = true;
            }
            else if (ddlType.SelectedValue == "8")
            {
                // trBroker.Visible = true;
                trLocation.Visible = true;
                trAddress.Visible = true;
                lblLOCATION.Text = "LOCATION";
                trDate.Visible = true;
            }
            else if (ddlType.SelectedValue == "12")
            {
                trBroker.Visible = true;
                trDate.Visible = true;
            }
            else if (ddlType.SelectedValue == "13")
            {
                // trBroker.Visible = true;
                trCity.Visible = true;
                trCounty.Visible = true;
                trState.Visible = true;
                trZipCode.Visible = true;
                trDate.Visible = true;

            }
            else if (ddlType.SelectedValue == "17")
            {
                trDate.Visible = false;
            }
            else if (ddlType.SelectedValue == "18")
            {

                trDate.Visible = true;
                trBroker.Visible = false;
                trMultipleBroker.Visible = true;
            }
            else if (ddlType.SelectedValue == "19")
            {
                trDate.Visible = true;
                trDesignationType.Visible = true;
            }
            else if (ddlType.SelectedValue == "20")
            {

                trDate.Visible = false;
                trBroker.Visible = false;
                trMultipleBroker.Visible = true;
                trBrokerList.Visible = true;
            }
            string i = ddlType.SelectedValue;
            //if (!string.IsNullOrEmpty(i))
            //{
            //    string ParaId = Request.Url.GetLeftPart(UriPartial.Path);
            //    int index = ParaId.LastIndexOf('/');
            //    string url = ParaId.Substring(0, index);
            //    string IsAdminUserType = "1";
            //    if (ddlBroker.SelectedValue == "" || ddlBroker.SelectedIndex == -1 || ddlBroker.SelectedValue == "0")
            //    {
            //        if (usertype == 1)
            //        {
            //            IsAdminUserType = "1";
            //        }
            //        else
            //        {
            //            IsAdminUserType = "2";
            //        }
            //    }
            //    else
            //    {
            //        IsAdminUserType = "2";
            //    }
            //    ReportParameter p1 = new ReportParameter("ParamUrl", url, false);
            //    ReportParameter p2 = new ReportParameter("IsAdminUserType", IsAdminUserType, false);

            //    rptReport.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
            //}

            if (i == "1")
            {
                //lbltitle.Text = "INVOICE";
                rptReport.LocalReport.ReportPath = "Reports/Invoice.rdlc";
            }
            else if (i == "2")
            {
                //  lbltitle.Text = "OPTIONS DUE";
                rptReport.LocalReport.ReportPath = "Reports/OptionsDue.rdlc";
            }
            else if (i == "3")
            {
                // lbltitle.Text = "TENANTS";
                rptReport.LocalReport.ReportPath = "Reports/Tenant.rdlc";

            }
            else if (i == "4")
            {
                // lbltitle.Text = "OWNERS";
                rptReport.LocalReport.ReportPath = "Reports/OwnerReport.rdlc";
            }
            else if (i == "6")
            {
                // lbltitle.Text = "SQUARE FOOT LEASE";

                rptReport.LocalReport.ReportPath = "Reports/SquareFootLease.rdlc";
            }
            else if (i == "7")
            {
                //lbltitle.Text = "TOTAL VALUE LEASE";
                rptReport.LocalReport.ReportPath = "Reports/TotalValueLease.rdlc";
            }
            else if (i == "8")
            {
                // lbltitle.Text = "TOTAL VALUE SALES";
                rptReport.LocalReport.ReportPath = "Reports/TotalValueSales.rdlc";
            }
            else if (i == "9")
            {
                // lbltitle.Text = "GROSS RECEIVABLES";
                rptReport.LocalReport.ReportPath = "Reports/GrossReceivables.rdlc";
            }
            else if (i == "10")
            {
                lbltitle.Text = "";
                rptReport.LocalReport.ReportPath = "Reports/OptionContingent.rdlc";
            }
            else if (i == "11")
            {

                // lbltitle.Text = "In-House Co-Broker Report";
                rptReport.LocalReport.ReportPath = "Reports/InHouseCoBroker.rdlc";
            }
            else if (i == "12")
            {
                lbltitle.Text = "Select Broker";
                CustomList objCL = new CustomList();
                //   Utility.FillDropDown(ref ddlBroker, objCL.GetOutsideBrokerList(), "Name", "UserID");
                rptReport.LocalReport.ReportPath = "Reports/OutsideBroker.rdlc";
            }
            else if (i == "13")
            {
                //  lbltitle.Text = "Geographic Report";
                rptReport.LocalReport.ReportPath = "Reports/Geographic.rdlc";
            }
            else if (i == "14")
            {
                //  lbltitle.Text = "Geographic Report";
                rptReport.LocalReport.ReportPath = "Reports/DeletedChargeSlips.rdlc";
            }
            else if (i == "15")
            {
                //  lbltitle.Text = "Geographic Report";
                rptReport.LocalReport.ReportPath = "Reports/CreditedChargeSlips.rdlc";
            }
            else if (i == "17")
            {

            }
            else if (i == "18")
            {
                //  lbltitle.Text = "Geographic Report";
                rptReport.LocalReport.ReportPath = "Reports/BrokerTrend.rdlc";
            }

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Report.aspx", "displayDropdown", ex.Message);
        }

    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Resetcontrols();
            HideDropdown();
            int usertype = Convert.ToInt32(Session["UserType"]);
            if (usertype == 1)
            {
                trBroker.Visible = true;
            }
            else
                trBroker.Visible = false;
            if (ddlType.SelectedValue == "1" || ddlType.SelectedValue == "2" || ddlType.SelectedValue == "9" || ddlType.SelectedValue == "10" || ddlType.SelectedValue == "11" || ddlType.SelectedValue == "14" || ddlType.SelectedValue == "15" || ddlType.SelectedValue == "21")
            {
                // trBroker.Visible = true;
            }
            else if (ddlType.SelectedValue == "3")
            {
                // trBroker.Visible = true;
                trTenant.Visible = true;
            }
            else if (ddlType.SelectedValue == "4")
            {
                // trBroker.Visible = true;
                trOwner.Visible = true;
            }
            else if (ddlType.SelectedValue == "5")
            {
                // trBroker.Visible = true;
                trLandLord.Visible = true;
            }
            else if (ddlType.SelectedValue == "6")
            {
                // trBroker.Visible = true;
                trOwner.Visible = true;
                trTenant.Visible = true;
            }
            else if (ddlType.SelectedValue == "7")
            {
                // trBroker.Visible = true;
                trOwner.Visible = true;
                trValue.Visible = true;
                trLocation.Visible = true;
                lblLOCATION.Text = string.Empty;
            }
            else if (ddlType.SelectedValue == "8")
            {
                // trBroker.Visible = true;
                trLocation.Visible = true;
                trAddress.Visible = true;
                lblLOCATION.Text = "LOCATION";
            }
            else if (ddlType.SelectedValue == "12")
            {
                trBroker.Visible = true;
            }
            else if (ddlType.SelectedValue == "13")
            {
                //trBroker.Visible = true;
                trCity.Visible = true;
                trCounty.Visible = true;
                trState.Visible = true;
                trZipCode.Visible = true;

            }
            else if (ddlType.SelectedValue == "18")
            {
                //trBroker.Visible = true;
                trDate.Visible = true;
                trBroker.Visible = false;
                trMultipleBroker.Visible = true;
            }
            else if (ddlType.SelectedValue == "19")
            {
                //trBroker.Visible = true;
                trDesignationType.Visible = true;

            }
            else if (ddlType.SelectedValue == "20")
            {
                //trBroker.Visible = true;
                trDate.Visible = false;
                trBroker.Visible = false;
                trMultipleBroker.Visible = true;
                trBrokerList.Visible = true;
                cleanlistbox();
            }
            string i = ddlType.SelectedValue;
            // string ParaId = "http://dev.rhealtech.com/SegallChargeSlip/Pages";
            // ParaId = "http://localhost/SegallChargeSlip/Pages"; 
            // ParaId = Request.Url.GetLeftPart(UriPartial.Authority); 
            string ParaId = Request.Url.GetLeftPart(UriPartial.Path);
            int index = ParaId.LastIndexOf('/');
            string url = ParaId.Substring(0, index);
            string IsAdminUserType = "1";
            if (ddlBroker.SelectedValue == "" || ddlBroker.SelectedIndex == -1 || ddlBroker.SelectedValue == "0")
            {
                if (usertype == 1)
                {
                    IsAdminUserType = "1";
                }
                else
                {
                    IsAdminUserType = "2";
                }
            }
            else
            {
                IsAdminUserType = "2";
            }
            ReportParameter p1 = new ReportParameter("ParamUrl", url, false);
            ReportParameter p2 = new ReportParameter("IsAdminUserType", IsAdminUserType, false);
            rptReport.Reset();
            rptReport.LocalReport.DataSources.Clear();
            rptReport.LocalReport.EnableHyperlinks = true;
            if (i == "1")
            {
                lbltitle.Text = "";
                //rptReport.Reset();
                //rptReport.LocalReport.DataSources.Clear();
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataPaidInvoice);
                ds.DataSourceId = "ObjectDataPaidInvoice";
                rptReport.LocalReport.DataSources.Add(ds);
                rptReport.LocalReport.ReportPath = "Reports/Invoice.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
                rptReport.LocalReport.SubreportProcessing +=
                   new Microsoft.Reporting.WebForms.SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
            }
            if (i == "15")
            {
                lbltitle.Text = "";
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataCreditChargeSlip);
                ds.DataSourceId = "ObjectDataCreditChargeSlip";
                rptReport.LocalReport.DataSources.Add(ds);
                rptReport.LocalReport.ReportPath = "Reports/CreditedChargeSlips.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1 });
            }
            if (i == "2")
            {
                lbltitle.Text = "";
                //rptReport.Reset();
                //rptReport.LocalReport.DataSources.Clear();
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataOption);
                ds.DataSourceId = "ObjectDataOption";
                rptReport.LocalReport.DataSources.Add(ds);
                //rptReport.LocalReport.EnableHyperlinks = true;
                rptReport.LocalReport.ReportPath = "Reports/OptionsDue.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
            }
            if (i == "3")
            {
                lbltitle.Text = "";
                //rptReport.Reset();
                //rptReport.LocalReport.DataSources.Clear();
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataTenants);
                ds.DataSourceId = "ObjectDataTenants";
                rptReport.LocalReport.DataSources.Add(ds);
                //   rptReport.LocalReport.EnableHyperlinks = true;
                rptReport.LocalReport.ReportPath = "Reports/Tenant.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
                //   string mimeType, encoding, extension, deviceInfo;
                //   string[] streamids;Microsoft.Reporting.WebForms.Warning[] warnings;
                //   string format ="PDF"; //Desired format goes here (PDF, Excel, or Image)
                //   deviceInfo = "<DeviceInfo>'=" +"<SimplePageHeaders>True</SimplePageHeaders>" +
                //"</DeviceInfo>";
                //   byte[] bytes = rptReport.ServerReport.Render(format, deviceInfo, out mimeType, 
                //  out encoding, out extension, out streamids, out warnings);
                //   Response.Clear();

                //   if (format =="PDF")
                //   {
                //         Response.ContentType = "application/pdf";
                //         Response.AddHeader("Content-disposition", "filename=output.pdf");
                //       }
                //       else if (format == "Excel")
                //       {
                //         Response.ContentType ="application/excel";
                //         Response.AddHeader("Content-disposition", "filename=output.xls");
                //       }
                //       Response.OutputStream.Write(bytes, 0, bytes.Length);
                //       Response.OutputStream.Flush();
                //       Response.OutputStream.Close();
                //       Response.Flush();
                //       Response.Close();
                //string mimeType;
                //string encoding;
                //string fileNameExtension;
                //Warning[] warnings;//ReportViewer.ServerReport.Render
                //string[] streamids;
                //byte[] exportBytes = rptReport.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streamids, out warnings);
                //HttpContext.Current.Response.Buffer = true;
                //HttpContext.Current.Response.Clear();
                //HttpContext.Current.Response.ContentType = mimeType;
                //HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=ExportedReport." + fileNameExtension);
                //HttpContext.Current.Response.BinaryWrite(exportBytes);
                //HttpContext.Current.Response.Flush();
                //HttpContext.Current.Response.End();

            }
            if (i == "4")
            {
                lbltitle.Text = "";
                //rptReport.Reset();
                //rptReport.LocalReport.DataSources.Clear();
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataOwner);
                ds.DataSourceId = "ObjectDataOwner";
                rptReport.LocalReport.DataSources.Add(ds);
                // rptReport.LocalReport.EnableHyperlinks = true;
                rptReport.LocalReport.ReportPath = "Reports/OwnerReport.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
            }
            if (i == "5")
            {
                lbltitle.Text = "";
                //rptReport.Reset();
                //rptReport.LocalReport.DataSources.Clear();
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataLandlord);
                ds.DataSourceId = "ObjectDataLandlord";
                rptReport.LocalReport.DataSources.Add(ds);
                //  rptReport.LocalReport.EnableHyperlinks = true;
                rptReport.LocalReport.ReportPath = "Reports/Owners.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
            }
            if (i == "6")
            {
                lbltitle.Text = "";
                //rptReport.Reset();
                //rptReport.LocalReport.DataSources.Clear();
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataSquareFootLease);
                ds.DataSourceId = "ObjectDataSquareFootLease";
                rptReport.LocalReport.DataSources.Add(ds);
                // rptReport.LocalReport.EnableHyperlinks = true;
                rptReport.LocalReport.ReportPath = "Reports/SquareFootLease.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1 });
            }
            if (i == "7")
            {
                lbltitle.Text = "";
                //rptReport.Reset();
                //rptReport.LocalReport.DataSources.Clear();
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataTotalValueLease);
                ds.DataSourceId = "ObjectDataTotalValueLease";
                rptReport.LocalReport.DataSources.Add(ds);
                rptReport.LocalReport.ReportPath = "Reports/TotalValueLease.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1 });
            }
            if (i == "8")
            {
                lbltitle.Text = "";
                //rptReport.Reset();
                //rptReport.LocalReport.DataSources.Clear();
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataTotalValueSales);
                ds.DataSourceId = "ObjectDataTotalValueSales";
                rptReport.LocalReport.DataSources.Add(ds);
                rptReport.LocalReport.ReportPath = "Reports/TotalValueSales.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1 });
            }
            if (i == "9")
            {
                lbltitle.Text = "";
                //rptReport.Reset();
                //rptReport.LocalReport.DataSources.Clear();
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataGrossReceivables);
                ds.DataSourceId = "ObjectDataGrossReceivables";
                rptReport.LocalReport.DataSources.Add(ds);
                rptReport.LocalReport.ReportPath = "Reports/GrossReceivables.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
            }
            if (i == "10")
            {
                lbltitle.Text = "";
                //rptReport.Reset();
                //rptReport.LocalReport.DataSources.Clear();
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataOptionsContingent);
                ds.DataSourceId = "ObjectDataOptionsContingent";
                rptReport.LocalReport.DataSources.Add(ds);
                rptReport.LocalReport.ReportPath = "Reports/OptionContingent.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
            }
            if (i == "11")
            {
                lbltitle.Text = "";
                //rptReport.Reset();
                //rptReport.LocalReport.DataSources.Clear();
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataInHouseBroker);
                ds.DataSourceId = "ObjectDataInHouseBroker";
                rptReport.LocalReport.DataSources.Add(ds);
                rptReport.LocalReport.ReportPath = "Reports/InHouseCoBroker.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
            }
            if (i == "12")
            {
                lbltitle.Text = "Select Broker";
                CustomList objCL = new CustomList();
                Utility.FillDropDown(ref ddlBroker, objCL.GetOutsideBrokerList(), "Name", "UserID");
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataOutsideBroker);
                ds.DataSourceId = "ObjectDataOutsideBroker";
                rptReport.LocalReport.DataSources.Add(ds);
                rptReport.LocalReport.ReportPath = "Reports/OutsideBroker.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1 });
            }
            if (i == "13")
            {
                lbltitle.Text = "";
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataGeographic);
                ds.DataSourceId = "ObjectDataGeographic";
                rptReport.LocalReport.DataSources.Add(ds);
                rptReport.LocalReport.ReportPath = "Reports/Geographic.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1 });
            }
            if (i == "14")
            {
                lbltitle.Text = "";
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataDeleted);
                ds.DataSourceId = "ObjectDataDeleted";
                rptReport.LocalReport.DataSources.Add(ds);
                rptReport.LocalReport.ReportPath = "Reports/DeletedChargeSlips.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1 });
            }
            if (i == "17")
            {
                //rptReport.Visible = false;
                DealTransactions objDealTransactions = new DealTransactions();
                string BrokerID = ddlBroker.SelectedValue;
                if (BrokerID == "")
                {
                    //BrokerID = "0";
                    objDealTransactions.BrokerID = 0;
                }
                else
                {
                    objDealTransactions.BrokerID = Convert.ToInt32(BrokerID);
                }
                Session["BrokerID"] = objDealTransactions.BrokerID;
                DataSet ds = objDealTransactions.GetBrokerReport();
                gvBrokerReport.DataSource = ds;
                gvBrokerReport.DataBind();
                gvBrokerReport.Visible = true;
                rptReport.Visible = false;
            }
            if (i == "18")
            {
                lbltitle.Text = "";
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataBrokerTrend);
                ds.DataSourceId = "ObjectDataBrokerTrend";
                rptReport.LocalReport.DataSources.Add(ds);
                rptReport.LocalReport.ReportPath = "Reports/BrokerTrend.rdlc";
                //rptReport.LocalReport.SetParameters(new ReportParameter[] { p1 });
            }
            if (i == "19")
            {
                lbltitle.Text = "";
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataDealDesignationTypeList);
                ds.DataSourceId = "ObjectDataDealDesignationTypeList";
                rptReport.LocalReport.DataSources.Add(ds);
                rptReport.LocalReport.ReportPath = "Reports/DealDesignationTypeList.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
            }
            if (i == "20")
            {
                DealTransactions obj = new DealTransactions();
                DataSet ds = new DataSet();
                ds = obj.GetBrokerReportList(Convert.ToInt32(Session["UserId"].ToString()), null, Convert.ToInt32(drpCriteria1.SelectedValue), txtCritera1From.Text, txtCritera1To.Text, Convert.ToInt32(drpCriteria2.SelectedValue), txtCritera2From.Text, txtCritera2To.Text, Convert.ToInt32(drpCriteria3.SelectedValue), txtCritera3From.Text, txtCritera3To.Text);
                // ds=obj.GetBrokerReportList(Convert.ToInt32(Session[""].ToString(),BrokerId,Convert.ToInt32(),Conve
                pnlBrokerReport.Visible = true;
                grdBrokerReport.Visible = true;
                grdBrokerReport.DataSource = null;
                grdBrokerReport.DataBind();
                grdBrokerReport.DataSource = ds.Tables[0];
                grdBrokerReport.DataBind();
                rptReport.Visible = false;
                pnlBrokerReport.Visible = true;
                //DealTransactions objdealtransaction = new DealTransactions();
                //DataSet ds = objdealtransaction.GetBrokerReportList(Convert.ToInt32(Session["UserId"].ToString()),);
            }
            if (i == "21")
            {
                lbltitle.Text = "";
                ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataCLBBrokerDeals);
                ds.DataSourceId = "ObjectDataCLBBrokerDeals";
                rptReport.LocalReport.DataSources.Add(ds);
                rptReport.LocalReport.ReportPath = "Reports/CLBBrokerDeals.rdlc";
                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Report.aspx", "ddlType_SelectedIndexChanged", ex.Message);
        }


    }
    void LocalReport_SubreportProcessing(
            object sender,
            Microsoft.Reporting.WebForms.SubreportProcessingEventArgs e)
    {
        Session["CommissionDueId"] = null;
        //// get empID from the parameters
        int CommissionDueId = Convert.ToInt32(e.Parameters[0].Values[0]);

        //// remove all previously attached Datasources, since we want to attach a
        //// new one
        //e.DataSources.Clear();

        //// Retrieve employeeFamily list based on EmpID
        //var lines = BLL.GetOrders().Single(m => m.OrderNo == iOrderNo).Lines;

        //// add retrieved dataset or you can call it list to data source
        //e.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource()
        //{
        //    Name = "LineDS",
        //    Value = lines
        //});
        Session["CommissionDueId"] = CommissionDueId;
        ReportDataSource ds = new ReportDataSource("DataSet1", ObjectDataPaidInvoice_SubReport);
        ds.DataSourceId = "ObjectDataPaidInvoice_SubReport";
        e.DataSources.Add(ds);
    }
    protected void btnRun_Click(object sender, EventArgs e)
    {
        Session["MultipleBrokerId"] = null;
        string i = ddlType.SelectedValue;
        if (!string.IsNullOrEmpty(i) && (i == "1" || i == "2" || i == "3" || i == "4" || i == "5" || i == "9" || i == "10" || i == "11" || i == "19" ||i=="21"))
        {
            if (!string.IsNullOrEmpty(i))
            {
                int usertype = Convert.ToInt32(Session["UserType"]);
                string ParaId = Request.Url.GetLeftPart(UriPartial.Path);
                int index = ParaId.LastIndexOf('/');
                string url = ParaId.Substring(0, index);
                string IsAdminUserType = "1";
                if (ddlBroker.SelectedValue == "" || ddlBroker.SelectedIndex == -1 || ddlBroker.SelectedValue == "0")
                {
                    if (usertype == 1)
                    {
                        IsAdminUserType = "1";
                    }
                    else
                    {
                        IsAdminUserType = "2";
                    }
                }
                else
                {
                    IsAdminUserType = "2";
                }
                ReportParameter p1 = new ReportParameter("ParamUrl", url, false);
                ReportParameter p2 = new ReportParameter("IsAdminUserType", IsAdminUserType, false);

                rptReport.LocalReport.SetParameters(new ReportParameter[] { p1, p2 });
                rptReport.Visible = true;
                gvBrokerReport.Visible = false;
                pnlBrokerReport.Visible = false;
            }
        }
        if (i == "1")
        {
            rptReport.LocalReport.SubreportProcessing +=
                 new Microsoft.Reporting.WebForms.SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
        }
        if (i == "12")
        {
            string j = ddlBroker.SelectedValue;
            if (string.IsNullOrEmpty(j))
            {
                lbltitle.Text = "Select Broker first";
                lbltitle.ForeColor = System.Drawing.Color.Red;
            }
            else
            {

                rptReport.LocalReport.Refresh();
                rptReport.Visible = true;
                gvBrokerReport.Visible = false;
            }
        }
        else if (i == "17")//Added by Shishir 29-04-2016
        {

            //string j = ddlBroker.SelectedValue;
            //if (string.IsNullOrEmpty(j))
            //{
            //    lbltitle.Text = "Select Broker first";
            //    lbltitle.ForeColor = System.Drawing.Color.Red;
            //}
            //else
            //{
            //lbltitle.Text = "";
            DealTransactions objDealTransactions = new DealTransactions();
            string BrokerID = ddlBroker.SelectedValue;
            if (BrokerID == "")
            {
                //BrokerID = "0";
                objDealTransactions.BrokerID = 0;
            }
            else
            {
                objDealTransactions.BrokerID = Convert.ToInt32(BrokerID);
            }



            Session["BrokerID"] = objDealTransactions.BrokerID;
            DataSet ds = objDealTransactions.GetBrokerReport();
            gvBrokerReport.DataSource = ds;
            gvBrokerReport.DataBind();
            gvBrokerReport.Visible = true;
            rptReport.Visible = false;
            //}
        }     
        else
        {
            string BrokerId = "";
            if (i == "18" || i=="20")
            {
                
                foreach (System.Web.UI.WebControls.ListItem item in DropDownCheckBoxes1.Items)
                {
                    if (item.Selected)
                    {
                        BrokerId = BrokerId + item.Value + ",";

                    }
                }
                if (!string.IsNullOrEmpty(BrokerId))
                {
                    BrokerId = BrokerId.Substring(0, BrokerId.Length - 1);
                }
                Session["MultipleBrokerId"] = BrokerId;
                CustomList objCL = new CustomList();
                DataTable dtinternalbroker = new DataTable();
                dtinternalbroker = objCL.GetBrokerList();
                if (dtinternalbroker != null && dtinternalbroker.Rows.Count > 0)
                {
                    DropDownCheckBoxes1.DataSource = dtinternalbroker;
                    DropDownCheckBoxes1.DataValueField = "UserID";
                    DropDownCheckBoxes1.DataTextField = "Name";
                    DropDownCheckBoxes1.DataBind();
                }
                foreach (System.Web.UI.WebControls.ListItem item in DropDownCheckBoxes1.Items)
                {
                    //item.Attributes.Add("style", "color:red");
                    item.Attributes.Add("class", "RedColor");

                }
                DataTable dtnoninternalBroker = new DataTable();
                dtnoninternalBroker = objCL.GetOutsideBrokerList();
                if (dtnoninternalBroker != null && dtnoninternalBroker.Rows.Count > 0)
                {
                    for (int j = 0; j < dtnoninternalBroker.Rows.Count; j++)
                    {
                        System.Web.UI.WebControls.ListItem objli = new System.Web.UI.WebControls.ListItem();
                        objli.Value = dtnoninternalBroker.Rows[j]["UserID"].ToString();
                        objli.Text = dtnoninternalBroker.Rows[j]["Name"].ToString();
                        DropDownCheckBoxes1.Items.Add(objli);
                    }
                }
                foreach (System.Web.UI.WebControls.ListItem item in DropDownCheckBoxes1.Items)
                {
                    string[] str1 = BrokerId.Split(',');
                    for (int k = 0; k < str1.Length; k++)
                    {
                        if (item.Value == str1[k])
                        {
                            item.Selected = true;
                        }
                    }
                }
            }
            gvBrokerReport.Visible = false;
            if (i == "18")
            {
                rptReport.LocalReport.Refresh();
                rptReport.Visible = true;               
            }
            else if (i == "20")
            {
                DealTransactions obj = new DealTransactions();
                DataSet ds = new DataSet();
                ds=obj.GetBrokerReportList(Convert.ToInt32(Session["UserId"].ToString()),BrokerId,Convert.ToInt32(drpCriteria1.SelectedValue),txtCritera1From.Text,txtCritera1To.Text,Convert.ToInt32(drpCriteria2.SelectedValue),txtCritera2From.Text,txtCritera2To.Text,Convert.ToInt32(drpCriteria3.SelectedValue),txtCritera3From.Text,txtCritera3To.Text);
               // ds=obj.GetBrokerReportList(Convert.ToInt32(Session[""].ToString(),BrokerId,Convert.ToInt32(),Conve
                pnlBrokerReport.Visible = true;
                grdBrokerReport.Visible = true;
                grdBrokerReport.DataSource = null;
                grdBrokerReport.DataBind();
                grdBrokerReport.DataSource = ds.Tables[0];
                grdBrokerReport.DataBind();
                rptReport.Visible = false;
                pnlBrokerReport.Visible = true;
            }
           
            //Warning[] warnings;
            //string[] streamids;
            //string mimeType;
            //string encoding;
            //string extension;
            //string deviceInfo;

            //deviceInfo = "<DeviceInfo><SimplePageHeaders>True</SimplePageHeaders></DeviceInfo>";

            //byte[] bytes = rptReport.LocalReport.Render(
            //   "PDF", null, out mimeType, out encoding, out extension,
            //   out streamids, out warnings);

            //FileStream fs =
            //  new FileStream(@"d:\output.pdf", FileMode.Create);
            //fs.Write(bytes, 0, bytes.Length);
            //fs.Close();

        }
        bindSelectedList();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Resetcontrols();
    }
    protected void gvBrokerReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void drpCriteria2_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindBrokerReport();        
    }
    protected void btnFilterDateRange_Click(object sender, EventArgs e)
    {
        BindBrokerReport();       
                
    }
    private void BindBrokerReport()
    {
        string BrokerId = "";
        foreach (System.Web.UI.WebControls.ListItem item in DropDownCheckBoxes1.Items)
        {
            if (item.Selected)
            {
                BrokerId = BrokerId + item.Value + ",";

            }
        }
        if (!string.IsNullOrEmpty(BrokerId))
        {
            BrokerId = BrokerId.Substring(0, BrokerId.Length - 1);
        }
        DealTransactions obj = new DealTransactions();
        DataSet ds = new DataSet();
        ds = obj.GetBrokerReportList(Convert.ToInt32(Session["UserId"].ToString()), BrokerId, Convert.ToInt32(drpCriteria1.SelectedValue), txtCritera1From.Text, txtCritera1To.Text, Convert.ToInt32(drpCriteria2.SelectedValue), txtCritera2From.Text, txtCritera2To.Text, Convert.ToInt32(drpCriteria3.SelectedValue), txtCritera3From.Text, txtCritera3To.Text);
        pnlBrokerReport.Visible = true;
        grdBrokerReport.Visible = true;
        grdBrokerReport.DataSource = null;
        grdBrokerReport.DataBind();
        grdBrokerReport.DataSource = ds.Tables[0];
        grdBrokerReport.DataBind();
        rptReport.Visible = false;
        pnlBrokerReport.Visible = true;
    }
    protected void grdBrokerReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                Label lblCriteria1 = (Label)e.Row.FindControl("lblCriteria1");

                //if (drpCriteria1.SelectedValue == "1")
                //{
                //    lblCriteria1.Text = "This fiscal year";
                //}
                //else if (drpCriteria1.SelectedValue == "2")
                //{
                //    lblCriteria1.Text = "Last fiscal year";
                //}
                //else if (drpCriteria1.SelectedValue == "3")
                //{
                //    lblCriteria1.Text = "Year to Date";
                //}
                //else if (drpCriteria1.SelectedValue == "4")
                //{
                //    lblCriteria1.Text = "Previous month";
                //}
                //else if (drpCriteria1.SelectedValue == "5")
                //{
                //    lblCriteria1.Text = "Calendar Period Range";
                //    lblCriteria1.Text = txtCritera1From.Text + "To" + txtCritera1To.Text;
                //}

                if(drpCriteria1.SelectedValue!="0")
                {
                    
                    if (drpCriteria1.SelectedValue=="5" && txtCritera1From.Text!="" && txtCritera1To.Text!="")
                    {                        
                        lblCriteria1.Text = txtCritera1From.Text + " To " + txtCritera1To.Text;                        
                    }
                    else
                    {
                        lblCriteria1.Text = drpCriteria1.SelectedItem.ToString();
                    }                    
                }
            }

            if (e.Row.RowType == DataControlRowType.Header)
            {
                Label lblCriteria2 = (Label)e.Row.FindControl("lblCriteria2");

                //if (drpCriteria2.SelectedValue == "1")
                //{
                //    lblCriteria2.Text= "This fiscal year";
                //}
                //else if (drpCriteria2.SelectedValue == "2")
                //{
                //    lblCriteria2.Text = "Last fiscal year";
                //}
                //else if (drpCriteria2.SelectedValue == "3")
                //{
                //    lblCriteria2.Text = "Year to Date";
                //}
                //else if (drpCriteria2.SelectedValue == "4")
                //{
                //    lblCriteria2.Text = "Previous month";
                //}
                //else if (drpCriteria2.SelectedValue == "5")
                //{
                //    lblCriteria2.Text = "Calendar Period Range";
                //}

                if (drpCriteria2.SelectedValue != "0")
                {
                    
                    if (drpCriteria2.SelectedValue == "5" && txtCritera2From.Text!="" && txtCritera2To.Text!="")
                    {
                        lblCriteria2.Text = txtCritera2From.Text + " To " + txtCritera2To.Text;
                    }
                    else
                    {
                        lblCriteria2.Text = drpCriteria2.SelectedItem.ToString();
                    }                    
                }
            }

            if (e.Row.RowType == DataControlRowType.Header)
            {
                Label lblCriteria3 = (Label)e.Row.FindControl("lblCriteria3");

                //if (drpCriteria3.SelectedValue == "1")
                //{
                //    lblCriteria3.Text = "This fiscal year";
                //}
                //else if (drpCriteria3.SelectedValue == "2")
                //{
                //    lblCriteria3.Text = "Last fiscal year";
                //}
                //else if (drpCriteria3.SelectedValue == "3")
                //{
                //    lblCriteria3.Text = "Year to Date";
                //}
                //else if (drpCriteria3.SelectedValue == "4")
                //{
                //    lblCriteria3.Text = "Previous month";
                //}
                //else if (drpCriteria3.SelectedValue == "5")
                //{
                //    lblCriteria3.Text = "Calendar Period Range";
                //}

                if (drpCriteria3.SelectedValue != "0")
                {
                    
                    if (drpCriteria3.SelectedValue == "5" && txtCritera3From.Text!="" && txtCritera3To.Text!="")
                    {
                        lblCriteria3.Text = txtCritera3From.Text + " To " + txtCritera3To.Text;
                    }
                    else
                    {
                        lblCriteria3.Text = drpCriteria3.SelectedItem.ToString();
                    }                    
                }
            }

                if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (drpCriteria1.SelectedValue == "0")
                {
                    HtmlTable rw = (HtmlTable)e.Row.FindControl("classCriterai1");
                    rw.Style.Add("display", "none");
                }
                if (drpCriteria2.SelectedValue == "0")
                {
                    HtmlTable rw = (HtmlTable)e.Row.FindControl("classCriterai2");
                    rw.Style.Add("display", "none");
                }
                if (drpCriteria3.SelectedValue == "0")
                {
                    HtmlTable rw = (HtmlTable)e.Row.FindControl("classCriterai3");
                    rw.Style.Add("display", "none");
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Reports.aspx.cs", "grdBrokerReport_RowDataBound", ex.Message);
        }
    }

    protected void imgExport_Click(object sender, ImageClickEventArgs e)
    {
        if (drpCriteria1.SelectedValue != "0" || drpCriteria2.SelectedValue != "0" || drpCriteria3.SelectedValue != "0")
        {
        Response.ClearContent();
        Response.Buffer = true;
        //Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Customers.xls"));

        Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        GridViewRow HeaderRow = grdBrokerReport.HeaderRow;
        HeaderRow.Height = 20;
        HeaderRow.Style.Add("font-size", "12px");
        GridViewRow FooterRow = grdBrokerReport.FooterRow;
        FooterRow.Height = 20;
        FooterRow.Style.Add("font-size", "12px");

        grdBrokerReport.Style.Add("font-family", "Verdana");
        grdBrokerReport.Style.Add("font-size", "7px");

        Response.AddHeader("content-disposition", "attachment;filename=BrokerComparisonReport.xls");
        grdBrokerReport.RenderControl(htw);
        Response.Write(sw.ToString());
        //tdNotes.Visible = false;
        Response.Flush();
        Response.End();
        }
    }

    protected void imgPDF_Click(object sender, ImageClickEventArgs e)
    {
        if (drpCriteria1.SelectedValue != "0" || drpCriteria2.SelectedValue != "0" || drpCriteria3.SelectedValue != "0")
        {
            //grdBrokerReport.GridLines = GridLines.None;              
            

            Response.ClearContent();
            Response.Buffer = true;
            //grdBrokerReport.BorderWidth = 5;

            //Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf");
            Response.ContentType = "application/pdf";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            GridViewRow HeaderRow = grdBrokerReport.HeaderRow;
            HeaderRow.Height = 20;
            HeaderRow.Style.Add("font-size", "12px");
            GridViewRow FooterRow = grdBrokerReport.FooterRow;
            FooterRow.Height = 20;
            FooterRow.Style.Add("font-size", "12px");

            grdBrokerReport.Style.Add("font-family", "Verdana");
            grdBrokerReport.Style.Add("font-size", "7px");
            //grdBrokerReport.Style.Add("text-decoration", "none");

            Response.AddHeader("content-disposition", "attachment;filename=BrokerComparisonReport.pdf");
            grdBrokerReport.RenderControl(hw);

            Document pdfDoc = new Document(new Rectangle(612f, 792f), 36f, 36f, 36f, 36f);

            //grdBrokerReport.Style.Add("border", "5px");


            //PdfPTable table = new PdfPTable(10);
            //table.DefaultCell.Border= Rectangle.NO_BORDER;
            //table.DefaultCell.Border = 0;
            
            //PdfPCell cell = new PdfPCell();
            ////table.DefaultCell.Border = Rectangle.NO_BORDER;

            //table.DefaultCell.Border = 0;
            //cell.Border = Rectangle.NO_BORDER;
            //cell.DisableBorderSide(1);

            //table.Border = PdfPCell.NO_BORDER;
            //table.DefaultCell.Border = Rectangle.NO_BORDER;
            //cell.setBorder(Rectangle.NO_BORDER);
            //table.getDefaultCell().setBorder(Rectangle.NO_BORDER);              


            //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            pdfDoc.HtmlStyleClass = "SetWidth";
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            StringReader sr = new StringReader(sw.ToString());
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            //tdNotes.Visible = false;
            Response.End();
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void drpCriteria1_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }    

    protected void drpCriteria3_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindBrokerReport();        
    }
  /// <summary>
    /// Add Selected BrokerName in List
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void bindSelectedList() 
    {
       
            List<string> SelectedBroker = new List<string>();
            foreach (System.Web.UI.WebControls.ListItem Item in DropDownCheckBoxes1.Items)
            {
                if (Item.Selected)
                {
                    SelectedBroker.Add(Item.Text);
                }
            }

            lstBrokerList.DataSource = SelectedBroker;
            lstBrokerList.DataBind();
       
    }

    /// <summary>
    /// Clear List Box
    /// </summary>
    
    private void cleanlistbox()
    {
        lstBrokerList.Items.Clear();
    }

}