﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true"
    CodeFile="Billed.aspx.cs" Inherits="Pages_Billed" EnableEventValidation="false" %>

<%--<%@ Register Src="UserControl/ucBilledTab.ascx" TagName="ucBilledTab" TagPrefix="uc1" %>--%>
<%@ Register Src="~/UserControl/ucBilledTab.ascx" TagName="ucBilledTab" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="contHeadCont" ContentPlaceHolderID="cntHeadTag" runat="server">


</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../Js/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Navigate() {

            var Index = 1;
            $.ajax({
                type: "POST",
                url: "Billed.aspx/GetChargeSlipDetails",
                data: '{RowIndx: "' + Index + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    window.open("../Pages/NewCharge.aspx", "_self");
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
            
            return false;
        }
        function NavigateSplitCalculator() {

            var Index = 1;
            $.ajax({
                type: "POST",
                url: "Billed.aspx/GetChargeSlipDetails",
                data: '{RowIndx: "' + Index + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    window.open("../Pages/NewChargeCommissionSpitCalCulator.aspx", "_self");
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
            
            return false;
        }
        function Reprint() {
          
            $.ajax({
                type: "POST",
                url: "../Admin/Customers.aspx/GetChargeSlipFile",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    window.open(response.d, '_blank');
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
            return false;
        }
    </script>
    <table style="width: 100%;">
        <tr><%--style="padding-top: 0.5em; padding-right:5px; text-align: right; width: 80%; color: #F68620; text-shadow: initial; font-weight: 600; font-size: 20px;"--%>
            <td></td>
            <td class="mainPageTitle">BILLED CHARGE SLIPS
            </td>
        </tr>
         <tr>
            <td colspan="2">
                <hr />
            </td>
        </tr>
    </table>
    <%--<hr style="width: 99%" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <link href="../Styles/SiteTEST.css" rel="stylesheet" />
    <uc1:ucBilledTab ID="ucBilledTab1" runat="server" />
</asp:Content>

