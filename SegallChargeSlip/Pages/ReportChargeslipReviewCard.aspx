﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportChargeslipReviewCard.aspx.cs"  EnableEventValidation="false" Inherits="Pages_ReportChargeslipReviewCard" %>
<%@ Register Src="~/UserControl/ucChargeDetailCard.ascx" TagName="ucChargeDetailCard" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/ucPaymentHistoryDetail.ascx" TagPrefix="uc1" TagName="ucPaymentHistoryDetail" %>
<link href="../Styles/SiteTEST.css" rel="stylesheet" />
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  
</head>
<body>
        <script type="text/javascript">
            //added by jaywanti on 8-7-2014

            function SaveIndividualPaymentEdit(PaymentHistoryId, ChargeslipId, NewAmount, ReasonToEdit) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url: "../Admin/Customers.aspx/SaveIndividualBrokerPaymentEdit",
                    data: '{PaymentHistoryId: "' + PaymentHistoryId + '",ChargeslipId: "' + ChargeslipId + '",NewAmount: "' + NewAmount + '",ReasonToEdit: "' + ReasonToEdit + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        //alert('p');
                        if (response.d == "") {
                            debugger;
                            document.getElementById('<%=btnRefreshPayment.ClientID%>').click();

                    }
                    else {

                        alert(response.d);
                        return false;

                    }
                },
                failure: function (response) {

                    alert(response.d);
                    return false;
                }
            });
            $("#<%=hdnChargeslipId.ClientID%>").val(ChargeslipId);

                 //alert('hi');
            }
            function imgEditbtn_ClientAmountClick(imgbtn) {
                try {

                    var ttd = imgbtn.parentElement
                    var childLength = ttd.childNodes.length;
                    for (var i = 0; i < childLength; i++) {
                        if (ttd.childNodes[i].type == 'text') {

                            ttd.childNodes[i].setAttribute("Style", "display:inline-grid;width:120px;");
                            ttd.childNodes[i].value = ttd.childNodes[5].innerHTML;
                        }

                    }
                    imgbtn.setAttribute("Style", "display:none;");
                    ttd.childNodes[7].setAttribute("Style", "display:none;");
                    ttd.childNodes[9].setAttribute("Style", "display:inline-grid;width:13px;height:13px;");
                    ttd.childNodes[5].setAttribute("Style", "display:none;");
                    ttd.childNodes[3].setAttribute("Style", "display:none;");
                    ttd.childNodes[11].setAttribute("Style", "display:inline-grid;width:12px;height:12px;");

                }
                catch (exce) {
                }
                return false;

            }
            function imgSavebtn_ClientAmountClick(imgbtn) {
                try {

                    debugger;
                    var ttd = imgbtn.parentElement
                    var childLength = ttd.childNodes.length;
                    var textValue;
                    for (var i = 0; i < childLength; i++) {
                        if (ttd.childNodes[i].type == 'text') {
                            textValue = ttd.childNodes[i].value;
                            ttd.childNodes[i].setAttribute("Style", "display:none;");
                            break;
                        }

                    }

                    var ttr = ttd.parentElement;


                    var PaymentHistoryID = ttd.childNodes[13].innerHTML; // ttr.cells[1].textContent;
                    var lblChargeslipid = ttd.childNodes[15].innerHTML;

                    var hdnChargeslipId = document.getElementById('<%=hdnChargeslipId.ClientID%>');
                hdnChargeslipId.value = lblChargeslipid;
                imgbtn.setAttribute("Style", "display:none;");
                var OldValue = ttd.childNodes[3].innerHTML;
                ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[7].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[9].setAttribute("Style", "display:none;");
                ttd.childNodes[11].setAttribute("Style", "display:none;");



                    //if (textValue.trim() != '') {
                ttd.childNodes[5].innerHTML = textValue;

                $.ajax({
                    async: true,
                    type: "POST",
                    url: "../Admin/Customers.aspx/SavePaymentAmount",
                    data: '{Paymentd: "' + PaymentHistoryID + '",Amount: "' + textValue.replace(/\$/g, '') + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        //alert('p');
                        if (response.d == "") {
                            debugger;
                            document.getElementById('<%=btnRefreshPayment.ClientID%>').click();

                        }
                        else {
                            ttd.childNodes[5].innerHTML = response.d;
                            alert('Payment Amount cannot be greater than Commission Due');

                        }
                    },
                    failure: function (response) {

                        alert(response.d);
                        return false;
                    }
                });
                //}
                //else {
                //    //ttd.childNodes[3].innerHTML = OldValue;
                //    ttd.childNodes[3].innerHTML = textValue;
                //}
            }
            catch (ex) {
            }
            return false;
            }
            function imgCancelAmountClick(imgbtn) {
                var ttd = imgbtn.parentElement
                var childLength = ttd.childNodes.length;
                var textValue;
                for (var i = 0; i < childLength; i++) {
                    if (ttd.childNodes[i].type == 'text') {

                        ttd.childNodes[i].setAttribute("Style", "display:none;");
                        break;
                    }

                }
                imgbtn.setAttribute("Style", "display:none;");
                var OldValue = ttd.childNodes[3].innerHTML;
                ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[7].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[9].setAttribute("Style", "display:none;");
                ttd.childNodes[11].setAttribute("Style", "display:none;");
                return false;
            }
            function imgCancelClick(imgbtn) {
                try {
                    var ttd = imgbtn.parentElement
                    var childLength = ttd.childNodes.length;
                    var textValue;
                    for (var i = 0; i < childLength; i++) {
                        if (ttd.childNodes[i].type == 'text') {
                            textValue = ttd.childNodes[i].value;
                            ttd.childNodes[i].setAttribute("Style", "display:none;");
                            break;
                        }

                    }
                    imgbtn.setAttribute("Style", "display:none;");
                    ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
                    ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
                    ttd.childNodes[7].setAttribute("Style", "display:none;");
                }
                catch (ex) {
                }
                return false;

            }
            //
            function imgEditbtn_ClientClick(imgbtn) {
                try {

                    var ttd = imgbtn.parentElement
                    var childLength = ttd.childNodes.length;
                    for (var i = 0; i < childLength; i++) {
                        if (ttd.childNodes[i].type == 'text') {
                            ttd.childNodes[i].setAttribute("Style", "display:inline-grid;width:120px;");
                            ttd.childNodes[i].value = ttd.childNodes[3].innerHTML
                        }

                    }
                    imgbtn.setAttribute("Style", "display:none;");
                    ttd.childNodes[5].setAttribute("Style", "display:none;");
                    ttd.childNodes[7].setAttribute("Style", "display:inline-grid;width:13px;height:13px;");
                    ttd.childNodes[9].setAttribute("Style", "display:inline-grid;width:12px;height:12px;");
                    ttd.childNodes[3].setAttribute("Style", "display:none;");

                }
                catch (exce) {
                }
                return false;

            }
            function imgSavebtn_ClientClick(imgbtn) {
                try {

                    var ttd = imgbtn.parentElement
                    var childLength = ttd.childNodes.length;
                    var textValue;
                    for (var i = 0; i < childLength; i++) {
                        if (ttd.childNodes[i].type == 'text') {
                            textValue = ttd.childNodes[i].value;
                            ttd.childNodes[i].setAttribute("Style", "display:none;");
                            break;
                        }

                    }

                    var ttr = ttd.parentElement;


                    var PaymentHistoryID = ttd.childNodes[11].innerHTML; // ttr.cells[1].textContent;

                    imgbtn.setAttribute("Style", "display:none;");
                    var OldValue = ttd.childNodes[3].innerHTML;
                    ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
                    ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
                    ttd.childNodes[7].setAttribute("Style", "display:none;");
                    ttd.childNodes[9].setAttribute("Style", "display:none;");



                    //if (textValue.trim() != '') {
                    ttd.childNodes[3].innerHTML = textValue;
                    $.ajax({
                        type: "POST",
                        url: "Customers.aspx/SaveBrokerCheckNo",
                        data: '{PaymentHistoryId: "' + PaymentHistoryID + '",CheckNo: "' + textValue + '" }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                    //}
                    //else {
                    //    //ttd.childNodes[3].innerHTML = OldValue;
                    //    ttd.childNodes[3].innerHTML = textValue;
                    //}
                }
                catch (ex) {
                }
                return false;
            }
       </script>
    <script type="text/javascript">
        //$(document).ready(function hideEdit() {
        //    document.getElementById("ucChargeDetailCard1_imgEditForm2").style.display = "none";
        //    document.getElementById("ucChargeDetailCard1_imgBaseCommision").style.display = "none";
        //    return false;
        //});
        //function hideEdit() {
        //    ucChargeDetailCard1_imgEditForm2
        //    var Parent = "ucChargeDetailCard1_imgEditForm2";
        //    $("#" + Parent).Hide();
        //    //alert('hide');
        //    return false;
        //}
        function display() {
            //debugger;
            document.getElementById('<%=divChargeDetailCard.ClientID%>').style.display = "block";
           return false;
       }

       function Hide() {
           //debugger;
           document.getElementById('<%=divChargeDetailCard.ClientID%>').style.display = "none";
            document.getElementById('<%=hdnPopup.ClientID %>').value = "0";
            return false;
        }

        function displayPayHistory() {
            //debugger;
            document.getElementById('<%=divPaymentHistory.ClientID%>').style.display = "block";

        //alert(document.getElementById('<%=divPaymentHistory.ClientID%>'));

    }

        function HidePayHistory() {
            //debugger;
            document.getElementById('<%=divPaymentHistory.ClientID%>').style.display = "none";
        }

        function hidepopup() {

            var vhdnpopup = document.getElementById('<%=hdnPopup.ClientID %>');

        if (vhdnpopup.value == "1") {
            HidePayHistory();
            vhdnpopup.value = "0";
            return display();
        }
        else {
            HidePayHistory();
        }
    }
   </script>
    
    <form id="form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div id="divChargeDetailCard" style="width:100%; height:100%;" runat="server">
     <uc1:ucChargeDetailCard ID="ucChargeDetailCard1" runat="server" />
    </div>
        <div id="divPaymentHistory" style="display:none" runat="server">
        <uc1:ucPaymentHistoryDetail runat="server" ID="ucPaymentHistoryDetail1" />
        </div>
          <asp:Button ID="btnRefreshPayment" runat ="server" text="Save Payment Amount" OnClick="btnRefreshPayment_Click" Style="display:none;"/>
        <asp:HiddenField ID="hdnChargeslipId" runat ="server" Value="0" />
         <asp:HiddenField ID="hdnPopup" runat="server" />
    </form>
</body>
</html>
