﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using SegallChargeSlipBll;

public partial class Pages_ChargeSlipNewsForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {              
                string strChargeslipid = string.Empty;
                strChargeslipid = Request.QueryString["ChargeslipId"];
                if (!string.IsNullOrEmpty(strChargeslipid))
                {
                    ChargeSlipNewsForms objChargeSlipNewsForms = new ChargeSlipNewsForms();
                    objChargeSlipNewsForms.ChargeSlipID = Convert.ToInt32(strChargeslipid);                   
                    //objChargeSlipNewsForms.ChargeSlipID = 131667;
                    objChargeSlipNewsForms.Load();
                    //hdnNewsFormID.Value = objChargeSlipNewsForms.NewsFormsID.ToString();
                    txtBrokerName.Text = objChargeSlipNewsForms.BrokerName.ToString();
                    txtDealName.Text = objChargeSlipNewsForms.DealName.ToString();
                    txtChargeDate.Text = objChargeSlipNewsForms.ChargeDate.ToShortDateString();
                    string Address = "";
                    if (!String.IsNullOrEmpty(objChargeSlipNewsForms.LocShoppingCenter))
                    {
                        Address += objChargeSlipNewsForms.LocShoppingCenter + ", ";
                    }
                    if (!String.IsNullOrEmpty(objChargeSlipNewsForms.LocAddress1))
                    {
                        Address += objChargeSlipNewsForms.LocAddress1 + ", ";
                    }
                    if (!String.IsNullOrEmpty(objChargeSlipNewsForms.LocAddress2))
                    {
                        Address += objChargeSlipNewsForms.LocAddress2 + ", ";
                    }
                    if (!String.IsNullOrEmpty(objChargeSlipNewsForms.LocCity))
                    {
                        Address += objChargeSlipNewsForms.LocCity + ", ";
                    }
                    if (!String.IsNullOrEmpty(objChargeSlipNewsForms.LocState))
                    {
                        Address += objChargeSlipNewsForms.LocState + " ";
                    }
                    if (!String.IsNullOrEmpty(objChargeSlipNewsForms.LocZipCode))
                    {
                        Address += objChargeSlipNewsForms.LocZipCode;
                    }
                    txtAddress.Text = Address;
                    txtTenant.Text = objChargeSlipNewsForms.TenantName.ToString();
                    txtLandlord.Text = objChargeSlipNewsForms.LandlordName.ToString();
                    txtSize.Text = objChargeSlipNewsForms.Size.ToString();
                    ddlSize.SelectedValue = objChargeSlipNewsForms.UnitTypeValue.ToString();

                    txtBrokerName.ReadOnly = true;
                    txtDealName.ReadOnly = true;
                    txtChargeDate.ReadOnly = true;
                    txtAddress.ReadOnly = true;
                    txtTenant.ReadOnly = true;
                    txtLandlord.ReadOnly = true;
                    txtSize.ReadOnly = true;
                    ddlSize.Enabled = false;
                    txtAnnounceDate.Attributes.Add("readonly", "readonly");//TO only select date from calendar extender, can't type, paste in textbox

                    Int64 UserId = Convert.ToInt64(Session["UserID"].ToString());
                    DealTransactions objDealTransactions = new DealTransactions();
                    objDealTransactions.ChargeSlipID =Convert.ToInt64(strChargeslipid);
                    DataSet ds = objDealTransactions.GetChargeSlipDetails(UserId);

                    if(ds != null)
                    {
                        if(ds.Tables[0].Rows.Count > 0)
                        {
                            if (Convert.ToString(ds.Tables[0].Rows[0]["DealType"]) == "SALE")
                            {
                                lblStatusOfLease.Text = "Contract Status :"; 
                            }
                            else
                            {
                                lblStatusOfLease.Text = "Status of Lease :";
                            }
                        }
                    }
                }
            }
            
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ChargeSlipNewsForm.aspx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            ChargeSlipNewsForms objChargeSlipNewsForms = new ChargeSlipNewsForms();
            if (Request.QueryString["ChargeslipId"] != null)
            {
                objChargeSlipNewsForms.ChargeSlipID = Convert.ToInt32(Request.QueryString["ChargeslipId"]);
            }
            //if (hdnNewsFormID.Value != "0")
            if (objChargeSlipNewsForms.NewsFormsID != 0)
            {
                //objChargeSlipNewsForms.NewsFormsID = Convert.ToInt32(hdnNewsFormID.Value);
                objChargeSlipNewsForms.ModifiedBy = Convert.ToInt32(Session["UserID"]);
                objChargeSlipNewsForms.ModifiedDate = DateTime.Now;
            }
            else
            {
                objChargeSlipNewsForms.CreatedBy = Convert.ToInt32(Session["UserID"]);
                objChargeSlipNewsForms.CreatedDate = DateTime.Now;
            }  
            objChargeSlipNewsForms.BrokerName = txtBrokerName.Text.ToString();
            objChargeSlipNewsForms.DealName = txtDealName.Text.ToString();
            objChargeSlipNewsForms.ChargeDate = Convert.ToDateTime(txtChargeDate.Text);
            objChargeSlipNewsForms.Address = txtAddress.Text.ToString();
            objChargeSlipNewsForms.TenantName = txtTenant.Text.ToString();
            objChargeSlipNewsForms.LandlordName = txtLandlord.Text.ToString();
            objChargeSlipNewsForms.Size = Convert.ToDecimal(txtSize.Text);
            objChargeSlipNewsForms.UnitValue = 1;
            //here the value is hardcoded to 1 as we are dealing with Size. 2 is for Time.
            objChargeSlipNewsForms.UnitTypeValue = Convert.ToInt32(ddlSize.SelectedValue);
            objChargeSlipNewsForms.StatusOfLease = ddlStatusofLease.SelectedValue.ToString();
            if(!string.IsNullOrEmpty(txtAnnounceDate.Text))
            objChargeSlipNewsForms.AnnounceDate = Convert.ToDateTime(txtAnnounceDate.Text);
            objChargeSlipNewsForms.Title = txtTitle.Text.ToString();
            objChargeSlipNewsForms.Description = txtDescription.Text.ToString();
            objChargeSlipNewsForms.TenantRepresentedBy = txtTenantRepresentedBy.Text.ToString();
            objChargeSlipNewsForms.LandlordRepresentedBy = txtLandlordRepresentedBy.Text.ToString();
            //objChargeSlipNewsForms.Photos = Convert.ToBoolean(Convert.ToInt32(chkPhotos.SelectedValue));
            if (rdbYes.Checked == true)
            {
                objChargeSlipNewsForms.Photos = true;
            }
            else
            {
                objChargeSlipNewsForms.Photos = false;
            }

            objChargeSlipNewsForms.PhotosText = txtPhotosText.Text.ToString();
            objChargeSlipNewsForms.AdditionalNotes = txtAdditionalNotes.Text.ToString();            
            
            /*to select Multiple Email list options*/
            if (CheckBoxListEmailList.SelectedValue != "-1")
            {
                string interests = string.Empty;
                foreach (ListItem item in this.CheckBoxListEmailList.Items)
                {
                    if (item.Selected)
                        interests += item + ",";
                }
                objChargeSlipNewsForms.EmailListTo = interests;
            }
            

            //objChargeSlipNewsForms.EmailListTo = DropDownCheckBoxesEmailList.SelectedValue.ToString();
            //if(DropDownCheckBoxesEmailList.SelectedValue!="0")
            //{ 
            //List<string> SelectedEmailList = new List<string>();
            //foreach (System.Web.UI.WebControls.ListItem Item in DropDownCheckBoxesEmailList.Items)
            //{
            //    if (Item.Selected)
            //    {
            //        SelectedEmailList.Add(Item.Text);
            //    }
            //}
            //objChargeSlipNewsForms.EmailListTo = SelectedEmailList.ToString();
            //}
            //else
            //{
            //    objChargeSlipNewsForms.EmailListTo = "0";
            //}

            if (objChargeSlipNewsForms.Save())
            {
                if (objChargeSlipNewsForms.NewsFormsID != 0)
                {
                    Response.Write("<script  type = 'text/javascript'>alert('Data Inserted Successfully!!');</script>");
                }
                else
                {
                    Response.Write("<script  type = 'text/javascript'>alert('Data Insert Failed!!');</script>");
                }
                //if (hdnNewsFormID.Value != "0")
                //    if(objChargeSlipNewsForms.NewsFormsID != 0)
                //    {
                //        Response.Write("<script  type = 'text/javascript'>alert('Data Updated Successfully!!');</script>");
                //    }
                //    else
                //    {
                //        Response.Write("<script  type = 'text/javascript'>alert('Data Inserted Successfully!!');</script>");
                //    }
                //}
                //else
                //{
                //    //if (hdnNewsFormID.Value != "0")
                //    if (objChargeSlipNewsForms.NewsFormsID != 0)
                //    {
                //        Response.Write("<script  type = 'text/javascript'>alert('Data Update Failed!!');</script>");
                //    }
                //    else
                //    {
                //        Response.Write("<script  type = 'text/javascript'>alert('Data Insert Failed!!');</script>");
                //    }
            }
            //btnSave.Enabled = false;
            Response.Redirect("~/Pages/AddOrEditNewChargeSlip.aspx", false);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ChargeSlipNewsForm.aspx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
   
}