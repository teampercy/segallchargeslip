﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true" CodeFile="ChargeSlipNewsForm.aspx.cs" Inherits="Pages_ChargeSlipNewsForm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntHeadTag" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link href="../Styles/SiteTEST.css" rel="stylesheet" />
    <link href="../Styles/AutoCompleteStyle.css" rel="stylesheet" />
    <link href="../Js/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../Js/jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
    <link href="../Js/jquery-ui-1.10.3.custom/development-bundle/themes/ui-lightness/jquery-ui.css" rel="stylesheet" />
    <link href="../Js/jquery-ui.css" rel="stylesheet" />
    <link href="../Js/jquery-ui-1.10.3.custom/development-bundle/themes/base/jquery-ui.css" rel="stylesheet" />
    <script src="../Js/jquery-ui.js"></script>
    <script src="../Js/CommonValidations.js"></script>
    <style type="text/css">
        .drpdown {
            width: 229px;
            height: 24px;
        }

        .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=80);
            opacity: 0.8;
        }

        .label {
            font-weight: bold;
            font-size: small;
            padding-left: 5px;
            width: 250px;
        }

        .TextBox {
            width: 450px;
        }

        input[type="text"] {
            height: 18px;
            font-size: small;
            min-width: 225px;
        }

        .SqureButton {
            background-color: darkgrey;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function Clear() {
            debugger;
            document.getElementById('<%=ddlStatusofLease.ClientID%>').value = "0";
            document.getElementById('<%=txtAnnounceDate.ClientID%>').value = "";
            document.getElementById('<%=txtTitle.ClientID%>').value = "";
            document.getElementById('<%=txtDescription.ClientID%>').value = "";
            document.getElementById('<%=txtTenantRepresentedBy.ClientID%>').value = "";
            document.getElementById('<%=txtLandlordRepresentedBy.ClientID%>').value = "";
            document.getElementById('<%=rdbYes.ClientID%>').checked = false;
            document.getElementById('<%=rdbNo.ClientID%>').checked = false;
            document.getElementById('<%=txtPhotosText.ClientID%>').value = "";
            document.getElementById('<%=txtAdditionalNotes.ClientID%>').value = "";           
            
            clearCheckBoxList();
        }

        //to deselect other checkbox option when Click on No Blast Needed
        function MutExChkList(chk) {
            var chkList = chk.parentNode.parentNode.parentNode;
            var chks = chkList.getElementsByTagName("input");
            for (var i = 0; i < chks.length; i++)
            {
                if (chks[i] != chk && chk.checked)
                {
                    chks[i].checked = false;
                }
            }
        }
        //to deselect No Blast Needed when Click on Other Option
        function MutExChkListAll(chk) {
            var chkList = chk.parentNode.parentNode.parentNode;
            var chks = chkList.getElementsByTagName("input");
            
            if ((chks[1] != chk && chk.checked) || (chks[2] != chk && chk.checked) || (chks[3] != chk && chk.checked) || (chks[4] != chk && chk.checked) || (chks[5] != chk && chk.checked)) {
                    chks[0].checked = false;
                }
            
        }

        //to Clear Checkbox list & set to No Blast Needed checkbox
        function clearCheckBoxList() {

            var elementRef = document.getElementById('<%= CheckBoxListEmailList.ClientID %>');
            var inputElementArray = elementRef.getElementsByTagName('input');
            inputElementArray[0].checked = true;      //to only select No Blast Needed checkbox

            for (var i = 1; i < inputElementArray.length; i++) {
                var inputElement = inputElementArray[i];

                inputElement.checked = false;
                
            }
            return false;
        }
        
    </script>
    <table style="width: 100%">
        <tr>
            <%--<td style="padding-top: 0.5em; color: black; font-family: Verdana; border-color: black; text-align: left; text-shadow: initial; font-weight: 600; font-size: 18px;">
            --%>

            <td>
                <asp:Label ID="lblAdmin" runat="server" Text="ADMINISTRATION"></asp:Label>
                <asp:Label ID="lbErrorMsg" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="Small" /></td>
            <td class="mainPageTitle">Charge Slip News Form
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr style="font-size: medium;" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <%--<asp:HiddenField ID="hdnNewsFormID" runat="server" />--%>

    <table style="width: 100%">
        <tr>
            <td class="label">
                <asp:Label ID="lblBrokerName" runat="server" Text="Broker Name :"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtBrokerName" Class="TextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lblDealName" runat="server" Text="Deal Name :"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtDealName" Class="TextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lblChargeDate" runat="server" Text="Charge Date :"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtChargeDate" Class="TextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lblAddress" runat="server" Text="Address :"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtAddress" Class="TextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lblTenant" runat="server" Text="Tenant / Buyer Name :"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtTenant" Class="TextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lblLandlord" runat="server" Text="Landlord / Seller Name"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtLandlord" Class="TextBox" runat="server"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <td class="label">
                <asp:Label ID="lblSize" runat="server" Text="Size :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtSize" runat="server"></asp:TextBox>
                <asp:DropDownList ID="ddlSize" Class="drpdown" runat="server" Width="220px">
                    <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Square Feet" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Acres" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <hr />
    <table style="width: 100%">
        <tr>
            <td class="label">
  <%--              <asp:Label ID="lblStatusOfLease" runat="server" Text="Status of Lease :"></asp:Label>--%>
                 <asp:Label ID="lblStatusOfLease" runat="server" Text=""></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlStatusofLease" Class="drpdown" runat="server" Style="min-width: 200px;">
                    <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
                    <%--<asp:ListItem Text="Signed" Value="Signed"></asp:ListItem>--%>
                    <asp:ListItem Text="Executed" Value="Executed"></asp:ListItem>
                    <asp:ListItem Text="Custom Description" Value="Custom Description"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lblAnnounceDate" runat="server" Text="Announce Date :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtAnnounceDate" runat="server"></asp:TextBox>
                <ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txtAnnounceDate" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lblTitle" runat="server" Text="Title :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtTitle" Class="TextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lblDescription" runat="server" Text="Description :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtDescription" Class="TextBox" Width="448px" runat="server" TextMode="MultiLine" Rows="10"></asp:TextBox>
            </td>
        </tr>
        <tr class="label">
            <td>
                <asp:Label ID="lblTenantRepresentedBy" runat="server" Text="Tenant Represented By :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtTenantRepresentedBy" Class="TextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lblLandlordRepresentedBy" runat="server" Text="Landlord Represented By :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtLandlordRepresentedBy" Class="TextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lblPhotos" runat="server" Text="Photos :"></asp:Label>
            </td>
            <td style="white-space: nowrap; display: inline;">
                <%--  <asp:CheckBoxList ID="chkPhotos" runat="server"  data-toggle="radio" RepeatDirection="Horizontal" style="overflow:auto;white-space:nowrap;">
                    <asp:ListItem Text="Yes" groupname="Photos" Value="1" runat="server"></asp:ListItem>
                    <asp:ListItem Text="No" groupname="Photos" Value="0" runat="server"></asp:ListItem>
                </asp:CheckBoxList>--%>
                <asp:RadioButton ID="rdbNo" GroupName="Photos" Text="No" Width="50px" runat="server" />
                <asp:RadioButton ID="rdbYes" GroupName="Photos" Text="Yes" Width="50px" runat="server" />
                <asp:TextBox ID="txtPhotosText" Width="340px" runat="server" placeholder="if Yes, please include description and location"></asp:TextBox>

            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lblAdditionalNotes" runat="server" Text="Additional Notes :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtAdditionalNotes" Class="TextBox" Width="448px" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="label">
                <asp:Label ID="lblEmailListTo" runat="server" Text="Email List To :"></asp:Label>
            </td>
            <td>
                <asp:CheckBoxList ID="CheckBoxListEmailList" runat="server" >
                    <asp:ListItem Text="No Blast Needed" Value="0" Selected="True" onclick="MutExChkList(this);"></asp:ListItem>
                    <asp:ListItem Text="Brokers" Value="1" onclick="MutExChkListAll(this);"></asp:ListItem>
                    <asp:ListItem Text="Landlords" Value="2" onclick="MutExChkListAll(this);"></asp:ListItem>
                    <asp:ListItem Text="Segall Tenants" Value="3" onclick="MutExChkListAll(this);"></asp:ListItem>
                    <asp:ListItem Text="Friends of Segall" Value="4" onclick="MutExChkListAll(this);"></asp:ListItem>
                    <asp:ListItem Text="Real Estate Reporters" Value="5" onclick="MutExChkListAll(this);"></asp:ListItem>
                </asp:CheckBoxList>               
                
            </td>
        </tr>
        <tr>
            <td style="height: 20px;"></td>
            <td></td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="SqureButton" style="margin-right:20px;" OnClick="btnSave_Click" />

                <input id="butClearForm" type="button" value="Clear" onclick="Clear()"  class="SqureButton" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentFooter" runat="Server">
</asp:Content>

