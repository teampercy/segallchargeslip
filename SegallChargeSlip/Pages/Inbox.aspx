﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true" CodeFile="Inbox.aspx.cs" Inherits="Pages_Inbox" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="contHeadCont" ContentPlaceHolderID="cntHeadTag" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <table style="width: 100%">
        <tr>
            <%--<td style="padding-top: 0.5em; color: black; font-family: Verdana; border-color: black; text-align: left; text-shadow: initial; font-weight: 600; font-size: 18px;">
            --%>

            <td>
                <asp:Label ID="lblAdmin" runat="server" Text="ADMINISTRATION"></asp:Label>
                <asp:Label ID="lbErrorMsg" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="Small" /></td>
            <td class="mainPageTitle">INBOX
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr style="font-size: medium;" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <%--  <link href="../Styles/SiteTEST.css" rel="stylesheet" />--%>
    <asp:ScriptManager ID="scrpmanager1" runat="server"></asp:ScriptManager>
    <style type="text/css">
        a img {
            border: none;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function test()
        {
            Navigate();
        }
        function Navigate() {
            debugger;
            var Index = 1;
            $.ajax({
                type: "POST",
                url: "Inbox.aspx/GetChargeSlipDetails",
                data: '{RowIndx: "' + Index + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    window.open("NewCharge.aspx", '_self');
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
           // window.open("NewCharge.aspx", '_blank');
            
            return false;
        }
        function CloseMessageDetail() {
            // debugger;

            document.getElementById('<%=btnCancel.ClientID %>').click();
            return false;
        }
        function HideConfirmBox() {
            alert('Please Select checkbox to Delete');
        }
        function SelectAll(id) {

            var frm = document.forms[0];

            for (i = 0; i < frm.elements.length; i++) {
                if (frm.elements[i].type == "checkbox") {
                    frm.elements[i].checked = document.getElementById(id).checked;
                }
            }
        }
        //function deleteMessage() {
        //    var frm = document.forms[0];

        //    for (i = 0; i < frm.elements.length; i++) {
        //        if (frm.elements[i].type == "checkbox") {
        //            if (document.getElementById(id).checked) {
        //                return true;
        //            }
        //            else {
        //                alert('hiiiiii');
        //                return false;
        //            }
        //        }
        //    }

        //}
    </script>
    <style type="text/css">
        .lnkMsg {
            color: black;
            text-decoration: none;
        }
    </style>
    <asp:UpdatePanel ID="upMessage" runat="server">
        <ContentTemplate>
            <table style="width: 100%;">
                <tr>
                    <td style="padding-left: 5px;">

                        <asp:Button ID="btnDelete" runat="server" Text="DELETE" CssClass="SqureButton" OnClick="btnDelete_Click" />
                        <asp:Button ID="btnRead" runat="server" Text="MARK AS READ" CssClass="SqureButton" OnClick="btnRead_Click" />
                    </td>
                    <td align="right">
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="normalTextBox" Height="18px"></asp:TextBox>
                        <ajax:TextBoxWatermarkExtender ID="TBWE2" runat="server"
                            TargetControlID="txtSearch"
                            WatermarkText="Search" />

                    </td>
                    <td align="right" style="padding-top: 0.5em; width: 30px; padding-right: 7px;">
                        <asp:ImageButton ID="btnSearch" runat="server" Height="20px" ImageUrl="~/Images/Search-Icon.gif" OnClick="btnSearch_Click" />

                    </td>
                </tr>


                <tr>
                    <td align="left" style="padding-left: 5px;" colspan="3">
                        <%--<asp:Label ID="lblTotalRecord" Text ="" runat ="server" Font-Bold="true"></asp:Label>--%>
                         
                    </td>
                    <%--  <td  align="right" colspan="2" style ="padding-right:5px;">
                       
                       <asp:Label ID="lblPages" runat="server" Font-Bold="true"></asp:Label>
                       </td>--%>
                </tr>
                <tr>
                    <td colspan="3">
                        <div style="padding-left: 5px;">
                            <asp:GridView ID="grdMessage" runat="server" Width="99.5%" AllowPaging="true" PageSize="27" AutoGenerateColumns="false" HeaderStyle-HorizontalAlign="Center" OnPageIndexChanging="grdMessage_PageIndexChanging" OnRowCreated="grdMessage_RowCreated"
                                OnRowDataBound="grdMessage_RowDataBound" OnRowCommand="grdMessage_RowCommand"
                                CssClass="AllBorder"
                                HeaderStyle-CssClass="HeaderGridView"
                                RowStyle-CssClass="RowGridView"
                                FooterStyle-CssClass="FooterGridView"
                                EmptyDataRowStyle-HorizontalAlign="Center"
                                AlternatingRowStyle-CssClass="AlternatingRowGridView2">
                                <%--    <HeaderStyle BackColor="DarkGray" ForeColor="Black" Font-Bold="true" Font-Size="Small" Font-Names="Verdena" HorizontalAlign="Center" />
                        <FooterStyle BackColor="DarkGray" ForeColor="Black" Font-Bold="true" Font-Size="Small" Font-Names="Verdena" Wrap="false" />
                        <RowStyle Font-Size="Small" Font-Names="Verdena" />--%>
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="cbSelectAll1" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkMessage" runat="server" CssClass="case" />
                                            <asp:HiddenField ID="hdnMessageID" runat="server" Value='<%# Eval("MessageID") %>' />
                                            <asp:HiddenField ID="hdnIsRead" runat="server" Value='<%# Eval("IsRead") %>' />
                                            <asp:HiddenField ID="hdnChargeslipId" runat ="server" Value='<%#Eval("ChargeSlipId")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="MessageDate" HeaderText="DATE" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" />
                                    <%--<asp:BoundField DataField="MessageText" HeaderText="NOTICE" ItemStyle-Width="80%" />--%>
                                    <asp:TemplateField HeaderText="NOTICE" ItemStyle-Width="77%">
                                        <ItemTemplate>

                                            <%-- <asp:LinkButton ID="linkNotice" runat ="server" CssClass ="lnkMsg"  OnClick="linkNotice_Click" Text ='<%# Eval("MessageText") %>' CommandArgument='<%# Eval("MessageID") %>'
                                        OnClientClick ='<%# "check(" +Eval("MessageID") + ");" %>' ></asp:LinkButton>--%>
                                            <asp:LinkButton ID="linkNotice" runat="server" CssClass="lnkMsg" Text='<%# Eval("MessageText") %>' CommandArgument='<%# Eval("MessageID") %>'
                                                OnClick="linkNotice_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ControlStyle-Width="10px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgView" CommandName="View" CommandArgument='<%#Eval("MessageID") %>' Height="10px" Width="10px" ImageUrl="~/Images/Info-Card-Icon.gif" runat="server" />

                                        </ItemTemplate>
                                        <ControlStyle Width="10px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                                        <ItemTemplate>
                                             <asp:ImageButton ID="imgView" ToolTip="View Notes Detail" Style="text-align: left;float:left;" CommandName="View" CommandArgument='<%#Eval("MessageID") %>' Height="15px" Width="15px" ImageUrl="~/Images/Info-Card-Icon.gif" runat="server" />
                                              <asp:ImageButton ID="imgChargeslip"  Style="text-align: center;" ToolTip="View Chargeslip Detail" Width="15px" CommandName="ViewChargeslip" CommandArgument='<%#Eval("ChargeSlipId")%>' Height="15px" runat="server" ImageUrl="~/Images/Edit-Icon.gif"  />
                                         <%--  <asp:Label ID="lblText" runat ="server" Text="" Visible="false" Width="8px"></asp:Label>--%>
                                             <asp:LinkButton ID="lnkDelete" CommandArgument='<%# Eval("MessageID") %>' Style="text-align: right;float:right;" CommandName="Del"
                                                OnClientClick="return confirm('Are you sure you want delete ?')" OnClick="lnkDelete_Click" runat="server">                                                
                                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/delete.gif" Height="15"
                                                    Width="15" ToolTip="Delete this Message" />
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <b style="padding: 2px 5px  2px 25px; color: #000000; background-color: #FFFFFF; height: 26px; vertical-align: middle; cursor: pointer;">No records found matching
                                            search criteria or filter.</b>
                                </EmptyDataTemplate>
                                <PagerTemplate>
                                </PagerTemplate>
                                <PagerStyle CssClass="pagerStyle" />
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
            </table>
            <div style="margin: 0; width: 100%;">

                <ajax:ModalPopupExtender ID="modalMessageDetail" runat="server" CancelControlID="btnCancel"
                    TargetControlID="lbDummy" PopupControlID="pnlAddOrder" BackgroundCssClass="modalBackground">
                </ajax:ModalPopupExtender>
                <asp:Label ID="lbDummy" runat="server"></asp:Label>
                <asp:LinkButton ID="btnCancel" runat="server" Style="display: none" />
                <asp:Panel ID="pnlAddOrder" runat="server" Style="display: none; height: 100%; width: 100%; z-index: 100000000;">

                    <div style="width: 700px; height: auto; overflow: auto; margin: auto; margin-top: 250px;">
                        <table border="0" cellpadding="0" cellspacing="0" style="vertical-align: top; border-bottom-color: #728AA3; border-bottom-style: solid; border-bottom-width: 1px; border-right-color: #728AA3; border-right-style: solid; border-right-width: 1px; border-left-color: #728AA3; border-left-style: solid; border-left-width: 1px; background-color: White;"
                            width="100%"
                            align="center">
                            <tr>
                                <td valign="top" style="background-color: gray; height: 21px; width: 590px;padding-left:10px;" align="center">
                                    <asp:Label ID="lblTitle" runat="server" Text="Details" Font-Bold="true" 
                                        ForeColor="White">
                                    </asp:Label>
                                </td>
                                <td valign="middle" align="right" style="background-color: gray; height: 21px; width: 20px;">
                                    <asp:ImageButton ID="imgbtnClose" runat="server" ImageUrl="~/Images/close.png" OnClientClick="javascript:return CloseMessageDetail();" />
                                </td>
                            </tr>
                             <tr>
                                <td colspan="2" style="height: 20px;">
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 50px;padding-left:50px;padding-right:50px;">
                                    <asp:Label ID="lblMessageChangeDetail" runat="server" Text=""></asp:Label></td>
                            </tr>
                          
                            <tr>
                                <td colspan="2">
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnOk" runat="server" Text="OK" OnClientClick="javascript:return CloseMessageDetail();" CssClass="SqureButton" Width="100px" />
                                </td>
                            </tr>
                             <tr>
                                <td colspan="2" style="height: 25px;">
                                    <br />
                                </td>
                            </tr>
                        </table>

                    </div>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

