﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true"
    CodeFile="NewChargeSlipReview.aspx.cs" Inherits="Pages_NewChargeSlipReview" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>--%>
<%@ Register Src="~/UserControl/ucBaseCommisiion.ascx" TagName="ucBase" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucOptionCommission.ascx" TagName="ucOption" TagPrefix="uc" %>

<%@ Register Src="~/UserControl/ucNewCharegeReviewSummary.ascx" TagName="ucReviewSummary" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <table style="width: 100%">
        <tr>
            <td style="padding-top: 0.5em; color: #F68620; border-color: black; text-align: left; text-shadow: initial; font-weight: 600; font-size: 18px;">CHARGE SLIP REVIEW
            </td>
            <td style="padding-top: 0.5em; text-align: right; width: 60%; color: #F68620; text-shadow: initial; font-weight: 600; font-size: 20px;">
                <%--<h2 style="color: yellowgreen; border-color: black; text-align: right; text-shadow: initial;">--%>NEW CHARGE SLIP<%--</h2>--%>
            </td>
        </tr>
    </table>
    <hr style="width: 100%" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <link href="../Styles/SiteTEST.css" rel="stylesheet" />
    <link href="../Styles/AutoCompleteStyle.css" rel="stylesheet" />
    <link href="../Js/ jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../Js/ jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
    <link href="../Js/ jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />
    <link href="../Js/jquery-ui.css" rel="stylesheet" />
    <script src="../Js/jquery-ui.js"></script>


    <script src="../Js/CommonValidations.js"></script>

    <style type="text/css">
        .leaseTermHeader {
            min-width: 90px;
        }

        .leaseTermIncrease {
            min-width: 105px;
        }

        .DisplayNone {
            display: none;
        }

        .tblfixayout {
            width: 100%;
            color: black;
            table-layout: fixed;
            padding: 0px 0px 0px 0px;
            border-spacing: 0;
            text-align: center;
            /*cellspacing:0px;
        cellpadding:opx;*/
        }

        /*.tblfixayout tr td {
         height:8px;
        }*/
        .tbOnverFlow {
            text-align: left;
            overflow: hidden;
            width: 50px;
            padding: 0px 0px 0px 0px;
        }

        .tbNormal {
            overflow: hidden;
            width: 40%;
            padding: 0px 0px 0px 0px;
            white-space: nowrap;
            text-align: left;
        }

        .tdmain {
            /*width: 120px;*/
            width: 40%;
            text-align: left;
            /*text-wrap: initial;*/
        }

        .tdspceing {
            /*width: 20px;*/
            width: 8%;
            text-align: right;
        }

        .tdNuneric {
            /*width: 60px;*/
            width: 22%;
            text-align: right;
        }

        .auto-style1 {
            width: 89%;
        }

        .tdBorderBottom {
            border-bottom: solid 1px black;
        }

        .tdBorderLeft {
            border-left: solid 1px black;
        }

        .tdBorderRight {
            border-right: solid 1px black;
        }

        .tdBorderTop {
            border-top: solid 1px black;
        }
        /*popup jquery*/

        .ui-widget-header {
            font-weight: bold;
            font-size: 1.2em;
            background: gray;
            font-family: Verdana;
            /*border: 2px solid #000000;
             margin-left: auto;
             margin-right: auto*/
        }

        .ui-widget-content {
            border: 2px solid black;
        }

        .ui-dialog-titlebar-close {
            border: none;
            background-color: gray;
            background-repeat: no-repeat;
            background-image: url('../Images/close.png');
            padding: 0px;
            border-width: 0px;
        }
        /*.ui-dialog .ui-dialog-content {
             border: 2px solid #000000;
             border-collapse: separate;
             margin-left: auto;
             margin-right: auto;
        }
        
        }*/
        /*.ui-state-default .ui-icon {
            background-image:url(../Images/close.png);
        }*/
    </style>

    <script type="text/javascript">
        $(function () {
            $("#ContentMain_dvPrivacySettings").dialog({
                autoOpen: false,
                width: 430,
                modal: true,
                show: {
                    duration: 300
                },
                hide: {
                    duration: 300
                }
            });
        });
        $(function () {
            $("#ContentMain_divEditedGenerated").dialog({
                autoOpen: false,
                width: 430,
                modal: true,
                show: {
                    duration: 300
                },
                hide: {
                    duration: 300
                },
                closeOnEscape: false,
                open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); }

            });
        });
        function NavigateEditRegenerated() {
            document.getElementById('<%=btnEditRegenerateClick.ClientID%>').click();
        }
        function ShowEditedRegenerated() {
            //To Check Commission Due Date % is 100 before submitting the ChargeSlip                                   
            if (CheckDueDatesPercentageByTermType() == true) {
                $("#ContentMain_divEditedGenerated").dialog("open");
            }
            return false;
        }
        function ShowPopUp() {            
            //To Check Commission Due Date % is 100 before submitting the ChargeSlip
            if (CheckDueDatesPercentageByTermType() == true) {
                $("#ContentMain_dvPrivacySettings").dialog("open");
                $('#ContentMain_chkUsers').find('input[type="checkbox"]').prop('checked', false);
            }
            return false;
        }

        $(document).ready(function () {

            $('#btnSelAllUsers').click(function () {
                if ($("#ContentMain_chkUsers").find('input[type="checkbox"]').is(':checked')) {
                    $('#ContentMain_chkUsers').find('input[type="checkbox"]').prop('checked', false);
                } else {
                    $('#ContentMain_chkUsers').find('input[type="checkbox"]').prop('checked', true);
                }
                var LoggedInUser = '<%=Session["UserName"]%>';
            });
        });

        function SaveAlert(Result) {
            if (Result == true) {
                alert("New Charge Slip Saved Successfully");
                document.getElementById('<%= btnSendMail.ClientID %>').click();
                // window.location.href = "AddOrEditNewChargeSlip.aspx";
                return true;
            }
            else {
                alert("Error Occured While Save");
                window.location.href = "AddOrEditNewChargeSlip.aspx";
                return true;
            }
        }
        function OpenWindow(Path) {
            var width = 650;
            var height = 450;
            var left = (screen.width - width) / 2;
            var top = (screen.height - height) / 2;
            var params = 'width=' + width + ', height=' + height;
            params += ', top=' + top + ', left=' + left;
            params += ', directories=no';
            params += ', location=no';
            params += ', menubar=no';
            params += ', resizable=no';
            params += ', scrollbars=no';
            params += ', status=no';
            params += ', toolbar=no';
            window.open(Path, 'mywindow', params);
        }

        function SelectPropertyAccessUsers() {

            var Users = "";
            $('#ContentMain_chkUsers input:checked').each(function () {
                if (Users == "") {
                    Users = this.value;
                }
                else {
                    Users = Users + "," + this.value;
                }
            });

            $("#ContentMain_hdnPropertyAccessUsers").val(Users);
            // $("#ContentMain_PageTitle").css("display", "block");
            InsertPropertyAccessRights(Users);
            $("#ContentMain_dvPrivacySettings").dialog("close");
            // __doPostBack('btnS', 'OnClick');
        }

        function InsertPropertyAccessRights(Users) {

            var SaveStatus = document.getElementById('<%= hdnSaveStatus.ClientID %>').value
            $.ajax({
                type: "POST",
                url: "NewChargeSlipReview.aspx/InsertPropertyAccessRights",
                data: '{Users: "' + Users + '",SaveStatus:"' + SaveStatus + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                },
                success: function (reslt) {
                    SaveAlert(reslt.d);
                }
            });
            return true;
        }

        function CheckDueDatesPercentageByTermType() {
            var bool = false;
            $.ajax({
                async: false,
                type: "POST",
                url: "NewChargeSlipReview.aspx/CheckDueDatesPercentageByTermType",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                failure: function (response) {
                    alert(respose.d);
                    bool = false;
                },
                success: function (result) {                    
                    if (result.d.length > 0) {
                        alert('Commission Due date percentage total is not 100% for ' + result.d + ' Please check the Commission Due table and rectify before saving the ChargeSlip.');
                        bool = false;
                    } else {
                        bool = true;
                    }
                }
            });            
            return bool;
        }

        function GenerateSummary(UserName, CommPer, CommAmt, TermType, BrokerPer, CompPer, CompanyName) {
            debugger;
            var UserName = UserName.replace("#", "'");
            // alert(UserName);
            //document.getElementById('tbSummary').style.display = 'inlineBlock';
            if (TermType == 1) {
                var tableSumm = document.getElementById('tb_CommSplitSummBaseTerm');
                var tableCmpSplitSumm = document.getElementById('tb_CompSplitSummBaseTerm');

                document.getElementById('tr_BaseHeading').style.display = 'block';
                document.getElementById('tr_BaseDetails').style.display = 'block';
                document.getElementById('tr_OptFooter').style.display = 'block';
            }
            if (TermType == 2) {
                var tableSumm = document.getElementById('tb_CommSplitSummOptTerm');
                var tableCmpSplitSumm = document.getElementById('tb_CompSplitSummOptTerm');

                document.getElementById('tr_OptHeading').style.display = 'block';
                document.getElementById('tr_OptDetails').style.display = 'block';
                //document.getElementById('tr_ConFooter1').style.display = 'block';
                document.getElementById('tr_Opt1Footer').style.display = 'block';

            }
            if (TermType == 3) {
                var tableSumm = document.getElementById('tb_CommSplitSummCntTerm');
                var tableCmpSplitSumm = document.getElementById('tb_CompSplitSummConTerm');

                document.getElementById('tr_ConHeading').style.display = 'block';
                document.getElementById('tr_ConDetails').style.display = 'block';
            }

            if (TermType == 4) {
                var tableSumm = document.getElementById('tb_CommSplitSummOptTerm1');
                var tableCmpSplitSumm = document.getElementById('tb_CompSplitSummOptTerm1');

                document.getElementById('tr_Opt1Heading').style.display = 'block';
                document.getElementById('tr_Opt1Details').style.display = 'block';
                document.getElementById('tr_Opt2Footer').style.display = 'block';
            }
            if (TermType == 5) {
                var tableSumm = document.getElementById('tb_CommSplitSummOptTerm2');
                var tableCmpSplitSumm = document.getElementById('tb_CompSplitSummOptTerm2');

                document.getElementById('tr_Opt2Heading').style.display = 'block';
                document.getElementById('tr_Opt2Details').style.display = 'block';
                document.getElementById('tr_Opt3Footer').style.display = 'block';
            }
            if (TermType == 6) {
                var tableSumm = document.getElementById('tb_CommSplitSummOptTerm3');
                var tableCmpSplitSumm = document.getElementById('tb_CompSplitSummOptTerm3');

                document.getElementById('tr_Opt3Heading').style.display = 'block';
                document.getElementById('tr_Opt3Details').style.display = 'block';
                document.getElementById('tr_ConFooter1').style.display = 'block';
            }
            //var tableSumm = document.getElementById("ContentMain_ucNewCharegeReviewSummary_tb_CommSplitSumm");

            var rowCount = tableSumm.rows.length;
            var cellCount = tableSumm.rows[0].cells.length;
            var row = tableSumm.insertRow(rowCount);
            rowCount = rowCount + 1;
            var i;
            for (i = 1; i <= 5; i++) {
                var cell_1 = row.insertCell(i - 1);
                if (i % 2 == 0) {
                    // cell_1.style.width = "20px";
                    //cell_1.style.width = "3%"; &#13;&#10;
                    var element_1 = document.createElement("span");
                    element_1.name = 'lbSummary_' + rowCount + i;
                    element_1.id = 'lbSummary_' + rowCount + i;
                    element_1.type = "span";
                    element_1.innerHTML = "--";
                    cell_1.className = "tdspceing";
                }
                else {
                    var element_1 = document.createElement("span");
                    element_1.type = "span";
                    element_1.name = 'lbSummary_' + rowCount + i;
                    element_1.id = 'lbSummary_' + rowCount + i;

                    switch (i) {
                        case 1:

                            cell_1.className = "tdmain";
                            cell_1.id = "td_" + TermType + "_" + rowCount + i;
                            $("#spPixelWidth").text(UserName);
                            var StrWidth = $("#spPixelWidth").width();

                            if (StrWidth < 119) {
                                element_1.innerHTML = UserName + "";
                                $("#" + cell_1.id).attr('class', 'tbNormal');
                            }
                            else {
                                element_1.innerHTML = UserName;
                                $("#" + cell_1.id).attr('class', 'tbOnverFlow');
                            }
                            element_1.style.textAlign = "left";
                            break;
                        case 3:
                            cell_1.className = "tdNuneric";
                            element_1.innerHTML = (parseFloat(CommPer)).toFixed(2) + '%';
                            element_1.style.textAlign = "right";
                            break;
                        case 5:
                            cell_1.className = "tdNuneric";
                            element_1.innerHTML = FormatCurrency(parseFloat(CommAmt), "$");
                            element_1.style.textAlign = "right";
                            break;
                    }
                    cell_1.appendChild(element_1);
                }
                cell_1.appendChild(element_1);
                //  element_1.innerHTML = UserName + '---------------' + parseFloat(CommPer).toFixed(2) + '% -----' + parseFloat(CommAmt).toFixed(2);
            }//&& TermType == 1
            debugger;
            var LoggedInUser = '<%=Session["UserName"]%>';
            var loggedintype = '<%=Session["UserType"]%>';
            //if (UserName.includes(LoggedInUser + ' (CLB-Broker)'))
            if (UserName.indexOf(LoggedInUser + ' (CLB-Broker)')>=0)
            {
                LoggedInUser = LoggedInUser + ' (CLB-Broker)';
            }

            if (BrokerPer > 0 && (UserName == LoggedInUser || loggedintype == "1")) {

                var rowCount = tableCmpSplitSumm.rows.length;
                //if (rowCount == 1) {
                var BrokerAmt = (parseFloat(BrokerPer) * (parseFloat(CommAmt)) / 100);//.toFixed(2)
                var CompAmt = (parseFloat(CompPer) * (parseFloat(CommAmt)) / 100);//.toFixed(2)
                var row = tableCmpSplitSumm.insertRow(rowCount);
                rowCount = rowCount + 1;

                var i;
                for (i = 1; i <= 5; i++) {
                    var cell_1 = row.insertCell(i - 1);
                    if (i % 2 == 0) {
                        // cell_1.style.width = "20px";
                        //cell_1.style.width = "3%"; &#13;&#10;
                        var element_1 = document.createElement("span");
                        element_1.name = 'lbSummary_' + rowCount + i;
                        element_1.id = 'lbSummary_' + rowCount + i;
                        element_1.type = "span";
                        element_1.innerHTML = "--";
                        cell_1.className = "tdspceing";
                    }
                    else {
                        var element_1 = document.createElement("span");
                        element_1.type = "span";
                        element_1.name = 'lbBrokerSummary_' + rowCount + i;
                        element_1.id = 'lbBrokerSummary_' + rowCount + i;
                            <%--line removed to remove -- --------------------------------------------------------------------------------- ---%>

                        switch (i) {
                            case 1:

                                //cell_1.style.width = "120px";
                                cell_1.className = "tbNormal";
                                cell_1.id = "tdBrokerSummary_" + rowCount + i;
                                $("#spPixelWidth").text(UserName);
                                var StrWidth = $("#spPixelWidth").width();

                                if (StrWidth < 119) {
                                    element_1.innerHTML = UserName + "";
                                    $("#" + cell_1.id).attr('class', 'tbNormal');
                                }
                                else {
                                    element_1.innerHTML = UserName;
                                    $("#" + cell_1.id).attr('class', 'tbOnverFlow');
                                }
                                element_1.style.textAlign = "left";
                                break;
                            case 3:
                                //cell_1.style.width = "70px";
                                cell_1.className = "tdNuneric";
                                element_1.innerHTML = (parseFloat(BrokerPer)).toFixed(2) + '%';
                                element_1.style.textAlign = "right";
                                break;
                            case 5:
                                //cell_1.style.width = "70px";
                                cell_1.className = "tdNuneric";
                                element_1.innerHTML = FormatCurrency(parseFloat(BrokerAmt), "$");
                                element_1.style.textAlign = "right";
                                break;
                        }
                        cell_1.appendChild(element_1);
                    }
                    cell_1.appendChild(element_1);
                }

                //var cellCount = tableCmpSplitSumm.rows[0].cells.length;
                var row = tableCmpSplitSumm.insertRow(rowCount);

                var i;
                for (i = 1; i <= 5; i++) {
                    var cell_1 = row.insertCell(i - 1);
                    if (i % 2 == 0) {
                        var element_1 = document.createElement("span");
                        element_1.name = 'lbSummary_' + rowCount + i;
                        element_1.id = 'lbSummary_' + rowCount + i;
                        element_1.type = "span";
                        element_1.innerHTML = "--";
                        cell_1.className = "tdspceing";
                    }
                    else {
                        var element_1 = document.createElement("span");
                        element_1.type = "span";
                        element_1.name = 'lbBrokerSummary_' + rowCount + i;
                        element_1.id = 'lbBrokerSummary_' + rowCount + i;

                        switch (i) {
                            case 1:

                                cell_1.className = "tdmain";
                                cell_1.id = "tdCmpSummary_" + rowCount + i;

                                $("#spPixelWidth").text(CompanyName);
                                var StrWidth = $("#spPixelWidth").width();
                                if (StrWidth < 119) {
                                    element_1.innerHTML = CompanyName + "";
                                    $("#" + cell_1.id).attr('class', 'tbNormal');
                                }
                                else {
                                    element_1.innerHTML = CompanyName;
                                    $("#" + cell_1.id).attr('class', 'tbOnverFlow');
                                }
                                element_1.style.textAlign = "left";
                                break;
                            case 3:
                                cell_1.className = "tdNuneric";
                                element_1.innerHTML = (parseFloat(CompPer)).toFixed(2) + '%';
                                element_1.style.textAlign = "right";
                                break;
                            case 5:
                                cell_1.className = "tdNuneric";
                                element_1.innerHTML = FormatCurrency(parseFloat(CompAmt), "$");
                                element_1.style.textAlign = "right";
                                break;
                        }
                        cell_1.appendChild(element_1);
                    }
                    cell_1.appendChild(element_1);
                }

                //set Comp Split Summary

                var AmtTot, PerTot;
                AmtTot = parseFloat(CompAmt) + parseFloat(BrokerAmt);
                PerTot = parseFloat(CompPer) + parseFloat(BrokerPer);

                GenerateSummaryTotal(PerTot, AmtTot, TermType, true);
                //var cell_1 = row.insertCell(0);
                //row = tableCmpSplitSumm.insertRow(rowCount);
                //var cell_2 = row.insertCell(0);
                //var element_2 = document.createElement("Lable");
                //element_2.type = "lable";
                //element_2.name = 'lbCmpSummary_' + rowCount;
                //element_2.id = 'lbCmpSummary__' + rowCount;
                //element_2.innerHTML = CompanyName + '----------' + parseFloat(CompPer).toFixed(2) + '% -----' + CompAmt;
                //cell_2.appendChild(element_2);
                //}
            }
        }

        function GenerateSummaryTotal(CommPerTot, CommAmtTot, TermType, CmpSplit) {
            debugger;
            if (TermType == 1) {
                var tableSumm = document.getElementById('tb_CommSplitSummBaseTerm');
                if (CmpSplit == true) {
                    var tableSumm = document.getElementById('tb_CompSplitSummBaseTerm');
                }
            }
            if (TermType == 2) {
                var tableSumm = document.getElementById('tb_CommSplitSummOptTerm');
                if (CmpSplit == true) {
                    var tableSumm = document.getElementById('tb_CompSplitSummOptTerm');
                }
            }
            if (TermType == 3) {
                var tableSumm = document.getElementById('tb_CommSplitSummCntTerm');
                if (CmpSplit == true) {
                    var tableSumm = document.getElementById('tb_CompSplitSummConTerm');
                }
            }

            if (TermType == 4) {
                var tableSumm = document.getElementById('tb_CommSplitSummOptTerm1');
                if (CmpSplit == true) {
                    var tableSumm = document.getElementById('tb_CompSplitSummOptTerm1');
                }
            }
            if (TermType == 5) {
                var tableSumm = document.getElementById('tb_CommSplitSummOptTerm2');
                if (CmpSplit == true) {
                    var tableSumm = document.getElementById('tb_CompSplitSummOptTerm2');
                }
            }
            if (TermType == 6) {
                var tableSumm = document.getElementById('tb_CommSplitSummOptTerm3');
                if (CmpSplit == true) {
                    var tableSumm = document.getElementById('tb_CompSplitSummOptTerm3');
                }
            }

            var rowCount = tableSumm.rows.length;
            var cellCount = tableSumm.rows[0].cells.length;
            var row = tableSumm.insertRow(rowCount);

            var row = tableSumm.insertRow(rowCount);

            for (i = 1; i <= 5; i++) {
                var cell_1 = row.insertCell(i - 1);
                if (i >= 3) {
                    cell_1.id = "hr" + i;
                    var element_1 = document.createElement("hr");
                    //element_1.innerHTML = "<hr/>";
                    element_1.style.width = "100%";
                    element_1.style.backgroundColor = "black";
                }
                else {
                    var element_1 = document.createElement("span");
                }
                element_1.name = 'lbBlank' + rowCount + i;
                element_1.id = 'lbSummaryTotLine_' + rowCount + i;
                cell_1.appendChild(element_1);
            }


            rowCount = rowCount + 1;
            var row = tableSumm.insertRow(rowCount);

            var i;
            for (i = 1; i <= 5; i++) {
                var cell_1 = row.insertCell(i - 1);
                if (i % 2 == 0) {
                    var element_1 = document.createElement("span");
                    element_1.name = 'lbSummary_' + rowCount + i;
                    element_1.id = 'lbSummary_' + rowCount + i;
                    element_1.type = "span";
                    element_1.innerHTML = "    ";
                    cell_1.className = "tdspceing";
                }
                else {
                    var element_1 = document.createElement("span");
                    element_1.type = "span";
                    element_1.name = 'lbSummary_' + rowCount + i;
                    element_1.id = 'lbSummary_' + rowCount + i;

                    switch (i) {
                        case 1:
                            cell_1.className = "tdmain";
                            //cell_1.style.width = "120px";
                            // cell_1.id = "td_" + TermType + "_" + rowCount + i;
                            break;
                        case 3:
                            cell_1.className = "tdNuneric";
                            //cell_1.style.width = "70px";
                            if (CommPerTot == "")
                            { element_1.innerHTML = ""; }
                            else { element_1.innerHTML = parseFloat((CommPerTot)).toFixed(2) + '%'; }

                            element_1.style.textAlign = "right";
                            break;
                        case 5:
                            cell_1.className = "tdNuneric";
                            //cell_1.style.width = "70px";
                            element_1.innerHTML = FormatCurrency(parseFloat(CommAmtTot), "$");
                            element_1.style.textAlign = "right";
                            break;
                    }
                    cell_1.appendChild(element_1);
                }
                cell_1.appendChild(element_1);
            }
        }

        function DueDateSummary(DueDate, CommPer, CommAmt, TermType) {
            debugger;
            if (TermType == 2) {

                var tableDueDtSumm = document.getElementById('tb_DueDatesOpt');
                document.getElementById('tr_OptHeading').style.display = 'block';
                document.getElementById('tr_OptDetails').style.display = 'block';
                document.getElementById('tr_OptFooter').style.display = 'block';
                document.getElementById('tr_OptFooter1').style.display = 'block';
            }
            if (TermType == 3) {
                var tableDueDtSumm = document.getElementById('tb_DueDatesCon');
                document.getElementById('tr_ConHeading').style.display = 'block';
                document.getElementById('tr_ConDetails').style.display = 'block';
                document.getElementById('tr_ConFooter').style.display = 'block';
                document.getElementById('tr_ConFooter1').style.display = 'block';
                document.getElementById('tr_ConFooter2').style.display = 'block';
            }

            if (TermType == 4) {
                var tableDueDtSumm = document.getElementById('tb_DueDatesOpt1');
                document.getElementById('tr_Opt1Heading').style.display = 'block';
                document.getElementById('tr_Opt1Details').style.display = 'block';
                document.getElementById('tr_Opt1Footer').style.display = 'block';
                document.getElementById('tr_Opt1Footer1').style.display = 'block';
            }
            if (TermType == 5) {
                var tableDueDtSumm = document.getElementById('tb_DueDatesOpt2');
                document.getElementById('tr_Opt2Heading').style.display = 'block';
                document.getElementById('tr_Opt2Details').style.display = 'block';
                document.getElementById('tr_Opt2Footer').style.display = 'block';
                document.getElementById('tr_Opt2Footer1').style.display = 'block';
            }
            if (TermType == 6) {
                var tableDueDtSumm = document.getElementById('tb_DueDatesOpt3');
                document.getElementById('tr_Opt3Heading').style.display = 'block';
                document.getElementById('tr_Opt3Details').style.display = 'block';
                document.getElementById('tr_Opt3Footer').style.display = 'block';
                document.getElementById('tr_Opt3Footer1').style.display = 'block';
            }

            var rowCount = tableDueDtSumm.rows.length;
            var cellCount = tableDueDtSumm.rows[0].cells.length;
            var row = tableDueDtSumm.insertRow(rowCount);
            //  var cell_1 = row.insertCell(0);
            rowCount = rowCount + 1;


            for (i = 1; i <= 5; i++) {
                var cell_1 = row.insertCell(i - 1);
                if (i % 2 == 0) {
                    var element_1 = document.createElement("span");
                    element_1.name = 'lbDueDtsSummary_' + rowCount + i;
                    element_1.id = 'lbDueDtsSummary_' + rowCount + i;
                    element_1.type = "span";
                    element_1.innerHTML = "--";
                    cell_1.className = "tdspceing";
                }
                else {
                    var element_1 = document.createElement("span");
                    element_1.type = "span";
                    element_1.name = 'lbDueDtsSummary_' + rowCount + i;
                    element_1.id = 'lbDueDtsSummary_' + rowCount + i;

                    switch (i) {
                        case 1:

                            cell_1.className = "tdmain";
                            cell_1.id = "tdlbDueDtsSummary_" + TermType + "_" + rowCount + i;
                            $("#spPixelWidth").text(DueDate);
                            var StrWidth = $("#spPixelWidth").width();

                            if (StrWidth < 119) {
                                element_1.innerHTML = DueDate + "";
                                $("#" + cell_1.id).attr('class', 'tbNormal');
                            }
                            else {
                                element_1.innerHTML = DueDate;
                                $("#" + cell_1.id).attr('class', 'tbOnverFlow');
                            }
                            element_1.style.textAlign = "left";
                            break;
                        case 3:
                            cell_1.className = "tdspceing";
                            element_1.innerHTML = (parseFloat(CommPer)).toFixed(2) + '%';
                            element_1.style.textAlign = "right";
                            break;
                        case 5:
                            cell_1.className = "tdspceing";
                            element_1.innerHTML = FormatCurrency(parseFloat(CommAmt), "$");
                            element_1.style.textAlign = "right";
                            break;
                    }
                    cell_1.appendChild(element_1);
                }
                cell_1.appendChild(element_1);
            }

            //var element_1 = document.createElement("Lable");
            //element_1.type = "lable";
            //element_1.name = 'lbDueDtsSummary_' + rowCount;
            //element_1.id = 'lbDueDtsSummary_' + rowCount;
            //element_1.innerHTML = DueDate + '---------------' + parseFloat(CommPer).toFixed(2) + '% -----' + parseFloat(CommAmt).toFixed(2);
            //cell_1.appendChild(element_1);

            return false;
        }

        function SetSaveStatus(SaveStatus) {
            document.getElementById('<%= hdnSaveStatus.ClientID %>').value = SaveStatus;
        }

        function roundToTwo(num) {
            return +(Math.round(num + "e+2") + "e-2");
        }

        function FormatCurrency(n, currency) {

            return currency + " " + n.toFixed(2).replace(/./g, function (c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
            });
        }


    </script>
    <style>
        .table tr td {
            display: inline-block;
            vertical-align: top;
        }

        .LabelStyle {
            padding-left: 20PX;
            font-weight: bold;
            width: 35%;
        }

        .PdfBorder {
            border: 3px double black;
        }
    </style>
    <asp:ScriptManager ID="SM" runat="server"></asp:ScriptManager>
    <%-- <rsweb:ReportViewer ID="rptReport" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="602px" Height="16px">
        <LocalReport ReportPath="Reports\ChargeSlipSales.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataChargeSlipSales" Name="DataSet1" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataChargeSlipComm" Name="DataSet3" />
                <rsweb:ReportDataSource DataSourceId="ObjectDataChargeSlipDueDates" Name="DataSet2" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataChargeSlipDueDates" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="ChargeSlipBrokerCommissionTableAdapters.usp_CommissionDueDatesGetListAsPerChargeSlipIdTableAdapter">
        <SelectParameters>
            <asp:Parameter DefaultValue="1528" Name="ChargeSlipId" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataChargeSlipComm" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="ChargeSlipBrokerCommissionTableAdapters.usp_ChargeSlipCommissionTermsGetDetailsTableAdapter">
        <SelectParameters>
            <asp:Parameter DefaultValue="1528" Name="ChargeSlipId" Type="Int64" />
            <asp:Parameter DefaultValue="1" Name="TermTypeId" Type="Byte" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ObjectDataChargeSlipSales" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="ChargeSlipSalesTableAdapters.rpt_ChargeSlipTableAdapter">
        <SelectParameters>
            <asp:Parameter DefaultValue="1528" Name="ChargeSlipID" Type="Int64" />
        </SelectParameters>
    </asp:ObjectDataSource>--%>
    <table style="width: 100%">
        <tr>
            <td style="width: 60%; vertical-align: top;">
                <div style="border: solid 1px; overflow-y: scroll; height: 670px;" runat="server" id="dvReview">
                    <table id="Table1" runat="server">
                        <%--style="width: 100%; font-weight: normal; font-size: small;"--%>
                        <%--  <tr>
                            <td style="height: 15px;" colspan="2"></td>class="OuterBorder"
                        </tr>--%>
                        <tr style="display: none;" id="PageTitle" runat="server">
                            <td colspan="6" align="center" style="font-size: large; font-weight: 900;">Charge Slip Review</td>

                        </tr>
                        <tr>
                            <td class="LabelStyle" style="text-align: left; width: 10%;">Charge Date:</td>
                            <td style="text-align: left;" colspan="5">
                                <asp:Label runat="server" ID="lblChargeDate"></asp:Label></td>

                        </tr>
                        <tr>
                            <td class="LabelStyle">Deal Name:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblDealName"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;">
                            <td class="LabelStyle" style="vertical-align: top;" valign="top">Location:
                            </td>
                            <td style="vertical-align: top;" colspan="5" valign="top">
                                <asp:Label runat="server" ID="lblLocation"></asp:Label></td>
                        </tr>
                        <tr id="tr_Size">
                            <td class="LabelStyle">
                                <asp:Label ID="lblLandSize" runat="server" Text="Land Size:"></asp:Label>
                            </td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblSize"></asp:Label></td>
                        </tr>
                        <tr id="tr_BldgSize">
                            <td class="LabelStyle">Bldg Size:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblBldgSize"></asp:Label></td>
                        </tr>
                        <tr id="tr_Use">
                            <td class="LabelStyle">Use:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblUse"></asp:Label></td>
                        </tr>
                        <tr id="tr_Title">
                            <td id="td_TenantOrBuyerTitle" class="LabelStyle">Tenant:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblTenantOrBuyer"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="LabelStyle">Deal type:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblDealType"></asp:Label></td>
                        </tr>
                        <tr id="tr_DealSubType">
                            <td class="LabelStyle">Deal Subtype:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblDealSubType"></asp:Label></td>
                        </tr>
                        <tr id="tr_cam_ins_tax" runat="server">
                            <td id="td_cam_ins_tax" class="LabelStyle">Nets:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblcam_ins_tax"></asp:Label></td>
                        </tr>
                        <tr id="tr_NetsOrSalePrice" runat="server">
                            <td id="td_NetsOrSalePriceTitle" class="LabelStyle" style="vertical-align: top;"></td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblNetsOrSalePrice"></asp:Label>
                                <asp:GridView ID="grdLeaseTerm" runat="server" AutoGenerateColumns="false" Visible="false" OnRowDataBound="grdLeaseTerm_RowDataBound" ShowFooter="true">
                                    <Columns>
                                        <asp:BoundField DataField="RentPeriodFrom" HeaderText="Period From" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px" HeaderStyle-Width="90px" HeaderStyle-CssClass="leaseTermHeader" />
                                        <asp:BoundField DataField="RentPeriodTo" HeaderText="Period To" ItemStyle-Width="90px" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="90px" HeaderStyle-CssClass="leaseTermHeader" />
                                        <asp:TemplateField ItemStyle-Width="12%" HeaderText="RENT(PSF)" HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                $<asp:Label ID="lbItemRentPSP" runat="server" Text='<%#String.Format("{0:n2}",Eval("RentPerSqFt")) %>' Style="text-align: right;"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="% INCREASE" HeaderStyle-Wrap="true" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="105px" HeaderStyle-Width="105px" HeaderStyle-CssClass="leaseTermIncrease">
                                            <ItemTemplate>
                                                <asp:Label ID="lbItemPerIncrese" runat="server" Text='<%#String.Format("{0:n2}",Eval("PercentIncrease")) %>' Style="text-align: right;"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="18%" HeaderText="PERIOD RENT" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                $<asp:Label ID="lbItemPeriodRent" runat="server" Text='<%#String.Format("{0:n2}",Eval("PeriodRent")) %>' Style="text-align: right;"></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                $<asp:Label ID="lblPeriodRent" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>

                                    </Columns>

                                </asp:GridView>
                            </td>
                        </tr>
                        <tr id="tr_TI">
                            <td class="LabelStyle">TI:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblTI"></asp:Label></td>
                        </tr>
                        <tr id="tr_FreeRent">
                            <td class="LabelStyle">Free Rent:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblFreeRent"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;">
                            <td class="LabelStyle" style="vertical-align: top;" valign="top">Charge To:
                                <%-- <asp:Label runat="server" ID="lblChargeToTitle"></asp:Label>--%>
                               
                            </td>
                            <td id="td_ChargeTo" colspan="5" valign="top">
                                <asp:Label runat="server" ID="lblChargeTo"></asp:Label>
                            </td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_LeseStart">
                            <td class="LabelStyle">Lease Start:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblLeaseStart"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_AggregateRent" runat="server">
                            <td class="LabelStyle">Aggregate Rent:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblAggregateRent"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_Commissionable" runat="server">
                            <td class="LabelStyle">Commissionable:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblCommissionable"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_Commissionper" runat="server">
                            <td class="LabelStyle">Commission:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblCommissionper"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_BaseTerm">
                            <td class="LabelStyle">Base Term:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblBaseTerm"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_LeaseDueTot">
                            <td class="LabelStyle">Total Due:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblTotalLeaseDue"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_LeaseDueDate">
                            <td class="LabelStyle" valign="top">Due Date(s):</td>
                            <td id="td_LeaseDueDates" colspan="5" valign="top">
                                <asp:Label runat="server" ID="lblLeaseDueDates"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_OptTerms">
                            <td class="LabelStyle">Options Term 1:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblOptTerm"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_OptDueDueTot">
                            <td class="LabelStyle">Total Due:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblTotalOpteDue"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_OptDueDate">
                            <td class="LabelStyle" valign="top">Due Date(s):</td>
                            <td id="td_OptDueDates" colspan="5" valign="top">
                                <asp:Label runat="server" ID="lblOpteDueDate"></asp:Label></td>
                        </tr>

                        <tr style="vertical-align: top;" id="tr_OptTerms1">
                            <td class="LabelStyle">Options Term 2:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblOptTerm1"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_OptDueDueTot1">
                            <td class="LabelStyle">Total Due:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblTotalOpteDue1"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_OptDueDate1">
                            <td class="LabelStyle" valign="top">Due Date(s):</td>
                            <td id="td_OptDueDates1" colspan="5" valign="top">
                                <asp:Label runat="server" ID="lblOpteDueDate1"></asp:Label></td>
                        </tr>

                        <tr style="vertical-align: top;" id="tr_OptTerms2">
                            <td class="LabelStyle">Options Term 3:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblOptTerm2"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_OptDueDueTot2">
                            <td class="LabelStyle">Total Due:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblTotalOpteDue2"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_OptDueDate2">
                            <td class="LabelStyle" valign="top">Due Date(s):</td>
                            <td id="td_OptDueDates2" colspan="5" valign="top">
                                <asp:Label runat="server" ID="lblOpteDueDate2"></asp:Label></td>
                        </tr>

                        <tr style="vertical-align: top;" id="tr_OptTerms3">
                            <td class="LabelStyle">Options Term 4:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblOptTerm3"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_OptDueDueTot3">
                            <td class="LabelStyle">Total Due:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblTotalOpteDue3"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_OptDueDate3">
                            <td class="LabelStyle" valign="top">Due Date(s):</td>
                            <td id="td_OptDueDates3" colspan="5" valign="top">
                                <asp:Label runat="server" ID="lblOpteDueDate3"></asp:Label></td>
                        </tr>

                        <tr style="vertical-align: top;" id="tr_ContiTerms">
                            <td class="LabelStyle">Contingent Term:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblConTerm"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_ContiDueDueTot">
                            <td class="LabelStyle">Total Due:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblTotalConeDue"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_ContiDueDueDates">
                            <td class="LabelStyle" valign="top">Due Date(s):</td>
                            <td id="td_ContiDueDueDates" colspan="5" valign="top">
                                <asp:Label runat="server" ID="lblContigentDueDate"></asp:Label></td>
                        </tr>
                        <tr style="vertical-align: top;">
                            <td class="LabelStyle">Notify Me Times:</td>
                            <td>
                                <asp:Label runat="server" ID="lblNotifyMeTimes"></asp:Label>
                            </td>
                        </tr>
                        <tr style="vertical-align: top;" id="tr_CoBroker">
                            <td class="LabelStyle">Co-brokers:</td>
                            <td colspan="5">
                                <asp:Label runat="server" ID="lblCoBroker"></asp:Label></td>
                        </tr>
                        <tr id="tr_Comments">
                            <td class="LabelStyle" valign="top">Comments:</td>
                            <td colspan="5" valign="top">
                                <asp:Label runat="server" ID="lblComments"></asp:Label></td>
                        </tr>
                        <tr id="tr_AddComments" valign="top">
                            <td class="LabelStyle">Additional Notes:</td>
                            <td colspan="5" valign="top">
                                <asp:Label runat="server" ID="lblAddComments"></asp:Label></td>
                        </tr>
                        <tr id="tr_Attachments" runat="server">
                            <td class="LabelStyle">Attachments:</td>
                            <td id="td_Attachments" colspan="5">
                                <asp:UpdatePanel ID="upAttach" runat="server">
                                    <ContentTemplate>
                                        <div id="dvAttachment" runat="server">
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <%--  //<asp:Label runat="server" ID="Label20"></asp:Label></td>--%>
                        </tr>

                    </table>
                </div>
            </td>

            <td style="width: 40%; vertical-align: top;">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 100%">
                            <%--    <div>--%>
                            <%--  <uc:ucBase ID="BaseCommission" runat="server" />--%>
                            <uc:ucReviewSummary ID="ucReviewSumm" runat="server" />
                            <%--</div>--%>
                        </td>
                    </tr>
                    <%--  <tr>
                        <td style="height: 8px; width: 100%"></td>
                    </tr>--%>
                    <%--   <tr>
                        <td style="width: 100%">
                            <div>
                                <uc:ucOption ID="OptionCommission" runat="server" />
                            </div>
                        </td>
                    </tr>class="PopupDivBodyStyle" style="height: auto; display: none;"--%>
                </table>
            </td>
        </tr>
    </table>

    <div id="dvPrivacySettings" runat="server" title="PRIVACY SETTINGS" style="width: 430px; padding-left: 1px; padding-right: 1px;">
        <table style="width: 100%; font-size: smaller;">
            <%-- <tr style="border-bottom: solid;">
                <td style="text-align: left;">PRIVACY SETTINGS
                </td>
                <td align="right">
                    <asp:ImageButton ImageUrl="~/Images/close.png" ID="btnCancel" runat="server" OnClientClick="javascript:window.close();return false;" />
                </td>
            </tr>--%>
            <tr>
                <td>
                    <%--<p>--%>
                        YOUR CHARGE SLIP HAS BEEN SAVED AND SENT TO 
                    ACCOUNTING.
                    <%--</p>--%>
                    <p>
                        PROPERTY DETAILS AND SALES/LEASE TERMS FROM THIS 
                        CHARGE SLIP WILL BE PUSHED TO THE GEO APPLICATION.
                    </p>
                    <p>
                        PLEASE SELECT THE USERS WHO ARE GIVEN PERMISSION TO 
                        VIEW THE SALES/LEASE DETAILS.
                    </p>
                    <br />
                    <%--<input id="lblerr" runat="server" visible="false" value="wass up doc?" /><br />--%>

                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="btnSelAllUsers" class="SqureButton" value="SELECT ALL" />
                    <%-- onclick="ChkAll();"--%>
                </td>
            </tr>
            <tr>
                <td style="text-align: left;">
                    <div style="height: 200px; overflow-y: scroll; border: solid 1px black;">
                        <asp:CheckBoxList ID="chkUsers" runat="server" DataTextField="Users" DataValueField="UserID"></asp:CheckBoxList>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Button ID="btnSaveUsers" class="SqureButton" Text="  SAVE  " runat="server" OnClick="btnSaveUsers_Click" OnClientClick="SelectPropertyAccessUsers();" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divEditedGenerated" runat="server" title="Edited Chargeslip" style="width: 430px; padding-left: 1px; padding-right: 1px;">
        <table style="width: 100%; font-size: smaller;">

            <tr>
                <td style="text-align: left;">
                    <div style="height: 50px;">
                        <asp:Label ID="lblDealNameEditRegenerate" runat="server" Text=""></asp:Label>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Button ID="btnEditRegenerated" runat="server" Text="OK" CssClass="SqureButton" Width="100px" OnClientClick="NavigateEditRegenerated()" />

                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField ID="hdnPropertyAccessUsers" runat="server" />
    <asp:Button ID="btnEditRegenerateClick" runat="server" Style="display: none;" OnClick="btnEditRegenerateClick_Click" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" runat="Server">
    <table style="width: 100%;">
        <tr>
            <td style="width: 50%; text-align: left;">
                <label class="whitelabel"><b>PAGE 4 OF 4</b></label>

                <asp:Label ID="lbModificationHistory" runat="server" Text=""></asp:Label>
            </td>
            <td style="width: 50%; text-align: right;">
                <asp:Button ID="btnSaveForLater" runat="server" Text="SAVE FOR LATER" OnClick="btnNext_Click" BackColor="#808080" CssClass="SqureButton" OnClientClick="SetSaveStatus('D');" />
                &nbsp; 
                <asp:Button ID="btnBack" runat="server" CssClass="btnSmallOrange" Text="BACK" OnClick="btnBack_Click" />
                &nbsp; 
                <asp:Button ID="btnNext" runat="server" BackColor="Green" Text="SAVE & SEND" Width="120px" CssClass="SqureButton" OnClick="btnNext_Click" OnClientClick="ShowPopUp();return false;" />
                <asp:HiddenField ID="hdnSaveStatus" runat="server" Value="N" />
                <ajax:ModalPopupExtender ID="mpPrivacySettings" TargetControlID="lbDummybtn" CancelControlID="btnCancel"
                    PopupControlID="divNwCenterOrCompany" runat="server" BackgroundCssClass="modalBackground">
                </ajax:ModalPopupExtender>
                <asp:Label ID="lbDummybtn" runat="server"></asp:Label>
                <asp:Button ID="btnSendMail" runat="server" OnClick="btnSendMail_Click" CssClass="DisplayNone" />
                <asp:Button ID="btnResend" runat="server" BackColor="Green" Text="SAVE & RESEND" Width="120px" CssClass="SqureButton" Visible="false" OnClientClick="ShowEditedRegenerated();return false;" />
                <%-- <asp:Button ID="btnReport" runat="server" Text="Report" OnClick="btnReport_Click" />--%>                
            </td>
        </tr>
    </table>
    <span id="spPixelWidth" style="visibility: hidden;"></span>
</asp:Content>

