﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true" CodeFile="AddOrEditNewChargeSlip.aspx.cs" Inherits="Pages_AddOrEditNewChargeSlip" %>


<asp:Content ID="contHeadCont" ContentPlaceHolderID="cntHeadTag" runat="server">
    <%-- <style type="text/css">
        .removeunderline {
            text-decoration: none;
        }

        .linkbut {
            color: black;
            font-weight: bold;
            text-decoration: none;
        }

        .lnkMsg {
            color: black;
            text-decoration: none;
        }
    </style>--%>
    <%-- <link href="../Styles/SiteTEST.css" rel="stylesheet" />
    <script src="../Js/CommonValidations.js"></script>--%>
    <script type="text/javascript">
        function SelectAllGridCheckboxes(headerchk, GridName) {
            debugger;
            var gvcheck = document.getElementById(GridName);
            var i;
            //Condition to check header checkbox selected or not if that is true checked all checkboxes
            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    var inputs = gvcheck.rows[i].getElementsByTagName('input');
                    inputs[0].checked = true;
                }
            }
                //if condition fails uncheck all checkboxes in gridview
            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    var inputs = gvcheck.rows[i].getElementsByTagName('input');
                    inputs[0].checked = false;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <table style="width: 100%">
        <tr>
            <%-- <td class="mainPageSubTitle">
            </td>--%>
            <td class="mainPageTitle">CHARGE SLIP
            </td>
        </tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <table style="width: 100%">
        <tr>
            <td colspan="2">

                <asp:Button ID="btnAddNewCharge" runat="server" OnClick="btnAddNewCharge_Click" Text="BEGIN NEW CHARGE SLIP" CssClass="SqureButton"></asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button runat="server" Text="DELETE" ID="btnDelete" OnClick="btnDelete_Click" OnClientClick='javascript:return confirm("Do You Want To Delete Selected Records");' CssClass="SqureButton"></asp:Button>
            </td>
            <td align="right">
                <asp:Label ID="lblPages" runat="server" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:UpdatePanel ID="upCS" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvChargeSlipsInProgress" runat="server" AutoGenerateColumns="false"
                            Width="100%" AllowPaging="true" PageSize="30"
                            OnPageIndexChanging="gvChargeSlipsInProgress_PageIndexChanging"
                            EmptyDataRowStyle-HorizontalAlign="Center"
                            OnRowCreated="gvChargeSlipsInProgress_RowCreated"
                            OnRowDataBound="gvChargeSlipsInProgress_RowDataBound"
                            CssClass="AllBorder"
                            HeaderStyle-CssClass="HeaderGridView"
                            RowStyle-CssClass="RowGridView"
                            FooterStyle-CssClass="FooterGridView"
                            PagerSettings-Visible="true"
                            PagerSettings-Mode="NextPrevious" RowStyle-Height="16px">
                            <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkAllChargeSlip" runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkChargeSlip" runat="server" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="LastUpdated" HeaderText="LAST UPDATED" />
                                <asp:BoundField DataField="DealName" HeaderText="CHARGE SLIPS IN-PROGRESS" />
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ImageUrl="~/Images/edit.jpg" ID="btnEdit" OnClick="btnEdit_Click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ImageUrl="~/Images/delete.gif" OnClientClick='javascript:return confirm("Do You Want To Delete This Record");' ID="delRecord" OnClick="delRecord_Click" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ChargeSlipID" />

                            </Columns>

                            <EmptyDataTemplate>
                                <b style="padding: 2px 5px  2px 25px; color: #000000; background-color: #FFFFFF; height: 26px; vertical-align: middle; cursor: pointer;">No records found matching
                                            search criteria or filter.</b>
                            </EmptyDataTemplate>
                            <PagerTemplate>
                            </PagerTemplate>
                            <PagerStyle CssClass="pagerStyle" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" runat="Server">
</asp:Content>

