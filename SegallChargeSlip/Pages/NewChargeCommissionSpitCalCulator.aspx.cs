﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;
using System.Web.Script.Services;
using System.Web.UI.HtmlControls;

public partial class Pages_NewChargeCommissionSpitCalCulator : System.Web.UI.Page
{
    //void Page_LoadComplete(object sender, EventArgs e)
    //{
    //    // call your download function
    //    string g = "testss";
    //    CommCalculator.SetAfterPageLoad();
    //}


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {

                // 08/12/2013 : SK : 
                //if (Session[GlobleData.User] == null)
                //{
                //    Session.Clear();
                //    Session.RemoveAll();
                //    Server.Transfer("Login.aspx");
                //    //Response.Redirect("Login.aspx");
                //}


                if (Session[GlobleData.NewChargeSlipObj] != null)
                {
                    ChargeSlip objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
                    Users objuser = new Users();
                    objuser.UserID = objChargeSlip.CreatedBy;
                    objuser.Load();
                    hdnCoBrokerCompany.Value = objuser.CompanyName;
                    //set status
                    if (GlobleData.cns_Status_Complete == objChargeSlip.Status)
                    {
                        Session[GlobleData.cns_Is_Complete] = true;
                        btnSaveForLater.Visible = false;
                    }

                    //if (objChargeSlip.DealDesignationTypesId == 3)
                    //{
                    //    HtmlTableRow rwCommCalculator = (HtmlTableRow)CommCalculator.FindControl("trCLBBrokerSection");
                    //    rwCommCalculator.Style.Add("display", "table-row");
                    //    HtmlTableRow rwOptCommCalculator = (HtmlTableRow)OptCommCalculator.FindControl("trCLBBrokerSection");
                    //    rwOptCommCalculator.Style.Add("display", "table-row");
                    //    HtmlTableRow rwContingentCommCalculator = (HtmlTableRow)ContingentCommCalculator.FindControl("trCLBBrokerSection");
                    //    rwContingentCommCalculator.Style.Add("display", "table-row");
                    //    HtmlTableRow rwOpt1CommCalculator = (HtmlTableRow)Opt1CommCalculator.FindControl("trCLBBrokerSection");
                    //    rwOpt1CommCalculator.Style.Add("display", "table-row");
                    //    HtmlTableRow rwOpt2CommCalculator = (HtmlTableRow)Opt2CommCalculator.FindControl("trCLBBrokerSection");
                    //    rwOpt2CommCalculator.Style.Add("display", "table-row");
                    //    HtmlTableRow rwOpt3CommCalculator = (HtmlTableRow)Opt3CommCalculator.FindControl("trCLBBrokerSection");
                    //    rwCommCalculator.Style.Add("display", "table-row");
                    //}

                    if (objChargeSlip.DealTransactionsProperty.DealType == 1 || objChargeSlip.DealTransactionsProperty.DealType == 3)
                    {
                        CommissionDueDates objCommissionDueDates = new CommissionDueDates();
                        objCommissionDueDates.ChargeSlipId = objChargeSlip.ChargeSlipID;
                        DataTable dt = objCommissionDueDates.GetListAsPerChargeSlipId();
                        decimal TotCommission = 0;
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                TotCommission = TotCommission + Convert.ToDecimal(dr["CommAmount"].ToString());
                            }
                        }
                        Session[GlobleData.cns_totBaseLeaseComm] = TotCommission.ToString("#.##");
                        hdnLeaseCommTot.Value = TotCommission.ToString("#.##");
                        hdnOptCommTot.Value = "0";
                        hdnContigentTot.Value = "0";

                        hdnOptCommTot1.Value = "0";
                        hdnOptCommTot2.Value = "0";
                        hdnOptCommTot3.Value = "0";

                        //totCommission.Text = Convert.ToDecimal(hdnLeaseCommTot.Value).ToString(@"$#,##0.00;$\(#,##0.00\)");
                        totCommission.Text = hdnLeaseCommTot.Value == "" ? "0" : Convert.ToDecimal(hdnLeaseCommTot.Value).ToString();
                    }
                    else if (objChargeSlip.DealTransactionsProperty.DealType == 2)
                    {


                        CommissionDueDates objCommissionDueDates = new CommissionDueDates();
                        objCommissionDueDates.ChargeSlipId = objChargeSlip.ChargeSlipID;
                        DataTable dt = objCommissionDueDates.GetListAsPerChargeSlipId();
                        decimal TotCommission = 0, TotOptCommission = 0, TotConCommission = 0;
                        decimal TotOptCommission1 = 0, TotOptCommission2 = 0, TotOptCommission3 = 0;
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (dr["TermTypeId"].ToString() == "1")
                                {
                                    TotCommission = TotCommission + Convert.ToDecimal(dr["CommAmount"].ToString());
                                }
                                else if (dr["TermTypeId"].ToString() == "2")
                                {
                                    TotOptCommission = TotOptCommission + Convert.ToDecimal(dr["CommAmount"].ToString());
                                }
                                else if (dr["TermTypeId"].ToString() == "3")
                                {
                                    TotConCommission = TotConCommission + Convert.ToDecimal(dr["CommAmount"].ToString());
                                }
                                else if (dr["TermTypeId"].ToString() == "4")
                                {
                                    TotOptCommission1 = TotOptCommission1 + Convert.ToDecimal(dr["CommAmount"].ToString());
                                }
                                else if (dr["TermTypeId"].ToString() == "5")
                                {
                                    TotOptCommission2 = TotOptCommission2 + Convert.ToDecimal(dr["CommAmount"].ToString());
                                }
                                else if (dr["TermTypeId"].ToString() == "6")
                                {
                                    TotOptCommission3 = TotOptCommission3 + Convert.ToDecimal(dr["CommAmount"].ToString());
                                }
                            }
                        }
                        Session[GlobleData.cns_totBaseLeaseComm] = TotCommission.ToString("#.##");
                        Session[GlobleData.cns_totOptComm] = TotOptCommission.ToString("#.##");
                        Session[GlobleData.cns_totContingentComm] = TotConCommission.ToString("#.##");

                        Session[GlobleData.cns_totOptComm1] = TotOptCommission1.ToString("#.##");
                        Session[GlobleData.cns_totOptComm2] = TotOptCommission2.ToString("#.##");
                        Session[GlobleData.cns_totOptComm3] = TotOptCommission3.ToString("#.##");

                        if (Session[GlobleData.cns_totBaseLeaseComm] == "")
                        {
                            Session[GlobleData.cns_totBaseLeaseComm] = "0";
                        }
                        if (Session[GlobleData.cns_totOptComm] == "")
                        {
                            Session[GlobleData.cns_totOptComm] = "0";
                        }
                        if (Session[GlobleData.cns_totContingentComm] == "")
                        {
                            Session[GlobleData.cns_totContingentComm] = "0";
                        }
                        if (Session[GlobleData.cns_totOptComm1] == "")
                        {
                            Session[GlobleData.cns_totOptComm1] = "0";
                        }
                        if (Session[GlobleData.cns_totOptComm2] == "")
                        {
                            Session[GlobleData.cns_totOptComm2] = "0";
                        }
                        if (Session[GlobleData.cns_totOptComm3] == "")
                        {
                            Session[GlobleData.cns_totOptComm3] = "0";
                        }
                        hdnLeaseCommTot.Value = Convert.ToDecimal(Session[GlobleData.cns_totBaseLeaseComm]).ToString();
                        hdnOptCommTot.Value = Convert.ToDecimal(Session[GlobleData.cns_totOptComm]).ToString();
                        hdnContigentTot.Value = Convert.ToDecimal(Session[GlobleData.cns_totContingentComm]).ToString();
                        totCommission.Text = Convert.ToDecimal(hdnLeaseCommTot.Value).ToString();

                        hdnOptCommTot1.Value = Convert.ToDecimal(Session[GlobleData.cns_totOptComm1]).ToString();
                        hdnOptCommTot2.Value = Convert.ToDecimal(Session[GlobleData.cns_totOptComm2]).ToString();
                        hdnOptCommTot3.Value = Convert.ToDecimal(Session[GlobleData.cns_totOptComm3]).ToString();
                    }
                }

                //hdnLeaseCommTot.Value = "1200";
                //hdnOptCommTot.Value = "";
                //hdnContigentTot.Value = "1500";
                //totCommission.Text = "100000";

                //TEST Edit
                if (Session[GlobleData.NewChargeSlipId] != null)
                {
                    int CSID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                    if (CSID > 0)
                    {

                        Boolean IsEdit = CommCalculator.GetCommissionSpilCalRecordsForSelChargeSlip(CommCalculator.ClientID, hdnLeaseCommTot.Value);
                        if (IsEdit == false)
                        {
                            decimal CommAmt = hdnLeaseCommTot.Value == "" ? 0 : Convert.ToDecimal(hdnLeaseCommTot.Value);
                            if (CommAmt != 0)
                            {
                                CommCalculator.SaveAllBrokerCommission(1, CommAmt);
                            }

                        }
                        IsEdit = true;
                        IsEdit = OptCommCalculator.GetCommissionSpilCalRecordsForSelChargeSlip(OptCommCalculator.ClientID, hdnOptCommTot.Value);
                        if (!IsEdit)
                        {
                            decimal CommAmt = hdnOptCommTot.Value == "" ? 0 : Convert.ToDecimal(hdnOptCommTot.Value);
                            if (CommAmt != 0)
                            {
                                OptCommCalculator.SaveAllBrokerCommission(2, CommAmt);
                            }
                        }
                        IsEdit = true;
                        IsEdit = ContingentCommCalculator.GetCommissionSpilCalRecordsForSelChargeSlip(ContingentCommCalculator.ClientID, hdnContigentTot.Value);
                        if (!IsEdit)
                        {
                            decimal CommAmt = hdnContigentTot.Value == "" ? 0 : Convert.ToDecimal(hdnContigentTot.Value);
                            if (CommAmt != 0)
                            {
                                ContingentCommCalculator.SaveAllBrokerCommission(3, CommAmt);
                            }
                        }

                        IsEdit = true;
                        IsEdit = Opt1CommCalculator.GetCommissionSpilCalRecordsForSelChargeSlip(Opt1CommCalculator.ClientID, hdnOptCommTot1.Value);
                        if (!IsEdit)
                        {
                            decimal CommAmt = hdnOptCommTot1.Value == "" ? 0 : Convert.ToDecimal(hdnOptCommTot1.Value);
                            if (CommAmt != 0)
                            {
                                Opt1CommCalculator.SaveAllBrokerCommission(4, CommAmt);
                            }
                        }
                        IsEdit = true;
                        IsEdit = Opt2CommCalculator.GetCommissionSpilCalRecordsForSelChargeSlip(Opt2CommCalculator.ClientID, hdnOptCommTot2.Value);
                        if (!IsEdit)
                        {
                            decimal CommAmt = hdnOptCommTot2.Value == "" ? 0 : Convert.ToDecimal(hdnOptCommTot2.Value);
                            if (CommAmt != 0)
                            {
                                Opt2CommCalculator.SaveAllBrokerCommission(5, CommAmt);
                            }
                        }
                        IsEdit = true;
                        IsEdit = Opt3CommCalculator.GetCommissionSpilCalRecordsForSelChargeSlip(Opt3CommCalculator.ClientID, hdnOptCommTot3.Value);
                        if (!IsEdit)
                        {
                            decimal CommAmt = hdnOptCommTot3.Value == "" ? 0 : Convert.ToDecimal(hdnOptCommTot3.Value);
                            if (CommAmt != 0)
                            {
                                Opt3CommCalculator.SaveAllBrokerCommission(6, CommAmt);
                            }
                        }
                    }
                }
                //
                //  Page.LoadComplete += new EventHandler(Page_LoadComplete);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "Script", "javascript:alert(' Page--hiiii--1')", true);
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeCommissionSpitCalCulator.aspx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #region Web Methods

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] SearchBrokerComapny(string prefixText, int count, string contextKey)
    {
        List<string> lstSearchList = new List<string>();
        try
        {
            int BrokerType = 0;
            if (contextKey.Contains("ExternalBroker"))
            {
                BrokerType = 1;
            }
            if (contextKey.Contains("MasterBroker"))
            {
                BrokerType = 2;
            }
            if (contextKey.Contains("ReferralBroker"))
            {
                BrokerType = 3;
            }
            if (contextKey.Contains("Co_Broker"))
            {
                BrokerType = 4;
            }
            if (contextKey.Contains("Clb_Broker"))
            {
                BrokerType = 4;
            }

            Brokers objBrokers = new Brokers();
            objBrokers.CompanyName = prefixText;
            DataSet ds = objBrokers.GetBrokerCompSeachList("D", BrokerType); //put D
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dt in ds.Tables[0].Rows)
                {
                    //dt["ValueType"].ToString()
                    lstSearchList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dt["CompanyName"].ToString(), dt["UserID"].ToString()));
                    // lstSearchList.Add(dt[1].ToString());  dt["BrokerId"].ToString())
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("CommissionSpiltCalculator.aspx", "SearchBrokerComapny", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
        return lstSearchList.ToArray();
    }


    [System.Web.Services.WebMethod(EnableSession = true)]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json,UseHttpGet = true, XmlSerializeString = false)]
    public static string CheckDuplicateEntryForSelBroker(string txtCompany, string txtAddress, string txtCity, string ddlState, string txtZipCode, string txtEin, string txtEmail, string txtName, string txtShare, string txtSetAmount, string hdnBrokerId, string hdnBrokerType)
    {
        try
        {
            Brokers objBrokers = new Brokers();
            if (!string.IsNullOrEmpty(hdnBrokerId))
            {
                objBrokers.BrokerId = Convert.ToInt32(hdnBrokerId);

            }
            string BrokerType = hdnBrokerType;


            if (hdnBrokerType.Contains("ExternalBroker"))
            {
                objBrokers.BrokerTypeId = 1;
            }
            if (hdnBrokerType.Contains("MasterBroker"))
            {
                objBrokers.BrokerTypeId = 2;
            }
            if (hdnBrokerType.Contains("ReferralBroker"))
            {
                objBrokers.BrokerTypeId = 3;
            }
            if (hdnBrokerType.Contains("Co_Broker"))
            {
                objBrokers.BrokerTypeId = 4;
            }
            if (hdnBrokerType.Contains("Clb_Broker"))
            {
                objBrokers.BrokerTypeId = 4;
            }

            objBrokers.CompanyName = txtCompany;
            objBrokers.Address = txtAddress;
            objBrokers.City = txtCity;
            objBrokers.State = ddlState;
            objBrokers.Zipcode = txtZipCode;
            objBrokers.EIN = txtEin;
            objBrokers.Email = txtEmail;
            objBrokers.Name = txtName;


            objBrokers.SharePercent = Convert.ToDouble(txtShare);
            objBrokers.SetAmount = Convert.ToDecimal(txtSetAmount);
            DataSet ds = objBrokers.CheckForDuplicateBrokerEntry();
            return ds.GetXml();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeCommissionSpitCalCulator.aspx", "CheckDuplicateEntryForSelBroker", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return null;
        }


    }


    [System.Web.Services.WebMethod(EnableSession = true)]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json,UseHttpGet = true, XmlSerializeString = false)]
    public static int[] SaveBroker(string txtCompany, string txtAddress, string txtCity, string ddlState, string txtZipCode, string txtEin, string txtEmail, string txtName, string txtShare, string txtSetAmount, string hdnBrokerId, string hdnBrokerType, string TermType)
    {
        try
        {
            TermType = TermType.Replace("ContentMain_", "");
            int termTyp = 0;
            if (TermType == "CommCalculator")
            {
                termTyp = 1;
            }
            else if (TermType == "OptCommCalculator")
            {
                termTyp = 2;
            }
            else if (TermType == "ContingentCommCalculator")
            {
                termTyp = 3;
            }
            else if (TermType == "Opt1CommCalculator")
            {
                termTyp = 4;
            }
            else if (TermType == "Opt2CommCalculator")
            {
                termTyp = 5;
            }
            else if (TermType == "Opt3CommCalculator")
            {
                termTyp = 6;
            }

            Brokers objBrokers = new Brokers();

            if (!string.IsNullOrEmpty(hdnBrokerId))
            {
                objBrokers.BrokerId = Convert.ToInt32(hdnBrokerId);
            }
            objBrokers.Load();
            string BrokerType = hdnBrokerType;

            int BrokerTypeId = 0;
            if (hdnBrokerType.Contains("ExternalBroker"))
            {
                // objBrokers.BrokerTypeId = 1;//Commented by Jaywanti on 17-09-2015
                if (objBrokers.BrokerId == null || objBrokers.BrokerId == 0)
                {
                    objBrokers.BrokerTypeId = 1;//added by Jaywanti on 17-09-2015
                }
                BrokerTypeId = 1;
            }
            if (hdnBrokerType.Contains("MasterBroker"))
            {
                // objBrokers.BrokerTypeId = 2;
                if (objBrokers.BrokerId == null || objBrokers.BrokerId == 0)
                {
                    objBrokers.BrokerTypeId = 2;//added by Jaywanti on 17-09-2015
                }
                BrokerTypeId = 2;
            }
            if (hdnBrokerType.Contains("ReferralBroker"))
            {
                // objBrokers.BrokerTypeId = 3;
                if (objBrokers.BrokerId == null || objBrokers.BrokerId == 0)
                {
                    objBrokers.BrokerTypeId = 3;//added by Jaywanti on 17-09-2015
                }
                BrokerTypeId = 3;
            }
            //In Users Table we are storing Broker Type Id as 4 for Company Landlord Broker
            if (hdnBrokerType.Contains("Clb_Broker"))
            {
                if (objBrokers.BrokerId == null || objBrokers.BrokerId == 0)
                {
                    objBrokers.BrokerTypeId = 4;
                }
                BrokerTypeId = 4;
            }
            if (hdnBrokerType.Contains("Co_Broker"))
            {
                // objBrokers.BrokerTypeId = 4;
                if (objBrokers.BrokerId == null || objBrokers.BrokerId == 0)
                {
                    objBrokers.BrokerTypeId = 4;//added by Jaywanti on 17-09-2015
                }
                BrokerTypeId = 4;
            }

            objBrokers.CompanyName = txtCompany;
            objBrokers.Address = txtAddress;
            objBrokers.City = txtCity;
            objBrokers.State = ddlState;
            objBrokers.Zipcode = txtZipCode;
            objBrokers.EIN = txtEin;
            objBrokers.Email = txtEmail;
            objBrokers.Name = txtName.Trim();

            objBrokers.SharePercent = Convert.ToDouble(txtShare);
            objBrokers.SetAmount = Convert.ToDecimal(txtSetAmount);
            objBrokers.Save();

            //In Deal Broker Commissions Table we are storing Broker Type Id as 5 for Company Landlord Broker(CLB)
            //But in Users Table we have Updated BrokerTypeId as 4 for CLB Brokers. For both this Brokers we are used same BrokerId
            if (hdnBrokerType.Contains("Clb_Broker"))
            {
                if (objBrokers.BrokerId == null || objBrokers.BrokerId == 0)
                {
                    objBrokers.BrokerTypeId = 5;
                }
                BrokerTypeId = 5;
            }

            //if (HttpContext.Current.Session[GlobleData.NewChargeSlipObj] != null)
            //{
            //    ChargeSlip objChargeslip = (ChargeSlip)HttpContext.Current.Session[GlobleData.NewChargeSlipObj];
            //    if (objChargeslip.DealDesignationTypesId == 1)
            //    {
            //        objBrokers.BrokerTypeId = 5;
            //        BrokerTypeId = 5;
            //    }
            //}

            //save in DealBrokerCommissions

            //DataTable dtBrokerCommission = (DataTable)HttpContext.Current.Session["BrokerCommission"];
            //DataRow dRow;         dRow = dtBrokerCommission.NewRow();
            DealBrokerCommissions objNewDealBrokerCommissions = new DealBrokerCommissions();
            objNewDealBrokerCommissions.BrokerId = Convert.ToInt32(objBrokers.BrokerId);
            objNewDealBrokerCommissions.BrokerTypeId = Convert.ToInt32(BrokerTypeId);
            objNewDealBrokerCommissions.ChargeSlipId = Convert.ToInt32(HttpContext.Current.Session[GlobleData.NewChargeSlipId]);
            objNewDealBrokerCommissions.DealTransactionId = Convert.ToInt32(HttpContext.Current.Session[GlobleData.DealTransactionId]);
            objNewDealBrokerCommissions.BrokerPercent = Convert.ToDecimal(txtShare);
            objNewDealBrokerCommissions.BrokerCommission = Convert.ToDecimal(txtSetAmount);
            objNewDealBrokerCommissions.TermTypeId = Convert.ToInt16(termTyp);
            objNewDealBrokerCommissions.Save();
            int[] result = new int[2];
            result[0] = objBrokers.BrokerId;
            result[1] = objNewDealBrokerCommissions.DealBrokerCommissionId;
            //if (objBrokers.BrokerTypeId == 4)
            //{
            //    UserDetails objUserDetails = new UserDetails();
            //    objUserDetails.UserID = Convert.ToInt32(objBrokers.BrokerId); 
            //    Boolean rst = objUserDetails.LoadByUserIdAndCurrentDate(Convert.ToInt64(HttpContext.Current.Session[GlobleData.NewChargeSlipId].ToString()));//CommAmount
            //    if (rst == true)
            //    {
            //        Co_BrokerPer = objUserDetails.PercentBroker;
            //        CompPer = objUserDetails.PercentCompany;
            //    }
            //}


            return result;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeCommissionSpitCalCulator.aspx", "SaveBroker", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return null;
        }

    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static Brokers FillBrokerInfo(string CmpName, int BrokerId)
    {
        try
        {
            //string BrokerId,
            DataSet ds;
            DataTable dt;
            Brokers objBrokers = new Brokers();
            if (BrokerId == 0)
            {
                objBrokers.BrokerId = Convert.ToInt32(HttpContext.Current.Session[GlobleData.User]);
                objBrokers.Load();
                Brokers objBroker = new Brokers();
                objBroker.CompanyName = objBrokers.CompanyName.ToString();
                return objBroker;
                //objBrokers.CompanyName = CmpName.Trim();
                //ds = objBrokers.GetBrokerCompSeachList("", 0);
                //dt = ds.Tables[0];
                //if (dt.Rows.Count == 0)
                //{
                //    objBrokers.BrokerId = Convert.ToInt32(dt.Rows[0]["BrokerId"].ToString());
                //    objBrokers.Load();
                //}
                //else if (dt.Rows.Count >= 1)
                //{
                //    objBrokers = new Brokers();
                //}
            }
            else
            {
                objBrokers.BrokerId = Convert.ToInt32(BrokerId);
                objBrokers.Load();
                if (objBrokers.BrokerTypeId == 4)
                {
                    UserDetails objUserDetails = new UserDetails();
                    objUserDetails.UserID = Convert.ToInt32(objBrokers.BrokerId);
                    Boolean rst = objUserDetails.LoadByUserIdAndCurrentDate(Convert.ToInt64(HttpContext.Current.Session[GlobleData.NewChargeSlipId].ToString()));//CommAmount
                    if (rst == true)
                    {
                        objBrokers.PercentBroker = objUserDetails.PercentBroker;
                    }
                }
            }
            return objBrokers;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeCommissionSpitCalCulator.aspx", "FillBrokerInfo", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return null;
        }

    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string FillBrokerInfoForSameCmp(string CmpName, string tableRef)
    {
        try
        {
            List<Brokers> lstBrokers = new List<Brokers>(); ;
            Brokers objBrokers = new Brokers();
            objBrokers.CompanyName = CmpName;
            if (tableRef.Contains("ExternalBroker"))
            {
                objBrokers.BrokerTypeId = 1;
            }
            if (tableRef.Contains("MasterBroker"))
            {
                objBrokers.BrokerTypeId = 2;
            }
            if (tableRef.Contains("ReferralBroker"))
            {
                objBrokers.BrokerTypeId = 3;
            }
            if (tableRef.Contains("Co_Broker"))
            {
                objBrokers.BrokerTypeId = 4;
            }
            DataSet ds = objBrokers.GetBrokerDetailsAsPerCompanyName();
            DataTable dt = ds.Tables[0];

            if (dt.Rows.Count != 0)
            {
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    Brokers objBrk = new Brokers();
                    objBrk.BrokerId = Convert.ToInt32(dt.Rows[i]["BrokerId"].ToString());
                    objBrk.Name = dt.Rows[i]["Name"].ToString();
                    objBrk.Address = dt.Rows[i]["Address"].ToString();
                    objBrk.SharePercent = Convert.ToDouble(dt.Rows[i]["SharePercent"].ToString());
                    objBrk.SetAmount = Convert.ToDecimal(dt.Rows[i]["SetAmount"].ToString());
                    lstBrokers.Add(objBrk);
                }
                objBrokers.BrokerId = Convert.ToInt32(dt.Rows[0]["BrokerId"].ToString());
                objBrokers.Load();
            }
            return ds.GetXml();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeCommissionSpitCalCulator.aspx", "FillBrokerInfoForSameCmp", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return null;
        }
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string FillCoBrokerInfoForSameCmp(string CmpName, string tableRef)
    {
        try
        {
            List<Brokers> lstBrokers = new List<Brokers>(); ;
            Brokers objBrokers = new Brokers();
            objBrokers.CompanyName = CmpName;
            if (tableRef.Contains("ExternalBroker"))
            {
                objBrokers.BrokerTypeId = 1;
            }
            if (tableRef.Contains("MasterBroker"))
            {
                objBrokers.BrokerTypeId = 2;
            }
            if (tableRef.Contains("ReferralBroker"))
            {
                objBrokers.BrokerTypeId = 3;
            }
            if (tableRef.Contains("Co_Broker"))
            {
                objBrokers.BrokerTypeId = 4;
            }
            if (tableRef.Contains("Clb_Broker"))
            {
                objBrokers.BrokerTypeId = 5;
            }
            DataSet ds = objBrokers.GetBrokerDetailsAsPerCompanyName();
            //if (dt.Rows.Count != 0)
            //{
            //    for (int i = 0; i <= dt.Rows.Count - 1; i++)
            //    {
            //        Brokers objBrk = new Brokers();
            //        objBrk.BrokerId = Convert.ToInt32(dt.Rows[i]["BrokerId"].ToString());
            //        objBrk.Name = dt.Rows[i]["Name"].ToString();
            //        objBrk.Address = dt.Rows[i]["Address"].ToString();
            //        objBrk.SharePercent = Convert.ToDouble(dt.Rows[i]["SharePercent"].ToString());
            //        objBrk.SetAmount = Convert.ToDecimal(dt.Rows[i]["SetAmount"].ToString());
            //        lstBrokers.Add(objBrk);
            //    }
            //    objBrokers.BrokerId = Convert.ToInt32(dt.Rows[0]["BrokerId"].ToString());
            //    objBrokers.Load();
            //}

            return ds.GetXml();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeCommissionSpitCalCulator.aspx", "FillCoBrokerInfoForSameCmp", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return null;
        }

    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string[] UpdateBrokerAmount(int BrokerId, float CommPer, float CommAmt, int DealBrokerCommissionId)
    {
        try
        {
            Users objBrokers = new Users();
            objBrokers.UserID = Convert.ToInt32(BrokerId);
            objBrokers.Load();
            objBrokers.SharePercent = CommPer;
            objBrokers.SetAmount = Convert.ToDecimal(CommAmt);
            objBrokers.Save();
            objBrokers.UserID = 0;

            DealBrokerCommissions objDealBrokerCommissions = new DealBrokerCommissions();
            objDealBrokerCommissions.DealBrokerCommissionId = DealBrokerCommissionId;
            objDealBrokerCommissions.Load();
            objDealBrokerCommissions.BrokerPercent = Convert.ToDecimal(CommPer);
            objDealBrokerCommissions.BrokerCommission = Convert.ToDecimal(CommAmt);
            objDealBrokerCommissions.Save();
            objDealBrokerCommissions.BrokerId = 0;


            // For Comapny Split Calculator

            UserDetails objUserDetails = new UserDetails();
            objUserDetails.UserID = Convert.ToInt32(HttpContext.Current.Session[GlobleData.User]);
            objUserDetails.LoadByUserIdAndCurrentDate(Convert.ToInt64(HttpContext.Current.Session[GlobleData.NewChargeSlipId].ToString()));

            string UserCommission, CompCommission;

            UserCommission = ((objDealBrokerCommissions.BrokerCommission * objUserDetails.PercentBroker) / 100).ToString("#.##");

            CompCommission = ((objDealBrokerCommissions.BrokerCommission * objUserDetails.PercentCompany) / 100).ToString("#.##");

            //string Threshold = objUserDetails.PercentBroker + " : " + objUserDetails.PercentCompany;

            List<string> lstResult = new List<string>();

            lstResult.Add(objUserDetails.PercentBroker.ToString());
            lstResult.Add(objUserDetails.PercentCompany.ToString());
            lstResult.Add(HttpContext.Current.Session["User"].ToString());
            lstResult.Add(objUserDetails.CompanyName);
            lstResult.Add(UserCommission);
            lstResult.Add(CompCommission);
            return lstResult.ToArray();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeCommissionSpitCalCulator.aspx", "UpdateBrokerAmount", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return null;
        }
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void DeleteBroker(int BrokerId, int DealBrokerCommissionId)
    {
        try
        {
            //Brokers objBrokers = new Brokers();
            //objBrokers.BrokerId = Convert.ToInt32(BrokerId);
            //objBrokers.Delete();

            DealBrokerCommissions objDealBrokerCommissions = new DealBrokerCommissions();
            objDealBrokerCommissions.DealBrokerCommissionId = DealBrokerCommissionId;
            objDealBrokerCommissions.Delete();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeCommissionSpitCalCulator.aspx", "DeleteBroker", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }


    //SaveDealBrokerCommissions

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SaveDealBrokerCommissions(DealBrokerCommissions data)
    {


    }


    #endregion

    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            DealBrokerCommissions objDealBrokerCommissions = new DealBrokerCommissions();
            objDealBrokerCommissions.ChargeSlipId = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);

            //Set Data as per Change in Commission FOR EACH TERM TYPE--TAB 
            if (hdnLeaseCommTot.Value != "")
            {
                objDealBrokerCommissions.TermTypeId = 1;
                objDealBrokerCommissions.SetCommAmountAsPerChangeInBaseAmt(Convert.ToDecimal(hdnLeaseCommTot.Value));
                objDealBrokerCommissions.SetCommAmountAsPerChangeInBaseAmt(Convert.ToDecimal(hdnLeaseCommTot.Value));
            }

            if (hdnOptCommTot.Value != "0")
            {
                objDealBrokerCommissions.TermTypeId = 2;
                objDealBrokerCommissions.SetCommAmountAsPerChangeInBaseAmt(Convert.ToDecimal(hdnOptCommTot.Value));
                objDealBrokerCommissions.SetCommAmountAsPerChangeInBaseAmt(Convert.ToDecimal(hdnOptCommTot.Value));
            }

            if (hdnContigentTot.Value != "0")
            {
                objDealBrokerCommissions.TermTypeId = 3;
                objDealBrokerCommissions.SetCommAmountAsPerChangeInBaseAmt(Convert.ToDecimal(hdnContigentTot.Value));
                objDealBrokerCommissions.SetCommAmountAsPerChangeInBaseAmt(Convert.ToDecimal(hdnContigentTot.Value));
            }

            if (hdnOptCommTot1.Value != "0")
            {
                objDealBrokerCommissions.TermTypeId = 4;
                objDealBrokerCommissions.SetCommAmountAsPerChangeInBaseAmt(Convert.ToDecimal(hdnOptCommTot1.Value));
                objDealBrokerCommissions.SetCommAmountAsPerChangeInBaseAmt(Convert.ToDecimal(hdnOptCommTot1.Value));
            }

            if (hdnOptCommTot2.Value != "0")
            {
                objDealBrokerCommissions.TermTypeId = 5;
                objDealBrokerCommissions.SetCommAmountAsPerChangeInBaseAmt(Convert.ToDecimal(hdnOptCommTot2.Value));
                objDealBrokerCommissions.SetCommAmountAsPerChangeInBaseAmt(Convert.ToDecimal(hdnOptCommTot2.Value));
            }

            if (hdnOptCommTot3.Value != "0")
            {
                objDealBrokerCommissions.TermTypeId = 6;
                objDealBrokerCommissions.SetCommAmountAsPerChangeInBaseAmt(Convert.ToDecimal(hdnOptCommTot3.Value));
                objDealBrokerCommissions.SetCommAmountAsPerChangeInBaseAmt(Convert.ToDecimal(hdnOptCommTot3.Value));
            }
            //CommCalculator.SaveAllBrokerCommission();

            if (hdnSaveStatus.Value == "N")
            {
                Server.Transfer("NewChargeSlipReview.aspx");
            }
            else if (hdnSaveStatus.Value == "D")
            {
                Utility.ClearSessionExceptUser();
                Response.Redirect("AddOrEditNewChargeSlip.aspx", false);
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeCommissionSpitCalCulator.aspx", "btnNext_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session[GlobleData.NewChargeSlipObj] != null)
            {
                ChargeSlip objChargeSlip = new ChargeSlip();
                objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];

                if (objChargeSlip.DealTransactionsProperty.DealType == 2)
                {
                    Response.Redirect("NewChargeLeaseTerms.aspx", false);
                }
                else
                {
                    Response.Redirect("NewCharge.aspx", false);
                }

            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeCommissionSpitCalCulator.aspx", "btnBack_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }



    #region New changes 9/23/19 start
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static Brokers FillEditBrokerInfoByBrokerId(int BrokerId)
    {
        try
        {
            Brokers objBrokers = new Brokers();
            if (BrokerId > 0)
            {
                objBrokers.BrokerId = Convert.ToInt32(BrokerId);
                objBrokers.Load();
                if (objBrokers.BrokerTypeId == 4)
                {
                    UserDetails objUserDetails = new UserDetails();
                    objUserDetails.UserID = Convert.ToInt32(objBrokers.BrokerId);
                    Boolean rst = objUserDetails.LoadByUserIdAndCurrentDate(Convert.ToInt64(HttpContext.Current.Session[GlobleData.NewChargeSlipId].ToString()));//CommAmount
                    if (rst == true)
                    {
                        objBrokers.PercentBroker = objUserDetails.PercentBroker;
                    }
                }
            }
            return objBrokers;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeCommissionSpitCalCulator.aspx", "FillBrokerInfo", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return null;
        }
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string[] UpdateBroker(int BrokerId, int DealBrokerCommissionId, string Address, string City, string State, string ZipCode,  string Name, string Email, string Ein)
    {
        try
        {
            Users objBrokers = new Users();
            objBrokers.UserID = Convert.ToInt32(BrokerId);
            objBrokers.Load();
            objBrokers.Address1 = Address;
            objBrokers.City = City;
            objBrokers.State = State;
            objBrokers.Zip = ZipCode;
            string[] nm = Name.Split(new[] { ' ' },2); // break name into 2 substrings when find 1st space char(' ')
            objBrokers.FirstName = nm[0];
            objBrokers.LastName = nm[1];
            objBrokers.Email = Email;
            objBrokers.EIN = Ein;

            objBrokers.Save();
            objBrokers.UserID = 0;

            string[] result = new string[1];
            result[0] =Convert.ToString(objBrokers.UserID);
            return result;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeCommissionSpitCalCulator.aspx", "UpdateBroker", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return null;
        }

    }


    #endregion
}