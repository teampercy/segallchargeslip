﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SegallChargeSlipBll;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Microsoft.Reporting.WebForms;
using SegallChargeSlipBLL;

public partial class Pages_NewChargeSlipReview : System.Web.UI.Page
{
    #region variables
    string BreakLine = "<br/>";
    decimal totPeriodRent = 0;
    Random rnd;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                SetChargeSlipReview();


            }
            catch (Exception ex)
            {
                ErrorLog.WriteLog("NewChargeSlipReview", "SetChargeSlipReview", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
                // throw;
            }
        }
    }

    #region Custom Methods

    private void SetChargeSlipReview()
    {
        try
        {
            if (Session[GlobleData.NewChargeSlipObj] != null)
            {
                if (ViewState["CheckAttachmentStatus"] != null)
                {
                    if (ViewState["CheckAttachmentStatus"].ToString() == "True")
                    {

                        tr_Attachments.Visible = true;
                        //tr_Attachments.Style.Add("display", "inline");

                    }
                    else
                    {
                        tr_Attachments.Visible = false;
                        //tr_Attachments.Style.Add("display", "none");
                    }
                }
                ChargeSlip objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];

                if (GlobleData.cns_Status_Complete == objChargeSlip.Status)
                {
                    btnSaveForLater.Visible = false;
                    btnNext.Visible = false;
                    btnResend.Visible = true;
                }


                //No Need Now---09/019/2013--get reords from charge slip 
                //Locations objLocations = new Locations();
                //objLocations.LocationID = objChargeSlip.LocationID;
                //objLocations.Load();



                // Get GeoApp Users For Privacy Settings
                //GetGeoAppUsers(objChargeSlip);
                GetGeoAppUsers();
                string date = Convert.ToDateTime(objChargeSlip.ChargeDate).ToShortDateString();
                lblChargeDate.Text = date;

                //lblChargeDate.Text = Convert.ToString(DateTime.ParseExact(objChargeSlip.ChargeDate.ToString(), "MM/dd/yyyy", null));
                lblDealName.Text = objChargeSlip.DealName;
                lblDealNameEditRegenerate.Text = " Chargeslip DealName <b>'" + lblDealName.Text + "</b> ' has been edited and regenerated Successfully.";
                string Loc;
                string LocShoppingCenter = string.IsNullOrEmpty(objChargeSlip.LocShoppingCenter) == false ? Convert.ToString(objChargeSlip.LocShoppingCenter) + BreakLine : "";
                string LocAddress1 = string.IsNullOrEmpty(objChargeSlip.LocAddress1) == false ? Convert.ToString(objChargeSlip.LocAddress1) + BreakLine : "";
                string LocAddress2 = string.IsNullOrEmpty(objChargeSlip.LocAddress2) == false ? Convert.ToString(objChargeSlip.LocAddress2) + BreakLine : "";
                string LocCounty = string.IsNullOrEmpty(objChargeSlip.LocCounty) == false ? Convert.ToString(objChargeSlip.LocCounty) + BreakLine : "";

                Loc = Convert.ToString(LocShoppingCenter) + LocAddress1 + LocAddress2 + LocCounty + Convert.ToString(objChargeSlip.LocCity) + " , " + Convert.ToString(objChargeSlip.LocState) + " , " + Convert.ToString(objChargeSlip.LocZipCode);

                lblLocation.Text = Loc;
                //GenerateBreackLineForPDFFormat1(LocTitle, Loc);
                // lblLocTitle.Text = GenerateBreackLineForPDFFormat(Loc, "Location:");

                string Unit = Convert.ToUInt16(objChargeSlip.DealTransactionsProperty.UnitTypeValue) == 1 ? "Sq ft" : "A";
                lblSize.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.Size) + " " + Unit;


                // SK - 11-12-2013 - added bldg size 

                lblBldgSize.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.BldgSize) + " sq ft";


                //lblUse.Text = objChargeSlip.DealTransactionsProperty.DealUse == "Select" ? "" : objChargeSlip.DealTransactionsProperty.DealUse;
                lblUse.Text = objChargeSlip.DealTransactionsProperty.DealUseText == "- Select -" ? "" : objChargeSlip.DealTransactionsProperty.DealUseText;
                //No Need Now---09/019/2013--get reords from charge slip 
                //Company objCompany = new Company();
                //objCompany.CompanyID = objChargeSlip.CompanyID;
                //objCompany.Load();

                string strComDetails;
                string OwnershipEntity = string.IsNullOrEmpty(objChargeSlip.CTOwnershipEntity) == false ? Convert.ToString(objChargeSlip.CTOwnershipEntity) + BreakLine : "";
                string CompanyName = string.IsNullOrEmpty(objChargeSlip.Company) == false ? Convert.ToString(objChargeSlip.Company) + BreakLine : "";
                string CmpAddress1 = string.IsNullOrEmpty(objChargeSlip.CTAddress1) == false ? Convert.ToString(objChargeSlip.CTAddress1) + BreakLine : "";
                string CmpAddress2 = string.IsNullOrEmpty(objChargeSlip.CTAddress2) == false ? Convert.ToString(objChargeSlip.CTAddress2) + BreakLine : "";
                string Email = string.IsNullOrEmpty(objChargeSlip.CTEmail) == false ? Convert.ToString(objChargeSlip.CTEmail) + BreakLine : "";
                string Name = string.IsNullOrEmpty(objChargeSlip.CTName) == false ? Convert.ToString(objChargeSlip.CTName) + BreakLine : "";


                strComDetails = OwnershipEntity + CompanyName + Name + Email + CmpAddress1 + CmpAddress2 + Convert.ToString(objChargeSlip.CTCity) + " , " + Convert.ToString(objChargeSlip.CTState) + " , " + Convert.ToString(objChargeSlip.CTZipCode);

                //lblChargeToTitle.Text = GenerateBreackLineForPDFFormat(strComDetails, "Charge To:");
                lblChargeTo.Text = strComDetails;

                lblLeaseStart.Text = Convert.ToDateTime(objChargeSlip.DealTransactionsProperty.LeaseStartDate.ToString()).ToShortDateString();

                HideTermsDetails();
                if (!string.IsNullOrEmpty(lblUse.Text))
                {
                    tr_Use.Visible = true;
                }
                //To Print Notify days 
                CommissionDueDatesNotification objCommissionDueDatesNotification = new CommissionDueDatesNotification();
                objCommissionDueDatesNotification.ChargeSlipId = Convert.ToInt32(objChargeSlip.ChargeSlipID);
                DataSet dsNotifyDays = objCommissionDueDatesNotification.BindNotificationDaysByChargeSlip(objCommissionDueDatesNotification.ChargeSlipId);
                string IsNotifyOn = string.Empty;
                if (dsNotifyDays.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in dsNotifyDays.Tables[0].Rows)
                    {
                        IsNotifyOn = string.Empty;
                        IsNotifyOn = Convert.ToString(row["IsNotifyOn"]);
                        lblNotifyMeTimes.Text = IsNotifyOn.Replace(",", ".....") + " DAYS";
                    }
                }


                if (objChargeSlip.DealTransactionsProperty.DealType == 2)
                {



                    tr_Size.Visible = true;
                    tr_DealSubType.Visible = true;
                    tr_LeseStart.Visible = true;
                    tr_BldgSize.Visible = false;



                    DealParties objTenant = new DealParties();
                    objTenant.PartyID = objChargeSlip.DealTransactionsProperty.Tenant;
                    objTenant.Load();
                    if (!string.IsNullOrEmpty(objTenant.PartyCompanyName))
                    {
                        tr_Title.Visible = true;
                    }
                    td_TenantOrBuyerTitle.InnerText = "Tenant:";
                    lblTenantOrBuyer.Text = objTenant.PartyCompanyName;
                    lblComments.Text = objChargeSlip.DealTransactionsProperty.Notes;
                    lblDealType.Text = "Lease";
                    td_NetsOrSalePriceTitle.InnerText = "Rent PSF:";

                    // string AddNotes = objChargeSlip.DealTransactionsProperty.AdditionalNotes;
                    if (!string.IsNullOrEmpty(objChargeSlip.DealTransactionsProperty.AdditionalNotes.ToString()))
                    {
                        tr_AddComments.Visible = true;
                        lblAddComments.Text = objChargeSlip.DealTransactionsProperty.AdditionalNotes.ToString();
                    }


                    CommissionableTerms objCommissionableTerms = new CommissionableTerms();
                    objCommissionableTerms.ChargeSlipId = objChargeSlip.ChargeSlipID;
                    DataSet dsComm = objCommissionableTerms.GetCommTermsChargeSlipWise();
                    DataTable dtComm = dsComm.Tables[0];
                    DataTable dtCommPeriod = dsComm.Tables[1];
                    DataTable dtCommBaseDetails = dsComm.Tables[2];
                    tr_CoBroker.Visible = false;
                    tr_AggregateRent.Visible = true;
                    tr_Commissionable.Visible = true;
                    tr_Commissionper.Visible = true;
                    lblAggregateRent.Text = Convert.ToDecimal(dtCommBaseDetails.Rows[0]["AggregateRent"].ToString()).ToString("C", CultureInfo.CurrentCulture);
                    lblCommissionable.Text = Convert.ToDecimal(dtCommBaseDetails.Rows[0]["commisionablePeriodRent"].ToString()).ToString("C", CultureInfo.CurrentCulture);
                    lblCommissionper.Text = dtCommBaseDetails.Rows[0]["CommissionPercent"].ToString();
                    //Commented by Jaywanti on 09-08-2016
                    //lblCommissionper.Text = dtCommBaseDetails.Rows[0]["CommissionPer"].ToString();
                    //if (dtCommBaseDetails.Rows[0]["CommissionPer"].ToString() == "Varying")
                    //    lblCommissionper.Text = dtCommBaseDetails.Rows[0]["CommissionPer"].ToString();
                    //else
                    //    lblCommissionper.Text = Convert.ToDecimal(dtCommBaseDetails.Rows[0]["CommissionPer"].ToString()).ToString() + " %";
                    if (objChargeSlip.DealTransactionsProperty.DealSubType == 3)
                    {
                        grdLeaseTerm.Visible = false;
                        td_NetsOrSalePriceTitle.InnerText = "Nets:";
                        lblDealSubType.Text = "Ground";
                        if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.Nets) > 0)
                        {
                            lblNetsOrSalePrice.Text = Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.Nets).ToString("C", CultureInfo.CurrentCulture);
                            //+" NNN (" + Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.CAM).ToString("C", CultureInfo.CurrentCulture) + " CAM, " + Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.INS).ToString("C", CultureInfo.CurrentCulture) + " INS, " + Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.TAX).ToString("C", CultureInfo.CurrentCulture) + " TAX)";
                        }
                        lblLandSize.Text = "Parcel Size:";
                    }
                    else
                    {
                        grdLeaseTerm.Visible = true;
                        lblDealSubType.Text = "Building";
                        lblLandSize.Text = "Size:";
                        DataSet ds = new DataSet();
                        LeaseTerms objLeaseTerms = new LeaseTerms();
                        objLeaseTerms.ChargeSlipId = objChargeSlip.ChargeSlipID;
                        ds = objLeaseTerms.GetListByChargeSlip();
                        if (ds != null)
                        {
                            if (ds.Tables.Count > 0)
                            {
                                DataTable dtLeaseTerm = new DataTable();
                                dtLeaseTerm = ds.Tables[0];
                                DataView dv = new DataView(dtLeaseTerm);
                                dv.RowFilter = "TermTypeId = 1";
                                grdLeaseTerm.DataSource = null;
                                grdLeaseTerm.DataBind();
                                grdLeaseTerm.DataSource = dv;
                                grdLeaseTerm.DataBind();
                            }
                        }
                        if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.Nets) > 0)
                        {
                            lblNetsOrSalePrice.Text = Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.Nets).ToString("C", CultureInfo.CurrentCulture) + " NNN ";
                        }
                        if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.CAM) > 0)
                        {
                            lblcam_ins_tax.Text += Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.CAM).ToString("C", CultureInfo.CurrentCulture) + " CAM";
                        }
                        if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.INS) > 0)
                        {
                            if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.CAM) > 0)
                            {
                                lblcam_ins_tax.Text += ", " + Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.INS).ToString("C", CultureInfo.CurrentCulture) + " INS";
                            }
                            else
                            {
                                lblcam_ins_tax.Text += Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.INS).ToString("C", CultureInfo.CurrentCulture) + " INS";
                            }
                        }
                        if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.TAX) > 0)
                        {
                            if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.CAM) > 0 || Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.INS) > 0)
                            {
                                lblcam_ins_tax.Text += ", " + Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.TAX).ToString("C", CultureInfo.CurrentCulture) + " TAX";
                            }
                            else
                            {
                                lblcam_ins_tax.Text += Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.TAX).ToString("C", CultureInfo.CurrentCulture) + " TAX";
                            }
                        }
                        //if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.Nets) > 0)
                        //{
                        //    lblNetsOrSalePrice.Text = Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.Nets).ToString("C", CultureInfo.CurrentCulture)+" NNN " ; 
                        //}
                        //if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.CAM) > 0 || Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.INS) > 0 || Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.TAX) > 0)
                        //{
                        //    lblNetsOrSalePrice.Text += "( ";
                        //}
                        //if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.CAM)>0)
                        //{
                        //    lblNetsOrSalePrice.Text +=  Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.CAM).ToString("C", CultureInfo.CurrentCulture) + " CAM";
                        //}
                        //if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.INS) > 0)
                        //{
                        //    if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.CAM) > 0)
                        //    {
                        //        lblNetsOrSalePrice.Text += ", " + Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.INS).ToString("C", CultureInfo.CurrentCulture) + " INS";
                        //    }
                        //    else
                        //    {
                        //        lblNetsOrSalePrice.Text +=  Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.INS).ToString("C", CultureInfo.CurrentCulture) + " INS";
                        //    }
                        //}
                        //if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.TAX)>0)
                        //{
                        //    if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.CAM) > 0 || Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.INS) > 0)
                        //    {
                        //        lblNetsOrSalePrice.Text += ", " + Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.TAX).ToString("C", CultureInfo.CurrentCulture) + " TAX";
                        //    }
                        //    else
                        //    {
                        //        lblNetsOrSalePrice.Text += Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.TAX).ToString("C", CultureInfo.CurrentCulture) + " TAX";
                        //    }
                        //}
                        //if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.CAM) > 0 || Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.INS) > 0 || Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.TAX) > 0)
                        //{
                        //    lblNetsOrSalePrice.Text += ") ";
                        //}

                        //lblNetsOrSalePrice.Text = Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.Nets).ToString("C", CultureInfo.CurrentCulture) + " NNN (" + Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.CAM).ToString("C", CultureInfo.CurrentCulture) + " CAM, " + Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.INS).ToString("C", CultureInfo.CurrentCulture) + " INS, " + Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.TAX).ToString("C", CultureInfo.CurrentCulture) + " TAX)";




                        //lblFreeRent.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.FreeRent) + " " + FreeRentPeriod;
                        //lblTI.Text = Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.TI).ToString("C", CultureInfo.CurrentCulture);
                    }
                    string FreeRentPeriod = "";
                    if (objChargeSlip.DealTransactionsProperty.FreeRentUnitTypeValue == 1)
                    {
                        FreeRentPeriod = "DAYS";
                    }
                    else if (objChargeSlip.DealTransactionsProperty.FreeRentUnitTypeValue == 2)
                    {
                        FreeRentPeriod = "MONTHS";
                    }
                    else if (objChargeSlip.DealTransactionsProperty.FreeRentUnitTypeValue == 3)
                    {
                        FreeRentPeriod = "YEARS";
                    }
                    if (objChargeSlip.DealTransactionsProperty.FreeRent > 0)
                    {
                        tr_FreeRent.Visible = true;
                        lblFreeRent.Text = Convert.ToString(objChargeSlip.DealTransactionsProperty.FreeRent) + " " + FreeRentPeriod;
                    }
                    if (Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.TI) > 0)
                    {
                        tr_TI.Visible = true;
                        lblTI.Text = Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.TI).ToString("C", CultureInfo.CurrentCulture);
                    }
                    if (string.IsNullOrEmpty(lblNetsOrSalePrice.Text))
                    {
                        tr_NetsOrSalePrice.Visible = false;
                    }
                    else
                    {
                        tr_NetsOrSalePrice.Visible = true;
                    }
                    if (string.IsNullOrEmpty(lblcam_ins_tax.Text))
                    {
                        tr_cam_ins_tax.Visible = false;
                    }
                    else
                    {
                        tr_cam_ins_tax.Visible = true;
                    }
                    CommissionDueDates objCommissionDueDates = new CommissionDueDates();
                    objCommissionDueDates.ChargeSlipId = objChargeSlip.ChargeSlipID;
                    DataTable dt = objCommissionDueDates.GetListAsPerChargeSlipId();

                    ////var strLease = new StringBuilder();
                    ////var strOpt = new StringBuilder();
                    ////var strContingent = new StringBuilder();
                    string DueDtDetailStrLease = "", DueDtDetailStrOpt = "", DueDtDetailStrContingent = "";
                    string DueDtDetailStrOpt1 = "", DueDtDetailStrOpt2 = "", DueDtDetailStrOpt3 = "";
                    int TermType;
                    string Terms;
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i <= dt.Rows.Count - 1; i++)
                        {
                            TermType = Convert.ToInt16(dt.Rows[i]["TermTypeId"].ToString());
                            if (TermType == GlobleData.cns_TertType_BaseLease)
                            {
                                if (DueDtDetailStrLease.Trim() != "")
                                {
                                    DueDtDetailStrLease = DueDtDetailStrLease + BreakLine + Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));
                                }
                                else
                                {
                                    DueDtDetailStrLease = Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));

                                }
                                tr_BaseTerm.Visible = true;
                                tr_LeaseDueDate.Visible = true;
                                tr_LeaseDueTot.Visible = true;

                                DataRow[] dRow = dtComm.Select("[TermTypeId]=" + GlobleData.cns_TertType_BaseLease);

                                //Terms = "Lease: " + dRow[0]["TotalMonthsBase"].ToString() + " MONTHS";
                                // Terms = Terms + BreakLine + "Commissionable: " + dRow[0]["TotalMonths"].ToString() + " MONTHS";

                                Terms = "Lease: " + dtCommPeriod.Rows[0]["BaseLeaseTerm"].ToString() + " MONTHS";
                                Terms = Terms + BreakLine + "Commissionable: " + dtCommPeriod.Rows[0]["BaseCommissionableTerm"].ToString() + " MONTHS";

                                //Option Term
                                string OptTerms;
                                // Display Option Terms --if and only if Option term payable is not set

                                int OptionTerms = Convert.ToInt32(dtCommPeriod.Rows[0]["OptionLeaseTerm"].ToString());
                                int OptionPayableTerms = Convert.ToInt32(dtCommPeriod.Rows[0]["OptionCommissionableTerm"].ToString());

                                DataRow[] OptdRow = dtComm.Select("[TermTypeId]=" + GlobleData.cns_TertType_OptionTerm);
                                if (OptionPayableTerms == 0 && OptionTerms > 0 && OptdRow.Length == 0)
                                {
                                    tr_OptTerms.Visible = true;
                                    OptTerms = "Lease: " + dtCommPeriod.Rows[0]["OptionLeaseTerm"].ToString() + " MONTHS";
                                    OptTerms = OptTerms + BreakLine + "Commissionable: " + dtCommPeriod.Rows[0]["OptionCommissionableTerm"].ToString() + " MONTHS";
                                    lblOptTerm.Text = OptTerms;
                                }

                                //Option Term 1
                                string OptTerms1;
                                // Display Option Terms --if and only if Option term payable is not set

                                int OptionTerms1 = Convert.ToInt32(dtCommPeriod.Rows[0]["OptionLeaseTerm1"].ToString());
                                int OptionPayableTerms1 = Convert.ToInt32(dtCommPeriod.Rows[0]["OptionCommissionableTerm1"].ToString());

                                DataRow[] OptdRow1 = dtComm.Select("[TermTypeId]=" + GlobleData.cns_TertType_OptionTerm1);
                                if (OptionPayableTerms1 == 0 && OptionTerms1 > 0 && OptdRow1.Length == 0)
                                {
                                    tr_OptTerms1.Visible = true;
                                    OptTerms1 = "Lease: " + dtCommPeriod.Rows[0]["OptionLeaseTerm1"].ToString() + " MONTHS";
                                    OptTerms1 = OptTerms1 + BreakLine + "Commissionable: " + dtCommPeriod.Rows[0]["OptionCommissionableTerm1"].ToString() + " MONTHS";
                                    lblOptTerm1.Text = OptTerms1;
                                }

                                //Option Term 2
                                string OptTerms2;
                                // Display Option Terms --if and only if Option term payable is not set

                                int OptionTerms2 = Convert.ToInt32(dtCommPeriod.Rows[0]["OptionLeaseTerm2"].ToString());
                                int OptionPayableTerms2 = Convert.ToInt32(dtCommPeriod.Rows[0]["OptionCommissionableTerm2"].ToString());

                                DataRow[] OptdRow2 = dtComm.Select("[TermTypeId]=" + GlobleData.cns_TertType_OptionTerm2);
                                if (OptionPayableTerms2 == 0 && OptionTerms2 > 0 && OptdRow2.Length == 0)
                                {
                                    tr_OptTerms2.Visible = true;
                                    OptTerms2 = "Lease: " + dtCommPeriod.Rows[0]["OptionLeaseTerm2"].ToString() + " MONTHS";
                                    OptTerms2 = OptTerms2 + BreakLine + "Commissionable: " + dtCommPeriod.Rows[0]["OptionCommissionableTerm2"].ToString() + " MONTHS";
                                    lblOptTerm2.Text = OptTerms2;
                                }

                                //Option Term 3
                                string OptTerms3;
                                // Display Option Terms --if and only if Option term payable is not set

                                int OptionTerms3 = Convert.ToInt32(dtCommPeriod.Rows[0]["OptionLeaseTerm3"].ToString());
                                int OptionPayableTerms3 = Convert.ToInt32(dtCommPeriod.Rows[0]["OptionCommissionableTerm3"].ToString());

                                DataRow[] OptdRow3 = dtComm.Select("[TermTypeId]=" + GlobleData.cns_TertType_OptionTerm3);
                                if (OptionPayableTerms3 == 0 && OptionTerms3 > 0 && OptdRow3.Length == 0)
                                {
                                    tr_OptTerms3.Visible = true;
                                    OptTerms3 = "Lease: " + dtCommPeriod.Rows[0]["OptionLeaseTerm3"].ToString() + " MONTHS";
                                    OptTerms3 = OptTerms3 + BreakLine + "Commissionable: " + dtCommPeriod.Rows[0]["OptionCommissionableTerm3"].ToString() + " MONTHS";
                                    lblOptTerm3.Text = OptTerms3;
                                }

                                lblBaseTerm.Text = Terms;
                                lblTotalLeaseDue.Text = String.Format("{0:0.00}", Convert.ToDouble(dRow[0]["Fee"].ToString()).ToString("C", CultureInfo.CurrentCulture));


                                //GenerateSummary(objChargeSlip.ChargeSlipID, TermType);
                                //strLease.Append(DueDtDetailStrLease);
                            }
                            else if (TermType == GlobleData.cns_TertType_OptionTerm)
                            {
                                //DueDtDetailStrOpt = DueDtDetailStrOpt + BreakLine + Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));
                                if (DueDtDetailStrOpt.Trim() != "")
                                {
                                    DueDtDetailStrOpt = DueDtDetailStrOpt + BreakLine + Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));
                                }
                                else
                                {
                                    DueDtDetailStrOpt = Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));

                                }

                                tr_OptTerms.Visible = true;
                                tr_OptDueDueTot.Visible = true;
                                tr_OptDueDate.Visible = true;

                                DataRow[] dRow = dtComm.Select("[TermTypeId]=" + GlobleData.cns_TertType_OptionTerm);
                                //Terms = "Lease: " + dRow[0]["TotalMonthsBase"].ToString() + " MONTHS";
                                //Terms = Terms + BreakLine + "Commissionable: " + dRow[0]["TotalMonths"].ToString() + " MONTHS";

                                Terms = "Lease: " + dtCommPeriod.Rows[0]["OptionLeaseTerm"].ToString() + " MONTHS";
                                Terms = Terms + BreakLine + "Commissionable: " + dtCommPeriod.Rows[0]["OptionCommissionableTerm"].ToString() + " MONTHS";

                                lblOptTerm.Text = Terms;
                                lblTotalOpteDue.Text = String.Format("{0:0.00}", Convert.ToDouble(dRow[0]["Fee"].ToString()).ToString("C", CultureInfo.CurrentCulture));
                                //strOpt.Append(DueDtDetailStrOpt);

                                //set Due Date Summary
                                int No = 0;
                                rnd = new Random();
                                //No = rnd.Next(100, 130);
                                No = rnd.Next(100, 250);
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript_" + No, "javascript:DueDateSummary('" + dt.Rows[i]["DueDate"].ToString() + "','" + dt.Rows[i]["CommPer"].ToString() + "','" + dt.Rows[i]["CommAmount"].ToString() + "','" + TermType + "');", true);
                            }
                            else if (TermType == GlobleData.cns_TertType_ContingentTerm)
                            {
                                // DueDtDetailStrContingent = DueDtDetailStrContingent + BreakLine + Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));
                                if (DueDtDetailStrContingent.Trim() != "")
                                {
                                    DueDtDetailStrContingent = DueDtDetailStrContingent + BreakLine + Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));
                                }
                                else
                                {
                                    DueDtDetailStrContingent = Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));

                                }

                                tr_ContiDueDueDates.Visible = true;
                                tr_ContiDueDueTot.Visible = true;
                                tr_ContiTerms.Visible = true;

                                DataRow[] dRow = dtComm.Select("[TermTypeId]=" + GlobleData.cns_TertType_ContingentTerm);

                                //Terms = "Lease: " + dRow[0]["TotalMonthsBase"].ToString() + " MONTHS";
                                //Terms = Terms + BreakLine + "Commissionable: " + dRow[0]["TotalMonths"].ToString() + " MONTHS";

                                Terms = "Lease: " + dtCommPeriod.Rows[0]["ContingentLeaseTerm"].ToString() + " MONTHS";
                                Terms = Terms + BreakLine + "Commissionable: " + dtCommPeriod.Rows[0]["ContingentCommissionableTerm"].ToString() + " MONTHS";

                                lblConTerm.Text = Terms;
                                lblTotalConeDue.Text = String.Format("{0:0.00}", Convert.ToDouble(dRow[0]["Fee"].ToString()).ToString("C", CultureInfo.CurrentCulture));
                                //strContingent.Append(DueDtDetailStrContingent);

                                //set Due Date Summary
                                int No = 0;
                                rnd = new Random();
                                //No = rnd.Next(130, 160);
                                No = rnd.Next(251, 400);
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript_" + No, "javascript:DueDateSummary('" + dt.Rows[i]["DueDate"].ToString() + "','" + dt.Rows[i]["CommPer"].ToString() + "','" + dt.Rows[i]["CommAmount"].ToString() + "','" + TermType + "');", true);
                            }
                            else if (TermType == GlobleData.cns_TertType_OptionTerm1)
                            {
                                if (DueDtDetailStrOpt1.Trim() != "")
                                {
                                    DueDtDetailStrOpt1 = DueDtDetailStrOpt1 + BreakLine + Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));
                                }
                                else
                                {
                                    DueDtDetailStrOpt1 = Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));
                                }

                                tr_OptTerms1.Visible = true;
                                tr_OptDueDueTot1.Visible = true;
                                tr_OptDueDate1.Visible = true;

                                DataRow[] dRow = dtComm.Select("[TermTypeId]=" + GlobleData.cns_TertType_OptionTerm1);

                                Terms = "Lease: " + dtCommPeriod.Rows[0]["OptionLeaseTerm1"].ToString() + " MONTHS";
                                Terms = Terms + BreakLine + "Commissionable: " + dtCommPeriod.Rows[0]["OptionCommissionableTerm1"].ToString() + " MONTHS";

                                lblOptTerm1.Text = Terms;
                                lblTotalOpteDue1.Text = String.Format("{0:0.00}", Convert.ToDouble(dRow[0]["Fee"].ToString()).ToString("C", CultureInfo.CurrentCulture));

                                //set Due Date Summary
                                int No = 0;
                                rnd = new Random();
                                //No = rnd.Next(160, 190);
                                No = rnd.Next(401, 550);
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript_" + No, "javascript:DueDateSummary('" + dt.Rows[i]["DueDate"].ToString() + "','" + dt.Rows[i]["CommPer"].ToString() + "','" + dt.Rows[i]["CommAmount"].ToString() + "','" + TermType + "');", true);
                            }
                            else if (TermType == GlobleData.cns_TertType_OptionTerm2)
                            {
                                if (DueDtDetailStrOpt2.Trim() != "")
                                {
                                    DueDtDetailStrOpt2 = DueDtDetailStrOpt2 + BreakLine + Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));
                                }
                                else
                                {
                                    DueDtDetailStrOpt2 = Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));
                                }

                                tr_OptTerms2.Visible = true;
                                tr_OptDueDueTot2.Visible = true;
                                tr_OptDueDate2.Visible = true;

                                DataRow[] dRow = dtComm.Select("[TermTypeId]=" + GlobleData.cns_TertType_OptionTerm2);

                                Terms = "Lease: " + dtCommPeriod.Rows[0]["OptionLeaseTerm2"].ToString() + " MONTHS";
                                Terms = Terms + BreakLine + "Commissionable: " + dtCommPeriod.Rows[0]["OptionCommissionableTerm2"].ToString() + " MONTHS";

                                lblOptTerm2.Text = Terms;
                                lblTotalOpteDue2.Text = String.Format("{0:0.00}", Convert.ToDouble(dRow[0]["Fee"].ToString()).ToString("C", CultureInfo.CurrentCulture));

                                //set Due Date Summary
                                int No = 0;
                                rnd = new Random();
                                //No = rnd.Next(190, 220);
                                No = rnd.Next(551, 700);
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript_" + No, "javascript:DueDateSummary('" + dt.Rows[i]["DueDate"].ToString() + "','" + dt.Rows[i]["CommPer"].ToString() + "','" + dt.Rows[i]["CommAmount"].ToString() + "','" + TermType + "');", true);
                            }
                            else if (TermType == GlobleData.cns_TertType_OptionTerm3)
                            {
                                if (DueDtDetailStrOpt3.Trim() != "")
                                {
                                    DueDtDetailStrOpt3 = DueDtDetailStrOpt3 + BreakLine + Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));
                                }
                                else
                                {
                                    DueDtDetailStrOpt3 = Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));
                                }

                                tr_OptTerms3.Visible = true;
                                tr_OptDueDueTot3.Visible = true;
                                tr_OptDueDate3.Visible = true;

                                DataRow[] dRow = dtComm.Select("[TermTypeId]=" + GlobleData.cns_TertType_OptionTerm3);

                                Terms = "Lease: " + dtCommPeriod.Rows[0]["OptionLeaseTerm3"].ToString() + " MONTHS";
                                Terms = Terms + BreakLine + "Commissionable: " + dtCommPeriod.Rows[0]["OptionCommissionableTerm3"].ToString() + " MONTHS";

                                lblOptTerm3.Text = Terms;
                                lblTotalOpteDue3.Text = String.Format("{0:0.00}", Convert.ToDouble(dRow[0]["Fee"].ToString()).ToString("C", CultureInfo.CurrentCulture));

                                //set Due Date Summary
                                int No = 0;
                                rnd = new Random();
                                //No = rnd.Next(220, 250);
                                No = rnd.Next(701, 850);
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript_" + No, "javascript:DueDateSummary('" + dt.Rows[i]["DueDate"].ToString() + "','" + dt.Rows[i]["CommPer"].ToString() + "','" + dt.Rows[i]["CommAmount"].ToString() + "','" + TermType + "');", true);
                            }

                        }
                        GenerateSummary(objChargeSlip.ChargeSlipID, 1);
                        GenerateSummary(objChargeSlip.ChargeSlipID, 2);
                        GenerateSummary(objChargeSlip.ChargeSlipID, 3);

                        GenerateSummary(objChargeSlip.ChargeSlipID, 4);
                        GenerateSummary(objChargeSlip.ChargeSlipID, 5);
                        GenerateSummary(objChargeSlip.ChargeSlipID, 6);
                    }
                    lblLeaseDueDates.Text = DueDtDetailStrLease;
                    //lblLeaseDueDtTitle.Text = GenerateBreackLineForPDFFormat(DueDtDetailStrLease, "Due Date(s):");
                    lblOpteDueDate.Text = DueDtDetailStrOpt;
                    // lblOpteDueDtTitle.Text = GenerateBreackLineForPDFFormat(DueDtDetailStrOpt, "Due Date(s):");
                    lblContigentDueDate.Text = DueDtDetailStrContingent;
                    // lblContigentDueDtCon.Text = GenerateBreackLineForPDFFormat(DueDtDetailStrContingent, "Due Date(s):");

                    lblOpteDueDate1.Text = DueDtDetailStrOpt1;
                    lblOpteDueDate2.Text = DueDtDetailStrOpt2;
                    lblOpteDueDate3.Text = DueDtDetailStrOpt3;

                    if (objChargeSlip.DealTransactionsProperty.DealSubType == 4)
                    {
                        lblNetsOrSalePrice.Text = "";
                    }
                }
                else if (objChargeSlip.DealTransactionsProperty.DealType == 1)
                {


                    tr_Size.Visible = true;
                    tr_DealSubType.Visible = true;
                    tr_AggregateRent.Visible = false;
                    tr_Commissionable.Visible = false;
                    tr_Commissionper.Visible = false;

                    DealParties objBuyer = new DealParties();
                    objBuyer.PartyID = objChargeSlip.DealTransactionsProperty.Buyer;
                    objBuyer.Load();
                    if (!string.IsNullOrEmpty(objBuyer.PartyCompanyName))
                    {
                        tr_Title.Visible = true;
                    }
                    td_TenantOrBuyerTitle.InnerText = "Buyer:";
                    lblTenantOrBuyer.Text = objBuyer.PartyCompanyName;
                    lblComments.Text = objChargeSlip.DealTransactionsProperty.Notes;
                    lblDealType.Text = "Sale";
                    td_NetsOrSalePriceTitle.InnerText = "Sale Price:";
                    lblNetsOrSalePrice.Text = Convert.ToDecimal(objChargeSlip.DealTransactionsProperty.SalePrice).ToString("C", CultureInfo.CurrentCulture);

                    if (objChargeSlip.DealTransactionsProperty.DealSubType == 1)
                    {
                        lblDealSubType.Text = "Improved";
                        tr_BldgSize.Visible = true;
                    }
                    else
                    {
                        lblLandSize.Text = "Size:";
                        lblDealSubType.Text = "Unimproved";
                        tr_BldgSize.Visible = false;
                    }
                    if (string.IsNullOrEmpty(lblNetsOrSalePrice.Text))
                    {
                        tr_NetsOrSalePrice.Visible = false;
                    }
                    else
                    {
                        tr_NetsOrSalePrice.Visible = true;
                    }
                    SetDueDateDetailsAndSummary(objChargeSlip);


                }
                else if (objChargeSlip.DealTransactionsProperty.DealType == 3)
                {
                    tr_AggregateRent.Visible = false;
                    tr_Commissionable.Visible = false;
                    tr_Commissionper.Visible = false;
                    lblComments.Text = objChargeSlip.DealTransactionsProperty.Notes;
                    td_NetsOrSalePriceTitle.InnerText = "Fee:";
                    lblNetsOrSalePrice.Text = "$ " + Convert.ToString(objChargeSlip.DealTransactionsProperty.SetFee);
                    lblDealType.Text = "Consulting";
                    if (string.IsNullOrEmpty(lblNetsOrSalePrice.Text))
                    {
                        tr_NetsOrSalePrice.Visible = false;
                    }
                    else
                    {
                        tr_NetsOrSalePrice.Visible = true;
                    }
                    SetDueDateDetailsAndSummary(objChargeSlip);
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "SetChargeSlipReview", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private void SetDueDateDetailsAndSummary(ChargeSlip objChargeSlip)// Used For Sale and Consulting Transactions
    {
        try
        {
            //For Due Dates
            CommissionDueDates objCommissionDueDates = new CommissionDueDates();
            objCommissionDueDates.ChargeSlipId = objChargeSlip.ChargeSlipID;
            DataTable dt = objCommissionDueDates.GetListAsPerChargeSlipId();
            string DueDtDetailStrLease = "";
            decimal TotDue = 0;
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    if (DueDtDetailStrLease.Trim() != "")
                    {
                        DueDtDetailStrLease = DueDtDetailStrLease + BreakLine + Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));
                        TotDue = TotDue + Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString());
                    }
                    else
                    {
                        DueDtDetailStrLease = Convert.ToString(dt.Rows[i]["DueDate"].ToString() + "............" + Convert.ToString(dt.Rows[i]["CommPer"].ToString()) + "% ............ " + String.Format("{0:f2}", Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString()).ToString("C", CultureInfo.CurrentCulture)));

                        TotDue = TotDue + Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString());
                    }
                    //tr_BaseTerm.Visible = true;
                    tr_LeaseDueDate.Visible = true;
                    tr_LeaseDueTot.Visible = true;

                    lblLeaseDueDates.Text = DueDtDetailStrLease;
                    // lblLeaseDueDtTitle.Text = GenerateBreackLineForPDFFormat(DueDtDetailStrLease, "Due Date(s):");
                    lblTotalLeaseDue.Text = TotDue.ToString("C", CultureInfo.CurrentCulture);
                    // }
                }
            }
            GenerateSummary(objChargeSlip.ChargeSlipID, 1);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "SetDueDateDetailsAndSummary", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private void HideTermsDetails()
    {
        tr_BaseTerm.Visible = false;
        tr_LeaseDueDate.Visible = false;
        tr_LeaseDueTot.Visible = false;

        tr_OptTerms.Visible = false;
        tr_OptDueDueTot.Visible = false;
        tr_OptDueDate.Visible = false;

        tr_OptTerms1.Visible = false;
        tr_OptDueDueTot1.Visible = false;
        tr_OptDueDate1.Visible = false;

        tr_OptTerms2.Visible = false;
        tr_OptDueDueTot2.Visible = false;
        tr_OptDueDate2.Visible = false;

        tr_OptTerms3.Visible = false;
        tr_OptDueDueTot3.Visible = false;
        tr_OptDueDate3.Visible = false;

        tr_ContiDueDueDates.Visible = false;
        tr_ContiDueDueTot.Visible = false;
        tr_ContiTerms.Visible = false;

        tr_FreeRent.Visible = false;
        tr_TI.Visible = false;

        tr_CoBroker.Visible = false;

        tr_LeaseDueDate.Visible = false;
        tr_LeaseDueTot.Visible = false;
        tr_Title.Visible = false;
        tr_Use.Visible = false;
        tr_Size.Visible = false;
        tr_BldgSize.Visible = false;
        tr_DealSubType.Visible = false;

        tr_LeseStart.Visible = false;
        tr_AddComments.Visible = false;
        tr_cam_ins_tax.Visible = false;
        tr_NetsOrSalePrice.Visible = false;
    }
    #endregion

    protected override void OnInit(EventArgs e)
    {
        try
        {
            base.OnInit(e);

            int listItemIds = 1;
            ChargeSlip objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
            if (objChargeSlip != null)
            {
                Attachments objAttachments = new Attachments();
                objAttachments.ChargeSlipID = objChargeSlip.ChargeSlipID;
                DataTable dtAtt = objAttachments.GetDetailsByChargeSlip();
                ViewState["CheckAttachmentStatus"] = false;
                if (dtAtt.Rows.Count > 0)
                {
                    // tr_Attachments.Visible = true ;
                    for (int i = 0; i < dtAtt.Rows.Count; i++)
                    {
                        HtmlGenericControl li = new HtmlGenericControl("li");
                        LinkButton lnk = new LinkButton();
                        // string fileToSelect = System.Configuration.ConfigurationManager.AppSettings["FolderPath"] + dtAtt.Rows[0]["AttachmentDescription"].ToString();
                        string fileToSelect = "../Uploads1/" + dtAtt.Rows[i]["AttachmentDescription"].ToString();
                        lnk.ID = "lnk" + listItemIds;
                        string attachment = dtAtt.Rows[i]["AttachmentDescription"].ToString();
                        int count = attachment.IndexOf('_');
                        lnk.Text = attachment.Substring(count + 1).ToString();
                        // dtGrid.Rows[0]["AttachmentDescription"] = attachment.Substring(count + 1).ToString();
                        //lnk.Text = dtAtt.Rows[i]["AttachmentDescription"].ToString();
                        // lnk.Click += Clicked;
                        lnk.Attributes.Add("onclick", "javascript:return OpenWindow('" + fileToSelect + "')");
                        //lnk.Command += new CommandEventHandler(lnkColourAlternative_Click);
                        //lnk.Click 
                        li.Controls.Add(lnk);
                        dvAttachment.Controls.Add(li);
                        listItemIds++;
                        ViewState["CheckAttachmentStatus"] = true;

                    }
                }
            }
        }
        catch (Exception ex) { ErrorLog.WriteLog("NewChargeSlipReview", "OnInit", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString()); }
        //else
        //{
        //    tr_Attachments.Visible = false;
        //}
    }

    private void Clicked(object sender, EventArgs e)
    {
        try
        {
            var btn = sender as LinkButton;
            btn.Text = "Link Button Clicked";
            //   window.open('doubt_mvc.txt','mywindow', 'width=500,height=500,resizable=yes');

            string fileToSelect = @"C:\Users\Diptee Adhatrao\Desktop\ttt.txt";
            string args = string.Format("/Select, {0}", fileToSelect);

            ProcessStartInfo pfi = new ProcessStartInfo("Explorer.exe", args);
            System.Diagnostics.Process.Start(pfi);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "Clicked", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session[GlobleData.NewChargeSlipId] != null)
            {
                ChargeSlip objChargeSlip = (ChargeSlip)(Session[GlobleData.NewChargeSlipObj]);
                ChargeSlip NewobjChargeSlipId = new ChargeSlip();
                NewobjChargeSlipId.ChargeSlipID = objChargeSlip.ChargeSlipID;
                NewobjChargeSlipId.Load();
                if (hdnSaveStatus.Value == "N")
                {
                    NewobjChargeSlipId.Status = 1;
                }

                ////mpPrivacySettings.Show();
                Boolean reslt = NewobjChargeSlipId.Save();
                if (reslt == true && hdnSaveStatus.Value == "N")
                {
                    NewobjChargeSlipId.TotAnnualRent = Convert.ToDecimal(Session[GlobleData.TotalRent]);
                    NewobjChargeSlipId.PushDataToGeoApp();
                    ViewState["Result"] = reslt;
                }

                if (reslt == true)
                {
                    // Page.ClientScript.RegisterStartupScript(Page.GetType(), "Save", "SaveAlert('" + true + "');", true);
                    Utility.ClearSessionExceptUser();
                    string test = hdnPropertyAccessUsers.Value;
                    if (hdnSaveStatus.Value == "D")
                    {
                        Response.Redirect("AddOrEditNewChargeSlip.aspx", false);
                    }
                    //if (hdnSaveStatus.Value == "N")
                    //{
                    //    Page.ClientScript.RegisterStartupScript(Page.GetType(), "Save", "SaveAlert('" + true + "');", true);
                    //    Utility.ClearSessionExceptUser();
                    //   ////        Response.Redirect("AddOrEditNewChargeSlip.aspx");

                    //}
                    //else
                    //{
                    //    Page.ClientScript.RegisterStartupScript(Page.GetType(), "Save", "SaveAlert('" + reslt + "');", true);
                    //    Utility.ClearSessionExceptUser();
                    //}

                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "btnNext_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void btnSaveUsers_Click(object sender, EventArgs e)
    {
        try
        {
            if (ViewState["Result"] != null)
            {
                Boolean reslt = Convert.ToBoolean(ViewState["Result"].ToString());
                if (reslt == true)
                {
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "Save", "SaveAlert('" + true + "');", true);
                    Utility.ClearSessionExceptUser();
                    string test = hdnPropertyAccessUsers.Value;
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "btnSaveUsers_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session[GlobleData.NewChargeSlipObj] != null)
            {
                ChargeSlip objChargeSlip = new ChargeSlip();
                objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
                Response.Redirect("NewChargeCommissionSpitCalCulator.aspx", false);
                //if (objChargeSlip.DealTransactionsProperty.DealType == 3)
                //{
                //    Response.Redirect("NewCharge.aspx");
                //}
                //else
                //{
                //    Response.Redirect("NewChargeCommissionSpitCalCulator.aspx");
                //}
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "btnBack_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private void GenerateSummary(int ChargeSlipID, int TermTypeId)
    {
        try
        {
            Boolean IsAdmin = false;
            if (Convert.ToInt16(Session["UserType"].ToString()) == 1)
            {
                IsAdmin = true;
            }
            UserDetails objUserDetails = new UserDetails();
            // For Commission Split Summary
            DealBrokerCommissions objDealBrokerCommissions = new DealBrokerCommissions();
            objDealBrokerCommissions.ChargeSlipId = ChargeSlipID;
            objDealBrokerCommissions.TermTypeId = TermTypeId;
            DataSet ds = objDealBrokerCommissions.GetListByChargeSlip();
            DataTable dtSummary;
            string DisplayBroker;
            double CommPerTot = 0, CommAmtTot = 0;
            if (ds.Tables.Count > 0)
            {
                dtSummary = ds.Tables[0];
                if (dtSummary.Rows.Count > 0)
                {
                    int i = 0;
                    int No = 0;
                    rnd = new Random();
                    if (TermTypeId == 1)
                    {
                        No = rnd.Next(1, 30);
                        foreach (DataRow dRow in dtSummary.Rows)
                        {
                            //if (IsAdmin == true)
                            //{
                            objUserDetails.UserID = Convert.ToInt16(dRow["BrokerId"].ToString());
                            objUserDetails.LoadByUserIdAndCurrentDate(objDealBrokerCommissions.ChargeSlipId);
                            //}
                            //else
                            //{
                            //    objUserDetails.UserID = Convert.ToInt16(Session["UserType"].ToString();
                            //    objUserDetails.LoadByUserIdAndCurrentDate(objDealBrokerCommissions.ChargeSlipId);
                            //}
                            DisplayBroker = (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 4 || Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5) ? dRow["Broker"].ToString() : dRow["CompanyName"].ToString();
                            if (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5)
                            {
                                DisplayBroker = DisplayBroker + " (CLB-Broker)";
                            }

                            //  DisplayBroker.Replace("'", "#");

                            No = No + i;
                            if (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 4 || Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5)
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummary('" + DisplayBroker.Replace("'", "#") + "','" + dRow["BrokerSubPercent"].ToString() + "','" + dRow["BrokerCommission"].ToString() + "','" + TermTypeId + "','" + objUserDetails.PercentBroker + "','" + objUserDetails.PercentCompany + "','" + objUserDetails.CompanyName + "');", true);
                            else
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummary('" + DisplayBroker.Replace("'", "#") + "','" + dRow["BrokerSubPercent"].ToString() + "','" + dRow["BrokerCommission"].ToString() + "','" + TermTypeId + "','-1','" + objUserDetails.PercentCompany + "','" + objUserDetails.CompanyName + "');", true);
                            CommPerTot = CommPerTot + Convert.ToDouble(dRow["BrokerSubPercent"].ToString());
                            CommAmtTot = CommAmtTot + Convert.ToDouble(dRow["BrokerCommission"].ToString());
                            i = i + 1;
                        }
                        No = rnd.Next(400, 430);
                        Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummaryTotal('" + CommPerTot + "','" + CommAmtTot + "','" + TermTypeId + "','false');", true);

                    }
                    if (TermTypeId == 2)
                    {
                        No = rnd.Next(31, 60);
                        foreach (DataRow dRow in dtSummary.Rows)
                        {
                            DisplayBroker = (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 4 || Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5) ? dRow["Broker"].ToString() : dRow["CompanyName"].ToString();
                            if (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5)
                            {
                                DisplayBroker = DisplayBroker + " (CLB-Broker)";
                            }
                            No = No + i;
                            if (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 4 || Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5)
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummary('" + DisplayBroker.Replace("'", "#") + "','" + dRow["BrokerSubPercent"].ToString() + "','" + dRow["BrokerCommission"].ToString() + "','" + TermTypeId + "','" + objUserDetails.PercentBroker + "','" + objUserDetails.PercentCompany + "','" + objUserDetails.CompanyName + "');", true);
                            else
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummary('" + DisplayBroker.Replace("'", "#") + "','" + dRow["BrokerSubPercent"].ToString() + "','" + dRow["BrokerCommission"].ToString() + "','" + TermTypeId + "','-1','" + objUserDetails.PercentCompany + "','" + objUserDetails.CompanyName + "');", true);
                            CommPerTot = CommPerTot + Convert.ToDouble(dRow["BrokerSubPercent"].ToString());
                            CommAmtTot = CommAmtTot + Convert.ToDouble(dRow["BrokerCommission"].ToString());
                            i = i + 1;
                        }
                        No = rnd.Next(430, 460);
                        Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummaryTotal('" + CommPerTot + "','" + CommAmtTot + "','" + TermTypeId + "','false');", true);
                    }
                    if (TermTypeId == 3)
                    {
                        No = rnd.Next(61, 90);
                        foreach (DataRow dRow in dtSummary.Rows)
                        {
                            DisplayBroker = (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 4 || Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5) ? dRow["Broker"].ToString() : dRow["CompanyName"].ToString();
                            if (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5)
                            {
                                DisplayBroker = DisplayBroker + " (CLB-Broker)";
                            }
                            No = No + i;
                            if (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 4 || Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5)
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummary('" + DisplayBroker.Replace("'", "#") + "','" + dRow["BrokerSubPercent"].ToString() + "','" + dRow["BrokerCommission"].ToString() + "','" + TermTypeId + "','" + objUserDetails.PercentBroker + "','" + objUserDetails.PercentCompany + "','" + objUserDetails.CompanyName + "');", true);
                            else
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummary('" + DisplayBroker.Replace("'", "#") + "','" + dRow["BrokerSubPercent"].ToString() + "','" + dRow["BrokerCommission"].ToString() + "','" + TermTypeId + "','-1','" + objUserDetails.PercentCompany + "','" + objUserDetails.CompanyName + "');", true);
                            CommPerTot = CommPerTot + Convert.ToDouble(dRow["BrokerSubPercent"].ToString());
                            CommAmtTot = CommAmtTot + Convert.ToDouble(dRow["BrokerCommission"].ToString());
                            i = i + 1;
                        }
                        No = rnd.Next(460, 490);
                        Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummaryTotal('" + CommPerTot + "','" + CommAmtTot + "','" + TermTypeId + "','false');", true);
                    }

                    if (TermTypeId == 4)
                    {
                        No = rnd.Next(91, 120);
                        foreach (DataRow dRow in dtSummary.Rows)
                        {
                            DisplayBroker = (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 4 || Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5) ? dRow["Broker"].ToString() : dRow["CompanyName"].ToString();
                            if (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5)
                            {
                                DisplayBroker = DisplayBroker + " (CLB-Broker)";
                            }
                            No = No + i;
                            if (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 4 || Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5)
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummary('" + DisplayBroker.Replace("'", "#") + "','" + dRow["BrokerSubPercent"].ToString() + "','" + dRow["BrokerCommission"].ToString() + "','" + TermTypeId + "','" + objUserDetails.PercentBroker + "','" + objUserDetails.PercentCompany + "','" + objUserDetails.CompanyName + "');", true);
                            else
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummary('" + DisplayBroker.Replace("'", "#") + "','" + dRow["BrokerSubPercent"].ToString() + "','" + dRow["BrokerCommission"].ToString() + "','" + TermTypeId + "','-1','" + objUserDetails.PercentCompany + "','" + objUserDetails.CompanyName + "');", true);
                            CommPerTot = CommPerTot + Convert.ToDouble(dRow["BrokerSubPercent"].ToString());
                            CommAmtTot = CommAmtTot + Convert.ToDouble(dRow["BrokerCommission"].ToString());
                            i = i + 1;
                        }
                        No = rnd.Next(490, 520);
                        Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummaryTotal('" + CommPerTot + "','" + CommAmtTot + "','" + TermTypeId + "','false');", true);
                    }
                    if (TermTypeId == 5)
                    {
                        No = rnd.Next(121, 150);
                        foreach (DataRow dRow in dtSummary.Rows)
                        {
                            DisplayBroker = (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 4 || Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5) ? dRow["Broker"].ToString() : dRow["CompanyName"].ToString();
                            if (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5)
                            {
                                DisplayBroker = DisplayBroker + " (CLB-Broker)";
                            }
                            No = No + i;
                            if (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 4 || Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5)
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummary('" + DisplayBroker.Replace("'", "#") + "','" + dRow["BrokerSubPercent"].ToString() + "','" + dRow["BrokerCommission"].ToString() + "','" + TermTypeId + "','" + objUserDetails.PercentBroker + "','" + objUserDetails.PercentCompany + "','" + objUserDetails.CompanyName + "');", true);
                            else
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummary('" + DisplayBroker.Replace("'", "#") + "','" + dRow["BrokerSubPercent"].ToString() + "','" + dRow["BrokerCommission"].ToString() + "','" + TermTypeId + "','-1','" + objUserDetails.PercentCompany + "','" + objUserDetails.CompanyName + "');", true);
                            CommPerTot = CommPerTot + Convert.ToDouble(dRow["BrokerSubPercent"].ToString());
                            CommAmtTot = CommAmtTot + Convert.ToDouble(dRow["BrokerCommission"].ToString());
                            i = i + 1;
                        }
                        No = rnd.Next(520, 550);
                        Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummaryTotal('" + CommPerTot + "','" + CommAmtTot + "','" + TermTypeId + "','false');", true);
                    }
                    if (TermTypeId == 6)
                    {
                        No = rnd.Next(121, 150);
                        foreach (DataRow dRow in dtSummary.Rows)
                        {
                            DisplayBroker = (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 4 || Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5) ? dRow["Broker"].ToString() : dRow["CompanyName"].ToString();
                            if (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5)
                            {
                                DisplayBroker = DisplayBroker + " (CLB-Broker)";
                            }
                            No = No + i;
                            if (Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 4 || Convert.ToInt16(dRow["BrokerTypeId"].ToString()) == 5)
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummary('" + DisplayBroker.Replace("'", "#") + "','" + dRow["BrokerSubPercent"].ToString() + "','" + dRow["BrokerCommission"].ToString() + "','" + TermTypeId + "','" + objUserDetails.PercentBroker + "','" + objUserDetails.PercentCompany + "','" + objUserDetails.CompanyName + "');", true);
                            else
                                Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummary('" + DisplayBroker.Replace("'", "#") + "','" + dRow["BrokerSubPercent"].ToString() + "','" + dRow["BrokerCommission"].ToString() + "','" + TermTypeId + "','-1','" + objUserDetails.PercentCompany + "','" + objUserDetails.CompanyName + "');", true);
                            CommPerTot = CommPerTot + Convert.ToDouble(dRow["BrokerSubPercent"].ToString());
                            CommAmtTot = CommAmtTot + Convert.ToDouble(dRow["BrokerCommission"].ToString());
                            i = i + 1;
                        }
                        No = rnd.Next(550, 580);
                        Page.ClientScript.RegisterStartupScript(GetType(), "myScript1_" + No, "javascript:GenerateSummaryTotal('" + CommPerTot + "','" + CommAmtTot + "','" + TermTypeId + "','false');", true);
                    }

                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "GenerateSummary", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    private void GetGeoAppUsers()
    {
        try
        {
            ChargeSlip obj = new ChargeSlip();
            // lblerr.Value =   "  1  ";
            DataTable dtUsers = obj.GetGeoAppUsers();
            if (dtUsers != null && dtUsers.Rows.Count > 0)
            {
                chkUsers.DataSource = dtUsers;
                // lblerr.Value = lblerr.Value + "  2  ";


                //   lblerr.Value = lblerr.Value + dtUsers.Rows.Count.ToString() + "  2  ";
                chkUsers.DataBind();
                // lblerr.Value = lblerr.Value + "  3 ";
            }
            //else
            //{
            //    lblerr.Value = lblerr.Value +  " dt is null " ;
            //}
        }
        catch (Exception ex)
        {
            // lblerr.Value = lblerr.Value + " catch"+  ex.Message.ToString(); 
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "GetGeoAppUsers", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }


    [System.Web.Services.WebMethod(EnableSession = true)]
    public static Boolean InsertPropertyAccessRights(string Users, string SaveStatus)
    {
        try
        {
            Boolean reslt = false;
            if (HttpContext.Current.Session[GlobleData.NewChargeSlipId] != null)
            {
                ChargeSlip objChargeSlip = (ChargeSlip)(HttpContext.Current.Session[GlobleData.NewChargeSlipObj]);
                ChargeSlip NewobjChargeSlipId = new ChargeSlip();
                NewobjChargeSlipId.ChargeSlipID = objChargeSlip.ChargeSlipID;
                NewobjChargeSlipId.Load();
                if (SaveStatus == "N")
                {
                    NewobjChargeSlipId.Status = 1;
                }

                ////mpPrivacySettings.Show();
                reslt = NewobjChargeSlipId.Save();
                if (reslt == true)
                {
                    // undo below 3 line after test

                    NewobjChargeSlipId.TotAnnualRent = Convert.ToDecimal(HttpContext.Current.Session[GlobleData.TotalRent]);
                    NewobjChargeSlipId.UserIds = Users;
                    NewobjChargeSlipId.PushDataToGeoApp();


                    //page.ViewState["Result"] = reslt;
                }
                // Utility.ClearSessionExceptUser();
            }
            return reslt;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "InsertPropertyAccessRights", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return false;
        }
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string CheckDueDatesPercentageByTermType()
    {
        try
        {
            string ErrorMsg = "";
            if (HttpContext.Current.Session[GlobleData.NewChargeSlipId] != null)
            {
                ChargeSlip objChargeSlip = (ChargeSlip)(HttpContext.Current.Session[GlobleData.NewChargeSlipObj]);
                CommissionDueDates objCommissionDueDates = new CommissionDueDates();
                objCommissionDueDates.ChargeSlipId = objChargeSlip.ChargeSlipID;
                DataTable dt = objCommissionDueDates.GetListAsPerChargeSlipId();

                var result = from tab in dt.AsEnumerable()
                             group tab by tab["TermTypeId"]
                    into groupDt
                             select new
                             {
                                 TermTypeId = groupDt.Key.ToString(),
                                 Sum = groupDt.Sum((r) => decimal.Parse(r["CommPer"].ToString()))
                             };
                if (result.Count() > 0)
                {
                    foreach (var item in result)
                    {
                        if (item.Sum != 100)
                        {
                            if (item.TermTypeId == "1")
                            {
                                ErrorMsg += "Base Term, ";
                            }
                            else if (item.TermTypeId == "2")
                            {
                                ErrorMsg += "Option Term 1, ";
                            }
                            else if (item.TermTypeId == "3")
                            {
                                ErrorMsg += "Contingent Term, ";
                            }
                            else if (item.TermTypeId == "4")
                            {
                                ErrorMsg += "Option Term 2, ";
                            }
                            else if (item.TermTypeId == "5")
                            {
                                ErrorMsg += "Option Term 3, ";
                            }
                            else if (item.TermTypeId == "6")
                            {
                                ErrorMsg += "Option Term 4, ";
                            }
                        }
                        else
                        {
                            ErrorMsg += "";
                        }
                    }
                }
                else
                {
                    ErrorMsg += "Commissions Term";
                }
                return ErrorMsg;
            }
            return ErrorMsg;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "CheckDueDatesPercentageByTermType", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return "";
        }
    }

    private Boolean SendEmail(string EmailFrom, string EmailTo, string ReceptionistName)
    {
        try
        {
            PageTitle.Style.Add("display", "block");
            Table1.Border = 1;
            //Table1.Style.Add("border", "2px solid");
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            dvReview.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 8f);
            pdfDoc.HtmlStyleClass = "PdfBorder";
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                PdfWriter.GetInstance(pdfDoc, memoryStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                byte[] bytes = memoryStream.ToArray();
                memoryStream.Close();
                EmailUtility objEmail = new EmailUtility();
                objEmail.EmailFrom = EmailFrom;///"jaywanti.jain@rhealtech.com";
                objEmail.EmailTo = EmailTo; //"Diptee.Adhatrao@rhealtech.com";
                objEmail.EmailSub = "ChargeSlipReview";
                objEmail.EmailBody = "<html><body><table><tr><td>Hello " + ReceptionistName + ",</td><tr><tr><td><p>    Please Find Attached Document.</td><tr><tr><td><br><br><br><br><br>Thanks & Regards,<br>" + Session["UserName"].ToString() + "</td><tr></table></body></html>";
                objEmail.Memorybyte = bytes;
                objEmail.EmailAttachmentName = "ChargeSlipReview.pdf";
                Boolean IsMailSend = objEmail.SendEmailWithPdfAttachment();
                return IsMailSend;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "SendEmail", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return false;
        }
    }
    //private string GenerateBreackLineForPDFFormat(string Source, string Title)
    //{
    //    string TitleStr = Title;
    //    int count = new Regex("<br/>").Matches(Source).Count;
    //    for (int i = 0; i < count; i++)
    //    {
    //        TitleStr += BreakLine;
    //    }
    //    return TitleStr;
    //}


    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }
    protected void btnSendMail_Click(object sender, EventArgs e)
    {
        try
        {
            //send Email to admin
            //string EmailFrom, EmailTo, ReceptionistName;
            //DataTable dt;
            //Users objUsers = new Users();
            //objUsers.UserID = Convert.ToInt32(HttpContext.Current.Session[GlobleData.User].ToString());
            //dt = objUsers.GetUserAndAdminEmail();
            //EmailFrom = dt.Rows[0]["UserEmail"].ToString();
            //EmailTo = "samiya.tisekar@rhealtech.com";//dt.Rows[0]["AdminEmail"].ToString();
            //ReceptionistName = dt.Rows[0]["AdminName"].ToString();
            //if (!(string.IsNullOrEmpty(EmailFrom) && string.IsNullOrEmpty(EmailTo)))
            //{
            //    SendEmail(EmailFrom, EmailTo, ReceptionistName);
            //}
            string ChargeslipId = "0";
            if (Session[GlobleData.NewChargeSlipObj] != null)
            {
                ChargeSlip objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
                ChargeslipId = objChargeSlip.ChargeSlipID.ToString();
            }
            Utility.ClearSessionExceptUser();
            Response.Redirect("ChargeSlipNewsForm.aspx?ChargeslipId=" + ChargeslipId, false);
            //Utility.ClearSessionExceptUser();
            //Response.Redirect("AddOrEditNewChargeSlip.aspx", false);\
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "btnSendMail_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void btnResend_Click(object sender, EventArgs e)
    {
        try
        {
            ChargeSlip objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
            objChargeSlip.SetResendChargeSlip();
            Utility.ClearSessionExceptUser();
            if (HttpContext.Current.Session["UserType"] != null)
            {
                if (HttpContext.Current.Session["UserType"].ToString() == "1")
                {
                    Response.Redirect("~/Admin/Snapshot.aspx", false);
                }
                else
                {
                    Response.Redirect("AddOrEditNewChargeSlip.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "btnResend_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
        //  Response.Redirect("AddOrEditNewChargeSlip.aspx", false);
    }




    protected void btnEditRegenerateClick_Click(object sender, EventArgs e)
    {
        try
        {
            ChargeSlip objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
            objChargeSlip.SetResendChargeSlip();
            Utility.ClearSessionExceptUser();
            if (HttpContext.Current.Session["UserType"] != null)
            {
                if (HttpContext.Current.Session["UserType"].ToString() == "1")
                {
                    Response.Redirect("~/Admin/Snapshot.aspx", false);
                }
                else
                {
                    Response.Redirect("AddOrEditNewChargeSlip.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "btnEditRegenerateClick_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }
    protected void grdLeaseTerm_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label PeriodRent = ((Label)e.Row.FindControl("lbItemPeriodRent"));

                if (PeriodRent != null)
                {
                    if (!string.IsNullOrEmpty(PeriodRent.Text))
                    {
                        totPeriodRent += Convert.ToDecimal(PeriodRent.Text);
                    }
                }

            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label PeriodRent = ((Label)e.Row.FindControl("lblPeriodRent"));
                if (PeriodRent != null)
                {
                    PeriodRent.Text = totPeriodRent.ToString("N2");
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeSlipReview.aspx", "grdLeaseTerm_RowDataBound", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

}