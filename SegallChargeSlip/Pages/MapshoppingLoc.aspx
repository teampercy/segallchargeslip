﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MapshoppingLoc.aspx.cs" Inherits="Pages_MapshoppingLoc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
    <%--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&Place=true&libraries=places"></script>--%>    
    <%-- Use this keyOnly for Local Purpose key=AIzaSyDSDb_7E8u8qM8-klC2YslnlCCo6SVCGOU --%>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDSDb_7E8u8qM8-klC2YslnlCCo6SVCGOU&sensor=false&Place=true&libraries=places"></script>
    <script src="../Js/GoogleMap/infobox.js"></script>

    <link href="../Styles/Style.css" rel="stylesheet" />
    <link href="../Styles/SiteTEST.css" rel="stylesheet" />
    <title></title>

    <script type="text/javascript">
        var overlay;
        var mapgoogle;
        var markersArray = [];
        var strLocationDesc;
        var autocomplete;
        var AutoCompleteAddress = '';

        //$(function () {

        //     loadShowMap();
        //});
        google.maps.event.addDomListener(window, 'load', loadShowMap);

        function setLocation() {
           // alert('SetLocation gets called...!');
           // debugger;
            var place = autocomplete.getPlace();
            AutoCompleteAddress = '';
            if (place.address_components) {
                AutoCompleteAddress = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || ''),
                    (place.address_components[3] && place.address_components[3].short_name || ''),
                    (place.address_components[4] && place.address_components[4].short_name || ''),
                    (place.address_components[5] && place.address_components[5].short_name || ''),
                    (place.address_components[6] && place.address_components[6].short_name || ''),
                    (place.address_components[7] && place.address_components[7].short_name || ''),
                    (place.address_components[8] && place.address_components[8].short_name || ''),
                    (place.address_components[9] && place.address_components[9].short_name || ''),
                    (place.address_components[10] && place.address_components[10].short_name || ''),
                    (place.address_components[11] && place.address_components[11].short_name || ''),
                    (place.address_components[12] && place.address_components[12].short_name || ''),
                    (place.address_components[13] && place.address_components[13].short_name || ''),
                    (place.address_components[14] && place.address_components[14].short_name || ''),
                ].join(' ');
            }
        }

        function loadShowMap() {            
            var input = document.getElementById('txtSearch');
            autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.addListener('place_changed', setLocation);
            var latlng = new google.maps.LatLng(41.4925374, -99.9018);
            var myOptions = {
                zoom: 11,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            geocoder = new google.maps.Geocoder();
            mapgoogle = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            NewMapLoad(mapgoogle, geocoder, '');

            $('#btnMapSearch').click(function () {
                deleteOverlays();
               // alert(AutoCompleteAddress);
                if (AutoCompleteAddress != '') {
                    NewMapLoad(mapgoogle, geocoder, AutoCompleteAddress);
                }
                else { alert('Please enter a Location to Search'); }
            });

            $('#btnCloneLocation').click(function () {
                geocoder = new google.maps.Geocoder();
                var address = $('#txtSearch').val();
                deleteOverlays();
                if (address != '') {
                    NewMapLoad(mapgoogle, geocoder, 'clone');

                }
                else { alert('Please enter a Location to Search'); }
            });


            $("#draggable").draggable({
                helper: 'clone',
                stop: function (e) {
                    var parentOffset = $('#map_canvas').parent().offset();
                    var point = new google.maps.Point(e.pageX - parentOffset.left, e.pageY - parentOffset.top);
                    var ll = overlay.getProjection().fromContainerPixelToLatLng(point);
                    var address;
                    geocoder = new google.maps.Geocoder();

                    geocoder.geocode({ 'address': "", 'latLng': ll, 'region': 'US' }, function (results, status) {

                        if (status == google.maps.GeocoderStatus.OK) {
                            deleteOverlays();
                            address = results[0].formatted_address;
                            if (address != '') {
                                NewMapLoad(mapgoogle, geocoder, address);
                            }
                            else { alert('Please enter a Location to Search'); }

                        }
                        else { alert('Please enter a Location to Search'); }
                    });
                }
            });
        }



        function NewMapLoad(Nmapgoogle, geocoder, searchplace) {            
            var strAddr = "Hollywood, LA"
            var strPopupDiv = "<table cellPadding='0' cellSpacing='0' style='width:100%;'> <tr><td>" + "Initial Point" + "</td></tr></table> ";
            var strSearchDiv = "<table cellPadding='0' cellSpacing='0' style='width:100%;'> <tr><td>" + "Search Point" + "</td></tr></table> ";
            var PointColor = "RED";

            var OrglatLon = document.getElementById('hdnactlocation');
            var Orglat = "", OrgLon = "";

            if (OrglatLon != null && OrglatLon.value.split('|').length > 1) {
                //alert('hi');
                Orglat = OrglatLon.value.split('|')[0];
                OrgLon = OrglatLon.value.split('|')[1];
                AddMappointsNew(Orglat, OrgLon, strPopupDiv, geocoder, mapgoogle, PointColor, strAddr, "Initial Point");
            }
            else if (OrglatLon != null && OrglatLon.value.trim() != "") {
                //var service = new google.maps.places.AutocompleteService();

                //service.getPlacePredictions({ input: OrglatLon.value }, function (predictions, status) {                   
                //    if (status == google.maps.places.PlacesServiceStatus.OK) {
                //        strAddr = predictions[0].structured_formatting.secondary_text; 
                //        AddMappointsNew(Orglat, OrgLon, strPopupDiv, geocoder, mapgoogle, PointColor, strAddr, "Initial Point");

                       
                //    }
                //});
                var service = new google.maps.places.PlacesService(Nmapgoogle);
                service.textSearch({ query: OrglatLon.value }, function (results, status) {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {
                        if (results.length > 0) {
                            strAddr = results[0].formatted_address;
                            AddMappointsNew(Orglat, OrgLon, strPopupDiv, geocoder, mapgoogle, PointColor, strAddr, "Initial Point");
                        }
                    }
                });

            }            
            
            if (searchplace != '' && searchplace.toLowerCase() != 'clone') {
                AddMappointsNew('', '', strSearchDiv, geocoder, mapgoogle, "GREEN", searchplace, "Search Point");
            }
            if (searchplace.toLowerCase() == 'clone') {
                AddMappointsNew(orglat, orglon, strSearchDiv, geocoder, mapgoogle, "GREEN", strAddr, "Search Point");
            }
            overlay = new google.maps.OverlayView();
            overlay.draw = function () { };
            overlay.setMap(mapgoogle);
        }

        function AddMappointsNew(lat, lon, strpopup, geocoder, mapgoogle, pointcolor, address, title) {
         
           // alert(title);
            var point;
            var iconlnk = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
             debugger;

            if (pointcolor != "" && pointcolor == "GREEN") {
                iconlnk = "http://maps.google.com/mapfiles/ms/icons/green-dot.png";
            }

            geocoder = new google.maps.Geocoder();
            //var addr = strpopup;
            var Latitude = lat;
            var longtitude = lon;
            var addressdiv = $('<div/>').html(strpopup).html();

            if (Latitude != '' && longtitude != '') {
                //add point using lat lan
                var latlon = Latitude + "," + longtitude;
                mapgoogle.setCenter(new google.maps.LatLng(Latitude, longtitude));
                var marker = new google.maps.Marker({
                    map: mapgoogle,
                    icon: iconlnk,
                    position: new google.maps.LatLng(Latitude, longtitude),
                    title: title
                });
              
                //JAY mapgoogle.setCenter(new google.maps.LatLng(Latitude, longtitude));

                // marker.setDraggable(true);

                //google.maps.event.addListener(marker, "dragend", function (event) {
                //    var point = marker.getPosition();
                //    map.panTo(point);

                //});

                var infowindow = new google.maps.InfoWindow({
                    content: addressdiv,
                    maxWidth: 370,
                    maxHeight: 230
                });
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(mapgoogle, marker);
                });
                if (pointcolor != "" && pointcolor == "GREEN") {
                    setSearchmarker(marker);
                }
                setSearchmarker(marker);
                //JAY markersArray.push(marker);
                //mapgoogle.setZoom(5);
            }
            else {
               // alert(address);
                geocoder.geocode({ 'address': address }, function (results, status) {
                   // alert(status);
                  //  alert(status);
                    if (status == google.maps.GeocoderStatus.OK) {
                        mapgoogle.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: mapgoogle,
                            icon: iconlnk,
                            position: results[0].geometry.location,
                            title: title
                        });
                        var infowindow = new google.maps.InfoWindow({
                            content: addressdiv,
                            maxWidth: 300,
                            maxHeight: 200
                        });
                        google.maps.event.addListener(marker, 'click', function () {
                            infowindow.open(mapgoogle, marker);
                        });
                       setSearchmarker(marker);
                        if (pointcolor != "" && pointcolor == "GREEN") {
                            setSearchmarker(marker);
                        }

                        markersArray.push(marker);
                       // mapgoogle.setZoom(18);
                    } else {
                        mapgoogle.setZoom(5);
                        alert("Geocode was not successful for the following reason: " + status);

                    }
                });
                //add point using geo code
            }

        }



        function setSearchmarker(marker) {
            debugger;
            google.maps.event.addListener(marker, 'dragend', function () {
                document.getElementById('hdnNewlocationLon').value = marker.position.lng();
                document.getElementById('hdnNewlocationLat').value = marker.position.lat();
            });

            marker.setDraggable(true);
            document.getElementById('hdnNewlocationLon').value = marker.position.lng();
            document.getElementById('hdnNewlocationLat').value = marker.position.lat();
            //alert(document.getElementById('hdnNewlocationLon').value + " ____" + document.getElementById('hdnNewlocationLat').value);
            //alert(marker.position.lat() + "_________" + marker.position.lng());
        }


        // Deletes all markers in the array by removing references to them
        function deleteOverlays() {
            document.getElementById('hdnNewlocationLon').value = "";
            document.getElementById('hdnNewlocationLat').value = "";
            if (markersArray) {
                for (i in markersArray) {
                    markersArray[i].setMap(null);
                }
                markersArray.length = 0;
            }
        }


        function getLaton() {

            if (document.getElementById('hdnNewlocationLon').value.trim() != "" && document.getElementById('hdnNewlocationLat').value.trim() != "") {
                parent.callfromIfram(document.getElementById('hdnNewlocationLat').value, document.getElementById('hdnNewlocationLon').value.trim());
                //set values and //close pop up 
            }
            else {
                alert('Please select New location first');
                //alert(document.getElementById('hdnNewlocationLon').value.trim() + "____" + document.getElementById('hdnNewlocationLat').value.trim());
                return false;
            }
        }
    </script>
    <style type="text/css">
        #draggable {
            /*position: absolute;
        width: 30px;
        height: 30px;*/
            z-index: 1000000000;
        }
    </style>
</head>
<body style="margin: 0px;">
    <form runat="server" style="height: 100%; padding: 0px 5px 0px 5px;">
        <table style="text-align: center; width: 100%; height: 100%">
            <tr>
                <td width="455px;">
                    <input id="txtSearch" type="text" placeholder="search address" style="width: 450px; height: 30px; float: left; padding-left: 10px;" />
                    <input runat="server" type="hidden" id="hdnNewlocationLon" value="" />
                    <input runat="server" type="hidden" id="hdnNewlocationLat" value="" />
                    <input type="text" runat="server" id="lbl123" value="adsf" visible="false" />

                    <input runat="server" type="hidden" id="hdnActLatLon" value="" />
                    <input runat="server" type="hidden" value=" Hollywood,LA " id="hdnactlocation" />
                </td>
                <td>
                    <a href="#" id="btnMapSearch" style="border: none; color: black; cursor: pointer; float: left">
                        <img src="../Images/Map%20Search-Button.gif" title="Search" width="35" height="30" style="border: none; vertical-align: middle;" />
                    </a>
                    &nbsp; &nbsp;
                    <a href="#" style="border: none; color: black; float: left; cursor: pointer; padding-left: 15px">
                        <img src="../Images/Map-Set-Location-Button.gif" id="draggable" title="Search" width="35" height="30" style="border: none; vertical-align: middle;" />
                    </a>

                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <div id="map_canvas" style="width: 100%; padding-left: .2%; height: 570px; background-color: GrayText"></div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="width: 98%;">
                        <a onclick="return deleteOverlays();" style="display: none">Clear all points</a>
                        <%--    <input id="btnnextMap" onclick="return getLaton();" class="btnSmallOrange"
                        style="background-color: #F68620; float: right; text-align: center" title="OK" value="OK" />--%>

                        <asp:Button ID="btnnextMap" Style="float: right" runat="server" Text="OK" OnClientClick="return getLaton();" CssClass="btnSmallOrange" />


                        <%-- <a onclick="return getLaton();" id="btnnextMap"  href="#"   class="SqureButton" style="cursor: hand; Textdecoration:none; " >NEXT </a>--%>

                        <a id="btnCloneLocation" style="display: none">Clone </a>
                    </div>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
