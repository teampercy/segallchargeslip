﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Pages_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Styles/Login.css" rel="stylesheet" />
    <title>Login Page</title>
</head>
<body>
    <form id="form1" runat="server">

        <section class="container">
            <div class="login">
                <h1>Login Here</h1>
                <p>
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </p>
                <p>
                    <label for="username">
                        Username</label>
                    <asp:TextBox ID="txtUser" runat="server" class="form-login"></asp:TextBox>
                </p>
                <p>
                    <label for="password">
                        Password</label>
                    <asp:TextBox ID="txtPassword" runat="server" class="form-login" TextMode="Password"></asp:TextBox>
                </p>
                <p class="remember_me">
                    <label>
                        <asp:CheckBox ID="chkRememberMe" runat="server" />
                        Remember me on this computer
                    </label>
                </p>
                <p class="submit">
                    <asp:Button ID="btnLogin" Text="Login" runat="server" OnClick="btnLogin_Click" />
                    <asp:Button ID="abc" runat="server" OnClick="abc_Click" Text="Click Here" />
                </p>
            </div>

            <div class="login-help">
                <p>Forgot your password? <a href="index.html">Click here to reset it</a></p>
            </div>

        </section>
    </form>
</body>
</html>
