﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.IO;
using SegallChargeSlipBll;
using System.Reflection;
using SegallChargeSlipBLL;
//using System.Runtime.Serialization;
//using System.Runtime.Serialization.Formatters.Binary;
//using EditableControls;

public partial class Pages_NewCharge : System.Web.UI.Page
{
    #region Variables
    DataTable dtDealSubType, dtAttachments;
    public int ucSel;
    public bool test = false;
    const byte cns_Status_Draft = 0;
    #endregion

    #region Page Events

    public bool IsNextClicked { get { return ViewState["IsNextClicked"] == null ? false : Convert.ToBoolean(ViewState["IsNextClicked"]); } set { ViewState["IsNextClicked"] = value; } }
    public bool IsTextChanged_ShoppingCenter { get { return ViewState["IsTextChanged_ShoppingCenter"] == null ? false : Convert.ToBoolean(ViewState["IsTextChanged_ShoppingCenter"]); } set { ViewState["IsTextChanged_ShoppingCenter"] = value; } }
    public bool IsTextChanged_Company { get { return ViewState["IsTextChanged_Company"] == null ? false : Convert.ToBoolean(ViewState["IsTextChanged_Company"]); } set { ViewState["IsTextChanged_Company"] = value; } }
   

    protected void Page_Load(object sender, EventArgs e)
    {


        //long totalSessionBytes = 0;
        //BinaryFormatter b = new BinaryFormatter();
        //MemoryStream m;
        //foreach (var obj in Session)
        //{
        //    m = new MemoryStream();
        //    b.Serialize(m, obj);
        //    totalSessionBytes += m.Length;
        //}


        //-----------------------------vv-----------TEST ONLY-------------

        // SetProperties -  Convert.ToInt32(Session["UserId"]); 
        //Session[GlobleData.User] = "9";
        // --- SK : TEST VALUE  commenteted 
        //--------- 08/12/2013 ----vv--------------------------
        //if (Session[GlobleData.User] == null)
        //{
        //    Session.Clear();
        //    Session.RemoveAll();
        //    Server.Transfer("Login.aspx");
        //    //Response.Redirect("Login.aspx");
        //}
        //-----------------------------^^-----------TEST ONLY-------------

        if (Utility.IsValidSession())
        {
            RefreshLoadUsercontrol();

            if (!IsPostBack)
            {
                try
                {                    
                    Utility.FillStates(ref ddlLocState);

                    CustomList objCL = new CustomList();
                    txtChargeDate.Text = DateTime.Now.Date.ToString("MM/dd/yyyy");
                    // hdnisAddresOrShopping.Value = "1"; //JAY to set hdn for shopping by default
                    Utility.FillDropDown(ref ddlDealType, objCL.GetDealTypes(), "DealType", "DealTypeID"); //Fill Deal Type
                    Utility.FillDropDown(ref ddlDealDesignationType, objCL.GetDealDesignationTypes(), "DealDesignationTypes", "DealDesignationTypesId"); //Fill Deal Designation Type
                    SetAttributes();
                    //   Utility.FillDropDown(ref ajaxcombLocShoppingCenter, objCL.GetLocationList(), "ShoppingCenter", "LocationID"); //Fill Locations
                    //    Utility.FillDropDown(ref ajaxcomCTCCompany, objCL.GetCompanyList(), "Company", "CompanyID"); //Fill Company

                    //Utility.FillDropDown(ref ddlCTState, objCL.GetStateList(), "StateName", "State");

                    //Utility.FillStates(ref ddlLocCounty);   //  ----------------------------  SK : 08/05 : missing field added
                    Utility.FillStates(ref ddlCTState);
                    Utility.FillStates(ref ddlLocState);          //  ----------------------------  SK : 08/02 : to enter address without popup
                    hdnIsShoppingCenterAddressSaved.Value = "0";//  ----------------------------  SK : 08/02 : to enter address without popup 
                    hdnIsCompanySaved.Value = "0";  //------ SK : 08/08 : to enter address
                    dtDealSubType = objCL.GetDealSubTypes(); //Fill Deal Sub Type
                    ViewState.Add("dtDealSubType", dtDealSubType);
                    FilterDealSubTypes();

                    //    HideAllUserControls();

                    //upNewCharge.Update();
                    if (Session[GlobleData.NewChargeSlipObj] != null)
                    {
                        SetPagesControlData();
                        //HideAllUserControls();
                    }
                    else // New charge slip JP
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "newchradio", "DisaableLocaitonFielsds();", true);

                    }
                }
                catch (Exception ex)
                {
                    ErrorLog.WriteLog("NewCharge.aspx", "Page_Load", ex.Message.ToString()+" \n Inner Exception:"+ (ex.InnerException!=null?ex.InnerException.ToString():"")+" \n Stack Trace:"+ ex.StackTrace.ToString());
                    // throw;
                }
                finally
                {
                    if (Session[GlobleData.NewChargeSlipObj] == null)
                    {
                        HideAllUserControls();
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "loceditsetprop", "toggleLocation('1');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "wmText", "applyWatermark();", true);
                
            }
        }
    }
    #endregion

    #region DropDownListEvents

    protected void ddlDealType_SelectedIndexChange(object sender, EventArgs e)
    {
        string str1 = txtDealName.Text;
        HideAllUserControls();
        FilterDealSubTypes();
        if (ddlDealType.SelectedIndex == 3)
        {
            ucConsulting.SetPropertiesDueDate();
        }
       
        test = true;
        ucSel = 0;
        ddlDealType.Focus();
    }

    protected void ddlDealSubType_SelectedIndexChange(object sender, EventArgs e)
    {
        LoadUsercontrol();
        ddlDealSubType.Focus();
        //   test = true;
    }

    #endregion

    #region Button Events
     
    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            
            //if (DateTime.Now <= DateTime.Parse("2014/02/03"))
            //{
                IsNextClicked = true;

                bool isShoppingCenterAndComapnySaved = IsShoppingCenterAndCompanySaved();
                if (isShoppingCenterAndComapnySaved)
                {
                    ProceedToNextStep();
                }
                else
                {
                    return;
                }
            //}
            //else
            //{
            //    return;
            //}


        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge", "btnNext_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
    }
     
    private bool IsShoppingCenterAndCompanySaved()
    {
        try
        {
            bool isDataSaved = true;
            #region Save ShoppingCenter and Company

            if (
                (hdnIsShoppingCenterAddressSaved.Value == "0" &&
                        (ucNewCenterOrCompany.IsAddressOnly && ucNewCenterOrCompany.IsAddressSaved == "0"))
                ||
                (hndIsShoppingCenterAddressChanged.Value == "1" && hdnIsShoppingCenterAddressSaved.Value == "0")
               //|| (hdnisAddresOrShopping.Value.Trim() == "2" && txtSearchLocShoppingCenter.Text != "")
               )
            {
                //Get the Location OBJECT
                Locations objLocation = new Locations();
                //if (hdnLocId.Value != "")
                //{

                if (hdnLocId.Value != "")
                {
                    objLocation.LocationID = Convert.ToInt32(hdnLocId.Value);
                    objLocation.Load();
                }
                else
                {
                    objLocation.ShoppingCenter = txtSearchLocShoppingCenter.Text.Trim();
                }
                GetLocationObject(objLocation);

                DataTable dt = objLocation.CheckForDuplication();
                if (dt != null && objLocation.IsDuplicate == true && hdnIsShoppingCenterAddressSaved.Value == "0")
                {

                    if (dt.Rows[0]["IsDuplicate"].ToString().Trim() == "0")
                    {
                        // SHOW THE DUPLICATE RECORDS FOR SHOPPING CENTER ( iff the location is not saved after update or save as new)

                        // CODE
                        ucNewCenterOrCompany.IsAddressSaved = "0";
                        // set duplicate locations
                        ucNewCenterOrCompany.SetDuplicateRecordsinGrid(dt);
                        if (hndIsShoppingCenterAddressChanged.Value == "1" && hdnisAddresOrShopping.Value == "2")
                        {
                            ucNewCenterOrCompany.IsAddressOnly = true;
                        }
                        // show the dulicate records modal popup and proceed save from User control
                        ucNewCenterOrCompany.ResetControls();
                        ucNewCenterOrCompany.SetControlPropertiesLocationFromObject(objLocation);
                        ucNewCenterOrCompany.ISNEXT = true;
                        AjaxControlToolkit.ModalPopupExtender mpExt = (AjaxControlToolkit.ModalPopupExtender)ucNewCenterOrCompany.FindControl("mpDuplicateShoppingCenter");
                        mpeAddNewComapny.Show();
                        mpExt.Show();
                        string locId = hdnLocId.Value;
                        isDataSaved = false; // return false;
                        return isDataSaved;
                        //ctd. update / New db save in popup
                    }
                    else
                    {
                        //JAYDUP
                        hdnIsShoppingCenterAddressSaved.Value = "1";
                        objLocation.LocationID = Convert.ToInt32(dt.Rows[0]["LocationID"].ToString().Trim());
                        objLocation.Load();
                        hdnLocId.Value = objLocation.LocationID.ToString();
                    }
                }
                else
                {
                    // SAVE THE SHOPPING CENTER if it is not saved from 1. Update 2. Save as new 3. New save

                    // CODE
                    // . . .
                    // mpDuplicateShoppingCenter.Hide(); 
                    if (hdnIsShoppingCenterAddressSaved.Value == "0")
                    {
                        ucNewCenterOrCompany.ShowDuplicateLocationsPopup = false;
                        if (hdnisAddresOrShopping.Value == "1")
                        {
                            objLocation.ShoppingCenter = txtSearchLocShoppingCenter.Text;
                            objLocation.Address1 = "";
                            objLocation.Address2 = "";
                        }
                        else if (hdnisAddresOrShopping.Value == "2")
                        {
                            objLocation.ShoppingCenter = "";
                        }
                        else
                        {
                            if (txtSearchLocShoppingCenter.Text.Trim() != "")
                            {
                                objLocation.ShoppingCenter = txtSearchLocShoppingCenter.Text;
                                objLocation.Address1 = "";
                                objLocation.Address2 = "";
                            }
                            else
                            {
                                objLocation.ShoppingCenter = "";
                            }
                        }
                        objLocation.CreatedBy = Convert.ToInt32(Session[GlobleData.User].ToString());
                        objLocation.LastModifiedOn = DateTime.Now;
                        objLocation.CreatedOn = DateTime.Now;
                        objLocation.LastModifiedBy = Convert.ToInt32(Session[GlobleData.User].ToString());
                        if (!objLocation.Save())
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "NewAddresOnlycenterNotSaved", "alert('Error : Shopping Center Could not be saved.');", true);
                        }
                        else
                        {
                            hdnLocId.Value = objLocation.LocationID.ToString();
                        }
                    }
                    //mpeAddNewComapny.Hide();
                    //this.Page.GetType().InvokeMember("SetLocationAddress", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
                }
                //}
                //else //Location is not been saved.
                //{
                //    

                //}
            }
            if (hdnIsCompanySaved.Value == "0")
            {
                // sk : 08/05 : SAVE THE COMPANY

                Company objCompany = new Company();


                if (!(String.IsNullOrEmpty(hdnCompId.Value)) || hdnCompId.Value == "0")
                {
                    objCompany.CompanyID = Convert.ToInt32(hdnCompId.Value);
                }
                else
                {
                    objCompany.CompanyID = 0;
                }

                objCompany.Load();
                GetCompanyObject(objCompany);
                DataTable dt = objCompany.CheckForDuplication();
                if (dt != null && objCompany.IsDuplicate == true && hdnIsCompanySaved.Value == "0")
                {
                    if (dt.Rows[0]["IsDuplicate"].ToString().Trim() == "0")
                    {

                        // SHOW THE DUPLICATE RECORDS FOR SHOPPING CENTER ( iff the location is not saved after update or save as new)

                        // CODE
                        ucNewCenterOrCompany.IsAddressSaved = "0";
                        // set duplicate locations
                        ucNewCenterOrCompany.SetDuplicateRecordsinGridCompany(dt);
                        //Comment by jaywanti on 02-01-14
                        //if (hdnIsCompanyChanged.Value == "1")
                        //{
                        //    ucNewCenterOrCompany.IsAddressOnly = true;
                        //}
                        //
                        ucNewCenterOrCompany.IsAddressOnly = true;
                        // show the dulicate records modal popup and proceed save from User control
                        ucNewCenterOrCompany.ResetControls();
                        ucNewCenterOrCompany.SetControlPropertiesCompanyFromObject(objCompany);
                        //we have to SetDuplicateRecordsinGridCompany 

                        //lblCmpName.Text = txtLandlordName.Text;
                        //lblactCmpAddr.Text = dt.Rows[0]["EnteredAddress"].ToString();
                        AjaxControlToolkit.ModalPopupExtender mpExt = (AjaxControlToolkit.ModalPopupExtender)ucNewCenterOrCompany.FindControl("mpDuplicateCompany");
                        mpeAddNewComapny.Show();
                        mpExt.Show();

                        string locId = hdnLocId.Value;
                        isDataSaved = false; // return false;
                        return isDataSaved;
                        //ctd. update / New db save in popup
                    }
                    else
                    {
                        hdnIsCompanySaved.Value = "1";
                        hdnCompId.Value = dt.Rows[0]["CompanyID"].ToString().Trim();
                    }
                }
                else
                {
                    // SAVE THE COMPANY if it is not saved from 1. Update 2. Save as new 3. New save

                    // CODE
                    // . . .
                    // mpDuplicateShoppingCenter.Hide();

                    //if (hdnIsShoppingCenterAddressSaved.Value == "0")
                    //{
                    //    ucNewCenterOrCompany.ShowDuplicateLocationsPopup = false;
                    //    if (!objLocation.Save())
                    //    {
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "NewAddresOnlycenterNotSaved", "alert('Error : Shopping Center Could not be saved.');", true);
                    //    }
                    //}
                    //mpeAddNewComapny.Hide();
                    //this.Page.GetType().InvokeMember("SetLocationAddress", System.Reflection.BindingFlags.InvokeMethod, null, this.Page, null);
                    if (hdnIsCompanySaved.Value == "0")
                    {
                        if (txtCTName.Text == "LANDLORD TRADE NAME..")
                        {
                            txtCTName.Text = string.Empty;
                        }
                        SaveCompany();
                    }

                }
                //SaveCompany();
            }
            return isDataSaved;
            #endregion
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog("NewCharge", "IsShoppingCenterAndCompanySaved", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return false;
        }
        

    }

    //private int SaveDataForNextStep()
    //{
    //   // int status = 0;        

    //}
    private void ProceedToNextStep()
    {
        try
        {
            mpeAddNewShoppingCenter.Hide();

            AjaxControlToolkit.ModalPopupExtender mpExDuplicateCompany = (AjaxControlToolkit.ModalPopupExtender)ucNewCenterOrCompany.FindControl("mpDuplicateCompany");
            AjaxControlToolkit.ModalPopupExtender mpExDuplicateShoppingCenter = (AjaxControlToolkit.ModalPopupExtender)ucNewCenterOrCompany.FindControl("mpDuplicateShoppingCenter");
            mpExDuplicateCompany.Hide();
            mpExDuplicateShoppingCenter.Hide();


            #region Proceed to Next Page
            // Validate Before Setting Properties

            // 1.First Check Which User Control Is Present On Page and Set Its Properties
            Boolean Duedt = true;
            if (ddlDealType.SelectedValue == "1")
            {

                //Check For Due Date Entry
                if (CheckDueDate() == true)
                {
                    if (ddlDealSubType.SelectedValue == "1")
                    {
                        ucSaleImmproved.SetProperties();
                        //UserControl_ucSaleTransactionImprovedLand uc1 = new UserControl_ucSaleTransactionImprovedLand();
                        //uc1.SetProperties();

                    }
                    else if (ddlDealSubType.SelectedValue == "2")
                    {
                        ucSaleUnImprovedLand.SetProperties();
                        //UserControl_ucSaleTransactionUnimmprovedLand uc2 = new UserControl_ucSaleTransactionUnimmprovedLand();
                        //uc2.SetProperties();
                    }
                }
                else
                {
                    Duedt = false;
                }
            }
            else if (ddlDealType.SelectedValue == "2")
            {
                if (ddlDealSubType.SelectedValue == "3")
                {
                    ucLeaseGround.SetProperties();
                    //UserControl_ucLeaseTransactionGround uc3 = new UserControl_ucLeaseTransactionGround();
                    //uc3.SetProperties();
                }
                else if (ddlDealSubType.SelectedValue == "4")
                {
                    ucLeaseBuilding.SetProperties();
                    //UserControl_ucLeaseTransactionBuilding uc4 = new UserControl_ucLeaseTransactionBuilding();
                    //uc4.SetProperties();
                }
            }

            else
            {
                if (CheckDueDate() == true)
                {
                    ucConsulting.SetProperties();
                    ucConsulting.GetLatestDueDates();
                }
                else
                {
                    Duedt = false;
                }
                //UserControl_ucConsultingTransaction uc5 = new UserControl_ucConsultingTransaction();
                //uc5.SetProperties();


            }
            //2.Set Content Page(Current Page) Properties
            if (hdnSaveStatus.Value == "D")
            {
                if (ValidateForSaveAsDraft() == false)
                {
                    Duedt = false;
                }
            }
            if (Duedt == true)
            {

                SetProperties();

                //3. Add Object Of Current Class To Session

                //4.Redirect To Another Page

                if (hdnSaveStatus.Value == "N")
                {
                    if (ddlDealType.SelectedValue == "2")
                    {

                        Response.Redirect("NewChargeLeaseTerms.aspx"); // Server.Transfer("NewChargeLeaseTerms.aspx");
                    }
                    else
                    {
                        Response.Redirect("NewChargeCommissionSpitCalCulator.aspx");
                    }
                    //else
                    //{
                    //    //Server.Transfer("NewChargeSlipReview.aspx"); .aspx
                    //    Response.Redirect("NewChargeSlipReview.aspx", false);
                    //}
                }
                else if (hdnSaveStatus.Value == "D") // D==save as draft for edit in next transactions 
                {
                    Utility.ClearSessionExceptUser();
                    Response.Redirect("AddOrEditNewChargeSlip.aspx", false);
                }

            }

            #endregion
        }
        catch (Exception ex)
        {
            //ErrorLog.WriteLog("NewCharge", "ProceedToNextStep", ex.Message);
            Utility.WriteErrorLog("NewCharge", "ProceedToNextStep", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }

    }

    public void GetLocationObject(Locations objLocations)
    {
        try
        {
            if (txtSearchLocShoppingCenter.Text != "")
            {
                objLocations.Address1 = txtlocSHPaddress1.Text;

            }
            else
            {
                //objLocations.ShoppingCenter = txtSearchLocShoppingCenter.Text;
                objLocations.Address1 = txtLocAddress1.Text;
            }
            objLocations.Address2 = txtLocAddress2.Text;
            objLocations.City = txtLocCity.Text;
            objLocations.State = ddlLocState.SelectedValue;
            objLocations.ZipCode = txtLocZipCode.Text;
            objLocations.Country = txtLocCounty.Text; // SK : 08/06 : added county
            objLocations.LastModifiedOn = DateTime.Now;
            objLocations.LastModifiedBy = Convert.ToInt32(Session[GlobleData.User].ToString());
            //JP add new for the location 
            if (hdnMapLatLon.Value.Trim() != "" && hdnMapLatLon.Value.ToString().Split('|').Count() == 2)
            {
                objLocations.Latitude = Convert.ToDecimal(hdnMapLatLon.Value.ToString().Split('|')[0].Trim());
                objLocations.Longitude = Convert.ToDecimal(hdnMapLatLon.Value.ToString().Split('|')[1].Trim());
            }
            else
            {
                objLocations.Latitude = 0;
                objLocations.Longitude = 0;

            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge", "GetLocationObject", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }

    }

    public void GetCompanyObject(Company objCompany)
    {
        try
        {
            objCompany.Email = txtCTEmail.Text; // SK : 08/06 : added Email
            objCompany.OwnershipEntity = txtCTOwnershipEntity.Text; // SK : 08/05 : added OwnershipEntity
            objCompany.Name = txtComapny.Text; //JAY switched comp name and CTName   // SK : 08/05 : added landlordship name
            objCompany.CompanyName = txtCTName.Text; //objCompany.CompanyName = txtCTName.Text; // SK : 08/05 Company name  = company name, Name => Landlord name
            objCompany.Address1 = txtCTAddress1.Text;
            objCompany.Address2 = txtCTAddress2.Text;
            objCompany.City = txtCTCity.Text;
            objCompany.State = ddlCTState.SelectedValue;
            objCompany.ZipCode = txtCTZipCode.Text;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge", "GetCompanyObject", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
    }
     
    // continue processing  after the location is saved for duplicate recoreds 1. save company if not saved 2. proceed to next step
    public void ContinueFromUpdateExistingToNextStep()
    {
        try
        {
            mpeAddNewShoppingCenter.Hide();
            //----------vv--------------- 08/07 SK : Removed -------
            //if (hdnIsCompanySaved.Value == "0")
            //{
            //    SaveCompany();                    
            //}
            //----------------^^-------------- ----------------    ----

            if (IsNextClicked)
            {
                bool isShoppingCenterAndComapnySaved = IsShoppingCenterAndCompanySaved();
                if (isShoppingCenterAndComapnySaved)
                {
                    ProceedToNextStep();
                }
                else
                {
                    return;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "locNext", "toggleLocation('1');", true);
            }

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge", "ContinueFromUpdateExistingToNextStep", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
    }

    // To save the company if not saved
    private void SaveCompany()
    {
        try
        {
            bool rslt = false;

            Company objComp = new Company();

            // COMPANY IS SAVED AS A NEW ENTRY
            //if (!string.IsNullOrEmpty(hdnCompId.Value))
            //{
            //    objComp.CompanyID = Convert.ToInt32(hdnCompId.Value);
            //}
            objComp.Load();
            objComp.Address1 = txtCTAddress1.Text;
            objComp.Address2 = txtCTAddress2.Text;
            objComp.City = txtCTCity.Text;
            objComp.State = ddlCTState.SelectedValue;
            objComp.ZipCode = txtCTZipCode.Text;
            objComp.CreatedBy = Convert.ToInt32(Session["UserID"]);
            objComp.CreatedOn = DateTime.Now;
            objComp.OwnershipEntity = txtCTOwnershipEntity.Text;   // ---- SK : 08/05 : add ownership entity
            objComp.Name = txtComapny.Text; // ----- SK : 08/05 : add landlord name
            //---^^^^-- samiya--12_27----
            ///changed as water mark was getting saved in db---
            objComp.CompanyName = (txtCTName.Text == "" || txtCTName.Text == "LANDLORD TRADE NAME..") ? string.Empty : txtCTName.Text;  // ---- SK : 08/05 : company name
            //---vvv-- samiya--12_27----
            objComp.Email = txtCTEmail.Text; // ------ sk : 08/06 : Email
            rslt = objComp.Save();

            //------------vv-------- SK : 07/25 : to verify if Company address saved For Next click
            if (rslt)
            {
                hdnIsCompanySaved.Value = "1";
                hdnCompId.Value = objComp.CompanyID.ToString();
            }
            else
            {
                hdnCompId.Value = "0";
                //Diptee
                if (hdnSaveStatus.Value == "S")
                {

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "NewCompanyNotSaved", "alert('Error : Company could not be saved.');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge", "SaveCompany", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }

    }

    #endregion

    #region CustomActions

    private void FilterDealSubTypes()
    {
        if (ViewState["dtDealSubType"] != null)
        {
            dtDealSubType = (DataTable)ViewState["dtDealSubType"];
            DataView dtView = dtDealSubType.DefaultView;
            string SelectedValue = (ddlDealType.SelectedValue.ToString() == "") ? "0" : ddlDealType.SelectedValue;
            dtView.RowFilter = "DealTypeID=" + SelectedValue;
            if (dtView.ToTable() == null)
            {
                if (dtView.ToTable().Rows.Count > 0)
                { trDealSubType.Visible = true;
                trDealSubTypeLabel.Visible = true;
                }
                else { trDealSubType.Visible = false;
                trDealSubTypeLabel.Visible = false;
                }
                
            }
            else
            {
                if (dtView.ToTable().Rows.Count > 0)
                { trDealSubType.Visible = true;
                trDealSubTypeLabel.Visible = true;
                }
                else { trDealSubType.Visible = false;
                trDealSubTypeLabel.Visible = false;
                }
                
            }
            Utility.FillDropDown(ref ddlDealSubType, dtView.ToTable(), "DealSubType", "DealSubTypeID");
            
            FillConsultingDropDown();
        }

    }
    private void RefreshLoadUsercontrol()
    {
        try
        {
            //UserControl uc=new UserControl();
            //pnUserControl.Controls.Clear();

            if (ddlDealType.SelectedValue == "1")
            {
                if (ddlDealSubType.SelectedValue == "1")
                {
                    // uc = (UserControl)this.LoadControl("~/UserControl/ucSaleTransactionImprovedLand.ascx");
                    ucSel = 1;
                    dvSalImmroved.Style.Remove("display");
                    dvSaleUnimmproved.Style.Add("display", "none");
                    dvLeaseGround.Style.Add("display", "none");
                    dvLeaseBuilding.Style.Add("display", "none");
                    dvConsulting.Style.Add("display", "none");
                    //ucSaleImmproved.test_1();
                    //ucSaleImmproved.SetDurDtAttribues_Impr();

                }
                else
                {
                    // uc = (UserControl)this.LoadControl("~/UserControl/ucSaleTransactionUnimmprovedLand.ascx");
                    ucSel = 2;

                    dvSalImmroved.Style.Add("display", "none");
                    dvSaleUnimmproved.Style.Remove("display");
                    dvLeaseGround.Style.Add("display", "none");
                    dvLeaseBuilding.Style.Add("display", "none");
                    dvConsulting.Style.Add("display", "none");
                    //ucSaleUnImprovedLand.test();

                }
            }
            else if (ddlDealType.SelectedValue == "2")
            {
                if (ddlDealSubType.SelectedValue == "3")
                {
                    // uc  =(UserControl)this.LoadControl("~/UserControl/ucLeaseTransactionGround.ascx");
                    ucSel = 3;
                    dvSalImmroved.Style.Add("display", "none");
                    dvSaleUnimmproved.Style.Add("display", "none");
                    dvLeaseGround.Style.Remove("display");
                    dvLeaseBuilding.Style.Add("display", "none");
                    dvConsulting.Style.Add("display", "none");

                }
                else
                {
                    //uc =(UserControl)this.LoadControl("~/UserControl/ucLeaseTransactionBuilding.ascx");
                    ucSel = 4;
                    dvSalImmroved.Style.Add("display", "none");
                    dvSaleUnimmproved.Style.Add("display", "none");
                    dvLeaseGround.Style.Add("display", "none");
                    dvLeaseBuilding.Style.Remove("display");
                    dvConsulting.Style.Add("display", "none");
                }
            }

            else if (ddlDealType.SelectedValue == "3")
            {
                HideAllUserControls();
                FillConsultingDropDown();
                //ucConsulting.SetPropertiesDueDate();
            }


            //pnUserControl.Controls.Add(uc);
            //if (pnUserControl.Controls.Count > 1)
            //{
            //    pnUserControl.Controls.Clear();
            //}
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "RefreshLoadUsercontrol", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
    }
    private void LoadUsercontrol()
    {
        try
        {
            //UserControl uc=new UserControl();
            //pnUserControl.Controls.Clear();

            if (ddlDealType.SelectedValue == "1")
            {
                if (ddlDealSubType.SelectedValue == "1")
                {
                    // uc = (UserControl)this.LoadControl("~/UserControl/ucSaleTransactionImprovedLand.ascx");
                    ucSel = 1;
                    dvSalImmroved.Style.Remove("display");
                    dvSaleUnimmproved.Style.Add("display", "none");
                    dvLeaseGround.Style.Add("display", "none");
                    dvLeaseBuilding.Style.Add("display", "none");
                    dvConsulting.Style.Add("display", "none");
                    ucSaleImmproved.test_1();
                    //ucSaleImmproved.SetDurDtAttribues_Impr();

                }
                else
                {
                    // uc = (UserControl)this.LoadControl("~/UserControl/ucSaleTransactionUnimmprovedLand.ascx");
                    ucSel = 2;

                    dvSalImmroved.Style.Add("display", "none");
                    dvSaleUnimmproved.Style.Remove("display");
                    dvLeaseGround.Style.Add("display", "none");
                    dvLeaseBuilding.Style.Add("display", "none");
                    dvConsulting.Style.Add("display", "none");
                    ucSaleUnImprovedLand.test();

                }
            }
            else if (ddlDealType.SelectedValue == "2")
            {
                if (ddlDealSubType.SelectedValue == "3")
                {
                    // uc  =(UserControl)this.LoadControl("~/UserControl/ucLeaseTransactionGround.ascx");
                    ucSel = 3;
                    dvSalImmroved.Style.Add("display", "none");
                    dvSaleUnimmproved.Style.Add("display", "none");
                    dvLeaseGround.Style.Remove("display");
                    dvLeaseBuilding.Style.Add("display", "none");
                    dvConsulting.Style.Add("display", "none");

                }
                else
                {
                    //uc =(UserControl)this.LoadControl("~/UserControl/ucLeaseTransactionBuilding.ascx");
                    ucSel = 4;
                    dvSalImmroved.Style.Add("display", "none");
                    dvSaleUnimmproved.Style.Add("display", "none");
                    dvLeaseGround.Style.Add("display", "none");
                    dvLeaseBuilding.Style.Remove("display");
                    dvConsulting.Style.Add("display", "none");
                }
            }

            else
            {

                HideAllUserControls();
                FillConsultingDropDown();
                ucConsulting.SetPropertiesDueDate();
            }


            //pnUserControl.Controls.Add(uc);
            //if (pnUserControl.Controls.Count > 1)
            //{
            //    pnUserControl.Controls.Clear();
            //}
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "LoadUsercontrol", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
    }

    private void FillConsultingDropDown()
    {
        if (ddlDealType.SelectedValue == "3")
        {
            //uc=(UserControl)this.LoadControl("~/UserControl/ucConsultingTransaction.ascx");
            ucSel = 5;
            HideAllUserControls();
            dvConsulting.Style.Remove("display");
        }
    }

    private void HideAllUserControls()
    {
        dvSalImmroved.Style.Add("display", "none");
        dvSaleUnimmproved.Style.Add("display", "none");
        dvLeaseGround.Style.Add("display", "none");
        dvLeaseBuilding.Style.Add("display", "none");
        dvConsulting.Style.Add("display", "none");

        //dvSalImmroved.Style.Add("","") = false;
        //dvSaleUnimmproved.Visible = false;
        //dvLeaseGround.Visible = false;
        //dvLeaseBuilding.Visible = false;
        //dvConsulting.Visible = false;
    }

    private void SaveNotificationDays()
    {
        #region save Notification days
        try
        {
            if (Session[GlobleData.NewChargeSlipId] != null)
            {
                CommissionDueDatesNotification objCommissionDueDatesNotification = new CommissionDueDatesNotification();
                //long ChargeSlipId = 182085;
                objCommissionDueDatesNotification.ChargeSlipId = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                DataSet ds = objCommissionDueDatesNotification.GetCommissionDueDatesByChargeSlip(objCommissionDueDatesNotification.ChargeSlipId);
                long CommissionDueId = 0;
                //string NotifyDays = "15,30,60,100";
                string NotifyDays = Session["NotifyDays"].ToString();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        CommissionDueId = 0;
                        CommissionDueId = Convert.ToInt64(row["CommissionDueId"]);
                        objCommissionDueDatesNotification.CommissionDueId = CommissionDueId;
                        objCommissionDueDatesNotification.DueDate = Convert.ToDateTime(row["DueDate"]);
                        objCommissionDueDatesNotification.Delete(NotifyDays);
                        int[] NotifyDaysArray = NotifyDays.Split(',').Select(x => Convert.ToInt32(x)).ToArray();
                        for (int i = 0; i < NotifyDaysArray.Length; i++)
                        {
                            objCommissionDueDatesNotification.IsNotifyOn = 0;
                            objCommissionDueDatesNotification.IsNotifyOn = NotifyDaysArray[i];
                            objCommissionDueDatesNotification.CommissionDueId = CommissionDueId;
                            objCommissionDueDatesNotification.Save();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "SaveNotificationDays", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }

        #endregion
    }


    private void SetProperties()
    {
        try
        {
            DealTransactions objDealTransactions = (DealTransactions)Session[GlobleData.DealTransactionObj];
            ChargeSlip objChargeSlip = new ChargeSlip();
            objChargeSlip.DealName = txtDealName.Text;
            objChargeSlip.LocationID = Convert.ToInt32(hdnLocId.Value);
            objChargeSlip.ChargeDate = Convert.ToDateTime(Convert.ToString(txtChargeDate.Text));
            objChargeSlip.DealDesignationTypesId = Convert.ToInt32(ddlDealDesignationType.SelectedValue); //Added by Jaywanti on 15-11-2016
            objChargeSlip.CompanyID = Convert.ToInt32(hdnCompId.Value);
            objChargeSlip.CreatedBy = Convert.ToInt32(Session[GlobleData.User]);    // Convert.ToInt32(Session["UserId"]);  // --- SK : TEST VALUE  commenteted 
            objChargeSlip.CreatedOn = DateTime.Now;
            // Set Staus as Draft--- before final save

            if (Session[GlobleData.cns_Is_Complete] != null)
            {
                if (Convert.ToBoolean(Session[GlobleData.cns_Is_Complete].ToString()) == true)
                {
                    objChargeSlip.Status = 1;

                }
                else
                {

                    objChargeSlip.Status = 0;
                }
            }
            else
            {
                objChargeSlip.Status = 0;
            }



            objChargeSlip.DealTransactionsProperty = objDealTransactions;
            Session.Add(GlobleData.NewChargeSlipObj, objChargeSlip);
            if (Session[GlobleData.NewChargeSlipId] != null && Session[GlobleData.DealTransactionId] != null)
            {
                objChargeSlip.ChargeSlipID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                objDealTransactions.DealTransactionID = Convert.ToInt32(Session[GlobleData.DealTransactionId]);
            }
            // objChargeSlip.DealTransactionId = objDealTransactions.DealTransactionID;
            if (Session[GlobleData.NewChargeSlipId] != null)
            {
                objChargeSlip.ChargeSlipID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                objChargeSlip.LastModifiedBy = Convert.ToInt32(Session[GlobleData.User]);
                objChargeSlip.LastModifiedOn = DateTime.Now;
            }
            //set chargslip location and company values JAY to save vales in the database

            objChargeSlip.LocShoppingCenter = txtSearchLocShoppingCenter.Text.Trim();
            if (txtSearchLocShoppingCenter.Text.Trim() != "")
            {
                objChargeSlip.LocAddress1 = txtlocSHPaddress1.Text.Trim();

            }
            else
            {
                objChargeSlip.LocAddress1 = txtLocAddress1.Text.Trim();
            }
            objChargeSlip.LocAddress2 = txtLocAddress2.Text.Trim();
            objChargeSlip.LocCity = txtLocCity.Text.Trim();
            objChargeSlip.LocState = ddlLocState.SelectedItem.Value;
            objChargeSlip.LocCounty = txtLocCounty.Text.Trim();
            objChargeSlip.LocZipCode = txtLocZipCode.Text.Trim();

            if (hdnMapLatLon.Value.Trim() != "" && hdnMapLatLon.Value.Split('|').Length > 1)
            {
                objChargeSlip.LocLatitude = Convert.ToDecimal(hdnMapLatLon.Value.Split('|')[0].ToString());
                objChargeSlip.LocLongitude = Convert.ToDecimal(hdnMapLatLon.Value.Split('|')[1].ToString());
            }

            //.Text.Trim();
            objChargeSlip.CTOwnershipEntity = txtCTOwnershipEntity.Text.Trim();
            objChargeSlip.Company = txtCTName.Text.Trim();
            objChargeSlip.CTName = txtComapny.Text.Trim();
            objChargeSlip.CTEmail = txtCTEmail.Text.Trim();
            objChargeSlip.CTAddress1 = txtCTAddress1.Text.Trim();
            objChargeSlip.CTAddress2 = txtCTAddress2.Text.Trim();
            objChargeSlip.CTCity = txtCTCity.Text.Trim();
            //objChargeSlip.CTCounty =  
            objChargeSlip.CTState = ddlCTState.SelectedItem.Value;
            objChargeSlip.CTZipCode = txtCTZipCode.Text.Trim();

            if (ddlDealType.SelectedIndex == 0 || ddlDealType.SelectedIndex == -1)
            {
                objChargeSlip.DealTransactionsProperty.DealType = 0;
                objChargeSlip.DealTransactionsProperty.DealSubType = 0;
                objDealTransactions.DealType = 0;
                objDealTransactions.DealSubType = 0;
            }
            if (ddlDealSubType.SelectedIndex == 0 || ddlDealSubType.SelectedIndex == -1)
            {
                objChargeSlip.DealTransactionsProperty.DealSubType = 0;
                objDealTransactions.DealSubType = 0;
            }

            //
            objChargeSlip.Save();
            Session[GlobleData.NewChargeSlipId] = objChargeSlip.ChargeSlipID;

            objDealTransactions.ChargeSlipID = objChargeSlip.ChargeSlipID;

            if (objDealTransactions.DealType == 1)
            {
                if (objChargeSlip.DealTransactionsProperty.Buyer == 0)
                {
                    DealParties objDp = new DealParties();
                    objDp.PartyCompanyName = objDealTransactions.DealPartyCompanyName;
                    objDp.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    objDp.CreatedOn = DateTime.Now;
                    objDp.IsActive = true;
                    objDp.PartyTypeId = 1;
                    bool rsltbuyer = objDp.Save();
                    if (rsltbuyer == true)
                    {
                        //TextBox txtBuyer = (TextBox)this.Parent.FindControl("txtBuyer");
                        // HiddenField hdnBuyerID = (HiddenField)this.Parent.FindControl("hdnBuyerId");

                        // txtBuyer.Text = txtComapanyName.Text.Trim();
                        objDealTransactions.Buyer = (objDp.PartyID);

                    }

                }
            }
            else if (objDealTransactions.DealType == 2)
            {
                if (objChargeSlip.DealTransactionsProperty.Tenant == 0)
                {
                    DealParties objDp = new DealParties();
                    objDp.PartyCompanyName = objDealTransactions.DealPartyCompanyName;
                    objDp.CreatedBy = Convert.ToInt32(Session["UserId"]);
                    objDp.CreatedOn = DateTime.Now;
                    objDp.IsActive = true;
                    objDp.PartyTypeId = 2;
                    bool rslttenant = objDp.Save();
                    if (rslttenant == true)
                    {
                        //TextBox txtBuyer = (TextBox)this.Parent.FindControl("txtBuyer");
                        // HiddenField hdnBuyerID = (HiddenField)this.Parent.FindControl("hdnBuyerId");

                        // txtBuyer.Text = txtComapanyName.Text.Trim();
                        objDealTransactions.Tenant = (objDp.PartyID);

                    }
                }
            }
            bool IsUpdateAdjustment=false;
            if (objDealTransactions.DealType == 2)
            {
                if (hdnSize.Value != "")
                {
                    decimal oldSize = Convert.ToDecimal(hdnSize.Value.ToString());
                    decimal Size = objDealTransactions.Size;
                    String OldUnitValue = hdnOldUnitValue.Value.ToString();
                    string OldUnitTypeValue = hdnOldUnitTypeValue.Value.ToString();
                    if ((oldSize != Size) || (OldUnitValue != Convert.ToString(objDealTransactions.UnitValue)) || (OldUnitTypeValue != Convert.ToString(objDealTransactions.UnitTypeValue)))
                    {
                        IsUpdateAdjustment = true;
                    }

                }
            }
            Boolean rslt = objDealTransactions.Save();
            //Added by Jaywanti on 31-08-2015
            if (objDealTransactions.DealType == 2)
            {
                if (IsUpdateAdjustment)
                {
                    CommissionDueDates objCommissionDueDates = new CommissionDueDates();
                    objCommissionDueDates.ChargeSlipId = objChargeSlip.ChargeSlipID;
                    DataTable dueDt = objCommissionDueDates.CheckPaymentpaid();
                    Boolean IsPaymentComp;
                    if (dueDt != null)
                    {
                        if (dueDt.Rows.Count > 0)
                        {
                            //Session[GlobleData.cns_IsFullCommPaid] = true;
                            for (int i = 0; i <= dueDt.Rows.Count - 1; i++)
                            {
                                IsPaymentComp = Convert.ToBoolean(dueDt.Rows[i]["IsPayment"].ToString());
                                if (IsPaymentComp == false || IsPaymentComp == true) //In both cases we have to checkd the size was edited or not
                                {
                                    objDealTransactions.UpdateAmountAdjustment();
                                    break;
                                }
                            }
                        }
                    }
                }
            
            }
            Session.Add(GlobleData.DealTransactionId, objDealTransactions.DealTransactionID);
            Session.Add(GlobleData.DealTransactionObj, objDealTransactions);
            //if (rslt == true)
            //{
            if (objDealTransactions.DealType != 2)
            {
                if (Session[GlobleData.CommissionTb] != null)
                {
                    DataTable dt = (DataTable)Session[GlobleData.CommissionTb];
                    DataRow[] dRowSave = dt.Select("[DueDate] is not null");
                    if (dRowSave.Length > 0)
                    {
                        dt = dRowSave.CopyToDataTable();
                    }
                    //Boolean DueDtSaveNotAllowed = false;
                    //if (Session[GlobleData.cns_IsFullCommPaid] != null)
                    //{
                    //    if (Convert.ToBoolean(Session[GlobleData.cns_IsFullCommPaid].ToString()))
                    //    {
                    //        DueDtSaveNotAllowed = true;
                    //    }
                    //}
                    //if (DueDtSaveNotAllowed == false)
                    //{
                        CommissionDueDates objCommissionDueDates = new CommissionDueDates();

                        //delte records for same Charge Slip
                        objCommissionDueDates.ChargeSlipId = objChargeSlip.ChargeSlipID;
                        objCommissionDueDates.DealTransactionId = objDealTransactions.DealTransactionID;

                        DataTable dtCommExist = new DataTable();
                        dtCommExist.Columns.Add("ChargeSlipId",typeof(Int64));
                        dtCommExist.Columns.Add("DueDate", typeof(DateTime));
                        dtCommExist.Columns.Add("PercentComm", typeof(decimal));
                        dtCommExist.Columns.Add("CommAmountDue", typeof(decimal));
                        dtCommExist.Columns.Add("TermTypeId", typeof(Int16));
                        //dtCommExist.Columns.Add("RowId", typeof(Int16));
                        DataRow dr;
                        if (dt.Rows.Count != 0)
                        {
                            for (int i = 0; i <= dt.Rows.Count - 1; i++)
                            {
                                dr = dtCommExist.NewRow();
                                dr[0] = objCommissionDueDates.ChargeSlipId;
                                dr[1] = DateTime.ParseExact(dt.Rows[i]["DueDate"].ToString(), "MM/dd/yyyy", null);
                                dr[2] = Convert.ToDouble(dt.Rows[i]["CommPer"].ToString());
                                dr[3] = Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString());
                                dr[4] = 1;
                                //dr[5] = Convert.ToInt16(dt.Rows[i]["RowId"].ToString());
                                dtCommExist.Rows.Add(dr);
                            }
                        }
                                           //ADDED BY Jaywanti on 05-10-2013
                        //Check CommissionDueDate already exist for particular chargeslip ..

                            //if (dt.Rows.Count != 0)
                            //{
                            //    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                            //    {
                            //        objCommissionDueDates.PercentComm = Convert.ToDouble(dt.Rows[i]["CommPer"].ToString());
                            //        objCommissionDueDates.CommAmountDue = Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString());
                            //        objCommissionDueDates.DueDate = DateTime.ParseExact(dt.Rows[i]["DueDate"].ToString(), "MM/dd/yyyy", null);
                            //        objCommissionDueDates.TermTypeId = 1;
                            //        objCommissionDueDates.RowId = Convert.ToInt32(dt.Rows[i]["RowId"].ToString());
                        objCommissionDueDates.CommissiionDueDateCheckExist(dtCommExist);

                    #region save Notification days
                        SaveNotificationDays();
                    #endregion
                    //    }
                    //}
                    //END
                    //NOT REQUIRED AS UPDATE ,INSERT HAPPENING IN CommissiionDueDateCheckExist
                    //objCommissionDueDates.DeleteByChargeSlip();

                    //if (dt.Rows.Count != 0)
                    //{
                    //    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    //    {
                    //        objCommissionDueDates.PercentComm = Convert.ToDouble(dt.Rows[i]["CommPer"].ToString());
                    //        objCommissionDueDates.CommAmountDue = Convert.ToDecimal(dt.Rows[i]["CommAmount"].ToString());
                    //        objCommissionDueDates.DueDate = DateTime.ParseExact(dt.Rows[i]["DueDate"].ToString(), "MM/dd/yyyy", null);
                    //        objCommissionDueDates.Save();
                    //        objCommissionDueDates.CommissionDueId = 0;
                    //    }
                    //}
                    //  }
                }
                Session[GlobleData.CommissionTb] = null;
            }

            //objChargeSlip.Save();
            //Session[GlobleData.NewChargeSlipId] = objChargeSlip.ChargeSlipID;

            if (Session[GlobleData.Attachments] != null)
            {
                Attachments objAttachments1 = new Attachments();
                objAttachments1.ChargeSlipID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                objAttachments1.DeleteByChargeSlip();
                dtAttachments = (DataTable)(Session[GlobleData.Attachments]);
                if (dtAttachments != null)
                {
                    if (dtAttachments.Rows.Count > 0)
                    {
                        int i;
                        Attachments objAttachments = new Attachments();
                        for (i = 0; i <= dtAttachments.Rows.Count - 1; i++)
                        {
                            objAttachments.CreatedBy = Convert.ToInt32(Session[GlobleData.User]);
                            objAttachments.CreatedOn = DateTime.Now;
                            objAttachments.AttachmentDescription = dtAttachments.Rows[i]["UniqueAttachmentDescription"].ToString();
                            objAttachments.FilePath = dtAttachments.Rows[i][1].ToString();
                            objAttachments.ChargeSlipID = Convert.ToInt32(objChargeSlip.ChargeSlipID);
                            objAttachments.Save();
                            objAttachments.AttachmentID = 0;
                        }
                        Session[GlobleData.Attachments] = null;
                    }
                    else
                    {
                        Attachments objAttachments = new Attachments();
                        objAttachments.ChargeSlipID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                        objAttachments.DeleteByChargeSlip();
                    }
                }
            }
            hdnPageLeave.Value = "1";

        }
        //}
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge", "SetProperties", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }

    }

    private void SetAttributes()
    {
        //txtLocZipCode.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtLocZipCode.ClientID + "')");
        // txtCTZipCode.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtCTZipCode.ClientID + "')");

        //--------------vv--------- SK 08/02 : allow address change without the popup, should initiate process for new address save.

        // Shopping center
        txtLocAddress1.Attributes.Add("onkeyup", "javascript:return ShoppingCenterAddressChanged()");
        txtLocAddress2.Attributes.Add("onkeyup", "javascript:return ShoppingCenterAddressChanged()");
        txtLocCity.Attributes.Add("onkeyup", "javascript:return ShoppingCenterAddressChanged()");
        ddlLocState.Attributes.Add("onchange", "javascript:return ShoppingCenterAddressChanged()");
        txtLocZipCode.Attributes.Add("onkeyup", "javascript:return ShoppingCenterAddressChanged()");
        txtlocSHPaddress1.Attributes.Add("onkeyup", "javascript:return ShoppingCenterAddressChanged()");
        txtLocCounty.Attributes.Add("onkeyup", "javascript:return ShoppingCenterAddressChanged()");

        // txtMapLocation.Attributes.Add("onkeyup", "javascript:return ShoppingCenterAddressChanged()");
        //hdnMapLatLon.ValueChanged
        // company
        txtComapny.Attributes.Add("onkeyup", "javascript:return CompanyAddressChanged()");

        txtCTAddress1.Attributes.Add("onkeyup", "javascript:return CompanyAddressChanged()");
        txtCTAddress2.Attributes.Add("onkeyup", "javascript:return CompanyAddressChanged()");
        txtCTCity.Attributes.Add("onkeyup", "javascript:return CompanyAddressChanged()");
        ddlCTState.Attributes.Add("onchange", "javascript:return CompanyAddressChanged()");
        txtCTZipCode.Attributes.Add("onkeyup", "javascript:return CompanyAddressChanged()");


        //btnCancel.Attributes.Add("onclick", "javascript:return ClearControls('" + this.ClientID + "')");

        // txtCTOwnershipEntity.Attributes.Add("onkeyup", "javascript:return CompanyAddressChanged()");
        // txtCTName.Attributes.Add("onkeyup", "javascript:return CompanyAddressChanged()");
        // txtComapny.Attributes.Add("onkeyup", "javascript:return CompanyAddressChanged()");
        //--------------^^--------- SK 08/02 : allow address change without the popup, should initiate process for new address save.
    }

    //private void EnableLocationAdd()
    //{
    //    //  txtLocAddress1.ReadOnly = false;
    //    txtLocAddress1.ReadOnly = false;
    //    txtLocAddress2.ReadOnly = false;
    //    txtLocCity.ReadOnly = false;
    //    ddlLocState.Enabled = false;
    //    txtLocZipCode.ReadOnly = false;
    //}

    //private void DisableLocationAdd()
    //{
    //    //    txtLocAddress1.ReadOnly = true ;
    //    txtLocAddress1.ReadOnly = true;
    //    txtLocAddress2.ReadOnly = true;
    //    txtLocCity.ReadOnly = true;
    //    ddlLocState.Enabled = true;
    //    txtLocZipCode.ReadOnly = true;
    //}

    public void SetLocationAddress()
    {
        try
        {
            Locations objLocations = new Locations();
            if (ucNewCenterOrCompany.IsAddressOnly && !IsTextChanged_ShoppingCenter) // if not a text changed event & from called from address only
            {

                txtLocAddress1.Text = ucNewCenterOrCompany.Address1;
                txtLocAddress2.Text = ucNewCenterOrCompany.Address2;
                txtLocCity.Text = ucNewCenterOrCompany.City;
                ddlLocState.SelectedValue = ucNewCenterOrCompany.State;
                txtLocCounty.Text = ucNewCenterOrCompany.County;  // SK : 08/06 : Added country for location - missing field

                if (!string.IsNullOrEmpty(ucNewCenterOrCompany.State))
                {
                    if (ddlLocState.Items.FindByValue(ddlLocState.SelectedValue) != null)
                    {
                        ddlLocState.SelectedValue = ucNewCenterOrCompany.State;
                    }
                }
                else
                {
                    ddlLocState.SelectedIndex = 0;
                }

                if (ucNewCenterOrCompany.Lattitude != "" && ucNewCenterOrCompany.Lontitude != "")
                {
                    txtMapLocation.Text = " Lat :" + ucNewCenterOrCompany.Lattitude + " Lon: " + ucNewCenterOrCompany.Lontitude;
                    hdnMapLatLon.Value = ucNewCenterOrCompany.Lattitude + "|" + ucNewCenterOrCompany.Lontitude;
                }
                else
                {
                    txtMapLocation.Text = "";
                    hdnMapLatLon.Value = ucNewCenterOrCompany.Address1.Trim() + " " + ucNewCenterOrCompany.Address2.Trim() + " " + ucNewCenterOrCompany.City.Trim() + " " + ucNewCenterOrCompany.State.Trim()
                        + " " + ucNewCenterOrCompany.County.Trim() + " " + ucNewCenterOrCompany.ZipCode;
                }
                txtLocZipCode.Text = Convert.ToString(ucNewCenterOrCompany.ZipCode);
                hdnIsShoppingCenterAddressSaved.Value = "0";
                // hdnLocId.Value = "";
            }
            else
            {
                objLocations.LocationID = Convert.ToInt32(hdnLocId.Value);
                objLocations.Load();
                txtlocSHPaddress1.Text = objLocations.Address1;

                //txtLocAddress1.Text = objLocations.Address1;
                txtLocAddress2.Text = objLocations.Address2;
                txtLocCity.Text = objLocations.City;
                txtLocCounty.Text = objLocations.Country;  // SK : 08/06 : Added country for location - missing field


                if (!string.IsNullOrEmpty(objLocations.State))
                {
                    if (ddlLocState.Items.FindByValue(objLocations.State) != null)
                    {
                        ddlLocState.SelectedValue = objLocations.State;
                    }
                }
                else
                {
                    ddlLocState.SelectedIndex = 0;
                }
                txtLocZipCode.Text =Convert.ToString(objLocations.ZipCode);
                hdnIsShoppingCenterAddressSaved.Value = "1";
                //  hdnLocId.Value = "";
                if (objLocations.Latitude != 0 && objLocations.Longitude != 0)
                {
                    txtMapLocation.Text = " Lat :" + objLocations.Latitude + " Lon: " + objLocations.Longitude;
                    hdnMapLatLon.Value = objLocations.Latitude + "|" + objLocations.Longitude;
                }
                else
                {
                    txtMapLocation.Text = "";
                    hdnMapLatLon.Value = objLocations.Address1.Trim() + " " + objLocations.Address2.Trim() + " " + objLocations.City.Trim() + " " + objLocations.State.Trim()
                        + " " + objLocations.Country.Trim() + " " + objLocations.ZipCode.ToString();
                }
            }
            IsTextChanged_ShoppingCenter = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "locedit", "toggleLocation('1');", true);
            upLocation.Update();
            // upNewCharge.Update();
            // txtMapLocation.Focus();

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "SetLocationAddress", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
        //   DisableLocationAdd();
    }

    //set text box value here 
    public void setMapLocation()
    {
        //hdnMapLatLon.Value = ucMaplocation.NewLatitude +"|" +ucMaplocation.NewLongtitude;
        //txtMapLocation.Text = "Lat: " +ucMaplocation.NewLatitude +" Lon: "+ ucMaplocation.NewLongtitude;
        //upNewCharge.Update();
    }

    public void SetCompanyAddress()
    {
        try
        {
            Company objCompany = new Company();
            if (ucNewCenterOrCompany.IsAddressOnly && !IsTextChanged_Company) // if not a text changed event & from called from address only
            {
                txtCTAddress1.Text = ucNewCenterOrCompany.Address1;
                txtCTAddress2.Text = ucNewCenterOrCompany.Address2;
                txtCTCity.Text = ucNewCenterOrCompany.City;

                //-----vv-------- Address only is changed------------vv--------
                // txtCTOwnershipEntity.Text = ucNewCenterOrCompany.OwnershipEntity; // SK 08/05 : Ownership Entity
                //  txtCTName.Text = ucNewCenterOrCompany.LandlordName;   // SK 08/05 : Landlord Name
                // txtComapny.Text = ucNewCenterOrCompany.CompanyName;   // SK 08/05 : Company Name
                // txtCTEmail.Text = ucNewCenterOrCompany.Email;  // SK 08/06 : Email
                //----------vv-------- SK : 07/25 : set the company name
                // txtCTName.Text = txtComapny.Text;

                if (!string.IsNullOrEmpty(ucNewCenterOrCompany.State))
                {
                    if (ddlCTState.Items.FindByValue(ddlCTState.SelectedValue) != null)
                    {
                        ddlCTState.SelectedValue = ucNewCenterOrCompany.State;
                    }
                }
                else
                {
                    ddlCTState.SelectedIndex = 0;
                }
                txtCTZipCode.Text = Convert.ToString(ucNewCenterOrCompany.ZipCode);
                hdnIsCompanySaved.Value = "0";


            }
            else   // called from paged control data, and text changed
            {
                objCompany.CompanyID = Convert.ToInt32(hdnCompId.Value);
                objCompany.Load();
                //txtComapny.Text = objCompany.CompanyName;  // SK 08/05 : 
                //txtCTAddress1.Text = objCompany.Address1;
                //txtCTAddress2.Text = objCompany.Address2;
                //txtCTCity.Text = objCompany.City;
                //txtCTOwnershipEntity.Text = objCompany.OwnershipEntity;  // SK 08/05 : OwnershipEntity added
                //txtCTName.Text = objCompany.Name;  // SK 08/05 : Landlord name added
                //txtCTEmail.Text = objCompany.Email;  // SK 08/06 : Email

                txtComapny.Text = objCompany.Name;  // Jay 08/19 : 
                txtCTAddress1.Text = objCompany.Address1;
                txtCTAddress2.Text = objCompany.Address2;
                txtCTCity.Text = objCompany.City;
                //txtCTOwnershipEntity.Text = objCompany.OwnershipEntity;  Jay 08/19
                txtCTName.Text = objCompany.CompanyName;  // Jay 08/19
                txtCTEmail.Text = objCompany.Email;
                if (!string.IsNullOrEmpty(objCompany.State))
                {
                    if (ddlCTState.Items.FindByValue(ddlCTState.SelectedValue) != null)
                    {
                        ddlCTState.SelectedValue = objCompany.State;
                    }
                }
                else
                {
                    ddlCTState.SelectedIndex = 0;
                }
                txtCTZipCode.Text =Convert.ToString(objCompany.ZipCode);
                hdnIsCompanySaved.Value = "1";
            }
            IsTextChanged_Company = false;
            upChargeto.Update();
            //  hdnCompId.Value = "";
            //upNewCharge.Update();
            // ddlDealType.Focus();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "SetCompanyAddress", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
    }

    private void SetPagesControlData()
    {
        try
        {

            ChargeSlip objChargeSlip = (ChargeSlip)Session[GlobleData.NewChargeSlipObj];
            //if (objChargeSlip.ChargeSlipID != 0 && objChargeSlip.Status == cns_Status_Draft)
            if (objChargeSlip.ChargeSlipID != 0)
            {
                ChargeSlip NwobjChargeSlip = new ChargeSlip();
                DealTransactions NwobjDealTransactions = new DealTransactions();


                //JAY NEW LOGIC 9/12
                //Locations objLocations = new Locations();
                //objLocations.LocationID = objChargeSlip.LocationID;
                //objLocations.Load();

                //Company objCompany = new Company();
                //objCompany.CompanyID = objChargeSlip.CompanyID;
                //objCompany.Load();

                NwobjChargeSlip.ChargeSlipID = objChargeSlip.ChargeSlipID;
                NwobjChargeSlip.Load();

                // Set New Charge Slip Info
                Session[GlobleData.NewChargeSlipId] = objChargeSlip.ChargeSlipID;
                Session[GlobleData.DealTransactionId] = objChargeSlip.DealTransactionsProperty.DealTransactionID;

                txtDealName.Text = NwobjChargeSlip.DealName;
                txtSearchLocShoppingCenter.Text = Convert.ToString(objChargeSlip.LocShoppingCenter);
                hdnLocId.Value = Convert.ToString(objChargeSlip.LocationID);

                //JAY
                //SetLocationAddress();
                //Load from the charge slip

                txtComapny.Text = Convert.ToString(objChargeSlip.Company);
                hdnCompId.Value = Convert.ToString(objChargeSlip.CompanyID);
                txtCTOwnershipEntity.Text = objChargeSlip.CTOwnershipEntity;  // SK 08/13 : OwnershipEntity added
                txtCTName.Text = objChargeSlip.CTName;  // SK 08/13 : Landlord name added
                txtCTEmail.Text = objChargeSlip.CTEmail;  // SK 08/13 : Email
                txtChargeDate.Text = objChargeSlip.ChargeDate.ToString("MM/dd/yyyy");//Added 8-3-2017

                ddlDealDesignationType.SelectedValue = Convert.ToString(objChargeSlip.DealDesignationTypesId);
                //Once DealDesignationTypeId stored in database table we not allowed to edit it in future.
                ddlDealDesignationType.Enabled = false;

                //set status
                if (GlobleData.cns_Status_Draft == objChargeSlip.Status || GlobleData.cns_Status_Complete == objChargeSlip.Status)
                {
                    btnClearAll.Visible = false;
                }
                if (GlobleData.cns_Status_Complete == objChargeSlip.Status)
                {
                    Session[GlobleData.cns_Is_Complete] = true;
                    Session[GlobleData.cns_IsFullCommPaid] = false;
                    btnSaveForLater.Visible = false;
                    //Comment the below function to stiil edit the functionality when chargeslip submitted : 10-05-2018 Sudhakar
                    //disableChargeslipDetails();
                    CommissionDueDates objCommissionDueDates = new CommissionDueDates();

                    objCommissionDueDates.ChargeSlipId = objChargeSlip.ChargeSlipID;
                    DataTable dueDt = objCommissionDueDates.GetListAsPerChargeSlipId();
                    Boolean IsPaymentComp;
                    if (dueDt != null)
                    {
                        if (dueDt.Rows.Count > 0)
                        {
                            //Session[GlobleData.cns_IsFullCommPaid] = true;
                            for (int i = 0; i <= dueDt.Rows.Count - 1; i++)
                            {
                                IsPaymentComp = Convert.ToBoolean(dueDt.Rows[i]["IsFullCommPaid"].ToString());
                                if (IsPaymentComp == true)
                                {
                                    Session[GlobleData.cns_IsFullCommPaid] = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Session[GlobleData.cns_Is_Complete] = null;
                    Session[GlobleData.cns_IsFullCommPaid] = null;
                }
                //JAY  //SetCompanyAddress();

                SeLocationNCompany(objChargeSlip);
                txtSearchLocShoppingCenter.Text = objChargeSlip.LocShoppingCenter;
                if (txtSearchLocShoppingCenter.Text.Trim() != "")
                {
                    rdbLocation.SelectedIndex = rdbLocation.Items.IndexOf(rdbLocation.Items.FindByValue("0"));
                    txtlocSHPaddress1.Text = objChargeSlip.LocAddress1;
                    txtLocAddress1.Text = "";
                    //toggle diplay
                }
                else
                {
                    rdbLocation.SelectedIndex = rdbLocation.Items.IndexOf(rdbLocation.Items.FindByValue("1"));
                    //toggle displayk
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "loceditsetprop", "toggleLocation('1');", true);
                // if(return toggleLocation('1');

                ddlDealType.SelectedValue = Convert.ToString(objChargeSlip.DealTransactionsProperty.DealType);
                FilterDealSubTypes();
                ddlDealSubType.SelectedValue = Convert.ToString(objChargeSlip.DealTransactionsProperty.DealSubType);

                LoadUsercontrol();
                //  Session.Add(GlobleData.DealTransactionId, NwobjChargeSlip.DealTransactionId);
                //upNewCharge.Update();
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "SetPagesControlData", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
    }



    private void SeLocationNCompany(ChargeSlip ObjChargeSlip)
    {
        try
        {
            //objLocations.LocationID = Convert.ToInt32(hdnLocId.Value);
            //      objLocations.Load();
            txtLocAddress1.Text = ObjChargeSlip.LocAddress1;
            txtLocAddress2.Text = ObjChargeSlip.LocAddress2;
            txtLocCity.Text = ObjChargeSlip.LocCity;
            txtLocCounty.Text = ObjChargeSlip.LocCounty;  // SK : 08/06 : Added country for location - missing field

            if (!string.IsNullOrEmpty(ObjChargeSlip.LocState))
            {
                if (ddlLocState.Items.FindByValue(ObjChargeSlip.LocState) != null)
                {
                    ddlLocState.SelectedValue = ObjChargeSlip.LocState;
                }
            }
            else
            {
                ddlLocState.SelectedIndex = 0;
            }


            txtLocZipCode.Text = Convert.ToString(ObjChargeSlip.LocZipCode);
            hdnIsShoppingCenterAddressSaved.Value = "1";
            //  hdnLocId.Value = "";


            if (ObjChargeSlip.LocLatitude != 0 && ObjChargeSlip.LocLongitude != 0)
            {
                txtMapLocation.Text = " Lat :" + ObjChargeSlip.LocLatitude + " Lon: " + ObjChargeSlip.LocLongitude;
                hdnMapLatLon.Value = ObjChargeSlip.LocLatitude + "|" + ObjChargeSlip.LocLongitude;
            }
            else
            {
                txtMapLocation.Text = "";
                hdnMapLatLon.Value = ObjChargeSlip.LocAddress1.Trim() + " " + ObjChargeSlip.LocAddress2.Trim() + " " + ObjChargeSlip.LocCity.Trim() + " " + ObjChargeSlip.LocState.Trim()
                    + " " + ObjChargeSlip.LocCounty.Trim() + " " + ObjChargeSlip.LocZipCode.ToString();
            }


            IsTextChanged_ShoppingCenter = false;
            //upNewCharge.Update();
            // txtMapLocation.Focus();



            //objCompany.CompanyID = Convert.ToInt32(hdnCompId.Value);
            //objCompany.Load();

            txtComapny.Text = ObjChargeSlip.CTName;  // Jay 08/19 : 
            txtCTAddress1.Text = ObjChargeSlip.CTAddress1;
            txtCTAddress2.Text = ObjChargeSlip.CTAddress2;
            txtCTCity.Text = ObjChargeSlip.CTCity;
            txtCTOwnershipEntity.Text = ObjChargeSlip.CTOwnershipEntity;  // Jay 08/19
            txtCTName.Text = ObjChargeSlip.Company;  // Jay 08/19
            txtCTEmail.Text = ObjChargeSlip.CTEmail;
            if (!string.IsNullOrEmpty(ObjChargeSlip.CTState))
            {
                if (ddlCTState.Items.FindByValue(ddlCTState.SelectedValue) != null)
                {
                    ddlCTState.SelectedValue = ObjChargeSlip.CTState;
                }
            }
            else
            {
                ddlCTState.SelectedIndex = 0;
            }
            txtCTZipCode.Text = Convert.ToString(ObjChargeSlip.CTZipCode);
            hdnIsCompanySaved.Value = "1";
        }
        catch(Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "SeLocationNCompany", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }


    private Boolean CheckDueDate()
    {
        try
        { 
        // new changes for due dates
        if (ViewState[GlobleData.cns_DueDate] != null)
        {
            DataTable dt = (DataTable)(ViewState[GlobleData.cns_DueDate]);
        }

        if (Session[GlobleData.CommissionTb] != null)
        {
            DataTable dt = (DataTable)(Session[GlobleData.CommissionTb]);
            if (dt.Rows.Count == 0 && hdnSaveStatus.Value == "S")
            {
                lbErrorMsg.Text = "Enter Due Date";
                return false;
            }
        }
        else if (hdnSaveStatus.Value == "S" && Session[GlobleData.CommissionTb] == null)
        {
            lbErrorMsg.Text = "Enter Due Date";
            return false;
        }
        
        }
        catch(Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "CheckDueDate", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return false;
        }
        lbErrorMsg.Text = " ";
        return true;

    }

    private Boolean ValidateForSaveAsDraft()
    {
        try
        { 
        if (txtDealName.Text.Trim() == "")
        {
            lbErrorMsg.Text = "Please Enter Deal Name";
            txtDealName.Focus();
            return false;
        }
        if (txtSearchLocShoppingCenter.Text.Trim() == "" && rdbLocation.SelectedValue == "0")
        {
            lbErrorMsg.Text = "Please enter Showroom";
            txtSearchLocShoppingCenter.Focus();
            return false;
        }
        if (txtLocAddress1.Text.Trim() == "" && rdbLocation.SelectedValue == "1")
        {
            lbErrorMsg.Text = "Please enter Location address";
            txtLocAddress1.Focus();
            return false;
        }
        if (txtLocCity.Text.Trim() == "")
        {
            lbErrorMsg.Text = "Please Enter City";
            txtLocCity.Focus();
            return false;
        }
        if (ddlLocState.SelectedIndex == 0)
        {
            lbErrorMsg.Text = "Please Enter State";
            ddlLocState.Focus();
            return false;
        }
        if (txtLocZipCode.Text.Trim() == "")
        {
            lbErrorMsg.Text = "Please Enter Zip Code";
            txtLocZipCode.Focus();
            return false;
        }
        if (txtCTName.Text.Trim() == "")
        {
            lbErrorMsg.Text = "Please Enter Company Name";
            txtCTName.Focus();
            return false;
        }
        if (txtComapny.Text.Trim() == "")
        {
            lbErrorMsg.Text = "Please Select Company";
            txtComapny.Focus();
            return false;
        }
        if (txtCTAddress1.Text.Trim() == "")
        {
            lbErrorMsg.Text = "Please EnterComapny Address Line 1";
            txtCTAddress1.Focus();
            return false;
        }
        if (txtCTCity.Text.Trim() == "")
        {
            lbErrorMsg.Text = "Please Enter City";
            txtCTCity.Focus();
            return false;
        }
        if (ddlCTState.SelectedIndex == 0)
        {
            lbErrorMsg.Text = "Please Enter State";
            ddlCTState.Focus();
            return false;
        }
        if (txtCTZipCode.Text.Trim() == "")
        {
            lbErrorMsg.Text = "Please Enter Zip Code";
            txtCTZipCode.Focus();
            return false;
        }
        }
        catch(Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "ValidateForSaveAsDraft", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            return false;
        }
        return true;
    }

    #endregion

    #region Model Popus

    protected void btnAddNewComapny_Click(object sender, EventArgs e)
    {
        try
        { 
        lbTitle.Text = "ADD NEW COMPANY";
        ucNewCenterOrCompany.Name = "NAME";//JAY
        ucNewCenterOrCompany.ClassRef = "C";
        ucNewCenterOrCompany.PopupType = "C";
        ucNewCenterOrCompany.CenterName = ""; //txtComapny.Text; 
        ucNewCenterOrCompany.IsAddressOnly = false; // - SK 07/23
        ucNewCenterOrCompany.ResetControls();
        //ucNewCenterOrCompany.ActName = txtCTName.Text;
        hdnIsCompanySaved.Value = "0";
        ((UpdatePanel)ucNewCenterOrCompany.FindControl("upAddNewShoppingCenterOrCompany")).Update();
        IsNextClicked = false; // ------ SK : 08/07
        mpeAddNewComapny.Show();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "btnAddNewComapny_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());         
        }

    }

    protected void btnAddNewCenter_Click(object sender, EventArgs e)
    {
        try
        { 
        lbTitle.Text = "ADD NEW SHOPPING CENTER";
        ucNewCenterOrCompany.Name = "CENTER";
        ucNewCenterOrCompany.ClassRef = "SC";
        ucNewCenterOrCompany.PopupType = "SC";
        ucNewCenterOrCompany.IsAddressOnly = false; // - SK 07/23
        ucNewCenterOrCompany.CenterName = txtSearchLocShoppingCenter.Text.Trim() == "SHOPPING CENTER.." ? "" : txtSearchLocShoppingCenter.Text; // SK : 12/13/2013
        ucNewCenterOrCompany.ResetControls();
        //ucNewCenterOrCompany.ActName =txtSearchLocShoppingCenter.Text;
        hdnIsShoppingCenterAddressSaved.Value = "0";
        ((UpdatePanel)ucNewCenterOrCompany.FindControl("upAddNewShoppingCenterOrCompany")).Update();
        IsNextClicked = false; // ------ SK : 08/07
        mpeAddNewComapny.Show();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "btnAddNewCenter_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void btnAddNewShoppingCenterAddress_Click(object sender, EventArgs e)
    {
        try
        { 
        lbTitle.Text = "ADD NEW ADDRESS";
        ucNewCenterOrCompany.Name = "CENTER";
        ucNewCenterOrCompany.ClassRef = "SC";
        ucNewCenterOrCompany.PopupType = "SC";
        ucNewCenterOrCompany.CenterName = "";  // txtSearchLocShoppingCenter.Text;
        ucNewCenterOrCompany.ResetControls();
        ucNewCenterOrCompany.IsAddressOnly = true;
        hdnIsShoppingCenterAddressSaved.Value = "0";
        //ucNewCenterOrCompany.ActName = txtCTAddress1.Text;
        ((UpdatePanel)ucNewCenterOrCompany.FindControl("upAddNewShoppingCenterOrCompany")).Update();
        ucNewCenterOrCompany.ResetControls();
        IsNextClicked = false; // ------ SK : 08/07
        mpeAddNewComapny.Show();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "locpopup", "toggleLocation('1');", true);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "ValidateForSaveAsDraft", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }


    protected void btnAddNewComapnyAddress_Click(object sender, EventArgs e)
    {
        try
        {        
        lbTitle.Text = "ADD NEW COMPANY";
        ucNewCenterOrCompany.Name = "COMPANY";
        ucNewCenterOrCompany.ClassRef = "C";
        ucNewCenterOrCompany.PopupType = "C";
        ucNewCenterOrCompany.CenterName = "";
        ucNewCenterOrCompany.ResetControls();
        ucNewCenterOrCompany.IsAddressOnly = true;
        //ucNewCenterOrCompany.ActName = txtCTName.Text;
        hdnIsCompanySaved.Value = "0";
        ((UpdatePanel)ucNewCenterOrCompany.FindControl("upAddNewShoppingCenterOrCompany")).Update();
        ucNewCenterOrCompany.ResetControls();
        IsNextClicked = false; // ------ SK : 08/07
        mpeAddNewComapny.Show();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "locpopup", "toggleLocation('1');", true);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "btnAddNewComapnyAddress_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    #endregion

    #region TextBox Events

    protected void txtSearchLocShoppingCenter_TextChanged(object sender, EventArgs e)
    {
        try
        { 
        IsTextChanged_ShoppingCenter = true;
        if (hdnLocId.Value != "" && hdnisAddresOrShopping.Value.Trim() == "1")
        {
            if (txtSearchLocShoppingCenter.Text != "")
            {
                //   EnableLocationAdd();
                SetLocationAddress();
                txtLocAddress1.Text = "";
                txtLocAddress2.Text = "";
            }
            else
            {
                txtLocAddress1.Text = "";
                txtLocAddress2.Text = "";
                txtLocCity.Text = "";
                ddlLocState.SelectedIndex = 0;
                txtLocZipCode.Text = "";
                hdnLocId.Value = "";
                //  btnAddNewCenter_Click(sender, e);
            }
            rdbLocation.SelectedIndex = rdbLocation.Items.IndexOf(rdbLocation.Items.FindByValue("0"));
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Locationshopcenter", "return toggleLocation('1');", true);
        this.Focus();
        txtSearchLocShoppingCenter.Focus();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "txtSearchLocShoppingCenter_TextChanged", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void txtLocAddress1_TextChanged(object sender, EventArgs e)
    {
        try
        { 
        IsTextChanged_ShoppingCenter = true;
        txtSearchLocShoppingCenter.Text = "";
        if (hdnLocId.Value != "" && hdnisAddresOrShopping.Value.Trim() != "1")
        {

            if (txtSearchLocShoppingCenter.Text == "" && txtLocAddress1.Text != "")
            {
                //   EnableLocationAdd();
                if (txtLocAddress1.Text != "")
                {
                    txtSearchLocShoppingCenter.Text = ""; 
                }
                SetLocationAddress();
                hdnisAddresOrShopping.Value = "1";
                txtSearchLocShoppingCenter.Text = "";
            }
            else
            {
                txtLocAddress1.Text = "";
                txtLocAddress2.Text = "";
                txtLocCity.Text = "";
                ddlLocState.SelectedIndex = 0;
                txtLocZipCode.Text = "";
                hdnLocId.Value = "";
                //  btnAddNewCenter_Click(sender, e);
            }
            rdbLocation.SelectedIndex = rdbLocation.Items.IndexOf(rdbLocation.Items.FindByValue("1"));

        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "loceditAddress", "toggleLocation('1');", true);
        txtLocAddress1.Focus();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "txtLocAddress1_TextChanged", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    protected void txtComapny_TextChanged(object sender, EventArgs e)
    {
        try
        { 
        IsTextChanged_Company = true;

        if (hdnCompId.Value != "") //Condition changed JAY to stop clearing data when new charge to is directly entered 
        {
            if (txtCTName.Text != "" || txtCTName.Text != "LANDLORD TRADE NAME..")//txtComapny
            {
                SetCompanyAddress();
            }
            else
            {
                txtCTName.Text = ""; // landlord name
                txtCTEmail.Text = "";
                txtCTAddress1.Text = "";
                txtCTAddress2.Text = "";
                txtCTCity.Text = "";
                ddlCTState.SelectedIndex = 0;
                txtCTZipCode.Text = "";
                hdnCompId.Value = "";
                //  btnAddNewComapny_Click(sender, e);
            }
        }
        else
        {
            hdnIsCompanySaved.Value = "0";
        }
        txtCTName.Focus();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "txtComapny_TextChanged", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
    }

    public void CloseModalAddShoppingCenter()
    {
        mpeAddNewShoppingCenter.Hide();
    }

    #endregion

    #region Web Methods
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] SearchGetWhatList1(string prefixText, int count)
    {
        List<string> lstSearchList = new List<string>();
        try
        {
            Locations objLocations = new Locations();
            objLocations.ShoppingCenter = prefixText;
            DataSet ds = objLocations.GetLocationSeachList();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dt in ds.Tables[0].Rows)
                {
                    //dt["ValueType"].ToString()
                    lstSearchList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dt["ShoppingCenter"].ToString(), dt["LocationID"].ToString()));
                    // lstSearchList.Add(dt[1].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "SearchGetWhatList1", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
        return lstSearchList.ToArray();
    }


    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] SearchGetAddrList(string prefixText, int count)
    {
        List<string> lstSearchList = new List<string>();
        try
        {
            Locations objLocations = new Locations();
            objLocations.ShoppingCenter = prefixText;
            objLocations.IsAddressSearch = true;
            DataSet ds = objLocations.GetLocationSeachList();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dt in ds.Tables[0].Rows)
                {
                    //dt["ValueType"].ToString()
                    lstSearchList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dt["Address1"].ToString(), dt["LocationID"].ToString()));
                    // lstSearchList.Add(dt[1].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "SearchGetWhatList1", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
        return lstSearchList.ToArray();
    }



    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] SearchGetComapnyList(string prefixText, int count)
    {
        List<string> lstSearchList = new List<string>();
        try
        {
            Company objCompany = new Company();
            objCompany.CompanyName = prefixText;
            DataSet ds = objCompany.GetLocationSeachList();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dt in ds.Tables[0].Rows)
                {
                    //dt["ValueType"].ToString()
                    lstSearchList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dt["Company"].ToString(), dt["CompanyId"].ToString()));
                    // lstSearchList.Add(dt[1].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "SearchGetComapnyList", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
        return lstSearchList.ToArray();
    }



    //[System.Web.Script.Services.ScriptMethod()]
    //[System.Web.Services.WebMethod]
    //public static string[] SearchGetTenantList(string prefixText, int count)
    //{
    //    List<string> lstSearchList = new List<string>();
    //    try
    //    {
    //        Tenant objTenant = new Tenant();
    //        objTenant.TenantName = prefixText;
    //        DataSet ds = objTenant.GetTenantSeachList();
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            foreach (DataRow dt in ds.Tables[0].Rows)
    //            {
    //                //dt["ValueType"].ToString()
    //                lstSearchList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dt["TenantName"].ToString(), dt["TenantID"].ToString()));
    //                // lstSearchList.Add(dt[1].ToString());
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //    return lstSearchList.ToArray();
    //}


    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] SearchGetBuyer(string prefixText, int count)
    {
        List<string> lstSearchList = new List<string>();
        try
        {
            Buyer objBuyer = new Buyer();
            objBuyer.BuyerName = prefixText;
            DataSet ds = objBuyer.GetBuyerList();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dt in ds.Tables[0].Rows)
                {
                    lstSearchList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dt["BuyerName"].ToString(), dt["BuyerID"].ToString()));
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "SearchGetBuyer", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
        return lstSearchList.ToArray();
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] SearchGetPartyComapanyList(string prefixText, int count)
    {
        List<string> lstSearchList = new List<string>();
        try
        {
            DealParties objDealParties = new DealParties();
            objDealParties.PartyCompanyName = prefixText;
            DataSet ds = objDealParties.GetDealPartiesCompanyList();

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dt in ds.Tables[0].Rows)
                {
                    //dt["ValueType"].ToString()
                    lstSearchList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dt["PartyCompanyName"].ToString(), "1"));
                    // lstSearchList.Add(dt[1].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "SearchGetPartyComapanyList", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
        return lstSearchList.ToArray();
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] SearchGetDealPartiesList(string prefixText, int count)
    {
        List<string> lstSearchList = new List<string>();
        try
        {
            DealParties objDealParties = new DealParties();
            objDealParties.PartyName = prefixText;

            DataSet ds = objDealParties.GetDealPartiesList();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dt in ds.Tables[0].Rows)
                {
                    //dt["ValueType"].ToString()
                    lstSearchList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dt["PartyName"].ToString(), dt["PartyID"].ToString()));
                    // lstSearchList.Add(dt[1].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "SearchGetDealPartiesList", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
        return lstSearchList.ToArray();
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SetCommissionDatatb(string RowIndx)
    {
        try
        { 
        int RowIndex = Convert.ToInt32(RowIndx) + 1;
        DataTable dtDueDtComm = (DataTable)HttpContext.Current.Session[GlobleData.CommissionTb];
        if (dtDueDtComm.Rows.Count == 1)
        {
            dtDueDtComm.Rows[0].Delete();
        }
        else
        {
            dtDueDtComm.Rows[RowIndex].Delete();
        }
        dtDueDtComm.AcceptChanges();
        HttpContext.Current.Session[GlobleData.CommissionTb] = dtDueDtComm;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "SetCommissionDatatb", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }
        //return "Hello " + name + Environment.NewLine + "The Current Time is: "
        //    + DateTime.Now.ToString();
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SetCommissionDate(string Index, string Date)
    {
        try
        { 
        int RowIndex = Convert.ToInt32(Index);
        DataTable dtDueDtComm = (DataTable)HttpContext.Current.Session[GlobleData.CommissionTb];
        if (dtDueDtComm != null)
        {
            if (dtDueDtComm.Rows.Count == 1)
            {
                dtDueDtComm.Rows[RowIndex]["DueDate"] = Date;
            }
            else
            {
                dtDueDtComm.Rows[RowIndex]["DueDate"] = Date;
            }
            dtDueDtComm.AcceptChanges();
            HttpContext.Current.Session[GlobleData.CommissionTb] = dtDueDtComm;

        }
        else
        {
            HttpContext.Current.Response.Redirect("~/Login.aspx");
        }

            //int RowIndex = Convert.ToInt32(RowIndx) + 1;
            //DataTable dtDueDtComm = (DataTable)HttpContext.Current.Session[GlobleData.CommissionTb];
            //DataView view = new DataView();
            //view.Table = dtDueDtComm;
            //view.RowFilter =
            //dtDueDtComm.AcceptChanges();
            //HttpContext.Current.Session[GlobleData.CommissionTb] = dtDueDtComm;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewCharge.aspx", "SetCommissionDate", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }
    /// <summary>
    /// Called when set fee is chenged 
    /// All the due date comm is updated according to the new fee
    /// JAY
    /// </summary>
    /// <param name="saleprice"></param>
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void updateCommisiondata(decimal CommFee)
    {
        try
        {
            DataTable dtDueDtComm = (DataTable)HttpContext.Current.Session[GlobleData.CommissionTb];
            if (dtDueDtComm != null)
            {
                if (dtDueDtComm.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtDueDtComm.Rows)
                    {
                        string strComper = dr["CommPer"].ToString();
                        if (strComper.ToString().Trim() != "")
                        {
                            decimal compr = Convert.ToDecimal(dr["CommPer"].ToString());
                            decimal newAmount = CommFee * compr / 100;
                            dr["CommAmount"] = newAmount.ToString();
                        }
                    }
                }
                // dtDueDtComm.AcceptChanges();
                HttpContext.Current.Session[GlobleData.CommissionTb] = dtDueDtComm;
                //return "Hello " + name + Environment.NewLine + "The Current Time is: "
                //    + DateTime.Now.ToString();
                // SK : 08/12/2013 
            }

        }
        catch (Exception ex)
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.RemoveAll();
            HttpContext.Current.Response.Redirect("~/Login.aspx");
            ErrorLog.WriteLog("NewCharge.aspx", "updateCommisiondata", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }


    #endregion


    ////protected void rdbLocation_SelectedIndexChanged(object sender, EventArgs e)
    ////{
    ////    if (rdbLocation.SelectedItem.Value = "1")
    ////    {
    ////    }
    ////    else
    ////    {

    ////    }
    ////}
    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        Response.Redirect("NewCharge.aspx");
        hdnPageLeave.Value = "0";
    }
    protected void disableChargeslipDetails()
    {
        txtDealName.Enabled = false;
        txtSearchLocShoppingCenter.Enabled = false;
        txtComapny.Enabled = false;
        txtCTOwnershipEntity.Enabled = false;
        txtCTName.Enabled = false;
        txtCTEmail.Enabled = false;
        ddlDealType.Enabled = false;
        ddlDealSubType.Enabled = false;
        txtLocZipCode.Enabled = false;
        txtLocAddress1.Enabled = false;
        txtLocAddress2.Enabled = false;
        txtLocCity.Enabled = false;
        txtLocCounty.Enabled = false;
        ddlLocState.Enabled = false;
        txtChargeDate.Enabled = false;
        txtCTAddress1.Enabled = false;
        txtCTAddress2.Enabled = false;
        txtCTCity.Enabled = false;
        txtCTZipCode.Enabled = false;
        ddlCTState.Enabled = false;
        txtlocSHPaddress1.Enabled = false;
        txtMapLocation.Enabled = false;

    }
}