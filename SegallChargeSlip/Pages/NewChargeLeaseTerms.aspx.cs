﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Reflection;
using SegallChargeSlipBll;
using SegallChargeSlipBLL;

public partial class Pages_NewChargeLeaseTerms : System.Web.UI.Page
{
    const string cns_ucBaseLeaseTerms = "ucBaseLeaseTerms";//used in javascript function addDueDateRecordsEditMode
    const string cns_ucOptionalTerms = "ucOptionalTerms";
    const string cns_ucContingentTerms = "ucContingentCommission";

    const string cns_ucOptional1Terms = "ucOptional1Terms";
    const string cns_ucOptional2Terms = "ucOptional2Terms";
    const string cns_ucOptional3Terms = "ucOptional3Terms";


    // DataTable dtBaseLease, dtBaseCommLease, dtOptTerm, dtOptCommTerm, dtContingentTerm, dtContingentComm, tempdt;


    DataTable dtSaveTerms, dtSaveCommTerms, dtTempTerms, dtTempCommTerms;
    DataRow dtNewRow;


    protected void Page_Load(object sender, EventArgs e)
    {

        // ------- 08/12/2013 : SK
        //if (Session["UserId"] == null)
        //{
        //    //Response.Redirect("~/adlogin.aspx");
        //    Session.Clear();
        //    Session.RemoveAll();
        //    Server.Transfer("Login.aspx");
        //    //Response.Redirect("Login.aspx");
        //}




        // On Page Load Option term are disable and check box are not selected
        if (!IsPostBack)
        {
            try
            {
                //User Control For Lease Term
                ucBaseLeaseTerms.userCntr = Convert.ToString(cns_ucBaseLeaseTerms);
                ucBaseLeaseTerms.CreateBasicLeaseTermTable();
                //  ucBaseLeaseTerms.userCntr = Convert.ToString(cns_ucBaseLeaseTerms);
                ucBaseLeaseTerms.CreateCommissionableTermTable();
                ucBaseLeaseTerms.SetTitles("BASE LEASE TERM", "BASE COMMISSIONABLE TERM");

                //User Control For Commissionable Term
                ucOptionalTerms.userCntr = Convert.ToString(cns_ucOptionalTerms);
                ucOptionalTerms.CreateBasicLeaseTermTable();
                //ucOptionalTerms.userCntr = Convert.ToString(cns_ucOptionalTerms);
                ucOptionalTerms.CreateCommissionableTermTable();
                ucOptionalTerms.SetTitles("OPTION LEASE TERM 1", "OPTION COMMISSIONABLE TERM 1");

                //User Control For Contingent Term
                ucContingentCommission.userCntr = Convert.ToString(cns_ucContingentTerms);
                ucContingentCommission.CreateBasicLeaseTermTable();
                // ucContingentCommission.userCntr = Convert.ToString(cns_ucContingentTerms);
                ucContingentCommission.CreateCommissionableTermTable();
                ucContingentCommission.SetTitles("CONTINGENT TERM", "CONTINGENT COMMISSIONABLE TERM");

                //User Control For Commissionable Term 1
                ucOptional1Terms.userCntr = Convert.ToString(cns_ucOptional1Terms);
                ucOptional1Terms.CreateBasicLeaseTermTable();
                //ucOptional1Terms.userCntr = Convert.ToString(cns_ucOptional1Terms);
                ucOptional1Terms.CreateCommissionableTermTable();
                ucOptional1Terms.SetTitles("OPTION LEASE TERM 2", "OPTION COMMISSIONABLE TERM 2");

                //User Control For Commissionable Term 2
                ucOptional2Terms.userCntr = Convert.ToString(cns_ucOptional2Terms);
                ucOptional2Terms.CreateBasicLeaseTermTable();
                //ucOptional2Terms.userCntr = Convert.ToString(cns_ucOptional2Terms);
                ucOptional2Terms.CreateCommissionableTermTable();
                ucOptional2Terms.SetTitles("OPTION LEASE TERM 3", "OPTION COMMISSIONABLE TERM 3");

                //User Control For Commissionable Term 3
                ucOptional3Terms.userCntr = Convert.ToString(cns_ucOptional3Terms);
                ucOptional3Terms.CreateBasicLeaseTermTable();
                //ucOptional3Terms.userCntr = Convert.ToString(cns_ucOptional3Terms);
                ucOptional3Terms.CreateCommissionableTermTable();
                ucOptional3Terms.SetTitles("OPTION LEASE TERM 4", "OPTION COMMISSIONABLE TERM 4");

                if (Session[GlobleData.NewChargeSlipId] != null)
                {

                    int CSID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                    if (CSID > 0)
                    {
                        //Test For Edit
                        //set status
                        ChargeSlip objChargeSlip = new ChargeSlip();
                        objChargeSlip.ChargeSlipID = CSID;
                        objChargeSlip.Load();

                        if (GlobleData.cns_Status_Complete == objChargeSlip.Status)
                        {
                            Session[GlobleData.cns_Is_Complete] = true;
                            btnSaveForLater.Visible = false;
                        }
                        ucBaseLeaseTerms.GetLeaseTermRecordsForSelChargeSlip(ucBaseLeaseTerms.ClientID);
                        ucOptionalTerms.GetLeaseTermRecordsForSelChargeSlip(ucOptionalTerms.ClientID);
                        ucContingentCommission.GetLeaseTermRecordsForSelChargeSlip(ucContingentCommission.ClientID);

                        ucOptional1Terms.GetLeaseTermRecordsForSelChargeSlip(ucOptional1Terms.ClientID);
                        ucOptional2Terms.GetLeaseTermRecordsForSelChargeSlip(ucOptional2Terms.ClientID);
                        ucOptional3Terms.GetLeaseTermRecordsForSelChargeSlip(ucOptional3Terms.ClientID);

                    }
                }
           
            }
            catch (Exception ex)
            {
                ErrorLog.WriteLog("NewChargeLeaseTerms.aspx", "Page_Load", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
                //throw;
            }
        }
    }
    //public void UpdateContANdOptnGrid()
    //{
    //    ucOptionalTerms.GetLeaseTermRecordsForSelChargeSlip(ucOptionalTerms.ClientID);
    //    ucContingentCommission.GetLeaseTermRecordsForSelChargeSlip(ucContingentCommission.ClientID);
    //}

    protected void btnBack_Click(object sender, EventArgs e)
    {
        string val = hdnSaveStatus.Value;
        Server.Transfer("NewCharge.aspx");
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            //ucBaseLeaseTerms.ValidateForDueDates(ucBaseLeaseTerms.ClientID);
            if (hdnSaveStatus.Value != "")
            {

                Boolean ChkOpt = chkOptionTermPayable.Checked;
                Boolean ChkOpt1 = chkOptionTermPayable1.Checked;
                Boolean ChkOpt2 = chkOptionTermPayable2.Checked;
                Boolean ChkOpt3 = chkOptionTermPayable3.Checked;

                ucBaseLeaseTerms.Savechanges();
                int BaseTermCommissionType = 0, OptionTermCommissionType = 0, ContingentCommissionType = 0;
                int OptionTermCommissionType1 = 0, OptionTermCommissionType2 = 0, OptionTermCommissionType3 = 0;
                BaseTermCommissionType = Session["CommissionTypeBaseTerm"].ToString() == "" ? 0 : Convert.ToInt32(Session["CommissionTypeBaseTerm"].ToString());
                ucBaseLeaseTerms.SaveDueDates(ChkOpt);
                if (ChkOpt)
                {
                    OptionTermCommissionType = Session["CommissionTypeOptionTerm"].ToString() == "" ? 0 : Convert.ToInt32(Session["CommissionTypeOptionTerm"].ToString());
                    ucOptionalTerms.Savechanges();
                }
                if (chkContingentComm.Checked)
                {
                    ContingentCommissionType = Session["CommissionTypeContingent"].ToString() == "" ? 0 : Convert.ToInt32(Session["CommissionTypeContingent"].ToString());
                    ucContingentCommission.Savechanges();
                }

                ucBaseLeaseTerms.SaveDueDates(ChkOpt1);
                if (ChkOpt1)
                {                    
                    OptionTermCommissionType1 = Session["CommissionTypeOptionTerm1"].ToString() == "" ? 0 : Convert.ToInt32(Session["CommissionTypeOptionTerm1"].ToString());
                    ucOptional1Terms.Savechanges();
                }
                ucBaseLeaseTerms.SaveDueDates(ChkOpt2);
                if (ChkOpt2)
                {
                    OptionTermCommissionType2 = Session["CommissionTypeOptionTerm2"].ToString() == "" ? 0 : Convert.ToInt32(Session["CommissionTypeOptionTerm2"].ToString());
                    ucOptional2Terms.Savechanges();
                }
                ucBaseLeaseTerms.SaveDueDates(ChkOpt3);
                if (ChkOpt3)
                {
                    OptionTermCommissionType3 = Session["CommissionTypeOptionTerm3"].ToString() == "" ? 0 : Convert.ToInt32(Session["CommissionTypeOptionTerm3"].ToString());
                    ucOptional3Terms.Savechanges();
                }

                DealTransactions objdealtransaction = new DealTransactions();
                objdealtransaction.CommissionTypeUpdate(Convert.ToInt32(Session[GlobleData.NewChargeSlipId]), ContingentCommissionType, OptionTermCommissionType, BaseTermCommissionType, OptionTermCommissionType1, OptionTermCommissionType2, OptionTermCommissionType3);
                ucBaseLeaseTerms.UpdateDataTableForTermPerid(ucBaseLeaseTerms.ClientID);
                ucOptionalTerms.UpdateDataTableForTermPerid(ucOptionalTerms.ClientID);
                ucContingentCommission.UpdateDataTableForTermPerid(ucContingentCommission.ClientID);

                ucOptional1Terms.UpdateDataTableForTermPerid(ucOptional1Terms.ClientID);
                ucOptional2Terms.UpdateDataTableForTermPerid(ucOptional2Terms.ClientID);
                ucOptional3Terms.UpdateDataTableForTermPerid(ucOptional3Terms.ClientID);

                LeaseTerms objLeaseTerms = new LeaseTerms();
                CommissionableTerms objCommissionableTerms = new CommissionableTerms();

                CreateBasicLeaseTermTable();
                CreateCommissionableTermTable();

                //Save For BASE LEASE TERM
                if (Session[GlobleData.cns_dtBasicLeaseTerm + cns_ucBaseLeaseTerms] != null)
                {
                    dtTempTerms = (DataTable)Session[GlobleData.cns_dtBasicLeaseTerm + cns_ucBaseLeaseTerms];
                    if (dtTempTerms.Rows.Count > 0)
                    {
                        FillLeaseTermTable(dtTempTerms, 1);
                    }
                }

                //Save For BASE COMMISSIONABLE TERM
                if (Session[GlobleData.cns_dtCommTerm + cns_ucBaseLeaseTerms] != null)
                {
                    dtTempCommTerms = (DataTable)Session[GlobleData.cns_dtCommTerm + cns_ucBaseLeaseTerms];
                    if (dtTempCommTerms.Rows.Count > 0)
                    {
                        FillCommissionableTermTable(dtTempCommTerms, 1);
                    }
                }

                //Save For OPTION(S) TERM PAYABLE? 
                if (Session[GlobleData.cns_dtBasicLeaseTerm + cns_ucOptionalTerms] != null)
                {
                    dtTempTerms = (DataTable)Session[GlobleData.cns_dtBasicLeaseTerm + cns_ucOptionalTerms];
                    if (dtTempTerms.Rows.Count > 0)
                    {
                        FillLeaseTermTable(dtTempTerms, 2);
                    }

                }

                //Save For OPTION(S) TERM PAYABLE? COMMISSIONABLE
                if (Session[GlobleData.cns_dtCommTerm + cns_ucOptionalTerms] != null && chkOptionTermPayable.Checked == true)
                {
                    dtTempCommTerms = (DataTable)Session[GlobleData.cns_dtCommTerm + cns_ucOptionalTerms];
                    if (dtTempCommTerms.Rows.Count > 0)
                    {
                        FillCommissionableTermTable(dtTempCommTerms, 2);
                    }
                }                

                //Save For CONTINGENT COMMISSION?
                if (Session[GlobleData.cns_dtBasicLeaseTerm + cns_ucContingentTerms] != null)
                {
                    dtTempTerms = (DataTable)Session[GlobleData.cns_dtBasicLeaseTerm + cns_ucContingentTerms];
                    if (dtTempTerms.Rows.Count > 0)
                    {
                        FillLeaseTermTable(dtTempTerms, 3);
                    }
                }

                //Save For CONTINGENT COMMISSION? COMMISSIONABLE
                if (Session[GlobleData.cns_dtCommTerm + cns_ucContingentTerms] != null)
                {
                    dtTempCommTerms = (DataTable)Session[GlobleData.cns_dtCommTerm + cns_ucContingentTerms];
                    if (dtTempCommTerms.Rows.Count > 0)
                    {
                        FillCommissionableTermTable(dtTempCommTerms, 3);
                    }
                }

                //Save For OPTION(S) TERM PAYABLE? 1
                if (Session[GlobleData.cns_dtBasicLeaseTerm + cns_ucOptional1Terms] != null)
                {
                    dtTempTerms = (DataTable)Session[GlobleData.cns_dtBasicLeaseTerm + cns_ucOptional1Terms];
                    if (dtTempTerms.Rows.Count > 0)
                    {
                        FillLeaseTermTable(dtTempTerms, 4);
                    }
                }

                //Save For OPTION(S) TERM PAYABLE? COMMISSIONABLE 1
                if (Session[GlobleData.cns_dtCommTerm + cns_ucOptional1Terms] != null && chkOptionTermPayable1.Checked == true)
                {
                    dtTempCommTerms = (DataTable)Session[GlobleData.cns_dtCommTerm + cns_ucOptional1Terms];
                    if (dtTempCommTerms.Rows.Count > 0)
                    {
                        FillCommissionableTermTable(dtTempCommTerms, 4);
                    }
                }

                //Save For OPTION(S) TERM PAYABLE? 2
                if (Session[GlobleData.cns_dtBasicLeaseTerm + cns_ucOptional2Terms] != null)
                {
                    dtTempTerms = (DataTable)Session[GlobleData.cns_dtBasicLeaseTerm + cns_ucOptional2Terms];
                    if (dtTempTerms.Rows.Count > 0)
                    {
                        FillLeaseTermTable(dtTempTerms, 5);
                    }

                }

                //Save For OPTION(S) TERM PAYABLE? COMMISSIONABLE 2
                if (Session[GlobleData.cns_dtCommTerm + cns_ucOptional2Terms] != null && chkOptionTermPayable2.Checked == true)
                {
                    dtTempCommTerms = (DataTable)Session[GlobleData.cns_dtCommTerm + cns_ucOptional2Terms];
                    if (dtTempCommTerms.Rows.Count > 0)
                    {
                        FillCommissionableTermTable(dtTempCommTerms, 5);
                    }
                }

                //Save For OPTION(S) TERM PAYABLE? 3
                if (Session[GlobleData.cns_dtBasicLeaseTerm + cns_ucOptional3Terms] != null)
                {
                    dtTempTerms = (DataTable)Session[GlobleData.cns_dtBasicLeaseTerm + cns_ucOptional3Terms];
                    if (dtTempTerms.Rows.Count > 0)
                    {
                        FillLeaseTermTable(dtTempTerms, 6);
                    }

                }

                //Save For OPTION(S) TERM PAYABLE? COMMISSIONABLE 3
                if (Session[GlobleData.cns_dtCommTerm + cns_ucOptional3Terms] != null && chkOptionTermPayable3.Checked == true)
                {
                    dtTempCommTerms = (DataTable)Session[GlobleData.cns_dtCommTerm + cns_ucOptional3Terms];
                    if (dtTempCommTerms.Rows.Count > 0)
                    {
                        FillCommissionableTermTable(dtTempCommTerms, 6);
                    }
                }

                if (Session["dtSaveTerms"] != null)
                {
                    dtSaveTerms = (DataTable)Session["dtSaveTerms"];
                    if (dtSaveTerms.Rows.Count > 0)
                    {
                        Boolean rslt = objLeaseTerms.SaveTb(dtSaveTerms, dtSaveCommTerms);

                        if (hdnSaveStatus.Value == "N")
                        {
                            if (rslt)
                            {
                                ucAttachment.InsertAddNotes();
                                Response.Redirect("NewChargeCommissionSpitCalCulator.aspx", false);
                            }
                        }
                        else if (hdnSaveStatus.Value == "D")
                        {
                            // Utility.ClearSessionExceptUser();

                            int user;
                            string usertype, Name, LoggedInUser;

                            user = Convert.ToInt32(HttpContext.Current.Session[GlobleData.User]);
                            usertype = HttpContext.Current.Session["UserType"].ToString();
                            LoggedInUser = HttpContext.Current.Session["UserName"].ToString();

                            Name = HttpContext.Current.Session["User"].ToString();
                            HttpContext.Current.Session.Clear();
                            //HttpContext.Current.Session.RemoveAll();
                            HttpContext.Current.Session[GlobleData.User] = user;
                            HttpContext.Current.Session["UserType"] = usertype;
                            HttpContext.Current.Session["User"] = Name;
                            HttpContext.Current.Session["UserName"] = LoggedInUser;

                            Response.Redirect("AddOrEditNewChargeSlip.aspx", false);
                        }
                        else if ((hdnSaveStatus.Value == "B")) // marker : 12/06/2013 : Added to save data on Back request 
                        {
                            Response.Redirect("NewCharge.aspx", false);
                        }

                    }
                    else
                    {
                        if (hdnSaveStatus.Value == "D")
                        {
                            int user;
                            string usertype, Name, LoggedInUser;

                            int CSID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                            // LeaseTerms objLeaseTerms = new LeaseTerms();
                            objLeaseTerms.LeaseAndCommTermsDelete(CSID);

                            user = Convert.ToInt32(HttpContext.Current.Session[GlobleData.User]);
                            usertype = HttpContext.Current.Session["UserType"].ToString();
                            LoggedInUser = HttpContext.Current.Session["UserName"].ToString();

                            Name = HttpContext.Current.Session["User"].ToString();
                            HttpContext.Current.Session.Clear();
                            //HttpContext.Current.Session.RemoveAll();
                            HttpContext.Current.Session[GlobleData.User] = user;
                            HttpContext.Current.Session["UserType"] = usertype;
                            HttpContext.Current.Session["User"] = Name;
                            HttpContext.Current.Session["UserName"] = LoggedInUser;

                            Response.Redirect("AddOrEditNewChargeSlip.aspx", false);
                        }
                        else if ((hdnSaveStatus.Value == "B")) // marker : 12/06/2013 : Added to save data on Back request 
                        {

                            int CSID = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                            // LeaseTerms objLeaseTerms = new LeaseTerms();
                            objLeaseTerms.LeaseAndCommTermsDelete(CSID);
                            ucAttachment.InsertAddNotes();
                            Response.Redirect("NewCharge.aspx", false);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "display", "ValidateOnNext();", true);
                        }
                    }
                }

                #region save Notification days
                SaveNotificationDays();
                #endregion

            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeLeaseTerms.aspx", "btnNext_Click", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
        //else
        //{
        //    ucBaseLeaseTerms.ReCreateDueDatesOnPostBack(ucBaseLeaseTerms.ClientID);
        //    ucOptionalTerms.ReCreateDueDatesOnPostBack(ucBaseLeaseTerms.ClientID);
        //    ucContingentCommission.ReCreateDueDatesOnPostBack(ucBaseLeaseTerms.ClientID);
        //}
    }


    public void CreateBasicLeaseTermTable()
    {
        dtSaveTerms = new DataTable();
        dtSaveTerms.Columns.Clear();
        dtSaveTerms.Columns.Add("ChargeSlipId", typeof(int));
        dtSaveTerms.Columns.Add("DealTransactionId", typeof(int));
        dtSaveTerms.Columns.Add("TermTypeId", typeof(int));
        dtSaveTerms.Columns.Add("RentPeriodFrom", typeof(int));
        dtSaveTerms.Columns.Add("RentPeriodTo", typeof(int));
        dtSaveTerms.Columns.Add("TotalMonths", typeof(decimal));
        dtSaveTerms.Columns.Add("RentPerSqFt", typeof(decimal));
        dtSaveTerms.Columns.Add("AnnualRent", typeof(decimal));
        dtSaveTerms.Columns.Add("PercentIncrease", typeof(decimal));
        dtSaveTerms.Columns.Add("PeriodRent", typeof(decimal));
        dtSaveTerms.Columns.Add("TotalSqFt", typeof(decimal));
        dtSaveTerms.Columns.Add("TermPeriod", typeof(int));
        // dtSaveTerms.Columns.Add("LeaseTermId", typeof(int));
        Session["dtSaveTerms"] = dtSaveTerms;
    }

    public void CreateCommissionableTermTable()
    {
        dtSaveCommTerms = new DataTable();
        dtSaveCommTerms.Columns.Clear();
        dtSaveCommTerms.Columns.Add("ChargeSlipId", typeof(int));
        dtSaveCommTerms.Columns.Add("DealTransactionId", typeof(int));
        dtSaveCommTerms.Columns.Add("TermTypeId", typeof(int));
        dtSaveCommTerms.Columns.Add("RentPeriodFrom", typeof(int));
        dtSaveCommTerms.Columns.Add("RentPeriodTo", typeof(int));
        dtSaveCommTerms.Columns.Add("TotalMonths", typeof(int));
        dtSaveCommTerms.Columns.Add("PercentBrokerComm", typeof(float));
        dtSaveCommTerms.Columns.Add("Fee", typeof(decimal));
        dtSaveCommTerms.Columns.Add("CommTermPeriod", typeof(int));
        // dtSaveCommTerms.Columns.Add("CommissionableTermId", typeof(int));
        Session["dtSaveCommTerms"] = dtSaveCommTerms;
    }

    private void FillLeaseTermTable(DataTable dtTempTerms, int TermType)
    {
        try
        { 
        dtSaveTerms = (DataTable)Session["dtSaveTerms"];
        if (dtSaveTerms != null)
        {
            foreach (DataRow dRow in dtTempTerms.Rows)
            {
                dtNewRow = dtSaveTerms.NewRow();
                dtNewRow["ChargeSlipId"] = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                dtNewRow["DealTransactionId"] = Convert.ToInt32(Session[GlobleData.DealTransactionId]);
                dtNewRow["TermTypeId"] = TermType;
                dtNewRow["RentPeriodFrom"] = Convert.ToInt32(dRow["RentFromPeriod"].ToString());
                dtNewRow["RentPeriodTo"] = Convert.ToInt32(dRow["RentToPeriod"].ToString());
                dtNewRow["TotalMonths"] = Convert.ToInt32(dRow["TotalMonths"].ToString());
                dtNewRow["RentPerSqFt"] = Convert.ToDecimal(dRow["Rent_PSF"].ToString());
                dtNewRow["AnnualRent"] = Convert.ToDecimal(dRow["AnnualRent"].ToString());
                dtNewRow["PercentIncrease"] = Convert.ToDecimal(dRow["PerIncrease"].ToString());
                dtNewRow["PeriodRent"] = Convert.ToDecimal(dRow["PeriodRent"].ToString());
                dtNewRow["TotalSqFt"] = Convert.ToDecimal(dRow["TotalSF"].ToString());
                dtNewRow["TermPeriod"] = Convert.ToInt32(dRow["TermPeriod"].ToString());
                //    dtNewRow["LeaseTermId"] = Convert.ToInt32(dRow["LeaseTermId"].ToString());
                dtSaveTerms.Rows.Add(dtNewRow);
            }
            dtTempTerms.Rows.Clear();
            Session["dtSaveTerms"] = dtSaveTerms;
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeLeaseTerms.aspx", "FillLeaseTermTable", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }

    private void FillCommissionableTermTable(DataTable dtTempCommTerms, int TermType)
    {
        try
        { 
        decimal LeaseCommTot, OptCommTot, ContingentCommTot;
        decimal OptCommTot1, OptCommTot2, OptCommTot3;
        LeaseCommTot = 0;
        OptCommTot = 0;
        ContingentCommTot = 0;
        OptCommTot1 = 0;
        OptCommTot2 = 0;
        OptCommTot3 = 0;

        dtSaveCommTerms = (DataTable)Session["dtSaveCommTerms"];
        if (dtSaveCommTerms != null)
        {
            foreach (DataRow dRow in dtTempCommTerms.Rows)
            {
                dtNewRow = dtSaveCommTerms.NewRow();
                dtNewRow["ChargeSlipId"] = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                dtNewRow["DealTransactionId"] = Convert.ToInt32(Session[GlobleData.DealTransactionId]);
                dtNewRow["TermTypeId"] = TermType;
                dtNewRow["RentPeriodFrom"] = Convert.ToInt32(dRow["RentFromPeriod"].ToString());
                dtNewRow["RentPeriodTo"] = Convert.ToInt32(dRow["RentToPeriod"].ToString());
                dtNewRow["TotalMonths"] = Convert.ToInt32(dRow["TotalMonths"].ToString());
                dtNewRow["PercentBrokerComm"] = Convert.ToDecimal(dRow["BrokerCommission"].ToString());
                dtNewRow["Fee"] = Convert.ToDecimal(dRow["Fee"].ToString());
                dtNewRow["CommTermPeriod"] = Convert.ToInt32(dRow["CommTermPeriod"].ToString());
                //  dtNewRow["CommissionableTermId"] = Convert.ToDecimal(dRow["CommissionableTermId"].ToString());
                dtSaveCommTerms.Rows.Add(dtNewRow);

                if (TermType == 1)
                {
                    LeaseCommTot = LeaseCommTot + Convert.ToDecimal(dRow["Fee"].ToString()); ;
                }
                else if (TermType == 2)
                {
                    OptCommTot = OptCommTot + Convert.ToDecimal(dRow["Fee"].ToString()); ;
                }
                else if (TermType == 3)
                {
                    ContingentCommTot = ContingentCommTot + Convert.ToDecimal(dRow["Fee"].ToString()); ;
                }

                else if (TermType == 4)
                {
                    OptCommTot1 = OptCommTot1 + Convert.ToDecimal(dRow["Fee"].ToString()); ;
                }
                else if (TermType == 5)
                {
                    OptCommTot2 = OptCommTot2 + Convert.ToDecimal(dRow["Fee"].ToString()); ;
                }
                else if (TermType == 6)
                {
                    OptCommTot3 = OptCommTot3 + Convert.ToDecimal(dRow["Fee"].ToString()); ;
                }
            }
            Session[GlobleData.cns_totBaseLeaseComm] = LeaseCommTot == 0 && (Session[GlobleData.cns_totBaseLeaseComm] != null) ? Convert.ToDecimal(Session[GlobleData.cns_totBaseLeaseComm]) : LeaseCommTot;
            Session[GlobleData.cns_totOptComm] = OptCommTot == 0 && (Session[GlobleData.cns_totOptComm] != null) ? Session[GlobleData.cns_totOptComm] : OptCommTot;
            Session[GlobleData.cns_totContingentComm] = ContingentCommTot;

            Session[GlobleData.cns_totOptComm1] = OptCommTot1 == 0 && (Session[GlobleData.cns_totOptComm1] != null) ? Session[GlobleData.cns_totOptComm1] : OptCommTot1;
            Session[GlobleData.cns_totOptComm2] = OptCommTot2 == 0 && (Session[GlobleData.cns_totOptComm2] != null) ? Session[GlobleData.cns_totOptComm2] : OptCommTot2;
            Session[GlobleData.cns_totOptComm3] = OptCommTot3 == 0 && (Session[GlobleData.cns_totOptComm3] != null) ? Session[GlobleData.cns_totOptComm3] : OptCommTot3;
            dtTempTerms.Rows.Clear();
            Session["dtSaveCommTerms"] = dtSaveCommTerms;
        }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeLeaseTerms.aspx", "FillCommissionableTermTable", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
        }

    }



    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void DeleteDueDateFromSession(string DueDate, string Percentage)
    {
        try
        {
            int i, RowIndex = 0;
            if (HttpContext.Current.Session[GlobleData.CommissionTb] != null)
            {
                DataTable dtDueDtComm = (DataTable)HttpContext.Current.Session[GlobleData.CommissionTb];
                if (dtDueDtComm.Rows.Count > 0)
                {
                    for (i = 0; i < dtDueDtComm.Rows.Count; i++)
                    {
                        if (dtDueDtComm.Rows[i]["DueDate"].ToString() == DueDate && Convert.ToDecimal(dtDueDtComm.Rows[i]["PercentComm"].ToString()) == Convert.ToDecimal(Percentage))
                        {
                            RowIndex = i;
                        }
                    }
                    dtDueDtComm.Rows[RowIndex].Delete();
                    dtDueDtComm.AcceptChanges();
                }

                HttpContext.Current.Session[GlobleData.CommissionTb] = dtDueDtComm;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeLeaseTerms.aspx", "DeleteDueDateFromSession", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }
    }

    private void SaveNotificationDays()
    {
        #region save Notification days
        try
        {
            if (Session[GlobleData.NewChargeSlipId] != null)
            {
                CommissionDueDatesNotification objCommissionDueDatesNotification = new CommissionDueDatesNotification();
                //long ChargeSlipId = 182085;
                objCommissionDueDatesNotification.ChargeSlipId = Convert.ToInt32(Session[GlobleData.NewChargeSlipId]);
                DataSet ds = objCommissionDueDatesNotification.GetCommissionDueDatesByChargeSlip(objCommissionDueDatesNotification.ChargeSlipId);
                long CommissionDueId = 0;
                //string NotifyDays = "15,30,60,100";
                string NotifyDays = Session["NotifyDays"].ToString();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        CommissionDueId = 0;
                        CommissionDueId = Convert.ToInt64(row["CommissionDueId"]);
                        objCommissionDueDatesNotification.CommissionDueId = CommissionDueId;
                        objCommissionDueDatesNotification.DueDate = Convert.ToDateTime(row["DueDate"]);
                        objCommissionDueDatesNotification.Delete(NotifyDays);
                        int[] NotifyDaysArray = NotifyDays.Split(',').Select(x => Convert.ToInt32(x)).ToArray();
                        for (int i = 0; i < NotifyDaysArray.Length; i++)
                        {
                            objCommissionDueDatesNotification.IsNotifyOn = 0;
                            objCommissionDueDatesNotification.IsNotifyOn = NotifyDaysArray[i];
                            objCommissionDueDatesNotification.CommissionDueId = CommissionDueId;
                            objCommissionDueDatesNotification.Save();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("NewChargeLeaseTerms.aspx", "SaveNotificationDays", ex.Message.ToString() + " \n Inner Exception:" + (ex.InnerException != null ? ex.InnerException.ToString() : "") + " \n Stack Trace:" + ex.StackTrace.ToString());
            // throw;
        }

        #endregion
    }
}