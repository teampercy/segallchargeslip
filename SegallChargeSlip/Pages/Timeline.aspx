﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true" CodeFile="Timeline.aspx.cs" Inherits="Pages_Timeline" %>
<%@ Register TagPrefix="UC" TagName="Timeline" Src="~/UserControl/ucTimeline.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td class="mainPageTitle" style="padding-right: 5px;">TIMELINE
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr />
            </td>
        </tr>
    </table>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" runat="Server">
    <UC:Timeline ID="ucTimeline" runat="server" />

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        $(function () {            
            google.charts.load('current', { 'packages': ['corechart'] });
            google.charts.setOnLoadCallback(function () {
                DrawTimelineChart();
            });
        });
    </script>
</asp:Content>


