﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true" CodeFile="Snapshot.aspx.cs" Inherits="Pages_Snapshot" EnableEventValidation="false"%>

<%@ Register Src="~/UserControl/ucSnapshot.ascx" TagName="Snapshot" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <table style="width: 100%;">
        <tr>
            <td></td>
             <td class="mainPageTitle" style="padding-right:5px;">SNAPSHOT</td>
        </tr>
         <tr>
              <td colspan="2">
                <hr />
            </td>
        </tr>
    </table>
<%--    <hr style="width: 99%" />--%>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
   <uc:Snapshot id="ucsnapshot" runat="server" />
</asp:Content>

