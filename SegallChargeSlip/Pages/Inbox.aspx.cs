﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

using SegallChargeSlipBll;


public partial class Pages_Inbox : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        // SK : 08/12/213 ------vv--------------
        //if (Session["UserId"] == null)
        //{
        //    //Response.Redirect("~/adlogin.aspx");
        //    Session.Clear();
        //    Session.RemoveAll();            
        //    Server.Transfer("Login.aspx");
        //    //Response.Redirect("Login.aspx");
        //}



        if (!Page.IsPostBack)
        {
            ViewState["PageSize"] = 27;
            ViewState["PageNo"] = 1;
            if (Session["UserType"] != null)
            {
                if (Session["UserType"].ToString() == "1")
                {
                    lblAdmin.Visible = true;
                }
                else
                {
                    lblAdmin.Visible = false;
                }
            }
            BindMessageList();
        }
        //BindMessageList();
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        GridViewRow grdStud = grdMessage.BottomPagerRow;
        if (grdStud != null)
        {
            grdStud.Visible = true;
        }
    }
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        ViewState["PageNo"] = "1";
        BindMessageList();
    }
    public void BindMessageList()
    {
        try
        {
            Messages obj = new Messages();
            DataSet ds = new DataSet();
            if (Session["UserID"] != null)
            {

                ds = obj.GetPagingList(txtSearch.Text.Trim(), Convert.ToInt32(Session["UserID"].ToString()), Convert.ToInt32(ViewState["PageNo"]), Convert.ToInt32(ViewState["PageSize"]));

                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        double TotalRecord = Convert.ToInt32(ds.Tables[0].Rows[0]["RowCounts"]);
                        ViewState["TotalRecord"] = TotalRecord;
                        double Pagesize = Convert.ToInt32(ViewState["PageSize"]);
                        double TotalPage = Math.Ceiling(TotalRecord / Pagesize);
                        ViewState["TotalPage"] = TotalPage;
                        //if (TotalRecord > 0)
                        //{
                        //    lblTotalRecord.Text = "Total Records: " + TotalRecord;
                        //}
                    }
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        grdMessage.DataSource = ds.Tables[1];
                        grdMessage.DataBind();
                    }
                    else
                    {
                        grdMessage.DataSource = null;
                        grdMessage.DataBind();
                        // lblTotalRecord.Text = string.Empty;
                    }
                }

            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Inbox.aspx", "BindMessageList", ex.Message);
        }
    }
    protected void grdMessage_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            //int PageNo = Convert.ToInt32(ViewState["PageNo"]);
            //PageNo = e.NewPageIndex + 1;
            //ViewState["PageNo"] = PageNo;


            int PageNo = Convert.ToInt32(ViewState["PageNo"]);

            if (e.NewPageIndex == -1)
            {
                PageNo++;
                ViewState["PageNo"] = PageNo;
            }
            else if (e.NewPageIndex == -2)
            {
                PageNo--;
                ViewState["PageNo"] = PageNo;
            }
            else
            {
                PageNo = e.NewPageIndex + 1;
                ViewState["PageNo"] = PageNo;
            }
            BindMessageList();

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Inbox.aspx", "grdMessage_PageIndexChanging", ex.Message);
        }
    }
    protected void grdMessage_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                int pageno = Convert.ToInt32(ViewState["PageNo"]);
                int totalpage = Convert.ToInt32(ViewState["TotalPage"]);
                LinkButton btnPrevious = new LinkButton();
                btnPrevious.ID = "btnPrevious";
                btnPrevious.Text = "<";
                btnPrevious.CommandName = "Page";
                btnPrevious.CommandArgument = "-1";
                btnPrevious.Width = Unit.Pixel(20);
                btnPrevious.CssClass = "removeunderline";

                LinkButton btnNext = new LinkButton();
                btnNext.ID = "btnNext";
                btnNext.Text = ">";
                btnNext.CommandName = "Page";
                btnNext.CssClass = "removeunderline";
                btnNext.CommandArgument = "0";
                e.Row.Cells[0].Controls.Add(btnPrevious);
                for (int i = 1; i <= totalpage; i++)
                {
                    LinkButton lnk = new LinkButton();
                    lnk.ID = "lnk" + i;
                    lnk.Text = i.ToString() + " ";
                    lnk.CommandArgument = i.ToString();
                    lnk.CommandName = "Page";
                    lnk.Width = Unit.Pixel(20);
                    e.Row.Cells[0].Controls.Add(lnk);
                    lnk.CssClass = "removeunderline";
                    if (Convert.ToInt32(ViewState["PageNo"]) == i)
                    {
                        lnk.CssClass = "linkbut";
                    }
                }
                e.Row.Cells[0].Controls.Add(btnNext);
                if (ViewState["TotalRecord"] != null)
                {

                    Label lblTotalRecords = new Label();
                    lblTotalRecords.CssClass = "PageNoCSS";
                    lblTotalRecords.Text = "Total Records:" + ViewState["TotalRecord"].ToString();
                    e.Row.Cells[0].Controls.Add(lblTotalRecords);
                }
                //Label lblPages = new Label();
                //lblPages.CssClass = "PageNoCSS";
                //lblPages.Text = "Page No:" + pageno + " of " + totalpage + " Pages";
                //e.Row.Cells[0].Controls.Add(lblPages);
                if (Convert.ToInt32(ViewState["TotalPage"]) == 1)
                {
                    btnNext.Enabled = false;
                    btnPrevious.Enabled = false;
                }
                else if (Convert.ToInt32(ViewState["TotalPage"]) == Convert.ToInt32(ViewState["PageNo"]))
                {
                    btnNext.Enabled = false;
                    btnPrevious.Enabled = true;
                }
                else if (Convert.ToInt32(ViewState["PageNo"]) == 1)
                {
                    btnNext.Enabled = true;
                    btnPrevious.Enabled = false;
                }
                else
                {
                    btnPrevious.Enabled = true;
                    btnNext.Enabled = true;
                }

            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Inbox.aspx", "grdMessage_RowCreated", ex.Message);
        }
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;

            Messages msg = new Messages(Convert.ToInt64(lnk.CommandArgument));
            // msg.IsActive = false;
            msg.Delete();
            BindMessageList();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Inbox.aspx", "lnkDelete_Click", ex.Message);
        }
    }
    protected void linkNotice_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            Messages msg = new Messages(Convert.ToInt64(lnk.CommandArgument));
            msg.IsRead = false;
            msg.Save();
            BindMessageList();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Inbox.aspx", "linkNotice_Click", ex.Message);
        }

    }

    [System.Web.Services.WebMethod]
    public static void check(string name)
    {
        try
        {
            Messages msg = new Messages(Convert.ToInt64(name));
            msg.IsRead = false;
            msg.Save();

            var call = new Pages_Inbox();
            call.BindMessageList();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Inbox.aspx", "check", ex.Message);
        }
        // BindMessageList();

    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            int i = 0;
            string MessageIdList = string.Empty;
            Messages msg = new Messages();
            foreach (GridViewRow row in grdMessage.Rows)
            {
                CheckBox chkb = (CheckBox)row.FindControl("chkMessage") as CheckBox;
                if (chkb.Checked == true)
                {
                    HiddenField hdnMessageID = (HiddenField)row.FindControl("hdnMessageID") as HiddenField;

                    MessageIdList = MessageIdList + "," + Convert.ToInt64(hdnMessageID.Value);
                    // msg.IsActive = false;

                    i = i + 1;
                }
            }
            if (i == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "HideConfirmBox", "HideConfirmBox()", true);
                return;
            }
            else
            {
                msg.DeleteList(MessageIdList);
            }

            BindMessageList();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Inbox.aspx", "btnDelete_Click", ex.Message);
        }
    }
    protected void btnRead_Click(object sender, EventArgs e)
    {
        try
        {
            int i = 0;
            string MessageIdList = string.Empty;
            Messages msg = new Messages();
            foreach (GridViewRow row in grdMessage.Rows)
            {
                CheckBox chkb = (CheckBox)row.FindControl("chkMessage") as CheckBox;
                if (chkb.Checked == true)
                {
                    HiddenField hdnMessageID = (HiddenField)row.FindControl("hdnMessageID") as HiddenField;

                    MessageIdList = MessageIdList + "," + Convert.ToInt64(hdnMessageID.Value);
                    // msg.IsActive = false;

                    i = i + 1;
                }
            }
            if (i == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "HideConfirmBox", "HideConfirmBox()", true);
                return;
            }
            else
            {
                msg.MarkAsReadList(MessageIdList);
            }

            BindMessageList();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Inbox.aspx", "btnRead_Click", ex.Message);
        }
    }
    protected void grdMessage_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "ViewChargeslip")
            {
                Session["ChargeSlipId"] = Convert.ToInt32(e.CommandArgument);
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "javascript:Navigate();", true);
            }
            if (e.CommandName == "View")
            {
                Messages msg = new Messages();
                msg.MessageID = Convert.ToInt64(e.CommandArgument);
                DataSet ds = new DataSet();
                ds = msg.GetMessageDetail();
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = new DataTable();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["FieldChange"].ToString()))
                        if (ds.Tables[0].Rows[0]["MessageType"].ToString() == "2")
                        {
                            string filedchange = string.Empty;
                            for (int icount = 0; icount < ds.Tables[0].Rows.Count; icount++)
                            {
                                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["FieldChange"].ToString()))
                                {
                                    filedchange += ds.Tables[0].Rows[icount]["FieldChange"].ToString();
                                }
                                else
                                    filedchange += ds.Tables[0].Rows[icount]["MessageText"].ToString();
                            }

                            int i = filedchange.Length;
                            if (filedchange[i - 1] == ',')
                            {
                                filedchange = filedchange.Substring(0, i - 1);
                            }
                            lblMessageChangeDetail.Text = "<b>The following fields of Chargeslip " + ds.Tables[0].Rows[0]["DealName"].ToString() + " have been changed:-<br/></b> ";
                            string[] fielarr = filedchange.Split(',');
                            int len = fielarr.Length;
                            for (int x = 0; x < len; x++)
                            {
                                //lblMessageChangeDetail.Text += "<b>" + fielarr[x] + "<b/><br/>";
                                lblMessageChangeDetail.Text += fielarr[x] + "<br/>";
                            }
                            // lblMessageChangeDetail.Text += ".";
                        }

                        else
                        {
                            if (ds.Tables[0].Rows[0]["MessageType"].ToString() == "3")//deleted msgs
                            {
                                lblMessageChangeDetail.Text = "<b>" + ds.Tables[0].Rows[0]["MessageText"].ToString() + ".<br/></b>";
                                lblMessageChangeDetail.Text += ds.Tables[0].Rows[0]["ReasonToDelete"].ToString();
                            }
                            if (ds.Tables[0].Rows[0]["MessageType"].ToString() == "4")//Editing Payment Amount for Individual Co-Broker
                            {
                                //lblMessageChangeDetail.Text = "<b>" + ds.Tables[0].Rows[0]["MessageText"].ToString() + ".<br/></b>";
                                lblMessageChangeDetail.Text = "<b> The Reason for Payment Changed: <br/><br/></b>";
                                lblMessageChangeDetail.Text += ds.Tables[0].Rows[0]["FieldChange"].ToString();
                            }
                            else
                                lblMessageChangeDetail.Text = "<b>" + ds.Tables[0].Rows[0]["MessageText"].ToString() + ".<b/>";
                        }
                    }
                }
                msg.IsRead = false;
                msg.Save();
                BindMessageList();
                modalMessageDetail.Show();
            }//view

        }//try
        catch (Exception ex)
        {

            ErrorLog.WriteLog("Inbox.aspx.cs", "grdMessage_RowCommand", ex.Message);
        }
    }
    protected void grdMessage_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                ((CheckBox)e.Row.FindControl("cbSelectAll1")).Attributes.Add("onclick", "javascript:SelectAll('" + ((CheckBox)e.Row.FindControl("cbSelectAll1")).ClientID + "')");
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnIsRead = (HiddenField)e.Row.FindControl("hdnIsRead");
                HiddenField hdnChargeslipId = (HiddenField)e.Row.FindControl("hdnChargeslipId");
                //Label lblText = (Label)e.Row.FindControl("lblText");
                if (hdnChargeslipId != null)
                {
                    ImageButton imgChargeslip = (ImageButton)e.Row.FindControl("imgChargeslip");

                    if (Convert.ToInt32(hdnChargeslipId.Value) > 0)
                    {
                        imgChargeslip.Visible = true;
                        //lblText.Visible = true;
                    }
                    else
                    {

                        imgChargeslip.Visible = false;
                        //lblText.Visible = false;
                    }
                }
                if (hdnIsRead.Value == "True")
                {
                    LinkButton lnk = (LinkButton)e.Row.FindControl("linkNotice");
                    lnk.CssClass = "linkbut";


                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Inbox.aspx", "grdMessage_RowDataBound", ex.Message);
        }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void GetChargeSlipDetails(string RowIndx)
    {
        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {
                int ChargeSlipID = Convert.ToInt32(HttpContext.Current.Session["ChargeSlipId"]);
                Utility.ClearSessionExceptUser();
                ChargeSlip objChargeSlip = new ChargeSlip();
                objChargeSlip.ChargeSlipID = ChargeSlipID;
                objChargeSlip.Load();

                DealTransactions objDealTransactions = new DealTransactions();
                objDealTransactions.ChargeSlipID = ChargeSlipID;
                objDealTransactions.LoadByChargeSlipId();


                objChargeSlip.DealTransactionsProperty = objDealTransactions;
                HttpContext.Current.Session[GlobleData.NewChargeSlipObj] = objChargeSlip;
                HttpContext.Current.Session[GlobleData.NewChargeSlipId] = ChargeSlipID;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Billed.aspx", "GetChargeSlipDetails", ex.Message);
        }
    }
}