﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;

public partial class Pages_AddOrEditNewChargeSlip : System.Web.UI.Page
{
    #region Variables
    DataSet dsCSDetails;
    DataTable dtDraftChargeSlipList = null;
    string CSId = null;
    private const string RowCount = "RowCnt";
    private const string TotalPages = "PageCnt";
    private const string PageNumber = "PageNo";
    private const int PageSize = 20;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {

                ViewState[PageNumber] = 1;
                BindGgrid();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteLog("AddOrEditNewChargeSlip.aspx", "Page_Load", ex.Message);
                // throw;
            }
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        GridViewRow grdStud = gvChargeSlipsInProgress.BottomPagerRow;
        if (grdStud != null)
        {
            grdStud.Visible = true;
        }
    }
    protected void gvChargeSlipsInProgress_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            // int PageNo = Convert.ToInt16(ViewState[PageNumber]);
            //int PageNo = e.NewPageIndex + 1;
            //ViewState[PageNumber] = PageNo;
            //BindGgrid();

            int PageNo = Convert.ToInt32(ViewState[PageNumber]);

            if (e.NewPageIndex == -1)
            {
                PageNo++;
                ViewState[PageNumber] = PageNo;
            }
            else if (e.NewPageIndex == -2)
            {
                PageNo--;
                ViewState[PageNumber] = PageNo;
            }
            else
            {
                PageNo = e.NewPageIndex + 1;
                ViewState[PageNumber] = PageNo;
            }
            BindGgrid();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("AddOrEditNewChargeSlip.aspx", "gvChargeSlipsInProgress_PageIndexChanging", ex.Message);
        }
    }


    #region Actions

    private void BindGgrid()
    {
        ChargeSlip objChargeSlip = new ChargeSlip();
        objChargeSlip.Status = 0;
        dsCSDetails = objChargeSlip.GetChargeSlipPagingListByStatus(Convert.ToInt16(ViewState[PageNumber]), PageSize, Convert.ToInt32(Session[GlobleData.User]));
        if (dsCSDetails != null)
        {
            //get Row Count
            double Rows = Convert.ToInt32(dsCSDetails.Tables[0].Rows[0]["RowCounts"].ToString());

            ViewState[RowCount] = Rows;
            ViewState[TotalPages] = Math.Ceiling(Rows / PageSize);

            //get details
            if (dsCSDetails.Tables.Count > 0)
            {
                dtDraftChargeSlipList = dsCSDetails.Tables[1];

                //if (dtDraftChargeSlipList.Rows.Count > 0)
                //{
                gvChargeSlipsInProgress.DataSource = dtDraftChargeSlipList;
                gvChargeSlipsInProgress.DataBind();
                //}
            }
            else
            {
                gvChargeSlipsInProgress.DataSource = dtDraftChargeSlipList;
                gvChargeSlipsInProgress.DataBind();
            }
        }
    }

    private string GetDeleteChargeSlipIDList()
    {
        foreach (GridViewRow gRow in this.gvChargeSlipsInProgress.Rows)
        {
            CheckBox Chk = (CheckBox)(gRow.Cells[0].FindControl("chkChargeSlip"));
            if (Chk.Checked)
            {
                CSId = CSId + "," + gRow.Cells[5].Text;
            }
        }
        if (!string.IsNullOrEmpty(CSId))
        {
            CSId = CSId.TrimStart(',');
        }

        return CSId;
    }

    #endregion
    protected void gvChargeSlipsInProgress_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                int pageno = Convert.ToInt32(ViewState[PageNumber]);
                int totalpage = Convert.ToInt32(ViewState[TotalPages]);
                LinkButton btnPrevious = new LinkButton();
                btnPrevious.ID = "btnPrevious";
                btnPrevious.Text = "<";
                btnPrevious.CommandName = "Page";
                btnPrevious.CommandArgument = "-1";
                btnPrevious.Width = Unit.Pixel(15);
                btnPrevious.CssClass = "removeunderline";
                LinkButton btnNext = new LinkButton();
                btnNext.ID = "btnNext";
                btnNext.Text = ">";
                btnNext.CommandName = "Page";
                btnNext.CssClass = "removeunderline";
                btnNext.CommandArgument = "0";
                e.Row.Cells[0].Controls.Add(btnPrevious);
                for (int i = 1; i <= totalpage; i++)
                {
                    LinkButton lnk = new LinkButton();
                    lnk.ID = "lnk" + i;
                    lnk.Text = i.ToString() + " ";
                    lnk.CommandArgument = i.ToString();
                    lnk.CommandName = "Page";
                    e.Row.Cells[0].Controls.Add(lnk);
                    lnk.CssClass = "removeunderline";
                    if (Convert.ToInt32(ViewState["PageNo"]) == i)
                    {
                        lnk.CssClass = "linkbut";
                    }
                }
                e.Row.Cells[0].Controls.Add(btnNext);
                if (ViewState[RowCount] != null)
                {

                    Label lblTotalRecords = new Label();
                    lblTotalRecords.CssClass = "PageNoCSS";
                    lblTotalRecords.Text = "Total Records:" + ViewState[RowCount].ToString();
                    e.Row.Cells[0].Controls.Add(lblTotalRecords);
                }
                if (Convert.ToInt32(ViewState[TotalPages]) == 1)
                {
                    btnNext.Enabled = false;
                    btnPrevious.Enabled = false;
                }
                else if (Convert.ToInt32(ViewState[TotalPages]) == Convert.ToInt32(ViewState[PageNumber]))
                {
                    btnNext.Enabled = false;
                    btnPrevious.Enabled = true;
                }
                else if (Convert.ToInt32(ViewState[PageNumber]) == 1)
                {
                    btnNext.Enabled = true;
                    btnPrevious.Enabled = false;
                }
                else
                {
                    btnPrevious.Enabled = true;
                    btnNext.Enabled = true;
                }
                //Label lblpages = new Label();
                //lblpages = (Label)e.Row.FindControl("lblPages");
                //lblPages.Text = "Page No:" + pageno + " of " + totalpage + " Pages";
                //Label lbl = new Label();
                //lbl.ID = "lbl";
                //lbl.Text = "Page No:" + pageno + " of " + totalpage + " Pages";
                //e.Row.Cells[0].Controls.Add(lbl);
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("AddOrEditNewChargeSlip.aspx", "grdMessage_RowCreated", ex.Message);
        }
    }
    protected void gvChargeSlipsInProgress_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            ((CheckBox)e.Row.FindControl("chkAllChargeSlip")).Attributes.Add("onclick", "javascript:SelectAllGridCheckboxes(this,'" + gvChargeSlipsInProgress.ClientID + "')");
            e.Row.Cells[5].Visible = false;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[5].Visible = false;
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            CSId = GetDeleteChargeSlipIDList();

            if (!string.IsNullOrEmpty(CSId))
            {
                ChargeSlip objChargeSlip = new ChargeSlip();
                Boolean result = objChargeSlip.DeleteChargeSlipRecords(CSId);
                BindGgrid();
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("AddOrEditNewChargeSlip.aspx", "btnDelete_Click", ex.Message);
        }
    }
    protected void delRecord_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            ChargeSlip objChargeSlip = new ChargeSlip();
            ImageButton imgBtn = sender as ImageButton;
            GridViewRow gvRow = imgBtn.NamingContainer as GridViewRow;
            Boolean result = objChargeSlip.DeleteChargeSlipRecords(gvChargeSlipsInProgress.Rows[gvRow.RowIndex].Cells[5].Text);
            BindGgrid();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("AddOrEditNewChargeSlip.aspx", "delRecord_Click", ex.Message);
        }
    }

    protected void btnEdit_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            ImageButton imgBtn = sender as ImageButton;
            GridViewRow gvRow = imgBtn.NamingContainer as GridViewRow;
            GetChargeSlipDetails(Convert.ToInt32(gvChargeSlipsInProgress.Rows[gvRow.RowIndex].Cells[5].Text));
            // Response.Redirect("NewCharge.aspx");
            Response.Redirect("NewCharge.aspx", false);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("AddOrEditNewChargeSlip.aspx", "btnEdit_Click", ex.Message);
        }


    }

    private void GetChargeSlipDetails(int NewChargeSlipId)
    {
        try
        {
            Utility.ClearSessionExceptUser();//Added by Jaywanti on 28-09-2015
            ChargeSlip objChargeSlip = new ChargeSlip();
            objChargeSlip.ChargeSlipID = NewChargeSlipId;
            objChargeSlip.Load();

            DealTransactions objDealTransactions = new DealTransactions();
            objDealTransactions.ChargeSlipID = NewChargeSlipId;
            objDealTransactions.LoadByChargeSlipId();


            objChargeSlip.DealTransactionsProperty = objDealTransactions;

            Session[GlobleData.NewChargeSlipObj] = objChargeSlip;
            HttpContext.Current.Session[GlobleData.NewChargeSlipId] = NewChargeSlipId;//Added by Jaywanti on 28-09-2015
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("AddOrEditNewChargeSlip.aspx", "GetChargeSlipDetails", ex.Message);
        }

    }
    protected void btnAddNewCharge_Click(object sender, EventArgs e)
    {
        if (Session[GlobleData.User] != null && Session["User"] != null)
        {
            //int user = Convert.ToInt32(Session[GlobleData.User]);
            //string Name = Session["User"].ToString();
            //Session.Clear();
            //Session.RemoveAll();
            //Session[GlobleData.User] = user;
            //Session["UserType"] = 2;
            //Session["User"] = Name;

            Utility.ClearSessionExceptUser();
            Response.Redirect("NewCharge.aspx", false);
        }
        else
        {
            //session null 

        }
    }


}