﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true" CodeFile="Reports.aspx.cs" Inherits="Pages_Reports" EnableEventValidation="false" %>

<%@ Register TagPrefix="aspCustom" Namespace="Saplin.Controls" Assembly="DropDownCheckBoxes" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <script type="text/javascript">
        var drp1='<%=drpCriteria1.ClientID%>';
        var drp2='<%=drpCriteria2.ClientID%>';
        var drp3='<%=drpCriteria3.ClientID%>';
         
        $(document).ready(function(){
            //alert('Ready');
            loadDateRangeButton();
        });
        
        function loadDateRangeButton(){
            if($('#'+drp1).val()=="5")
            {
                document.getElementById('<%=btnCriteria1ResetCalenderPeriod.ClientID%>').style.display="block";
            }
            if($('#'+drp2).val()=="5")
            {
                document.getElementById('<%=btnCriteria2ResetCalenderPeriod.ClientID%>').style.display = "block";
            }
            if($('#'+drp3).val()=="5")
            {
                document.getElementById('<%=btnCriteria3ResetCalenderPeriod.ClientID%>').style.display = "block";
            }
        }
        function ddlClick(Control) {
            debugger;
            // alert(Control);
            if ($('#' + Control).val() == "5") {
                document.getElementById('trCriteria1From').style.display = "none";
                document.getElementById('trCriteria1To').style.display = "none";
                document.getElementById('trCriteria2From').style.display = "none";
                document.getElementById('trCriteria2To').style.display = "none";
                document.getElementById('trCriteria3From').style.display = "none";
                document.getElementById('trCriteria3To').style.display = "none";
                if (Control == "ContentMain_drpCriteria1") {
                    document.getElementById('<%=txtCritera1From.ClientID%>').value = "";
                    document.getElementById('<%=txtCritera1To.ClientID%>').value = "";
                    document.getElementById('trCriteria1From').style.display = "block";
                    document.getElementById('trCriteria1To').style.display = "block";
                    document.getElementById('<%=btnCriteria1ResetCalenderPeriod.ClientID%>').style.display = "block";
                    
                }
                else if (Control == "ContentMain_drpCriteria2") {
                    document.getElementById('<%=txtCritera2From.ClientID%>').value = "";
                    document.getElementById('<%=txtCritera2To.ClientID%>').value = "";
                    document.getElementById('trCriteria2From').style.display = "block";
                    document.getElementById('trCriteria2To').style.display = "block";
                    document.getElementById('<%=btnCriteria2ResetCalenderPeriod.ClientID%>').style.display = "block";
                }
                else if (Control == "ContentMain_drpCriteria3") {
                    document.getElementById('<%=txtCritera3From.ClientID%>').value = "";
                    document.getElementById('<%=txtCritera3To.ClientID%>').value = "";
                    document.getElementById('trCriteria3From').style.display = "block";
                    document.getElementById('trCriteria3To').style.display = "block";
                    document.getElementById('<%=btnCriteria3ResetCalenderPeriod.ClientID%>').style.display = "block";
                }
        $find('<%=modalCalendarDateRange.ClientID%>').show();
                //alert($('#' + '<%=modalCalendarDateRange.ClientID%>').id);
                //$('#' + '<%=modalCalendarDateRange.ClientID%>').show();
                return false;
            }
            document.getElementById('<%=btnFilterDateRange.ClientID%>').click();
            return true;
        }

        function showDateRangePopup() {
            debugger;                      
            $find('<%=modalCalendarDateRange.ClientID%>').show();
            return false;
            }            
        function ShowPopUpPayment(PaymentId, ChargeslipId, Amount) {
            debugger

                     <%-- $('#<%=hdnPayPaymentHistoryId.ClientID %>').val(PaymentId);
            $('#<%=hdnPayChargeslipId.ClientID %>').val(ChargeslipId);
            $('#<%=hdnPayNewAmount.ClientID %>').val(Amount);
            document.getElementById('<%=txtPaymentAmount.ClientID%>').value = parseFloat(Amount).toFixed(2);
            document.getElementById('<%=txtReasonEdit.ClientID%>').value = "";
            document.getElementById('<%=lblErrorPayment.ClientID%>').innerHTML = "";
            $('#<%=dvChargeSlipEditPayment.ClientID %>').show();
            return false;--%>
            return true;
        }

        function imgEditbtn_ClientClick(imgbtn) {
            try {

                var ttd = imgbtn.parentElement
                var childLength = ttd.childNodes.length;
                for (var i = 0; i < childLength; i++) {
                    if (ttd.childNodes[i].type == 'text') {
                        ttd.childNodes[i].setAttribute("Style", "display:inline-grid;width:120px;");
                        ttd.childNodes[i].value = ttd.childNodes[3].innerHTML
                    }

                }
                imgbtn.setAttribute("Style", "display:none;");
                ttd.childNodes[5].setAttribute("Style", "display:none;");
                ttd.childNodes[7].setAttribute("Style", "display:inline-grid;width:13px;height:13px;");
                ttd.childNodes[9].setAttribute("Style", "display:inline-grid;width:12px;height:12px;");
                ttd.childNodes[3].setAttribute("Style", "display:none;");

            }
            catch (exce) {
            }
            return false;

        }


        function imgSavebtn_ClientClick(imgbtn) {
            debugger;
            try {

                var ttd = imgbtn.parentElement
                var childLength = ttd.childNodes.length;
                var textValue;
                for (var i = 0; i < childLength; i++) {
                    if (ttd.childNodes[i].type == 'text') {
                        textValue = ttd.childNodes[i].value;
                        ttd.childNodes[i].setAttribute("Style", "display:none;");
                        break;
                    }

                }

                //var tr = $(e).parent().parent();
                //var ChargeSlipID = $(".RemainingQty > Span", $(tr))[0];

                var ttr = ttd.parentElement;


                var PaymentHistoryID = ttd.childNodes[11].innerHTML; // ttr.cells[1].textContent;
                //var ChargeSlipID = ttd.childNodes[13].innerHTML;
                //var PaymentID = ttd.childNodes[15].innerHTML;
                //var BrokerID = ttd.childNodes[17].innerHTML;

                imgbtn.setAttribute("Style", "display:none;");
                var OldValue = ttd.childNodes[3].innerHTML;
                ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[7].setAttribute("Style", "display:none;");
                ttd.childNodes[9].setAttribute("Style", "display:none;");



                //if (textValue.trim() != '') {
                ttd.childNodes[3].innerHTML = textValue;
                $.ajax({
                    type: "POST",
                    url: "Reports.aspx/SaveBrokerCheckNo",
                    //data: '{PaymentHistoryId: "' + PaymentHistoryID + '",CheckNo: "' + textValue + '",ChargeSlipID: "' + ChargeSlipID + '",PaymentID: "' + PaymentID + '",BrokerID: "' + BrokerID + '"  }',
                    data: '{PaymentHistoryId: "' + PaymentHistoryID + '",CheckNo: "' + textValue + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    failure: function (response) {
                        alert(response.d);
                    }
                });
                //}
                //else {
                //    ttd.childNodes[3].innerHTML = OldValue;
                //}
            }
            catch (ex) {
            }
            return false;
        }

        function DisplayPaymentMessage(PaymentHistoryId) {

            debugger;
     <%--       debugger;
            var pageurl = document.URL;
            var Page = "ReportChargeslipReviewCard";
            if (pageurl.indexOf(Page) < 0) {
                var UserRole = '<%=Session["UserType"]%>';
            if (UserRole != null) {
                if (UserRole != "") {
                    if (UserRole == "1") {
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "Customers.aspx/GetIndividualBrokerPaymentMessage",
                            data: '{PaymentHistoryId: "' + PaymentHistoryId + '" }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                //alert('p');
                                if (response.d == "") {

                                    document.getElementById('<%=lblMessageChangeDetail.ClientID%>').innerHTML = "<b>No Payment Amount Changed.</b>";

                                }
                                else {

                                    document.getElementById('<%=lblMessageChangeDetail.ClientID%>').innerHTML = "<b> The Reason for Payment Changed</b>" + response.d;
                                    $('#<%=divPaymentMessage.ClientID %>').show();
                                }
                            },
                            failure: function (response) {

                                alert(response.d);
                                return false;
                            }
                        });
                    }
                    else {
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "../Admin/Customers.aspx/GetIndividualBrokerPaymentMessage",
                            data: '{PaymentHistoryId: "' + PaymentHistoryId + '" }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                //alert('p');
                                if (response.d == "") {

                                    document.getElementById('<%=lblMessageChangeDetail.ClientID%>').innerHTML = "<b>No Payment Amount Changed.</b>";

                                }
                                else {

                                    document.getElementById('<%=lblMessageChangeDetail.ClientID%>').innerHTML = "<b> The Reason for Payment Changed</b>" + response.d;
                                    $('#<%=divPaymentMessage.ClientID %>').show();
                                }
                            },
                            failure: function (response) {

                                alert(response.d);
                                return false;
                            }
                        });

                    }
                }
            }
        }
        else {
            $.ajax({
                async: true,
                type: "POST",
                url: "../Admin/Customers.aspx/GetIndividualBrokerPaymentMessage",
                data: '{PaymentHistoryId: "' + PaymentHistoryId + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //alert('p');
                    if (response.d == "") {

                        document.getElementById('<%=lblMessageChangeDetail.ClientID%>').innerHTML = "<b>No Payment Amount Changed.</b>";

                    }
                    else {

                        document.getElementById('<%=lblMessageChangeDetail.ClientID%>').innerHTML = "<b> The Reason for Payment Changed</b>" + response.d;
                        $('#<%=divPaymentMessage.ClientID %>').show();
                    }
                },
                failure: function (response) {

                    alert(response.d);
                    return false;
                }
            });

        }


     --%>
            return true;
        }
    </script>

    <style type="text/css">
        .modalBackground {
            background-color: gray;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .Paddingtd {
            padding-left: 20px;
            width: 80px;
        }
    </style>
    <table style="width: 100%">
        <tr>
            <td style="padding-top: 0.5em; color: black; border-color: black; text-align: left; text-shadow: initial; font-weight: 600; font-size: 18px;">
                <%--<table>
                                    <tr>
                                        <td>--%>
                <asp:Label ID="lbErrorMsg" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="Small" /></td>
            <td class="mainPageTitle">REPORTS
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <script src="../Js/CommonValidations.js"></script>
    <link href="../Styles/SiteTEST.css" rel="stylesheet" />
    <asp:ScriptManager ID="scrpmanager1" runat="server"></asp:ScriptManager>
    <style type="text/css">
        .dd_chk_select {
            width: 248px;
        }

        .RedColor {
            color: blue;
        }

        .label {
            font-weight: bold;
            width: 25%;
        }

        .drpdown {
            Width: 250px;
        }

        .ajax__calendar_container {
            z-index: 1000;
        }
    </style>

    <script type="text/javascript">

        function isDecimal(field) {

            var valid = "0123456789."
            var ok = "yes";
            var temp;
            for (var i = 0; i < field.value.length; i++) {
                temp = "" + field.value.substring(i, i + 1)
                if (valid.indexOf(temp) == "-1") {
                    ok = "no";
                    // field.value = "";
                    return false;
                }
            }

            var res = field.value.split('.');
            if (res.length > 2) {
                ok = "no";
                // field.value = "";
                return false;

            }

            return true;
        }

        function validate_integer() {

            var field1 = document.getElementById('<%=txtvalue1.ClientID %>');
            var field2 = document.getElementById('<%=txtvalue2.ClientID %>');

            //if (!isDecimal(field1)) {
            //    field1.value = "";
            //}

            //if (!isDecimal(field2)) {
            //    field2.value = "";
            //}
            if ($.trim(field1.value) != "" && $.trim(field2.value) != "") {
                if (Date.parse($.trim(field1.value)) > Date.parse($.trim(field2.value))) {
                    alert('From value cannot be greater than To value');
                    field1.value = "";
                    field2.value = "";
                    return false;
                }
            }

        }


        function validateNumberOnly() {
            var field = document.getElementById('<%=txtZipCode.ClientID %>');
            var valid = "0123456789"
            var ok = "yes";
            var temp;
            for (var i = 0; i < field.value.length; i++) {
                temp = "" + field.value.substring(i, i + 1)
                if (valid.indexOf(temp) == "-1") {
                    ok = "no";
                    field.value = "";
                    return;
                }
            }

        }




        function validateDate() {
            var fromDate = document.getElementById('<%=txtFrom.ClientID %>');
            if (!isValidDate(fromDate.value)) {
                //alert("Please enter Daily call date in mm/dd/yyyy format. !");
                fromDate.value = "";
                // return false;
            }

            var toDate = document.getElementById('<%=txtTo.ClientID %>');
            if (!isValidDate(toDate.value)) {
                //alert("Please enter Daily call date in mm/dd/yyyy format. !");
                toDate.value = "";
                //return false;

            }
            if ($.trim(fromDate.value) != "" && $.trim(toDate.value) != "") {
                if (Date.parse($.trim(fromDate.value)) > Date.parse($.trim(toDate.value))) {
                    alert('From Date cannot be greater than To Date');
                    toDate.value = "";
                    fromDate.value = "";
                    return false;
                }
            }

        }

        //function validateRun() {

        //       if (dailycalldate == "") {
        //           alert("Please enter a daily call date.");
        //           return false;

        //       }

        //       if (!isValidDate(dailycalldate.value)) {
        //           alert("Please enter Daily call date in mm/dd/yyyy format. !");
        //           return false;
        //       }

        //       return true;
        //}


        function isLeapYear(year) {
            if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
                return true;
            }
            else {
                return false;
            }
        }

        function isValidDate(inputValue) {
            var regdate = /^\d{1,2}(-|\/)\d{1,2}(-|\/)\d{4}$/;    ////For Date in mm/dd/yyyy format
            //var regdate = /^\d{1,2}\/\d{1,2}\/\d{4}$/;    ////For Date in mm/dd/yyyy format
            //var reg = /^\d{1,2}\-\d{1,2}\-\d{4}$/;
            if (!regdate.test(inputValue)) {
                return false;
            }
            else {
                var arrDate;
                if (inputValue.indexOf('-') > 0) {
                    arrDate = inputValue.split('-');
                }
                else {
                    arrDate = inputValue.split('/');
                }

                var day = parseFloat(arrDate[1]);
                var month = parseFloat(arrDate[0]);
                var year = parseFloat(arrDate[2]);
                var MonthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
                if (isLeapYear(year)) {
                    MonthDays[1] = 29;
                }
                if (month < 1 || month > 12 || day < 1 || day > MonthDays[month - 1]) {
                    return false;
                }
                else {
                    return true;
                }
            }
        }


    </script>




    <%--</td>
                                        <td>--%>

    <table style="width: 99%; height: 90%;">
        <tr style="height: 75%">
            <td style="width: 25%; vertical-align: top; border: solid 1px black;">
                <div style="margin: 5px 7px 0px 5px; height: 99%;">
                    <%--</td>
                                        <td>--%>
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lbltitle" Text="" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td class="label">TYPE</td>
                            <td>
                                <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true" CssClass="drpdown" OnSelectedIndexChanged="ddlType_SelectedIndexChanged"></asp:DropDownList>
                            </td>
                        </tr>


                        <tr runat="server" id="trBroker" visible="false">
                            <td class="label" id="lblbroker" runat="server">BROKER</td>
                            <td>
                                <asp:DropDownList ID="ddlBroker" runat="server" CssClass="drpdown"></asp:DropDownList></td>
                        </tr>
                        <tr id="trOwner" runat="server" visible="false">
                            <td class="label">OWNER</td>
                            <td>
                                <asp:DropDownList ID="ddlOwner" runat="server" CssClass="drpdown"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr runat="server" id="trTenant" visible="false">
                            <td class="label">TENANT</td>
                            <td>
                                <asp:DropDownList ID="ddlTenant" runat="server" CssClass="drpdown"></asp:DropDownList></td>
                        </tr>
                        <tr id="trLandLord" runat="server" visible="false">
                            <td class="label">LANDLORD</td>
                            <td>
                                <asp:DropDownList ID="ddlLandLord" runat="server" CssClass="drpdown"></asp:DropDownList>
                            </td>
                        </tr>

                        <tr id="trLocation" runat="server" visible="false">
                            <td class="label">
                                <asp:Label ID="lblLOCATION" runat="server" Text="LOCATION"></asp:Label></td>
                            <td>
                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="drpdown"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trDesignationType" runat="server" visible="false">
                            <td class="label">
                                <asp:Label ID="lblDesignation" runat="server" Text="Designation Type"></asp:Label></td>
                            <td>
                                <asp:DropDownList ID="ddlDesignationType" runat="server" CssClass="drpdown"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trAddress" runat="server" visible="false">
                            <td style="width: 100px;"></td>
                            <td>
                                <asp:TextBox ID="txtLocation" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="trValue" runat="server" visible="false">
                            <td class="label">VALUE</td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtvalue1" runat="server" Width="80px" onblur="validate_integer();"></asp:TextBox></td>
                                        <td>
                                            <asp:Label ID="lblValueTo" Text="To" runat="server">
                                            </asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtvalue2" runat="server" Width="80px" onblur="validate_integer();"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trCity" runat="server" visible="false">
                            <td class="label">CITY</td>
                            <td>
                                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr id="trCounty" runat="server" visible="false">
                            <td class="label">COUNTY</td>
                            <td>
                                <asp:TextBox ID="txtCounty" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr id="trState" runat="server" visible="false">
                            <td class="label">STATE</td>
                            <td>
                                <asp:DropDownList ID="ddlState" runat="server" Width="155px"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trZipCode" runat="server" visible="false">
                            <td class="label">ZIPCODE</td>
                            <td>
                                <asp:TextBox ID="txtZipCode" runat="server" onblur="validateNumberOnly();" Width="80px"></asp:TextBox></td>
                        </tr>
                        <tr id="trMultipleBroker" runat="server" visible="false">
                            <td class="label" id="Td1" runat="server">BROKER</td>
                            <td>
                                <aspCustom:DropDownCheckBoxes ID="DropDownCheckBoxes1" runat="server" Width="180px">
                                </aspCustom:DropDownCheckBoxes>

                            </td>
                        </tr>
                        <tr id="trDate" runat="server" visible="true">
                            <td class="label">DATE</td>
                            <td>
                                <%--</td>
                                    </tr>
                                </table>--%>
                                <asp:TextBox ID="txtFrom" runat="server" Width="80px" onblur="validateDate();"></asp:TextBox>
                                <ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txtFrom" runat="server" />
                                <%-- OnClientClick="javascript:return validateRun();" --%>
                                <asp:Label ID="lblTo" Text="To" runat="server">
                                </asp:Label>
                                <%--</td>
                    </tr>
                </table>--%>
                                <asp:TextBox ID="txtTo" runat="server" Width="80px" onblur="validateDate();"></asp:TextBox>
                                <ajax:CalendarExtender ID="CalendarExtender2" TargetControlID="txtTo" runat="server" />

                                <%--Height="583px" Width="900px" --%>
                            </td>
                        </tr>

                        <tr>
                            <td class="label">&nbsp;</td>
                            <td>
                                <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" runat="server" Width="100px" Text="CLEAR" CssClass="SqureButton" />&nbsp;
                            
                                <asp:Button ID="btnRun" OnClick="btnRun_Click" runat="server" Width="100px" Text="RUN" CssClass="SqureButton" />
                                <%--Height="520px" --%>
                            </td>
                        </tr>
                        <tr id="trBrokerList" runat="server" visible="false">
                            <td class="label" runat="server" id="tdBrokerList">SELECTED BROKERS</td>
                            <td>
                                <asp:ListBox ID="lstBrokerList"  runat="server" Width="250px"></asp:ListBox>
                            </td>
                        </tr>
                    </table>
                    <%-- <table style="border: 1px solid; width: 100%; padding: 10px 10px 10px 10px">
                      <tr><td></td></tr>
                      </table> --%>
                </div>
            </td>
            <td></td>
            <td style="width: 74%; vertical-align: top; border: solid 1px black;">
                <div style="margin: 5px 5px 0px 5px; width: 100%; height: 99%;">
                    <%--Height="583px" Width="900px" --%><%--Height="520px" --%>

                    <div style="width: 98%; overflow: scroll; align-content: center;">
                        <asp:GridView ID="gvBrokerReport" runat="server" AutoGenerateColumns="False" CssClass="gv" font-family="Verdana" Font-Size="12px" OnRowDataBound="gvBrokerReport_RowDataBound" ShowFooter="True" Width="100%">
                            <Columns>

                                <asp:BoundField DataField="ChargeSlipID" HeaderText="ChargeSlipID" Visible="false" />
                                <asp:BoundField DataField="BrokerId" HeaderText="BrokerID" Visible="false" />
                                <asp:BoundField DataField="PaymentID" HeaderText="PaymentID" Visible="false" />
                                <asp:BoundField DataField="PaymentHistoryId" HeaderText="PaymentHistoryId" ItemStyle-HorizontalAlign="Center" Visible="false">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="DealName" HeaderText="Deal Name" />
                                <asp:BoundField DataField="ChargeDate" HeaderText="ChargeDate" Visible="false" />
                                <asp:BoundField DataField="DueDate" HeaderText="DueDate" />
                                <asp:BoundField DataField="BrokerRole" HeaderText="ROLE" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="CHECK AMT" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        $<asp:Label ID="lblCHECKAMT" runat="server" Text='<%# String.Format("{0:f2}",Eval("CheckAmount"))%>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lbltotalCHECKAMT" runat="server" Text=""></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="% DUE" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPercent" runat="server" Text='<%# String.Format("{0:n2}",Eval("BrokerPercent"))%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--                    <asp:BoundField DataField="BrokerPercent" HeaderText="% DUE" ItemStyle-HorizontalAlign="Right" />--%>
                                <%--   <asp:TemplateField HeaderText="AMOUNT DUE" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <asp:Label ID="lbldollar" runat="server" Text="$"></asp:Label>
                                    <asp:LinkButton ID="lnkAmountDue" runat="server" Text='<%# String.Format("{0:f2}",Eval("AmountDue"))%>' OnClientClick=<%#"javascript:DisplayPaymentMessage('" + Eval("PaymentHistoryId") + "');return false;" %>></asp:LinkButton>
                                    <asp:ImageButton ID="btnPaymentAmount" runat="server" ImageUrl="~/Images/edit.jpg"
                                        OnClientClick=<%# "javascript:ShowPopUpPayment('" + Eval("PaymentHistoryId") +"','"+Eval("ChargeSlipID")+"','"+Eval("AmountDue")+ "');return false;" %>
                                        Height="15px" Width="15px" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lbltotalAMOUNTDUE" runat="server" Text=""></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>--%>
                                <asp:BoundField DataField="AmountDue" HeaderText="AMOUNT DUE" ItemStyle-HorizontalAlign="Center" />

                                <%--   <asp:TemplateField HeaderText="AMOUNT DUE" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtAmountDue" Style="display: none"></asp:TextBox>
                            <asp:Label ID="lblAMOUNTDUE" runat="server" Text='<%# String.Format("{0:f2}",Eval("AmountDue"))%>'></asp:Label>
                            <asp:ImageButton runat="server" ID="imgEditbtnBroker" ImageUrl="~/Images/edit.jpg" AlternateText="Edit" OnClientClick="imgEditbtn_ClientBrokerAmountClick(this);return false;"></asp:ImageButton>
                            <asp:ImageButton runat="server" ID="imgSavebtnBroker" ImageUrl="~/Images/iconSave.png" AlternateText="Save" Style="display: none" text="save" OnClientClick="imgSavebtn_ClientBrokerAmountClick(this);return false;"></asp:ImageButton>
                            <asp:Label runat="server" ID="lblPayemntHistorysID" Text='<%#Eval("PaymentHistoryId")%>' Style="display: none"></asp:Label>
                            <asp:Label runat="server" ID="lblChargeslipid" Text='<%#Eval("ChargeslipId")%>' Style="display: none"></asp:Label>

                           
                        </ItemTemplate>
                              <FooterTemplate>
                            <asp:Label ID="lbltotalAMOUNTDUE" runat="server" Text=""></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="SPLIT" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSplit" runat="server" Text='<%# Eval("Split")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="SHARE" ItemStyle-HorizontalAlign="right">
                                    <ItemTemplate>
                                        $<asp:Label ID="lblShare" runat="server" Text='<%# String.Format("{0:f2}",Eval("PaymentReceived"))%>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lbltotalShare" runat="server" Text=""></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="PaymentDate" HeaderText="PAYMENT DATE" ItemStyle-HorizontalAlign="Center" />

                                <asp:BoundField DataField="CheckNo" ControlStyle-Width="20px" Visible="false" HeaderText="CHECK/WIRE NUMBER" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="CHECK/WIRE NUMBER" ItemStyle-HorizontalAlign="right">
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" ID="txtCheckNo" Style="display: none"></asp:TextBox>
                                        <asp:Label ID="lblTestCheckNo" runat="server" Text='<%# String.Format("{0:f2}",Eval("CheckNo"))%>'></asp:Label>
                                        <asp:ImageButton runat="server" ID="imgEditbtn" ImageUrl="~/Images/edit.jpg" AlternateText="Edit" OnClientClick="imgEditbtn_ClientClick(this);return false;"></asp:ImageButton>
                                        <asp:ImageButton runat="server" ID="imgSavebtn" ImageUrl="~/Images/iconSave.png" Width="13px" Height="13px" AlternateText="Save" Style="display: none; height: 13px; width: 13px;" text="save" OnClientClick="imgSavebtn_ClientClick(this);return false;"></asp:ImageButton>
                                        <asp:ImageButton ID="imgCancel" runat="server" ImageUrl="~/Images/close.png" Width="12px" Height="12px" Style="display: none; width: 12px; height: 12px;" AlternateText="Cancel" OnClientClick="imgCancelClick(this);return false;" />
                                        <asp:Label runat="server" ID="lblPayemntHistoryID" Text='<%#Eval("PaymentHistoryId")%>' Style="display: none"></asp:Label>

                                        <%--<asp:Label runat="server" ID="lblChargeSlipID" Text='<%#Eval("ChargeSlipID")%>' Style="display: none"></asp:Label>
                                        <asp:Label runat="server" ID="lblPaymentID" Text='<%#Eval("PaymentID")%>' Style="display: none"></asp:Label>
                                        <asp:Label runat="server" ID="lblBrokerID" Text='<%#Eval("BrokerId")%>' Style="display: none"></asp:Label>--%>
                                        <%-- <input type="image" onclick="imgEditbtn_ClientClick" value="Edit" style="background-color:yellowgreen" />--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PAYMENT" ItemStyle-HorizontalAlign="right" FooterStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        $<asp:Label ID="lblPaymentReceived" runat="server" Text='<%# String.Format("{0:f2}",Eval("PaymentReceived"))%>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblPaymentReceived" runat="server" Text=""></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                            <HeaderStyle CssClass="HeaderGridView2" />
                            <RowStyle CssClass="RowGridView2" />
                            <SelectedRowStyle CssClass="SelectedRowGridView2" />
                            <FooterStyle CssClass="FooterGridView2" />
                        </asp:GridView>

                    </div>


                    <asp:Panel ID="pnlBrokerReport" runat="server" Visible="false">
                        <table>
                            <tr>
                                <td style="width: 16%; text-align: right; font-size: 12px; font-weight: 500;padding-left: 12px;">EXPORT&nbsp;&nbsp;                                    
                                    <asp:ImageButton ID="ImageButton1" Width="15px" Height="15px" runat="server"
                                        OnClick="imgExport_Click" ImageUrl="~/Images/excel.png" />&nbsp;&nbsp;
                                    <asp:ImageButton ID="ImageButton2" Width="15px" Height="15px" runat="server"
                                        OnClick="imgPDF_Click" ImageUrl="~/Images/pdf.png" />
                                    </td>
                            </tr>
                        </table>
                        <table style="width: 98%; background-color: cornflowerblue;">                                                        
                            <tr>
                                <td>Quick Select :</td>
                                <td>Quick Select :</td>
                                <td>Quick Select :</td>
                            </tr>    
                            <tr>
                                <td>                                                                       
                                    <asp:DropDownList ID="drpCriteria1" runat="server" AutoPostBack="true" style="float:left;height: 20px;" onChange="javascript:return ddlClick(this.id)" OnSelectedIndexChanged="drpCriteria1_SelectedIndexChanged">
                                          <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="This fiscal year" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Last fiscal year" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Year to Date" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Previous month" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Calendar Period Range" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Next fiscal year" Value="6"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Button ID="btnCriteria1ResetCalenderPeriod" runat="server" Text="Date" OnClientClick="javascript:return ddlClick(document.getElementById(drp1).id)" style="font-weight: bold;border-color: cornflowerblue;display:none;" />
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpCriteria2" runat="server" AutoPostBack="true" style="float:left;height: 20px;" OnSelectedIndexChanged="drpCriteria2_SelectedIndexChanged" onChange="javascript:return ddlClick(this.id)">
                                             <asp:ListItem Text="--Select--" Value="0" Selected="True"></asp:ListItem>
                                         <asp:ListItem Text="This fiscal year" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Last fiscal year" Value="2" ></asp:ListItem>
                                        <asp:ListItem Text="Year to Date" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Previous month" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Calendar Period Range" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Next fiscal year" Value="6"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Button ID="btnCriteria2ResetCalenderPeriod" runat="server" Text="Date" OnClientClick="javascript:return ddlClick(document.getElementById(drp2).id)" style="font-weight: bold;border-color: cornflowerblue;display:none;" />
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpCriteria3" runat="server" AutoPostBack="true" style="float:left;height: 20px;" OnSelectedIndexChanged="drpCriteria3_SelectedIndexChanged" onChange="javascript:return ddlClick(this.id)">
                                         <asp:ListItem Text="--Select--" Value="0" Selected="True"></asp:ListItem>
                                         <asp:ListItem Text="This fiscal year" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Last fiscal year" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Year to Date" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Previous month" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Calendar Period Range" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Next fiscal year" Value="6"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Button ID="btnCriteria3ResetCalenderPeriod" runat="server" Text="Date" OnClientClick="javascript:return ddlClick(document.getElementById(drp3).id)" style="font-weight: bold;border-color: cornflowerblue;display:none;"/>
                                </td>
                            </tr>
                        </table>
                        <table style="width:100%;">
                            <tr>
                                <td>
                                    <div style="width: 98%; overflow: scroll; align-content: center;">
                                        <asp:GridView ID="grdBrokerReport" Width="100%" runat="server" Visible="false" CssClass="gv" font-family="Verdana" Font-Size="12px" AutoGenerateColumns="false" OnRowDataBound="grdBrokerReport_RowDataBound">
                                            <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                                            <HeaderStyle CssClass="HeaderGridView2" />
                                            <RowStyle CssClass="RowGridView2" />
                                            <SelectedRowStyle CssClass="SelectedRowGridView2" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table style="width: 100%;text-align: left;" border="0">
                                                            <tr>
                                                                <td style="max-width:34%;width:34%;">
                                                                    <asp:Label ID="lblCriteria1" runat="server" ></asp:Label>
                                                                </td>
                                                                <td style="max-width:33%;width:33%;">
                                                                    <asp:Label ID="lblCriteria2" runat="server" ></asp:Label>
                                                                </td>
                                                                <td style="max-width:33%;width:33%;">
                                                                    <asp:Label ID="lblCriteria3" runat="server" ></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table border="0">
                                                            <tr>
                                                                <td colspan="3"><b>Broker Name:</b>
                                                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("BrokerName")%>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="max-width:34%;width:34%;">
                                                                    <table class="classCriterai1" runat="server" id="classCriterai1" border="0">
                                                                        <tr>
                                                                            <td><b>Gross Total Collected:</b></td>
                                                                            <td>$<asp:Label ID="lblGrossTotalCollected" runat="server" Text='<%# String.Format("{0:f2}",Eval("GrossTotalCollected1"))%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Collected Broker Share Total (Net):</b></td>
                                                                            <td>$<asp:Label ID="Label1" runat="server" Text='<%# String.Format("{0:f2}",Eval("CollectedBrokerShareNet1"))%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Company Share Total:</b></td>
                                                                            <td>$<asp:Label ID="Label2" runat="server" Text='<%# String.Format("{0:f2}",Eval("CompanyShareTotal1"))%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Number of Deals Brokered:</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("NoOfChargeslip1")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Number of Landlord Deals Brokered:</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label12" runat="server" Text='<%# Eval("NoOfLandlordChargeslip1")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Number of Tenant Deals Brokered:</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label13" runat="server" Text='<%# Eval("NoOfTenantChargeslip1")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Total Landlord Square Footage Leased:</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label18" runat="server" Text='<%# Eval("SqFTLeased1")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="max-width:33%;width:33%;">
                                                                    <table class="classCriterai2" runat="server" id="classCriterai2"  border="0">
                                                                        <tr>
                                                                            <td><b>Gross Total Collected:</b></td>
                                                                            <td>$<asp:Label ID="Label4" runat="server" Text='<%# String.Format("{0:f2}",Eval("GrossTotalCollected2"))%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Collected Broker Share Total (Net):</b></td>
                                                                            <td>$<asp:Label ID="Label5" runat="server" Text='<%# String.Format("{0:f2}",Eval("CollectedBrokerShareNet2"))%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Company Share Total:</b></td>
                                                                            <td>$<asp:Label ID="Label6" runat="server" Text='<%# String.Format("{0:f2}",Eval("CompanyShareTotal2"))%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Number of Deals Brokered:</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label7" runat="server" Text='<%# Eval("NoOfChargeslip2")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Number of Landlord Deals Brokered:</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label14" runat="server" Text='<%# Eval("NoOfLandlordChargeslip2")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Number of Tenant Deals Brokered:</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label15" runat="server" Text='<%# Eval("NoOfTenantChargeslip2")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                         <tr>
                                                                            <td><b>Total Landlord Square Footage Leased:</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label19" runat="server" Text='<%# Eval("SqFTLeased2")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="max-width:33%;width:33%;">
                                                                    <table class="classCriterai3" runat="server" id="classCriterai3"  border="0">
                                                                        <tr>
                                                                            <td><b>Gross Total Collected:</b></td>
                                                                            <td>$<asp:Label ID="Label8" runat="server" Text='<%# String.Format("{0:f2}",Eval("GrossTotalCollected3"))%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Collected Broker Share Total (Net):</b></td>
                                                                            <td>$<asp:Label ID="Label9" runat="server" Text='<%# String.Format("{0:f2}",Eval("CollectedBrokerShareNet3"))%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Company Share Total:</b></td>
                                                                            <td>$<asp:Label ID="Label10" runat="server" Text='<%# String.Format("{0:f2}",Eval("CompanyShareTotal3"))%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Number of Deals Brokered:</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label11" runat="server" Text='<%# Eval("NoOfChargeslip3")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Number of Landlord Deals Brokered:</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label16" runat="server" Text='<%# Eval("NoOfLandlordChargeslip3")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Number of Tenant Deals Brokered:</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label17" runat="server" Text='<%# Eval("NoOfTenantChargeslip3")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                         <tr>
                                                                            <td><b>Total Landlord Square Footage Leased:</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label20" runat="server" Text='<%# Eval("SqFTLeased3")%>'></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>


                    <rsweb:ReportViewer ID="rptReport" runat="server" Font-Names="Verdana" Font-Size="8pt" Style="margin-left: 0px; margin-top: 0px; width: 98%;" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" HyperlinkTarget="_blank" Height="100%" Width="99%">
                        <LocalReport ReportPath="Reports\TotalValueSales.rdlc" EnableHyperlinks="true">
                            <DataSources>
                                <rsweb:ReportDataSource DataSourceId="ObjectDataDeleted" Name="DataSet1" />
                            </DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>



                    <asp:ObjectDataSource ID="ObjectDataDeleted" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="DeletedChargeSlipsTableAdapters.rpt_DeletedChargeslipsTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DueDateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DueDateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>



                    <asp:ObjectDataSource ID="ObjectDataOption" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="OptionDueTableAdapters.rpt_OptionsDueTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>

                    <asp:ObjectDataSource ID="ObjectDataOwner" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="TenantOwnerTableAdapters.rpt_OwnersTableAdapter">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlOwner" Name="CompanyID" PropertyName="SelectedValue" Type="Int64" />
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DueDateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DueDateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataTenants" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="TenantOwnerTableAdapters.rpt_TenantsTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlTenant" Name="CompanyID" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DueDateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DueDateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>

                    <asp:ObjectDataSource ID="ObjectDataPaidInvoice" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="PaidInvoiceTableAdapters.rpt_PaidInvoiceTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter DefaultValue="null" Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataPaidInvoice_SubReport" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="PaidInvoiceTableAdapters.rpt_PaidInvoice_SubReportTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter DefaultValue="null" Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DateTo" PropertyName="Text" Type="DateTime" />
                            <asp:SessionParameter DefaultValue="null" Name="CommissionDueId" SessionField="CommissionDueId" Type="Int64" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataCreditChargeSlip" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="CreditedChargeSlipsTableAdapters.rpt_PaidCreditAmountTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter DefaultValue="null" Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataLandlord" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="TenantOwnerTableAdapters.rpt_OwnersTableAdapter">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ddlLandLord" Name="CompanyID" PropertyName="SelectedValue" Type="Int64" />
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DueDateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DueDateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataTotalValueSales" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="TotalValuesSalesTableAdapters.rpt_TotalValueSalesTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:Parameter Name="companyId" Type="Int32" />
                            <asp:ControlParameter ControlID="ddlLocation" Name="LocationId" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="txtLocation" Name="Address" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataOptionsContingent" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="OptionContingentTableAdapters.rpt_OptionsContingentReceivablesTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataGrossReceivables" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="GrossReceivablesTableAdapters.rpt_GrossRecievablesTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataGeographic" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="GeographicTableAdapters.rpt_GeographicReportTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtCity" Name="City" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="txtCounty" Name="County" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="ddlState" Name="State" PropertyName="SelectedValue" Type="String" />
                            <asp:ControlParameter ControlID="txtZipCode" Name="ZipCode" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataOutsideBroker" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="OutsideBrokerTableAdapters.rpt_OutsideBrokerTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataInHouseBroker" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="InHouseBrokerTableAdapters.rpt_InHouseCoBrokerTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataSquareFootLease" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="SqaureFootLeaseTableAdapters.rpt_SquareFootageLeaseTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlTenant" Name="TenantId" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="ddlOwner" Name="companyId" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataInvoice" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="InvoiceTableAdapters.rpt_PaidInvoiceTableAdapter">
                        <SelectParameters>
                            <asp:Parameter Name="UserId" Type="Int64" />
                            <asp:Parameter Name="DueDateFrom" Type="DateTime" />
                            <asp:Parameter Name="DueDateTo" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataTotalValueLease" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="TotalValueInvoiceTableAdapters.rpt_TotalValueLeaseTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlOwner" Name="companyId" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="ddlLocation" Name="LocationId" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="txtvalue1" Name="ValueFrom" PropertyName="Text" Type="Int32" />
                            <asp:ControlParameter ControlID="txtvalue2" Name="ValueTo" PropertyName="Text" Type="Int32" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataBrokerTrend" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="BrokerTrendTableAdapters.rpt_GetListBrokerTrendReportTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:SessionParameter Name="BrokerId" SessionField="MultipleBrokerId" Type="String" />
                            <%--    <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="String" />--%>
                            <asp:ControlParameter ControlID="txtFrom" Name="DateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataDealDesignationTypeList" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="DealDesignationTypeListTableAdapters.rpt_GetListDesignationTypeTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlDesignationType" Name="DealDesignationTypeID" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DueDateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DueDateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="ObjectDataCLBBrokerDeals" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="CLBBrokerDealsTableAdapters.rpt_GetCLBBrokerDealsTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="UserId" SessionField="UserId" Type="Int64" />                            
                            <asp:ControlParameter ControlID="ddlBroker" Name="BrokerId" PropertyName="SelectedValue" Type="Int64" />
                            <asp:ControlParameter ControlID="txtFrom" Name="DueDateFrom" PropertyName="Text" Type="DateTime" />
                            <asp:ControlParameter ControlID="txtTo" Name="DueDateTo" PropertyName="Text" Type="DateTime" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <br />
                </div>
            </td>

        </tr>

    </table>
    <ajax:ModalPopupExtender ID="modalCalendarDateRange" runat="server"
        PopupControlID="divCalendarDateRange" BackgroundCssClass="modalBackground" TargetControlID="lnbPopUp1" CancelControlID="imgbtnClose">
    </ajax:ModalPopupExtender>
    <asp:LinkButton ID="lnbPopUp1" runat="server"></asp:LinkButton>
    <div id="divCalendarDateRange" class="PopupDivBodyStyle" runat="server" style="width: 350px; height: 300px; overflow-y: auto; background-color: white; z-index: 111; position: absolute; display: none;">
        <table style="width: 100%" class="spacing">
            <tr style="background-color: gray; border-bottom: solid;">
                <td colspan="2">
                    <table>
                        <tr>
                            <td style="text-align: left; width: 490px;">
                                <asp:Label ID="lblHeader" runat="server" Text="CALENDAR DATE RANGE" ForeColor="Black" Style="padding-left: 10px; vertical-align: middle; font-weight: bold;"></asp:Label>
                            </td>
                            <td style="text-align: right;">
                                <asp:ImageButton ID="imgbtnClose" runat="server" ImageUrl="~/Images/Close.png" AlternateText="Close"
                                    ForeColor="Red" ImageAlign="Right" Style="margin-top: 4px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="Paddingtd">
                    <asp:Label ID="lblErrorMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                </td>
            </tr>

            <tr id="trCriteria1From">
                <td class="Paddingtd">Date From:
                </td>
                <td>
                    <asp:TextBox ID="txtCritera1From" runat="server" Width="120px"></asp:TextBox>
                    <ajax:CalendarExtender ID="CalendarExtender3" TargetControlID="txtCritera1From" runat="server" />
                </td>
            </tr>
            <tr id="trCriteria1To">
                <td class="Paddingtd ">Date To:
                </td>
                <td>
                    <asp:TextBox ID="txtCritera1To" runat="server" Width="120px"></asp:TextBox>
                    <ajax:CalendarExtender ID="CalendarExtender4" TargetControlID="txtCritera1To" runat="server" />
                </td>
            </tr>
            <tr id="trCriteria2From">
                <td class="Paddingtd">Date From:
                </td>
                <td>
                    <asp:TextBox ID="txtCritera2From" runat="server" Width="120px"></asp:TextBox>
                    <ajax:CalendarExtender ID="CalendarExtender5" TargetControlID="txtCritera2From" runat="server" />
                </td>
            </tr>
            <tr id="trCriteria2To">
                <td class="Paddingtd">Date To:
                </td>
                <td>
                    <asp:TextBox ID="txtCritera2To" runat="server" Width="120px"></asp:TextBox>
                    <ajax:CalendarExtender ID="CalendarExtender6" TargetControlID="txtCritera2To" runat="server" />
                </td>
            </tr>
            <tr id="trCriteria3From">
                <td class="Paddingtd">Date From:
                </td>
                <td>
                    <asp:TextBox ID="txtCritera3From" runat="server" Width="120px"></asp:TextBox>
                    <ajax:CalendarExtender ID="CalendarExtender7" TargetControlID="txtCritera3From" runat="server" />
                </td>
            </tr>
            <tr id="trCriteria3To">
                <td class="Paddingtd">Date To:
                </td>
                <td>
                    <asp:TextBox ID="txtCritera3To" runat="server" Width="120px"></asp:TextBox>
                    <ajax:CalendarExtender ID="CalendarExtender8" TargetControlID="txtCritera3To" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: center; padding-right: 30px;" colspan="2">
                    <asp:Button ID="btnFilterDateRange" runat="server" Text="Search" CssClass="SqureButton" Width="100px" OnClick="btnFilterDateRange_Click" OnClientClick="return ValidateControl();" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
