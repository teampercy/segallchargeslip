﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true" CodeFile="NewCharge.aspx.cs" Inherits="Pages_NewCharge" %>

<%--< %@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>--%>
<%@ Register Src="~/UserControl/ucAddNewShoppingCenterOrCompany.ascx" TagName="AddNewShoppingCenterOrCompany" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucSaleTransactionImprovedLand.ascx" TagName="SaleTranseImprovedLand" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucSaleTransactionUnimmprovedLand.ascx" TagName="SaleTranseUnImprovedLand" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucLeaseTransactionGround.ascx" TagName="LeaseTransactionGround" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucLeaseTransactionBuilding.ascx" TagName="LeaseTransactionBuilding" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucConsultingTransaction.ascx" TagName="ConsultingTransaction" TagPrefix="uc" %>
<%@ Register Src="~/UserControl/ucMapShoppingLocation.ascx" TagName="ShoppingMapLocation" TagPrefix="uc" %>
<asp:Content ID="contHeadCont" ContentPlaceHolderID="cntHeadTag" runat="server">
    <script src="../Js/ChargeSlipValidation.js"></script>
    <%--<script src="../Js/jquery.watermark.min.js"></script>--%>

    
    <script type="text/javascript">
        function Validate(event) {
            var regex = new RegExp("^[0-9 \-]+$");
            var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        }
        //window.onload = LoadWaterMark();

        //$(document).ready(function () {

        //    // $.watermarker.setDefaults({ color: '#000', left: 8 });
        //    $('< %= txtCTOwnershipEntity.ClientID %>').watermark();


        //    alert('asdf');
        //    //$('#txtCTOwnershipEntity , #ContentMain_txtCTName, #ContentMain_txtComapny, #ContentMain_txtCTZipCode, #txtCTCity, #ContentMain_txtCTAddress2, #ContentMain_txtCTEmail, #ContentMain_txtCTAddress1 , #ContentMain_txtLocZipCode').addClass('jq_watermark');
        //    //$('#ContentMain_txtLocCounty,#ContentMain_txtLocCity ,#ContentMain_txtLocAddress2,#ContentMain_txtlocSHPaddress1').addClass('jq_watermark');

        //    // applyWatermark();

        //    // $('#ContentMain_txtLocAddress2').watermark();
        //    // $('#txtCTAddress2').watermark();

        //    // alert('No error');

        //});

        //alert(Sys.Application);
        //Sys.Application.add_load(applyWatermark);

        //function applyWatermark() {
        //      console.log("apply watermark manually");
        //    debugger;
        //    alert("apply watermark manually");
        //    $('#txtCTOwnershipEntity , #txtCTName, #txtComapny, #txtCTZipCode, #txtCTCity, #txtCTAddress2, #txtCTEmail, #txtCTAddress1 , #txtLocZipCode').watermark();
        //    $('#txtLocCounty,#txtLocCity ,#txtLocAddress2,#txtlocSHPaddress1').watermark();
        //      $.watermark.show('input'); 
        //}


        //function pageLoad() {
        //    //$('#ContentMain_txtCTOwnershipEntity , #ContentMain_txtCTName, #ContentMain_txtComapny, #ContentMain_txtCTZipCode, #txtCTCity, #ContentMain_txtCTAddress2, #ContentMain_txtCTEmail, #ContentMain_txtCTAddress1 , #ContentMain_txtLocZipCode').addClass(' ');
        //    //$('#ContentMain_txtLocCounty,#ContentMain_txtLocCity ,#ContentMain_txtLocAddress2,#ContentMain_txtlocSHPaddress1').addClass('jq_watermark');

        //    //debugger;
        //    $.Watermarker.showAll();
        //    alert('pageload');
        //}

    </script>



    <style>
        .radio-toolbar input[type="radio"] {
            display: none;
        }

        .radio-toolbar input[type="radio"] {
            display: none;
        }

        .radio-toolbar label {
            display: inline-block;
            background-color: #ddd;
            padding: 4px 11px;
            font-family: Arial;
            font-size: 15px;
            font-weight: 500;
        }

        .radio-toolbar input[type="radio"]:checked + label {
            background-color: #F98620;
        }
    </style>
    <style type="text/css">
        .loadingNewCharge {
            /*width: 230px; JAY*/
            width: 250px;
        }

        .textwidthNewCharge {
            width: 250px;
        }

        .loadingUC {
            width: 130px;
        }

        .textwidthUC {
            width: 150px;
        }

        .watermark {
            color: Gray;
        }

        .blueWatermark {
            color: Blue;
        }

        .input {
            width: 200px;
            border: solid 1px #383838;
            font-size: 13px;
            padding: 2px;
        }

        .Title {
            color: #383838;
            font-size: 20px;
            font-weight: bold;
            text-decoration: underline;
        }
    </style>
    <style type="text/css">
        .Marbottom3 {
            margin-bottom: 8px !important;
        }
    </style>


    <%--<script src="../Js/test.js"></script>--%>

    <script type="text/javascript">
        $(document).ready(function () {
            $('body').on('keydown', 'input, select, textarea', function (e) {
                var self = $(this)
                  , form = self.parents('form:eq(0)')
                  , focusable
                  , next
                ;
                if (e.keyCode == 13) {
                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {
                        form.submit();
                    }
                    return false;
                }
            });


            //    $('[placeholder]').focus(function () {
            //        var input = $(this);
            //        if (input.val() == input.attr('placeholder')) {
            //            input.val('');
            //            input.removeClass('placeholder');
            //        }
            //    }).blur(function () {
            //        var input = $(this);
            //        if (input.val() == "''" || input.val() == input.attr('placeholder')) {
            //            input.addClass('placeholder');
            //            input.val(input.attr('placeholder'));
            //        }
            //    }).blur();
            //});

            //    $('[placeholder]').focus(function () {
            //        //alert('PlaceHolder');
            //        //debugger;
            //        var input = $(this);
            //        if (input.val() == input.attr('placeholder')) {
            //            input.val('');
            //            input.removeClass('placeholder');
            //        }
            //    }).blur(function () {
            //        var input = $(this);
            //        if (input.val() == '' || input.val() == input.attr('placeholder')) {
            //            input.addClass('placeholder');
            //            input.val(input.attr('placeholder'));
            //        }
            //        }).blur().parents('form').submit(function () 
            //        {                       /* $(this).find('[placeholder]').each(function () {
            //            var input = $(this);
            //            if (input.val() == input.attr('placeholder')) {
            //                input.val('');
            //            }
            //        }*/
            //            var i = 0;
            //            }
            //        )
            //    });
            //});

            //alert("watermark");
            //$('< %= txtSearchLocShoppingCenter.ClientID %>').watermark({ watermarkText: "Shopping center ####" });
            //$('< %= txtlocSHPaddress1.ClientID %>').watermark({ watermarkText: "Location address center ####" });

            //$('# < %= txtSearchLocShoppingCenter.ClientID %>').addClass('jq_watermark');
            //$('#< % = txtlocSHPaddress1.ClientID %>').addClass('jq_watermark');

            //$.watermarker.setDefaults({ color: '#f00', left: 8 });
            //$('input[type=text]').watermark();

            ////$('#txtSearchLocShoppingCenter , #txtlocSHPaddress1').addClass('jq_watermark');

            //$("#setValueButton").click(function () {
            //    alert('set value btn');
            //    $('input[type=text]').watermark();
            //});

        });

    </script>
    <script type="text/javascript">
        function SetPageLeave() {

            document.getElementById('<%= hdnPageLeave.ClientID %>').value = "1";

        }
        function validateZipLen(strzip) {
            var strziptrm = $.trim(strzip);
            if (strziptrm.length < 5 || strziptrm.length > 10) {
                return false;
            }
            return true;
        }
        function setIframPath() {
            debugger
            var strLocation = "";
            var txtMapLocation = document.getElementById('<%= hdnMapLatLon.ClientID %>');
           // alert(txtMapLocation.value);
            if (txtMapLocation.value != "") {

                //alert(txtMapLocation.value);
                strLocation = txtMapLocation.value;
            }
            else { //strLocation = $('<%= hdnMapLatLon.ClientID %>').val(); 
                alert("Please select shopping Location first");
                return false;
            }

            document.getElementById('<%= frmaploc.ClientID %>').src = "MapshoppingLoc.aspx?location=" + strLocation + "";//+ "washington dc'";
            return true;

            // = "MapshoppingLoc.aspx?id='washington dc'";
        }


        //JAY
        function callfromIfram(lat, lon) {
            //alert("this is called from parent" + lat + "__" + lon);
            ShoppingCenterAddressChanged();
            document.getElementById('<%=txtMapLocation.ClientID %>').value = " Lat: " + lat + " Lon: " + lon;
            document.getElementById('<%=hdnMapLatLon.ClientID %>').value = lat + "|" + lon;
            // alert("this is called from parent" + document.getElementById('<%=txtMapLocation.ClientID %>'), value + "__" + document.getElementById('<%=hdnMapLatLon.ClientID %>'), value);


            document.getElementById('<%=btnCancelMap.ClientID %>').click();

        }


        function updatemapaddress() {
            debugger;
            //Added by Jaywanti on 26-08-2015
            <%--if (document.getElementById('<%= txtSearchLocShoppingCenter.ClientID %>').value == "SHOPPING CENTER..")
            {
                document.getElementById('<%= txtSearchLocShoppingCenter.ClientID %>').value = "";
            }
            if (document.getElementById('<%= txtlocSHPaddress1.ClientID %>').value == "ADDRESS LINE 1..") {
                document.getElementById('<%= txtlocSHPaddress1.ClientID %>').value = "";
            }
            if (document.getElementById('<%= txtLocAddress1.ClientID %>').value == "ADDRESS LINE 1..") {
                document.getElementById('<%= txtLocAddress1.ClientID %>').value = "";
            }
            if (document.getElementById('<%= txtLocAddress2.ClientID %>').value == "ADDRESS LINE 2..") {
                document.getElementById('<%= txtLocAddress2.ClientID %>').value = "";
            }
            if (document.getElementById('<%= txtLocCity.ClientID %>').value == "CITY..") {
                document.getElementById('<%= txtLocCity.ClientID %>').value = "";
            }
            if (document.getElementById('<%= txtLocCounty.ClientID %>').value == "COUNTY..") {
                document.getElementById('<%= txtLocCounty.ClientID %>').value = "";
            }
            if (document.getElementById('<%= txtLocZipCode.ClientID %>').value == "ZIP CODE..") {
                document.getElementById('<%= txtLocZipCode.ClientID %>').value = "";
            }--%>
           
            var SearchLocShoppingCenter, locSHPaddress1, LocAddress1, LocAddress2, LocCity, LocCounty, LocZipCode;
            if (document.getElementById('<%= txtSearchLocShoppingCenter.ClientID %>').value == "SHOPPING CENTER..")
            {
                SearchLocShoppingCenter = "";
            }
            else
            {
                SearchLocShoppingCenter = document.getElementById('<%= txtSearchLocShoppingCenter.ClientID %>').value;
            }
            if (document.getElementById('<%= txtlocSHPaddress1.ClientID %>').value == "ADDRESS LINE 1..") {
                locSHPaddress1 = "";
            }
            else
            {
                locSHPaddress1 = document.getElementById('<%= txtlocSHPaddress1.ClientID %>').value;
            }

            if (document.getElementById('<%= txtLocAddress1.ClientID %>').value == "ADDRESS LINE 1..") {
                LocAddress1 = "";
            }
            else
            {
                LocAddress1 = document.getElementById('<%= txtLocAddress1.ClientID %>').value;
            }

            if (document.getElementById('<%= txtLocAddress2.ClientID %>').value == "ADDRESS LINE 2..") {
                LocAddress2 = "";
            }
            else
            {
                LocAddress2 = document.getElementById('<%= txtLocAddress2.ClientID %>').value;
            }

            if (document.getElementById('<%= txtLocCity.ClientID %>').value == "CITY..") {
                LocCity = "";
            }
            else
            {
                LocCity = document.getElementById('<%= txtLocCity.ClientID %>').value;
            }

            if (document.getElementById('<%= txtLocCounty.ClientID %>').value == "COUNTY..") {
                LocCounty = "";
            }
            else {
                LocCounty = document.getElementById('<%= txtLocCounty.ClientID %>').value;
            }

            if (document.getElementById('<%= txtLocZipCode.ClientID %>').value == "ZIP CODE..") {
                LocZipCode = "";
            }
            else
            {
                LocZipCode = document.getElementById('<%= txtLocZipCode.ClientID %>').value;
            }

            var rblSelectedValue = $("#<%= rdbLocation.ClientID %> input:checked");
            if (rblSelectedValue.val() == "0") {
                <%--var Newaddress = document.getElementById('<%= txtSearchLocShoppingCenter.ClientID %>').value + " " + document.getElementById('<%= txtlocSHPaddress1.ClientID %>').value + " " + document.getElementById('<%= txtLocAddress2.ClientID %>').value + " " + document.getElementById('<%= txtLocCity.ClientID %>').value + " " +
                   document.getElementById('<%= txtLocCounty.ClientID %>').value + " " + document.getElementById('<%= ddlLocState.ClientID %>').value + " " + document.getElementById('<%= txtLocZipCode.ClientID %>').value;--%>
                var Newaddress = SearchLocShoppingCenter + " " + locSHPaddress1 + " " + LocAddress2 + " " + LocCity + " " + LocCounty + " " + document.getElementById('<%= ddlLocState.ClientID %>').value + " " + LocZipCode;
            }
            else {
                <%--var Newaddress = document.getElementById('<%= txtLocAddress1.ClientID %>').value + " " + document.getElementById('<%= txtLocAddress2.ClientID %>').value + " " + document.getElementById('<%= txtLocCity.ClientID %>').value + " " +
                 document.getElementById('<%= txtLocCounty.ClientID %>').value + " " + document.getElementById('<%= ddlLocState.ClientID %>').value + " " + document.getElementById('<%= txtLocZipCode.ClientID %>').value;--%>
                var Newaddress = LocAddress1 + " " + LocAddress2 + " " + LocCity + " " + LocCounty + " " + document.getElementById('<%= ddlLocState.ClientID %>').value + " " + LocZipCode;
            }
            //alert('udpatre');          

            //alert(Newaddress);
            document.getElementById('<%= hdnMapLatLon.ClientID %>').value = Newaddress;

        }

    </script>
    <script type="text/javascript">
        window.onbeforeunload = confirmExit;

        function confirmExit() {
            if (document.getElementById('<%= hdnPageLeave.ClientID %>').value != "1") {
                return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
            }
            else {
            }
        }


        function PageLeaveCheck(SaveStatus) {
            document.getElementById('<%= hdnPageLeave.ClientID %>').value = "1";
            document.getElementById('<%= hdnSaveStatus.ClientID %>').value = SaveStatus;
            //Clearwatermark();
            if (ValidateCS(SaveStatus) == false) {
                return false;
            }
            else {
             
                Clearwatermark();
            }

            return true;
        }

        function Clearwatermark() {
            hideWaterText('shoppingcenter');
            hideWaterText('txtlocSHPaddress1');
            hideWaterText('txtLocAddress2');
            hideWaterText('locaddr2');
            hideWaterText('txtCTOwnershipEntity');
            hideWaterText('txtCTEmail');
            hideWaterText('txtCTAddress2');
            hideWaterText('txtLocCounty');
            hideWaterText('txtCTName');
            hideWaterText('txtComapny');
        }

        function ClearControls() {
            var Parent = "ContentMain_ucNewCenterOrCompany_";
            $("#" + Parent + "txtLandlordName").val('');
            // $("#ContentMain_ucNewCenterOrCompany_lbErrorMsg").text('');
            $("#" + Parent + "txtName").val('');
            $("#" + Parent + "txtEmail").val('');
            $("#" + Parent + "txtAddress1").val('');
            $("#" + Parent + "txtAddress2").val('');
            $("#" + Parent + "txtCity").val('');
            $("#" + Parent + "txtCountry").val('');
            $("#" + Parent + "txtZipCode").val('');
            $("#" + Parent + "txtZipCode").val('');
            $('#' + Parent + 'lbErrorMsg').text("");
            //   applyWatermark();
            // $("#" + Parent + "lbErrorMsg").text('');
            // $(Parent + "_lbErrorMsg").text('');
            return true;
        }

        function hideWaterText(name) {
            debugger;
            
            var txtbx;
            var txtbxValue;
            if (name == 'shoppingcenter') {
                txtbx = $("#<%= txtSearchLocShoppingCenter.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'SHOPPING CENTER..') {
                    txtbx.val('');
                }
            }
            else if (name == 'txtlocSHPaddress1') {
                txtbx = $("#<%= txtlocSHPaddress1.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'ADDRESS LINE 1..') {
                    txtbx.val('');
                }
            }

            else if (name == 'locaddr1') {
                txtbx = $("#<%= txtLocAddress1.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'ADDRESS LINE 1..') {
                    txtbx.val('');
                }
            }
            else if (name == 'locaddr2') {
                txtbx = $("#<%= txtLocAddress2.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'ADDRESS LINE 2..') {
                    txtbx.val('');
                }
            }
            else if (name == 'txtLocCity') {
                txtbx = $("#<%= txtLocCity.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'CITY..') {
                    txtbx.val('');
                }
            }
            else if (name == 'txtLocCounty') {
                txtbx = $("#<%= txtLocCounty.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'COUNTY..') {
                    txtbx.val('');
                }
            }
            else if (name == 'txtLocZipCode') {
                txtbx = $("#<%= txtLocZipCode.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'ZIP CODE..') {
                    txtbx.val('');
                }
            }
            else if (name == 'txtCTOwnershipEntity') {
                txtbx = $("#<%= txtCTOwnershipEntity.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'OWNERSHIP ENTITY NAME...') {
                    txtbx.val('');
                }
            }
            else if (name == 'txtCTName') {
                txtbx = $("#<%= txtCTName.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'LANDLORD TRADE NAME..') {
                    txtbx.val('');
                }
            }
            else if (name == 'txtComapny') {
                txtbx = $("#<%= txtComapny.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'NAME..') {
                    txtbx.val('');
                }
            }
            else if (name == 'txtCTEmail') {
                txtbx = $("#<%= txtCTEmail.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'EMAIL...') {
                    txtbx.val('');
                }
            }
            else if (name == 'txtCTAddress1') {
                txtbx = $("#<%= txtCTAddress1.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'ADDRESS LINE 1..') {
                    txtbx.val('');
                }
            }

            else if (name == 'txtCTAddress2') {
                txtbx = $("#<%= txtCTAddress2.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'ADDRESS LINE 2..') {
                    txtbx.val('');
                }
            }
            else if (name == 'txtCTCity') {
                txtbx = $("#<%= txtCTCity.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue.toUpperCase() == 'CITY..') {
                    txtbx.val('');
                }
            }
            else if (name == 'txtCTZipCode') {
                txtbx = $("#<%= txtCTZipCode.ClientID %>");
                    txtbxValue = $.trim(txtbx.val());
                    if (txtbxValue.toUpperCase() == 'ZIP CODE..') {
                        txtbx.val('');
                    }
                }

            //else if (name == 'company') {
            //    var txtbx = document.getElementById('< %= txtCompany.ClientID %>');
            //    txtbx.value = Trim(txtbx.value);
            //    if (txtbx.value.toLowerCase() == 'company') {
            //        txtbx.value = '';
            //    }
            //}
            //else if (name == 'city') {
            //    var txtbx = document.getElementById('< %= txtCity.ClientID %>');
            //    txtbx.value = Trim(txtbx.value);
            //    if (txtbx.value.toLowerCase() == 'city') {
            //        txtbx.value = '';
            //    }
            //}


}
        function CheckCompany(name)
        {
            debugger;
            if (name == 'txtCTName') {
                txtbx = $("#<%= txtCTName.ClientID %>");
                txtbxValue = $.trim(txtbx.val());
                if (txtbxValue == '') {
                    $("#<%= hdnCompId.ClientID %>").val('');
                    
                }
                showWaterText(name)
            }
        }
        function showWaterText(name) {
            debugger;
    var txtbx;
    var txtbxValue;
    if (name == 'shoppingcenter') {
        txtbx = $("#<%= txtSearchLocShoppingCenter.ClientID %>");
        txtbxValue = $.trim(txtbx.val());
        if (txtbxValue == '') {
            txtbx.val('SHOPPING CENTER..');
        }
    }
    else if (name == 'txtlocSHPaddress1') {
        txtbx = $("#<%= txtlocSHPaddress1.ClientID %>");
        txtbxValue = $.trim(txtbx.val());
        if (txtbxValue == '') {
            txtbx.val('ADDRESS LINE 1..');
        }
    }
    else if (name == 'locaddr1') {
        txtbx = $("#<%= txtLocAddress1.ClientID %>");
        txtbxValue = $.trim(txtbx.val());
        if (txtbxValue == '') {
            txtbx.val('ADDRESS LINE 1..');
        }

    }
    else if (name == 'locaddr2') {
        txtbx = $("#<%= txtLocAddress2.ClientID %>");
        txtbxValue = $.trim(txtbx.val());
        if (txtbxValue == '') {
            txtbx.val('ADDRESS LINE 2..');
        }
    }
    else if (name == 'txtLocCity') {
        txtbx = $("#<%= txtLocCity.ClientID %>");
        txtbxValue = $.trim(txtbx.val());
        if (txtbxValue == '') {
            txtbx.val('CITY..');
        }
    }
    else if (name == 'txtLocCounty') {
        txtbx = $("#<%= txtLocCounty.ClientID %>");
        txtbxValue = $.trim(txtbx.val());
        if (txtbxValue == '') {
            txtbx.val('COUNTY..');
        }
    }
    else if (name == 'txtLocZipCode') {
        txtbx = $("#<%= txtLocZipCode.ClientID %>");
        txtbxValue = $.trim(txtbx.val());
        if (txtbxValue == '') {
            txtbx.val('ZIP CODE..');
        }
    }
    else if (name == 'txtCTOwnershipEntity') {
        txtbx = $("#<%= txtCTOwnershipEntity.ClientID %>");
        txtbxValue = $.trim(txtbx.val());
        if (txtbxValue == '') {
            txtbx.val('OWNERSHIP ENTITY NAME...');
        }
    }
    else if (name == 'txtCTName') {
        txtbx = $("#<%= txtCTName.ClientID %>");
                    txtbxValue = $.trim(txtbx.val());
                    if (txtbxValue == '') {
                        txtbx.val('LANDLORD TRADE NAME..');
                    }
                }

                else if (name == 'txtComapny') {
                    txtbx = $("#<%= txtComapny.ClientID %>");
                    txtbxValue = $.trim(txtbx.val());
                    if (txtbxValue == '') {
                        txtbx.val('NAME..');
                    }
                }
                else if (name == 'txtCTEmail') {
                    txtbx = $("#<%= txtCTEmail.ClientID %>");
                    txtbxValue = $.trim(txtbx.val());
                    if (txtbxValue == '') {
                        txtbx.val('EMAIL...');
                    }
                }

                else if (name == 'txtCTAddress1') {
                    txtbx = $("#<%= txtCTAddress1.ClientID %>");
                    txtbxValue = $.trim(txtbx.val());
                    if (txtbxValue == '') {
                        txtbx.val('ADDRESS LINE 1..');
                    }
                }
                else if (name == 'txtCTAddress2') {
                    txtbx = $("#<%= txtCTAddress2.ClientID %>");
                    txtbxValue = $.trim(txtbx.val());
                    if (txtbxValue == '') {
                        txtbx.val('ADDRESS LINE 2..');
                    }
                }
                else if (name == 'txtCTCity') {
                    txtbx = $("#<%= txtCTCity.ClientID %>");
                    txtbxValue = $.trim(txtbx.val());
                    if (txtbxValue == '') {
                        txtbx.val('CITY..');
                    }
                }
                else if (name == 'txtCTZipCode') {
                    txtbx = $("#<%= txtCTZipCode.ClientID %>");
                        txtbxValue = $.trim(txtbx.val());
                        if (txtbxValue == '') {
                            txtbx.val('ZIP CODE..');
                        }
                    }
}
    </script>

    <%--    <script type="text/javascript">
        $(document).ready(function () {
            $("#ContentFooter_btnNext").click(function () {
               
                if ($("#ContentMain_txtDealName").val() == "") {
                    alert("Please Enter Deal Name");
                    $("#ContentMain_txtDealName").focus();
                    return false;
                }
                if ($("#ContentMain_ajaxcombLocShoppingCenter_TextBox").val() == "- Select -") {
                    alert("Please Select Shopping Center");
                    $("#ContentMain_ajaxcombLocShoppingCenter_TextBox").focus();
                    return false;
                }

                return false;
            });
        });

        function AllowNumbersOnly1(CtrId) {
           
            var regExpD = /^[A-Za-z0-9 ]{3,20}$/;
            
            var testvar = "abcd";
            alert(testvar);
            alert(regExpD.test(testvar));
            alert(regExpD.test(document.getElementById("<%=txtLocZipCode.ClientID%>").value));
            if (!regExpD.test(document.getElementById("<%=txtLocZipCode.ClientID%>").value)) {
                document.getElementById(CtrId).value = "";
                return false;
            }
            else {
                return true;
            }
        }

    </script>--%>

    <script type="text/javascript">


        function ShowIconNC(sender, e) {
            document.getElementById('<%=hdnLocId.ClientID %>').value = "";
            sender._element.className = "loadingNewCharge";
        }
        function ShowIconNCOMP(sender, e) {
            document.getElementById('<%=hdnCompId.ClientID %>').value = "";
            sender._element.className = "loadingNewCharge";
        }
        function hideIconNC(sender, e) {
            sender._element.className = "textwidthNewCharge";
        }

        function OnCenterSelected(source, eventArgs) {

            document.getElementById('<%=hdnLocId.ClientID %>').value = eventArgs.get_value();
            document.getElementById('<%= hdnisAddresOrShopping.ClientID %>').value = "1";
            document.getElementById('<%=hndIsShoppingCenterAddressChanged.ClientID %>').value = "0";
            //document.getElementById('< %=hndIsShoppingCenterAddressChanged.ClientID %>').value = "1";
            //hdnLocId JAY//   alert(eventArgs.get_value());
            //alert(document.getElementById('< %=hdnLocId.ClientID %>').value);
            return false;
        }


        function OnAddrSelected(source, eventArgs) {
            document.getElementById('<%=hdnLocId.ClientID %>').value = eventArgs.get_value();
            document.getElementById('<%=hndIsShoppingCenterAddressChanged.ClientID %>').value = "1";
            document.getElementById('<%=txtSearchLocShoppingCenter.ClientID %>').value = "";
            document.getElementById('<%= hdnisAddresOrShopping.ClientID %>').value = "2";

            // alert(document.getElementById('<%= hdnisAddresOrShopping.ClientID %>').value);
            return false;
        }


        function OnCompanySelected(source, eventArgs) {
            //alert(eventArgs.get_value());
            document.getElementById('<%=hdnCompId.ClientID %>').value = eventArgs.get_value();
            //JAY //   alert(eventArgs.get_value());
            return false;
        }

        //--------------vv--------- SK 08/02 : to allow address change without the popup & should initiate process for new address save.
        function ShoppingCenterAddressChanged() {
            //alert('callled');
            document.getElementById('<%=hdnIsShoppingCenterAddressSaved.ClientID %>').value = "0";
            document.getElementById('<%=hndIsShoppingCenterAddressChanged.ClientID %>').value = "1";
            updatemapaddress();
            //alert( document.getElementById('< %=hdnIsShoppingCenterAddressSaved.ClientID %>').value +"_"+
            //         document.getElementById('< %=hndIsShoppingCenterAddressChanged.ClientID %>').value );
        }
        

        function CompanyAddressChanged() {
            document.getElementById('<%=hdnIsCompanySaved.ClientID %>').value = "0";
            document.getElementById('<%=hdnIsCompanyChanged.ClientID %>').value = "1";
            //alert(document.getElementById('< %=hdnIsCompanySaved.ClientID %>').value + "_" +
            //     document.getElementById('< %=hdnIsCompanyChanged.ClientID %>').value);
        }
        //--------------^^--------- SK 08/02 : allow address change without the popup, should initiate process for new address save.


        function toggleLocation(StrMain) {

            //alert(StrMain);
            // $("#rdbLocation").val();
            var rblSelectedValue = $("#<%= rdbLocation.ClientID %> input:checked");
            var PrevSelection = document.getElementById('<%=hdnisAddresOrShopping.ClientID %>');

            //var radioButtons = $("#masterForm input:radio[name='rdbLocation']");
            //var selectedIndex = radioButtons.index(radioButtons.filter(':checked'));

            if (StrMain != "1") {
                document.getElementById('<%=hdnLocId.ClientID %>').value = "";
            }

            if (PrevSelection.value == "0") {
                EnableLocaitonFielsds();
            }

            // alert(document.getElementById('<%=hdnLocId.ClientID %>').value);
            if (rblSelectedValue.val() == "1") {
                $("#trAddress").show();
                $("#trshopping").hide();
                //var txtbx = $("#< %= txtSearchLocShoppingCenter.ClientID %>");
                //var txtbx1 = $("#< %= txtlocSHPaddress1.ClientID %>");
                //txtbx.val('');
                //txtbx1.val('');
                //showWaterText('shoppingcenter');
                //showWaterText('txtlocSHPaddress1');

                //document.getElementById('< %=txtlocSHPaddress1.ClientID %>').value = "";
                //document.getElementById('< %=txtSearchLocShoppingCenter.ClientID %>').value = "";
            }

            else {
                //var txtbx2 = $("#< %= txtLocAddress1.ClientID %>");
                //txtbx2.val('');
                //showWaterText('locaddr1');

                $("#trshopping").show();
                $("#trAddress").hide();
                $("#txtLocAddress1").val();
                //document.getElementById('< % =txtLocAddress1.ClientID %>').value = "";
            }

            return true;
        }


        function DisaableLocaitonFielsds() {

            //$("#hdnisAddresOrShopping").attr("disabled", true);
            document.getElementById('<%=hdnisAddresOrShopping.ClientID %>').value = "0";
            document.getElementById('<%=txtlocSHPaddress1.ClientID %>').disabled = "Disabled";
            document.getElementById('<%=txtSearchLocShoppingCenter.ClientID %>').disabled = "Disabled";
            document.getElementById('<%=btnAddNewShoppingCenter.ClientID %>').disabled = "Disabled";
            document.getElementById('<%=txtLocAddress2.ClientID %>').disabled = "Disabled";
            document.getElementById('<%=txtLocCity.ClientID %>').disabled = "Disabled";
            document.getElementById('<%=txtLocCounty.ClientID %>').disabled = "Disabled";
            document.getElementById('<%=ddlLocState.ClientID %>').disabled = "Disabled";
            document.getElementById('<%=txtLocZipCode.ClientID %>').disabled = "Disabled";
            document.getElementById('<%=btnMapLocation.ClientID %>').disabled = "Disabled";
            document.getElementById('<%=txtMapLocation.ClientID %>').disabled = "Disabled";
        }

        function EnableLocaitonFielsds() {
            $("#hdnisAddresOrShopping").removeAttr("disabled");
            document.getElementById('<%=txtlocSHPaddress1.ClientID %>').disabled = "";
            document.getElementById('<%=txtSearchLocShoppingCenter.ClientID %>').disabled = "";
            document.getElementById('<%=btnAddNewShoppingCenter.ClientID %>').disabled = "";
            document.getElementById('<%=txtLocAddress2.ClientID %>').disabled = "";
            document.getElementById('<%=txtLocCity.ClientID %>').disabled = "";
            document.getElementById('<%=txtLocCounty.ClientID %>').disabled = "";
            document.getElementById('<%=ddlLocState.ClientID %>').disabled = "";
            document.getElementById('<%=txtLocZipCode.ClientID %>').disabled = "";
            document.getElementById('<%=btnMapLocation.ClientID %>').disabled = "";
            document.getElementById('<%=txtMapLocation.ClientID %>').disabled = "";
        }


        //var prm = Sys.WebForms.PageRequestManager.getInstance();
        //prm.add_endRequest(function () {
        //   
        //    SaveStatus = document.getElementById('< %= hdnSaveStatus.ClientID %>').value;
        //    ValidateCS();
        //});



    </script>

    <%-- <style type="text/css">
        .PopupDivBodyStyle {
            background-color: white;
            border: ridge;
            border-color: black;
            width: 320px;
            height: 250px;
            border-radius: 3px;
        }

         function VisiblePopupDiv() {
            var div = document.getElementById('<%=dvSalImmroved.ClientID%>');
            div.style.display = 'block';
            return true;
        }
    </style>--%>

    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <ajax:ToolkitScriptManager runat="server" EnablePageMethods="true" />
    --%>
    <%-- <ajax:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" />   <%-- CompletionListCssClass="AutoCompleteFlyout"
            CompletionListItemCssClass="AutoCompleteFlyoutItem"--%>

    <script type="text/javascript">


        function openMap() {
            window.open("testHTMlpage.html", "_blank", "height:725px,width=725px");
        }

    </script>



</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <table style="width: 100%">
        <tr>
            <td>
                <asp:Label ID="lbErrorMsg" runat="server" class="errorMsg" /></td>
            <td class="mainPageTitle">NEW CHARGE SLIP
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr />
            </td>
        </tr>
    </table>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Debug">
        <Services>
            <asp:ServiceReference Path="CacheService.asmx" />
        </Services>
    </asp:ScriptManager>

    <%-- <asp:UpdatePanel ID="upNewCharge" runat="server" UpdateMode="Conditional">
        <ContentTemplate>--%>
    <asp:HiddenField ID="hdnPageLeave" runat="server" />
    <table style="width: 100%">
        <tr>
            <td>
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 55%; vertical-align: top;">
                            <table style="width: 100%">

                                <tr>
                                    <td class="boldlabels">DEAL NAME</td>
                                    <td colspan="2">
                                        <asp:TextBox CssClass="textboxlarge Marbottom3" ID="txtDealName" runat="server" TabIndex="1"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="boldlabels">LOCATION</td>
                                    <td class="radio-toolbar">

                                        <asp:RadioButtonList ID="rdbLocation" runat="server" onchange="return toggleLocation('2');" RepeatDirection="Horizontal" TabIndex="2" RepeatLayout="Flow">
                                            <asp:ListItem Text="Shopping Center" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="No Shopping Center" Value="1"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="boldlabels"><%--LOCATION--%></td>
                                    <td>
                                        <asp:UpdatePanel ID="upLocation" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table>
                                                    <tr id="trshopping">
                                                        <td>
                                                            <asp:HiddenField ID="hdnisAddresOrShopping" runat="server" />
                                                            <%--     <asp:HiddenField ID="hdnIsCompanyChanged" runat="server" />
                                                <asp:HiddenField ID="hdnIsCompanySaved" runat="server" /> textboxlarge Marbottom3  --%>
                                                            <asp:HiddenField ID="hdnIsShoppingCenterAddressSaved" runat="server" />
                                                            <asp:HiddenField ID="hndIsShoppingCenterAddressChanged" runat="server" />
                                                            <asp:TextBox CssClass="textboxlarge Marbottom3" runat="server" ID="txtSearchLocShoppingCenter" AutoPostBack="true"
                                                                Text="SHOPPING CENTER.." OnTextChanged="txtSearchLocShoppingCenter_TextChanged" TabIndex="3" onclick="hideWaterText('shoppingcenter');" onblur="showWaterText('shoppingcenter');" />
                                                            <ajax:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server"
                                                                TargetControlID="txtSearchLocShoppingCenter"
                                                                DelimiterCharacters=";, :"
                                                                MinimumPrefixLength="1"
                                                                EnableCaching="true"
                                                                CompletionSetCount="10"
                                                                CompletionInterval="300"
                                                                ServiceMethod="SearchGetWhatList1"
                                                                OnClientPopulating="ShowIconNC"
                                                                OnClientPopulated="hideIconNC"
                                                                OnClientItemSelected="OnCenterSelected"
                                                                ShowOnlyCurrentWordInCompletionListItem="true"
                                                                CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList"
                                                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                                                CompletionListElementID="divwidth" />
                                                            <asp:HiddenField ID="hdnLocId" runat="server" Value="" />
                                                            <%--  <ajax:TextBoxWatermarkExtender ID="WMtxtSearchLocShoppingCenter" runat="server"
                                                                WatermarkText="SHOPPING CENTER.." TargetControlID="txtSearchLocShoppingCenter">
                                                            </ajax:TextBoxWatermarkExtender>--%>
                                                            <asp:HiddenField ID="hdnMapLatLon" runat="server" />
                                                            <%--   <ajax:ComboBox ID="ajaxcombLocShoppingCenter" runat="server" Width="250px" CssClass="ddlHeight" AutoPostBack="true"
                                        onFocus="this.Select()" AutoCompleteMode="SuggestAppend" RenderMode="Block" OnSelectedIndexChanged="ajaxcombLocShoppingCenter_SelectedIndexChanged">
                                    </ajax:ComboBox>--%>

                                                            <ajax:ModalPopupExtender ID="mpeAddNewShoppingCenter" TargetControlID="lbDummybtn" CancelControlID="btnCancel"
                                                                PopupControlID="divNwCenterOrCompany" runat="server" BackgroundCssClass="modalBackground">
                                                            </ajax:ModalPopupExtender>
                                                            <asp:ImageButton ID="btnAddNewShoppingCenter" runat="server" OnClick="btnAddNewCenter_Click" CssClass="imgSizeSmall" ImageUrl="~/Images/Add-Icon.gif" />
                                                            <asp:Label ID="lbDummybtn" runat="server"></asp:Label>

                                                            <br />
                                                            <asp:TextBox ID="txtlocSHPaddress1" CssClass="textboxlarge jq_watermark" runat="server" TabIndex="4"
                                                                Text="ADDRESS LINE 1.." onclick="hideWaterText('txtlocSHPaddress1');" onblur="showWaterText('txtlocSHPaddress1');"></asp:TextBox>

                                                            <%--<ajax:TextBoxWatermarkExtender ID="txtwatermark" runat="server" WatermarkText="ADDRESS LINE 1.." TargetControlID="txtlocSHPaddress1"></ajax:TextBoxWatermarkExtender>--%>

                                                        </td>
                                                        <%-- <td style="padding-right :70px;">--%>

                                                        <%--<asp:Button ID="btnAddNewShoppingCenter" runat="server" Text="Add New" CssClass="SqureButton" OnClick="btnAddNewCenter_Click" />--%>

                                                        <%-- </td>--%>
                                                    </tr>

                                                    <%--<tr>
                                <td>
                                    <br />
                                </td>
                            </tr>--%>
                                                    <tr id="trAddress" style="display: none">
                                                        <%--      <td class="boldlabels">
                                                <br />
                                            </td>--%>
                                                        <td colspan="2">
                                                            <div class=" Marbottom3" style="height: 8px"></div>
                                                            <br />
                                                            <asp:TextBox ID="txtLocAddress1" runat="server" CssClass="textboxlarge  " AutoPostBack="true"
                                                                OnTextChanged="txtLocAddress1_TextChanged" TabIndex="5" Text="ADDRESS LINE 1.." onclick="hideWaterText('locaddr1');" onblur="showWaterText('locaddr1');"></asp:TextBox>
                                                            <%--<asp:HiddenField ID="hdnAddrOnly" runat="server" --%>
                                                            <ajax:AutoCompleteExtender ID="AutotxtLocAddress1" runat="server"
                                                                TargetControlID="txtLocAddress1"
                                                                DelimiterCharacters=";, :"
                                                                MinimumPrefixLength="1"
                                                                EnableCaching="false"
                                                                CompletionSetCount="10"
                                                                CompletionInterval="300"
                                                                ServiceMethod="SearchGetAddrList"
                                                                OnClientPopulating="ShowIconNC"
                                                                OnClientPopulated="hideIconNC"
                                                                OnClientItemSelected="OnAddrSelected"
                                                                ShowOnlyCurrentWordInCompletionListItem="true"
                                                                CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList"
                                                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                                                CompletionListElementID="divwidth" />
                                                            <asp:HiddenField ID="HiddenField1" runat="server" Value="" />


                                                            <%-- <ajax:TextBoxWatermarkExtender ID="WMtxtLocAddress1" runat="server" WatermarkText="ADDRESS LINE 1.." TargetControlID="txtLocAddress1"></ajax:TextBoxWatermarkExtender>--%>
                                                            <asp:ImageButton ID="btnAddNewShoppingCenterAddress" runat="server" OnClick="btnAddNewShoppingCenterAddress_Click" CssClass="imgSizeSmall" ImageUrl="~/Images/Add-Icon.gif" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%-- <td class="boldlabels"></td>--%>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtLocAddress2" runat="server" CssClass="textboxlarge  " TabIndex="6" Text="ADDRESS LINE 2.." onclick="hideWaterText('locaddr2');" onblur="showWaterText('locaddr2');"></asp:TextBox>
                                                            <%--<ajax:TextBoxWatermarkExtender ID="WMtxtLocAddress2" runat="server" WatermarkText="ADDRESS LINE 2.." TargetControlID="txtLocAddress2"></ajax:TextBoxWatermarkExtender>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%-- <td></td>--%>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtLocCity" runat="server" CssClass="textboxmedium  " TabIndex="7" Text="CITY.." onclick="hideWaterText('txtLocCity');" onblur="showWaterText('txtLocCity');"></asp:TextBox>
                                                            <%--<ajax:TextBoxWatermarkExtender ID="WMtxtLocCity" runat="server" WatermarkText="CITY.." TargetControlID="txtLocCity"></ajax:TextBoxWatermarkExtender>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%-- <td></td>--%>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtLocCounty" runat="server" CssClass="textboxmedium" TabIndex="8" Text="COUNTY.." onclick="hideWaterText('txtLocCounty');" onblur="showWaterText('txtLocCounty');"></asp:TextBox>
                                                            <%-- <ajax:TextBoxWatermarkExtender ID="WMtxtLocCounty" runat="server" WatermarkText="COUNTY.." TargetControlID="txtLocCounty"></ajax:TextBoxWatermarkExtender>--%>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--<td></td>--%>
                                                        <td colspan="2">
                                                            <asp:DropDownList ID="ddlLocState" runat="server" CssClass="ddlSmall" Enabled="true" TabIndex="9">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--<td></td>--%>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtLocZipCode" runat="server" CssClass="textboxsmall Marbottom3  " Text="ZIP CODE.." onclick="hideWaterText('txtLocZipCode');" onblur="showWaterText('txtLocZipCode');" TabIndex="10" onkeypress="return Validate(event);"></asp:TextBox>
                                                            <%--<ajax:TextBoxWatermarkExtender ID="WMtxtLocZipCode" runat="server" WatermarkText="ZIP CODE.." TargetControlID="txtLocZipCode"></ajax:TextBoxWatermarkExtender>--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <%-- <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>--%>

                                <tr>
                                    <td class="boldlabels">MAP LOCATE</td>
                                    <td style="width: 45%">

                                        <ajax:ModalPopupExtender ID="mpmaplocation" TargetControlID="btnMapLocation" CancelControlID="btnCancelMap"
                                            PopupControlID="divMapLocation" runat="server" BackgroundCssClass="modalBackground">
                                        </ajax:ModalPopupExtender>


                                        <asp:TextBox ID="txtMapLocation" runat="server" CssClass="textboxlarge Marbottom3" TabIndex="11"></asp:TextBox>
                                    </td>
                                    <td style="width: 10%; padding-right: 30px; height: 18px;">
                                        <asp:Button ID="btnMapLocation" runat="server" Text="MAP" OnClientClick="return setIframPath()" CssClass="SqureGrayButtonSmall" />
                                    </td>
                                </tr>
                                <%--     <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>--%>
                                <%--  </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>--%>


                                <tr>
                                    <td class="boldlabels">CHARGE DATE</td>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtChargeDate" class="Marbottom3" runat="server" CssClass="textboxsmall Marbottom3"></asp:TextBox>
                                    </td>
                                </tr>
                                <%--    <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>--%>

                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <%-- <asp:UpdatePanel ID="updachargeto" runat="server" UpdateMode="Conditional">
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="txtCTName" />

                                        </Triggers>
                                        <ContentTemplate>
                                            <table style="width: 100%;">--%>

                                <tr>
                                    <td class="boldlabels" style="vertical-align: top; padding-top: 30px;">CHARGE TO</td>
                                    <td colspan="2">
                                        <asp:UpdatePanel ID="upChargeto" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>

                                                <table>
                                                    <tr>
                                                        <td colspan="2">

                                                            <asp:TextBox ID="txtCTOwnershipEntity" runat="server" CssClass="textboxlarge" Text="OWNERSHIP ENTITY NAME..." onclick="hideWaterText('txtCTOwnershipEntity');" onblur="showWaterText('txtCTOwnershipEntity');" TabIndex="13"></asp:TextBox>
                                                            <%--<ajax:TextBoxWatermarkExtender ID="WMCTOwnershipEntity" runat="server" WatermarkText="OWNERSHIP ENTITY NAME..." TargetControlID="txtCTOwnershipEntity"></ajax:TextBoxWatermarkExtender>--%>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <%--   <td class="boldlabels"></td>--%>
                                                        <td colspan="2">
                                                            <asp:HiddenField ID="hdnIsCompanyChanged" runat="server" />
                                                            <asp:HiddenField ID="hdnIsCompanySaved" runat="server" /><%--showWaterText('txtCTName');--%>
                                                            <asp:TextBox ID="txtCTName" runat="server" CssClass="textboxlarge" AutoPostBack="true" Text="LANDLORD TRADE NAME.." onclick="hideWaterText('txtCTName');" onblur="CheckCompany('txtCTName');" OnTextChanged="txtComapny_TextChanged" TabIndex="14"></asp:TextBox>
                                                            <%--<ajax:TextBoxWatermarkExtender ID="WMtxtCTName" runat="server" WatermarkText="LANDLORD TRADE NAME.." TargetControlID="txtCTName"></ajax:TextBoxWatermarkExtender>--%>

                                                            <ajax:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server"
                                                                TargetControlID="txtCTName"
                                                                DelimiterCharacters=";, :"
                                                                MinimumPrefixLength="1"
                                                                EnableCaching="true"
                                                                CompletionSetCount="10"
                                                                CompletionInterval="300"
                                                                ServiceMethod="SearchGetComapnyList"
                                                                OnClientPopulating="ShowIconNCOMP"
                                                                OnClientPopulated="hideIconNC"
                                                                OnClientItemSelected="OnCompanySelected"
                                                                ShowOnlyCurrentWordInCompletionListItem="true"
                                                                CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList"
                                                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                                                CompletionListElementID="divwidth" />
                                                            <asp:HiddenField ID="hdnCompId" runat="server" Value="" />

                                                            <ajax:ModalPopupExtender ID="mpeAddNewComapny" TargetControlID="lbDummy" CancelControlID="btnCancel"
                                                                PopupControlID="divNwCenterOrCompany" runat="server" BackgroundCssClass="modalBackground">
                                                            </ajax:ModalPopupExtender>
                                                            <asp:ImageButton ID="btnAddNewCompany" runat="server" OnClick="btnAddNewComapny_Click" CssClass="imgSizeSmall" ImageUrl="~/Images/Add-Icon.gif" />
                                                            <asp:Label ID="lbDummy" runat="server"></asp:Label>


                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--<td class="boldlabels"></td>--%>
                                                        <td style="width: 45%" colspan="2">
                                                            <%-- <editable:EditableDropDownList ID="ddlCTCompany" runat="server" Width="250px" CssClass="ddlHeight" onFocus="this.Select()"></editable:EditableDropDownList>--%>
                                                            <asp:TextBox runat="server" ID="txtComapny" CssClass="textboxlarge " TabIndex="14" Text="NAME.." onclick="hideWaterText('txtComapny');" onblur="showWaterText('txtComapny');" />

                                                            <%--  
                                                     <ajax:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" WatermarkText="NAME.." TargetControlID="txtComapny"></ajax:TextBoxWatermarkExtender>

                                                     <ajax:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server"
                                        TargetControlID="txtComapny"
                                        DelimiterCharacters=";, :"
                                        MinimumPrefixLength="1"
                                        EnableCaching="true"
                                        CompletionSetCount="10"
                                        CompletionInterval="300"
                                        ServiceMethod="SearchGetComapnyList"
                                        OnClientPopulating="ShowIconNC"
                                        OnClientPopulated="hideIconNC"
                                        OnClientItemSelected="OnCompanySelected"
                                        ShowOnlyCurrentWordInCompletionListItem="true"
                                        CompletionListCssClass="AutoExtender"
                                        CompletionListItemCssClass="AutoExtenderList"
                                        CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                        CompletionListElementID="divwidth" />
                                    <asp:HiddenField ID="hdnCompId" runat="server" Value="" />

                                    <ajax:ModalPopupExtender ID="mpeAddNewComapny" TargetControlID="lbDummy" CancelControlID="btnCancel"
                                        PopupControlID="divNwCenterOrCompany" runat="server" BackgroundCssClass="modalBackground">
                                    </ajax:ModalPopupExtender>
                                     <asp:ImageButton ID="btnAddNewCompany" runat="server" OnClick="btnAddNewComapny_Click" Height="16px" Width="16px" ImageUrl="~/Images/Add-Icon.gif" /> 
                                    <asp:Label ID="lbDummy" runat="server"></asp:Label>--%>
                                                        </td>
                                                        <%-- <td style="text-align: left;">
                                    <asp:Button ID="btnAddNewCompany" runat="server" Text="Add New" CssClass="SqureButton" OnClick="btnAddNewComapny_Click" />
                                    <asp:Label ID="lbDummy" runat="server"></asp:Label>
                                </td>--%>
                                                    </tr>

                                                    <tr>
                                                        <%-- <td class="boldlabels"></td>--%>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtCTEmail" runat="server" CssClass="textboxlarge  " TabIndex="15" Text="EMAIL..." onclick="hideWaterText('txtCTEmail');" onblur="showWaterText('txtCTEmail');"></asp:TextBox>
                                                            <%-- <ajax:TextBoxWatermarkExtender ID="WMtxtCTEmail" runat="server" WatermarkText="EMAIL..." TargetControlID="txtCTEmail"></ajax:TextBoxWatermarkExtender>--%>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--<td class="boldlabels"></td>--%>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtCTAddress1" runat="server" CssClass="textboxlarge  " TabIndex="16" onclick="hideWaterText('txtCTAddress1');" onblur="showWaterText('txtCTAddress1');" Text="ADDRESS LINE 1.."></asp:TextBox>

                                                            <%--  <ajax:TextBoxWatermarkExtender ID="WMtxtCTAddress1" runat="server" WatermarkText="ADDRESS LINE 1.." TargetControlID="txtCTAddress1"></ajax:TextBoxWatermarkExtender>
                                                     <asp:ImageButton ID="btnAddNewComapnyAddress" runat="server" OnClick="btnAddNewComapnyAddress_Click" Height="16px" Width="16px" ImageUrl="~/Images/Add-Icon.gif" />--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--  <td class="boldlabels"></td>--%>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtCTAddress2" runat="server" CssClass="textboxlarge  " TabIndex="17" onclick="hideWaterText('txtCTAddress2');" onblur="showWaterText('txtCTAddress2');" Text="ADDRESS LINE 2.."></asp:TextBox>
                                                            <%-- <ajax:TextBoxWatermarkExtender ID="WMtxtCTAddress2" runat="server" WatermarkText="ADDRESS LINE 2.." TargetControlID="txtCTAddress2"></ajax:TextBoxWatermarkExtender>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%-- <td class="boldlabels"></td>--%>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtCTCity" runat="server" CssClass="textboxmedium  " TabIndex="18" onclick="hideWaterText('txtCTCity');" onblur="showWaterText('txtCTCity');" Text="CITY.."></asp:TextBox>
                                                            <%-- <ajax:TextBoxWatermarkExtender ID="WMtxtCTCity" runat="server" WatermarkText="CITY.." TargetControlID="txtCTCity"></ajax:TextBoxWatermarkExtender>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%-- <td class="boldlabels"></td>--%>
                                                        <td colspan="2">
                                                            <asp:DropDownList ID="ddlCTState" runat="server" CssClass="ddlSmall" Enabled="true" TabIndex="19"></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--<td class="boldlabels"></td>--%>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtCTZipCode" runat="server" CssClass="textboxsmall  " TabIndex="20" onclick="hideWaterText('txtCTZipCode');" onblur="showWaterText('txtCTZipCode');" Text="ZIP CODE.."  onkeypress="return Validate(event);"></asp:TextBox>
                                                            <%--<ajax:TextBoxWatermarkExtender ID="WMtxtCTZipCode" runat="server" WatermarkText="ZIP CODE.." TargetControlID="txtCTZipCode"></ajax:TextBoxWatermarkExtender>--%>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <%-- </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>--%>
                                <tr>
                                    <td colspan="3">
                                        <br />
                                    </td>
                                </tr>
                                <%-- <tr>
                                    <td class="boldlabels">DEAL TYPE</td>
                                    <td colspan="2">
                                        <asp:UpdatePanel ID="upDealTypeSel" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlDealType" runat="server" OnSelectedIndexChanged="ddlDealType_SelectedIndexChange" AutoPostBack="true" CssClass="ddlSmall Marbottom3" Sorted="true" AutoselectFirstItem="true" TabIndex="21" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr runat="server" id="trDealSubType">
                                    <td class="boldlabels">DEAL SUBTYPE</td>
                                    <td>
                                        <asp:UpdatePanel ID="UpDealSubType" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlDealSubType" runat="server" CssClass="ddlSmall" OnSelectedIndexChanged="ddlDealSubType_SelectedIndexChange" AutoPostBack="true" TabIndex="22" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>--%>
                                <%-- <tr>
                                    <td colspan="3"  >
                                        <asp:UpdatePanel ID="upDealTypeSel" runat="server">
                                            <ContentTemplate>
                                                
                                                <table style="width: 100%" cellspacing="0">
                                                    <tr>
                                                        <td class="boldlabels"   >DEAL TYPE</td>
                                                        <td >
                                                            <asp:DropDownList ID="ddlDealType" runat="server" OnSelectedIndexChanged="ddlDealType_SelectedIndexChange" AutoPostBack="true" CssClass="ddlSmall Marbottom3" Sorted="true" AutoselectFirstItem="true" TabIndex="21" />
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trDealSubType">

                                                        <td class="boldlabels">DEAL SUBTYPE</td>
                                                        <td >

                                                            <asp:DropDownList ID="ddlDealSubType" runat="server" CssClass="ddlSmall" OnSelectedIndexChanged="ddlDealSubType_SelectedIndexChange" AutoPostBack="true" TabIndex="22" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <%--<td class="boldlabels">deal type</td>--%>
                                </tr>
                                <tr>
                                    <td class="boldlabels">
                                      Deal Designation Type
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlDealDesignationType" runat ="server" Width="152px" Height="24px">
                                           <%--   <asp:ListItem Text="-Select-" Value="0" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="LandLord" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Tenant" Value="2"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <%--<td class="boldlabels">sub type</td>--%>
                                    <td rowspan="2">
                                         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td class="boldlabels">DEAL TYPE</td>
                                            </tr>
                                            <tr runat ="server" id="trDealSubTypeLabel">
                                                <td class="boldlabels" style="padding-top:10px;">DEAL SUBTYPE</td>
                                            </tr>
                                        </table>
                                                 </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td rowspan="2">
                                            <asp:UpdatePanel ID="upDealTypeSel" runat="server">
                                            <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddlDealType" runat="server" OnSelectedIndexChanged="ddlDealType_SelectedIndexChange" AutoPostBack="true" CssClass="ddlSmall Marbottom3" Sorted="true" AutoselectFirstItem="true" TabIndex="21" /></td>
                                            </tr>
                                            <tr runat="server" id="trDealSubType">
                                                <td>
                                                    <asp:DropDownList ID="ddlDealSubType" runat="server" CssClass="ddlSmall" OnSelectedIndexChanged="ddlDealSubType_SelectedIndexChange" AutoPostBack="true" TabIndex="22" />
                                                       <asp:HiddenField ID="hdnSize" runat ="server" /><%--Added by Jaywanti on 31-08-2015--%>
                                                    <asp:HiddenField ID="hdnOldUnitValue" runat="server" Value="" />
                                                    <asp:HiddenField ID="hdnOldUnitTypeValue" runat ="server" Value="" />
                                                </td>
                                             
                                            </tr>
                                        </table>
                                         </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="ucbackground" style="width: 45%; vertical-align: top; border: 1px solid black">
                            <asp:UpdatePanel ID="upUserControl" runat="server">
                                <ContentTemplate>
                                    <div id="dvSalImmroved" runat="server">
                                        <uc:SaleTranseImprovedLand ID="ucSaleImmproved" runat="server" />
                                    </div>
                                    <div id="dvSaleUnimmproved" runat="server">
                                        <uc:SaleTranseUnImprovedLand ID="ucSaleUnImprovedLand" runat="server" TabIndex="24" />
                                    </div>
                                    <div id="dvLeaseGround" runat="server">
                                        <uc:LeaseTransactionGround ID="ucLeaseGround" runat="server" />
                                    </div>
                                    <div id="dvLeaseBuilding" runat="server">
                                        <uc:LeaseTransactionBuilding ID="ucLeaseBuilding" runat="server" />
                                    </div>
                                    <div id="dvConsulting" runat="server">
                                        <uc:ConsultingTransaction ID="ucConsulting" runat="server" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                        </td>
                    </tr>
                    <tr>

                        <td colspan="2">
                            <br />
                        </td>
                    </tr>

                </table>

            </td>
        </tr>
    </table>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
    <div id="divNwCenterOrCompany" class="PopupDivBodyStyle" style="height: auto; width: 530px; display: none;">
        <%--class="spacing"--%>
        <table style="width: 100%;" cellpadding="0" cellspacing="0">
            <tr style="border-bottom: solid; background-color: gray;">
                <td style="text-align: left;">
                    <asp:UpdatePanel ID="upTitile" runat="server">
                        <ContentTemplate>
                            <h3>&nbsp; &nbsp;      
                                <asp:Label ID="lbTitle" runat="server"></asp:Label></h3>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="right">
                    <asp:ImageButton ImageUrl="~/Images/close.png" ID="btnCancel" runat="server" OnClientClick="return ClearControls();" />
                    <%--OnClientClick="javascript:window.close();return false;"--%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc:AddNewShoppingCenterOrCompany ID="ucNewCenterOrCompany" runat="server" />

                </td>
            </tr>
        </table>

    </div>


    <div id="divMapLocation" class="PopupDivBodyStyle" style="height: 700px; width: 700px; display: none;">
        <%--class="spacing"--%>
        <table style="width: 100%" class="spacing">
            <tr class="ucbackground" style="border-bottom: solid; background-color: gray;">
                <td style="text-align: left;">
                    <h3>&nbsp; &nbsp;      
                                  Locate on Map</h3>
                </td>
                <td align="right" style="padding-right: 10px">
                    <asp:ImageButton ImageUrl="~/Images/close.png" ID="btnCancelMap" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <iframe id="frmaploc" src="" style="width: 100%; height: 650px; border: none" runat="server"></iframe>
                    <%-- <uc:ShoppingMapLocation ID="ucMaplocation" runat="server" style="position:relative"/>--%>
                </td>
            </tr>
        </table>

    </div>



</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" runat="server">
    <table style="width: 98%;">
        <tr>
            <td style="width: 60%; text-align: left;">

                <label class="whitelabel"><b>PAGE 1 OF 4</b></label>

                <asp:Label ID="lbModificationHistory" runat="server" Text=""></asp:Label>
            </td>
            <td style="width: 40%; text-align: right;">
                <asp:Button ID="btnClearAll" OnClientClick="SetPageLeave() ;" OnClick="btnClearAll_Click" runat="server" Style="background-color: #808080" Text="Clear All" CssClass="SqureButton" />
                <asp:Button ID="btnSaveForLater" runat="server" Text="SAVE FOR LATER" OnClick="btnNext_Click" BackColor="#808080" CssClass="SqureButton" OnClientClick="javascript:return PageLeaveCheck('D');" />
                &nbsp;
                <asp:Button ID="btnNext" runat="server" Text="NEXT" OnClick="btnNext_Click" CssClass="btnSmallOrange" OnClientClick="javascript:return PageLeaveCheck('N');" /></td>

            <asp:HiddenField ID="hdnSaveStatus" runat="server" Value="" />

        </tr>
    </table>

    <script type="text/javascript">


        //// Sys.WebForms.PageRequestManager.getInstance().add_endRequest(applyWatermark);

        //window.onbeforeunload = beforeUnloading; debugger;
        //var prm = Sys.WebForms.PageRequestManager.getInstance();
        //prm.add_endRequest(endRequest);

        //function beforeUnloading() {
        //    alert("before unloading");
        //    $.watermark.hideAll();
        //}

        //function endRequest(sender, args) {
        //    window.onbeforeunload = beforeUnloading;
        //    alert("end request");
        //    $('# < %=txtCTAddress1.ClientID%>').watermark("Enter a date");
        //}


    </script>

</asp:Content>

