﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using SegallChargeSlipBll;
public partial class test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }
    protected void bthSaveReprintPath_Click(object sender, EventArgs e)
    {
        ChargeSlip obj = new ChargeSlip();
        DataSet ds = new DataSet();
        ds = obj.GetListStatusCompleted();
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                bool result = false;
                string Filename = ds.Tables[0].Rows[i]["DealName"] + "_" + ds.Tables[0].Rows[i]["ChargeSlipID"];
                string EditedFilename = ds.Tables[0].Rows[i]["DealName"] + "_" + ds.Tables[0].Rows[i]["ChargeSlipID"] + "Edited.pdf";
                string fileeditedpath = System.Configuration.ConfigurationManager.AppSettings["ReprintReportPath"] + EditedFilename;
                if (System.IO.File.Exists(fileeditedpath))
                {
                    result = true;
                  
                }
                obj.ChargeSlipID = Convert.ToInt32(ds.Tables[0].Rows[i]["ChargeSlipID"]);
                obj.UpdateEditedChargeslip(result);
            }
        }
    }
}