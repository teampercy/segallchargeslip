﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    string PrevURL = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            lblMessage.Text = "";
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
           
            //if (DateTime.Now <= DateTime.Parse("2014/02/03"))
            //{
                if (Session[GlobleData.cns_PrevPage] != null)
                {
                    string PrevURL = Session[GlobleData.cns_PrevPage].ToString();
                }
                lblMessage.Text = "";
                Users user = new Users();
                user = user.UserAunthentication(txtUsername.Text, txtPassword.Text);
                if (user != null)
                {
                    if (user.UserID != null && user.UserID > 0)
                    {
                        Session["UserID"] = user.UserID;
                        Session["User"] = user.Username;
                        Session["UserType"] = user.UserType;
                        Session["UserName"] = user.FirstName + ' ' + user.LastName;
                        //hdnSessionTime
                        //((HiddenField)Master.FindControl("hdnSessionTime")).Value = DateTime.Now.ToString();
                        string LoginTime = DateTime.Now.ToString();
                        if (user.UserType == 1)
                        {
                            Response.Redirect("~/Admin/Snapshot.aspx", false);

                        }
                        else if (user.UserType == 2)
                        {
                            if (string.IsNullOrEmpty(PrevURL))
                            {
                                Response.Redirect("~/Pages/AddOrEditNewChargeSlip.aspx", false);
                            }
                            else
                            {
                                Response.Redirect(PrevURL, false);
                            }
                        }
                    }
                    else
                        LoginFailMsg();
                }
                else
                    LoginFailMsg();
            //}
            //else
            //{
            //    LoginFailMsg();
            //}
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Login.aspx", "btnLogin_Click", ex.Message);
        }
    }

    #region CustomActions

    private void LoginFailMsg()
    {
        lblMessage.Text = "Please enter correct username and password.";
        lblMessage.ForeColor = System.Drawing.Color.Red;
    }
    #endregion


}