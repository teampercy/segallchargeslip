// -----------------------------------------------------------------------
// <copyright file="Messages.cs" company="Rheal Software (P.) Ltd.">
// Copyright (c) "Rheal Software (P.) Ltd.". All rights reserved.
// </copyright>
// <author></author>
// -----------------------------------------------------------------------
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

/// <summary>
/// Business class representing Messages
/// </summary>
public class Messages
{
#region Basic Functionality

#region Variable Declaration

   /// <summary>
   /// Variable to store Database object to interact with database.
   /// </summary>
   private Database db;
#endregion

#region Constructors

   /// <summary>
   /// Initializes a new instance of the Messages class.
   /// </summary>
   public Messages()
   {
       this.db = DatabaseFactory.CreateDatabase("ConnectionString");
   }

   /// <summary>
   /// Initializes a new instance of the Messages class.
   /// </summary>
   /// <param name="messageID">Sets the value of MessageID.</param>
   public Messages(int messageID)
   {
       this.db = DatabaseFactory.CreateDatabase("ConnectionString");
      this.MessageID = messageID;
   }
#endregion

#region Properties

   /// <summary>
   /// Gets or sets MessageID
   /// </summary>
   public int MessageID
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets MessageText
   /// </summary>
   public string MessageText
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets MessageDate
   /// </summary>
   public DateTime MessageDate
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets IsRead
   /// </summary>
   public bool IsRead
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets CreatedBy
   /// </summary>
   public int CreatedBy
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets CreatedOn
   /// </summary>
   public DateTime CreatedOn
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets FromUser
   /// </summary>
   public int FromUser
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets ToUser
   /// </summary>
   public int ToUser
   {
      get; set;
   }

   /// <summary>
   /// Gets or sets IsActive
   /// </summary>
   public bool IsActive
   {
      get; set;
   }
#endregion

#region Actions

   /// <summary>
   /// Loads the details for Messages.
   /// </summary>
   /// <returns>True if Load operation is successful; Else False.</returns>
   public bool Load()
   {
      try
      {
         if (this.MessageID != 0)
         {
            DbCommand com = this.db.GetStoredProcCommand("upMessagesGetDetails");
            this.db.AddInParameter(com, "MessageID", DbType.Int32, this.MessageID);
            DataSet ds = this.db.ExecuteDataSet(com);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
               DataTable dt = ds.Tables[0];
               this.MessageID = Convert.ToInt32(dt.Rows[0]["MessageID"]);
               this.MessageText = Convert.ToString(dt.Rows[0]["MessageText"]);
               this.MessageDate = Convert.ToDateTime(dt.Rows[0]["MessageDate"]);
               this.IsRead = Convert.ToBoolean(dt.Rows[0]["IsRead"]);
               this.CreatedBy = Convert.ToInt32(dt.Rows[0]["CreatedBy"]);
               this.CreatedOn = Convert.ToDateTime(dt.Rows[0]["CreatedOn"]);
               this.FromUser = Convert.ToInt32(dt.Rows[0]["FromUser"]);
               this.ToUser = Convert.ToInt32(dt.Rows[0]["ToUser"]);
               this.IsActive = Convert.ToBoolean(dt.Rows[0]["IsActive"]);
               return true;
            }
         }

      return false;
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }
   }

   /// <summary>
   /// Inserts details for Messages if MessageID = 0.
   /// Else updates details for Messages.
   /// </summary>
   /// <returns>True if Save operation is successful; Else False.</returns>
   public bool Save()
   {
      if (this.MessageID == 0)
      {
         return this.Insert();
      }
      else
      {
         if (this.MessageID > 0)
         {
            return this.Update();
         }
         else
         {
            this.MessageID = 0;
            return false;
         }
      }
   }

   /// <summary>
   /// Inserts details for Messages.
   /// Saves newly created Id in MessageID.
   /// </summary>
   /// <returns>True if Insert operation is successful; Else False.</returns>
   private bool Insert()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("upMessagesInsert");
         this.db.AddOutParameter(com, "MessageID", DbType.Int32, 1024);
         if (!String.IsNullOrEmpty(this.MessageText))
         {
            this.db.AddInParameter(com, "MessageText", DbType.String, this.MessageText);
         }
         else
         {
            this.db.AddInParameter(com, "MessageText", DbType.String, DBNull.Value);
         }
         if (this.MessageDate > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "MessageDate", DbType.DateTime, this.MessageDate);
         }
         else
         {
            this.db.AddInParameter(com, "MessageDate", DbType.DateTime, DBNull.Value);
         }
         this.db.AddInParameter(com, "IsRead", DbType.Boolean, this.IsRead);
         if (this.CreatedBy > 0)
         {
            this.db.AddInParameter(com, "CreatedBy", DbType.Int32, this.CreatedBy);
         }
         else
         {
            this.db.AddInParameter(com, "CreatedBy", DbType.Int32, DBNull.Value);
         }
         if (this.CreatedOn > DateTime.MinValue)
         {
            this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, this.CreatedOn);
         }
         else
         {
            this.db.AddInParameter(com, "CreatedOn", DbType.DateTime, DBNull.Value);
         }
         if (this.FromUser > 0)
         {
            this.db.AddInParameter(com, "FromUser", DbType.Int32, this.FromUser);
         }
         else
         {
            this.db.AddInParameter(com, "FromUser", DbType.Int32, DBNull.Value);
         }
         if (this.ToUser > 0)
         {
            this.db.AddInParameter(com, "ToUser", DbType.Int32, this.ToUser);
         }
         else
         {
            this.db.AddInParameter(com, "ToUser", DbType.Int32, DBNull.Value);
         }
         this.db.AddInParameter(com, "IsActive", DbType.Boolean, this.IsActive);
         this.db.ExecuteNonQuery(com);
         this.MessageID = Convert.ToInt32(this.db.GetParameterValue(com, "MessageID"));      // Read in the output parameter value
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return this.MessageID > 0; // Return whether ID was returned
   }

   /// <summary>
   /// Updates details for Messages.
   /// </summary>
   /// <returns>True if Update operation is successful; Else False.</returns>
   private bool Update()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_MessagesUpdate");
         this.db.AddInParameter(com, "MessageID", DbType.Int32, this.MessageID);

         this.db.AddInParameter(com, "IsRead", DbType.Boolean, this.IsRead);
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Deletes details of Messages for provided MessageID.
   /// </summary>
   /// <returns>True if Delete operation is successful; Else False.</returns>
   public bool Delete()
   {
      try
      {
         DbCommand com = this.db.GetStoredProcCommand("usp_MessagesDelete");
         this.db.AddInParameter(com, "MessageID", DbType.Int32, this.MessageID);
         db.AddInParameter(com, "IsActive", DbType.Boolean, IsActive);
         this.db.ExecuteNonQuery(com);
      }
      catch (Exception ex)
      {
         // To Do: Handle Exception
         return false;
      }

      return true;
   }

   /// <summary>
   /// Get list of Messages for provided parameters.
   /// </summary>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetList()
   {
      DataSet ds = null;
      try
      {
         DbCommand com = db.GetStoredProcCommand("upMessagesGetList");
         ds = db.ExecuteDataSet(com);
      }
      catch (Exception ex)
      {
         //To Do: Handle Exception
      }

      return ds;
   }

   /// <summary>
   /// Get list of Messages for provided parameters optimized for custom paging technique.
   /// </summary>
   /// <param name="filter">Provide expression for filter result</param>
   /// <param name="sort">Provide expression to sort result</param>
   /// <param name="pageSize">Provide page size for custom paging.</param>
   /// <param name="pageNumber">Provide page number for custom paging</param>
   /// <param name="rowCount">Get total count of rows in result </param>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   public DataSet GetPagingList(string message, int UsersId, int pageNumber, int pageSize)
   {
      DataSet ds = null;
      try
      {
          DbCommand com = db.GetStoredProcCommand("usp_MessagesGetList");
         if (!string.IsNullOrEmpty(message))
         {
             db.AddInParameter(com, "message", DbType.String, message);
         }
         else
         {
             db.AddInParameter(com, "message", DbType.String, DBNull.Value);
         }
         if (UsersId > 0)
             db.AddInParameter(com, "Userid", DbType.Int32 , UsersId);
         else
             db.AddInParameter(com, "Userid", DbType.Int32, 0);
         if (pageNumber > 0)
             db.AddInParameter(com, "Pageno", DbType.Int32, pageNumber);
         else
             db.AddInParameter(com, "Pageno", DbType.Int32, 1);
         if (pageSize > 0)
             db.AddInParameter(com, "PageSize", DbType.Int32, pageSize);
         else
             db.AddInParameter(com, "PageSize", DbType.Int32, 5);
        
         ds = db.ExecuteDataSet(com);
      }
      catch (Exception ex)
      {
         //To Do: Handle Exception
      }

      return ds;
   }

   /// <summary>
   /// Get list of Messages for provided parameters optimized for custom paging technique.
   /// </summary>
   /// <param name="expression">Provide expression for filter or sort result</param>
   /// <param name="isFilterExpression">If provided expression is filter expression then True; Else if sort expression then false.</param>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
   //public DataSet GetPagingList(string expression, bool isFilterExpression)
   //{
   //   if (isFilterExpression == true)
   //   {
   //      return GetPagingList(expression, "", 0, 0, 0);
   //   }
   //   else
   //   {
   //      return GetPagingList("", expression, 0, 0, 0);
   //   }
   //}

   /// <summary>
   /// Get list of Messages for provided parameters optimized for custom paging technique.
   /// </summary>
   /// <param name="pageSize">Provide page size for custom paging.</param>
   /// <param name="pageNumber">Provide page number for custom paging</param>
   /// <param name="rowCount">Get total count of rows in result </param>
   /// <returns>DataSet of result</returns>
   /// <remarks></remarks>
//   public DataSet GetPagingList(int pageSize, int pageNumber, int rowCount)
//   {
//      return GetPagingList("", "", pageSize, pageNumber, rowCount);
//   }
#endregion

#endregion
}
