﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SettingMaster.master" AutoEventWireup="true" CodeFile="Tenants.aspx.cs" Inherits="Admin_Tenants" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/UserControl/ucAddNewBuyerTenantAdmin.ascx" TagName="AddNewBuyerTenant" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../Js/CommonValidations.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
    <style type="text/css">
        .removeunderline {
            text-decoration: none;
        }
    </style>
     <script type="text/ecmascript">
         function ClearControl() {

             $('#<%=txtSearchName.ClientID %>').val("");
              $('#<%=txtSearchCompanyName.ClientID%>').val("");
              $('#<%=txtSearchCity.ClientID%>').val("");
              document.getElementById('<%=ddlStatesearch.ClientID %>').selectedIndex = -1

          }
    </script>
    <script type="text/javascript">
        function SetActive(PartyID) {
               
            $.ajax({
                type: "POST",
                url: "Tenants.aspx/SetActive",
                data: '{PartyID: "' + PartyID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                failure: function (response) {
                    alert(response);
                }
            });
            return false;
        }
        </script>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <table width="100%">

        <tr>
            <td>
                <br />
            </td>

        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>Name:</td>
                        <td>
                            <asp:TextBox ID="txtSearchName" runat="server"></asp:TextBox></td>
                        <td>Company:</td>
                        <td><asp:TextBox ID="txtSearchCompanyName" runat="server"></asp:TextBox></td>
                        <td>
                            <asp:Button ID="btnSearchlocation" CssClass="SqureButton" Text="SEARCH" OnClick="btnSearchBuyer_Click" runat="server" />
                            <asp:Button ID="btnclear" runat="server" Text="RESET" CssClass="SqureButton" OnClientClick="ClearControl();" OnClick="btnclear_Click" />

                        </td>
                    </tr>
                    <tr>
                          <td>State:</td>
                        <td>
                            <asp:DropDownList ID="ddlStatesearch" runat="server">
                            </asp:DropDownList>
                            <%-- <asp:TextBox ID="txtLocationSearchState" runat="server"></asp:TextBox>--%>
                        </td>
                        <td>City:</td>
                        <td>
                            <asp:TextBox ID="txtSearchCity" runat="server"></asp:TextBox>
                        </td>
                        <%--<td>Zip Code: </td>
                        <td>
                            <asp:TextBox ID="txtSearchZip" runat="server"></asp:TextBox>
                        </td>--%>
                    </tr>
                </table>
            </td>

        </tr>
        <tr>
            <td>
                <br />
            </td>

        </tr>
        <tr>
           <td><table width="100%"><tr> <td align="left">
              <%--  <asp:Label ID="lblPages" runat="server" Font-Bold="true"></asp:Label>--%>
            </td>
            <td align="right">
                <ajax:ModalPopupExtender ID="mpeAddNewTenants" TargetControlID="lbDummybtn" CancelControlID="btnCancel"
                    PopupControlID="divNewBuyer" runat="server" BackgroundCssClass="modalBackground">
                </ajax:ModalPopupExtender>
                <asp:Button ID="btnaddNewBuyer" runat="server" Text="ADD NEW BUYER" CssClass="SqureButton" OnClick="btnaddNewBuyer_Click" />
                <asp:Label ID="lbDummybtn" runat="server"></asp:Label>
            </td></tr></table></td>

        </tr>
        <tr>
            <td>
               
                <asp:GridView ID="grdBuyers" runat="server" Width="100%" OnPageIndexChanging="grdBuyers_PageIndexChanging"
                     OnRowCommand="grdBuyers_RowCommand"
                     OnRowCreated="grdBuyers_RowCreated" OnRowDataBound="grdBuyers_RowDataBound"
                     AutoGenerateColumns="false"
                    HeaderStyle-CssClass="HeaderGridView"
                    FooterStyle-CssClass="FooterGridView"
                    AllowPaging="true" PageSize="25"
                    RowStyle-CssClass="RowGridView" AlternatingRowStyle-CssClass="AlternatingRowGridView2">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="ACTIVE" ItemStyle-Width="20px">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkActive" runat="server" Height="15px" Width="15px" CommandName="IsActive" Checked='<%# Eval("IsActive") %>' CommandArgument='<%# Eval("PartyID") %>' onchange='<%# Eval("PartyID","return SetActive({0})") %>' />
                         </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="PartyCompanyName" HeaderText="Company Name" />
                        <asp:BoundField DataField="PartyName" HeaderText="Name" />
                      <%--  <asp:BoundField DataField="PartyTypeId" HeaderText="PartyTypeId" />--%>
                        <asp:BoundField DataField="Email" HeaderText="Email" />
                        <asp:BoundField DataField="City" HeaderText="City" />
                        <asp:BoundField DataField="StateName" HeaderText="State" />
                        <asp:BoundField DataField="ZipCode" HeaderText="ZipCode" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnPartyId" runat="server" Value='<%# Eval("PartyID") %>' />
                                <asp:ImageButton runat="server" ImageUrl="~/Images/edit.jpg" ID="btnEdit" CommandName="Ed" CommandArgument='<%# Eval("PartyID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnCanDel" runat="server" Value='<%# Eval("CanDelete") %>' />
                                <asp:ImageButton runat="server" ImageUrl="~/Images/delete.gif" Height="16px" Width="16px" OnClientClick='javascript:return confirm("Do You Want To Delete This Record");' ID="delRecord" CommandName="Del" CommandArgument='<%# Eval("PartyID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        </Columns>
                     <PagerTemplate>
                    </PagerTemplate>
                    <PagerStyle CssClass="pagerStyle"  />
                    </asp:GridView>
            </td>
        </tr>
        </table>
     <div id="divNewBuyer" class="PopupDivBodyStyle" style="height: auto; width: 450px; display: none;">
        <%--class="spacing"--%>
        <table style="width: 100%;" cellspacing="0" cellpadding="0">
            <tr style="border-bottom: solid; background-color: gray;">
                <td style="text-align: left;">
                    <asp:UpdatePanel ID="upTitle" runat="server">
                        <ContentTemplate>
                            <h3>&nbsp; &nbsp;      
                                <asp:Label ID="lbTitle" runat="server"></asp:Label></h3>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="right">
                    <asp:ImageButton ImageUrl="~/Images/close.png" ID="btnCancel" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc:AddNewBuyerTenant ID="ucAddNewBuyerTenant" runat="server" />

                </td>
            </tr>
        </table>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" Runat="Server">
</asp:Content>

