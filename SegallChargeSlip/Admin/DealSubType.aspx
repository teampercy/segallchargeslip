﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SettingMaster.master" AutoEventWireup="true" CodeFile="DealSubType.aspx.cs" Inherits="Admin_DealSubType" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <script type ="text/javascript" >
         function Validate() {
             var DealSubType = document.getElementById('<%=txtDealSubType.ClientID%>');
             var DealType = document.getElementById('<%=ddlDealType.ClientID %>');
             if (DealType.selectedIndex == -1 || DealType.selectedIndex == 0) {
                 alert('Please Select Deal Type');
                 return false;
             }
             if ($.trim(DealSubType.value) == "") {
                alert('Please Enter Deal SubType');
                DealSubType.focus();
                return false;
             }
             
            return true;
         }
         function SetActive(DealSubTypeID) {
             //alert(locationid );
             $.ajax({
                 type: "POST",
                 url: "DealSubType.aspx/SetActive",
                 data: '{SubTypeID: "' + DealSubTypeID + '"}',
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 failure: function (response) {
                     alert(response);
                 }
             });
             return false;
         }
    </script>
    <table style="width: 97%;">
        <tr>
            <td align="right">
                <asp:Button ID="btnAddDealSubType" runat="server" Text="ADD DEAL SUBTYPE" OnClick="btnAddDealSubType_Click" CssClass="SqureButton" />
                <%--  <asp:LinkButton ID="lnkAddDealType" runat ="server" Text="AddDealType" OnClick ="lnkAddDealType_Click"></asp:LinkButton></td></tr>--%>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="grdDealSubType" runat="server" AutoGenerateColumns="false" Width="100%"
                    OnRowCommand="grdDealSubType_RowCommand" OnRowDataBound ="grdDealSubType_RowDataBound"
                    EmptyDataRowStyle-HorizontalAlign="Center"
                    CssClass="AllBorder"
                    HeaderStyle-CssClass="HeaderGridView" FooterStyle-CssClass="FooterGridView"
                    RowStyle-CssClass="RowGridView" AlternatingRowStyle-CssClass="AlternatingRowGridView2">
                    <Columns>
                         <asp:TemplateField  ItemStyle-HorizontalAlign ="Center" HeaderText="ACTIVE" ItemStyle-Width ="20px">
                            <ItemTemplate >
                                <asp:CheckBox ID="chkActive" runat ="server" Height="15px" Width ="15px" CommandArgument='<%# Eval("DealSubTypeID") %>' onchange='<%# Eval("DealSubTypeID","return SetActive({0})") %>'/>
                                <asp:HiddenField ID="hdnActive" runat ="server" Value='<% #Bind("IsActive") %>'/>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:BoundField DataField="DealType" HeaderText="DEAL TYPE" />
                        <asp:BoundField DataField="DealSubType" HeaderText="DEAL SUBTYPE" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnDealSubTypeId" runat="server" Value ='<%# Eval("DealSubTypeID") %>'/>
                                <asp:ImageButton runat="server" ImageUrl="~/Images/edit.jpg" ID="btnEdit" CommandName="Ed" CommandArgument='<%# Eval("DealSubTypeID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                 <asp:HiddenField ID="hdnCanDel" runat="server" Value='<%# Eval("CanDelete") %>' />
                                <asp:ImageButton runat="server" ImageUrl="~/Images/delete.gif" OnClientClick='javascript:return confirm("Do You Want To Delete This Record");' ID="delRecord" CommandName="Del" CommandArgument='<%# Eval("DealSubTypeID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpeUpdateDealSubType" TargetControlID="lbDummy" CancelControlID="imgbtnClose"
        PopupControlID="divPopUp" runat="server" BackgroundCssClass="modalBackground">
    </ajax:ModalPopupExtender>
    <asp:Label ID="lbDummy" runat="server"></asp:Label>
    <div id="divPopUp" class="PopupDivBodyStyle" style="width: 350px; height: 250px; overflow: auto; background-color: white; z-index: 111; position: absolute; display: block;">
        <table style="width: 100%">
            <tr style="background-color: gray; border-bottom: solid;">
                <td colspan="2">
                    <table>
                        <tr>
                            <td style="text-align: left; width: 490px;">
                                <asp:Label ID="lblHeader" runat="server" Text="DEAL SUBTYPE" ForeColor="Black" Style="padding-left: 10px; vertical-align: middle; font-weight: bold;"></asp:Label>
                            </td>
                            <td style="text-align: right;">
                                <asp:ImageButton ID="imgbtnClose" runat="server" ImageUrl="~/Images/Close.png" AlternateText="Close"
                                    ForeColor="Red" ImageAlign="Right" Style="margin-top: 4px" />
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="Paddingtd">
                    <asp:Label ID="lblErrorMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 20px;">Deal Type</td>
                <td>
                    <asp:DropDownList ID="ddlDealType" runat="server" Width="156px" Height="24px"></asp:DropDownList>
                </td>
            </tr>
            <tr><td><br /></td></tr>
            <tr>
                <td style="padding-left: 20px;">Deal SubType</td>
                <td>
                    <asp:TextBox ID="txtDealSubType" runat="server" Height="18px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td style="padding-left: 20px;">IsActive</td>
                <td>
                   <asp:CheckBox ID="chkIsActive" runat ="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td></td>
                <td >
                    <asp:Button ID="btnUpdate" runat="server" Text="ADD" CssClass="SqureButton" Width ="100px" OnClick="btnUpdate_Click" OnClientClick ="return Validate();"/>
                </td>
            </tr>
        </table>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" runat="Server">
</asp:Content>

