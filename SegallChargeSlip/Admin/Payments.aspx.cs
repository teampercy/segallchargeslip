﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_Payments : System.Web.UI.Page
{
    decimal totalGross = 0;
    decimal totalPaidAmount = 0;
    decimal totalBalance = 0;
    decimal commissiondue = 0;
    decimal percentdue = 0;
    decimal received = 0;
    decimal newpayment = 0;
    decimal unpaidamount = 0;
    decimal ShareAmount = 0;
    decimal TotalCreditAmount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            SetAttributes();
            if (Session["UserId"] == null)
            {

                Session.Clear();
                Session.RemoveAll();
                Server.Transfer("../Login.aspx");

            }

            btnApplyPayment.Visible = false;
            if (!Page.IsPostBack)
            {
                string strChargeslipid = string.Empty;
                strChargeslipid = Request.QueryString["ChargeslipId"];
                if (!string.IsNullOrEmpty(strChargeslipid))
                {
                    DueDateWiseCommission obj = new DueDateWiseCommission();
                    DataTable dt = new DataTable();
                    dt = obj.usp_GetCompIdByDealName(Convert.ToInt32(strChargeslipid));
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            Payments objPayment = new Payments();
                            DataSet ds = objPayment.GetListDealName(Convert.ToInt32(dt.Rows[0]["CompanyId"].ToString()));
                            hdnCompId.Value = dt.Rows[0]["CompanyId"].ToString();
                            DataTable dtDealName = null;
                            if (ds != null)
                            {
                                dtDealName = ds.Tables[0];
                                if (dtDealName.Rows.Count > 0)
                                {
                                    Utility.FillDropDown(ref ddlDealName, dtDealName, "DealName", "ChargeSlipID");


                                }
                                else
                                {
                                    Utility.FillDropDown(ref ddlDealName, dtDealName, "DealName", "ChargeSlipID");
                                }
                                txtReceivedFrom.Text = dt.Rows[0]["Company"].ToString();
                                ddlDealName.SelectedValue = strChargeslipid;
                                if (ddlDealName.SelectedItem != null)
                                {
                                    btnApplyPayment.Visible = true;
                                    trDealName.Visible = true;
                                    lbErrorMsg.Text = string.Empty;
                                    lblDealName.Text = ddlDealName.SelectedItem.Text.ToString();
                                    BindGrid();
                                }

                                if (ddlDealName.SelectedIndex == -1)
                                {
                                    lblMessage.Text = string.Empty;
                                    grdCommission.DataSource = null;
                                    grdCommission.DataBind();
                                    txtBalance.Text = "";
                                    lbErrorMsg.Text = string.Empty;
                                }
                            }
                        }
                    }
                }
                // BindDropDown();
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "Page_Load", ex.Message);
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
    }
    private void SetAttributes()
    {
        txtAmount.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txtAmount.ClientID + "')");
        txtCheckno.Attributes.Add("onkeyup", "javascript:return AllowIntergers('" + txtCheckno.ClientID + "')");
        // txtDealName.Attributes.Add("onkeyup", "javascript:return SetContextKey('" + this.ClientID + "')");
    }
    public void BindDropDown()
    {
        try
        {
            CustomList objCL = new CustomList();
            //Utility.FillDropDown(ref ddlReceivedFrom, objCL.GetCompanyList(), "Company", "CompanyID");
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "BindDropDown", ex.Message);
        }



    }
    //protected void ddlReceivedFrom_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        CustomList objCL = new CustomList();
    //        DataTable dtDealName = objCL.GetDealName();
    //        DataView dtView = dtDealName.DefaultView;
    //        string SelectedValue = (ddlReceivedFrom.SelectedValue.ToString() == "") ? "0" : ddlReceivedFrom.SelectedValue;
    //        dtView.RowFilter = "CompanyID=" + SelectedValue;
    //        Utility.FillDropDown(ref ddlDealName, dtView.ToTable(), "DealName", "ChargeSlipID");
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorLog.WriteLog("Payments.aspx", "ddlReceivedFrom_SelectedIndexChanged", ex.Message);
    //    }
    //}
    protected void ddlDealName_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            if (ddlDealName.SelectedItem != null)
            {
                btnApplyPayment.Visible = true;
                trDealName.Visible = true;
                lbErrorMsg.Text = string.Empty;
                lblDealName.Text = ddlDealName.SelectedItem.Text.ToString();
                BindGrid();
            }

            if (ddlDealName.SelectedIndex == -1)
            {
                lblMessage.Text = string.Empty;
                grdCommission.DataSource = null;
                grdCommission.DataBind();
                txtBalance.Text = "";
                lbErrorMsg.Text = string.Empty;
            }



        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "BindDropDown", ex.Message);
        }
    }

    protected void BindGrid()
    {
        try
        {
            DueDateWiseCommission obj = new DueDateWiseCommission();
            DataTable dtDueDate = new DataTable();
            //if (hdnSelected != null)
            //{
            //    if (hdnSelected.Value.ToString() == "Selected")
            //    {
            dtDueDate = obj.GetCommissionDueDateByChargeSlip(Convert.ToInt32((ddlDealName.SelectedValue.ToString() == "") ? "0" : ddlDealName.SelectedValue));
            // dtDueDate = obj.GetCommissionDueDateByChargeSlip(Convert.ToInt32((hdnDealId.Value.ToString() == "") ? "0" : hdnDealId.Value));
            if (dtDueDate != null && dtDueDate.Rows.Count > 0)
            {
                hdnChargeDate.Value = dtDueDate.Rows[0]["ChargeDate"].ToString();
                grdCommission.DataSource = dtDueDate;
                grdCommission.DataBind();
                // hdnSelected.Value = "Not Selected";
            }
            else
            {
                grdCommission.DataSource = null;
                grdCommission.DataBind();
                txtBalance.Text = "";
            }
            //    }
            //    else
            //    {
            //        grdCommission.DataSource = null;
            //        grdCommission.DataBind();
            //        txtBalance.Text = "";
            //        hdnSelected.Value = "Not Selected";
            //    }

            //}
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "BindGrid", ex.Message);
        }
    }
    protected void btnSaveAndClose_Click(object sender, EventArgs e)
    {

    }
    protected void btnSaveAndNew_Click(object sender, EventArgs e)
    {

    }
    protected void btnApplyPayment_Click(object sender, EventArgs e)
    {
        try
        {
            //if (DateTime.Now <= DateTime.Parse("2014/02/03"))
            //{
            btnApplyPayment.Visible = true;
            int i = 0;
            decimal Gross = 0;
            decimal PaidAmount = 0;
            string strCommissionDueId = string.Empty;
            foreach (GridViewRow row in grdCommission.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chkSelect") as CheckBox;
                Label lblPaymentAmount = (Label)row.FindControl("lblPaymentAmount") as Label;
                Label lblCommAmountDue = (Label)row.FindControl("lblCommAmountDue") as Label;
                HiddenField hdnCommissionDueId = (HiddenField)row.FindControl("hdnCommissionDueId") as HiddenField;
                if (chk.Checked == true && chk.Enabled == true)
                {
                    i = i + 1;
                    if (lblCommAmountDue.Text != null && lblCommAmountDue.Text != "")
                    {
                        Gross += Convert.ToDecimal(lblCommAmountDue.Text);
                    }
                    if (lblPaymentAmount.Text == "" || lblPaymentAmount.Text == null)
                    {
                        PaidAmount = 0;
                    }
                    else
                    {
                        PaidAmount += Convert.ToDecimal(lblPaymentAmount.Text);
                    }
                    strCommissionDueId += hdnCommissionDueId.Value + ",";
                }
            }
            if (i == 0)
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Please check at least one record for Apply Payment.";
                return;
            }
            decimal Amount = Gross - PaidAmount;
            //commented by jaywanti on 27-11-2014
            //if (Convert.ToDecimal(txtAmount.Text.Trim().ToString()) > Amount)
            //{
            //    lblMessage.Visible = true;
            //    lblMessage.Text = "please enter amount less than or equal to  remaining paid amount.";
            //    return;
            //}
            DueDateWiseCommission obj = new DueDateWiseCommission();
            lblDealNames.Text = ddlDealName.SelectedItem.Text;
            //lblDealNames.Text = txtDealName.Text;
            DataTable dt = new DataTable();

            ChargeSlip objChargeSlip = new ChargeSlip();
            objChargeSlip.ChargeSlipID = Convert.ToInt32(ddlDealName.SelectedValue.ToString());
            objChargeSlip.Load();
            //lblReceivedFrom.Text = objChargeSlip.Company + "</br>" + objChargeSlip.CTOwnershipEntity + "</br>" + objChargeSlip.CTAddress1 + " " + objChargeSlip.CTAddress2 + "</br> " + objChargeSlip.CTCity + "," + objChargeSlip.CTState + " " + objChargeSlip.CTZipCode;
            if (!string.IsNullOrEmpty(objChargeSlip.Company))
            {
                if (!string.IsNullOrEmpty(objChargeSlip.CTOwnershipEntity))
                {
                    lblReceivedFrom.Text = objChargeSlip.Company + "<br>" + objChargeSlip.CTOwnershipEntity + "<br>" + objChargeSlip.CTAddress1 + " " + objChargeSlip.CTAddress2 + "<br> " + objChargeSlip.CTCity + "," + objChargeSlip.CTState + " " + objChargeSlip.CTZipCode;


                }
                else
                {
                    lblReceivedFrom.Text = objChargeSlip.Company + "<br>" + objChargeSlip.CTAddress1 + " " + objChargeSlip.CTAddress2 + "<br> " + objChargeSlip.CTCity + "," + objChargeSlip.CTState + " " + objChargeSlip.CTZipCode;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(objChargeSlip.CTOwnershipEntity))
                {
                    lblReceivedFrom.Text = objChargeSlip.CTOwnershipEntity + "<br>" + objChargeSlip.CTAddress1 + " " + objChargeSlip.CTAddress2 + "<br> " + objChargeSlip.CTCity + "," + objChargeSlip.CTState + " " + objChargeSlip.CTZipCode;


                }
                else
                {
                    lblReceivedFrom.Text = objChargeSlip.CTName + "<br>" + objChargeSlip.CTAddress1 + " " + objChargeSlip.CTAddress2 + "<br> " + objChargeSlip.CTCity + "," + objChargeSlip.CTState + " " + objChargeSlip.CTZipCode;
                }
            }
            // Company objCom = new Company(Convert.ToInt32(hdnCompId.Value.ToString()));
            // objCom.Load();
            //lblReceivedFrom.Text = objCom.CompanyName + "</br>" + objCom.Address1 + " " + objCom.Address2 + "</br> " + objCom.City + "," + objCom.State + " " + objCom.ZipCode;
            lblPmtDate.Text = txtPMTDate.Text;
            lblNewPayment.Text = "$" + String.Format("{0:n2}", Convert.ToDecimal(txtAmount.Text)).ToString();
            lblPmtMethod.Text = ddlPMTMethod.SelectedItem.Text;
            lblCheckNo.Text = txtCheckno.Text;
            lblPayAmountDue.Text = String.Format("{0:n2}", Convert.ToDecimal(hdnlblPayAmountDue.Value)).ToString();
            lblPayAmount.Text = String.Format("{0:n2}", Convert.ToDecimal(txtAmount.Text)).ToString();
            lblRemBal.Text = String.Format("{0:n2}", Convert.ToDecimal(hdnlblRemBal.Value)).ToString();
            //lblRemainingBalanceCredit.Text = String.Format("{0:n2}", Convert.ToDecimal(hdnlblRemBal.Value)).ToString();
            lblRemainingBalanceCredit.Text = "$ " + String.Format("{0:n2}", Convert.ToDecimal(hdnUnappliedDet.Value)).ToString();
            dt = obj.GetPaymentDistributionGetDetails(Convert.ToInt32((ddlDealName.SelectedValue.ToString() == "") ? "0" : ddlDealName.SelectedValue), Convert.ToDouble(txtAmount.Text.ToString()), strCommissionDueId);
            // dt = obj.GetPaymentDistributionGetDetails(Convert.ToInt32((hdnDealId.Value.ToString() == "") ? "0" : hdnDealId.Value), Convert.ToDouble(txtAmount.Text.ToString()));
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    grdPaymentDetail.DataSource = null;
                    grdPaymentDetail.DataBind();
                    grdPaymentDetail.DataSource = dt;
                    grdPaymentDetail.DataBind();
                    grdPaymentDistributionCoBrokerDetail.DataSource = null;
                    grdPaymentDistributionCoBrokerDetail.DataBind();
                    DataView view = new DataView();
                    view.Table = dt;
                    string strquery = string.Empty;
                    strquery = "BrokerTypeId=4 OR BrokerTypeId=5 ";
                    view.RowFilter = strquery;
                    grdPaymentDistributionCoBrokerDetail.DataSource = view.ToTable();
                    grdPaymentDistributionCoBrokerDetail.DataBind();
                }
            }
            hdnBalanceSelected.Value = "";
            mpePaymentDetail.Show();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "DispalySummary", "DispalySummary();", true);
            //}
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "btnApplyPayment_Click", ex.Message);
        }
        //try {
        //    int i = 0;
        //    decimal Gross = 0;
        //    decimal PaidAmount = 0;

        //    foreach (GridViewRow row in grdCommission.Rows)
        //    {
        //        CheckBox chk = (CheckBox)row.FindControl("chkSelect") as CheckBox;
        //        Label lblPaymentAmount = (Label)row.FindControl("lblPaymentAmount") as Label;
        //        Label lblCommAmountDue = (Label)row.FindControl("lblCommAmountDue") as Label;
        //        if (chk.Checked == true && chk.Enabled == true)
        //        {
        //            i = i + 1;
        //            if (lblCommAmountDue.Text != null && lblCommAmountDue.Text != "")
        //            {
        //                Gross += Convert.ToDecimal(lblCommAmountDue.Text);
        //            }
        //            if (lblPaymentAmount.Text == "" || lblPaymentAmount.Text == null)
        //            {
        //                PaidAmount = 0;
        //            }
        //            else
        //            {
        //                PaidAmount += Convert.ToDecimal(lblPaymentAmount.Text);
        //            }
        //        }
        //    }
        //    if (i == 0)
        //    {
        //        lblMessage.Visible = true;
        //        lblMessage.Text = "Please check at least one record for Apply Payment.";
        //        return;
        //    }
        //    decimal Amount = Gross - PaidAmount;
        //    if (Convert.ToDecimal(txtAmount.Text.ToString()) > Amount)
        //    {
        //        lblMessage.Visible = true;
        //        lblMessage.Text = "Please Enter Amount Less than or equal to  Remaining Paid Amount.";
        //        return;
        //    }
        //    decimal remainingbalance=Convert.ToDecimal(txtAmount.Text.ToString());
        //    foreach (GridViewRow row in grdCommission.Rows)
        //    {
        //        CheckBox chk = (CheckBox)row.FindControl("chkSelect") as CheckBox;
        //        if (chk.Checked == true && chk.Enabled == true)
        //        {
        //            Label lblUNAPPLIED = (Label)row.FindControl("lblUNAPPLIED") as Label;
        //            if (remainingbalance >= Convert.ToDecimal(lblUNAPPLIED.Text.ToString()))
        //            {
        //                remainingbalance -= Convert.ToDecimal(lblUNAPPLIED.Text.ToString());
        //                HiddenField hdnCommissionDueId = (HiddenField)row.FindControl("hdnCommissionDueId") as HiddenField;
        //                HiddenField hdnDealTransactionId = (HiddenField)row.FindControl("hdnDealTransactionId") as HiddenField;
        //                HiddenField hdnPaymentId = (HiddenField)row.FindControl("hdnPaymentId") as HiddenField;
        //                Payments obj = new Payments();
        //                obj.CommissionDueId = Convert.ToInt32(hdnCommissionDueId.Value.ToString());
        //                obj.ChargeSlipId = Convert.ToInt32(ddlDealName.SelectedValue.ToString());
        //                obj.DealTransactionId = Convert.ToInt32(hdnDealTransactionId.Value.ToString());
        //                obj.ReceivedFrom = Convert.ToInt32(ddlReceivedFrom.SelectedValue.ToString());
        //                obj.PaymentAmount = Convert.ToDecimal(lblUNAPPLIED.Text.ToString());
        //                obj.PaymentMethod = Convert.ToInt32(ddlPMTMethod.SelectedValue.ToString());
        //                obj.BalanceAmount = 0;
        //                obj.PaymentDate = Convert.ToDateTime(txtPMTDate.Text.ToString());
        //                obj.CheckNo = txtCheckno.Text.ToString();
        //                obj.PaymentAppliedBy = Convert.ToInt32(Session["UserID"].ToString());
        //                obj.PaymentStatus = 1;
        //                obj.Save();
        //                lblMessage.Text = string.Empty;

        //            }
        //            else
        //            {

        //                decimal remaining = System.Math.Abs(remainingbalance);
        //                if (remaining == 0)
        //                {
        //                    BindGrid();
        //                    return;
        //                }
        //                remainingbalance -= remaining;
        //                HiddenField hdnCommissionDueId = (HiddenField)row.FindControl("hdnCommissionDueId") as HiddenField;
        //                HiddenField hdnDealTransactionId = (HiddenField)row.FindControl("hdnDealTransactionId") as HiddenField;
        //                HiddenField hdnPaymentId = (HiddenField)row.FindControl("hdnPaymentId") as HiddenField;
        //                Payments obj = new Payments();
        //                obj.CommissionDueId = Convert.ToInt32(hdnCommissionDueId.Value.ToString());
        //                obj.ChargeSlipId = Convert.ToInt32(ddlDealName.SelectedValue.ToString());
        //                obj.DealTransactionId = Convert.ToInt32(hdnDealTransactionId.Value.ToString());
        //                obj.ReceivedFrom = Convert.ToInt32(ddlReceivedFrom.SelectedValue.ToString());
        //                obj.PaymentAmount = remaining;
        //                obj.PaymentMethod = Convert.ToInt32(ddlPMTMethod.SelectedValue.ToString());
        //                obj.BalanceAmount = Convert.ToDecimal(lblUNAPPLIED.Text.ToString()) - remaining;
        //                obj.PaymentDate = Convert.ToDateTime(txtPMTDate.Text.ToString());
        //                obj.CheckNo = txtCheckno.Text.ToString();
        //                obj.PaymentAppliedBy = Convert.ToInt32(Session["UserID"].ToString());
        //                obj.PaymentStatus = 0;
        //                obj.Save();
        //                lblMessage.Text = string.Empty;
        //            }

        //        }
        //    }
        //    BindGrid();

        //}
        //catch (Exception ex)
        //{
        //    ErrorLog.WriteLog("Payments.aspx", "btnApplyPayment_Click", ex.Message);
        //}
    }

    protected void grdCommission_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnPaymentStatus = (HiddenField)e.Row.FindControl("hdnPaymentStatus") as HiddenField;
                HiddenField hdnIsFullCommPaid = (HiddenField)e.Row.FindControl("hdnIsFullCommPaid") as HiddenField;
                Label lblPaymentDate = (Label)e.Row.FindControl("lblPaymentDate") as Label;
                Label lblPaymentAmount = (Label)e.Row.FindControl("lblPaymentAmount") as Label;
                Label lblUNAPPLIED = (Label)e.Row.FindControl("lblUNAPPLIED") as Label;
                Label lblCommAmountDue = (Label)e.Row.FindControl("lblCommAmountDue") as Label;
                CheckBox chk = (CheckBox)e.Row.FindControl("chkSelect") as CheckBox;
                Label lblCredit = (Label)e.Row.FindControl("lblCredit") as Label;
                if (lblCredit != null)
                {
                    if (lblCredit.Text != "")
                    {
                        TotalCreditAmount += Convert.ToDecimal(lblCredit.Text);
                    }
                }
                if (lblCommAmountDue.Text != null && lblCommAmountDue.Text != "")
                {
                    totalGross += Convert.ToDecimal(lblCommAmountDue.Text);
                }

                if (hdnIsFullCommPaid != null)
                {
                    if (hdnIsFullCommPaid.Value == "True")
                    {
                        chk.Enabled = true;
                        lblUNAPPLIED.Text = "0.00";
                    }
                }
                if (lblPaymentDate.Text == "" || lblPaymentDate.Text == null)
                {
                    lblPaymentDate.Text = "N/A";
                }

                if (lblPaymentAmount.Text == "" || lblPaymentAmount.Text == null)
                {
                    lblPaymentAmount.Text = "0.00";
                }
                else
                {
                    totalPaidAmount += Convert.ToDecimal(lblPaymentAmount.Text);
                }
                if (lblPaymentAmount.Text == lblCommAmountDue.Text)
                {
                    chk.Enabled = true;
                }
                else
                {
                    chk.Enabled = true;
                }
                if (lblUNAPPLIED.Text == "" || lblUNAPPLIED.Text == null)
                {
                    if (lblPaymentAmount.Text == "" || lblPaymentAmount.Text == null)
                    {
                        lblUNAPPLIED.Text = String.Format("{0:n2}", Convert.ToDecimal(lblCommAmountDue.Text));

                    }
                    else
                    {
                        lblUNAPPLIED.Text = String.Format("{0:n2}", (Convert.ToDecimal(lblCommAmountDue.Text) - Convert.ToDecimal(lblPaymentAmount.Text))).ToString();
                    }

                    totalBalance += Convert.ToDecimal(lblUNAPPLIED.Text);
                }
                else
                {
                    totalBalance += Convert.ToDecimal(lblUNAPPLIED.Text);
                }
                //if (hdnIsFullCommPaid != null)
                //{
                //    if (hdnIsFullCommPaid.Value == "True")
                //    {
                //        chk.Enabled = false;
                //        lblUNAPPLIED.Text = "0.00";
                //    }
                //}
                //if (hdnPaymentStatus.Value.ToString() == "1")
                //{

                //    chk.Enabled = false;
                //}
                //else
                //{
                //    chk.Enabled = true ;
                //}

            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblFoCommAmountDue = (Label)e.Row.FindControl("lblFoCommAmountDue") as Label;
                Label lblFoPaymentAmount = (Label)e.Row.FindControl("lblFoPaymentAmount") as Label;
                Label lblFoUNAPPLIED = (Label)e.Row.FindControl("lblFoUNAPPLIED") as Label;
                Label lblFoCredit = (Label)e.Row.FindControl("lblFoCredit") as Label;
                if (lblFoCredit != null)
                {
                    lblFoCredit.Text = "$" + String.Format("{0:n2}", TotalCreditAmount);
                }
                if (lblFoCommAmountDue != null)
                {
                    lblFoCommAmountDue.Text = "$" + String.Format("{0:n2}", totalGross);
                }
                if (lblFoPaymentAmount != null)
                {
                    lblFoPaymentAmount.Text = "$" + String.Format("{0:n2}", totalPaidAmount);
                }
                if (lblFoUNAPPLIED != null)
                {
                    //txtBalance.Enabled = true ;
                    lblFoUNAPPLIED.Text = "$" + String.Format("{0:n2}", totalBalance);
                    hdnTotalBalance.Value = totalBalance.ToString();
                    txtBalance.Text = String.Format("{0:n2}", totalBalance).ToString();
                    txtBalance.Enabled = false;
                }

            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "grdCommission_RowDataBound", ex.Message);
        }
    }
    //protected void chkSelect_Click(object sender, EventArgs e)
    //{
    //    string  display = "false";
    //    decimal Balance = 0;
    //    decimal Gross = 0;
    //    decimal PaidAmount = 0;
    //    foreach (GridViewRow row in grdCommission.Rows)
    //    {
    //        CheckBox chk = (CheckBox)row.FindControl("chkSelect") as CheckBox;
    //        Label lblPaymentAmount = (Label)row.FindControl("lblPaymentAmount") as Label;
    //        Label lblUNAPPLIED = (Label)row.FindControl("lblUNAPPLIED") as Label;
    //        Label lblCommAmountDue = (Label)row.FindControl("lblCommAmountDue") as Label;

    //        if (ViewState["TotalBalance"] != null)
    //        {
    //            Balance = Convert.ToDecimal(ViewState["TotalBalance"].ToString());
    //        }
    //        if (chk.Checked == true)
    //        {
    //            display = "true" ;
    //            if (lblCommAmountDue.Text != null && lblCommAmountDue.Text != "")
    //            {
    //                Gross += Convert.ToDecimal(lblCommAmountDue.Text);
    //            }
    //            if (lblPaymentAmount.Text == "" || lblPaymentAmount.Text == null)
    //            {
    //                PaidAmount = 0 ;
    //            }
    //            else
    //            {
    //                PaidAmount += Convert.ToDecimal(lblPaymentAmount.Text);
    //            }


    //        }


    //    }
    //    lblAmountDueDet.Text = Gross.ToString();
    //    lblPaymentAmountDet.Text = txtAmount.Text;
    //    decimal Unapplied = Gross - PaidAmount - Convert.ToDecimal(txtAmount.Text.ToString());
    //    //decimal Unapplied = Convert.ToDecimal(lblCommAmountDue.Text.ToString()) - Convert.ToDecimal(lblPaymentAmount.Text.ToString()) - Convert.ToDecimal(txtAmount.Text.ToString());
    //    lblUnappliedDet.Text = Unapplied.ToString();
    //    decimal RemainingBalance = Balance - Convert.ToDecimal(txtAmount.Text.ToString());
    //    lblRemainingBalanceDet.Text = RemainingBalance.ToString();
    //    ScriptManager.RegisterStartupScript(upDetailSummary, typeof(string), "ShowPopup", "javascript: DisplaySummary(" + display + ");return false;", true);
    //}
    protected void btnApply_Click(object sender, EventArgs e)
    {
        //decimal TotalPayment = 0;
        //foreach (GridViewRow row in grdPaymentDetail.Rows)
        //{
        //    Label lblNewCommPayment = (Label)row.FindControl("lblNewCommPayment") as Label;
        //    if (Convert.ToDecimal(lblNewCommPayment.Text) > 0)
        //    {
        //        TotalPayment = TotalPayment + Convert.ToDecimal(lblNewCommPayment.Text);
        //    }
        //}
        //foreach (GridViewRow row in grdPaymentDetail.Rows)
        //{
        //    Label lblNewCommPayment =(Label)row.FindControl("lblNewCommPayment") as Label;
        //    if (Convert.ToDecimal(lblNewCommPayment.Text) > 0)
        //    {
        //        //DealBrokerCommissions Update column commission paid
        //        DealBrokerCommissions obj = new DealBrokerCommissions();
        //        HiddenField hdnBrokerId = (HiddenField)row.FindControl("hdnBrokerId") as HiddenField;
        //        obj.BrokerId = Convert.ToInt32(hdnBrokerId.Value .ToString());
        //        obj.ChargeSlipId = Convert.ToInt32(ddlDealName.SelectedValue.ToString());
        //        obj.CommissionPaid = Convert.ToDecimal(lblNewCommPayment.Text);
        //        obj.UpdateDealBrokerCommissions();                

        //    }
        //}
        //Payment Table Insert
        try
        {

            btnApplyPayment.Visible = true;
            bool result = false;
            Payments objPayment = new Payments();
            objPayment.ChargeSlipId = Convert.ToInt32(ddlDealName.SelectedValue.ToString());
            // objPayment.ChargeSlipId = Convert.ToInt32(hdnDealId.Value.ToString());
            if (hdnDealTransId != null)
            {
                if (hdnDealTransId.Value != null)
                {
                    objPayment.DealTransactionId = Convert.ToInt32(hdnDealTransId.Value.ToString());
                }

            }
            objPayment.ReceivedFrom = Convert.ToInt32(hdnCompId.Value.ToString());
            objPayment.PaymentAmount = Convert.ToDecimal(txtAmount.Text.Trim());
            objPayment.PaymentMethod = Convert.ToInt32(ddlPMTMethod.SelectedValue.ToString());
            objPayment.PaymentDate = Convert.ToDateTime(txtPMTDate.Text.Trim());
            objPayment.CheckNo = txtCheckno.Text.Trim();
            objPayment.PaymentAppliedBy = Convert.ToInt32(Session["UserID"].ToString());
            objPayment.Save();
            //PaymentCommssion table insert
            decimal remainingbalance = Convert.ToDecimal(txtAmount.Text.Trim());
            int count = 0;
            foreach (GridViewRow row in grdCommission.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chkSelect") as CheckBox;
                Label lblUNAPPLIED = (Label)row.FindControl("lblUNAPPLIED") as Label; 
                //To check if Unapplied Amount is 0 then does not execute this conditon. If already made payment due date selected 
                //then this should not executed.
                if (chk.Checked == true && chk.Enabled == true && Convert.ToDecimal(lblUNAPPLIED.Text)>0)
                {
                    
                    if (remainingbalance >= Convert.ToDecimal(lblUNAPPLIED.Text.ToString()))
                    {
                        remainingbalance -= Convert.ToDecimal(lblUNAPPLIED.Text.ToString());
                        HiddenField hdnCommissionDueId = (HiddenField)row.FindControl("hdnCommissionDueId") as HiddenField;
                        DueDateWiseCommission objdeal = new DueDateWiseCommission();
                        objdeal.PaymentCommssionInsert(Convert.ToInt32(objPayment.PaymentId), Convert.ToInt32(hdnCommissionDueId.Value), Convert.ToDecimal(lblUNAPPLIED.Text.ToString()));

                        lblMessage.Text = string.Empty;
                        result = true;

                    }
                    else
                    {

                        HiddenField hdnCommissionDueId = (HiddenField)row.FindControl("hdnCommissionDueId") as HiddenField;
                        DueDateWiseCommission objdeal = new DueDateWiseCommission();

                        if (count == 0 && remainingbalance > 0)
                        {
                            objdeal.PaymentCommssionInsert(Convert.ToInt32(objPayment.PaymentId), Convert.ToInt32(hdnCommissionDueId.Value), remainingbalance);
                        }
                        //Credit the remaining Balance
                        string value = hdnBalanceSelected.Value;
                        decimal OutstandingAmounts = 0;// "0";// rdbCreditBalance.SelectedItem.Value.ToString();
                        if (value == "1")
                        {
                            if (Convert.ToDecimal(hdnUnappliedDet.Value) > 0)
                            {
                                int CommissionDueDateId = Convert.ToInt32(hdnCommissionDueId.Value);

                                if (count == 0)
                                {
                                    OutstandingAmounts = Convert.ToDecimal(lblUNAPPLIED.Text) - remainingbalance;
                                    count++;
                                }
                                else
                                {
                                    OutstandingAmounts = Convert.ToDecimal(lblUNAPPLIED.Text);
                                }
                                objPayment.UpdateCreditAmount(CommissionDueDateId, OutstandingAmounts, Convert.ToInt32(Session[GlobleData.User].ToString()));
                            }
                        }
                        hdnUnappliedDet.Value = Convert.ToString(Convert.ToDecimal(hdnUnappliedDet.Value) - OutstandingAmounts);
                        lblMessage.Text = string.Empty;
                        remainingbalance = 0;
                        result = true;
                        //if (remainingbalance == 0)
                        //{
                        //    break;
                        //}
                    }

                }
            }
            //For Chargeslip OverPayment
            //if (DateTime.Now <= DateTime.Parse("2015/03/11"))
            //{
            if (remainingbalance > 0)
            {
                objPayment.UpdateChargeslipOverPayment(objPayment.ChargeSlipId, objPayment.PaymentId, remainingbalance);
            }
            //}
            foreach (GridViewRow row in grdPaymentDetail.Rows)
            {
                HiddenField hdnBrokerId = (HiddenField)row.FindControl("hdnBrokerId") as HiddenField;
                HiddenField hdnTermTypeId = (HiddenField)row.FindControl("hdnTermTypeId") as HiddenField;
                TextBox txtCoCheckNo = (TextBox)row.FindControl("txtCoCheckNo") as TextBox;
                if (txtCoCheckNo != null)
                {
                    if (txtCoCheckNo.Text.Trim() != "")
                    {
                        objPayment.BrokerCheckNumberInsert(objPayment.PaymentId, Convert.ToInt32(hdnBrokerId.Value), txtCoCheckNo.Text.Trim(), Convert.ToInt32(hdnTermTypeId.Value));
                    }
                }
            }
            try
            {
                if (hdnBalanceSelected.Value == "1")
                {
                    lblCreditAmount.Text = "$ " + hdnUnappliedDet.Value;
                    lblRemainingBalanceCredit.Text = "$ 0.00";
                }
                else
                {
                    lblRemainingBalanceCredit.Text = "$ " + hdnUnappliedDet.Value;
                    lblCreditAmount.Text = "$ 0.00";
                }

                //string appPath = HttpContext.Current.Request.ApplicationPath;
                string appPath = System.Configuration.ConfigurationManager.AppSettings["PaymentPath"].ToString();
                string path = Server.MapPath(appPath + ddlDealName.SelectedItem.Text.Trim().Replace("'", "_") + "_" + objPayment.PaymentId + ".pdf");
                string FileName = ddlDealName.SelectedItem.Text.Trim().Replace("'", "_") + "_" + objPayment.PaymentId + ".pdf";
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);


                foreach (GridViewRow row in grdPaymentDetail.Rows)
                {
                    row.Style.Add("font-size", "8px");
                    TextBox txtCheckNo = (TextBox)row.FindControl("txtCoCheckNo");
                    if (txtCheckNo != null)
                    {
                        row.Cells[8].Text = txtCheckNo.Text;
                    }
                }
                GridViewRow HeaderRow = grdPaymentDetail.HeaderRow;
                HeaderRow.Height = 20;
                HeaderRow.Style.Add("font-size", "9px");
                HeaderRow.Style.Add("background-color", "lightgray");
                GridViewRow FooterRow = grdPaymentDetail.FooterRow;
                FooterRow.Height = 20;
                FooterRow.Style.Add("font-size", "9px");
                FooterRow.Cells[2].Style.Add("text-align", "right");
                FooterRow.Cells[4].Style.Add("text-align", "right");
                FooterRow.Cells[5].Style.Add("text-align", "right");
                FooterRow.Cells[6].Style.Add("text-align", "right");
                FooterRow.Cells[7].Style.Add("text-align", "right");

                foreach (GridViewRow row in grdPaymentDistributionCoBrokerDetail.Rows)
                {
                    row.Style.Add("font-size", "8px");
                }
                GridViewRow HeaderRowgrd = grdPaymentDistributionCoBrokerDetail.HeaderRow;
                HeaderRowgrd.Height = 20;
                HeaderRowgrd.Style.Add("font-size", "9px");
                HeaderRowgrd.Style.Add("background-color", "lightgray");

                tblDealDetail.Style.Add("font-size", "9px");


                divHeader.RenderControl(hw);
                divBreakLine.RenderControl(hw);
                divDealNames.Style.Add("padding-left", "10px");
                divDealNames.RenderControl(hw);
                tblDealDetail.RenderControl(hw);
                divBreakLine.RenderControl(hw);
                divPaymDis.RenderControl(hw);
                divBlankForpdf1.RenderControl(hw);
                grdPaymentDetail.RenderControl(hw);
                divBlankForpdf2.RenderControl(hw);
                grdPaymentDistributionCoBrokerDetail.RenderControl(hw);
                divBlankForpdf3.RenderControl(hw);
                tblRemainingDet.Style.Add("width", "70%");
                tblRemainingDet.RenderControl(hw);

                dynamic output = new FileStream(path, FileMode.Create);
                StringReader sr = new StringReader(sw.ToString());
                iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 100f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, output);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();

                objPayment.UpdatePaymentPDFPath(objPayment.PaymentId, FileName);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteLog("Payment.ascx", "btnApply_Click", ex.Message);
            }
            //payment History table insert
            //foreach (GridViewRow row in grdPaymentDetail.Rows)
            //{
            //    Label lblNewCommPayment = (Label)row.FindControl("lblNewCommPayment") as Label;
            //    HiddenField hdnBrokerId = (HiddenField)row.FindControl("hdnBrokerId") as HiddenField;
            //    if (Convert.ToDecimal(lblNewCommPayment.Text) > 0)
            //    {
            //        PaymentHistory obj = new PaymentHistory();
            //        obj.ChargeSlipId = Convert.ToInt32(ddlDealName.SelectedValue.ToString());
            //        obj.BrokerId = Convert.ToInt32(hdnBrokerId.Value.ToString());
            //        obj.PaymentDate =Convert.ToDateTime(txtPMTDate.Text);
            //        obj.PaymentReceived = Convert.ToDecimal(lblNewCommPayment.Text.ToString());
            //        obj.CheckNo = txtCheckno.Text.ToString();
            //        obj.PaymentId = objPayment.PaymentId;
            //        obj.Save();

            //    }
            //}
            //if (hdnTotalBalance != null)
            //{
            //    if (!string.IsNullOrEmpty(hdnTotalBalance.Value))
            //    {
            //        decimal balance = Convert.ToDecimal(hdnTotalBalance.Value.ToString()) - Convert.ToDecimal(txtAmount.Text.ToString());
            //        txtBalance.Text = balance.ToString();
            //    }
            //}
            hdnBalanceSelected.Value = "";
            BindGrid();
            if (result)
            {
                ScriptManager.RegisterStartupScript(this, typeof(string), "Registering", "alert(' Payment for the " + lblDealName.Text + " is applied ');", true);// String.Format("MessagePayment('{0}');", lblDealName.Text), true);
                //ScriptManager.RegisterClientScriptBlock(this, typeof(string), "Registering", "alert(' Payment for the " + lblDealName.Text + " is applied ');", true);// String.Format("MessagePayment('{0}');", lblDealName.Text), true);
                //ScriptManager.RegisterStartupScript(this, typeof(string), "MessagePayment", "String.Format("MessagePayment('{0}');", lblDealName.Text), true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "NewCompanyNotSaved", "alert('the payment for '" + lblDealName.Text + "' has been applied');", true);
                ScriptManager.RegisterStartupScript(this, typeof(string), "PaymentRedirect", "PaymentRedirect()", true);
            }
            //Response.Redirect("Payments.aspx", false);

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "btnApply_Click", ex.Message);
        }
        //
    }
    protected void grdPaymentDetail_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblCommissionDue = (Label)e.Row.FindControl("lblCommissionDue") as Label;
                Label lblBrokerPercent = (Label)e.Row.FindControl("lblBrokerPercent") as Label;
                Label lblCommissionReceived = (Label)e.Row.FindControl("lblCommissionReceived") as Label;
                Label lblNewCommPayment = (Label)e.Row.FindControl("lblNewCommPayment") as Label;
                Label lblUnPaidAmount = (Label)e.Row.FindControl("lblUnPaidAmount") as Label;
                Label lblShare = (Label)e.Row.FindControl("lblShare") as Label;
                HiddenField hdnTermTypeId = (HiddenField)e.Row.FindControl("hdnTermTypeId") as HiddenField;
                if (hdnTermTypeId != null)
                {
                    if (hdnTermTypeId.Value == "2")
                    {
                        e.Row.BackColor = System.Drawing.Color.LightSkyBlue;
                    }
                    if (hdnTermTypeId.Value == "3")
                    {
                        e.Row.BackColor = System.Drawing.Color.LightPink;
                    }
                    if (hdnTermTypeId.Value == "4")
                    {
                        e.Row.BackColor = System.Drawing.Color.LightGreen;
                    }
                }
                if (lblCommissionDue.Text != null && lblCommissionDue.Text != "")
                {
                    commissiondue = commissiondue + Convert.ToDecimal(lblCommissionDue.Text.ToString());
                }
                if (lblBrokerPercent.Text != null && lblBrokerPercent.Text != "")
                {
                    percentdue = percentdue + Convert.ToDecimal(lblBrokerPercent.Text);
                }
                if (lblCommissionReceived.Text != null && lblCommissionReceived.Text != "")
                {
                    received = received + Convert.ToDecimal(lblCommissionReceived.Text);
                }
                if (lblNewCommPayment.Text != null && lblNewCommPayment.Text != "")
                {
                    newpayment = newpayment + Convert.ToDecimal(lblNewCommPayment.Text);
                }
                if (lblUnPaidAmount.Text != null && lblUnPaidAmount.Text != "")
                {
                    unpaidamount = unpaidamount + Convert.ToDecimal(lblUnPaidAmount.Text);
                }
                //if (lblShare.Text != null && lblShare.Text != "")
                //{
                //    ShareAmount = ShareAmount + Convert.ToDecimal(lblShare.Text.ToString());
                //}
                lblCommissionDue.Text = String.Format("{0:n2}", Convert.ToDecimal(lblCommissionDue.Text.ToString()));
                lblBrokerPercent.Text = String.Format("{0:n2}", Convert.ToDecimal(lblBrokerPercent.Text.ToString()));
                lblCommissionReceived.Text = String.Format("{0:n2}", Convert.ToDecimal(lblCommissionReceived.Text.ToString()));
                lblNewCommPayment.Text = String.Format("{0:n2}", Convert.ToDecimal(lblNewCommPayment.Text.ToString()));
                lblUnPaidAmount.Text = String.Format("{0:n2}", Convert.ToDecimal(lblUnPaidAmount.Text.ToString()));
                lblShare.Text = String.Format("{0:n2}", Convert.ToDecimal(lblShare.Text.ToString()));
                if (DataBinder.Eval(e.Row.DataItem, "ShareAmount") != DBNull.Value)
                    ShareAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "ShareAmount"));

            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblPayDetCommissionDue = (Label)e.Row.FindControl("lblPayDetCommissionDue") as Label;
                Label lblPayDetBrokerPercent = (Label)e.Row.FindControl("lblPayDetBrokerPercent") as Label;
                Label lblPayDetCommissionReceived = (Label)e.Row.FindControl("lblPayDetCommissionReceived") as Label;
                Label lblPayDetNewCommPayment = (Label)e.Row.FindControl("lblPayDetNewCommPayment") as Label;
                Label lblPayDetUnPaidAmount = (Label)e.Row.FindControl("lblPayDetUnPaidAmount") as Label;
                Label lblPayShare = (Label)e.Row.FindControl("lblPayShare") as Label;

                if (lblPayDetCommissionDue != null)
                {
                    lblPayDetCommissionDue.Text = "$" + String.Format("{0:n2}", commissiondue);
                }
                if (lblPayDetBrokerPercent != null)
                {
                    lblPayDetBrokerPercent.Text = String.Format("{0:n2}", percentdue);
                }
                if (lblPayDetCommissionReceived != null)
                {
                    lblPayDetCommissionReceived.Text = "$" + String.Format("{0:n2}", received);
                }
                if (lblPayDetNewCommPayment != null)
                {
                    lblPayDetNewCommPayment.Text = "$" + String.Format("{0:n2}", newpayment);
                }
                if (lblPayDetUnPaidAmount != null)
                {
                    lblPayDetUnPaidAmount.Text = "$" + String.Format("{0:n2}", unpaidamount);
                }
                if (lblPayShare != null)
                {
                    lblPayShare.Text = "$" + String.Format("{0:n2}", ShareAmount);
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "grdPaymentDetail_RowDataBound", ex.Message);
        }
    }
    protected void grdCommission_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "PayHistory")
            {
                //divPaymentHistoryDetail.Visible = true;
                ucPaymentHistoryDetail1.ChargeSlipId = Convert.ToInt32(e.CommandArgument);
                ucPaymentHistoryDetail1.Bindata();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "display", "display();", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "display", "display();", true);
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "grdCommission_RowCommand", ex.Message);
        }
    }
    protected void txtAmount_TextChanged(object sender, EventArgs e)
    {
        try
        {
            if (hdnTotalBalance != null)
            {
                if (!string.IsNullOrEmpty(hdnTotalBalance.Value))
                {
                    decimal balance = Convert.ToDecimal(hdnTotalBalance.Value.ToString()) - Convert.ToDecimal(txtAmount.Text.Trim());
                    txtBalance.Text = String.Format("{0:n2}", balance).ToString();
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "txtAmount_TextChanged", ex.Message);
        }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void GetChargeSlipDetails(string RowIndx)
    {
        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {
                int ChargeSlipID = Convert.ToInt32(HttpContext.Current.Session["ChargeSlipId"]);
                Utility.ClearSessionExceptUser();
                ChargeSlip objChargeSlip = new ChargeSlip();
                objChargeSlip.ChargeSlipID = ChargeSlipID;
                objChargeSlip.Load();

                DealTransactions objDealTransactions = new DealTransactions();
                objDealTransactions.ChargeSlipID = ChargeSlipID;
                objDealTransactions.LoadByChargeSlipId();


                objChargeSlip.DealTransactionsProperty = objDealTransactions;
                HttpContext.Current.Session[GlobleData.NewChargeSlipObj] = objChargeSlip;
                HttpContext.Current.Session[GlobleData.NewChargeSlipId] = ChargeSlipID;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "GetChargeSlipDetails", ex.Message);
        }
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] SearchGetComapnyList(string prefixText, int count)
    {
        List<string> lstSearchList = new List<string>();
        try
        {
            Company objCompany = new Company();
            objCompany.CompanyName = prefixText;
            DataSet ds = objCompany.GetLocationSeachList();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dt in ds.Tables[0].Rows)
                {
                    //dt["ValueType"].ToString()
                    lstSearchList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dt["Company"].ToString(), dt["CompanyId"].ToString()));
                    // lstSearchList.Add(dt[1].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "SearchGetComapnyList", ex.Message);
        }
        return lstSearchList.ToArray();
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string[] SearchGetDealNameList(string prefixText, int count, string contextKey)
    {
        string id = contextKey;
        List<string> lstSearchList = new List<string>();
        try
        {
            //string idd = HttpContext.Current.Session["CompId"].ToString();
            int Compid = Convert.ToInt32(contextKey);
            Company objCompany = new Company();
            objCompany.CompanyName = prefixText;
            DataSet ds = objCompany.GetDealNameSeachList(Compid, prefixText);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dt in ds.Tables[0].Rows)
                {
                    //dt["ValueType"].ToString()
                    lstSearchList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dt["DealName"].ToString(), dt["ChargeSlipID"].ToString()));
                    // lstSearchList.Add(dt[1].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "SearchGetDealNameList", ex.Message);
        }
        return lstSearchList.ToArray();
    }
    //protected void txtDealName_TextChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        if (!string.IsNullOrEmpty(txtDealName.Text))
    //        {
    //            lblDealName.Text = txtDealName.Text;
    //            BindGrid();
    //        }
    //        else
    //        {
    //            lblMessage.Text = string.Empty;
    //            grdCommission.DataSource = null;
    //            grdCommission.DataBind();
    //            txtBalance.Text = "";
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorLog.WriteLog("Payments.aspx", "BindDropDown", ex.Message);
    //    }
    //}
    //protected void ddlDealName_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //DataTable dtDealName = objCL.GetDealName();
    //        DataView dtView = dtDealName.DefaultView;
    //        string SelectedValue = (ddlReceivedFrom.SelectedValue.ToString() == "") ? "0" : ddlReceivedFrom.SelectedValue;
    //        dtView.RowFilter = "CompanyID=" + SelectedValue;
    //        Utility.FillDropDown(ref ddlDealName, dtView.ToTable(), "DealName", "ChargeSlipID");
    //    try
    //    {
    //        if (ddlDealName.SelectedItem != null)
    //        {
    //            lblDealName.Text = ddlDealName.SelectedItem.Text.ToString();
    //            BindGrid();
    //        }

    //        if (ddlDealName.SelectedIndex == -1)
    //        {
    //            lblMessage.Text = string.Empty;
    //            grdCommission.DataSource = null;
    //            grdCommission.DataBind();
    //            txtBalance.Text = "";
    //        }



    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorLog.WriteLog("Payments.aspx", "BindDropDown", ex.Message);
    //    }
    //}
    protected void txtReceivedFrom_TextChanged(object sender, EventArgs e)
    {
        try
        {
            lblMessage.Text = string.Empty;
            txtBalance.Text = string.Empty;
            lblDealName.Text = string.Empty;
            trDealName.Visible = false;
            txtPMTDate.Text = string.Empty;
            txtCheckno.Text = string.Empty;
            txtAmount.Text = string.Empty;
            hdnTotalBalance.Value = "0";
            ddlDealName.SelectedIndex = -1;
            ddlPMTMethod.SelectedIndex = -1;
            DataTable dt = new DataTable();
            DataColumn dtcol = new DataColumn("ChargeSlipId", typeof(Int64));
            dt.Columns.Add(dtcol);
            dtcol = new DataColumn("DealName", typeof(string));
            dt.Columns.Add(dtcol);
            DataTable dtDealName = null;
            if (hdnSelected != null && !string.IsNullOrEmpty(hdnSelected.Value))
            {
                if (hdnSelected.Value == "Selected")
                {
                    if (hdnCompId != null)
                    {
                        if (!string.IsNullOrEmpty(hdnCompId.Value))
                        {
                            Payments objPayment = new Payments();
                            DataSet ds = objPayment.GetListDealName(Convert.ToInt32(hdnCompId.Value.ToString()));

                            dtDealName = ds.Tables[0];
                            if (dtDealName.Rows.Count > 0)
                            {
                                Utility.FillDropDown(ref ddlDealName, dtDealName, "DealName", "ChargeSlipID");


                            }
                            else
                            {
                                Utility.FillDropDown(ref ddlDealName, dtDealName, "DealName", "ChargeSlipID");
                            }
                            hdnSelected.Value = "NotSelected";
                        }

                    }
                }
                else
                {
                    ddlDealName.DataSource = dt;
                    ddlDealName.DataTextField = "DealName";
                    ddlDealName.DataValueField = "ChargeSlipId";
                    ddlDealName.DataBind();
                    //Utility.FillDropDown(ref ddlDealName, dtDealName, "DealName", "ChargeSlipID");
                }
            }


        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "ddlReceivedFrom_SelectedIndexChanged", ex.Message);
        }

        //txtDealName.Text = string.Empty;
        grdCommission.DataSource = null;
        grdCommission.DataBind();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        btnApplyPayment.Visible = true;
        lblMessage.Text = string.Empty;
        try
        {
            if (hdnTotalBalance != null)
            {
                if (!string.IsNullOrEmpty(hdnTotalBalance.Value))
                {
                    decimal balance = Convert.ToDecimal(hdnTotalBalance.Value.ToString()) - Convert.ToDecimal(txtAmount.Text.ToString());
                    txtBalance.Text = String.Format("{0:n2}", balance).ToString();
                }
            }
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "DispalySummary", "DispalySummary();", true);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payments.aspx", "txtAmount_TextChanged", ex.Message);
        }
    }
    protected void btnAddNewPayment_Click(object sender, EventArgs e)
    {
        Response.Redirect("Payments.aspx", false);
    }
    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        Response.Redirect("Payments.aspx", false);
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string SavePaymentAmount(string Paymentd, string Amount)
    {
        string str = "";
        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {
                //if ((!String.IsNullOrEmpty(PaymentHistoryId)) && (!String.IsNullOrEmpty(CheckNo)))
                if ((!String.IsNullOrEmpty(Paymentd)))
                {
                    int PHID = Convert.ToInt32(Paymentd.Trim());
                    decimal PaymentAmount = Convert.ToDecimal(Amount);
                    Payments obj = new Payments();
                    if (HttpContext.Current.Session[GlobleData.User] != null)
                    {
                        DataSet ds = new DataSet();
                        ds = obj.UpdatePaymentAmount(PHID, PaymentAmount, Convert.ToInt32(HttpContext.Current.Session[GlobleData.User].ToString()));
                        if (ds != null)
                        {
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {

                                    str = ds.Tables[0].Rows[0]["OldAmount"].ToString();
                                    return str;
                                }
                            }
                            else
                            {

                                return str;
                            }
                        }
                        else
                        {

                            return str;
                        }
                    }
                }
            }
            return str;
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Customer.aspx", "SavePaymentAmount", ex.Message);
        }
        return str;
    }
      [System.Web.Services.WebMethod(EnableSession = true)]
    public static string PaymentPDFOpen(string Paymentd)
    {
        string str = "";
        try
        {

            //if ((!String.IsNullOrEmpty(PaymentHistoryId)) && (!String.IsNullOrEmpty(CheckNo)))
            if ((!String.IsNullOrEmpty(Paymentd)))
            {
                int PHID = Convert.ToInt32(Paymentd.Trim());
                Payments obj = new Payments();
                DataTable dt = new DataTable();
                dt = obj.PaymentPDFOpen(PHID);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        str = dt.Rows[0]["ChargeslipPaymentPath"].ToString();
                        return str;
                       
                    }
                    else
                    {

                        return str;
                    }
                }

            }

            return str;
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Payment.aspx", "PaymentPDFOpen", ex.Message);
        }
        return str;
    }
    protected void btnRefreshPayment_Click(object sender, EventArgs e)
    {
        if (hdnChargeslipId.Value != "0")
        {
            ucPaymentHistoryDetail1.ChargeSlipId = Convert.ToInt32(hdnChargeslipId.Value.ToString());
            ucPaymentHistoryDetail1.Bindata();

        }
    }
    protected void btnExporttoPDF_Click(object sender, EventArgs e)
    {
        try
        {
            if (hdnBalanceSelected.Value == "1")
            {
                lblCreditAmount.Text = "$ " + hdnUnappliedDet.Value;
                lblRemainingBalanceCredit.Text = "$ 0.00";
            }
            else
            {
                lblRemainingBalanceCredit.Text = "$ " + hdnUnappliedDet.Value;
                lblCreditAmount.Text = "$ 0.00";
            }
            Response.ClearContent();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            foreach (GridViewRow row in grdPaymentDetail.Rows)
            {
                row.Style.Add("font-size", "8px");
                TextBox txtCheckNo = (TextBox)row.FindControl("txtCoCheckNo");
                if (txtCheckNo != null)
                {
                    row.Cells[8].Text = txtCheckNo.Text;
                }
            }
            GridViewRow HeaderRow = grdPaymentDetail.HeaderRow;
            HeaderRow.Height = 20;
            HeaderRow.Style.Add("font-size", "9px");
            HeaderRow.Style.Add("background-color", "lightgray");
            GridViewRow FooterRow = grdPaymentDetail.FooterRow;
            FooterRow.Height = 20;
            FooterRow.Style.Add("font-size", "9px");
            FooterRow.Cells[2].Style.Add("text-align", "right");
            FooterRow.Cells[4].Style.Add("text-align", "right");
            FooterRow.Cells[5].Style.Add("text-align", "right");
            FooterRow.Cells[6].Style.Add("text-align", "right");
            FooterRow.Cells[7].Style.Add("text-align", "right");

            foreach (GridViewRow row in grdPaymentDistributionCoBrokerDetail.Rows)
            {
                row.Style.Add("font-size", "8px");
            }
            GridViewRow HeaderRowgrd = grdPaymentDistributionCoBrokerDetail.HeaderRow;
            HeaderRowgrd.Height = 20;
            HeaderRowgrd.Style.Add("font-size", "9px");
            HeaderRowgrd.Style.Add("background-color", "lightgray");

            tblDealDetail.Style.Add("font-size", "9px");

            //grdPaymentDetail.HeaderStyle.ForeColor = System.Drawing.Color.Green;
            //for (int col = 0; col < grdPaymentDetail.HeaderRow.Controls.Count; col++)
            //{
            //    TableCell tc = grdPaymentDetail.HeaderRow.Cells[col];
            //    tc.Style.Add("color", "#000");
            //    tc.Style.Add("backcolor", "#d8d8d8");
            //}

            Response.AddHeader("content-disposition", "attachment;filename=PaymentDetail_" + lblDealNames.Text + ".pdf");
            divHeader.RenderControl(hw);
            divBreakLine.RenderControl(hw);
            // hw.NewLine();
            divDealNames.Style.Add("padding-left", "10px");
            divDealNames.RenderControl(hw);
            tblDealDetail.RenderControl(hw);
            divBreakLine.RenderControl(hw);
            divPaymDis.RenderControl(hw);
            divBlankForpdf1.RenderControl(hw);
            grdPaymentDetail.RenderControl(hw);
            divBlankForpdf2.RenderControl(hw);
            grdPaymentDistributionCoBrokerDetail.RenderControl(hw);
            divBlankForpdf3.RenderControl(hw);
            tblRemainingDet.Style.Add("width", "70%");
            tblRemainingDet.RenderControl(hw);

            Document pdfDoc = new Document(new Rectangle(612f, 792f), 36f, 36f, 36f, 36f);
            pdfDoc.HtmlStyleClass = "SetWidth";
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            StringReader sr = new StringReader(sw.ToString());
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payment.ascx", "btnExporttoPDF_Click", ex.Message);
        }
    }
    protected void btnExporttoExcel_Click(object sender, EventArgs e)
    {
        try
        {
            if (hdnBalanceSelected.Value == "1")
            {
                lblCreditAmount.Text = "$ " + hdnUnappliedDet.Value;
                lblRemainingBalanceCredit.Text = "$ 0.00";
            }
            else
            {
                lblRemainingBalanceCredit.Text = "$ " + hdnUnappliedDet.Value;
                lblCreditAmount.Text = "$ 0.00";
            }
            Response.ClearContent();
            Response.Buffer = true;
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            foreach (GridViewRow row in grdPaymentDetail.Rows)
            {
                row.Style.Add("font-size", "14px");
                TextBox txtCheckNo = (TextBox)row.FindControl("txtCoCheckNo");
                if (txtCheckNo != null)
                {
                    row.Cells[8].Text = txtCheckNo.Text;
                }

            }
            GridViewRow HeaderRow = grdPaymentDetail.HeaderRow;
            HeaderRow.Height = 20;
            HeaderRow.Style.Add("font-size", "14px");
            HeaderRow.Style.Add("background-color", "lightgray");

            GridViewRow FooterRow = grdPaymentDetail.FooterRow;
            FooterRow.Height = 20;
            FooterRow.Style.Add("font-size", "14px");
            FooterRow.Cells[2].Style.Add("text-align", "right");
            FooterRow.Cells[4].Style.Add("text-align", "right");
            FooterRow.Cells[5].Style.Add("text-align", "right");
            FooterRow.Cells[6].Style.Add("text-align", "right");
            FooterRow.Cells[7].Style.Add("text-align", "right");

            foreach (GridViewRow row in grdPaymentDistributionCoBrokerDetail.Rows)
            {
                row.Style.Add("font-size", "14px");

            }
            GridViewRow HeaderRowgrd = grdPaymentDistributionCoBrokerDetail.HeaderRow;
            HeaderRowgrd.Height = 20;
            HeaderRowgrd.Style.Add("font-size", "14px");
            HeaderRowgrd.Style.Add("background-color", "lightgray");

            tblDealDetail.Style.Add("font-size", "14px");
            Response.AddHeader("content-disposition", "attachment;filename=PaymentDetail_" + lblDealNames.Text + ".xls");
            divHeader.RenderControl(hw);
            divBreakLine.RenderControl(hw);
            divDealNames.Style.Add("padding-left", "10px");
            divDealNames.RenderControl(hw);
            tblDealDetail.RenderControl(hw);
            divBreakLine.RenderControl(hw);
            divPaymDis.RenderControl(hw);
            divBlankForpdf1.RenderControl(hw);
            //divPaymentDis.RenderControl(hw);
            grdPaymentDetail.RenderControl(hw);
            divBlankForpdf2.RenderControl(hw);
            grdPaymentDistributionCoBrokerDetail.RenderControl(hw);
            divBlankForpdf3.RenderControl(hw);
            tblRemainingDet.Style.Add("width", "100%");
            tblRemainingDet.RenderControl(hw);
            Response.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Payment.ascx", "btnExporttoExcel_Click", ex.Message);
        }
    }
  
}