﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using SegallChargeSlipBll;

public partial class Admin_DealType : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Bindgrid();
        }
    }
    private void Bindgrid()
    {
        try
        {
            DealTypes objDealTypes = new DealTypes();
            DataSet ds = new DataSet();
            ds = objDealTypes.GetList();
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grdDealType.DataSource = ds.Tables[0];
                    grdDealType.DataBind();
                }
                else
                {
                    grdDealType.DataSource = null;
                    grdDealType.DataBind();
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealType.aspx", "Bindgrid", ex.Message);
        }
    }
    protected void grdDealType_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Del")
            {
                DealTypes objDealTypes = new DealTypes();
                objDealTypes.DealTypeID = Convert.ToInt32(e.CommandArgument);
                bool load = objDealTypes.Delete();
                Bindgrid();
                //DataTable dt= objDealTypes.LoadDealTransaction();
                //if (dt != null)
                //{
                //    if (dt.Rows.Count > 0)
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Update Deal Type", "alert('Deal Type cannot be delete because it has referenced.');", true);
                //    }
                //    else
                //    {
                //bool load = objDealTypes.Load();
                //        if (load)
                //        {
                //            objDealTypes.IsActive = false;
                //            objDealTypes.Save();
                //            Bindgrid();
                //        }
                //    }
                //}
            }
            if (e.CommandName == "Ed")
            {
                DealTypes objDealTypes = new DealTypes();
                objDealTypes.DealTypeID = Convert.ToInt32(e.CommandArgument);
                bool load = objDealTypes.Load();
                if (load)
                {
                    txtDealType.Text = objDealTypes.DealType;
                    if (objDealTypes.IsActive == true)
                    {
                        chkIsActive.Checked = true;
                    }
                    if (objDealTypes.IsActive == false)
                    {
                        chkIsActive.Checked = false;
                    }
                    ViewState["DealTypeID"] = objDealTypes.DealTypeID;
                    btnUpdate.Text = "UPDATE";
                    mpeUpdateDealType.Show();

                }

            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealType.aspx", "grdDealType_RowCommand", ex.Message);
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (ViewState["DealTypeID"] != null)
            {
                if (!string.IsNullOrEmpty(ViewState["DealTypeID"].ToString()))
                {
                    DealTypes objDealTypes = new DealTypes();
                    objDealTypes.DealTypeID = Convert.ToInt32(ViewState["DealTypeID"].ToString());
                    objDealTypes.Load();
                    objDealTypes.DealType = txtDealType.Text.Trim();
                    if (chkIsActive.Checked)
                    {
                        objDealTypes.IsActive = true;
                    }
                    else
                    {
                        objDealTypes.IsActive = false;
                    }
                    objDealTypes.Save();
                    Bindgrid();
                }
            }
            else
            {
                DealTypes objDealTypes = new DealTypes();
                objDealTypes.DealType = txtDealType.Text;
                if (chkIsActive.Checked)
                {
                    objDealTypes.IsActive = true;
                }
                else
                {
                    objDealTypes.IsActive = false;
                }
                objDealTypes.Save();
                Bindgrid();
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealType.aspx", "btnUpdate_Click", ex.Message);
        }
    }
    protected void lnkAddDealType_Click(object sender, EventArgs e)
    {
        ViewState["DealTypeID"] = null;
        txtDealType.Text = string.Empty;
        btnUpdate.Text = "ADD";
    }
    protected void btnAddDealType_Click(object sender, EventArgs e)
    {
        ViewState["DealTypeID"] = null;
        txtDealType.Text = string.Empty;
        chkIsActive.Checked = true;
        btnUpdate.Text = "ADD";
        mpeUpdateDealType.Show();
    }
    protected void grdDealType_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            //DealTypes objDealTypes = new DealTypes();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnDealTypeId = (HiddenField)e.Row.FindControl("hdnDealTypeId") as HiddenField;
                //objDealTypes.DealTypeID = Convert.ToInt32(hdnDealTypeId.Value);
                CheckBox chkActive = (CheckBox)e.Row.FindControl("chkActive");
                HiddenField hdnActive = (HiddenField)e.Row.FindControl("hdnActive") as HiddenField;
                HiddenField hdnCanDel = (HiddenField)e.Row.FindControl("hdnCanDel") as HiddenField;
                int candel = Convert.ToInt32(hdnCanDel.Value);
                if (candel == 0)
                {
                    ImageButton imgDelete = (ImageButton)e.Row.FindControl("delRecord") as ImageButton;
                    imgDelete.Enabled = false;
                    imgDelete.Width = 16;
                    imgDelete.Height = 16;
                    imgDelete.ImageUrl = "~/Images/deletegray.png";
                }
                //DataTable dt = objDealTypes.LoadDealTransaction();
                //if (dt != null)
                //{
                //    if (dt.Rows.Count > 0)
                //    {
                //        ImageButton imgDelete = (ImageButton)e.Row.FindControl("delRecord") as ImageButton;
                //        imgDelete.Enabled = false;
                //        // imgDelete.Visible = false;
                //        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Update Deal Type", "alert('Deal Type cannot be delete because it has referenced.');", true);
                //    }
                //}
                if (hdnActive != null)
                {
                    if (hdnActive.Value == "True")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealType.aspx", "grdDealType_RowDataBound", ex.Message);
        }
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SetActive(string DealID)
    {
        try
        {
            DealTypes objDealTypes = new DealTypes();
            objDealTypes.DealTypeID = Convert.ToInt32(DealID);
            bool load = objDealTypes.Load();
            if (load)
            {
                if (objDealTypes.IsActive)
                {
                    objDealTypes.IsActive = false;
                }
                else
                {
                    objDealTypes.IsActive = true;
                }
                objDealTypes.Save();

            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealType.aspx", "SetActive", ex.Message);
        }
    }
}