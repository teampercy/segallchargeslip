﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using SegallChargeSlipBll;

public partial class Admin_DealUse : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Bindgrid();
        }
    }
    private void Bindgrid()
    {
        try
        {
            DealUse objDealUse = new DealUse();
            DataSet ds = new DataSet();
            ds = objDealUse.GetList();
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grdUse.DataSource = ds.Tables[0];
                    grdUse.DataBind();
                }
                else
                {
                    grdUse.DataSource = null;
                    grdUse.DataBind();
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealUse.aspx", "Bindgrid", ex.Message);
        }
    }


    protected void btnUse_Click(object sender, EventArgs e)
    {
        ViewState["DealUseID"] = null;
        txtUse.Text = string.Empty;
        chkIsActive.Checked = true;
        btnUpdate.Text = "ADD";
        mpeUpdateUse.Show();
    }
    protected void grdUse_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Del")
            {
                DealUse objUse = new DealUse();
                objUse.DealUseId = Convert.ToInt32(e.CommandArgument);

                bool load = objUse.Delete();
                Bindgrid();
                //if (load)
                //{
                //    objUse.IsActive = false;
                //    objUse.Save();
                //    Bindgrid();
                //}


            }
            if (e.CommandName == "Ed")
            {
                DealUse objUse = new DealUse();
                objUse.DealUseId = Convert.ToInt32(e.CommandArgument);
                bool load = objUse.Load();
                if (load)
                {
                    txtUse.Text = objUse.DealUseType;
                    if (objUse.IsActive == true)
                    {
                        chkIsActive.Checked = true;
                    }
                    if (objUse.IsActive == false)
                    {
                        chkIsActive.Checked = false;
                    }
                    ViewState["DealUseID"] = objUse.DealUseId;
                    btnUpdate.Text = "UPDATE";
                    mpeUpdateUse.Show();

                }

            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealUse.aspx", "grdUse_RowCommand", ex.Message);
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (ViewState["DealUseID"] != null)
            {
                if (!string.IsNullOrEmpty(ViewState["DealUseID"].ToString()))
                {
                    DealUse objUse = new DealUse();
                    objUse.DealUseId = Convert.ToInt32(ViewState["DealUseID"].ToString());
                    objUse.Load();
                    objUse.DealUseType = txtUse.Text.Trim();
                    if (chkIsActive.Checked)
                    {
                        objUse.IsActive = true;
                    }
                    else
                    {
                        objUse.IsActive = false;
                    }
                    objUse.Save();
                    Bindgrid();
                }
            }
            else
            {
                DealUse objUse = new DealUse();
                objUse.DealUseType = txtUse.Text;
                if (chkIsActive.Checked)
                {
                    objUse.IsActive = true;
                }
                else
                {
                    objUse.IsActive = false;
                }
                objUse.Save();
                Bindgrid();
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealUse.aspx", "btnUpdate_Click", ex.Message);
        }
    }
    protected void grdUse_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            //DealUse objUse = new DealUse();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnUseId = (HiddenField)e.Row.FindControl("hdnUseId") as HiddenField;
                //objUse.DealUseId = Convert.ToInt32(hdnUseId.Value);
                CheckBox chkActive = (CheckBox)e.Row.FindControl("chkActive");
                HiddenField hdnActive = (HiddenField)e.Row.FindControl("hdnActive") as HiddenField;
                HiddenField hdnCanDel = (HiddenField)e.Row.FindControl("hdnCanDel") as HiddenField;
                int candel = Convert.ToInt32(hdnCanDel.Value);
                if (candel == 0)
                {
                    ImageButton imgDelete = (ImageButton)e.Row.FindControl("delRecord") as ImageButton;
                    imgDelete.Enabled = false;
                    imgDelete.Width = 16;
                    imgDelete.Height = 16;
                    imgDelete.ImageUrl = "~/Images/deletegray.png";
                }
                //DataTable dt = objUse.LoadDealTransaction();
                //if (dt != null)
                //{
                //    if (dt.Rows.Count > 0)
                //    {
                //        ImageButton imgDelete = (ImageButton)e.Row.FindControl("delRecord") as ImageButton;
                //        imgDelete.Enabled = false;
                //        // imgDelete.Visible = false;
                //        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Update Deal Type", "alert('Deal Type cannot be delete because it has referenced.');", true);
                //    }
                //}
                if (hdnActive != null)
                {
                    if (hdnActive.Value == "True")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealUse.aspx", "grdUse_RowDataBound", ex.Message);
        }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SetActive(string UseId)
    {
        try
        {
            DealUse objUse = new DealUse();
            objUse.DealUseId = Convert.ToInt32(UseId);
            bool load = objUse.Load();
            if (load)
            {
                if (objUse.IsActive)
                {
                    objUse.IsActive = false;
                }
                else
                {
                    objUse.IsActive = true;
                }
                objUse.Save();

            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealUse.aspx", "SetActive", ex.Message);
        }
    }
}