﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SetTest.aspx.cs" Inherits="Admin_SetTest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/UserControl/ucAddDealTypes.ascx" TagPrefix="uc1" TagName="ucAddDealTypes" %>
<%@ Register Src="~/UserControl/ucAddSubDealTypes.ascx" TagPrefix="uc1" TagName="ucAddSubDealTypes" %>




<link href="../Styles/Master.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="../Styles/SiteTEST.css" rel="stylesheet" />

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles/Master.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <style type="text/css">
        .modaltextboxlarge {
            width: 250px;
            height: 16px !important;
            Font-Size: Small;
        }

        .modaltextboxsmall {
            width: 160px;
            height: 16px !important;
            Font-Size: Small;
        }

        .modalddlSmall {
            width: 160px;
            height: 20px !important;
            Font-Size: Small;
        }

        .SqureGrayButtonSmall {
            Width: 80px;
            Height: 20px;
            font-family: Verdana;
            font-weight: 600;
            border-radius: 0px;
            border: 1px solid #000000;
            background-color: grey;
            color: black;
            cursor: pointer;
        }
    </style>
</head>
<body>

    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="scmgr" />


        <table style="width: 100%">
            <tr>
                <td style="width: 50%" align="left">
                    <h2>ADMINISTRATION </h2>
                </td>
                <td align="right">
                    <h2>SETTINGS </h2>
                </td>
            </tr>
        </table>
        <hr />

        <asp:UpdatePanel runat="server" ID="upManageDealTypes">
            <ContentTemplate>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 50%" align="left">
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <label class="contentTitle">DEAL TYPE DROPDOWN OPTIONS </label>
                                        </label></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlDealTypesMST">
                                        </asp:DropDownList>
                                        <asp:ImageButton ID="btnDealTypesManage" runat="server" OnClick="btnDealTypesManage_Click" Height="20px" Width="20px" ImageUrl="~/Images/manage-icon.png" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="contentTitle">DEAL SUBTYPE DROPDOWN OPTIONS </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                        <table style="width: 100%">
                                            <tr>
                                                <td align="left" style="width: 150px;">
                                                    <asp:DropDownList runat="server" ID="ddlDealTypesParentMST" AutoPostBack="true" OnSelectedIndexChanged="ddlDealTypesParentMST_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="right">
                                                    <label class="contentTitle">LINKED TO: </label>
                                                </td>
                                                <td align="left">
                                                    <asp:DropDownList runat="server" ID="ddlDealSubTypesParentMST">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left">
                                                    <asp:ImageButton ID="btnSubDealTypesManage" runat="server" OnClick="btnSubDealTypesManage_Click" Height="20px" Width="20px" ImageUrl="~/Images/manage-icon.png" />
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 50%" align="left"></td>
                    </tr>
                </table>

                <div>

                    <ajax:ModalPopupExtender ID="mpDealTypesManager" TargetControlID="lbDummybtn" CancelControlID="btnCancel"
                        PopupControlID="divDealTypesManage" runat="server" BackgroundCssClass="modalBackground">
                    </ajax:ModalPopupExtender>

                    <asp:Label ID="lbDummybtn" runat="server"></asp:Label>
                    <div id="divDealTypesManage" class="PopupDivBodyStyle" style="height: 320px; width: 550px; display: none;">
                        <table style="width: 100%" class="spacing">
                            <tr style="background-color: gray; border-bottom: solid;">
                                <td style="text-align: left;">
                                    <asp:UpdatePanel ID="upTitile" runat="server">
                                        <ContentTemplate>
                                            <h3>&nbsp; &nbsp;      
                                        <asp:Label ID="lbDealTypesTitle" runat="server"></asp:Label></h3>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td align="right">
                                    <asp:ImageButton ImageUrl="~/Images/close.png" ID="btnCancel" runat="server" OnClientClick="javascript:window.close();return false;" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <uc1:ucAddDealTypes runat="server" ID="ucAddDealTypes" />
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>

                <div>

                    <ajax:ModalPopupExtender ID="mpSubDealTypesManager" TargetControlID="lbDummySubDealTypebtn" CancelControlID="btnCancelSubDealType"
                        PopupControlID="divSubDealTypesManage" runat="server" BackgroundCssClass="modalBackground">
                    </ajax:ModalPopupExtender>

                    <asp:Label ID="lbDummySubDealTypebtn" runat="server"></asp:Label>
                    <div id="divSubDealTypesManage" class="PopupDivBodyStyle" style="height: 320px; width: 550px; display: none;">
                        <table style="width: 100%" class="spacing">
                            <tr style="background-color: gray; border-bottom: solid;">
                                <td style="text-align: left;">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <h3>&nbsp; &nbsp;      
                                        <asp:Label ID="lbDealSubTypesTitle" runat="server"></asp:Label></h3>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td align="right">
                                    <asp:ImageButton ImageUrl="~/Images/close.png" ID="btnCancelSubDealType" runat="server" OnClientClick="javascript:window.close();return false;" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <uc1:ucAddSubDealTypes runat="server" ID="ucAddSubDealTypes" />
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>




    </form>
</body>
</html>
