﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true" CodeFile="Users.aspx.cs" Inherits="Pages_Users" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="contHeadCont" ContentPlaceHolderID="cntHeadTag" runat="server">
    <style type="text/css">
        .space {
            padding-left: 10px;
        }

        .tdgray {
            border-color: gray !important;
            border-top: none !important;
            border-left: none !important;
        }

        .td-DeleteRecord {
            width: 4px;
            border-right: none !important;
        }
    </style>
    <style type="text/css">
        .removeunderline {
            text-decoration: none;
        }

        .linkbut {
            color: black;
            font-weight: bold;
            text-decoration: none;
        }

        .lnkMsg {
            color: black;
            text-decoration: none;
        }
    </style>
    <style type="text/css">
        .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=80);
            opacity: 0.8;
        }
    </style>
    <script type="text/javascript">
        function ShowIcon(sender, e) {
            sender._element.className = "loading";
        }

        function hideIcon(sender, e) {
            sender._element.className = "textwidth";
        }
        function OnCenterSelected(source, eventArgs) {
            //alert(eventArgs.get_value());
            document.getElementById('<%=hdnBrokerId.ClientID %>').value = eventArgs.get_value();
            //hdnLocId JAY//   alert(eventArgs.get_value());
            return false;
        }
        function SetActive(UserID) {
            //alert(locationid );
            $.ajax({
                type: "POST",
                url: "Users.aspx/SetActive",
                data: '{ID: "' + UserID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                failure: function (response) {
                    alert(response);
                }
            });
            return false;
        }
    </script>
    <script type="text/javascript">

        function ClearControl1() {
            document.getElementById('<%=txtFirstName.ClientID %>').value = "";
            document.getElementById('<%=txtLastName.ClientID %>').value = "";
            document.getElementById('<%=txtCompany.ClientID %>').value = "";
            document.getElementById('<%=txtEmail.ClientID %>').value = "";
            document.getElementById('<%=txtPwd.ClientID %>').value = "";
            document.getElementById('<%=txtConfmPass.ClientID %>').value = "";
            document.getElementById('<%=txt1_1.ClientID %>').value = "";
            document.getElementById('<%=txt2_1.ClientID %>').value = "";
            document.getElementById('<%=txt3_1.ClientID %>').value = "";
            document.getElementById('<%=txt4_1.ClientID %>').value = "";
            document.getElementById('<%=txtFrom.ClientID %>').value = "";
            document.getElementById('<%=txtTo.ClientID %>').value = "";
            var randomnumber = parseInt(document.getElementById('<%= hdnCount.ClientID %>').value);


            var output = document.getElementById("<%= tblUsers.ClientID %>");

            var rowCount = output.rows.length;

            for (var i = 1; i <= rowCount; i++) {

                // var j = i++;

                var txt1 = document.getElementById("ContentMain_txt1_" + i);
                var txt2 = document.getElementById("ContentMain_txt2_" + i);
                var txt3 = document.getElementById("ContentMain_txt3_" + i);
                var txt4 = document.getElementById("ContentMain_txt4_" + i);
                txt1.value = "";
                txt2.value = "";
                txt3.value = "";
                txt4.value = "";
            }
            return false;
        }
        function ClearControl() {
            document.getElementById('<%=txtFirstName.ClientID %>').value = "";
            document.getElementById('<%=txtLastName.ClientID %>').value = "";
            document.getElementById('<%=txtCompany.ClientID %>').value = "";
            document.getElementById('<%=txtEmail.ClientID %>').value = "";
            document.getElementById('<%=txtPwd.ClientID %>').value = "";
            document.getElementById('<%=txtConfmPass.ClientID %>').value = "";
            document.getElementById('<%=txt1_1.ClientID %>').value = "";
            document.getElementById('<%=txt2_1.ClientID %>').value = "";
            document.getElementById('<%=txt3_1.ClientID %>').value = "";
            document.getElementById('<%=txt4_1.ClientID %>').value = "";
            document.getElementById('<%=txtFrom.ClientID %>').value = "";
            document.getElementById('<%=txtTo.ClientID %>').value = "";


        }
    </script>
    <script type="text/javascript">

        function Validate() {
            var FirstName = document.getElementById('<%=txtFirstName.ClientID %>');
            var LastName = document.getElementById('<%=txtLastName.ClientID %>');
            var Company = document.getElementById('<%=txtCompany.ClientID %>');
            var Email = document.getElementById('<%=txtEmail.ClientID %>');
            var Password = document.getElementById('<%=txtPwd.ClientID %>');
            var Confirmpass = document.getElementById('<%=txtConfmPass.ClientID %>');
             
            if ($.trim(FirstName.value) == "") {
                alert('Please enter First Name');
                FirstName.focus();
                return false;
            }
            if ($.trim(LastName.value) == "") {
                alert('Please enter Last Name');
                LastName.focus();
                return false;
            }
            if ($.trim(Company.value) == "") {
                alert('Please enter Company Name');
                Company.focus();
                return false;
            }
            if ($.trim(Email.value) == "") {
                alert('Please enter EmailID');
                Email.focus();
                return false;
            }
            if (!IsValidEmailId(Email)) {
                alert('Please enter valid Email.');
                // lblErrorMsg.innerHTML = "Please enter valid Email.";
                return false;
            }
            if ($.trim(Password.value) == "") {
                alert('Please enter Password');
                Password.focus();
                return false;
            }
            if ($.trim(Confirmpass.value) == "") {
                alert('Please enter Confirm password');
                Password.focus();
                return false;
            }
            if (Password.value != Confirmpass.value) {
                alert('Password does not match');
                return false;
            }

            if (ValidateDynamicControls()) {

                $find('testIt').hide();
                $find('testIt_1').hide();
                return true;
            }
            else {
                return false;
            }
        }
        function display() {
            alert('hi');
        }
        function ValidateDynamicControls() {
            debugger;
            var FirstName = document.getElementById('<%=txtFirstName.ClientID %>');
            var LastName = document.getElementById('<%=txtLastName.ClientID %>');
            var Company = document.getElementById('<%=txtCompany.ClientID %>');
            var Email = document.getElementById('<%=txtEmail.ClientID %>');
            var Password = document.getElementById('<%=txtPwd.ClientID %>');
            var Confirmpass = document.getElementById('<%=txtConfmPass.ClientID %>');
            var fromdate = document.getElementById('<%=txtFrom.ClientID %>');
            var todate = document.getElementById('<%=txtTo.ClientID %>');
            var lblErrorMsg = document.getElementById('<%=lblErrorMsg.ClientID %>');

            if ($.trim(FirstName.value) == "") {
                lblErrorMsg.innerHTML = "Please enter First Name";
                FirstName.focus();
                return false;
            }
            if ($.trim(LastName.value) == "") {
                lblErrorMsg.innerHTML = "Please enter Last Name";
                LastName.focus();
                return false;
            }
            if ($.trim(Company.value) == "") {
                lblErrorMsg.innerHTML = "Please enter Company Name";
                Company.focus();
                return false;
            }
            if ($.trim(Email.value) == "") {
                lblErrorMsg.innerHTML = "Please enter EmailID";
                Email.focus();
                return false;
            }
            if (!IsValidEmailId(Email)) {
                lblErrorMsg.innerHTML = "Please enter valid Email";
                // lblErrorMsg.innerHTML = "Please enter valid Email.";
                return false;
            }
            if ($.trim(Password.value) == "") {
                lblErrorMsg.innerHTML = "Please enter Password";
                Password.focus();
                return false;
            }
            if ($.trim(Confirmpass.value) == "") {
                lblErrorMsg.innerHTML = "Please enter Confirm password";
                Confirmpass.focus();
                return false;
            }
            if (Password.value != Confirmpass.value) {
                lblErrorMsg.innerHTML = "Password does not match";
                return false;
            }

            if ($.trim(fromdate.value) == "") {
                lblErrorMsg.innerHTML = "Please enter From Date";
                fromdate.focus();
                return false;
            }
            if ($.trim(todate.value) == "") {
                lblErrorMsg.innerHTML = "Please enter To Date";
                todate.focus();
                return false;
            }
            if (!isDate(fromdate)) {
                return false;
            }
            if (!isDate(todate)) {
                return false;
            }
            if (Date.parse(fromdate.value) > Date.parse(todate.value)) {
                lblErrorMsg.innerHTML = "FromDate cannot be greater than To Date";
                fromdate.focus();
                return false;
            }

            var randomnumber = parseInt(document.getElementById('<%= hdnCount.ClientID %>').value);
            var output = document.getElementById("<%= tblUsers.ClientID %>");
            var rowCount = output.rows.length;
            MaxRngVal = 0;
            MinRngVal = 0;
            for (var i = 1; i <= rowCount; i++) {

                // var j = i++;

                var txt1 = document.getElementById("ContentMain_txt1_" + i);
                var txt2 = document.getElementById("ContentMain_txt2_" + i);
                var txt3 = document.getElementById("ContentMain_txt3_" + i);
                var txt4 = document.getElementById("ContentMain_txt4_" + i);

                if ($.trim(txt1.value) == "") {
                    lblErrorMsg.innerHTML = "Please enter Earnings for row " + i;
                    txt1.focus();
                    return false;
                }
                if ($.trim(txt2.value) == "") {
                    lblErrorMsg.innerHTML = "Please enter Earnings for row " + i;
                    txt2.focus();
                    return false;
                }
                if ($.trim(txt3.value) == "") {
                    lblErrorMsg.innerHTML = "Please enter Broker Commission split for row " + i;
                    txt3.focus();
                    return false;
                }
                if ($.trim(txt4.value) == "") {
                    lblErrorMsg.innerHTML = "Please enter Company Commission split for row " + i;
                    txt4.focus();
                    return false;
                }
                debugger;
                //JAY


                var brokercomission = parseFloat(txt3.value) + parseFloat(txt4.value);
                if (parseFloat(brokercomission) > parseFloat(100)) {
                    lblErrorMsg.innerHTML = "Summation of Broker and Company cannot be greater than 100 for row " + i ;
                    return false;
                }
                else if (parseFloat(brokercomission) < parseFloat(100)) {
                    lblErrorMsg.innerHTML = "Summation of Broker and Company cannot be less than 100 for row " + i ;
                    return false;
                }

                if (!checkRange(parseFloat(txt1.value), parseFloat(txt2.value), i)) {
                    //alert(' Repeated values in Earning range ' + i + ' row');
                    return false;
                }

                //var total = txt3.value + txt4.value
                //alert(total);
                // j = j + 1;
            }

            //$find('testIt').hide();
            //$find('testIt_1').hide();

            //if (CheckUsersEmailExist(Email.value) == true) {

            //}
            //else {
            //    return false;
            //}


            var emailid = Email.value;
            $.ajax({
                async: true,
                type: "POST",
                url: "Users.aspx/CheckUserEmailAlreadyExist",
                data: '{Email: "' + emailid + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    if (response.d == "Exist") {
                        debugger;
                        alert('EmailID already Exist.');
                        return false;
                    }
                    else {
                        debugger;
                        document.getElementById('<%=btnFillSave.ClientID%>').click();
                    }
                },
                failure: function (response) {
                    alert(response.d);
                    return false;
                }

            });
            return false;

        }

        var MaxRngVal = 0;
        var MinRngVal = 0;

        function checkRange(CurRngMin, CurRngMax, i) {
            debugger;
            //Can check this in parent button
            //if (CurRngMax < CurRngMax) {
            //    alert("Invalid range");
            //    return false;
            //}

            //implies first record e.g. 10-20
            var diffrange = 0;

            diffrange = (parseFloat(CurRngMin) - parseFloat(MaxRngVal)).toFixed(2);
            

            if (CurRngMin > CurRngMax) {
                alert(' Start Earning range can not be greater then end earning range for row ' + i  );
                return false;
            }

            
            if (MinRngVal == 0 && MaxRngVal == 0) {
                MinRngVal = CurRngMin;
                MaxRngVal = CurRngMax;
                return true;
            }
            else if ((diffrange < 0.01) || (diffrange > 0.01 && diffrange > 1)) {
                alert("Invalid minimum range value.The difference between the previous range should be between 0.01 and 1  for row " + i );
                    //alert("Invalid minim range It should be  " + (MaxRngVal + 0.1).toString() + ' row ' + ' ' + i);
                    return false;
                }
            //else if (!(CurRngMin >= (MaxRngVal + 0.01)) && !(CurRngMin<=(MaxRngVal + 1))) {
            //    alert("Invalid Min range It should be  " + (MaxRngVal + 0.1).toString() + ' row ' + ' ' + i);
            //    return true;
            //}
            //else if (CurRngMin != (MaxRngVal + 1)) {
            //    alert("Invalid Min range It should be  " + (MaxRngVal + 0.1).toString() + ' ' + i + ' row');
            //    return false;
            //}
            else {
                //1- 9 (CurRngMin < MinRngVal && CurRngMax < MinRngVal)
                //21-31 is valid amount
                if (CurRngMin > MaxRngVal && CurRngMax > MaxRngVal) {
                    //Cur range is lower then min range new rnage 1- 20
                    //if(CurRngMin < MinRngVal)
                    //{
                    //    MinRngVal  =  CurRngMin;
                    //}
                    //Cur range max is greater then the new range  10 - 31
                    if (MaxRngVal < CurRngMax) {
                        MaxRngVal = CurRngMax;
                    }
                    return true;
                }
                    //if range is exect below the current range i.e 1 - 9 then it becames--> 1 - 20
                else if (CurRngMin < MinRngVal && CurRngMax + 1 == MinRngVal) {
                    MinRngVal = CurRngMin;
                    return true;
                }
                else {
                    return false;
                }
            }
           
        }
        function AllowDecimals(CtrId) {
            var regExp = /^\d+(\.\d{0,9})?$/;
            var Input = document.getElementById(CtrId).value;
            if (!regExp.test(Input)) {
                document.getElementById(CtrId).value = "";
            }
            else {
                return true;
            }

            return false;
        }
        

        function addDuplicateEmail() {
            debugger;

            var randomnumber = parseInt(document.getElementById('<%= hdnCount.ClientID %>').value);

            var output = document.getElementById('<%= tblUsers.ClientID %>');

            var rowCount = output.rows.length;

            var cellCount = 0;

            var row = output.insertRow(rowCount);
            var cell1 = row.insertCell(cellCount);


            var element1 = document.createElement("input");
            element1.type = "hidden";
            rowCount = rowCount + 1;
            element1.name = "hdn1_" + rowCount;
            element1.id = "hdn1_" + rowCount;
            element1.value = 0;
            cell1.appendChild(element1);

            var element2 = document.createElement("input");
            element2.type = "hidden";
            element2.name = "hdnCheck_" + rowCount;
            element2.id = "hdnCheck_" + rowCount;
            element2.value = rowCount;
            cell1.appendChild(element2);

            //var element1 = document.createElement("input");
            //element1.type = "hidden";           
            //element1.name = "hdn_" + rowCount;
            //element1.id = "hdn_" + rowCount;
            //element1.value = rowCount;
            //cell1.appendChild(element1);
            cellCount++;

            cell1 = row.insertCell(cellCount);
            element1 = document.createElement("input");
            element1.type = "text";
            element1.name = 'ContentMain_txt1_' + rowCount;
            element1.id = 'ContentMain_txt1_' + rowCount;
            element1.style.width = "90%";
            element1.setAttribute('onkeyup', 'javascript:return AllowDecimals(\'' + element1.id + '\')');

            cell1.appendChild(element1);
            cellCount++;

            cell1 = row.insertCell(cellCount);
            element1 = document.createElement("hr");
            element1.style.width = "10px";
            cell1.appendChild(element1);
            cellCount++;

            cell1 = row.insertCell(cellCount);
            element1 = document.createElement("input");
            element1.type = "text";
            element1.name = 'ContentMain_txt2_' + rowCount;
            element1.id = 'ContentMain_txt2_' + rowCount;
            element1.style.width = "100%";
            element1.setAttribute('onkeyup', 'javascript:return AllowDecimals(\'' + element1.id + '\')');
            cell1.appendChild(element1);
            cellCount++;

            cell1 = row.insertCell(cellCount);
            cellCount++;

            cell1 = row.insertCell(cellCount);
            element1 = document.createElement("input");
            element1.type = "text";
            element1.name = 'ContentMain_txt3_' + rowCount;
            element1.id = 'ContentMain_txt3_' + rowCount;
            element1.style.width = "90%";
            element1.setAttribute('onkeyup', 'javascript:return AllowDecimals(\'' + element1.id + '\')');
            cell1.appendChild(element1);
            cellCount++;

            cell1 = row.insertCell(cellCount);
            element1 = document.createElement("input");
            element1.type = "text";
            element1.name = 'ContentMain_txt4_' + rowCount;
            element1.id = 'ContentMain_txt4_' + rowCount;
            element1.style.width = "100%";
            element1.setAttribute('onkeyup', 'javascript:return AllowDecimals(\'' + element1.id + '\')');
            //element1.style.paddingLeft ="25px";space
            //document.getElementById("element1").className = "space";
            // element1.className = "space";
            cell1.appendChild(element1);
            cellCount++;

            //Delete button
            var cell1 = row.insertCell(cellCount);
            var element1 = document.createElement("input");
            $(cell1).addClass("tdgray");
            $(cell1).addClass("td-DeleteRecord");
            element1.type = "image";
            //element1.id = "btnnnnnn";
            element1.src = "../Images/delete.gif";
            element1.setAttribute('onclick', 'javascript:DeleteRecord1("' + rowCount + '");return false;');
            cell1.appendChild(element1);
            //
            randomnumber = randomnumber + 1;
            document.getElementById('<%= hdnCount.ClientID %>').value = randomnumber;
            return false;

        }
        function DeleteRecord1(RowIndex) {
            debugger;
            var Row;
            var table = document.getElementById('<%= tblUsers.ClientID %>');

            for (var i = 0; i < table.rows.length; i++) {
                //if (typeof document.getElementsByName("username")[0] != 'undefined')
                if (typeof table.rows[i].cells[0].children[1] != 'undefined') {
                    if (parseInt(RowIndex) == parseInt(table.rows[i].cells[0].children[1].value)) {
                        Row = i;
                        break;
                    }
                }
            }
            $("#ContentMain_tblUsers" + " tr:eq(" + Row + ")").remove();
            RecalculateTable(Row);


            return false;
        }
        function DeleteRecord(RowIndex, id) {
            try {

                debugger;
                var table = document.getElementById('<%= tblUsers.ClientID %>');
                var Row;
                for (var i = 0; i < table.rows.length; i++) {
                    //if (typeof document.getElementsByName("username")[0] != 'undefined')
                    if (typeof table.rows[i].cells[0].children[1] != 'undefined') {
                        if (parseInt(RowIndex) == parseInt(table.rows[i].cells[0].children[1].value)) {
                            Row = i;
                            break;
                        }
                    }
                }
                if (parseInt(RowIndex) == 0) {
                    $("#ContentMain_tblUsers" + " tr:eq(" + RowIndex + ")").remove();
                }
                else {
                    $("#ContentMain_tblUsers" + " tr:eq(" + Row + ")").remove();
                }
                RecalculateTable(Row);
                //$.ajax({
                //    type: "POST",
                //    url: "Users.aspx/DeleteRecord",
                //    data: '{ID: "' + id + '"}',
                //    contentType: "application/json; charset=utf-8",
                //    dataType: "json",
                //    failure: function (response) {
                //        alert(response);
                //    }
                //});
            }
            catch (err) {
                alert(err);
            }
            return false;
        }
        function RecalculateTable(row) {

            var rows = row + 1;

            var table = document.getElementById('<%= tblUsers.ClientID %>');
            var rowCount = table.rows.length;
            for (var i = 1; i <= rowCount; i++) {
                if (i >= rows) {
                    var j = i + 1;
                    var txt1_1 = "ContentMain_txt1_" + j;
                    var txt1_2 = "ContentMain_txt2_" + j;
                    var txt1_3 = "ContentMain_txt3_" + j;
                    var txt1_4 = "ContentMain_txt4_" + j;
                    var txt1 = document.getElementById(txt1_1);
                    var txt2 = document.getElementById(txt1_2);
                    var txt3 = document.getElementById(txt1_3);
                    var txt4 = document.getElementById(txt1_4);
                    if (txt1 != null) {
                        txt1.id = "ContentMain_txt1_" + i;
                        txt1.name = "ContentMain_txt1_" + i;
                    }
                    if (txt2 != null) {
                        txt2.id = "ContentMain_txt2_" + i;
                        txt2.name = "ContentMain_txt2_" + i;
                    }
                    if (txt3 != null) {
                        txt3.id = "ContentMain_txt3_" + i;
                        txt3.name = "ContentMain_txt3_" + i;
                    }
                    if (txt4 != null) {
                        txt4.id = "ContentMain_txt4_" + i;
                        txt4.name = "ContentMain_txt4_" + i;
                    }
                }

            }
            var randomnumber;
            randomnumber = document.getElementById('<%= hdnCount.ClientID %>').value;
            randomnumber = randomnumber - 1;
            document.getElementById('<%= hdnCount.ClientID %>').value = randomnumber;
        }
        function adddollar() {
            var t1 = document.getElementById('TextBox1');
            if (t1.value != '') {
                if (t1.value.indexOf('$') == -1) {
                    t1.value = '$' + t1.value;
                    return false;
                }
            }
        }

    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--    <link href="../Styles/SiteTEST.css" rel="stylesheet" />
     <script src="../Js/CommonValidations.js"></script>--%>
    <table style="width: 100%">
        <tr>
            <td style="padding-top: 0.5em; color: black; font-family: Verdana; border-color: black; text-align: left; text-shadow: initial; font-weight: 600; font-size: 18px;">ADMINISTRATION
                <asp:Label ID="lbErrorMsg" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="Small" /></td>
            <td class="mainPageTitle">USERS
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <link href="../Styles/AutoCompleteStyle.css" rel="stylesheet" />
    <%--    <asp:UpdatePanel ID="upAddUser" runat="server">
        <ContentTemplate>--%>
    <table style="width: 100%; padding-left: 10px;">
        <tr>

            <td align="right">
                <asp:TextBox ID="txtSearch" runat="server" Height="18px" CssClass="normalTextBox"></asp:TextBox>
                <ajax:TextBoxWatermarkExtender ID="TBWE2" runat="server"
                    TargetControlID="txtSearch"
                    WatermarkText="Search" />


            </td>
            <td align="right" style="padding-top: 0.5em; width: 30px; padding-right: 12px;">
                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/Images/Search-Icon.gif" Width="18px" Height="18px" OnClick="btnSearch_Click" />
            </td>

            <%-- <td style="width: 10%"></td>--%>
        </tr>
        <%--<tr>
                    <td colspan ="3">
                         <hr style="width: 90%" />
                    </td>
                </tr>--%>
    </table>
    <%--       </ContentTemplate>
    </asp:UpdatePanel>--%>




    <table style="width: 100%;">
        <tr>
            <td align="left" style="padding-left: 10px;">
                <%-- <asp:Label ID="lblTotalRecord" Text ="" runat ="server" Font-Bold="true"></asp:Label>--%>
            </td>
            <td align="right" style="padding-right: 4px;">

                <asp:Button ID="btnAddOptTerms" runat="server" Text="ADD USER" OnClick="btnAddOptTerms_Click" OnClientClick="ClearControl();" CssClass="SqureButton" />
                <ajax:ModalPopupExtender ID="modalAdd" runat="server" PopupControlID="pnlpopup" BackgroundCssClass="modalBackground" TargetControlID="lnbPopUp1" CancelControlID="imgbtnClose" BehaviorID="testIt"></ajax:ModalPopupExtender>
                <asp:LinkButton ID="lnbPopUp1" runat="server"></asp:LinkButton>
            </td>
            <%--    <td align="right" style="padding-right:10px;" > 
         
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
          <asp:Label ID="lblPages" runat="server" Font-Bold="true" Style="padding-left: 10px;"></asp:Label>
            </ContentTemplate> </asp:UpdatePanel> </td>--%>
        </tr>
        <tr>
            <td colspan="2">
                <asp:UpdatePanel ID="upUsers" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Panel ID="pnUsers" runat="server" Width="100%">


                            <div style="padding-left: 10px;">
                                <asp:GridView ID="gvUsers" runat="server" Style="width: 99%;" AutoGenerateColumns="false"
                                    EmptyDataText="No Records Found" AllowPaging="true" DataKeyNames="UserID" OnRowCreated="gvUsers_RowCreated"
                                    OnRowCommand="gvUsers_RowCommand" OnPageIndexChanging="gvUsers_PageIndexChanging" PageSize="20" OnRowDataBound="gvUsers_RowDataBound"
                                    HeaderStyle-CssClass="HeaderGridView"
                                    FooterStyle-CssClass="FooterGridView"
                                    RowStyle-CssClass="RowGridView" AlternatingRowStyle-CssClass="AlternatingRowGridView2">



                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="ACTIVE" ItemStyle-Width="20px">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkActive" runat="server" Height="15px" Width="15px" CommandArgument='<%# Eval("UserID") %>' onchange='<%# Eval("UserID","return SetActive({0})") %>' />
                                                <asp:HiddenField ID="hdnActive" runat="server" Value='<% #Bind("IsActive")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Username" HeaderText="UserName" />
                                        <%--<asp:BoundField DataField="Company" HeaderText="Company" />--%>
                                        <asp:BoundField DataField="Email" HeaderText="Email" />
                                        <asp:BoundField DataField="LastModifiedOn" HeaderText="Last Updated" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnUserId" runat="server" Value='<%# Eval("UserID") %>' />
                                               <%-- <asp:LinkButton ID="lnkEdit" CommandArgument='<%# Eval("UserID") %>' CommandName="Ed"
                                                    runat="server">
                                                    <asp:Image ID="imgEdit" runat="server" ToolTip="Edit this User" ImageUrl="~/Images/edit.jpg"
                                                        Height="15" Width="15" />
                                                </asp:LinkButton>--%>
                                                <asp:ImageButton ID="lnkEdit" runat ="server" CommandArgument='<%# Eval("UserID") %>' CommandName="Ed" ImageUrl ="~/Images/edit.jpg"/> 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnCanDel" runat="server" Value='<%# Eval("CanDelete") %>' />

                                                <asp:ImageButton runat="server" ImageUrl="~/Images/delete.gif" OnClientClick='javascript:return confirm("Do You Want To Delete This Record");' ID="ImgDelete" CommandName="Del" CommandArgument='<%# Eval("UserID") %>' />
                                                <%-- <asp:LinkButton ID="lnkDelete" CommandArgument='<%# Eval("UserID") %>' CommandName="Del"
                                    OnClientClick="return confirm('Are you sure you want delete ?')" runat="server">
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/delete.gif" Height="15"
                                        Width="15" ToolTip="Delete this User" />
                                </asp:LinkButton>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerTemplate>
                                        <asp:PlaceHolder ID="phPageNo" runat="server" />
                                    </PagerTemplate>
                                    <PagerStyle CssClass="pagerStyle" />
                                </asp:GridView>
                            </div>
                        </asp:Panel>
                        <ajax:ModalPopupExtender ID="modalAddEdit" runat="server" PopupControlID="pnlpopup" BehaviorID="testIt_1"
                            BackgroundCssClass="modalBackground" TargetControlID="lnbPopUp" CancelControlID="imgbtnClose">
                        </ajax:ModalPopupExtender>
                        <asp:LinkButton ID="lnbPopUp" runat="server" Text=""></asp:LinkButton>
                        <%-- <div id="pnlpopup" runat="server" style ="height:550px;overflow:auto;background-color:white;z-index: 111;position: absolute; left: 35%; top: 12%;  padding: 5px; display: block;border: 1px solid;" >--%>

                        <div class="PopupDivBodyStyle" id="pnlpopup" runat="server" style="width: 510px; height: 570px; background-color: white; z-index: 111; position: absolute; display: none;">
                            <%--  <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Width="450px" BorderColor="Transparent" Height="500px" ScrollBars="Vertical" Style="z-index: 111; background-color: white; position: absolute; left: 35%; top: 12%;  padding: 5px; display: block;border: 1px solid;">--%>
                            <%-- <div style="flex-align: baseline;">--%>
                            <table style="width: 100%" class="spacing" cellspacing="0px" cellpadding="0px">
                                <tr style="background-color: gray; border-bottom: solid;">
                                    <td colspan="2">
                                        <table>
                                            <tr>
                                                <td style="text-align: left; width: 490px;">
                                                    <asp:Label ID="lblHeader" runat="server" Text="USER DETAILS" ForeColor="Black" Style="padding-left: 10px; vertical-align: middle; font-weight: bold;"></asp:Label>
                                                </td>
                                                <td style="text-align: right;">
                                                    <asp:ImageButton ID="imgbtnClose" runat="server" ImageUrl="~/Images/Close.png" AlternateText="Close"
                                                        ForeColor="Red" ImageAlign="Right" Style="margin-top: 4px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%--  <tr style="background-color: gray; border-bottom: solid;">
                        <td style="text-align: left; padding-left: 10px; width: 100px;">
                            <h3>
                               
                                <asp:Label ID="lblHeader" runat="server" Text="USER DETAILS" ForeColor="Black" Style="padding-left: 10px; vertical-align: middle; font-weight: bold;"></asp:Label></h3>
                        </td>
                        <td align="right">
                            <asp:ImageButton ID="imgbtnClose" runat="server" ImageUrl="~/Images/Close.png" AlternateText="Close"
                                ForeColor="Red" ImageAlign="Right" Style="margin-top: 4px" />

                        </td>

                    </tr>--%>
                                <tr>
                                    <td style="background: #FFFFFF;" colspan="2">
                                        <%-- <asp:Image ID="imgAdd" ImageUrl="~/Images/Edit.gif" runat="server" />--%>
                                        <asp:Label ID="lblNames" runat="server" Text="LAST NAME, FIRST NAME " ForeColor="Chocolate" Style="padding-left: 10px; vertical-align: middle; font-size: medium; font-weight: bold;"></asp:Label>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr style="width: 95%" />
                                    </td>
                                </tr>
                                <%--   </table>
             
                <table style="width: 100%; border: 0px;">--%>
                                <tr>
                                    <td colspan="2" style="padding-left: 10px; height: 18px;">
                                        <asp:Label ID="lblErrorMsg" runat="server" Text="" ForeColor="Red"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px; width: 100px;">
                                        <asp:Label ID="lblFirstName" Text="First Name" runat="server">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFirstName" runat="server" Width="370px">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px; width: 100px;">
                                        <asp:Label ID="lblLastName" Text="Last Name" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLastName" runat="server" Width="370px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px; width: 100px;">
                                        <asp:Label ID="lblCompany" Text="Company" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCompany" runat="server" Width="370px"></asp:TextBox>
                                        <ajax:AutoCompleteExtender ID="acBrokerInfo" runat="server"
                                            TargetControlID="txtCompany"
                                            DelimiterCharacters=";, :"
                                            MinimumPrefixLength="1"
                                            EnableCaching="true"
                                            CompletionSetCount="10"
                                            CompletionInterval="300"
                                            ServiceMethod="SearchGetComapnyList"
                                            OnClientItemSelected="OnCenterSelected"
                                            OnClientPopulating="ShowIcon"
                                            OnClientPopulated="hideIcon"
                                            ShowOnlyCurrentWordInCompletionListItem="true"
                                            CompletionListCssClass="AutoExtender"
                                            CompletionListItemCssClass="AutoExtenderList"
                                            CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                            CompletionListElementID="divwidth" />
                                        <asp:HiddenField ID="hdnBrokerId" runat="server" Value="" />
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding-left: 10px; width: 100px;">
                                        <asp:Label ID="lblEmail" Text="Email" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEmail" runat="server" Width="370px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px; width: 100px;">
                                        <asp:Label ID="lblPassword" Text="Password" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPwd" runat="server" Width="370px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px; width: 100px;">
                                        <asp:Label ID="lblConfPwd" Text="Confirm PW" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtConfmPass" runat="server" Width="370px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px; width: 100px;">
                                        <asp:Label ID="lblActive" Text="Active" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkActive" runat="server" Checked="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr style="width: 95%" />
                                    </td>
                                </tr>


                                <tr>
                                    <td style="flex-align: center; padding-left: 10px; width: 100px;">
                                        <asp:Label ID="lblPeriod" Text="Period" runat="server">
                                        </asp:Label>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtFrom" runat="server" Width="80px"></asp:TextBox>
                                                    <ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txtFrom" runat="server" />
                                                </td>
                                                <td>&nbsp;
                                        <asp:Label ID="lblTo" Text="To" runat="server">
                                        </asp:Label>&nbsp;
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTo" runat="server" Width="80px"></asp:TextBox>
                                                    <ajax:CalendarExtender ID="CalendarExtender2" TargetControlID="txtTo" runat="server" />

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>

                                    <td>
                                        <table style="border: 1px solid; width: 95%; padding: 8px 8px 8px 8px">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td style="width: 240px;">EARNING($)</td>
                                                            <td style="width: 170px; padding-left: 10px;">COMMISSION SPLIT(%)</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 240px;">
                                                                <hr />
                                                            </td>
                                                            <td style="width: 170px;">
                                                                <hr style="margin-left: 10px" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 230px;"></td>
                                                            <td style="width: 180px;">
                                                                <table>
                                                                    <tr>
                                                                        <td style="padding-left: 30px;">Broker </td>
                                                                        <td style="padding-left: 30px;">Company</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <%--  <tr>
                                    <td style="width: 100px;">EARNING
                                    </td>

                                    <td>COMISSION SPLIT
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr />
                                    </td>

                                    <td>
                                        <hr />
                                    </td>
                                </tr>--%>
                                            <%--  <tr>
                                                    <td></td>
                                                    <td align="right">
                                                        <table>
                                                            <tr>
                                                                <td>Broker </td>
                                                                <td>Company</td>
                                                            </tr>
                                                        </table>
                                                    </td>


                                                </tr>--%>
                                            <tr>

                                                <td colspan="2">
                                                    <div style="overflow: auto; height: 80px;">
                                                        <asp:Table ID="tblUsers" runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:HiddenField runat="server" ID="hdn1_1" />
                                                                    <asp:HiddenField runat="server" ID="hdnCheck_1" />
                                                                </asp:TableCell>

                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txt1_1" runat="server" Width="90%" onkeyup=""></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell HorizontalAlign="Center">
                                                                  <hr style="width:8px;"/>
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txt2_1" runat="server" Width="100%"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    &nbsp;
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txt3_1" runat="server" Width="90%"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txt4_1" runat="server" Width="100%"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:ImageButton ID="imgDelete" ImageUrl="~/Images/delete.gif" runat="server" />
                                                                    <%--                                                    <asp:Button ID="btnDelete" runat ="server" Text="kk" OnClientClick ="DeleteRecord('0')"/>--%>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                        <asp:HiddenField ID="hdnCount" runat="server" Value="2" />

                                                    </div>
                                                </td>


                                            </tr>
                                            <tr>
                                                <td colspan="2" align="right">
                                                    <%--                                        <asp:Button ID="btnAddDup" Text ="save" runat ="server" OnClientClick ="javascript:addDuplicateEmail(); return false;" CssClass ="SqureButton"/>--%>
                                                    <input type="button" onclick="addDuplicateEmail(); return false;" value="Add Earnings" class="SqureButton" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">
                                        <br />
                                    </td>

                                </tr>



                                <tr>
                                    <td colspan="2">
                                        <table>
                                            <tr>
                                                <td style="width: 110px; padding-left: 5px;">
                                                    <asp:Label ID="lblLastUpdated" runat="server"></asp:Label></td>
                                                <td style="width: 100px;">
                                                    <asp:Label ID="lblDate" runat="server"></asp:Label></td>
                                                <td style="padding-left: 60px;">
                                                    <asp:Button ID="btnClear" runat="server" Width="100" Text="CLEAR" OnClientClick="return ClearControl1();" CssClass="SqureButton" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnFillSave" runat="server" OnClick="btnFillSave_Click" Style="display: none;" />
                                                    <asp:Button ID="btnSave" runat="server" Text="SAVE" Width="100" OnClientClick="return ValidateDynamicControls();" CssClass="SqureButton" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <%-- <td  style="width:320px;">
                        </td>
                        <td style="text-align: right;" >
                            <asp:Button ID="btnClear" runat ="server" Text="CLEAR" OnClientClick="return ClearControl1();" CssClass="SqureButton" />
                            <asp:Button ID="btnSave" runat="server" Text="SAVE" Width="100" OnClientClick="return ValidateDynamicControls();" OnClick="btnSaveUsers_Click" CssClass="SqureButton"/>
                        </td>--%>
                                </tr>
                            </table>
                            <%--  </div>--%>
                            <%--  </asp:Panel>--%>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" runat="Server">
</asp:Content>

