﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using SegallChargeSlipBll;

public partial class Admin_DealSubType : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Bindgrid();
            BindDropDown();
        }
    }
    private void BindDropDown()
    {
        try
        {
            CustomList objCL = new CustomList();
            Utility.FillDropDown(ref ddlDealType, objCL.GetDealTypes(), "DealType", "DealTypeID");
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("DealSubType.aspx", "BindDropDown", ex.Message);
        }



    }
    private void Bindgrid()
    {
        try
        {
            DealSubTypes objDealSubTypes = new DealSubTypes();
            DataSet ds = new DataSet();
            ds = objDealSubTypes.GetAllSubType();
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grdDealSubType.DataSource = ds.Tables[0];
                    grdDealSubType.DataBind();
                }
                else
                {
                    grdDealSubType.DataSource = null;
                    grdDealSubType.DataBind();
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealSubType.aspx", "Bindgrid", ex.Message);
        }
    }
    protected void btnAddDealSubType_Click(object sender, EventArgs e)
    {
        ViewState["DealSubTypeID"] = null;
        ddlDealType.SelectedIndex = 0;
        txtDealSubType.Text = string.Empty;
        chkIsActive.Checked = true;
        btnUpdate.Text = "ADD";
        mpeUpdateDealSubType.Show();
    }
    protected void grdDealSubType_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Del")
            {
                DealSubTypes objDealSubTypes = new DealSubTypes();
                objDealSubTypes.DealSubTypeID = Convert.ToInt32(e.CommandArgument);
                bool load = objDealSubTypes.Delete();
                Bindgrid();
                //if (load)
                //{
                //    objDealSubTypes.IsActive = false;
                //    objDealSubTypes.Save();
                //    Bindgrid();
                //}                  

            }
            if (e.CommandName == "Ed")
            {
                DealSubTypes objDealSubTypes = new DealSubTypes();
                objDealSubTypes.DealSubTypeID = Convert.ToInt32(e.CommandArgument);
                bool load = objDealSubTypes.Load();
                if (load)
                {
                    ddlDealType.SelectedValue = Convert.ToString(objDealSubTypes.DealTypeID);
                    txtDealSubType.Text = objDealSubTypes.DealSubType;
                    if (objDealSubTypes.IsActive == true)
                    {
                        chkIsActive.Checked = true;
                    }
                    if (objDealSubTypes.IsActive == false)
                    {
                        chkIsActive.Checked = false;
                    }
                    ViewState["DealSubTypeID"] = objDealSubTypes.DealSubTypeID;
                    btnUpdate.Text = "UPDATE";
                    mpeUpdateDealSubType.Show();

                }

            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealSubType.aspx", "grdDealType_RowCommand", ex.Message);
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (ViewState["DealSubTypeID"] != null)
            {
                if (!string.IsNullOrEmpty(ViewState["DealSubTypeID"].ToString()))
                {
                    DealSubTypes objDealSubTypes = new DealSubTypes();
                    objDealSubTypes.DealSubTypeID = Convert.ToInt32(ViewState["DealSubTypeID"].ToString());
                    objDealSubTypes.Load();
                    objDealSubTypes.DealTypeID = Convert.ToInt32(ddlDealType.SelectedValue);
                    objDealSubTypes.DealSubType = txtDealSubType.Text.Trim();
                    if (chkIsActive.Checked)
                    {
                        objDealSubTypes.IsActive = true;
                    }
                    else
                    {
                        objDealSubTypes.IsActive = false;
                    }
                    objDealSubTypes.Save();
                    Bindgrid();
                }
            }
            else
            {
                DealSubTypes objDealSubTypes = new DealSubTypes();
                objDealSubTypes.DealTypeID = Convert.ToInt32(ddlDealType.SelectedValue);
                objDealSubTypes.DealSubType = txtDealSubType.Text;
                if (chkIsActive.Checked)
                {
                    objDealSubTypes.IsActive = true;
                }
                else
                {
                    objDealSubTypes.IsActive = false;
                }
                objDealSubTypes.Save();
                Bindgrid();
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealSubType.aspx", "btnUpdate_Click", ex.Message);
        }
    }
    protected void grdDealSubType_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            //DealSubTypes objDealSubTypes = new DealSubTypes();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnDealSubTypeId = (HiddenField)e.Row.FindControl("hdnDealSubTypeId") as HiddenField;
                //objDealSubTypes.DealSubTypeID = Convert.ToInt32(hdnDealSubTypeId.Value);
                CheckBox chkActive = (CheckBox)e.Row.FindControl("chkActive");
                HiddenField hdnActive = (HiddenField)e.Row.FindControl("hdnActive") as HiddenField;
                HiddenField hdnCanDel = (HiddenField)e.Row.FindControl("hdnCanDel") as HiddenField;
                int candel = Convert.ToInt32(hdnCanDel.Value);
                if (candel == 0)
                {
                    ImageButton imgDelete = (ImageButton)e.Row.FindControl("delRecord") as ImageButton;
                    imgDelete.Enabled = false;
                    imgDelete.Width = 16;
                    imgDelete.Height = 16;
                    imgDelete.ImageUrl = "~/Images/deletegray.png";
                }
                //DataTable dt = objDealSubTypes.LoadDealTransaction();
                //if (dt != null)
                //{
                //    if (dt.Rows.Count > 0)
                //    {
                //        ImageButton imgDelete = (ImageButton)e.Row.FindControl("delRecord") as ImageButton;
                //        imgDelete.Enabled = false;
                //        // imgDelete.Visible = false;
                //        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Update Deal Type", "alert('Deal Type cannot be delete because it has referenced.');", true);
                //    }
                //}
                if (hdnActive != null)
                {
                    if (hdnActive.Value == "True")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealSubType.aspx", "grdDealSubType_RowDataBound", ex.Message);
        }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SetActive(string SubTypeID)
    {
        try
        {
            DealSubTypes objDealSubTypes = new DealSubTypes();
            objDealSubTypes.DealSubTypeID = Convert.ToInt32(SubTypeID);
            bool load = objDealSubTypes.Load();
            if (load)
            {
                if (objDealSubTypes.IsActive)
                {
                    objDealSubTypes.IsActive = false;
                }
                else
                {
                    objDealSubTypes.IsActive = true;
                }
                objDealSubTypes.Save();

            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("DealSubType.aspx", "SetActive", ex.Message);
        }
    }
}