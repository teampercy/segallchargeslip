﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;
public partial class Admin_Vendors : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["IsVendor"] = "1";
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void GetChargeSlipDetails(string RowIndx)
    {
        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {
                int ChargeSlipID = Convert.ToInt32(HttpContext.Current.Session["ChargeSlipId"]);
                Utility.ClearSessionExceptUser();
                ChargeSlip objChargeSlip = new ChargeSlip();
                objChargeSlip.ChargeSlipID = ChargeSlipID;
                objChargeSlip.Load();

                DealTransactions objDealTransactions = new DealTransactions();
                objDealTransactions.ChargeSlipID = ChargeSlipID;
                objDealTransactions.LoadByChargeSlipId();


                objChargeSlip.DealTransactionsProperty = objDealTransactions;
                HttpContext.Current.Session[GlobleData.NewChargeSlipObj] = objChargeSlip;
                HttpContext.Current.Session[GlobleData.NewChargeSlipId] = ChargeSlipID;
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Vendors.aspx", "GetChargeSlipDetails", ex.Message);
        }
    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] SearchGetComapnyList(string prefixText, int count)
    {
        List<string> lstSearchList = new List<string>();
        try
        {
            Users objUsers = new Users();
            objUsers.CompanyName = prefixText;
            DataSet ds = objUsers.GetCompanySearchList();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dt in ds.Tables[0].Rows)
                {
                    //dt["ValueType"].ToString()
                    lstSearchList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dt["CompanyName"].ToString(), dt["CompanyName"].ToString()));
                    // lstSearchList.Add(dt[1].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Vendors.aspx", "SearchGetComapnyList", ex.Message);
        }
        return lstSearchList.ToArray();
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SaveBrokerCheckNo(string PaymentHistoryId, string CheckNo)
    {
        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {
                if ((!String.IsNullOrEmpty(PaymentHistoryId)) && (!String.IsNullOrEmpty(CheckNo)))
                {
                    int PHID = Convert.ToInt32(PaymentHistoryId.Trim());
                    DealTransactions objDealTransactions = new DealTransactions();
                    objDealTransactions.UpdateCheckNo(PHID, CheckNo);
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Customer.aspx", "GetChargeSlipDetails", ex.Message);
        }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string SavePaymentAmount(string Paymentd, string Amount)
    {
        string str = "0";
        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {
                //if ((!String.IsNullOrEmpty(PaymentHistoryId)) && (!String.IsNullOrEmpty(CheckNo)))
                if ((!String.IsNullOrEmpty(Paymentd)))
                {
                    int PHID = Convert.ToInt32(Paymentd.Trim());
                    decimal PaymentAmount = Convert.ToDecimal(Amount);
                    Payments obj = new Payments();
                    if (HttpContext.Current.Session[GlobleData.User] != null)
                    {
                        DataSet ds = new DataSet();
                        ds = obj.UpdatePaymentAmount(PHID, PaymentAmount, Convert.ToInt32(HttpContext.Current.Session[GlobleData.User].ToString()));
                        if (ds != null)
                        {
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {

                                    str = "1";
                                    return str;
                                }
                            }
                            else
                            {

                                return str;
                            }
                        }
                        else
                        {

                            return str;
                        }
                    }
                }
            }
            return str;
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Customer.aspx", "SavePaymentAmount", ex.Message);
        }
        return str;
    }
    protected void btnRefreshPayment_Click(object sender, EventArgs e)
    {
        if (hdnChargeslipId.Value != "0")
        {
            ucPaymentHistoryDetail ucbPay = (ucPaymentHistoryDetail)ucBilledTab1.FindControl("ucPaymentHistoryDetail1");
            if (ucbPay != null)
            {
                ucbPay.ChargeSlipId = Convert.ToInt32(hdnChargeslipId.Value.ToString());
                ucbPay.Bindata();

                if (ucBilledTab1.FindControl("hdnPopup") != null)
                {
                    HiddenField hdn = (HiddenField)ucBilledTab1.FindControl("hdnPopup");
                    hdn.Value = "0";
                }
            }
        }

    }
}