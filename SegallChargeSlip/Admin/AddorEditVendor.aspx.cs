﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_AddorEditVendor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["PageSize"] = 20;
            ViewState["PageNo"] = 1;
            Utility.FillStates(ref ddlStatesearch);
            Utility.FillStates(ref ddlState);
            Bindgrid();
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        GridViewRow grdVendorToPager = grdVendor.BottomPagerRow;
        if (grdVendorToPager != null)
        {
            grdVendorToPager.Visible = true;
        }
    }
    public void Bindgrid()
    {
        try
        {
            Users objUsers = new Users();
            objUsers.CompanyName = txtCompany.Text.Trim();
            objUsers.City = txtCity.Text.Trim();
            objUsers.State = ddlStatesearch.SelectedValue;
            if (!string.IsNullOrEmpty(txtZipCode.Text.Trim()))
            {
                objUsers.Zip = txtZipCode.Text.Trim();
            }
            DataSet ds = new DataSet();
            ds = objUsers.GetList(Convert.ToInt32(ViewState["PageNo"].ToString()), Convert.ToInt32(ViewState["PageSize"].ToString()));
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    double TotalRecord = Convert.ToInt32(ds.Tables[0].Rows[0]["RowCounts"]);
                    ViewState["TotalRecord"] = TotalRecord;
                    double Pagesize = Convert.ToInt32(ViewState["PageSize"].ToString());
                    double TotalPage = Math.Ceiling(TotalRecord / Pagesize);
                    ViewState["TotalPage"] = TotalPage;
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    grdVendor.DataSource = ds.Tables[1];
                    grdVendor.DataBind();
                }
                else
                {
                    grdVendor.DataSource = null;
                    grdVendor.DataBind();
                }


            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/AddorEditVendor.aspx", "Bindgrid", ex.Message);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            ViewState["PageNo"] = "1";
            Bindgrid();
            //Users obj = new Users();
            //obj.CompanyName = txtCompany.Text.Trim();
            //obj.City = txtCity.Text.Trim();
            //obj.State = ddlStatesearch.SelectedValue;
            //if (!string.IsNullOrEmpty(txtZipCode.Text.Trim()))
            //{
            //    obj.Zip = txtZipCode.Text.Trim();
            //}
            //DataSet ds = new DataSet();
            //ds = obj.GetList(Convert.ToInt32(ViewState["PageNo"].ToString()), Convert.ToInt32(ViewState["PageSize"].ToString()));
            //if (ds != null)
            //{
            //    if (ds.Tables[0].Rows.Count > 0)
            //    {
            //        double TotalRecord = Convert.ToInt32(ds.Tables[0].Rows[0]["RowCounts"]);
            //        double Pagesize = Convert.ToInt32(ViewState["PageSize"].ToString());
            //        double TotalPage = Math.Ceiling(TotalRecord / Pagesize);
            //        ViewState["TotalPage"] = TotalPage;
            //    }
            //    if (ds.Tables[1].Rows.Count > 0)
            //    {
            //        grdVendor.DataSource = ds.Tables[1];
            //        grdVendor.DataBind();
            //    }
            //    else
            //    {
            //        grdVendor.DataSource = null;
            //        grdVendor.DataBind();
            //    }
            //}
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/AddorEditVendor.aspx", "btnSearch_Click", ex.Message);
        }
    }
    protected void btnVendor_Click(object sender, EventArgs e)
    {
        ViewState["UserID"] = null;
        txtFirstName.Text = string.Empty;
        txtLastName.Text = string.Empty;
        txtCompanies.Text = string.Empty;
        txtAddress.Text = string.Empty;
        txtCities.Text = string.Empty;
        txtZip.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtEIN.Text = string.Empty;
        drpBrokerType.SelectedValue = "0";
        ddlState.SelectedIndex = -1;
        btnAddBroker.Text = "ADD";
        mpeUpdateVendor.Show();
    }
    protected void grdVendor_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
           
            if (e.CommandName == "Ed")
            {
                Users objUsers = new Users();
                objUsers.UserID  = Convert.ToInt32(e.CommandArgument);
                bool load = objUsers.Load();
                if (load)
                {
                    txtFirstName.Text = objUsers.FirstName;
                    txtLastName.Text = objUsers.LastName;
                    txtCompanies.Text = objUsers.CompanyName;
                    txtAddress.Text = objUsers.Address1;
                    txtCities.Text = objUsers.City;
                    ddlState.SelectedValue = objUsers.State;
                    txtZip.Text = objUsers.Zip;
                    txtEmail.Text = objUsers.Email;
                    txtEIN.Text = objUsers.EIN;
                    drpBrokerType.SelectedValue =Convert .ToString(objUsers.BrokerTypeId);
                    if (objUsers.IsActive == true)
                    {
                        chkIsActive.Checked = true;
                    }
                    if (objUsers.IsActive == false)
                    {
                        chkIsActive.Checked = false;
                    }
                    ViewState["UserID"] = objUsers.UserID;
                    btnAddBroker.Text = "UPDATE";
                    mpeUpdateVendor.Show();

                }

            }
            if (e.CommandName == "Del")
            {                
                Users user = new Users();
                user.UserID = Convert.ToInt32(e.CommandArgument);
                bool deleteUser = user.Delete();

                Bindgrid();
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/AddorEditVendor.aspx", "grdVendor_RowCommand", ex.Message);
        }
    }
    protected void grdVendor_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnUseId = (HiddenField)e.Row.FindControl("hdnUseId") as HiddenField;                
                CheckBox chkActive = (CheckBox)e.Row.FindControl("chkActive");
                HiddenField hdnActive = (HiddenField)e.Row.FindControl("hdnActive") as HiddenField;
                HiddenField hdnCanDel = (HiddenField)e.Row.FindControl("hdnCanDel") as HiddenField;
                int candel = Convert.ToInt32(hdnCanDel.Value);
                if (candel == 0)
                {
                    ImageButton imgDelete = (ImageButton)e.Row.FindControl("delRecord") as ImageButton;
                    imgDelete.Enabled = false;
                    imgDelete.Width = 16;
                    imgDelete.Height = 16;
                    imgDelete.ImageUrl = "~/Images/deletegray.png";
                }
                
                
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/AddorEditVendor.aspx", "grdVendor_RowDataBound", ex.Message);
        }
    }
    protected void grdVendor_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            //int PageNo;
            //PageNo = e.NewPageIndex + 1;
            //ViewState["PageNo"] = PageNo;
            //Bindgrid();

            int PageNo = Convert.ToInt32(ViewState["PageNo"]);

            if (e.NewPageIndex == -1)
            {
                PageNo++;
                ViewState["PageNo"] = PageNo;
            }
            else if (e.NewPageIndex == -2)
            {
                PageNo--;
                ViewState["PageNo"] = PageNo;
            }
            else
            {
                PageNo = e.NewPageIndex + 1;
                ViewState["PageNo"] = PageNo;
            }
            Bindgrid();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("AddorEditVendor.aspx", "grdVendor_PageIndexChanging", ex.Message);
        }
    }
    protected void grdVendor_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                int pageno = Convert.ToInt32(ViewState["PageNo"].ToString());
                double totalpage = Convert.ToDouble(ViewState["TotalPage"].ToString());
                LinkButton btnPrevious = new LinkButton();
                btnPrevious.ID = "btnPrevious";
                btnPrevious.Text = "<";
                btnPrevious.CommandName = "Page";
                btnPrevious.CommandArgument = "-1";
                btnPrevious.Width = Unit.Pixel(20);
                btnPrevious.CssClass = "removeunderline";

                LinkButton btnNext = new LinkButton();
                btnNext.ID = "btnNext";
                btnNext.Text = ">";
                btnNext.CommandName = "Page";
                btnNext.CssClass = "removeunderline";
                btnNext.CommandArgument = "0";
                e.Row.Cells[0].Controls.Add(btnPrevious);
                for (int i = 1; i <= totalpage; i++)
                {
                    LinkButton lnk = new LinkButton();
                    lnk.ID = "lnk" + i;
                    lnk.Text = i.ToString() + " ";
                    lnk.CommandArgument = i.ToString();
                    lnk.CommandName = "Page";
                    e.Row.Cells[0].Controls.Add(lnk);
                    lnk.CssClass = "removeunderline";
                    if (Convert.ToInt32(ViewState["PageNo"].ToString()) == i)
                    {
                        lnk.CssClass = "linkbut";
                    }
                }
                e.Row.Cells[0].Controls.Add(btnNext);
                if (ViewState["TotalRecord"] != null)
                {
                    
                    Label lblTotalRecords = new Label();
                    lblTotalRecords.CssClass = "PageNoCSS";
                    lblTotalRecords.Text = "Total Records:" + ViewState["TotalRecord"].ToString();
                    e.Row.Cells[0].Controls.Add(lblTotalRecords);
                }
                //Label lblpages = new Label();
                //lblpages = (Label)e.Row.FindControl("lblPages");
                //lblPages.Text = "Page No:" + pageno + " of " + totalpage + " Pages";
                if (Convert.ToInt32(ViewState["TotalPage"]) == 1)
                {
                    btnNext.Enabled = false;
                    btnPrevious.Enabled = false;
                }
                else if (Convert.ToInt32(ViewState["TotalPage"]) == Convert.ToInt32(ViewState["PageNo"]))
                {
                    btnNext.Enabled = false;
                    btnPrevious.Enabled = true;
                }
                else if (Convert.ToInt32(ViewState["PageNo"]) == 1)
                {
                    btnNext.Enabled = true;
                    btnPrevious.Enabled = false;
                }
                else
                {
                    btnPrevious.Enabled = true;
                    btnNext.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("AddorEditVendor.aspx", "grdVendor_RowCreated", ex.Message);
        }
    }
    protected void btnAddBroker_Click(object sender, EventArgs e)
    {
        try
        {
            if (ViewState["UserID"] != null)
            {
                if (!string.IsNullOrEmpty(ViewState["UserID"].ToString()))
                {
                    Users objUser = new Users();
                    objUser.UserID  = Convert.ToInt32(ViewState["UserID"].ToString());
                    objUser.Load();
                    objUser.FirstName = txtFirstName.Text;
                    objUser.LastName = txtLastName.Text;
                    objUser.CompanyName = txtCompanies.Text;
                    objUser.Address1 = txtAddress.Text;
                    objUser.City = txtCities.Text;
                    objUser.State = ddlState.SelectedValue;
                    objUser.Zip = txtZip.Text;
                    objUser.Email = txtEmail.Text;
                    objUser.EIN = txtEIN.Text;
                    objUser.BrokerTypeId =Convert.ToInt32(drpBrokerType.SelectedValue);
                    objUser.LastModifiedOn = DateTime.Now;
                    objUser.LastModifiedBy = Convert.ToInt32(Session[GlobleData.User].ToString());
                    if (chkIsActive.Checked)
                    {
                        objUser.IsActive = true;
                    }
                    else
                    {
                        objUser.IsActive = false;
                    }
                    objUser.Save();
                    Bindgrid();
                }
            }
            else
            {
                Users objUser = new Users();
                objUser.FirstName = txtFirstName.Text;
                objUser.LastName = txtLastName.Text;
                objUser.CompanyName = txtCompanies.Text;
                objUser.Address1 = txtAddress.Text;
                objUser.City = txtCities.Text;
                objUser.State = ddlState.SelectedValue;
                objUser.Zip = txtZip.Text;
                objUser.Email = txtEmail.Text;
                objUser.EIN = txtEIN.Text;
                objUser.BrokerTypeId = Convert.ToInt32(drpBrokerType.SelectedValue);
                objUser.CreatedOn = DateTime.Now;
                objUser.LastModifiedOn = DateTime.Now;
                objUser.CreatedBy = Convert.ToInt32(Session[GlobleData.User].ToString());
                if (chkIsActive.Checked)
                {
                    objUser.IsActive = true;
                }
                else
                {
                    objUser.IsActive = false;
                }
                objUser.Save();
                Bindgrid();
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/AddorEditVendor.aspx", "btnAddBroker_Click", ex.Message);
        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        ViewState["PageNo"] = "1";
        Bindgrid();
    }
}