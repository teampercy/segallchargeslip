﻿using SegallChargeSlipBll;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Admin_ShoppingCenter : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {

            Session.Clear();
            Session.RemoveAll();
            Server.Transfer("../Login.aspx");

        }
       
        if (!Page.IsPostBack)
        {
            Utility.FillStates(ref ddlLocationStatesearch);
          
            Bindgrid(1);
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        GridViewRow grdStud = grdShoppingCenter.BottomPagerRow;
        if (grdStud != null)
        {
            grdStud.Visible = true;
        }
    }
    public void Bindgrid(int pageno=1)
    {
        try
        {
            Locations objLocation = new Locations();
            
            objLocation.CurrentPageNo = pageno;
           
            objLocation.PageSize = 25;
            if (!String.IsNullOrEmpty(txtLocationSearchName.Text))
                objLocation.ShoppingCenter = txtLocationSearchName.Text;
            if (!String.IsNullOrEmpty(txtLocationSearchCity.Text))
                objLocation.City = txtLocationSearchCity.Text;
            if (!String.IsNullOrEmpty(ddlLocationStatesearch.SelectedValue) || ddlLocationStatesearch.SelectedValue=="0")
                objLocation.State = ddlLocationStatesearch.SelectedValue;
            if (!String.IsNullOrEmpty(txtLocationSearchZip.Text))
                objLocation.ZipCode = Convert.ToString(txtLocationSearchZip.Text);
            //else
            //    objLocation.ZipCode = 0;
            hdnLocationType.Value = rdbLocation.SelectedIndex.ToString();
            objLocation.IsShoppingCenter = Convert.ToInt32(hdnLocationType.Value);
            DataSet ds = new DataSet();
            ds = objLocation.GetListPageWise();
            
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ViewState["PageNumber"] = objLocation.CurrentPageNo;
                    //if (objLocation.TotalRecords > objLocation.PageSize)
                    double totalRecords=objLocation.TotalRecords;
                    ViewState["TotalRecords"] = objLocation.TotalRecords;
                    double pagesize = objLocation.PageSize;
                    ViewState["TotalPages"] = Math.Ceiling(totalRecords / pagesize);
                    //lblCount.Text = "Total Records:" + totalRecords.ToString();
                    grdShoppingCenter.DataSource = ds.Tables[0];
                    grdShoppingCenter.DataBind();
                    //this.PopulatePager(objLocation.TotalRecords, pageno);
                }
                else
                {
                    grdShoppingCenter.DataSource = null;
                    grdShoppingCenter.DataBind();
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/ShoppingCenter.aspx", "Bindgrid", ex.Message);
        }
    }
    public void Bindgrid()
    {
        try
        {
            int pageno = 1;

            Locations objLocation = new Locations();
            objLocation.CurrentPageNo = pageno;
            if (ViewState["PageNumber"] != null)
            { objLocation.CurrentPageNo = Convert.ToInt32(ViewState["PageNumber"].ToString()); }
            objLocation.PageSize = 25;
            if (!String.IsNullOrEmpty(txtLocationSearchName.Text))
                objLocation.ShoppingCenter = txtLocationSearchName.Text;
            if (!String.IsNullOrEmpty(txtLocationSearchCity.Text))
                objLocation.City = txtLocationSearchCity.Text;
            if (!String.IsNullOrEmpty(ddlLocationStatesearch.SelectedValue) || ddlLocationStatesearch.SelectedValue == "0")
                objLocation.State = ddlLocationStatesearch.SelectedValue;
            if (!String.IsNullOrEmpty(txtLocationSearchZip.Text))
                objLocation.ZipCode = Convert.ToString(txtLocationSearchZip.Text);
            //else
            //    objLocation.ZipCode = 0;
            hdnLocationType.Value = rdbLocation.SelectedIndex.ToString();
            objLocation.IsShoppingCenter = Convert.ToInt32(hdnLocationType.Value);
            DataSet ds = new DataSet();
            ds = objLocation.GetListPageWise();

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ViewState["PageNumber"] = objLocation.CurrentPageNo;
                    //if (objLocation.TotalRecords > objLocation.PageSize)
                    double totalRecords = objLocation.TotalRecords;
                    ViewState["TotalRecords"]= objLocation.TotalRecords;
                    double pagesize = objLocation.PageSize;
                    ViewState["TotalPages"] = Math.Ceiling(totalRecords / pagesize);
                   // lblCount.Text = "Total Records:"+ totalRecords.ToString();
                    grdShoppingCenter.DataSource = ds.Tables[0];
                    grdShoppingCenter.DataBind();
                    //this.PopulatePager(objLocation.TotalRecords, pageno);
                }
                else
                {
                    grdShoppingCenter.DataSource = null;
                    grdShoppingCenter.DataBind();
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/ShoppingCenter.aspx", "Bindgrid", ex.Message);
        }
    }
    //private void PopulatePager(int recordCount, int currentPage)
    //{

    //    double dblPageCount = (double)((decimal)recordCount / decimal.Parse("10"));
    //    int pageCount = (int)Math.Ceiling(dblPageCount);
    //    List<ListItem> pages = new List<ListItem>();
    //    if (pageCount > 0)
    //    {
    //        pages.Add(new ListItem("First", "1", currentPage > 1));
    //        for (int i = 1; i <= pageCount; i++)
    //        {
    //            pages.Add(new ListItem(i.ToString(), i.ToString(), i != currentPage));
    //        }
    //        pages.Add(new ListItem("Last", pageCount.ToString(), currentPage < pageCount));
    //    }
    //    //rptPager.DataSource = pages;
    //    //rptPager.DataBind();
    //}
    //protected void Page_Changed(object sender, EventArgs e)
    //{
    //    int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
    //    this.Bindgrid(pageIndex);
    //}
    protected void btnaddNewShoppingCenter_Click(object sender, EventArgs e)
    {
        lbTitle.Text = "ADD NEW SHOPPING CENTER";
        this.ucAddNewShoppingCenter.btnaddedit = "ADD";
        this.ucAddNewShoppingCenter.ResetControls();
        
        mpeAddNewShoppingCenter.Show();
    }
    protected void grdShoppingCenter_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //int PageNo = e.NewPageIndex + 1;
        //this.Bindgrid(PageNo);
        
        int PageNo = Convert.ToInt32(ViewState["PageNo"]);

        if (e.NewPageIndex == -1)
        {
            PageNo++;
            ViewState["PageNo"] = PageNo;
        }
        else if (e.NewPageIndex == -2)
        {
            PageNo--;
            ViewState["PageNo"] = PageNo;
        }
        else
        {
            PageNo = e.NewPageIndex + 1;
            ViewState["PageNo"] = PageNo;
        }
        this.Bindgrid(PageNo);
        
    }
    protected void grdShoppingCenter_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)

        {
            int pageno = Convert.ToInt32(ViewState["PageNumber"]);
            int totalpage = Convert.ToInt32(ViewState["TotalPages"]);
            int totalRecords = Convert.ToInt32(ViewState["TotalRecords"]); 
            LinkButton btnPrevious = new LinkButton();
            btnPrevious.ID = "btnPrevious";
            btnPrevious.Text = "<";
            btnPrevious.CommandName = "Page";
            btnPrevious.CommandArgument = "-1";
            btnPrevious.CssClass = "removeunderline";

            LinkButton btnNext = new LinkButton();
            btnNext.ID = "btnNext";
            btnNext.Text = ">";
            btnNext.CommandName = "Page";
            btnNext.CssClass = "removeunderline";
            btnNext.CommandArgument = "0";
            e.Row.Cells[0].Controls.Add(btnPrevious);


            for (int i = 1; i <= totalpage; i++)
            {
                LinkButton lnk = new LinkButton();
                lnk.ID = "lnk" + i;
                lnk.Text = i.ToString() + " ";
                lnk.CommandArgument = i.ToString();
                lnk.CommandName = "Page";
                e.Row.Cells[0].Controls.Add(lnk);
                lnk.CssClass = "removeunderline";
                if (Convert.ToInt32(ViewState["PageNo"]) == i)
                {
                    lnk.CssClass = "linkbut";
                }
            }
            e.Row.Cells[0].Controls.Add(btnNext);
            //lblPages.Text = "Page No:" + pageno + " of " + totalpage + " Pages";
            Label lblpg = new Label();
            lblpg.Text = "Total Records:" + totalRecords.ToString();
            lblpg.ID = "lblpage";

            lblpg.CssClass = "PageNoCSS";
            e.Row.Cells[0].Controls.Add(lblpg);

            if (Convert.ToInt32(ViewState["TotalPages"]) == 1)
            {
                btnNext.Enabled = false;
                btnPrevious.Enabled = false;
            }
            else if (Convert.ToInt32(ViewState["TotalPages"]) == Convert.ToInt32(ViewState["PageNo"]))
            {
                btnNext.Enabled = false;
                btnPrevious.Enabled = true;
            }
            else if (Convert.ToInt32(ViewState["PageNo"]) == 1)
            {
                btnNext.Enabled = true;
                btnPrevious.Enabled = false;
            }
            else
            {
                btnPrevious.Enabled = true;
                btnNext.Enabled = true;
            }
           
        }
    }
    protected void grdShoppingCenter_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            Locations objLocation = new Locations();
            //if(objLocation.IsShoppingCenter==0)hdnLocationType.Value
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (hdnLocationType.Value == "0")
                    e.Row.Cells[2].Visible = false;
                else
                    e.Row.Cells[1].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (hdnLocationType.Value == "0")
                    e.Row.Cells[2].Visible = false;
                else
                    e.Row.Cells[1].Visible = false;
                HiddenField hdnLocationId = (HiddenField)e.Row.FindControl("hdnLocationId") as HiddenField;
                objLocation.LocationID = Convert.ToInt32(hdnLocationId.Value);
                HiddenField hdnCanDel = (HiddenField)e.Row.FindControl("hdnCanDel") as HiddenField;
                int candel = Convert.ToInt32(hdnCanDel.Value);
                if (candel == 0)
                {
                    ImageButton imgDelete = (ImageButton)e.Row.FindControl("delRecord") as ImageButton;
                    imgDelete.Enabled = false;
                    imgDelete.Width = 16;
                    imgDelete.Height = 16;
                    imgDelete.ImageUrl = "~/Images/deletegray.png";
                }
            }

        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/ShoppingCenter.aspx", "grdShoppingCenter_RowDataBound", ex.Message);
        }
       
    }
    protected void btnSearchlocation_Click(object sender, EventArgs e)
    {
        Bindgrid(1);
    }
    protected void grdShoppingCenter_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Ed")
        {
            //if (hdnLocationType.Value == "0")
            //{
            //}
            //else
            //{
 
            //}
            Locations objLocation = new Locations();
            objLocation.LocationID = Convert.ToInt32(e.CommandArgument);
            bool load = objLocation.Load();

            if (string.IsNullOrEmpty(objLocation.ShoppingCenter))
            {
                this.ucAddNewShoppingCenter.LocationType = 1;
                lbTitle.Text = "EDIT SHOPPING CENTER";
            }
            else
                this.ucAddNewShoppingCenter.LocationType = 0;
            lbTitle.Text = "EDIT SHOPPING CENTER";
            if (load)
            {
                this.ucAddNewShoppingCenter.btnaddedit = "UPDATE";
                
                //txtDealType.Text = objDealTypes.DealType;
                ViewState["LocationID"] = objLocation.LocationID;
                this.ucAddNewShoppingCenter.LocationId = objLocation.LocationID.ToString();
                this.ucAddNewShoppingCenter.Name = objLocation.ShoppingCenter;
                 this.ucAddNewShoppingCenter.Address1 = objLocation.Address1;
                 this.ucAddNewShoppingCenter.Address2 = objLocation.Address2;
                 this.ucAddNewShoppingCenter.City = objLocation.City;
                 this.ucAddNewShoppingCenter.County = objLocation.Country;
                 this.ucAddNewShoppingCenter.State = objLocation.State;
                 this.ucAddNewShoppingCenter.ZipCode = objLocation.ZipCode.ToString();
                 this.ucAddNewShoppingCenter.Latitude = objLocation.Latitude.ToString();
                 this.ucAddNewShoppingCenter.Longitute = objLocation.Longitude.ToString();
                 this.ucAddNewShoppingCenter.Isactive = objLocation.IsActive;
                 //this.ucAddNewShoppingCenter.= objLocation.Address1;
                //btnAddNew.Text = "UPDATE";
                mpeAddNewShoppingCenter.Show();

            }
        }
        if (e.CommandName == "Del")
        {

            Locations objLocation = new Locations();
            objLocation.LocationID = Convert.ToInt32(e.CommandArgument);

            bool load = objLocation.CheckBeforeDelete();
           // if(!load)
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Location", "alert('Error : Location cannot be deleted,Dependent Charge slips are present');", true);
            Bindgrid(Convert.ToInt32(ViewState["PageNumber"].ToString()));
           
        }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SetActive(string locationid)
    {
        Locations objLocation = new Locations();
        objLocation.LocationID = Convert.ToInt32(locationid);
        bool load = objLocation.Load();
        if (load)
        {
            if (objLocation.IsActive)
            {
                objLocation.IsActive = false;
            }
            else
            {
                objLocation.IsActive = true;
            }
            objLocation.Save();

        }
    }
    //protected void btnEdit_Click(object sender, ImageClickEventArgs e)
    //{
    //    try
    //    {
    //       // string strlocationid=(sender as ImageButton).CommandArgument;
    //       // int locatonid = Convert.ToInt32(strlocationid);
    //       // Locations objLocation = new Locations();
    //       // objLocation.LocationID = locatonid;
    //       // objLocation.Load();
    //       // lbTitle.Text = "EDIT LOCATION";
    //       //// ucAddNewShoppingCenter.ResetControls();
    //       // mpeAddNewShoppingCenter.Show();
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorLog.WriteLog("Admin/ShoppingCenter.aspx", "Bindgrid", ex.Message);
    //    }
    //}
    private void GetLocationDetails(int locationid)
    {
        Locations objLocation = new Locations();
        objLocation.LocationID = locationid;

    }
    protected void rdbLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["PageNo"] = 1;
        int rad = rdbLocation.SelectedIndex;
        hdnLocationType.Value = rad.ToString();
        Bindgrid();
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        Bindgrid(1);
    }
}