﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/SettingMaster.master" CodeFile="ShoppingCenter.aspx.cs" Inherits="Admin_ShoppingCenter" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<%@ Register Src="~/UserControl/ucAddNewShoppingCenter.ascx" TagName="AddNewShoppingCenter" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <script type="text/ecmascript">
        function ClearControl()
        {
           
            $('#<%=txtLocationSearchName.ClientID %>').val("");
            $('#<%=txtLocationSearchCity.ClientID%>').val("");
            $('#<%=txtLocationSearchZip.ClientID%>').val("");
            document.getElementById('<%=ddlLocationStatesearch.ClientID %>').selectedIndex = -1 
           
        }
        function Validate(event) {
            var regex = new RegExp("^[0-9 \-]+$");
            var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        }
    </script>
    <style type="text/css">
        .removeunderline {
            text-decoration: none;
        }
        .Csspages {
        float:right;
        padding-right:4px;
        }
    </style>
    <style>
        .radio-toolbar input[type="radio"] {
            display: none;
        }

        .radio-toolbar input[type="radio"] {
            display: none;
        }

        .radio-toolbar label {
            display: inline-block;
            background-color: #ddd;
            padding: 4px 11px;
            font-family: Arial;
            font-size: 15px;
            font-weight: 500;
        }

        .radio-toolbar input[type="radio"]:checked + label {
            background-color: #bbb;
        }
    </style>
    <script type="text/javascript">
        function SetActive(locationid) {
            //alert(locationid );
            $.ajax({
                type: "POST",
                url: "ShoppingCenter.aspx/SetActive",
                data: '{locationid: "' + locationid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                failure: function (response) {
                    alert(response);
                }
            });
            return false;
        }
        function toggleLocation() {
            debugger;
            var rblSelectedValue = $("#<%= rdbLocation.ClientID %> input:checked");
        if (rblSelectedValue.val() == "0") {//shopping center=0
            document.getElementById('<%=hdnLocationType.ClientID %>').value = "0";
        }
        else if(rblSelectedValue.val() == "1") {//not a shopping center=1
            document.getElementById('<%=hdnLocationType.ClientID %>').value = "1";
        }
           
          }
      

    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upNewCharge" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
    <table width="100%">

        <tr>
            <td  colspan="2">
                <br />
            </td>

        </tr>
        <tr>
            <td  colspan="2">
                <table>
                    <tr>
                        <td>Shopping Center:</td>
                        <td>
                            <asp:TextBox ID="txtLocationSearchName" runat="server"></asp:TextBox></td>

                        <td>State:</td>
                        <td>
                            <asp:DropDownList ID="ddlLocationStatesearch" runat="server">
                            </asp:DropDownList>
                            <%-- <asp:TextBox ID="txtLocationSearchState" runat="server"></asp:TextBox>--%>
                        </td>
                        <td>
                            <asp:Button ID="btnSearchlocation" CssClass="SqureButton" Text="SEARCH" OnClick="btnSearchlocation_Click" runat="server" />
                            <asp:Button ID="btnclear" runat ="server" Text="RESET" CssClass="SqureButton" OnClientClick  ="ClearControl();" OnClick="btnclear_Click"/>
                         </td>
                    </tr>
                    <tr>
                        <td>City:</td>
                        <td>
                            <asp:TextBox ID="txtLocationSearchCity" runat="server"></asp:TextBox>
                        </td>
                        <td>ZipCode: </td>
                        <td>
                            <asp:TextBox ID="txtLocationSearchZip" runat="server" onkeypress="return Validate(event);"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
        <tr>
            <td  colspan="2">
                <br />
            </td>

        </tr>
        <tr  colspan="2">
            <td>
                <table width="100%">
                    <tr>
                        <td align="right">
                             <ajax:ModalPopupExtender ID="mpeAddNewShoppingCenter" TargetControlID="lbDummybtn" CancelControlID="btnCancel"
                                PopupControlID="divNewLocation" runat="server" BackgroundCssClass="modalBackground">
                            </ajax:ModalPopupExtender>
                            <asp:Button ID="btnaddNewShoppingCenter" runat="server" Text="ADD NEW LOCATION" CssClass="SqureButton" OnClick="btnaddNewShoppingCenter_Click" />
                            <asp:Label ID="lbDummybtn" runat="server"></asp:Label>
                        </td>
                        </tr>
                    
                </table>
            </td>

        </tr>
        <tr  colspan="2">
            <td class="radio-toolbar">
                <asp:RadioButtonList ID="rdbLocation" runat="server"  RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rdbLocation_SelectedIndexChanged">
                    <asp:ListItem Text="Shopping Center" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="No Shopping Center" Value="1"></asp:ListItem>
                </asp:RadioButtonList></td>
        </tr>
        <%--<tr>
                        <td><asp:Label ID="lblCount"  Font-Bold="true" runat="server"></asp:Label></td>
                        <td align="right">
                          
                             <asp:Label ID="lblPages" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>--%>
        <%-- '<%# string.Format("return SetActive({0},\"{1}\")", Eval("LocationID"), Eval("IsActive")) %>'--%>
        <tr>
            <td  colspan="2"><%-- // <asp:HiddenField ID="hdnActive" runat ="server" Value='<% #Bind("IsActive") %>'/>'<%# javascript: SetActive(Eval("City"), Eval("State"))  %>'--%>
                <asp:HiddenField ID="hdnLocationType" runat ="server" />
                <asp:GridView ID="grdShoppingCenter" runat="server" Width="100%"
                    OnPageIndexChanging="grdShoppingCenter_PageIndexChanging"
                    OnRowCreated="grdShoppingCenter_RowCreated"
                    OnRowDataBound="grdShoppingCenter_RowDataBound"
                    OnRowCommand="grdShoppingCenter_RowCommand"
                    AutoGenerateColumns="false"
                    HeaderStyle-CssClass="HeaderGridView"
                    FooterStyle-CssClass="FooterGridView"
                    AllowPaging="true" PageSize="25"
                    RowStyle-CssClass="RowGridView" AlternatingRowStyle-CssClass="AlternatingRowGridView2">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="ACTIVE" ItemStyle-Width="20px">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkActive" runat="server" Height="15px" Width="15px" CommandName="IsActive" Checked='<%# Eval("IsActive") %>' CommandArgument='<%# Eval("LocationID") %>' onchange='<%# Eval("LocationID","return SetActive({0})") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ShoppingCenter" HeaderText="Shopping Center" />
                        <asp:BoundField DataField="Address1" HeaderText="Address 1" />
                        <%--   <asp:BoundField DataField="Address2" HeaderText="Address 2" />--%>
                        <asp:BoundField DataField="City" HeaderText="City" />
                        <asp:BoundField DataField="StateName" HeaderText="State" />
                        <asp:BoundField DataField="ZipCode" HeaderText="ZipCode" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnLocationId" runat="server" Value='<%# Eval("LocationID") %>' />
                                <asp:ImageButton runat="server" ImageUrl="~/Images/edit.jpg" ID="btnEdit" CommandName="Ed" CommandArgument='<%# Eval("LocationID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnCanDel" runat="server" Value='<%# Eval("CanDelete") %>' />
                                <asp:ImageButton runat="server" ImageUrl="~/Images/delete.gif" OnClientClick='javascript:return confirm("Do You Want To Delete This Record");' ID="delRecord" CommandName="Del" CommandArgument='<%# Eval("LocationID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerTemplate>
                    </PagerTemplate>
                    <PagerStyle CssClass="pagerStyle" />
                </asp:GridView>
                <%-- <asp:Repeater ID="rptPager" runat="server">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>' Enabled='<%# Eval("Enabled") %>' OnClick="Page_Changed"></asp:LinkButton>
                    </ItemTemplate>
                </asp:Repeater>--%>
            </td>
        </tr>
    </table>
    <div id="divNewLocation" class="PopupDivBodyStyle" style="height: auto; width: 550px; display: none;">
        <%--class="spacing"--%>
        <table style="width: 100%;" cellspacing="0" cellpadding="0">
            <tr style="border-bottom: solid; background-color: gray;">
                <td style="text-align: left;">
                    <asp:UpdatePanel ID="upTitile" runat="server">
                        <ContentTemplate>
                            <h3>&nbsp; &nbsp;      
                                <asp:Label ID="lbTitle" runat="server"></asp:Label></h3>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="right"><%--OnClientClick="javascript:window.close();return false;" --%>
                    <asp:ImageButton ImageUrl="~/Images/close.png" ID="btnCancel" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc:AddNewShoppingCenter ID="ucAddNewShoppingCenter" runat="server" />

                </td>
            </tr>
        </table>

    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" runat="Server">
</asp:Content>

