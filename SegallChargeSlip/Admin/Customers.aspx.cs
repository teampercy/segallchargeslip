﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;
using System.Configuration;

public partial class Admin_Customers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["IsVendor"] = "0";        
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void GetChargeSlipDetails(string RowIndx)
    {
        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {
                int ChargeSlipID = Convert.ToInt32(HttpContext.Current.Session["ChargeSlipId"]);
                Utility.ClearSessionExceptUser();
                ChargeSlip objChargeSlip = new ChargeSlip();
                objChargeSlip.ChargeSlipID = ChargeSlipID;
                objChargeSlip.Load();

                DealTransactions objDealTransactions = new DealTransactions();
                objDealTransactions.ChargeSlipID = ChargeSlipID;
                objDealTransactions.LoadByChargeSlipId();


                objChargeSlip.DealTransactionsProperty = objDealTransactions;
                HttpContext.Current.Session[GlobleData.NewChargeSlipObj] = objChargeSlip;
                HttpContext.Current.Session[GlobleData.NewChargeSlipId] = ChargeSlipID;
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Customer.aspx", "GetChargeSlipDetails", ex.Message);
        }
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetChargeSlipFile()
    {
        string reportName = string.Empty;
        string reportNameEdited = string.Empty;

        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {
                int ChargeSlipID = Convert.ToInt32(HttpContext.Current.Session["ChargeSlipId"]);
                ChargeSlip objChargeSlip = new ChargeSlip();
                objChargeSlip.ChargeSlipID = ChargeSlipID;
                objChargeSlip.Load();
                reportName = objChargeSlip.Report;
                reportNameEdited = objChargeSlip.ReportEdited;

                //We have file name, now map server path
                string filePath = ConfigurationManager.AppSettings["ReprintReportPath"].ToString() + reportNameEdited;
                System.Net.WebRequest webRequest = System.Net.WebRequest.Create(new Uri(filePath));
                webRequest.Method = "HEAD";
                using(System.Net.WebResponse webResponse = webRequest.GetResponse())
                {
                    return filePath;
                }
            }
            return reportNameEdited;
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Customer.aspx", "GetChargeSlipFile", ex.Message);
            return "";
        }
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SaveBrokerCheckNo(string PaymentHistoryId, string CheckNo)
    {
        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {
                //if ((!String.IsNullOrEmpty(PaymentHistoryId)) && (!String.IsNullOrEmpty(CheckNo)))
                if ((!String.IsNullOrEmpty(PaymentHistoryId)))
                {
                    int PHID = Convert.ToInt32(PaymentHistoryId.Trim());
                    DealTransactions objDealTransactions = new DealTransactions();
                    objDealTransactions.UpdateCheckNo(PHID, CheckNo);
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Customer.aspx", "SaveBrokerCheckNo", ex.Message);
        }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string SavePaymentAmount(string Paymentd, string Amount)
    {
        string str = "";
        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {
                //if ((!String.IsNullOrEmpty(PaymentHistoryId)) && (!String.IsNullOrEmpty(CheckNo)))
                if ((!String.IsNullOrEmpty(Paymentd)))
                {
                    int PHID = Convert.ToInt32(Paymentd.Trim());
                    decimal PaymentAmount = Convert.ToDecimal(Amount);
                    Payments obj = new Payments();
                    if (HttpContext.Current.Session[GlobleData.User] != null)
                    {
                        DataSet ds = new DataSet();
                        ds = obj.UpdatePaymentAmount(PHID, PaymentAmount, Convert.ToInt32(HttpContext.Current.Session[GlobleData.User].ToString()));
                        if (ds != null)
                        {
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {

                                    str = ds.Tables[0].Rows[0]["OldAmount"].ToString();
                                    return str;
                                }
                            }
                            else
                            {

                                return str;
                            }
                        }
                        else
                        {

                            return str;
                        }
                    }
                }
            }
            return str;
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Customer.aspx", "SavePaymentAmount", ex.Message);
        }
        return str;
    }
    protected void btnRefreshPayment_Click(object sender, EventArgs e)
    {
        if (hdnChargeslipId.Value != "0")
        {
            ucPaymentHistoryDetail ucbPay = (ucPaymentHistoryDetail)ucBilledTab1.FindControl("ucPaymentHistoryDetail1");
            if (ucbPay != null)
            {
                ucbPay.ChargeSlipId = Convert.ToInt32(hdnChargeslipId.Value.ToString());
                ucbPay.Bindata();

                if (ucBilledTab1.FindControl("hdnPopup") != null)
                {
                    HiddenField hdn = (HiddenField)ucBilledTab1.FindControl("hdnPopup");
                    hdn.Value = "0";
                }
            }
        }
       
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string SaveIndividualBrokerPaymentEdit(string PaymentHistoryId, string ChargeslipId, string NewAmount, string ReasonToEdit)
    {
        string str = "";
        try
        {
            if (ChargeslipId != "")
            {
                //if ((!String.IsNullOrEmpty(PaymentHistoryId)) && (!String.IsNullOrEmpty(CheckNo)))
                if ((!String.IsNullOrEmpty(PaymentHistoryId)))
                {
                    DataSet ds = new DataSet();
                    int PHID = Convert.ToInt32(PaymentHistoryId.Trim());
                    decimal PaymentAmount = Convert.ToDecimal(NewAmount);
                    Payments obj = new Payments();
                    ds=obj.SaveIndividualBrokerPaymentEdit(PHID, PaymentAmount, Convert.ToInt32(HttpContext.Current.Session[GlobleData.User].ToString()),Convert.ToInt32(ChargeslipId), ReasonToEdit);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {

                                str = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
                                return str;
                            }
                        }
                        else
                        {

                            return str;
                        }
                    }
                    else
                    {

                        return str;
                    }
                }
            }
            
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Customer.aspx", "SaveIndividualBrokerPaymentEdit", ex.Message);
        }
        return str;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetIndividualBrokerPaymentMessage(string PaymentHistoryId)
    {
        string str = "";
        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {
                //if ((!String.IsNullOrEmpty(PaymentHistoryId)) && (!String.IsNullOrEmpty(CheckNo)))
                if ((!String.IsNullOrEmpty(PaymentHistoryId)))
                {
                    int PHID = Convert.ToInt32(PaymentHistoryId.Trim());
                    
                    Payments obj = new Payments();
                    if (HttpContext.Current.Session[GlobleData.User] != null)
                    {
                        DataSet ds = new DataSet();
                        ds = obj.GetIndividualBrokerPaymentMessage(PHID);
                        if (ds != null)
                        {
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {

                                    str = ds.Tables[0].Rows[0]["FieldChange"].ToString();
                                    return str;
                                }
                            }
                            else
                            {

                                return str;
                            }
                        }
                        else
                        {

                            return str;
                        }
                    }
                }
            }
            return str;
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Customer.aspx", "SavePaymentAmount", ex.Message);
        }
        return str;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string UpdateCreditAmount(string CommissionDueId, string OutstandingAmount)
    {
        string str = "";
        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {
                if ((!String.IsNullOrEmpty(CommissionDueId)) && (!String.IsNullOrEmpty(OutstandingAmount)))
                {
                    int CommissionDueDateId = Convert.ToInt32(CommissionDueId.Trim());
                    decimal OutstandingAmounts = Convert.ToDecimal(OutstandingAmount);
                    Payments obj = new Payments();
                    obj.UpdateCreditAmount(CommissionDueDateId, OutstandingAmounts, Convert.ToInt32(HttpContext.Current.Session[GlobleData.User].ToString()));
                    str = "1";
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Customer.aspx", "UpdateCreditAmount", ex.Message);
        }
        return str;
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string GetPaymentMessage(string MessageId)
    {
        string str = "";
        try
        {
            if (HttpContext.Current.Session["ChargeSlipId"] != null)
            {
                //if ((!String.IsNullOrEmpty(PaymentHistoryId)) && (!String.IsNullOrEmpty(CheckNo)))
                int MessgID = 0;
                if ((!String.IsNullOrEmpty(MessageId)))
                {
                    MessgID = Convert.ToInt32(MessageId.Trim());
                }
                   
                    Payments obj = new Payments();
                    if (HttpContext.Current.Session[GlobleData.User] != null)
                    {
                        DataSet ds = new DataSet();
                        ds = obj.GetPaymentMessage(MessgID);
                        if (ds != null)
                        {
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {

                                    str = ds.Tables[0].Rows[0]["MessageText"].ToString();
                                    return str;
                                }
                            }
                            else
                            {

                                return str;
                            }
                        }
                        else
                        {

                            return str;
                        }
                    }
                
            }
            return str;
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Customer.aspx", "GetPaymentMessage", ex.Message);
        }
        return str;
    }

}