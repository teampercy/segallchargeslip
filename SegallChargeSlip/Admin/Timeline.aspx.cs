﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;

public partial class Admin_Timeline : System.Web.UI.Page
{
    static SnapShot objSnapshot = new SnapShot();

    protected void Page_Load(object sender, EventArgs e)
    {
        ucTimeline.isAdmin = true;
        if (!IsPostBack)
        {

        }
    }

    [WebMethod]
    public static List<SnapShot.DashboardTimelineChart> GetTimelineChartData(int UserId, int Year,string Duration)
    {
        //objSnapshot.UserId=Convert.ToInt32(HttpContext.Current.Session[GlobleData.User]);
        return objSnapshot.GetTimelineChartData(UserId, Year,Duration);
    }

    [WebMethod]
    public static List<SnapShot.TimelineChartDataChargeslip> GetTimelineChartChargeSlip(int UserId, int Year,string Duration)
    {
        return objSnapshot.GetTimelineChartChargeSlip(UserId, Year,Duration);
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void GetChargeSlipDetails(int ChargeSlipId)
    {
        try
        {            
            Utility.ClearSessionExceptUser();
            ChargeSlip objChargeSlip = new ChargeSlip();
            objChargeSlip.ChargeSlipID = ChargeSlipId;
            objChargeSlip.Load();

            DealTransactions objDealTransactions = new DealTransactions();
            objDealTransactions.ChargeSlipID = ChargeSlipId;
            objDealTransactions.LoadByChargeSlipId();

            objChargeSlip.DealTransactionsProperty = objDealTransactions;
            HttpContext.Current.Session[GlobleData.NewChargeSlipObj] = objChargeSlip;
            HttpContext.Current.Session[GlobleData.NewChargeSlipId] = ChargeSlipId;

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Timeline.aspx", "GetChargeSlipDetails", ex.Message);
        }
    }
}