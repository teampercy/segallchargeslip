﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_ChargeTo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            ViewState["PageSize"] = 20;
            ViewState["PageNo"] = 1;
            Utility.FillStates(ref ddlStatesearch);
            Bindgrid();
        }
        // Bindgrid();
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        GridViewRow grdChargeToPager = grdChargeTo.BottomPagerRow;
        if (grdChargeToPager != null)
        {
            grdChargeToPager.Visible = true;
        }
    }
    public void Bindgrid()
    {
        try
        {
            Company objCompany = new Company();
            objCompany.CompanyName = txtCompany.Text.Trim();
            objCompany.City = txtCity.Text.Trim();
            objCompany.State = ddlStatesearch.SelectedValue;
            if (!string.IsNullOrEmpty(txtZipCode.Text.Trim()))
            {
                objCompany.ZipCode = Convert.ToString(txtZipCode.Text.Trim());
            }
            DataSet ds = new DataSet();
            ds = objCompany.GetList(Convert.ToInt32(ViewState["PageNo"].ToString()), Convert.ToInt32(ViewState["PageSize"].ToString()));
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    double TotalRecord = Convert.ToInt32(ds.Tables[0].Rows[0]["RowCounts"]);
                    ViewState["TotalRecord"] = TotalRecord;
                    double Pagesize = Convert.ToInt32(ViewState["PageSize"].ToString());
                    double TotalPage = Math.Ceiling(TotalRecord / Pagesize);
                    ViewState["TotalPage"] = TotalPage;
                    //if (TotalRecord > 0)
                    //{
                    //    lblTotalRecord.Text = "Total Records: " + TotalRecord;
                    //}
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    grdChargeTo.DataSource = ds.Tables[1];
                    grdChargeTo.DataBind();

                }
                else
                {
                    grdChargeTo.DataSource = null;
                    grdChargeTo.DataBind();
                   // lblTotalRecord.Text = string.Empty;
                }


            }
            else
            {
              //  lblTotalRecord.Text = string.Empty;
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/ChargeTo.aspx", "Bindgrid", ex.Message);
        }
    }
    protected void btnChargeTo_Click(object sender, EventArgs e)
    {
        try
        {
            lbTitle.Text = "ADD NEW COMPANY";
            ucNewCenterOrCompany.Name = "COMPANY";
            ucNewCenterOrCompany.ClassRef = "C";
            ucNewCenterOrCompany.PopupType = "C";
            //ucNewCenterOrCompany.CenterName = txtComapny.Text;
            ucNewCenterOrCompany.IsAddressOnly = false; // - SK 07/23
            ucNewCenterOrCompany.IsUpdate = true;
            ucNewCenterOrCompany.SetAdmin(true);
            ucNewCenterOrCompany.IsAdmin = true;
            ucNewCenterOrCompany.ResetControls();

            //hdnIsCompanySaved.Value = "0";
            Company objCompany = new Company();
            //if (Session["CompanyId"] != null)
            {

                //objCompany.CompanyID = Convert.ToInt32(hdnCompId.Value);
                // objCompany.CompanyID = Convert.ToInt32(e.CommandArgument);
                // objCompany.Load();

                ucNewCenterOrCompany.SetControlPropertiesCompanyFromObject(objCompany);

            }
            ((UpdatePanel)ucNewCenterOrCompany.FindControl("upAddNewShoppingCenterOrCompany")).Update();
            //IsNextClicked = false; // ------ SK : 08/07

            mpeAddNewComapny.Show();
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/ChargeTo.aspx", "btnChargeTo_Click", ex.Message);
        }
    }
    protected void grdChargeTo_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Del")
            {
                Company objComp = new Company();
                objComp.CompanyID = Convert.ToInt32(e.CommandArgument);
                bool load = objComp.Delete();
                Bindgrid();
            }
            if (e.CommandName == "Ed")
            {

                lbTitle.Text = "ADD NEW COMPANY";
                ucNewCenterOrCompany.Name = "COMPANY";
                ucNewCenterOrCompany.ClassRef = "C";
                ucNewCenterOrCompany.PopupType = "C";
                //ucNewCenterOrCompany.CenterName = txtComapny.Text;
                ucNewCenterOrCompany.IsAddressOnly = false; // - SK 07/23
                ucNewCenterOrCompany.IsUpdate = true;
                ucNewCenterOrCompany.SetAdmin(true);
                ucNewCenterOrCompany.IsAdmin = true;
                ucNewCenterOrCompany.ResetControls();

                //hdnIsCompanySaved.Value = "0";
                Company objCompany = new Company();
                //if (Session["CompanyId"] != null)
                {

                    //objCompany.CompanyID = Convert.ToInt32(hdnCompId.Value);
                    objCompany.CompanyID = Convert.ToInt32(e.CommandArgument);
                    objCompany.Load();
                   
                    ucNewCenterOrCompany.SetControlPropertiesCompanyFromObject(objCompany);

                }
                ((UpdatePanel)ucNewCenterOrCompany.FindControl("upAddNewShoppingCenterOrCompany")).Update();
                //IsNextClicked = false; // ------ SK : 08/07

                mpeAddNewComapny.Show();

            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ChargeTo.aspx", "grdChargeTo_RowCommand", ex.Message);
        }
    }
    protected void grdChargeTo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
           // Company objCompany = new Company();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnCompanyID = (HiddenField)e.Row.FindControl("hdnCompanyID") as HiddenField;
               // objCompany.CompanyID = Convert.ToInt32(hdnCompanyID.Value);
                CheckBox chkActive = (CheckBox)e.Row.FindControl("chkActive");
                HiddenField hdnActive = (HiddenField)e.Row.FindControl("hdnActive") as HiddenField;
                HiddenField hdnCanDel = (HiddenField)e.Row.FindControl("hdnCanDel") as HiddenField;
                int candel = Convert.ToInt32(hdnCanDel.Value);
                if (candel == 0)
                {
                    ImageButton imgDelete = (ImageButton)e.Row.FindControl("delRecord") as ImageButton;
                    imgDelete.Enabled = false;
                    imgDelete.Width = 16;
                    imgDelete.Height = 16;
                    imgDelete.ImageUrl = "~/Images/deletegray.png";
                }
                //DataTable dt = objCompany.CompanyExists();
                //if (dt != null)
                //{
                //    if (dt.Rows.Count > 0)
                //    {
                //        ImageButton imgDelete = (ImageButton)e.Row.FindControl("delRecord") as ImageButton;
                //        imgDelete.Enabled = false;
                //        // imgDelete.Visible = false;
                //        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Update Deal Type", "alert('Deal Type cannot be delete because it has referenced.');", true);
                //    }
                //}
                if (hdnActive != null)
                {
                    if (hdnActive.Value == "True")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealType.aspx", "grdDealType_RowDataBound", ex.Message);
        }
    }
    protected void grdChargeTo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            //int PageNo;
            //PageNo = e.NewPageIndex + 1;
            //ViewState["PageNo"] = PageNo;
            //Bindgrid();

            int PageNo = Convert.ToInt32(ViewState["PageNo"]);

            if (e.NewPageIndex == -1)
            {
                PageNo++;
                ViewState["PageNo"] = PageNo;
            }
            else if (e.NewPageIndex == -2)
            {
                PageNo--;
                ViewState["PageNo"] = PageNo;
            }
            else
            {
                PageNo = e.NewPageIndex + 1;
                ViewState["PageNo"] = PageNo;
            }
            Bindgrid();
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ChargeTo.aspx", "grdChargeTo_PageIndexChanging", ex.Message);
        }
    }
    protected void grdChargeTo_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                int pageno = Convert.ToInt32(ViewState["PageNo"].ToString());
                double totalpage = Convert.ToDouble(ViewState["TotalPage"].ToString());
                LinkButton btnPrevious = new LinkButton();
                btnPrevious.ID = "btnPrevious";
                btnPrevious.Text = "<";
                btnPrevious.CommandName = "Page";
                btnPrevious.CommandArgument = "-1";
                btnPrevious.Width = Unit.Pixel(15);
                btnPrevious.CssClass = "removeunderline";
                LinkButton btnNext = new LinkButton();
                btnNext.ID = "btnNext";
                btnNext.Text = ">";
                btnNext.CommandName = "Page";
                btnNext.CssClass = "removeunderline";
                btnNext.CommandArgument = "0";
                e.Row.Cells[0].Controls.Add(btnPrevious);
                for (int i = 1; i <= totalpage; i++)
                {
                    LinkButton lnk = new LinkButton();
                    lnk.ID = "lnk" + i;
                    lnk.Text = i.ToString() + " ";
                    lnk.CommandArgument = i.ToString();
                    lnk.CommandName = "Page";
                    e.Row.Cells[0].Controls.Add(lnk);
                    lnk.CssClass = "removeunderline";
                    if (Convert.ToInt32(ViewState["PageNo"].ToString()) == i)
                    {
                        lnk.CssClass = "linkbut";
                    }
                }
                e.Row.Cells[0].Controls.Add(btnNext);
                if (ViewState["TotalRecord"] != null)
                {

                    Label lblTotalRecords = new Label();
                    lblTotalRecords.CssClass = "PageNoCSS";
                    lblTotalRecords.Text = "Total Records:" + ViewState["TotalRecord"].ToString();
                    e.Row.Cells[0].Controls.Add(lblTotalRecords);
                }
                if (Convert.ToInt32(ViewState["TotalPage"]) == 1)
                {
                    btnNext.Enabled = false;
                    btnPrevious.Enabled = false;
                }
                else if (Convert.ToInt32(ViewState["TotalPage"]) == Convert.ToInt32(ViewState["PageNo"]))
                {
                    btnNext.Enabled = false;
                    btnPrevious.Enabled = true;
                }
                else if (Convert.ToInt32(ViewState["PageNo"]) == 1)
                {
                    btnNext.Enabled = true;
                    btnPrevious.Enabled = false;
                }
                else
                {
                    btnPrevious.Enabled = true;
                    btnNext.Enabled = true;
                }
                //Label lblPages = new Label();
                //lblPages.CssClass = "PageNoCSS";
                //lblPages.Text = "Page No:" + pageno + " of " + totalpage + " Pages";
                //e.Row.Cells[0].Controls.Add(lblPages);
             
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("ChargeTo.aspx", "grdChargeTo_RowCreated", ex.Message);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            ViewState["PageNo"] = "1";
            Bindgrid();
            //Company obj = new Company();
            //obj.CompanyName = txtCompany.Text.Trim();
            //obj.City = txtCity.Text.Trim();
            //obj.State = ddlStatesearch.SelectedValue;
            //if (!string.IsNullOrEmpty(txtZipCode.Text.Trim()))
            //{
            //    obj.ZipCode = Convert.ToInt32(txtZipCode.Text.Trim());
            //}
            //DataSet ds = new DataSet();
            //ds = obj.GetList(Convert.ToInt32(ViewState["PageNo"].ToString()), Convert.ToInt32(ViewState["PageSize"].ToString()));
            //if (ds != null)
            //{
            //    if (ds.Tables[0].Rows.Count > 0)
            //    {
            //        double TotalRecord = Convert.ToInt32(ds.Tables[0].Rows[0]["RowCounts"]);
            //        double Pagesize = Convert.ToInt32(ViewState["PageSize"].ToString());
            //        double TotalPage = Math.Ceiling(TotalRecord / Pagesize);
            //        ViewState["TotalPage"] = TotalPage;
            //        lblTotalRecord.Text = "Total Records: " +TotalRecord.ToString();
            //    }
            //    if (ds.Tables[1].Rows.Count > 0)
            //    {
            //        grdChargeTo.DataSource = ds.Tables[1];
            //        grdChargeTo.DataBind();
                   
            //    }
            //    else
            //    {
            //        grdChargeTo.DataSource = null;
            //        grdChargeTo.DataBind();
            //        lblTotalRecord.Text = string.Empty;
            //    }
            //}
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/ChargeTo.aspx", "btnSearch_Click", ex.Message);
        }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SetActive(string ChargeToid)
    {
        try
        {
            Company objCompany = new Company();
            objCompany.CompanyID = Convert.ToInt32(ChargeToid);
            bool load = objCompany.Load();
            if (load)
            {
                if (objCompany.IsActive)
                {
                    objCompany.IsActive = false;
                }
                else
                {
                    objCompany.IsActive = true;
                }
                objCompany.Save();
                objCompany.MessagesInsert(Convert.ToInt64(HttpContext.Current.Session["UserId"].ToString()));

            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/ChargeTo.aspx", "SetActive", ex.Message);
        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        ViewState["PageNo"] = "1";
        Bindgrid();
    }
}