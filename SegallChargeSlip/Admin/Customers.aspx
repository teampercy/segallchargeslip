﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true" CodeFile="Customers.aspx.cs" Inherits="Admin_Customers" EnableEventValidation="false" %>

<%@ Register Src="~/UserControl/ucBilledTab.ascx" TagName="ucBilledTab" TagPrefix="uc1" %>

<%@ Register Src="~/UserControl/ucPaymentHistoryDetail.ascx" TagName="ucPaymentHistory" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--     <table style="width: 100%">
        <tr>
           
            <td style="width: 70%; padding-left: 20px;vertical-align:bottom ;">&nbsp &nbsp
                <h3 style="border-color: black; text-align: left; text-shadow: initial;">ADMINISTRATION</h3>
            </td>
              <td style="text-align: right; vertical-align:bottom ;padding-top: 0.5em; color:#F68620; border-color: black; text-align: right; text-shadow: initial;font-weight:600; font-size: 15px;">CUSTOMERS
            </td>
           

        </tr>
    </table>--%>
    <table style="width: 99%">
        <tr>
            <td style="padding-top: 0.5em; color: black; font-family: Verdana; border-color: black; text-align: left; text-shadow: initial; font-weight: 600; font-size: 18px;">ADMINISTRATION
                <asp:Label ID="lbErrorMsg" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="Small" /></td>
            <td style="padding-top: 0.5em; color: #F68620; border-color: black; text-align: right; text-shadow: initial; font-weight: 600; font-size: 20px;">CUSTOMERS
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr style="width: 100%" />
            </td>
        </tr>
    </table>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <script src="../Js/jquery-1.9.1.js" type="text/javascript"></script>
    
    <script type="text/javascript">
       
        function SaveIndividualPaymentEdit(PaymentHistoryId,ChargeslipId,NewAmount,ReasonToEdit)
        {
            $.ajax({
                async: true,
                type: "POST",
                url: "Customers.aspx/SaveIndividualBrokerPaymentEdit",
                data: '{PaymentHistoryId: "' + PaymentHistoryId + '",ChargeslipId: "' + ChargeslipId + '",NewAmount: "' + NewAmount + '",ReasonToEdit: "' + ReasonToEdit + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //alert('p');
                    if (response.d == "") {
                        debugger;
                        document.getElementById('<%=btnRefreshPayment.ClientID%>').click();

                        }
                        else {
                            
                        alert(response.d);
                        return false;

                        }
                },
                failure: function (response) {

                    alert(response.d);
                    return false;
                }
            });
            $("#<%=hdnChargeslipId.ClientID%>").val(ChargeslipId);
            
            //alert('hi');
        }
        function imgEditbtn_ClientClick(imgbtn) {
            try {
               
                var ttd = imgbtn.parentElement
                var childLength = ttd.childNodes.length;
                for (var i = 0; i < childLength; i++) {
                    if (ttd.childNodes[i].type == 'text') {
                        ttd.childNodes[i].setAttribute("Style", "display:inline-grid;width:120px;");
                        ttd.childNodes[i].value= ttd.childNodes[3].innerHTML
                    }
                    
                }
                imgbtn.setAttribute("Style", "display:none;");
                ttd.childNodes[5].setAttribute("Style", "display:none;");
                ttd.childNodes[7].setAttribute("Style", "display:inline-grid;width:13px;height:13px;");
                ttd.childNodes[9].setAttribute("Style", "display:inline-grid;width:12px;height:12px;");
                ttd.childNodes[3].setAttribute("Style", "display:none;");
               
            }
            catch (exce) {
            }
            return false;

        }
        function imgEditbtn_ClientBrokerAmountClick(imgbtn) {
            try {

                var ttd = imgbtn.parentElement
                var childLength = ttd.childNodes.length;
                for (var i = 0; i < childLength; i++) {
                    if (ttd.childNodes[i].type == 'text') {

                        ttd.childNodes[i].setAttribute("Style", "display:inline-grid;width:120px;");
                        ttd.childNodes[i].value = ttd.childNodes[3].innerHTML;
                    }

                }
                imgbtn.setAttribute("Style", "display:none;");
                ttd.childNodes[5].setAttribute("Style", "display:none;");
                ttd.childNodes[7].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[3].setAttribute("Style", "display:none;");

            }
            catch (exce) {
            }
            return false;

        }
        function imgSavebtn_ClientBrokerAmountClick(imgbtn) {
            try {
                debugger;
                var ttd = imgbtn.parentElement
                var childLength = ttd.childNodes.length;
                var textValue;
                for (var i = 0; i < childLength; i++) {
                    if (ttd.childNodes[i].type == 'text') {
                        textValue = ttd.childNodes[i].value;
                        ttd.childNodes[i].setAttribute("Style", "display:none;");
                        break;
                    }

                }

                var ttr = ttd.parentElement;


                var PaymentHistoryID = ttd.childNodes[9].innerHTML; // ttr.cells[1].textContent;
                var lblChargeslipid = ttd.childNodes[11].innerHTML;

                var hdnChargeslipId = document.getElementById('<%=hdnChargeslipId.ClientID%>');
                hdnChargeslipId.value = lblChargeslipid;
                imgbtn.setAttribute("Style", "display:none;");
                var OldValue = ttd.childNodes[3].innerHTML;
                ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[7].setAttribute("Style", "display:none;");

                ttd.childNodes[3].innerHTML = textValue;
                //$.ajax({
                //    type: "POST",
                //    url: "Customers.aspx/SavePaymentAmount",
                //    data: '{Paymentd: "' + PaymentHistoryID + '",Amount: "' + textValue + '" }',
                //    contentType: "application/json; charset=utf-8",
                //    dataType: "json",
                //    failure: function (response) {

                //        alert(response.d);
                //    }
                //});
                document.getElementById('<%=btnRefreshPayment.ClientID%>').click();
               
            }
            catch (ex) {
            }
            return false;
        }
        function imgEditbtn_ClientAmountClick(imgbtn) {
            try {

                var ttd = imgbtn.parentElement
                var childLength = ttd.childNodes.length;
                for (var i = 0; i < childLength; i++) {
                    if (ttd.childNodes[i].type == 'text') {

                        ttd.childNodes[i].setAttribute("Style", "display:inline-grid;width:120px;");
                        ttd.childNodes[i].value = ttd.childNodes[5].innerHTML;
                    }

                }
                imgbtn.setAttribute("Style", "display:none;");
                ttd.childNodes[7].setAttribute("Style", "display:none;");
                ttd.childNodes[9].setAttribute("Style", "display:inline-grid;width:13px;height:13px;");
                ttd.childNodes[5].setAttribute("Style", "display:none;");
                ttd.childNodes[3].setAttribute("Style", "display:none;");
                ttd.childNodes[11].setAttribute("Style", "display:inline-grid;width:12px;height:12px;");

                //ttd.childNodes[5].setAttribute("Style", "display:none;");
                //ttd.childNodes[7].setAttribute("Style", "display:inline-grid;");
                //ttd.childNodes[3].setAttribute("Style", "display:none;");

            }
            catch (exce) {
            }
            return false;

        }
        function imgSavebtn_ClientAmountClick(imgbtn) {
            try {
                
                debugger;
                var ttd = imgbtn.parentElement
                var childLength = ttd.childNodes.length;
                var textValue;
                for (var i = 0; i < childLength; i++) {
                    if (ttd.childNodes[i].type == 'text') {
                        textValue = ttd.childNodes[i].value;
                        ttd.childNodes[i].setAttribute("Style", "display:none;");
                        break;
                    }

                }

                var ttr = ttd.parentElement;


                var PaymentHistoryID = ttd.childNodes[13].innerHTML; // ttr.cells[1].textContent;
                var lblChargeslipid = ttd.childNodes[15].innerHTML;
               
                var hdnChargeslipId=document.getElementById('<%=hdnChargeslipId.ClientID%>');
                hdnChargeslipId.value = lblChargeslipid;
                imgbtn.setAttribute("Style", "display:none;");
                var OldValue = ttd.childNodes[3].innerHTML;
                ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[7].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[9].setAttribute("Style", "display:none;");
                ttd.childNodes[11].setAttribute("Style", "display:none;");



                //if (textValue.trim() != '') {
                ttd.childNodes[5].innerHTML = textValue;
               


                $.ajax({
                    async: true,
                    type: "POST",
                    url: "Customers.aspx/SavePaymentAmount",
                    data: '{Paymentd: "' + PaymentHistoryID + '",Amount: "' + textValue.replace(/\$/g, '') + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        //alert('p');
                        if (response.d == "") {
                            debugger;
                            document.getElementById('<%=btnRefreshPayment.ClientID%>').click();
                           
                        }
                        else {
                            ttd.childNodes[5].innerHTML=response.d;
                            alert('Payment Amount cannot be greater than Commission Due');
                            
                        }
                    },
                    failure: function (response) {

                        alert(response.d);
                        return false;
                    }
                });
                //}
                //else {
                //    //ttd.childNodes[3].innerHTML = OldValue;
                //    ttd.childNodes[3].innerHTML = textValue;
                //}
            }
            catch (ex) {
            }
            return false;
        }
        function imgCancelAmountClick(imgbtn)
        {
            var ttd = imgbtn.parentElement
            var childLength = ttd.childNodes.length;
            var textValue;
            for (var i = 0; i < childLength; i++) {
                if (ttd.childNodes[i].type == 'text') {
                    
                    ttd.childNodes[i].setAttribute("Style", "display:none;");
                    break;
                }

            }
            imgbtn.setAttribute("Style", "display:none;");
            var OldValue = ttd.childNodes[3].innerHTML;
            ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
            ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
            ttd.childNodes[7].setAttribute("Style", "display:inline-grid;");
            ttd.childNodes[9].setAttribute("Style", "display:none;");
            ttd.childNodes[11].setAttribute("Style", "display:none;");
            return false;
        }
        function imgCancelClick(imgbtn)
        {
            try {
                var ttd = imgbtn.parentElement
                var childLength = ttd.childNodes.length;
                var textValue;
                for (var i = 0; i < childLength; i++) {
                    if (ttd.childNodes[i].type == 'text') {
                        textValue = ttd.childNodes[i].value;
                        ttd.childNodes[i].setAttribute("Style", "display:none;");
                        break;
                    }

                }
                imgbtn.setAttribute("Style", "display:none;");
                ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
                ttd.childNodes[7].setAttribute("Style", "display:none;");
            }
            catch (ex) {
            }
            return false;
                
        }
        function imgSavebtn_ClientClick(imgbtn) {
            try {
                
                var ttd = imgbtn.parentElement
                var childLength = ttd.childNodes.length;
                var textValue;
                for (var i = 0; i < childLength; i++) {
                    if (ttd.childNodes[i].type == 'text') {
                        textValue = ttd.childNodes[i].value;
                        ttd.childNodes[i].setAttribute("Style", "display:none;");
                        break;
                    }

                }
               
                var ttr = ttd.parentElement;
                
                
                var PaymentHistoryID = ttd.childNodes[11].innerHTML; // ttr.cells[1].textContent;
               
                    imgbtn.setAttribute("Style", "display:none;");
                    var OldValue=ttd.childNodes[3].innerHTML;
                    ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
                    ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
                    ttd.childNodes[7].setAttribute("Style", "display:none;");
                    ttd.childNodes[9].setAttribute("Style", "display:none;");
                    
                    
                    
                    //if (textValue.trim() != '') {
                        ttd.childNodes[3].innerHTML = textValue;
                        $.ajax({
                            type: "POST",
                            url: "Customers.aspx/SaveBrokerCheckNo",
                            data: '{PaymentHistoryId: "' + PaymentHistoryID + '",CheckNo: "' + textValue + '" }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    //}
                    //else {
                    //    //ttd.childNodes[3].innerHTML = OldValue;
                    //    ttd.childNodes[3].innerHTML = textValue;
                    //}
            }
            catch (ex) {
            }
            return false;
        }
       </script>

    <script type="text/javascript">
       
        function Navigate() {

            var Index = 1;
            $.ajax({
                type: "POST",
                url: "Customers.aspx/GetChargeSlipDetails",
                data: '{RowIndx: "' + Index + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    window.open("../Pages/NewCharge.aspx", "_self");
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
         
            return false;
        }
        //function PaymentPDFOpen(id) {
        //    $.ajax({
        //        type: "POST",
        //        url: "Payments.aspx/PaymentPDFOpen",
        //        data: '{Paymentd: "' + id + '" }',
        //        contentType: "application/json; charset=utf-8",
        //        dataType: "json",
        //        success: function (response) {
        //            alert('hi');
        //            // window.open("../Pages/NewCharge.aspx", "_self");
        //        },
        //        failure: function (response) {
        //            alert(response.d);
        //        }
        //    });

        //    return false;
        //}
        function Reprint() {
            $.ajax({
                type: "POST",
                url: "Customers.aspx/GetChargeSlipFile",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    window.open(response.d, '_blank');
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
            return false;
        }
        function NavigateSplitCalculator() {

            var Index = 1;
            $.ajax({
                type: "POST",
                url: "Customers.aspx/GetChargeSlipDetails",
                data: '{RowIndx: "' + Index + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    window.open("../Pages/NewChargeCommissionSpitCalCulator.aspx", "_self");
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
          
            return false;
        }
    </script>
    <asp:HiddenField ID="hdnChargeslipId" runat ="server" Value="0" />
      <asp:Button ID="btnRefreshPayment" runat ="server" text="Save Payment Amount" OnClick="btnRefreshPayment_Click" Style="display:none;"/>
    <uc1:ucBilledTab ID="ucBilledTab1" runat="server"></uc1:ucBilledTab>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" runat="Server">
</asp:Content>

