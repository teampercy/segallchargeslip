﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SettingMaster.master" AutoEventWireup="true" CodeFile="AddorEditVendor.aspx.cs" Inherits="Admin_AddorEditVendor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <link href="../Styles/SiteTEST.css" rel="stylesheet" />
    <link href="../Styles/AutoCompleteStyle.css" rel="stylesheet" />
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script src="../Js/CommonValidations.js"></script>
     <script type="text/ecmascript">
         function ClearControl() {

             $('#<%=txtCompany.ClientID %>').val("");
              $('#<%=txtCity.ClientID%>').val("");
              $('#<%=txtZipCode.ClientID%>').val("");
              document.getElementById('<%=ddlStatesearch.ClientID %>').selectedIndex = -1

         }
         function ValidateNumber(event) {
             var regex = new RegExp("^[0-9 \-]+$");
             var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
             if (!regex.test(key)) {
                 event.preventDefault();
                 return false;
             }
         }
    </script>
    <script type="text/javascript">
        function validateZipLen(strzip) {
            var strziptrm = $.trim(strzip);
            if (strziptrm.length < 5 || strziptrm.length > 10) {
                return false;
            }
            return true;
        }
        function SetActive(ChargesToid) {
            //alert(locationid );
            $.ajax({
                type: "POST",
                url: "ChargeTo.aspx/SetActive",
                data: '{ChargeToid: "' + ChargesToid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                failure: function (response) {
                    alert(response);
                }
            });
            return false;
        }
        function Validate() {
            debugger;
            var txtFirstName = document.getElementById('<%= txtFirstName.ClientID %>');
            var txtLastName = document.getElementById('<%= txtLastName.ClientID %>');
            var txtCompanies = document.getElementById('<%= txtCompanies.ClientID %>');
            var txtAddress = document.getElementById('<%= txtAddress.ClientID %>');
            var txtCities = document.getElementById('<%= txtCities.ClientID %>');
            var txtZip = document.getElementById('<%= txtZip.ClientID %>');
            var txtEmail = document.getElementById('<%= txtEmail.ClientID %>');
            var lblErrorMsg = document.getElementById('<%= lblErrorMsg.ClientID %>');
            var ddlState = document.getElementById('<%= ddlState.ClientID %>');
            var drpBrokerType = document.getElementById('<%= drpBrokerType.ClientID %>');
            if ($.trim(txtFirstName.value) == "") {
                lblErrorMsg.innerHTML = "Please enter First name.";
                txtFirstName.focus();
                return false;
            }
            else if ($.trim(txtLastName.value) == "") {
                lblErrorMsg.innerHTML = "Please enter Last name.";
                txtLastName.focus();
                return false;
            }
            else if ($.trim(txtCompanies.value) == "") {
                lblErrorMsg.innerHTML = "Please enter Company.";
                txtCompanies.focus();
                return false;
            }
            else if ($.trim(txtAddress.value) == "") {
                lblErrorMsg.innerHTML = "Please enter Address.";
                txtAddress.focus();
                return false;
            }
            else if ($.trim(txtCities.value) == "") {
                lblErrorMsg.innerHTML = "Please enter City.";
                txtCities.focus();
                return false;
            }
            else if (document.getElementById('<%=ddlState.ClientID %>').selectedIndex == -1 || document.getElementById('<%=ddlState.ClientID %>').selectedIndex == 0) {
                lblErrorMsg.innerHTML = "Please select State.";
                ddlState.focus();
                return false;
            }
            else if ($.trim(txtZip.value) == "") {
                lblErrorMsg.innerHTML = "Please enter ZipCode.";
                txtZip.focus();
                return false;
            }
            else if ($.trim(txtZip.value) != "") {
               
                if (!validateZipLen($.trim(txtZip.value))) {
                    lblErrorMsg.innerHTML = "Please Enter between 5 and 10 digit Valid ZipCode.";

                    txtZip.focus();
                    return false;
                }
            }
            //else if (txtZip.value != "") {
            //    if (!validateZipLen($.trim(txtZip.value))) {
            //        lblErrorMsg.innerHTML  = "Please Enter between 5 and 10 digit Valid Zip Code.";
                
            //    txtZip.focus();
            //    return false;
            //}
            else if ($.trim(txtEmail.value) == "") {
                lblErrorMsg.innerHTML = "Please enter Email.";
                txtEmail.focus();
                return false;
            }
            else if (!IsValidEmailId(txtEmail)) {
                lblErrorMsg.innerHTML = "Please enter valid Email.";
                return false;
            }
            else if (document.getElementById('<%=drpBrokerType.ClientID %>').selectedIndex == -1 || document.getElementById('<%=drpBrokerType.ClientID %>').selectedIndex == 0) {
                    lblErrorMsg.innerHTML = "Please select BrokerType.";
                    drpBrokerType.focus();
                    return false;
                }
    return true;
}
    </script>
    <style type="text/css">
        .labelPadding {
            padding-left: 20px;
        }

        .TextboxWidth {
            width: 250px;
        }

        .removeunderline {
            text-decoration: none;
        }

        .linkbut {
            color: black;
            font-weight: bold;
            text-decoration: none;
        }

        .lnkMsg {
            color: black;
            text-decoration: none;
        }

        .auto-style1 {
            width: 25%;
            height: 20px;
        }

        .auto-style2 {
            height: 20px;
        }
    </style>
    <table width="100%">

        <tr>
            <td>
                <br />
            </td>

        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>Company:</td>
                        <td>
                            <asp:TextBox ID="txtCompany" runat="server"></asp:TextBox></td>

                        <td>State:</td>
                        <td>
                            <asp:DropDownList ID="ddlStatesearch" runat="server">
                            </asp:DropDownList>
                            <%-- <asp:TextBox ID="txtLocationSearchState" runat="server"></asp:TextBox>--%>
                        </td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Height="20px" Text="SEARCH" CssClass="SqureButton" OnClick="btnSearch_Click" />
                            <asp:Button ID="btnclear" runat="server" Text="RESET" CssClass="SqureButton" OnClientClick="ClearControl();" OnClick="btnclear_Click" />

                        </td>
                    </tr>
                    <tr>
                        <td>City:</td>
                        <td>
                            <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                        </td>
                        <td>ZipCode: </td>
                        <td>
                            <asp:TextBox ID="txtZipCode" runat="server" onkeypress="return ValidateNumber(event);"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
        <tr>
            <td>
                <br />
            </td>

        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table width="99%">
                            <tr>
                                <td align="left">

                                    <asp:Label ID="lblPages" runat="server" Font-Bold="true"></asp:Label>

                                </td>
                                <td align="right">

                                    <asp:Button ID="btnVendor" runat="server" Text="ADD VENDOR" OnClick="btnVendor_Click" CssClass="SqureButton" />

                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>

        </tr>
        <tr>
            <td>

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="grdVendor" runat="server" AutoGenerateColumns="false" Width="99%"
                            OnRowCommand="grdVendor_RowCommand" OnRowDataBound="grdVendor_RowDataBound"
                            EmptyDataRowStyle-HorizontalAlign="Center" AllowPaging="true"
                            CssClass="AllBorder" PageSize="20" OnPageIndexChanging="grdVendor_PageIndexChanging"
                            OnRowCreated="grdVendor_RowCreated"
                            HeaderStyle-CssClass="HeaderGridView" FooterStyle-CssClass="FooterGridView"
                            RowStyle-CssClass="RowGridView" AlternatingRowStyle-CssClass="AlternatingRowGridView2">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="ACTIVE" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkActive" runat="server" Height="15px" Width="15px" Checked='<%# Eval("IsActive") %>' CommandArgument='<%# Eval("UserID") %>' onchange='<%# Eval("UserID","return SetActive({0})") %>' />
                                        <asp:HiddenField ID="hdnActive" runat="server" Value='<% #Bind("IsActive")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="UserName" HeaderText="USER NAME" />
                                <asp:BoundField DataField="Company" HeaderText="COMPANY" />
                                <asp:BoundField DataField="Address1" HeaderText="ADDRESS" />
                                <asp:BoundField DataField="City" HeaderText="City" />
                                <asp:BoundField DataField="State" HeaderText="State" />
                                <asp:BoundField DataField="Zip" HeaderText="ZipCode" />
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnUserID" runat="server" Value='<%# Eval("UserID") %>' />
                                        <asp:ImageButton runat="server" ImageUrl="~/Images/edit.jpg" ID="btnEdit" CommandName="Ed" CommandArgument='<%# Eval("UserID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnCanDel" runat="server" Value='<%# Eval("CanDelete") %>' />
                                        <asp:ImageButton runat="server" ImageUrl="~/Images/delete.gif" OnClientClick='javascript:return confirm("Do You Want To Delete This Record");' ID="delRecord" CommandName="Del" CommandArgument='<%# Eval("UserID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                            </PagerTemplate>
                            <PagerStyle CssClass="pagerStyle" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="ff" runat="server">
        <ContentTemplate>
            <ajax:ModalPopupExtender ID="mpeUpdateVendor" TargetControlID="lbDummy" CancelControlID="imgbtnClose"
                PopupControlID="divPopUp" runat="server" BackgroundCssClass="modalBackground">
            </ajax:ModalPopupExtender>
            <asp:Label ID="lbDummy" runat="server"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divPopUp" class="PopupDivBodyStyle" style="width: 500px; height: 430px; background-color: white; z-index: 111; position: absolute; display: block;">
        <asp:UpdatePanel ID="up1" runat="server">
            <ContentTemplate>
                <table style="width: 100%">
                    <tr style="background-color: gray; border-bottom: solid;">
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td style="text-align: left; width: 490px;">
                                        <asp:Label ID="lblHeader" runat="server" Text="CUSTOMER AND VENDOR" ForeColor="Black" Style="padding-left: 10px; vertical-align: middle; font-weight: bold;"></asp:Label>
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:ImageButton ID="imgbtnClose" runat="server" ImageUrl="~/Images/Close.png" AlternateText="Close"
                                            ForeColor="Red" ImageAlign="Right" Style="margin-top: 4px" />
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                        </td>
                    </tr>
                    <tr style="height: 22px;">
                        <td colspan="2" class="Paddingtd" style="padding-left: 20px;">
                            <asp:Label ID="lblErrorMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%" class="labelPadding">FIRST NAME
                        </td>
                        <td>
                            <asp:TextBox ID="txtFirstName" runat="server" Width="250px" Height="18px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%" class="labelPadding">LAST NAME
                        </td>
                        <td>
                            <asp:TextBox ID="txtLastName" runat="server" Width="250px" Height="18px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%" class="labelPadding">COMPANY
                        </td>
                        <td>
                            <asp:TextBox ID="txtCompanies" runat="server" Width="250px" Height="18px"></asp:TextBox>
                            <ajax:AutoCompleteExtender ID="acBrokerInfo" runat="server"
                                TargetControlID="txtCompanies"
                                DelimiterCharacters=";, :"
                                MinimumPrefixLength="1"
                                EnableCaching="true"
                                CompletionSetCount="10"
                                CompletionInterval="300"
                                ServiceMethod="SearchBrokerComapny"
                                OnClientPopulating="ShowIcon"
                                OnClientPopulated="hideIcon"
                                OnClientItemSelected="OnBrokerSelected"
                                ShowOnlyCurrentWordInCompletionListItem="true"
                                CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListElementID="divwidth"
                                UseContextKey="true" />
                            <asp:HiddenField ID="hdnBrokerId" runat="server" Value="" />
                            <asp:HiddenField ID="hdnDealBrokerCommissionId" runat="server" Value="" />
                        </td>
                    </tr>


                    <tr>
                        <td style="width: 25%" class="labelPadding">ADDRESS
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddress" runat="server" Width="250px" Height="18px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%" class="labelPadding">CITY
                        </td>
                        <td>
                            <asp:TextBox ID="txtCities" runat="server" Width="250px" Height="18px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelPadding">STATE
                        </td>
                        <td class="auto-style2">
                            <asp:DropDownList ID="ddlState" runat="server" Width="250px" Font-Size="Small"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%" class="labelPadding">ZIPCODE
                        </td>
                        <td>
                            <asp:TextBox ID="txtZip" CssClass="onlyNumbers" runat="server" Width="170px" Height="18px" onkeypress="return ValidateNumber(event);"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <%-- Onblur= "javascript:EmailValidation();"--%>
                        <td style="width: 25%" class="labelPadding">EMAIL
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" Width="250px" Height="18px"></asp:TextBox>
                        </td>
                    </tr>
                     <tr>                        
                        <td style="width: 25%" class="labelPadding">EIN
                        </td>
                        <td>
                            <asp:TextBox ID="txtEIN" runat="server" Width="250px" Height="18px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%" class="labelPadding">BROKER TYPE</td>
                        <td>
                            <asp:DropDownList ID="drpBrokerType" runat="server" Width="250px">
                                <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                <asp:ListItem Value="1" Text="External Broker"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Master Broker"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Referral Broker"></asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 25%" class="labelPadding">IsActive</td>
                        <td>
                            <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%"></td>
                        <td>
                            <asp:Button ID="btnAddBroker" runat="server" Text="ADD" CssClass="SqureButton" Width="100px" OnClick="btnAddBroker_Click" OnClientClick="return Validate();" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" runat="Server">
</asp:Content>

