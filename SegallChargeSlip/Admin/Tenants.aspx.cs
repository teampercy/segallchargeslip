﻿using SegallChargeSlipBll;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Admin_Tenants : System.Web.UI.Page
{
    # region properties
    //public string selected
    //{
    //    get { return hdnLocationId.Value; }
    //    set { hdnLocationId.Value = value; }

    //}
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {

            Session.Clear();
            Session.RemoveAll();
            Server.Transfer("../Login.aspx");

        }
        if (!Page.IsPostBack)
        {
            Utility.FillStates(ref ddlStatesearch);
            if (Session["Type"] != null)
            {
                ViewState["DealParty"] = Session["Type"].ToString();
                if (ViewState["DealParty"].ToString() == "Tenants")
                    btnaddNewBuyer.Text = "ADD NEW TENANT";
                else
                    btnaddNewBuyer.Text = "ADD NEW BUYER";
                Bindgrid(1);
            }
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        GridViewRow grdStud = grdBuyers.BottomPagerRow;
        if (grdStud != null)
        {
            grdStud.Visible = true;
        }
    }
    public void Bindgrid(int pageno = 1)
    {
        try
        {
            DealParties objDealParties = new DealParties();
            objDealParties.CurrentPageNo = pageno;
            objDealParties.PageSize = 25;
            if (ViewState["DealParty"].ToString() == "Tenants")
                objDealParties.PartyTypeId = 2;
            else if (ViewState["DealParty"].ToString() == "Buyers")
                objDealParties.PartyTypeId = 1;
            if (!String.IsNullOrEmpty(txtSearchName.Text))
                objDealParties.PartyName = txtSearchName.Text;
            if (!String.IsNullOrEmpty(txtSearchCompanyName.Text))
                objDealParties.PartyCompanyName = txtSearchCompanyName.Text;
            if (!String.IsNullOrEmpty(txtSearchCity.Text))
                objDealParties.City = txtSearchCity.Text;
            if (!String.IsNullOrEmpty(ddlStatesearch.SelectedValue) || ddlStatesearch.SelectedValue == "0")
                objDealParties.State = ddlStatesearch.SelectedValue;
            //if (!String.IsNullOrEmpty(txtSearchZip.Text))
            //    objDealParties.ZipCode = Convert.ToInt32(txtSearchZip.Text);
            //else
            //    objDealParties.ZipCode = 0;
            DataSet ds = new DataSet();
            ds = objDealParties.GetListPageWise();

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ViewState["PageNumber"] = objDealParties.CurrentPageNo;
                    //if (objLocation.TotalRecords > objLocation.PageSize)
                    double totalRecords = objDealParties.TotalRecords;
                    ViewState["TotalRecords"] = objDealParties.TotalRecords;
                    double pagesize = objDealParties.PageSize;
                    ViewState["TotalPages"] = Math.Ceiling(totalRecords / pagesize);

                    grdBuyers.DataSource = ds.Tables[0];
                    grdBuyers.DataBind();
                    //this.PopulatePager(objLocation.TotalRecords, pageno);
                }
                else
                {
                    grdBuyers.DataSource = null;
                    grdBuyers.DataBind();
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Tenants.aspx", "Bindgrid", ex.Message);
        }
    }
    public void Bindgrid()
    {
        try
        {
            int pageno = 1;
            DealParties objDealParties = new DealParties();
            objDealParties.CurrentPageNo =Convert.ToInt32(ViewState["PageNumber"].ToString());
            objDealParties.PageSize = 25;
            if (ViewState["DealParty"].ToString() == "Tenants")
                objDealParties.PartyTypeId = 2;
            else if (ViewState["DealParty"].ToString() == "Buyers")
                objDealParties.PartyTypeId = 1;
            if (!String.IsNullOrEmpty(txtSearchName.Text))
                objDealParties.PartyName = txtSearchName.Text;
            if (!String.IsNullOrEmpty(txtSearchCompanyName.Text))
                objDealParties.PartyCompanyName = txtSearchCompanyName.Text;
            if (!String.IsNullOrEmpty(txtSearchCity.Text))
                objDealParties.City = txtSearchCity.Text;
            if (!String.IsNullOrEmpty(ddlStatesearch.SelectedValue) || ddlStatesearch.SelectedValue == "0")
                objDealParties.State = ddlStatesearch.SelectedValue;
            //if (!String.IsNullOrEmpty(txtSearchZip.Text))
            //    objDealParties.ZipCode = Convert.ToInt32(txtSearchZip.Text);
            //else
            //    objDealParties.ZipCode = 0;
            DataSet ds = new DataSet();
            ds = objDealParties.GetListPageWise();

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ViewState["PageNumber"] = objDealParties.CurrentPageNo;
                    //if (objLocation.TotalRecords > objLocation.PageSize)
                    double totalRecords = objDealParties.TotalRecords;
                    ViewState["TotalRecords"] = objDealParties.TotalRecords;
                    double pagesize = objDealParties.PageSize;
                    ViewState["TotalPages"] = Math.Ceiling(totalRecords / pagesize);

                    grdBuyers.DataSource = ds.Tables[0];
                    grdBuyers.DataBind();
                    //this.PopulatePager(objLocation.TotalRecords, pageno);
                }
                else
                {
                    grdBuyers.DataSource = null;
                    grdBuyers.DataBind();
                }
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Tenants.aspx", "Bindgrid", ex.Message);
        }
    }
    protected void grdBuyers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        //int PageNo = e.NewPageIndex + 1;
        //this.Bindgrid(PageNo);
        int PageNo = Convert.ToInt32(ViewState["PageNo"]);

        if (e.NewPageIndex == -1)
        {
            PageNo++;
            ViewState["PageNo"] = PageNo;
        }
        else if (e.NewPageIndex == -2)
        {
            PageNo--;
            ViewState["PageNo"] = PageNo;
        }
        else
        {
            PageNo = e.NewPageIndex + 1;
            ViewState["PageNo"] = PageNo;
        }
        this.Bindgrid(PageNo);
    }
    protected void grdBuyers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Ed")
            {
                DealParties objDealParties = new DealParties();
                objDealParties.PartyID = Convert.ToInt32(e.CommandArgument);
                bool load = objDealParties.Load();
                if (load)
                {
                    if (ViewState["DealParty"].ToString() == "Tenants")
                    {
                        lbTitle.Text = "EDIT TENANT";
                      
                    }
                    else if (ViewState["DealParty"].ToString() == "Buyers")
                    {
                        lbTitle.Text = "EDIT BUYER";
                       
                    }
                    this.ucAddNewBuyerTenant.btnaddedit = "UPDATE";
                    this.ucAddNewBuyerTenant.PartyTypeId = objDealParties.PartyTypeId.ToString();
                    this.ucAddNewBuyerTenant.PartyId = objDealParties.PartyID.ToString();
                    this.ucAddNewBuyerTenant.Name = objDealParties.PartyName;
                    this.ucAddNewBuyerTenant.Address1 = objDealParties.Address1;
                    this.ucAddNewBuyerTenant.Address2 = objDealParties.Address2;
                    this.ucAddNewBuyerTenant.City = objDealParties.City;
                    this.ucAddNewBuyerTenant.Email = objDealParties.Email;
                    this.ucAddNewBuyerTenant.State = objDealParties.State;
                    this.ucAddNewBuyerTenant.ZipCode = objDealParties.ZipCode.ToString();
                    this.ucAddNewBuyerTenant.CompanyName = objDealParties.PartyCompanyName;
                    this.ucAddNewBuyerTenant.Isactive = objDealParties.IsActive;

                    mpeAddNewTenants.Show();

                }
            }
            if (e.CommandName == "Del")
            {

                DealParties objDealParties = new DealParties();
                objDealParties.PartyID = Convert.ToInt32(e.CommandArgument);
                bool load = objDealParties.Delete();
                //if (!load)
               // ScriptManager.RegisterStartupScript(this, this.GetType(), "Location", "alert('Error : Cannot be deleted,Dependent Deal Transaction is present');", true);
                Bindgrid();

            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Tenants.aspx", "grdBuyers_RowCommand", ex.Message);
        }

    }
    protected void grdBuyers_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                int pageno = Convert.ToInt32(ViewState["PageNumber"]);
                int totalpage = Convert.ToInt32(ViewState["TotalPages"]);
                int totalRecords = Convert.ToInt32(ViewState["TotalRecords"]);
                LinkButton btnPrevious = new LinkButton();
                btnPrevious.ID = "btnPrevious";
                btnPrevious.Text = "<";
                btnPrevious.CommandName = "Page";
                btnPrevious.CommandArgument = "-1";
                btnPrevious.CssClass = "removeunderline";

                LinkButton btnNext = new LinkButton();
                btnNext.ID = "btnNext";
                btnNext.Text = ">";
                btnNext.CommandName = "Page";
                btnNext.CssClass = "removeunderline";
                btnNext.CommandArgument = "0";
                e.Row.Cells[0].Controls.Add(btnPrevious);


                for (int i = 1; i <= totalpage; i++)
                {
                    LinkButton lnk = new LinkButton();
                    lnk.ID = "lnk" + i;
                    lnk.Text = i.ToString() + " ";
                    lnk.CommandArgument = i.ToString();
                    lnk.CommandName = "Page";
                    e.Row.Cells[0].Controls.Add(lnk);
                    lnk.CssClass = "removeunderline";
                    if (Convert.ToInt32(ViewState["PageNo"]) == i)
                    {
                        lnk.CssClass = "linkbut";
                    }
                }
                e.Row.Cells[0].Controls.Add(btnNext);
              
                Label lblpg = new Label();
                lblpg.Text = "Total Records:" + totalRecords.ToString();
                lblpg.ID = "lblpage";

                lblpg.CssClass = "PageNoCSS";
                e.Row.Cells[0].Controls.Add(lblpg);
              //  lblPages.Text = "Page No:" + pageno + " of " + totalpage + " Pages";
                if (Convert.ToInt32(ViewState["TotalPages"]) == 1)
                {
                    btnNext.Enabled = false;
                    btnPrevious.Enabled = false;
                }
                else if (Convert.ToInt32(ViewState["TotalPages"]) == Convert.ToInt32(ViewState["PageNo"]))
                {
                    btnNext.Enabled = false;
                    btnPrevious.Enabled = true;
                }
                else if (Convert.ToInt32(ViewState["PageNo"]) == 1)
                {
                    btnNext.Enabled = true;
                    btnPrevious.Enabled = false;
                }
                else
                {
                    btnPrevious.Enabled = true;
                    btnNext.Enabled = true;
                }

            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Tenants.aspx", "grdBuyers_RowCreated", ex.Message);
        }
    }
    protected void btnSearchBuyer_Click(object sender, EventArgs e)
    {
        Bindgrid(1);
    }
    protected void btnaddNewBuyer_Click(object sender, EventArgs e)
    {
        this.ucAddNewBuyerTenant.btnaddedit = "ADD";
        //this.ucAddNewBuyerTenant.ClassRef = "T";
        if (ViewState["DealParty"].ToString() == "Tenants")
        {
            lbTitle.Text = "ADD NEW TENANT";
            this.ucAddNewBuyerTenant.PartyTypeId = "2";
        }
        else if (ViewState["DealParty"].ToString() == "Buyers")
        {
            lbTitle.Text = "ADD NEW BUYERS";
            this.ucAddNewBuyerTenant.PartyTypeId = "1";
        }
       
       
        this.ucAddNewBuyerTenant.ResetControls();
        mpeAddNewTenants.Show();
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SetActive(string PartyID)
    {
        try
        {
            DealParties objDealParties = new DealParties();
            objDealParties.PartyID = Convert.ToInt32(PartyID);
            bool load = objDealParties.Load();
            if (load)
            {
                if (objDealParties.IsActive)
                {
                    objDealParties.IsActive = false;
                }
                else
                {
                    objDealParties.IsActive = true;
                }
                objDealParties.Save();

            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Tenants.aspx", "SetActive", ex.Message);
        }
    }
    protected void grdBuyers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            DealParties objDealParties = new DealParties();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnPartyId = (HiddenField)e.Row.FindControl("hdnPartyId") as HiddenField;
                objDealParties.PartyID = Convert.ToInt32(hdnPartyId.Value);

                HiddenField hdnCanDel = (HiddenField)e.Row.FindControl("hdnCanDel") as HiddenField;
                int candel = Convert.ToInt32(hdnCanDel.Value);
                if (candel == 0)
                {
                    ImageButton imgDelete = (ImageButton)e.Row.FindControl("delRecord") as ImageButton;
                    imgDelete.Enabled = false;
                    imgDelete.Width = 16;
                    imgDelete.Height = 16;
                    imgDelete.ImageUrl = "~/Images/deletegray.png";
                }
                        // imgDelete.Visible = false;
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Update Deal Type", "alert('Deal Type cannot be delete because it has referenced.');", true);
                   
             
            }
        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealType.aspx", "grdDealType_RowDataBound", ex.Message);
        }
    }
    protected void btnclear_Click(object sender, EventArgs e)
    {
        ViewState["PageNo"] = "1";
        Bindgrid(1);
    }
}