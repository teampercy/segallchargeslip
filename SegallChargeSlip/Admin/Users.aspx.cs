﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SegallChargeSlipBll;
using System.Data;

public partial class Pages_Users : System.Web.UI.Page
{
    private int PageSize = 20;

    private int UserID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["PageNo"] = 1;
            BindGrid(Convert.ToInt32(ViewState["PageNo"]));
            // btnSave.Attributes.Add("onclick", "javascript:alert('hi');return false;");
        }
        SetAttributes();
    }
    private void SetAttributes()
    {
        txt1_1.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txt1_1.ClientID + "')");
        txt2_1.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txt2_1.ClientID + "')");
        txt3_1.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txt3_1.ClientID + "')");
        txt4_1.Attributes.Add("onkeyup", "javascript:return AllowDecimals('" + txt4_1.ClientID + "')");
    }
    private void BindGrid(int PageIndex)
    {
        try
        {
            Users user = new Users();
            user.CurrentPageNo = Convert.ToInt32(ViewState["PageNo"]);

            user.PageSize = PageSize;
            user.Username = txtSearch.Text.Trim();
            gvUsers.DataSource = null;
            gvUsers.DataBind();
            DataSet ds = user.GetList();
            int TotalRecords = user.TotalRecords;
            ViewState["TotalRecords"] = TotalRecords;
            gvUsers.DataSource = ds;
            gvUsers.DataBind();

            if (gvUsers.Rows.Count > 0)
            {
                //if (TotalRecords > 0)
                //{
                //    lblTotalRecord.Text = "Total Records: " + TotalRecords;
                //}
                gvUsers.BottomPagerRow.Visible = true;
            }
            else 
            {
               // lblTotalRecord.Text = string.Empty;
            }

            // upUsers.Update();

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Users.aspx", "BindGrid", ex.Message);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            ViewState["PageNo"] = 1;
            BindGrid(Convert.ToInt32(ViewState["PageNo"]));
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Users.aspx", "btnSearch_Click", ex.Message);
        }
    }
    protected void gvUsers_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                double TotalRecord = Convert.ToInt32(ViewState["TotalRecords"]);
                double Pagesize = PageSize;
                double TotalPage = Math.Ceiling(TotalRecord / Pagesize);
                ViewState["TotalPage"] = TotalPage;

                //int TotalRecords = Convert.ToInt32(ViewState["TotalRecords"]);
                //double dblPageCount = (double)((decimal)TotalRecords / (decimal)PageSize);
                //int PageCount = (int)Math.Ceiling(dblPageCount);

                LinkButton btnPrevious = new LinkButton();
                btnPrevious.ID = "btnPrevious";
                btnPrevious.Text = "<";
                btnPrevious.CommandName = "Page";
                btnPrevious.CommandArgument = "-1";
                btnPrevious.Width = Unit.Pixel(15);
                btnPrevious.CssClass = "removeunderline";
                LinkButton btnNext = new LinkButton();
                btnNext.ID = "btnNext";
                btnNext.Text = ">";
                btnNext.CommandName = "Page";
                btnNext.CssClass = "removeunderline";
                btnNext.CommandArgument = "0";
                //e.Row.Cells[0].Controls.Add(btnPrevious);
                PlaceHolder place = e.Row.FindControl("phPageNo") as PlaceHolder;
                place.Controls.Add(btnPrevious);
                for (int i = 1; i <= TotalPage; i++)
                {
                    LinkButton btn = new LinkButton();
                    btn.Width = Unit.Pixel(15);
                    btn.CommandName = "Page";
                    btn.CommandArgument = i.ToString();

                    btn.Text = i.ToString().Trim();
                    btn.CssClass = "removeunderline";
                    if (Convert.ToInt32(ViewState["PageNo"].ToString()) == i)
                    {
                        btn.CssClass = "linkbut";
                    }
                    
                    place.Controls.Add(btn);
                }
                place.Controls.Add(btnNext);
                if (ViewState["TotalRecords"] != null)
                {

                    Label lblTotalRecords = new Label();
                    lblTotalRecords.CssClass = "PageNoCSS";
                    lblTotalRecords.Text = "Total Records:" + ViewState["TotalRecords"].ToString();
                    place.Controls.Add(lblTotalRecords);
                }
                //Label lblPages = new Label();
                //lblPages.CssClass = "PageNoCSS";
                //lblPages.Text = "Page No:" + ViewState["PageNo"].ToString() + " of " + TotalPage + " Pages";
                //place.Controls.Add(lblPages);
               
                //e.Row.Cells[0].Controls.Add(btnNext);
                //lblPages.Text = "Page No:" + ViewState["PageNo"].ToString() + " of " + TotalPage + " Pages";
                if (Convert.ToInt32(ViewState["TotalPage"]) == 1)
                {
                    btnNext.Enabled = false;
                    btnPrevious.Enabled = false;
                }
                else if (Convert.ToInt32(ViewState["TotalPage"]) == Convert.ToInt32(ViewState["PageNo"]))
                {
                    btnNext.Enabled = false;
                    btnPrevious.Enabled = true;
                }
                else if (Convert.ToInt32(ViewState["PageNo"]) == 1)
                {
                    btnNext.Enabled = true;
                    btnPrevious.Enabled = false;
                }
                else
                {
                    btnPrevious.Enabled = true;
                    btnNext.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Users.aspx", "gvUsers_RowCreated", ex.Message);
        }
    }

    protected void gvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Del")
            {
                UserDetails userdetail = new UserDetails();
                userdetail.UserID = Convert.ToInt32(e.CommandArgument);
                bool deleteUserdetail = userdetail.Delete();
                Users user = new Users();
                user.UserID = Convert.ToInt32(e.CommandArgument);
                bool deleteUser = user.Delete();

                BindGrid(0);
                //user.IsActive = false;
                //user.Delete();

                //UserDetails userdetail = new UserDetails();
                //userdetail.UserID = UserID;
                //userdetail.IsActive = false;
                //userdetail.Delete();



            }
            if (e.CommandName == "Ed")
            {
                UserID = Convert.ToInt32(e.CommandArgument);
                Users user = new Users();
                user.UserID = UserID;
                hdnCount.Value = "2";
                Boolean isloaded = user.Load();
                if (isloaded)
                {
                    lblNames.Text = user.LastName + "," + "&nbsp;" + user.FirstName;
                    lblDate.Text = string.Empty;
                    txtFirstName.Text = user.FirstName;
                    txtLastName.Text = user.LastName;
                    txtCompany.Text = user.CompanyName;
                    txtEmail.Text = user.Email;
                    txtPwd.Text = user.Password;
                    txtConfmPass.Text = user.Password;
                    chkActive.Checked = user.IsActive;
                    string date = Convert.ToString(user.LastModifiedOn);
                    if (user.LastModifiedOn == null || date.Contains("1/1/1900"))
                    {
                        lblLastUpdated.Text = string.Empty;
                        lblDate.Text = string.Empty;
                    }
                    else
                    {
                        lblLastUpdated.Text = "LAST UPDATED:";
                        lblDate.Text = user.LastModifiedOn.ToShortDateString();
                    }


                    DataTable dt = user.LoadUserDetails();
                    if (dt != null)
                    {
                        hdn1_1.Value = "0";
                        hdnCheck_1.Value = "0";
                        if (dt.Rows.Count >= 1)
                        {
                            hdn1_1.Value = Convert.ToString(dt.Rows[0]["UserDetailsID"]);
                            hdnCheck_1.Value = "0";
                            txtFrom.Text = Convert.ToString(dt.Rows[0]["FromDate"]);
                            txtTo.Text = Convert.ToString(dt.Rows[0]["ToDate"]);
                            txt1_1.Text = dt.Rows[0]["EarningsFrom"].ToString();
                            txt2_1.Text = Convert.ToString(dt.Rows[0]["EarningsTo"]);
                            txt3_1.Text = Convert.ToString(dt.Rows[0]["PercentBroker"]);
                            txt4_1.Text = Convert.ToString(dt.Rows[0]["PercentCompany"]);
                            imgDelete.OnClientClick = "DeleteRecord('" + 0 + "','" + hdn1_1.Value + "')";
                        }
                        else
                        {
                            txtFrom.Text = string.Empty;
                            txtTo.Text = string.Empty;
                            txt1_1.Text = string.Empty;
                            txt2_1.Text = string.Empty;
                            txt3_1.Text = string.Empty;
                            txt4_1.Text = string.Empty;
                            imgDelete.OnClientClick = "DeleteRecord('" + 0 + "','" + hdn1_1.Value + "')";
                        }

                        for (int i = 2; i <= dt.Rows.Count; i++)
                        {
                            int j = i - 1;
                            HiddenField hdn = new HiddenField();
                            hdn.ID = "hdn1_" + i;
                            hdn.Value = dt.Rows[i - 1]["UserDetailsID"].ToString();

                            HiddenField hdnCheck = new HiddenField();
                            hdnCheck.ID = "hdnCheck_" + i;
                            hdnCheck.Value = j.ToString();

                            TableRow tr = new TableRow();
                            TableCell tc = new TableCell();
                            tc.Controls.Add(hdn);
                            tc.Controls.Add(hdnCheck);
                            tr.Cells.Add(tc);


                            TextBox txt = new TextBox();
                            txt.ID = "txt1_" + i;
                            //txt.ClientID = "txt1_" + i;
                            txt.Text = dt.Rows[i - 1]["EarningsFrom"].ToString();
                            //txt.Width = 40;
                            txt.Style.Add("width", "90%");
                            txt.Attributes.Add("onkeyup", "javascript:return AllowDecimals('ContentMain_" + txt.ClientID + "')");
                            tc = new TableCell();
                            tc.Controls.Add(txt);
                            tr.Cells.Add(tc);

                            Label lbl = new Label();
                            lbl.ID = "lbl" + i;
                            lbl.Text = "<hr />";
                            lbl.Width = 10;
                            tc = new TableCell();
                            tc.Controls.Add(lbl);
                            tr.Cells.Add(tc);


                            txt = new TextBox();
                            txt.ID = "txt2_" + i;
                            txt.Text = dt.Rows[i - 1]["EarningsTo"].ToString();
                            txt.Style.Add("width", "100%");
                            txt.Attributes.Add("onkeyup", "javascript:return AllowDecimals('ContentMain_" + txt.ClientID + "')");
                            tc = new TableCell();
                            tc.Controls.Add(txt);
                            tr.Cells.Add(tc);

                            lbl = new Label();
                            lbl.ID = "lbl1_" + i;
                            lbl.Text = "";
                            lbl.Width = 20;
                            tc = new TableCell();
                            tc.Controls.Add(lbl);
                            tr.Cells.Add(tc);

                            txt = new TextBox();
                            txt.ID = "txt3_" + i;
                            txt.Text = dt.Rows[i - 1]["PercentBroker"].ToString();
                            txt.Style.Add("width", "90%");
                            txt.Attributes.Add("onkeyup", "javascript:return AllowDecimals('ContentMain_" + txt.ClientID + "')");
                            tc = new TableCell();
                            tc.Controls.Add(txt);
                            tr.Cells.Add(tc);

                            txt = new TextBox();
                            txt.ID = "txt4_" + i;
                            txt.Text = dt.Rows[i - 1]["PercentCompany"].ToString();
                            txt.Style.Add("width", "100%");
                            txt.Attributes.Add("onkeyup", "javascript:return AllowDecimals('ContentMain_" + txt.ClientID + "')");
                            tc = new TableCell();
                            tc.Controls.Add(txt);
                            tr.Cells.Add(tc);

                            // Button btn = new Button();
                            ImageButton btn = new ImageButton();
                            btn.ID = "btn_" + i;
                            // btn.Text = "btn";
                            btn.ImageUrl = "~/Images/delete.gif";
                            //btn.OnClientClick = "DeleteRecord('"+j+"')";
                            btn.OnClientClick = "DeleteRecord('" + j + "','" + hdn.Value + "')";
                            tc = new TableCell();
                            tc.Controls.Add(btn);
                            tr.Cells.Add(tc);


                            tblUsers.Rows.Add(tr);
                            hdnCount.Value = (Convert.ToInt32(hdnCount.Value) + 1).ToString();
                        }

                    }

                }
                
                lblHeader.Text = "Edit User";
                ViewState["userid"] = e.CommandArgument;
                Session["ExistUserId"] = e.CommandArgument;
                //Session["userid"] = UserID;

                modalAddEdit.Show();
            }


        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Users.aspx", "gvUsers_RowCommand", ex.Message);
        }
    }
    protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            //int i = e.NewPageIndex + 1;
            //ViewState["PageNo"] = i;
            //BindGrid(i);
            int PageNo = Convert.ToInt32(ViewState["PageNo"]);

            if (e.NewPageIndex == -1)
            {
                PageNo++;
                ViewState["PageNo"] = PageNo;
            }
            else if (e.NewPageIndex == -2)
            {
                PageNo--;
                ViewState["PageNo"] = PageNo;
            }
            else
            {
                PageNo = e.NewPageIndex + 1;
                ViewState["PageNo"] = PageNo;
            }
            BindGrid(PageNo);
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Users.aspx", "gvUsers_PageIndexChanging", ex.Message);
        }
    }
    private void FillUserDetails(DataTable dt)
    {


    }

    protected void btnSaveUsers_Click(object sender, EventArgs e)
    {
        


    }
    private string ControlValue(string ctrlPrefix)
    {
        try
        {
            string[] ctrls = Request.Form.ToString().Split('&');
            int cnt = FindOccurence(ctrlPrefix);
            if (cnt > 0)
            {
                for (int k = 1; k <= cnt; k++)
                {
                    for (int i = 0; i < ctrls.Length; i++)
                    {
                        if (ctrls[i].Contains(ctrlPrefix))
                        {
                            string ctrlName = ctrls[i].Split('=')[0];
                            string ctrlValue = ctrls[i].Split('=')[1];

                            //Decode the Value
                            ctrlValue = Server.UrlDecode(ctrlValue);
                            return ctrlValue;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Users.aspx", "ControlValue", ex.Message);
        }
        return "";
    }
    private int FindOccurence(string substr)
    {
        string reqstr = Request.Form.ToString();
        return ((reqstr.Length - reqstr.Replace(substr, "").Length)
                / substr.Length);
    }


    protected void btnAddOptTerms_Click(object sender, EventArgs e)
    {
        ViewState["userid"] = null;
        lblHeader.Text = "USER DETAILS";
        lblNames.Text = "LAST NAME, FIRST NAME";
        hdn1_1.Value = "0";
        //Users obj = new Users();
        //obj.Load();
        lblLastUpdated.Text = string.Empty;
        lblDate.Text = string.Empty;
        modalAdd.Show();
        hdnCount.Value = "2";
        imgDelete.OnClientClick = "DeleteRecord('" + 0 + "','" + 0 + "')";
        Session["ExistUserId"] = "0";

    }
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static string[] SearchGetComapnyList(string prefixText, int count)
    {
        List<string> lstSearchList = new List<string>();
        try
        {
            Users objUsers = new Users();
            objUsers.CompanyName = prefixText;
            DataSet ds = objUsers.GetCompanySearchList();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dt in ds.Tables[0].Rows)
                {
                    //dt["ValueType"].ToString()
                    lstSearchList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dt["CompanyName"].ToString(), dt["CompanyName"].ToString()));
                    // lstSearchList.Add(dt[1].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Users.aspx", "SearchGetComapnyList", ex.Message);
        }
        return lstSearchList.ToArray();
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void SetActive(string ID)
    {
        try
        {
            Users objUsers = new Users();
            objUsers.UserID = Convert.ToInt32(ID);
            bool load = objUsers.Load();
            if (load)
            {
                if (objUsers.IsActive)
                {
                    objUsers.IsActive = false;
                }
                else
                {
                    objUsers.IsActive = true;
                }
                objUsers.Save();

            }
        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Users.aspx", "SetActive", ex.Message);
        }
    }
    protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnUserId = (HiddenField)e.Row.FindControl("hdnUserId") as HiddenField;
                // objUsers.UserID = Convert.ToInt32(hdnUserId.Value);
                CheckBox chkActive = (CheckBox)e.Row.FindControl("chkActive");
                HiddenField hdnActive = (HiddenField)e.Row.FindControl("hdnActive") as HiddenField;
                HiddenField hdnCanDel = (HiddenField)e.Row.FindControl("hdnCanDel") as HiddenField;
                int candel = Convert.ToInt32(hdnCanDel.Value);
                if (candel == 0)
                {
                    ImageButton imgDelete = (ImageButton)e.Row.FindControl("ImgDelete") as ImageButton;
                    imgDelete.Enabled = false;
                    imgDelete.Width = 16;
                    imgDelete.Height = 16;
                    imgDelete.ImageUrl = "~/Images/deletegray.png";
                }
                //DataTable dt = objUsers.LoadUser();
                //if (dt != null)
                //{
                //    if (dt.Rows.Count > 0)
                //    {
                //        ImageButton imgDelete = (ImageButton)e.Row.FindControl("ImgDelete") as ImageButton;
                //        imgDelete.Enabled = false;
                //        // imgDelete.Visible = false;
                //        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Update Deal Type", "alert('Deal Type cannot be delete because it has referenced.');", true);
                //    }
                //}
                if (hdnActive != null)
                {
                    if (hdnActive.Value == "True")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                }
            }

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/DealType.aspx", "grdDealType_RowDataBound", ex.Message);
        }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static void DeleteRecord(string ID)
    {
        try
        {
            UserDetails objUserDetails = new UserDetails();
            objUserDetails.UserID = Convert.ToInt32(ID.ToString());
            objUserDetails.Delete();

        }

        catch (Exception ex)
        {
            ErrorLog.WriteLog("Admin/Users.aspx", "DeleteRecord", ex.Message);
        }
    }
    [System.Web.Services.WebMethod(EnableSession = true)]
    public static string CheckUserEmailAlreadyExist(string Email)
    {
        try
        {
            if (HttpContext.Current.Session["ExistUserId"] != null)
            {
                Int64  userid = Convert.ToInt64(HttpContext.Current.Session["ExistUserId"].ToString());
                Users objUsers = new Users();
                DataSet ds = objUsers.CheckUsersEmailAlreadyExist(Email, userid);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        if (dt != null && dt.Rows.Count > 0)
                        {

                            return "Exist";
                            // ScriptManager.RegisterStartupScript((Page)(HttpContext.Current.Handler), typeof(Page), "dd", "Test()", true); return;
                            //ScriptManager.RegisterStartupScript((Page)(HttpContext.Current.Handler), typeof(Page), "hdrEmpty1", "Test();", true); return;
                        }
                    }
                }
                else
                {
                    return "NotExist";
                }
            }


        }
        catch (Exception ex)
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.RemoveAll();
            HttpContext.Current.Response.Redirect("~/Login.aspx",false);

        }
        return "NotExist";
    }

    protected void btnFillSave_Click(object sender, EventArgs e)
    {
        try
        {

            //Users objUsers = new Users();
            //DataSet ds = objUsers.CheckUsersEmailAlreadyExist(txtEmail.Text );
            //if (ds != null)
            //{
            //    DataTable dt = ds.Tables[0];
            //    if (dt != null && dt.Rows.Count > 0)
            //    {
            //        ScriptManager.RegisterStartupScript(this, Page.GetType(), "display", "display();", true);
            //        modalAdd.Show();
            //        modalAddEdit.Show();
            //        return;
            //    }
            //}
            //else
            //{

            //}
            Users obj;
            if (ViewState["userid"] != null)
            {
                obj = new Users(Convert.ToInt32(ViewState["userid"].ToString()));
            }
            else
            {
                obj = new Users();
            }
            obj.Load();
            obj.FirstName = Convert.ToString(txtFirstName.Text.Trim());
            obj.LastName = Convert.ToString(txtLastName.Text.Trim());
            //if (hdnCompanyId.Value != null && !(hdnCompanyId.Value == ""))
            //{
            //    obj.CompanyID = Convert.ToInt32(Convert.ToString(hdnCompanyId.Value));
            //}
            obj.CompanyName = Convert.ToString(txtCompany.Text.Trim());
            obj.Email = Convert.ToString(txtEmail.Text.Trim());
            obj.Username = Convert.ToString(txtEmail.Text.Trim());
            obj.Password = Convert.ToString(txtPwd.Text.Trim());

            obj.LastModifiedOn = DateTime.Now;
            if (chkActive.Checked)
            {
                obj.IsActive = true;
            }
            else
            {
                obj.IsActive = false;
            }
            obj.UserTypeID = 2;
            obj.BrokerTypeId = 4;
            obj.UserPasswordChange(Convert.ToInt32(Session[GlobleData.User].ToString()));
            obj.Save();
            
            if (ViewState["userid"] == null)
            {
                ViewState["userid"] = obj.UserID;
            }
            string[] ctrls = Request.Form.ToString().Split('&');
            int detailCount = Convert.ToInt32(hdnCount.Value);
            UserDetails objUserDetails = new UserDetails();
            objUserDetails.UserID = Convert.ToInt32(ViewState["userid"].ToString());
            ////Added by Jaywanti on 13-1-2014

            for (int i = 1; i < detailCount; i++)
            {
                UserDetails userdetail = new UserDetails();
                userdetail.UserID = Convert.ToInt32(ViewState["userid"].ToString());
                string userdetailid = ControlValue("hdn1_" + i);
                string txtEarning1 = ControlValue("txt1_" + i);
                string txtEarning2 = ControlValue("txt2_" + i);
                string txtBroker = ControlValue("txt3_" + i);
                string txtCompanies = ControlValue("txt4_" + i);
                userdetail.EarningsFrom = Convert.ToDecimal(txtEarning1.Trim());
                userdetail.EarningsTo = Convert.ToDecimal(txtEarning2.Trim());
                userdetail.PercentBroker = Convert.ToDecimal(txtBroker.Trim());
                userdetail.PercentCompany = Convert.ToDecimal(txtCompanies.Trim());
                userdetail.UserThresoldExist(Convert.ToInt32(Session[GlobleData.User].ToString()));
            }
            ////
            objUserDetails.Delete();
            
          
            // Response.Write(Request.Form.ToString() + "<br/>"); --- SK
            
            for (int i = 1; i < detailCount; i++)
            {
                string userdetailid = ControlValue("hdn1_" + i);
                string txtEarning1 = ControlValue("txt1_" + i);
                string txtEarning2 = ControlValue("txt2_" + i);
                string txtBroker = ControlValue("txt3_" + i);
                string txtCompanies = ControlValue("txt4_" + i);
                // if (!string.IsNullOrEmpty(userdetailid))
                // {
                // int userdetid = Convert.ToInt32(userdetailid);
                // if (userdetid > 0)
                // {


                // UserDetails userdetail = new UserDetails(userdetid);
                UserDetails userdetail = new UserDetails();
                userdetail.UserID = Convert.ToInt32(ViewState["userid"].ToString());
                userdetail.FromDate = Convert.ToDateTime(txtFrom.Text.Trim());
                userdetail.ToDate = Convert.ToDateTime(txtTo.Text.Trim());
                userdetail.EarningsFrom = Convert.ToDecimal(txtEarning1.Trim());
                userdetail.EarningsTo = Convert.ToDecimal(txtEarning2.Trim());
                userdetail.PercentBroker = Convert.ToDecimal(txtBroker.Trim());
                userdetail.PercentCompany = Convert.ToDecimal(txtCompanies.Trim());
                userdetail.IsActive = true;
                userdetail.Save();

                // }



            }
            BindGrid(1);

        }
        catch (Exception ex)
        {
            ErrorLog.WriteLog("Users.aspx", "btnSaveUsers_Click", ex.Message);
        }
    }
}