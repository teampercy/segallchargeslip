﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SettingMaster.master" AutoEventWireup="true" CodeFile="CSSentToEmails.aspx.cs" Inherits="Admin_CSSentToEmails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <script src="../Js/CommonValidations.js"></script>
    <script type="text/javascript">
        //function ShowMessage()
        //{
        //    alert('Record Saved Successfully.');
        //    return false;
        //}

        function ValidateEmail() {
            debugger;
            var txtAdmin1 = document.getElementById('<%=txtAdmin1.ClientID%>');
            var txtAdmin2 = document.getElementById('<%=txtAdmin2.ClientID%>');
            var txtAccounting = document.getElementById('<%=txtAccounting.ClientID%>');
            var txtAccounting2 = document.getElementById('<%=txtAccounting2.ClientID%>');
            var txtNewsAdmin = document.getElementById('<%=txtNewsAdmin.ClientID%>');
            var txtNewsAdmin2 = document.getElementById('<%=txtNewsAdmin2.ClientID%>');
            var txtNewsAdmin3 = document.getElementById('<%=txtNewsAdmin3.ClientID%>');
            var txtNewsAdmin4 = document.getElementById('<%=txtNewsAdmin4.ClientID%>');
            var txtNewsAdmin5 = document.getElementById('<%=txtNewsAdmin5.ClientID%>');

            if ($.trim(txtAccounting.value) != "") {
                if (!IsValidEmailId(txtAccounting)) {
                    alert('Please enter valid accounting email');
                    return false;
                }
            }
            if ($.trim(txtAccounting2.value) != "") {
                if (!IsValidEmailId(txtAccounting2)) {
                    alert('Please enter valid accounting email #2');
                    return false;
                }
            }
            if ($.trim(txtAdmin1.value) != "") {
                if (!IsValidEmailId(txtAdmin1)) {
                    alert('Please enter valid admin email');
                    return false;
                }
            }
            if ($.trim(txtAdmin2.value) != "") {
                if (!IsValidEmailId(txtAdmin2)) {
                    alert('Please enter valid admin email');
                    return false;
                }
            }
            if ($.trim(txtNewsAdmin.value) != "") {
                if (!IsValidEmailId(txtNewsAdmin)) {
                    alert('Please enter valid News Admin Email');
                    return false;
                }
            }
            if ($.trim(txtNewsAdmin2.value) != "") {
                if (!IsValidEmailId(txtNewsAdmin2)) {
                    alert('Please enter valid News Admin Email #2');
                    return false;
                }
            }
            if ($.trim(txtNewsAdmin3.value) != "") {
                if (!IsValidEmailId(txtNewsAdmin3)) {
                    alert('Please enter valid News Admin Email #3');
                    return false;
                }
            }
            if ($.trim(txtNewsAdmin4.value) != "") {
                if (!IsValidEmailId(txtNewsAdmin4)) {
                    alert('Please enter valid News Admin Email #4');
                    return false;
                }
            }
            if ($.trim(txtNewsAdmin5.value) != "") {
                if (!IsValidEmailId(txtNewsAdmin5)) {
                    alert('Please enter valid News Admin Email #5');
                    return false;
                }
            }
            return true;
        }
    </script>
    <table style="width: 100%; border: 1px solid black;">
        <tr>
            <td colspan="2">
                <b>SAVED CHARGESLIP SENT TO</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height: 18px;">
                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>ACCOUNTING</td>
            <td>
                <asp:TextBox ID="txtAccounting" runat="server" Width="270px">
                </asp:TextBox></td>
        </tr>
        <tr>
            <td>ACCOUNTING #2</td>
            <td>
                <asp:TextBox ID="txtAccounting2" runat="server" Width="270px">
                </asp:TextBox></td>
        </tr>
        <tr>
            <td>ADMIN</td>
            <td>
                <asp:TextBox ID="txtAdmin1" runat="server" Width="270px">
                </asp:TextBox></td>
        </tr>
        <tr>
            <td>ADMIN</td>
            <td>
                <asp:TextBox ID="txtAdmin2" runat="server" Width="270px">
                </asp:TextBox></td>
        </tr>
        <tr>
            <td>NEWS ADMIN</td>
            <td>
                <asp:TextBox ID="txtNewsAdmin" runat="server" Width="270px">
                </asp:TextBox></td>
        </tr>
        <tr>
            <td>NEWS ADMIN #2</td>
            <td>
                <asp:TextBox ID="txtNewsAdmin2" runat="server" Width="270px">
                </asp:TextBox></td>
        </tr>
           <tr>
            <td>NEWS ADMIN #3</td>
            <td>
                <asp:TextBox ID="txtNewsAdmin3" runat="server" Width="270px">
                </asp:TextBox></td>
        </tr>
           <tr>
            <td>NEWS ADMIN #4</td>
            <td>
                <asp:TextBox ID="txtNewsAdmin4" runat="server" Width="270px">
                </asp:TextBox></td>
        </tr>
           <tr>
            <td>NEWS ADMIN #5</td>
            <td>
                <asp:TextBox ID="txtNewsAdmin5" runat="server" Width="270px">
                </asp:TextBox></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="ADD" Width="100px" CssClass="SqureButton" OnClick="btnSave_Click" OnClientClick="javascript:return ValidateEmail();" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" runat="Server">
</asp:Content>

