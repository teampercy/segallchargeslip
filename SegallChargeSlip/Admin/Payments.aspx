﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SegallChargeSlip.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="Payments.aspx.cs" Inherits="Admin_Payments" %>

<%@ Register Src="~/UserControl/ucPaymentHistoryDetail.ascx" TagName="ucPaymentHistoryDetail" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/UserControl/ucBilledTab.ascx" TagPrefix="uc1" TagName="ucBilledTab" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <table style="width: 100%">
        <tr>
            <td style="padding-top: 0.5em; color: black; font-family: Verdana; border-color: black; text-align: left; text-shadow: initial; font-weight: 600; font-size: 18px;">ADMINISTRATION
                <asp:Label ID="lbErrorMsg" runat="server" class="errorMsg" /></td>
            <td class="mainPageTitle">PAYMENTS
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr />
            </td>
        </tr>
    </table>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <link href="../Styles/SiteTEST.css" rel="stylesheet" />
    <link href="../Styles/AutoCompleteStyle.css" rel="stylesheet" />
    <link href="../Js/ jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../Js/ jquery-ui-1.10.3.custom/js/jquery-1.9.1.js"></script>
    <link href="../Js/ jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/resources/demos/style.css" />
    <link rel="stylesheet" href="http: //code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http: //code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="../Js/CommonValidations.js"></script>

    <script type="text/javascript">

        function SetBalance(thisid) {
            debugger;
            document.getElementById('btnCreditRemBalance').className = "";
            document.getElementById('btnLeaveRemBalance').className = "";
            var hdnUnappliedDet = document.getElementById('<%=hdnUnappliedDet.ClientID%>').value;
            var hdnBalanceSelecteds = document.getElementById('<%=hdnBalanceSelected.ClientID%>');
            if (thisid.id == "btnCreditRemBalance") {
                thisid.className = "SqureButton1";

                hdnBalanceSelecteds.value = "1";

                document.getElementById('<%=lblCreditAmount.ClientID%>').innerHTML = "$ " + hdnUnappliedDet;
                document.getElementById('<%=lblRemainingBalanceCredit.ClientID%>').innerHTML = "$ 0.00";
            }
            else if (thisid.id == "btnLeaveRemBalance") {
                thisid.className = "SqureButton1";
                hdnBalanceSelecteds.value = "0";

                document.getElementById('<%=lblCreditAmount.ClientID%>').innerHTML = "$ 0.00";
                document.getElementById('<%=lblRemainingBalanceCredit.ClientID%>').innerHTML = "$ " + hdnUnappliedDet;
            }

            //alert(hdnBalanceSelecteds.value);
        return false;
    }
    function ValidateApplyPayment() {
        debugger;
        var hdnBalanceSelecteds = document.getElementById('<%=hdnBalanceSelected.ClientID%>');
        var hdnUnappliedDet = document.getElementById('<%=hdnUnappliedDet.ClientID%>');

        debugger;
        if (hdnUnappliedDet.value != null) {
            if (parseFloat(hdnUnappliedDet.value) > 0) {
                if (hdnBalanceSelecteds != null) {
                    if (hdnBalanceSelecteds.value == "") {
                        alert("Please select any one of the option:-Credit Remaining Balance or Leave Remaining Balance");
                        return false;
                    }
                }
            }
            else {
                hdnBalanceSelecteds.value = "0";
            }

        }

        return true;
    }

    function SaveIndividualPaymentEdit(PaymentHistoryId, ChargeslipId, NewAmount, ReasonToEdit) {
        $.ajax({
            async: true,
            type: "POST",
            url: "Customers.aspx/SaveIndividualBrokerPaymentEdit",
            data: '{PaymentHistoryId: "' + PaymentHistoryId + '",ChargeslipId: "' + ChargeslipId + '",NewAmount: "' + NewAmount + '",ReasonToEdit: "' + ReasonToEdit + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //alert('p');
                if (response.d == "") {
                    debugger;
                    document.getElementById('<%=btnRefreshPayment.ClientID%>').click();

                }
                else {

                    alert(response.d);
                    return false;

                }
            },
            failure: function (response) {

                alert(response.d);
                return false;
            }
        });
        $("#<%=hdnChargeslipId.ClientID%>").val(ChargeslipId);

        //alert('hi');
    }
    function imgEditbtn_ClientBrokerAmountClick(imgbtn) {
        try {

            var ttd = imgbtn.parentElement
            var childLength = ttd.childNodes.length;
            for (var i = 0; i < childLength; i++) {
                if (ttd.childNodes[i].type == 'text') {

                    ttd.childNodes[i].setAttribute("Style", "display:inline-grid;width:120px;");
                    ttd.childNodes[i].value = ttd.childNodes[3].innerHTML;
                }

            }
            imgbtn.setAttribute("Style", "display:none;");
            ttd.childNodes[5].setAttribute("Style", "display:none;");
            ttd.childNodes[7].setAttribute("Style", "display:inline-grid;");
            ttd.childNodes[3].setAttribute("Style", "display:none;");

        }
        catch (exce) {
        }
        return false;

    }
    function imgSavebtn_ClientBrokerAmountClick(imgbtn) {
        try {
            debugger;
            var ttd = imgbtn.parentElement
            var childLength = ttd.childNodes.length;
            var textValue;
            for (var i = 0; i < childLength; i++) {
                if (ttd.childNodes[i].type == 'text') {
                    textValue = ttd.childNodes[i].value;
                    ttd.childNodes[i].setAttribute("Style", "display:none;");
                    break;
                }

            }

            var ttr = ttd.parentElement;


            var PaymentHistoryID = ttd.childNodes[9].innerHTML; // ttr.cells[1].textContent;
            var lblChargeslipid = ttd.childNodes[11].innerHTML;

            var hdnChargeslipId = document.getElementById('<%=hdnChargeslipId.ClientID%>');
            hdnChargeslipId.value = lblChargeslipid;
            imgbtn.setAttribute("Style", "display:none;");
            var OldValue = ttd.childNodes[3].innerHTML;
            ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
            ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
            ttd.childNodes[7].setAttribute("Style", "display:none;");

            ttd.childNodes[3].innerHTML = textValue;
            //$.ajax({
            //    type: "POST",
            //    url: "Customers.aspx/SavePaymentAmount",
            //    data: '{Paymentd: "' + PaymentHistoryID + '",Amount: "' + textValue + '" }',
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json",
            //    failure: function (response) {

            //        alert(response.d);
            //    }
            //});
            document.getElementById('<%=btnRefreshPayment.ClientID%>').click();

        }
        catch (ex) {
        }
        return false;
    }
    function MessagePayment(DealName) {

        alert('The payment for Charge slip ' + DealName + ' has been applied');
    }
    function PaymentRedirect() {
        document.getElementById('<%=btnClearAll.ClientID%>').click();
        //alert('hi');
        // window.location.href = "http://stackoverflow.com";
    }
        function imgEditbtn_ClientAmountClick(imgbtn) {
            debugger;
        try {

            var ttd = imgbtn.parentElement
            var childLength = ttd.childNodes.length;
            for (var i = 0; i < childLength; i++) {
                if (ttd.childNodes[i].type == 'text') {

                    ttd.childNodes[i].setAttribute("Style", "display:inline-grid;width:120px;");
                    ttd.childNodes[i].value = ttd.childNodes[5].innerHTML;
                }

            }
            imgbtn.setAttribute("Style", "display:none;");
            ttd.childNodes[7].setAttribute("Style", "display:none;");
            ttd.childNodes[9].setAttribute("Style", "display:inline-grid;width:13px;height:13px;");
            ttd.childNodes[5].setAttribute("Style", "display:none;");
            ttd.childNodes[3].setAttribute("Style", "display:none;");
            ttd.childNodes[11].setAttribute("Style", "display:inline-grid;width:12px;height:12px;");

        }
        catch (exce) {
        }
        return false;

    }
    function imgSavebtn_ClientAmountClick(imgbtn) {
        try {
            debugger;
            var ttd = imgbtn.parentElement
            var childLength = ttd.childNodes.length;
            var textValue;
            for (var i = 0; i < childLength; i++) {
                if (ttd.childNodes[i].type == 'text') {
                    textValue = ttd.childNodes[i].value;
                    ttd.childNodes[i].setAttribute("Style", "display:none;");
                    break;
                }

            }

            var ttr = ttd.parentElement;


            var PaymentHistoryID = ttd.childNodes[13].innerHTML; // ttr.cells[1].textContent;
            var lblChargeslipid = ttd.childNodes[15].innerHTML;

            var hdnChargeslipId = document.getElementById('<%=hdnChargeslipId.ClientID%>');
            hdnChargeslipId.value = lblChargeslipid;
            imgbtn.setAttribute("Style", "display:none;");
            var OldValue = ttd.childNodes[3].innerHTML;
            ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
            ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
            ttd.childNodes[7].setAttribute("Style", "display:inline-grid;");
            ttd.childNodes[9].setAttribute("Style", "display:none;");
            ttd.childNodes[11].setAttribute("Style", "display:none;");



            //if (textValue.trim() != '') {
            ttd.childNodes[5].innerHTML = textValue;
            $.ajax({
                async: true,
                type: "POST",
                url: "Payments.aspx/SavePaymentAmount",
                data: '{Paymentd: "' + PaymentHistoryID + '",Amount: "' + textValue.replace(/\$/g, '') + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //alert('p');
                    if (response.d == "") {
                        debugger;
                        document.getElementById('<%=btnRefreshPayment.ClientID%>').click();

                    }
                    else {
                        ttd.childNodes[5].innerHTML = response.d;
                        alert('Payment Amount cannot be greater than Commission Due');

                    }
                },
                failure: function (response) {

                    alert(response.d);
                    return false;
                }
            });








        }
        catch (ex) {
        }
        return false;
    }
    function imgCancelAmountClick(imgbtn) {
        var ttd = imgbtn.parentElement
        var childLength = ttd.childNodes.length;
        var textValue;
        for (var i = 0; i < childLength; i++) {
            if (ttd.childNodes[i].type == 'text') {

                ttd.childNodes[i].setAttribute("Style", "display:none;");
                break;
            }

        }
        imgbtn.setAttribute("Style", "display:none;");
        var OldValue = ttd.childNodes[3].innerHTML;
        ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
        ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
        ttd.childNodes[7].setAttribute("Style", "display:inline-grid;");
        ttd.childNodes[9].setAttribute("Style", "display:none;");
        ttd.childNodes[11].setAttribute("Style", "display:none;");
        return false;
    }
    function imgCancelClick(imgbtn) {
        try {
            var ttd = imgbtn.parentElement
            var childLength = ttd.childNodes.length;
            var textValue;
            for (var i = 0; i < childLength; i++) {
                if (ttd.childNodes[i].type == 'text') {
                    textValue = ttd.childNodes[i].value;
                    ttd.childNodes[i].setAttribute("Style", "display:none;");
                    break;
                }

            }
            imgbtn.setAttribute("Style", "display:none;");
            ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
            ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
            ttd.childNodes[7].setAttribute("Style", "display:none;");
        }
        catch (ex) {
        }
        return false;

    }
    function imgEditbtn_ClientClick(imgbtn) {
        try {

            var ttd = imgbtn.parentElement
            var childLength = ttd.childNodes.length;
            for (var i = 0; i < childLength; i++) {
                if (ttd.childNodes[i].type == 'text') {
                    ttd.childNodes[i].setAttribute("Style", "display:inline-grid;width:120px;");
                    ttd.childNodes[i].value = ttd.childNodes[3].innerHTML
                }

            }
            imgbtn.setAttribute("Style", "display:none;");
            ttd.childNodes[5].setAttribute("Style", "display:none;");
            ttd.childNodes[7].setAttribute("Style", "display:inline-grid;width:13px;height:13px;");
            ttd.childNodes[9].setAttribute("Style", "display:inline-grid;width:12px;height:12px;");
            ttd.childNodes[3].setAttribute("Style", "display:none;");

        }
        catch (exce) {
        }
        return false;

    }
    function imgSavebtn_ClientClick(imgbtn) {
        try {

            var ttd = imgbtn.parentElement
            var childLength = ttd.childNodes.length;
            var textValue;
            for (var i = 0; i < childLength; i++) {
                if (ttd.childNodes[i].type == 'text') {
                    textValue = ttd.childNodes[i].value;
                    ttd.childNodes[i].setAttribute("Style", "display:none;");
                    break;
                }

            }

            var ttr = ttd.parentElement;


            var PaymentHistoryID = ttd.childNodes[11].innerHTML; // ttr.cells[1].textContent;

            imgbtn.setAttribute("Style", "display:none;");
            var OldValue = ttd.childNodes[3].innerHTML;
            ttd.childNodes[3].setAttribute("Style", "display:inline-grid;");
            ttd.childNodes[5].setAttribute("Style", "display:inline-grid;");
            ttd.childNodes[7].setAttribute("Style", "display:none;");
            ttd.childNodes[9].setAttribute("Style", "display:none;");



            //if (textValue.trim() != '') {
            ttd.childNodes[3].innerHTML = textValue;
            $.ajax({
                type: "POST",
                url: "Customers.aspx/SaveBrokerCheckNo",
                data: '{PaymentHistoryId: "' + PaymentHistoryID + '",CheckNo: "' + textValue + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                failure: function (response) {
                    alert(response.d);
                }
            });
            //}
            //else {
            //    ttd.childNodes[3].innerHTML = OldValue;
            //}
        }
        catch (ex) {
        }
        return false;
    }
    </script>
    <script type="text/javascript">
        function ValidateForm() {
            var Amount = document.getElementById('<%=txtAmount.ClientID %>');
            if ($.trim(Amount.value) == "") {
                alert('Please Enter Some Amount');
                document.getElementById('<%=txtAmount.ClientID %>').focus();
                return false;
            }
            if (document.getElementById('<%=ddlPMTMethod.ClientID %>').selectedIndex == -1 || document.getElementById('<%=ddlPMTMethod.ClientID %>').selectedIndex == 0) {
                alert('Please Select PMT Method');

                return false;
            }
            if ($.trim(document.getElementById('<%=txtPMTDate.ClientID %>').value) == "") {
                alert('Please Enter  PMT Date');
                document.getElementById('<%=txtPMTDate.ClientID %>').focus();

                return false;
            }
            if (!isDate(document.getElementById('<%=txtPMTDate.ClientID %>'))) {

                return false;

            }
            //Modified by Shishir 05-05-2016
            if (document.getElementById('<%=ddlPMTMethod.ClientID %>').selectedIndex == 1) {
                if ($.trim(document.getElementById('<%=txtCheckno.ClientID %>').value) == "") {
                    alert('Please Enter  Checkno');
                    document.getElementById('<%=txtCheckno.ClientID %>').focus();

                    return false;
                }

            }

            return true;
        }
        function DispalySummary() {
            // alert('hi');
            debugger;
            document.getElementById('ContentMain_divDetailSummary').style.display = "block";
            Validate();
            // document.getElementById('ContentMain_divPaymentDetails').style.display = "none";

            //  mpePaymentDetail.hide();
            // return false;
        }
        function ChangeBalance() {
            debugger;
            var hdn = document.getElementById('<%=hdnTotalBalance.ClientID%>');
            var txtBalance = document.getElementById('<%=txtBalance.ClientID%>');
            var txtAmount = document.getElementById('<%=txtAmount.ClientID%>');
            var lblMessage = document.getElementById('<%=lblMessage.ClientID%>');
            lblMessage.innerHTML = "";
            if (hdn != null) {
                if (hdn.value != "") {
                    var balance = parseFloat(hdn.value) - parseFloat($.trim(txtAmount.value));
                    txtBalance.value = parseFloat(balance).toFixed(2);
                }
            }
            document.getElementById('ContentMain_divDetailSummary').style.display = "block";
            Validate();
            return false;

        }
    </script>
    <style type="text/css">
        /*body{
	 background:#202020;
	 font:bold 12px Arial, Helvetica, sans-serif;
	 margin:0;
	 padding:0;
	 min-width:960px;
	 color:#bbbbbb; 
}*/

        a {
            text-decoration: none;
            color: #00c6ff;
        }

        h1 {
            font: 4em normal Arial, Helvetica, sans-serif;
            padding: 20px;
            margin: 0;
            text-align: center;
        }

            h1 small {
                font: 0.2em normal Arial, Helvetica, sans-serif;
                text-transform: uppercase;
                letter-spacing: 0.2em;
                line-height: 5em;
                display: block;
            }

        h2 {
            font-weight: 700;
            color: #bbb;
            font-size: 20px;
        }

        h2, p {
            margin-bottom: 10px;
        }

        .container {
            width: 960px;
            margin: 0 auto;
            overflow: hidden;
            height: 910px;
        }

        .tooltip {
            display: none;
            position: absolute;
            border: 1px solid #333;
            background-color: #161616;
            border-radius: 5px;
            padding: 10px;
            color: #fff;
            font-size: 12px Arial;
        }
    </style>
    <style type="text/css">
        .loading {
            width: 250px;
        }

        .SqureButton1 {
            background-color: darkgrey;
        }
    </style>
    <script type="text/javascript">
        function ShowIcon(sender, e) {
            sender._element.className = "loading";
        }

        function hideIcon(sender, e) {
            sender._element.className = "textwidth";

        }
        function OnCenterSelected(source, eventArgs) {
            //alert(eventArgs.get_value());
            document.getElementById('<%=hdnCompId.ClientID %>').value = eventArgs.get_value();
            document.getElementById('<%=hdnSelected.ClientID %>').value = "Selected";
            // $find('AutoCompleteExtender2').set_contextKey(eventArgs.get_value());
            //hdnLocId JAY//   alert(eventArgs.get_value());
            return true;
        }


    </script>
    <script type="text/javascript">
        function Navigate() {

            var Index = 1;
            $.ajax({
                type: "POST",
                url: "Payments.aspx/GetChargeSlipDetails",
                data: '{RowIndx: "' + Index + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    window.open("../Pages/NewCharge.aspx", "_self");
                },
                failure: function (response) {
                    alert(response.d);
                }
            });

            return false;
        }
        //function PaymentPDFOpen(id) {  
        //    $.ajax({
        //        type: "POST",
        //        url: "Payments.aspx/PaymentPDFOpen",
        //        data: '{Paymentd: "' + id + '" }',
        //        contentType: "application/json; charset=utf-8",
        //        dataType: "json",
        //        success: function (response) {
        //            alert('hi');
        //            // window.open("../Pages/NewCharge.aspx", "_self");
        //        },
        //        failure: function (response) {
        //            alert(response.d);
        //        }
        //    });

        //    return false;
        //}
        function NavigateSplitCalculator() {

            var Index = 1;
            $.ajax({
                type: "POST",
                url: "Payments.aspx/GetChargeSlipDetails",
                data: '{RowIndx: "' + Index + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    window.open("../Pages/NewChargeCommissionSpitCalCulator.aspx", "_self");
                },
                failure: function (response) {
                    alert(response.d);
                }
            });

            return false;
        }
    </script>
    <script type="text/javascript">
        function CalculateBalance() {
            debugger;
            var hdn = document.getElementById('<%=hdnTotalBalance.ClientID%>');
            var Amount = document.getElementById('<%=txtAmount.ClientID%>');
            var Balance = document.getElementById('<%=txtBalance.ClientID%>');
            if (hdn != null) {
                if (hdn.value != null && hdn.value != "") {
                    if (hdn.value != "0") {
                        var balance = parseFloat(hdn.value) - parseFloat($.trim(Amount.value));

                        if (Amount.value == "") {
                            document.getElementById('<%=txtBalance.ClientID%>').value = parseFloat(hdn.value).toFixed(2);
                        }
                        else {
                            document.getElementById('<%=txtBalance.ClientID%>').value = parseFloat(balance).toFixed(2);
                        }
                    }
                }
            }
            Validate();

        }
    </script>
    <script type="text/javascript">
        function displayPayHistory() {

            document.getElementById('<%=divPaymentHistoryDetail.ClientID%>').style.display = "block";

            //alert(document.getElementById('<%=divPaymentHistoryDetail.ClientID%>'));

        }

        function HidePayHistory() {
            //debugger;
            document.getElementById('<%=divPaymentHistoryDetail.ClientID%>').style.display = "none";
        }
    </script>
    <script type="text/javascript">




        function Validate() {
            debugger;
            var count = 0;
            var Balance = 0;
            var Gross = 0;
            var PaidAmount = 0;
            var grid = document.getElementById('<%= grdCommission.ClientID %>');
            var hdnTotalBalance = document.getElementById('<%= hdnTotalBalance.ClientID %>');
            if (grid.rows.length > 0) {
                if (hdnTotalBalance != null) {
                    if (hdnTotalBalance.value > 0) {
                        Balance = parseFloat(hdnTotalBalance.value).toFixed(2);
                    }
                }
                for (var i = 0; i < grid.rows.length; i++) {

                    var chk = "ContentMain_grdCommission_chkSelect_" + i
                    var lblPaymentAmount = "ContentMain_grdCommission_lblPaymentAmount_" + i
                    var lblUNAPPLIED = "ContentMain_grdCommission_lblUNAPPLIED_" + i;
                    var lblCommAmountDue = "ContentMain_grdCommission_lblCommAmountDue_" + i;
                    var DealTransId = "ContentMain_grdCommission_hdnDealTransactionId_" + i;
                    var hdnChargeDate = document.getElementById('<%=hdnChargeDate.ClientID %>');
                    var hdnDealTransactionId = document.getElementById(DealTransId);
                    // alert(hdnDealTransactionId.value);
                    var hdnDealTransId = document.getElementById('<%= hdnDealTransId.ClientID %>');
                    if (hdnDealTransId != null) {
                        if (hdnDealTransactionId != null) {
                            if (hdnDealTransactionId.value != null) {
                                hdnDealTransId.value = hdnDealTransactionId.value;
                            }
                        }
                    }
                    var PaymentAmount = document.getElementById(lblPaymentAmount);
                    var UNAPPLIED = document.getElementById(lblUNAPPLIED);
                    var CommAmountDue = document.getElementById(lblCommAmountDue);


                    var checkboxSelect = document.getElementById(chk);
                    if (checkboxSelect != null) {
                        if (checkboxSelect.checked) {
                            count++;
                            var Amount = document.getElementById('<%=txtAmount.ClientID %>');
                            if ($.trim(Amount.value) == "") {
                                alert('Please Enter Some Amount');
                                document.getElementById('<%=txtAmount.ClientID %>').focus();
                                checkboxSelect.checked = false;
                                return false;
                            }
                            if (document.getElementById('<%=ddlPMTMethod.ClientID %>').selectedIndex == -1 || document.getElementById('<%=ddlPMTMethod.ClientID %>').selectedIndex == 0) {
                                alert('Please Select PMT Method');
                                checkboxSelect.checked = false;
                                return false;
                            }
                            if ($.trim(document.getElementById('<%=txtPMTDate.ClientID %>').value) == "") {
                                alert('Please Enter  PMT Date');
                                document.getElementById('<%=txtPMTDate.ClientID %>').focus();
                                checkboxSelect.checked = false;
                                return false;
                            }
                            if (!isDate(document.getElementById('<%=txtPMTDate.ClientID %>'))) {
                                checkboxSelect.checked = false;
                                return false;

                            }
                            //Modified by Shishir 05-05-2016
                            if (document.getElementById('<%=ddlPMTMethod.ClientID %>').selectedIndex == 1) {
                                if ($.trim(document.getElementById('<%=txtCheckno.ClientID %>').value) == "") {
                                    alert('Please Enter  Checkno');
                                    document.getElementById('<%=txtCheckno.ClientID %>').focus();
                                    checkboxSelect.checked = false;
                                    return false;
                                }

                            }



                            // if (hdnChargeDate != null)
                            // {
                            //   if (hdnChargeDate.value != "")
                            //  {
                            //     if (hdnChargeDate.value != "0") {
                            //        var PmtDate = document.getElementById('<%=txtPMTDate.ClientID %>');
                            //        if ($.trim(PmtDate.value) != "") {
                            //            if (Date.parse($.trim(PmtDate.value)) < Date.parse(hdnChargeDate.value)) {

                            //               alert('Payment Date cannot be less than ChargeSlip Createddate.');
                            //               PmtDate.value = "";
                            //              checkboxSelect.checked = false;

                            //             return false;
                            //         }
                            //     }
                            //   }
                            //  }
                            //  }
                            if (CommAmountDue != null) {
                                if (CommAmountDue.innerHTML != "") {
                                    //Gross += parseFloat((CommAmountDue.innerHTML).replace(",", ""));
                                    Gross += parseFloat(replaceAll(",", "", CommAmountDue.innerHTML));

                                }
                            }
                            if (PaymentAmount != null) {
                                if (PaymentAmount.innerHTML == "" || PaymentAmount.innerHTML == null) {

                                    PaidAmount = 0;
                                }
                                else {
                                    PaidAmount += parseFloat(replaceAll(",", "", PaymentAmount.innerHTML));
                                    //PaidAmount += parseFloat((PaymentAmount.innerHTML).replace(",", ""));

                                }
                            }


                        }
                    }
                }
                var lblAmountDueDet = document.getElementById('<%= lblAmountDueDet.ClientID %>');
                var lblPaymentAmountDet = document.getElementById('<%= lblPaymentAmountDet.ClientID %>');
                var lblUnappliedDet = document.getElementById('<%= lblUnappliedDet.ClientID %>');
                var lblRemainingBalanceDet = document.getElementById('<%= lblRemainingBalanceDet.ClientID %>');
                var lblPayAmountDue = document.getElementById('<%= lblPayAmountDue.ClientID %>');
                var lblPayAmount = document.getElementById('<%= lblPayAmount.ClientID %>');
                var txtAmount = document.getElementById('<%= txtAmount.ClientID %>');
                var lblRemBal = document.getElementById('<%= lblRemBal.ClientID %>');
                var hdnUnappliedDet = document.getElementById('<%= hdnUnappliedDet.ClientID %>');

                lblPayAmountDue.innerHTML = parseFloat(Gross).toFixed(2);
                lblPayAmount.innerHTML = parseFloat($.trim(txtAmount.value)).toFixed(2);

                lblAmountDueDet.innerHTML = "$ " + parseFloat(Gross).toFixed(2);
                lblPaymentAmountDet.innerHTML = "$ " + parseFloat($.trim(txtAmount.value)).toFixed(2);
                var Unapplied = Gross - PaidAmount - parseFloat($.trim(txtAmount.value));
                lblUnappliedDet.innerHTML = "$ " + parseFloat(Unapplied).toFixed(2);
                hdnUnappliedDet.value = parseFloat(Unapplied).toFixed(2);


                var RemainingBalance = Balance - parseFloat($.trim(txtAmount.value));
                lblRemainingBalanceDet.innerHTML = "$ " + parseFloat(RemainingBalance).toFixed(2);
                lblRemBal.innerHTML = parseFloat(RemainingBalance).toFixed(2);

                var hdnlblPayAmountDue = document.getElementById('<%= hdnlblPayAmountDue.ClientID %>');
                var hdnlblPayAmount = document.getElementById('<%= hdnlblPayAmount.ClientID %>');
                var hdnlblRemBal = document.getElementById('<%= hdnlblRemBal.ClientID %>');

                hdnlblPayAmountDue.value = parseFloat(Gross).toFixed(2);
                hdnlblPayAmount.value = parseFloat($.trim(txtAmount.value)).toFixed(2);
                hdnlblRemBal.value = parseFloat(RemainingBalance).toFixed(2);
                document.getElementById('<%=txtBalance.ClientID %>').value = parseFloat(RemainingBalance).toFixed(2);
            }
            if (count > 0) {
                document.getElementById('ContentMain_divDetailSummary').style.display = "block";
                return false;
            }
            else {
                document.getElementById('ContentMain_divDetailSummary').style.display = "none";
                //alert("Please Select atleast one checkbox.")
                return false;
            }
            //alert(id);
            //  alert(document.getElementById('<%=txtAmount.ClientID %>').value);

            return true;

        }
        function DisplaySummaryNone() {
            var sec = document.getElementById('ContentMain_divDetailSummary');
            if (sec != null) {
                sec.style.display = "none";
            }
        }

        function DisplaySummary(display) {



            var sec = document.getElementById('ContentMain_divDetailSummary');
            if (sec != null) {

                if (display == true) {

                    sec.style.display = "block";

                }
                else {

                    sec.style.display = "none";
                }
            }

        }
        function ClearControl() {
            document.getElementById('<%=txtAmount.ClientID %>').value = '';
            document.getElementById('<%=txtCheckno.ClientID %>').value = '';
            document.getElementById('<%=txtBalance.ClientID %>').value = '';
            document.getElementById('<%=txtPMTDate.ClientID %>').value = '';
            document.getElementById('<%=txtReceivedFrom.ClientID %>').value = '';
            document.getElementById('<%=ddlDealName.ClientID %>').selectedIndex = 0;

            document.getElementById('<%=ddlPMTMethod.ClientID %>').selectedIndex = 0;


            document.getElementById('ContentMain_lblMessage').innerHTML = "";
            document.getElementById('ContentMain_lblDealName').innerHTML = "";
            //document.getElementById('lblMessage').innerHTML = "";
            //$("#ContentMain_grdCommission").empty();

        }
    </script>
    <style type="text/css">
        .drpdown {
            width: 303px;
            height: 24px;
        }

        .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=80);
            opacity: 0.8;
        }

        .label {
            font-weight: bold;
            font-size: small;
            padding-left: 5px;
        }

        input[type="text"] {
            height: 18px;
            font-size: small;
        }
    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Tooltip only Text
            $('.masterTooltip').hover(function () {
                // Hover over code
                var title = $(this).attr('title');
                $(this).data('tipText', title).removeAttr('title');
                $('<p class="tooltip"></p>')
                .text(title)
                .appendTo('body')
                .fadeIn('slow');
            }, function () {
                // Hover out code
                $(this).attr('title', $(this).data('tipText'));
                $('.tooltip').remove();
            }).mousemove(function (e) {
                var mousex = e.pageX + 20; //Get X coordinates
                var mousey = e.pageY + 10; //Get Y coordinates
                $('.tooltip')
                .css({ top: mousey, left: mousex })
            });
        });
    </script>
    <asp:ScriptManager ID="scriptmanager2" runat="server"></asp:ScriptManager>
    <asp:HiddenField ID="hdnChargeDate" runat="server" Value="0" />
    <asp:HiddenField ID="hdnSelected" runat="server" />
    <table style="border: 1px solid black; width: 99%; margin-left: 5px;">
        <tr>
            <td colspan="4">
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="4">

                <asp:Button ID="btnAddNewPayment" runat="server" Text="NEW PAYMENT" CssClass="SqureButton" OnClick="btnAddNewPayment_Click" />
            </td>
        </tr>
        <tr>
            <td class="label">RECEIVED FROM</td>
            <td>
                <asp:TextBox ID="txtReceivedFrom" runat="server" Width="300px" CssClass="textboxlarge Marbottom3" AutoPostBack="true" OnTextChanged="txtReceivedFrom_TextChanged"></asp:TextBox>
                <ajax:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server"
                    TargetControlID="txtReceivedFrom"
                    DelimiterCharacters=";, :"
                    MinimumPrefixLength="1"
                    EnableCaching="true"
                    CompletionSetCount="10"
                    CompletionInterval="300"
                    ServiceMethod="SearchGetComapnyList"
                    OnClientPopulating="ShowIcon"
                    OnClientPopulated="hideIcon"
                    OnClientItemSelected="OnCenterSelected"
                    ShowOnlyCurrentWordInCompletionListItem="true"
                    CompletionListCssClass="AutoExtender"
                    CompletionListItemCssClass="AutoExtenderList"
                    CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                    CompletionListElementID="divwidth" />
                <asp:HiddenField ID="hdnCompId" runat="server" Value="" />
                <%--   <asp:DropDownList ID="ddlReceivedFrom" runat="server" CssClass="drpdown" AutoPostBack="true" OnSelectedIndexChanged="ddlReceivedFrom_SelectedIndexChanged"></asp:DropDownList>--%>
            </td>
            <td class="label">BALANCE</td>
            <td>
                <asp:TextBox ID="txtBalance" runat="server" Width="130px"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="height: 5px;" colspan="4"></td>
        </tr>
        <tr>
            <td class="label">DEAL NAME</td>
            <td>
                <%--  <asp:TextBox ID="txtDealName" runat="server" AutoPostBack="true"  OnTextChanged="txtDealName_TextChanged" Width="300px" CssClass="textboxlarge Marbottom3"></asp:TextBox>
                <ajax:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server"
                    TargetControlID="txtDealName"
                    DelimiterCharacters=";, :"
                    MinimumPrefixLength="1"
                    EnableCaching="true"
                    CompletionSetCount="10"
                    CompletionInterval="300"
                    ServiceMethod="SearchGetDealNameList"
                    OnClientPopulating="ShowIcon"
                    OnClientPopulated="hideIcon"
                    OnClientItemSelected="OnCenterDealNameSelected"
                    ShowOnlyCurrentWordInCompletionListItem="true"
                    CompletionListCssClass="AutoExtender"
                    CompletionListItemCssClass="AutoExtenderList"
                    CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                    CompletionListElementID="divwidth"
                    UseContextKey="true"
                    BehaviorID="testID" />
                <asp:HiddenField ID="hdnDealId" runat="server" Value="" />--%>
                <asp:DropDownList ID="ddlDealName" runat="server" CssClass="drpdown" AutoPostBack="true" OnSelectedIndexChanged="ddlDealName_SelectedIndexChanged"></asp:DropDownList></td>
            <td class="label">PMT DATE</td>
            <td>
                <asp:TextBox ID="txtPMTDate" runat="server" Width="130px"></asp:TextBox>
                <ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txtPMTDate" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="height: 5px;" colspan="4"></td>
        </tr>
        <tr>
            <td class="label">PMT AMOUNT</td>
            <td>
                <asp:TextBox ID="txtAmount" runat="server" Width="130px" onchange="CalculateBalance();"></asp:TextBox></td>
            <td class="label">CHECK NO</td>
            <td>
                <asp:TextBox ID="txtCheckno" runat="server" Width="80px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="height: 5px;" colspan="4"></td>
        </tr>
        <tr>
            <td class="label">PMT METHOD</td>
            <td>
                <asp:DropDownList ID="ddlPMTMethod" runat="server" Width="80px">
                    <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Check" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Wire" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="height: 5px;" colspan="4"></td>
        </tr>
        <tr>
            <td colspan="4" style="height: 10px;">
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="4" style="height: 10px;">
                <asp:Label ID="lblMessage" runat="server" Visible="false" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr id="trDealName" runat="server" visible="false">
            <td style="background-color: gray; height: 20px; padding-left: 5px;" colspan="4">
                <asp:Label ID="lblDealName" runat="server"></asp:Label>
            </td>
        </tr>

        <tr>
            <td colspan="4">

                <asp:UpdatePanel ID="upCommission" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="grdCommission" runat="server" ShowFooter="true" Width="100%" AutoGenerateColumns="false" FooterStyle-CssClass="FooterGridView2"
                            HeaderStyle-CssClass="HeaderGridView2" RowStyle-CssClass="RowGridView2" OnRowDataBound="grdCommission_RowDataBound" OnRowCommand="grdCommission_RowCommand">
                            <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnPaymentId" runat="server" Value='<%# Eval("PaymentId") %>' />
                                        <asp:HiddenField ID="hdnCommissionDueId" runat="server" Value='<%# Eval("CommissionDueId") %>' />
                                        <asp:HiddenField ID="hdnDealTransactionId" runat="server" Value='<%# Eval("DealTransactionId") %>' />
                                        <asp:HiddenField ID="hdnPaymentStatus" runat="server" Value='<%# Eval("PaymentStatus") %>' />
                                        <asp:CheckBox ID="chkSelect" runat="server" onchange="javascript:Validate();" />
                                        <asp:HiddenField ID="hdnIsFullCommPaid" runat="server" Value='<%# Eval("IsFullCommPaid") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Company" HeaderText="CUSTOMER" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="Name" HeaderText="COBROKERS" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DealType" HeaderText="DEAL TYPE" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="DueDate" HeaderText="DUE DATE" ItemStyle-HorizontalAlign="Center" />
                                <%--  <asp:BoundField DataField ="CommAmountDue" HeaderText ="GROSS" />--%>
                                <asp:TemplateField HeaderText="GROSS" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        $<asp:Label ID="lblCommAmountDue" runat="server" Text='<%#String.Format("{0:n2}",Eval("CommAmountDue")) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblFoCommAmountDue" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <%-- <asp:BoundField DataField="PaymentDate" HeaderText="PAID DATE" />--%>
                                <asp:TemplateField HeaderText="PAID DATE" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPaymentDate" runat="server" Text='<%# Eval("PaymentDate") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PAID AMOUNT" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        $<asp:Label ID="lblPaymentAmount" runat="server" Text='<%#String.Format("{0:n2}",Eval("CommissionPaid")) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblFoPaymentAmount" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UNAPPLIED" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        $<asp:Label ID="lblUNAPPLIED" runat="server"></asp:Label>
                                        <%-- Text='<%#String.Format("{0:n2}",Eval("BalanceAmount")) %>'--%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblFoUNAPPLIED" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CREDIT" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        $<asp:Label ID="lblCredit" runat="server" Text='<%#String.Format("{0:n2}",Eval("CreditAmount")) %>'></asp:Label>
                                        <%-- Text='<%#String.Format("{0:n2}",Eval("BalanceAmount")) %>'--%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblFoCredit" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <%--  <asp:BoundField DataField ="PaymentAmount" HeaderText ="PAID AMOUNT" />--%>
                                <%-- <asp:BoundField DataField="BalanceAmount" HeaderText="UNAPPLIED" />--%>
                                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ControlStyle-Width="10px">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgPayHistory" CommandName="PayHistory" CommandArgument='<%#Eval("ChargeSlipID") %>' Height="10px" Width="10px" ImageUrl="~/Images/Payment-Icon.gif" runat="server" />
                                    </ItemTemplate>
                                    <ControlStyle Width="10px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="right">
                <asp:UpdatePanel ID="upDetailSummary" runat="server">
                    <ContentTemplate>

                        <div id="divDetailSummary" runat="server" style="display: none; height: 120px; width: 343px; border: 1px solid black;">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="label">AMOUNT DUE</td>
                                    <td align="right">
                                        <asp:Label ID="lblAmountDueDet" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="label">PAYMENT AMOUNT</td>
                                    <td align="right">
                                        <asp:Label ID="lblPaymentAmountDet" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <hr style="width: 100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">UNAPPLIED</td>
                                    <td align="right">
                                        <asp:Label ID="lblUnappliedDet" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdnUnappliedDet" runat="server" Value="0" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">REMAINING BALANCE</td>
                                    <td align="right">
                                        <asp:Label ID="lblRemainingBalanceDet" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <br />
            </td>
        </tr>
        <tr id="trApplyPayment" runat="server">
            <%--<td colspan ="3" align="right">
                
            </td>--%>
            <td align="right" colspan="4">
                <%--<asp:Label ID="lblStatus" runat="server" Text="NOT APPLIED" ForeColor="Red"></asp:Label>--%>
                <asp:Button ID="btnApplyPayment" runat="server" Text="APPLY PAYMENT" CssClass="SqureButton" OnClientClick="return ValidateForm();" OnClick="btnApplyPayment_Click" />
                <asp:Button ID="btnClearAll" runat="server" Text="CLEAR ALL" CssClass="SqureButton" OnClick="btnClearAll_Click" />
                <ajax:ModalPopupExtender ID="mpePaymentDetail" runat="server" BackgroundCssClass="modalBackground" PopupControlID="divPaymentDetails" CancelControlID="imgbtnClose" TargetControlID="lnbPopUp"></ajax:ModalPopupExtender>
                <asp:LinkButton ID="lnbPopUp" runat="server"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <br />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnTotalBalance" runat="server" />
    <div id="divPaymentDetails" runat="server" class="PopupDivBodyStyle"
        style="border: 1px solid black; height: 750px; width: 840px; background-color: white; z-index: 111; position: absolute; display: none;">
        <table style="width: 100%;" class="spacing" id="formtest" runat="server">
            <tr style="background-color: gray; border-bottom: solid;">
                <td style="text-align: left; height: 25px;">
                    <div id="divHeader" runat="server">
                        <asp:Label ID="lblHeader" runat="server" Text="PAYMENT DETAIL" ForeColor="Black" Style="padding-left: 10px; vertical-align: middle; font-weight: bold;"></asp:Label>
                    </div>
                </td>
                <td style="text-align: right; padding-right: 10px; height: 25px;">
                    <asp:ImageButton ID="imgbtnClose" runat="server" ImageUrl="~/Images/Close.png" AlternateText="Close"
                        ForeColor="Red" ImageAlign="Right" Style="margin-top: 4px" OnClientClick="return ChangeBalance();" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="display: none; width: 100%;">
                    <div id="divBreakLine" runat="server">
                        ________________________________________________________________________________

                    </div>

                </td>
            </tr>
            <tr>
                <td style="padding-left: 10px; padding-top: 10px; vertical-align: bottom;">
                    <div id="divDealNames" runat="server">
                        <asp:Label ID="lblDealNames" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                </td>
                <td style="text-align: right">

                    <asp:Button ID="btnExporttoExcel" runat="server" Text="Export To Excel" Style="margin-right: 8px;" CssClass="SqureButton" OnClick="btnExporttoExcel_Click" />
                    <asp:Button ID="btnExporttoPDF" runat="server" Text="Export To PDF" Style="margin-right: 8px;" CssClass="SqureButton" OnClick="btnExporttoPDF_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr style="width: 98%" />
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <table style="width: 100%; font-size: 13px;" id="tblDealDetail" runat="server">
                        <tr>
                            <td style="padding-left: 15px; vertical-align: top;" valign="top">Received From:</td>
                            <td style="vertical-align: top;" valign="top">
                                <asp:Label ID="lblReceivedFrom" runat="server"></asp:Label></td>
                            <td colspan="2" align="right" style="padding-left: 0px;">
                                <div>
                                    <table class="OuterBorder">
                                        <tr>
                                            <td>AMOUNT DUE:</td>
                                            <td>$<asp:Label ID="lblPayAmountDue" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>PAYMENT AMOUNT:</td>
                                            <td>$<asp:Label ID="lblPayAmount" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="border: 1px solid black; background-color: black; height: 1px;"></td>

                                        </tr>
                                        <%--  <tr>
                                            <td colspan="2">
                                                <hr style="width: 98%;" />
                                            </td>

                                        </tr>--%>
                                        <tr>
                                            <td>REMAINING BALANCE:</td>
                                            <td>$
                                                <asp:Label ID="lblRemBal" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td style="padding-left: 15px; width: 18%;">Pmt Date:</td>
                            <td align="left">
                                <asp:Label ID="lblPmtDate" runat="server"></asp:Label></td>
                            <td colspan="2" align="right" style="padding-left: 0px;">
                                <table>
                                    <tr>
                                        <td style="padding-left: 0px;">Pmt Method:</td>
                                        <td align="right">
                                            <asp:Label ID="lblPmtMethod" runat="server"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>

                        </tr>
                        <tr>
                            <td style="padding-left: 15px;">New Payment:</td>
                            <td align="left">
                                <asp:Label ID="lblNewPayment" runat="server"></asp:Label></td>
                            <td colspan="2" align="right" style="padding-left: 0px;">
                                <table>
                                    <tr>
                                        <td style="padding-left: 0px;">Check Number:</td>
                                        <td align="right">
                                            <asp:Label ID="lblCheckNo" runat="server"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr style="width: 98%" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-left: 20px;">
                    <div id="divPaymDis" runat="server">
                        Payment Distribution:
                    </div>

                </td>
            </tr>
            <tr>
                <td>
                    <div id="divBlankForpdf1" runat="server">
                        <br />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-left: 20px;">
                    <div style="overflow: auto; width: 810px; height: 160px;">
                        <asp:GridView ID="grdPaymentDetail" runat="server" ShowFooter="true" Width="98%" AutoGenerateColumns="false" FooterStyle-CssClass="FooterGridView2"
                            HeaderStyle-CssClass="HeaderGridView2" RowStyle-CssClass="RowGridView2" OnRowDataBound="grdPaymentDetail_RowDataBound">
                            <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                            <Columns>

                                <asp:BoundField DataField="Name" HeaderText="VENDOR" ItemStyle-HorizontalAlign="Left" />
                                <%--Modified by Shishir 04-05-2016--%>
                                <%--<asp:TemplateField HeaderText="VENDOR" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label><br />
                                        <asp:Label ID="Label1" runat="server" Text="50"></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblPayDetName" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>--%>


                                <asp:BoundField DataField="BrokerType" HeaderText="ROLE" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="COMMISSION DUE" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        $
                                    <asp:Label ID="lblCommissionDue" runat="server" Text='<%#Eval("CommissionDue") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblPayDetCommissionDue" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <%-- <asp:BoundField DataField ="BrokerPercent" HeaderText ="% DUE" />--%>
                                <asp:TemplateField HeaderText="% DUE" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBrokerPercent" runat="server" Text='<%#Eval("BrokerPercent") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblPayDetBrokerPercent" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="RECEIVED" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        $<asp:Label ID="lblCommissionReceived" runat="server" Text='<%#Eval("CommissionReceived") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblPayDetCommissionReceived" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="NEW PAYMENT" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        $<asp:Label ID="lblNewCommPayment" runat="server" Text='<%#Eval("NewCommPayment") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblPayDetNewCommPayment" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UNPAID AMT" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        $<asp:Label ID="lblUnPaidAmount" runat="server" Text='<%#Eval("UnPaidAmount") %>'></asp:Label>
                                        <asp:HiddenField ID="hdnBrokerId" runat="server" Value='<%# Eval("BrokerId") %>' />
                                        <asp:HiddenField ID="hdnTermTypeId" runat="server" Value='<%# Eval("TermTypeId") %>' />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblPayDetUnPaidAmount" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SHARE" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        $<asp:Label ID="lblShare" runat="server" Text='<%#Eval("ShareAmount") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblPayShare" runat="server" Enabled="false" Style="text-align: right;"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CO CHECK NO" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtCoCheckNo" runat="server" Text="" Width="70px" Style="text-align: right;"></asp:TextBox>
                                    </ItemTemplate>
                                    <%--     <ItemTemplate>
                                   
                                       $<asp:Label ID="lbItemRentPSP" runat="server" Text='<%#String.Format("{0:n2}",Eval("Rent_PSF")) %>' Style="text-align: right;"></asp:Label>
                                    </ItemTemplate>--%>

                                </asp:TemplateField>
                                <%--<asp:BoundField DataField ="CommissionDue" HeaderText ="COMMISSION DUE" />
                            
                             <asp:BoundField DataField ="CommissionReceived" HeaderText ="RECEIVED" />
                            <asp:BoundField DataField ="NewCommPayment" HeaderText ="NEW PAYMENT" />
                             <asp:BoundField DataField ="UnPaidAmount" HeaderText ="UNPAID AMT" />--%>
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divBlankForpdf2" runat="server">
                        <br />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-left: 20px;">
                    <div style="overflow: auto; width: 810px; height: 150px;">
                        <asp:GridView ID="grdPaymentDistributionCoBrokerDetail" runat="server" Width="98%" AutoGenerateColumns="false"
                            HeaderStyle-CssClass="HeaderGridView2" RowStyle-CssClass="RowGridView2">
                            <AlternatingRowStyle CssClass="AlternatingRowGridView2" />
                            <Columns>
                                <asp:TemplateField HeaderText="Co-Broker and Clb-Broker Payment Split Detail">
                                    <ItemTemplate>
                                        <table style="width: 100%;" border="0">
                                            <tr>
                                                <td colspan="2" style="font-weight: bold;">
                                                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                    ( 
                                                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("BrokerType") %>'></asp:Label>
                                                    )
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;" border="0">
                                                        <tr>
                                                            <td>YTD Collected:</td>

                                                        </tr>
                                                        <tr>
                                                            <td>Current Split:   
                                                                <asp:Label ID="lblCurrentSplit" runat="server" Text='<%# Eval("CurrentSplit1") %>'></asp:Label></td>

                                                        </tr>
                                                        <tr>
                                                            <td>Remaining needed till next Tier: 
                                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("TotalSplitAmount1") %>'></asp:Label></td>

                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align: right;">
                                                    <table style="width: 100%;" border="0">
                                                        <tr>
                                                            <td style="text-align: right;">
                                                                <asp:Label ID="lblNewCommPayment" runat="server" Text='<%#Eval("TotalSplitAmount1") %>'></asp:Label>
                                                            </td>
                                                            <td style="text-align: right;">
                                                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("CurrentSplit1") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label3" runat="server" Text="="></asp:Label>
                                                            </td>
                                                            <td style="text-align: right;">
                                                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("ShareAmount1") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: right;">
                                                                <asp:Label ID="Label5" runat="server" Text='<%#Eval("TotalSplitAmount2") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label6" runat="server" Text='<%#Eval("CurrentSplit2") %>'></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label7" runat="server" Text='<%#Eval("EqualTo") %>'></asp:Label>
                                                            </td>
                                                            <td style="text-align: right;">
                                                                <asp:Label ID="Label8" runat="server" Text='<%#Eval("ShareAmount2") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="text-align: right;"></td>
                                                            <td>
                                                                <asp:Label ID="Label12" runat="server" Text='<%#Eval("TotalShareAmount") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divBlankForpdf3" runat="server">
                        <br />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">

                    <div style="width: 62%; margin-right: 10px; border: 1px solid; border-radius: 8px;">
                        <table style="width: 100%;" id="tblRemainingDet" runat="server">

                            <tr>
                                <td style="width: 200px;">Remaining Balance:</td>
                                <td align="right">
                                    <asp:Label ID="lblRemainingBalanceCredit" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px;">Credit:</td>
                                <td align="right">
                                    <asp:Label ID="lblCreditAmount" runat="server" Text="$ 0.00"></asp:Label>
                                </td>
                            </tr>

                            <tr>
                                <td align="left">
                                    <input type="button" id="btnCreditRemBalance" style="border-radius: 4px;" value="CREDIT REMAINING BALANCE" onclick="return SetBalance(this);" /></td>
                                <td align="right">
                                    <asp:HiddenField ID="hdnBalanceSelected" runat="server" Value="" />
                                    <input type="button" id="btnLeaveRemBalance" style="border-radius: 4px;" value="LEAVE REMAINING BALANCE" onclick="return SetBalance(this);" /></td>

                                <%--  <td ><asp:Button ID="btnLeaveRemBalance" runat ="server" Text ="LEAVE REMAINING BALANCE" /></td>--%>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right" style="padding-right: 20px;">
                    <asp:HiddenField ID="hdnDealTransId" runat="server" />
                    <asp:HiddenField ID="hdnlblPayAmountDue" runat="server" />
                    <asp:HiddenField ID="hdnlblPayAmount" runat="server" />
                    <asp:HiddenField ID="hdnlblRemBal" runat="server" />
                    <asp:Button ID="btnApply" runat="server" Text="APPLY" CssClass="SqureButton" OnClick="btnApply_Click" OnClientClick="return ValidateApplyPayment();" />
                    <asp:Button ID="btnCancel" runat="server" Text="CANCEL" CssClass="SqureButton" OnClick="btnCancel_Click" />
                    <br />
                    <asp:TextBox ID="asdfasdf" runat="server" Text="asdfasdf" Visible="false"></asp:TextBox>
                    <%--  <br />--%>
                </td>
            </tr>
        </table>

    </div>
    <asp:HiddenField ID="hdnChargeslipId" runat="server" Value="0" />
    <asp:Button ID="btnRefreshPayment" runat="server" Text="Save Payment Amount" OnClick="btnRefreshPayment_Click" Style="display: none;" />
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <div id="divPaymentHistoryDetail" runat="server" style="display: none;">
                <uc1:ucPaymentHistoryDetail ID="ucPaymentHistoryDetail1" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" runat="Server">
    <%-- <table style="width: 100%; background-color: gray;">
        <tr>
           
            <td style="width: 50%; text-align: right;">
                <asp:Button ID="btnSaveAndNew" runat="server" Text="SAVE & NEW" CssClass="SqureButton" OnClick="btnSaveAndNew_Click" />
                <asp:Button ID="btnSaveAndClose" runat="server" Text="SAVE & CLOSE" CssClass="SqureButton" OnClick="btnSaveAndClose_Click" />
                <asp:Button ID="btnClear" runat="server" Text="CLEAR" CssClass="SqureButton" OnClientClick="ClearControl()" />
            </td>
        </tr>
    </table>--%>
</asp:Content>

