﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SettingMaster.master" AutoEventWireup="true" CodeFile="ChargeTo.aspx.cs" Inherits="Admin_ChargeTo" %>

<%@ Register Src="~/UserControl/ucAddNewShoppingCenterOrCompany.ascx" TagName="AddNewShoppingCenterOrCompany" TagPrefix="uc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../Js/CommonValidations.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      <script type="text/ecmascript">
          function ClearControl() {

              $('#<%=txtCompany.ClientID %>').val("");
            $('#<%=txtCity.ClientID%>').val("");
            $('#<%=txtZipCode.ClientID%>').val("");
            document.getElementById('<%=ddlStatesearch.ClientID %>').selectedIndex = -1

        }
    </script>
    <script type="text/javascript">
        function Validate(event) {
            var regex = new RegExp("^[0-9 \-]+$");
            var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        }
        function SetActive(ChargesToid) {
            //alert(locationid );
            $.ajax({
                type: "POST",
                url: "ChargeTo.aspx/SetActive",
                data: '{ChargeToid: "' + ChargesToid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                failure: function (response) {
                    alert(response);
                }
            });
            return false;
        }
    </script>
    <style type="text/css">
        .removeunderline {
            text-decoration: none;
        }

        .linkbut {
            color: black;
            font-weight: bold;
            text-decoration: none;
        }

        .lnkMsg {
            color: black;
            text-decoration: none;
        }
    </style>
    <table width="100%">

        <tr>
            <td>
                <br />
            </td>

        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>Company:</td>
                        <td>
                            <asp:TextBox ID="txtCompany" runat="server"></asp:TextBox></td>

                        <td>State:</td>
                        <td>
                            <asp:DropDownList ID="ddlStatesearch" runat="server">
                            </asp:DropDownList>
                            <%-- <asp:TextBox ID="txtLocationSearchState" runat="server"></asp:TextBox>--%>
                        </td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Height="20px" Text="SEARCH" CssClass="SqureButton" OnClick="btnSearch_Click" />
                            <asp:Button ID="btnclear" runat="server" Text="RESET" CssClass="SqureButton" OnClientClick="ClearControl();" OnClick="btnclear_Click" />

                        </td>
                    </tr>
                    <tr>
                        <td>City:</td>
                        <td>
                            <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                        </td>
                        <td>ZipCode: </td>
                        <td>
                            <asp:TextBox ID="txtZipCode" runat="server" onkeypress="return Validate(event);"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
        <%--   <tr>
            <td>
                <br />
            </td>

        </tr>--%>
        <%-- <tr>
            <td>
                <asp:Button ID="btnChargeTo" runat="server" Text="ADD CHARGE TO" OnClick="btnChargeTo_Click" CssClass="SqureButton" />
            </td>
        </tr>--%>
        <tr>
            <td>
                <table width="99%">
                    <tr>
                        <td align="left">

                            <%--  <asp:Label ID="lblTotalRecord" Text="" runat="server" Font-Bold="true"></asp:Label>--%>

                        </td>
                        <td align="right">
                            <asp:Button ID="btnChargeTo" runat="server" Text="ADD CHARGE TO" OnClick="btnChargeTo_Click" CssClass="SqureButton" />
                        </td>
                        <%--  <td align="right">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="lblPages" runat="server" Font-Bold="true"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>


                        </td>--%>
                    </tr>
                </table>
            </td>

        </tr>
        <tr>
            <td>

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="grdChargeTo" runat="server" AutoGenerateColumns="false" Width="99%"
                            OnRowCommand="grdChargeTo_RowCommand" OnRowDataBound="grdChargeTo_RowDataBound"
                            EmptyDataRowStyle-HorizontalAlign="Center" AllowPaging="true"
                            PageSize="25" OnPageIndexChanging="grdChargeTo_PageIndexChanging"
                            OnRowCreated="grdChargeTo_RowCreated"
                            HeaderStyle-CssClass="HeaderGridView" FooterStyle-CssClass="FooterGridView"
                            RowStyle-CssClass="RowGridView" AlternatingRowStyle-CssClass="AlternatingRowGridView2">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="ACTIVE" ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkActive" runat="server" Height="15px" Width="15px" CommandArgument='<%# Eval("CompanyID") %>' onchange='<%# Eval("CompanyID","return SetActive({0})") %>' />
                                        <asp:HiddenField ID="hdnActive" runat="server" Value='<% #Bind("IsActive")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Company" HeaderText="COMPANY" />
                                <asp:BoundField DataField="Address1" HeaderText="ADDRESS" />
                                <asp:BoundField DataField="City" HeaderText="City" />
                                <asp:BoundField DataField="State" HeaderText="State" />
                                <asp:BoundField DataField="ZipCode" HeaderText="ZipCode" />
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnCompanyID" runat="server" Value='<%# Eval("CompanyID") %>' />
                                        <asp:ImageButton runat="server" ImageUrl="~/Images/edit.jpg" ID="btnEdit" CommandName="Ed" CommandArgument='<%# Eval("CompanyID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnCanDel" runat="server" Value='<%# Eval("CanDelete") %>' />
                                        <asp:ImageButton runat="server" ImageUrl="~/Images/delete.gif" OnClientClick='javascript:return confirm("Do You Want To Delete This Record");' ID="delRecord" CommandName="Del" CommandArgument='<%# Eval("CompanyID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                            </PagerTemplate>
                            <PagerStyle CssClass="pagerStyle" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>

    <div id="divNwCenterOrCompany" class="PopupDivBodyStyle" style="height: auto; display: none;">
        <table style="width: 100%" class="spacing">
            <tr style="background-color: gray; border-bottom: solid;">
                <td style="text-align: left;">
                    <asp:UpdatePanel ID="upTitile" runat="server">
                        <ContentTemplate>
                            <h3>&nbsp; &nbsp;      
                                <asp:Label ID="lbTitle" runat="server"></asp:Label></h3>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="right">
                    <asp:ImageButton ImageUrl="~/Images/close.png" ID="btnCancel" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc:AddNewShoppingCenterOrCompany ID="ucNewCenterOrCompany" runat="server" />

                </td>
            </tr>
        </table>

    </div>
    <ajax:ModalPopupExtender ID="mpeAddNewComapny" TargetControlID="lbDummy" CancelControlID="btnCancel"
        PopupControlID="divNwCenterOrCompany" runat="server" BackgroundCssClass="modalBackground">
    </ajax:ModalPopupExtender>
    <asp:Label ID="lbDummy" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" runat="Server">
</asp:Content>

