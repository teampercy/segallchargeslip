﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using SegallChargeSlipBll;

public partial class Admin_CSSentToEmails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData();
        }
    }
    private void LoadData()
    {
        CSSentToEmails obj = new CSSentToEmails();
        DataSet ds = new DataSet();
        ds = obj.GetList();
        DataTable dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            btnSave.Text = "UPDATE";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["SentToType"].ToString() == "ACCOUNTING")
                {
                    txtAccounting.Text = dt.Rows[i]["EmailId"].ToString();
                }
                else if(dt.Rows[i]["SentToType"].ToString()=="ACCOUNTING2")
                {
                    txtAccounting2.Text = dt.Rows[i]["EmailId"].ToString();
                }
                else if (dt.Rows[i]["SentToType"].ToString() == "ADMIN1")
                {
                    txtAdmin1.Text = dt.Rows[i]["EmailId"].ToString();
                }
                else if (dt.Rows[i]["SentToType"].ToString() == "ADMIN2")
                {
                    txtAdmin2.Text = dt.Rows[i]["EmailId"].ToString();
                }
                else if (dt.Rows[i]["SentToType"].ToString() == "NEWSADMIN")
                {
                    txtNewsAdmin.Text = dt.Rows[i]["EmailId"].ToString();
                }
                else if (dt.Rows[i]["SentToType"].ToString() == "NEWSADMIN1")
                {
                    txtNewsAdmin2.Text = dt.Rows[i]["EmailId"].ToString();
                }
                else if (dt.Rows[i]["SentToType"].ToString() == "NEWSADMIN2")
                {
                    txtNewsAdmin3.Text = dt.Rows[i]["EmailId"].ToString();
                }
                else if (dt.Rows[i]["SentToType"].ToString() == "NEWSADMIN3")
                {
                    txtNewsAdmin4.Text = dt.Rows[i]["EmailId"].ToString();
                }
                else if (dt.Rows[i]["SentToType"].ToString() == "NEWSADMIN4")
                {
                    txtNewsAdmin5.Text = dt.Rows[i]["EmailId"].ToString();
                }
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            CSSentToEmails obj = new CSSentToEmails();
            obj.InsertEmail(txtAccounting.Text.Trim(), txtAccounting2.Text.Trim(), txtAdmin1.Text.Trim(), txtAdmin2.Text.Trim(), txtNewsAdmin.Text.Trim(), txtNewsAdmin2.Text.Trim(), txtNewsAdmin3.Text.Trim(), txtNewsAdmin4.Text.Trim(), txtNewsAdmin5.Text.Trim());
            lblMessage.Text = "Record Saved Successfully.";
           // ScriptManager.RegisterStartupScript(this, this.GetType(), "Record", "ShowMessage();", true);

            //DataSet ds = new DataSet();
            //ds = obj.GetListBySentToType();
            //DataTable dtAccounting = ds.Tables[0];
            //DataTable dtAdmin1 = ds.Tables[1];
            //DataTable dtAdmin2 = ds.Tables[2];
            //if (dtAccounting.Rows.Count > 0)
            //{
            //    obj.CSSentToEmailId = Convert.ToInt32(dtAccounting.Rows[0]["CSSentToEmailId"]);
            //    obj.EmailId = txtAccounting.Text;
            //    obj.Save();
            //}
            //else
            //{
            //    obj.SentToType = "ACCOUNTING";
            //    obj.EmailId = txtAccounting.Text;
            //    obj.Save();
            //}
            //if (dtAdmin1.Rows.Count > 0)
            //{
            //    obj.CSSentToEmailId = Convert.ToInt32(dtAdmin1.Rows[0]["CSSentToEmailId"]);
            //    obj.EmailId = txtAdmin1.Text;
            //    obj.Save();
            //}
            //else
            //{
            //    obj.SentToType = "ADMIN1";
            //    obj.EmailId = txtAdmin1.Text;
            //    obj.Save();
            //}
            //if (dtAdmin2.Rows.Count > 0)
            //{
            //    obj.CSSentToEmailId = Convert.ToInt32(dtAdmin2.Rows[0]["CSSentToEmailId"]);
            //    obj.EmailId = txtAdmin2.Text;
            //    obj.Save();
            //}
            //else
            //{
            //    obj.SentToType = "ADMIN2";
            //    obj.EmailId = txtAdmin2.Text;
            //    obj.Save();
            //}
            
        }

        catch (Exception ex)
        {
            lblMessage.Text = "Record failed to Save Successfully.";
            ErrorLog.WriteLog("Admin/CSSentToEmails.aspx", "btnSave_Click", ex.Message);
        }
    }
}