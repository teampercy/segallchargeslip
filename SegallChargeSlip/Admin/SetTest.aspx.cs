﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_SetTest : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDealTypes();
            BindSubDealTypes();
        }
    }

    public void BindDealTypes()
    {
        int selectedValue = 0;
        if (ddlDealTypesMST.SelectedIndex > 0 && ddlDealTypesMST.Items.Count > 0)
        {
            selectedValue = Convert.ToInt32(ddlDealTypesMST.SelectedValue);
        }
        int dealTypesParentselectedValue = 0;
        if (ddlDealTypesParentMST.SelectedIndex > 0 && ddlDealTypesParentMST.Items.Count > 0)
        {
            dealTypesParentselectedValue = Convert.ToInt32(ddlDealTypesParentMST.SelectedValue);
        }


        DealTypes objdealType = new DealTypes();
        ddlDealTypesMST.ClearSelection();
        Utility.FillDropDown(ref ddlDealTypesMST, objdealType.GetList().Tables[0], "DealType", "DealTypeID"); //Fill Deal Type
        Utility.FillDropDown(ref ddlDealTypesParentMST, objdealType.GetList().Tables[0], "DealType", "DealTypeID"); //Fill Deal Type

        // Deal types drop down
        if (selectedValue != 0)
        {
            ddlDealTypesMST.ClearSelection();
            if (ddlDealTypesMST.Items.FindByValue(selectedValue.ToString()) != null)
            {
                ddlDealTypesMST.Items.FindByValue(selectedValue.ToString()).Selected = true;
            }
            else
            {
                ddlDealTypesMST.SelectedIndex = 0;
            }
        }

        // Deal types for SUBTYPE
        if (dealTypesParentselectedValue != 0)
        {
            ddlDealTypesParentMST.ClearSelection();
            if (ddlDealTypesParentMST.Items.FindByValue(dealTypesParentselectedValue.ToString()) != null)
            {
                ddlDealTypesParentMST.Items.FindByValue(dealTypesParentselectedValue.ToString()).Selected = true;
            }
            else
            {
                ddlDealTypesParentMST.SelectedIndex = 0;
            }          
        }

    }

    public void BindSubDealTypes()
    {
        if (ddlDealTypesParentMST.SelectedIndex > 0)
        {
            int linkedDealType = 0;
            linkedDealType = Convert.ToInt32(ddlDealTypesParentMST.SelectedValue);
            int selectedValueDealSubTypesParentMST = 0;
            if (ddlDealTypesParentMST.SelectedIndex > 0 && ddlDealTypesParentMST.Items.Count > 0)
            {
                selectedValueDealSubTypesParentMST = Convert.ToInt32(ddlDealTypesParentMST.SelectedValue);
            }

            DealSubTypes objdealSubType = new DealSubTypes();
            objdealSubType.DealTypeID = linkedDealType;
            ddlDealSubTypesParentMST.ClearSelection();
            Utility.FillDropDown(ref ddlDealSubTypesParentMST, objdealSubType.GetList().Tables[0], "DealSubType", "DealSubTypeID"); //Fill Deal Type

        }
        else
        {
            ddlDealSubTypesParentMST.ClearSelection();
        }

    }


    //popup for add,edit, delete DEAL TYPES
    protected void btnDealTypesManage_Click(object sender, ImageClickEventArgs e) //ImageClickEventArgs e
    {
        ucAddDealTypes.DealTypeMode = "M";
        ucAddDealTypes.BindGrid(0);
        mpDealTypesManager.Show();
    }
    //popup for add,edit, delete DEAL SUB TYPES
    protected void btnSubDealTypesManage_Click(object sender, ImageClickEventArgs e) //ImageClickEventArgs e
    {
        ucAddSubDealTypes.DealTypeMode = "M";
        ucAddSubDealTypes.BindControls();
        ucAddSubDealTypes.BindGrid(0);
        mpSubDealTypesManager.Show();
    }
    protected void ddlDealTypesParentMST_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindSubDealTypes();
    }
}