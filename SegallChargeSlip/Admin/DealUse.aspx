﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/SettingMaster.master" AutoEventWireup="true" CodeFile="DealUse.aspx.cs" Inherits="Admin_DealUse" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
     <script type ="text/javascript" >
         function Validate() {
             var UseType = document.getElementById('<%=txtUse.ClientID%>');
            if ($.trim(UseType.value) == "") {
                alert('Please Enter Use Type');
                UseType.focus();
                return false;
            }
            return true;
         }
         function SetActive(DealUseId) {
             //alert(locationid );
             $.ajax({
                 type: "POST",
                 url: "DealUse.aspx/SetActive",
                 data: '{UseId: "' + DealUseId + '"}',
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 failure: function (response) {
                     alert(response);
                 }
             });
             return false;
         }
         
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <table style="width: 97%;">
        <tr>
            <td align="right">
                <asp:Button ID="btnUse" runat="server" Text="ADD USE" OnClick="btnUse_Click" CssClass="SqureButton" />
                <%--  <asp:LinkButton ID="lnkAddDealType" runat ="server" Text="AddDealType" OnClick ="lnkAddDealType_Click"></asp:LinkButton></td></tr>--%>
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="grdUse" runat="server" AutoGenerateColumns="false" Width="100%"
                    OnRowCommand="grdUse_RowCommand" OnRowDataBound ="grdUse_RowDataBound"
                    EmptyDataRowStyle-HorizontalAlign="Center"
                    CssClass="AllBorder"
                    HeaderStyle-CssClass="HeaderGridView" FooterStyle-CssClass="FooterGridView"
                    RowStyle-CssClass="RowGridView" AlternatingRowStyle-CssClass="AlternatingRowGridView2">
                    <Columns>
                        <asp:TemplateField  ItemStyle-HorizontalAlign ="Center" HeaderText="ACTIVE" ItemStyle-Width ="20px">
                            <ItemTemplate >
                                <asp:CheckBox ID="chkActive" runat ="server" Height="15px" Width ="15px" CommandArgument='<%# Eval("DealUseId") %>' onchange='<%# Eval("DealUseId","return SetActive({0})") %>'/>
                                <asp:HiddenField ID="hdnActive" runat ="server" Value='<% #Bind("IsActive") %>'/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DealUseType" HeaderText="USE" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnUseId" runat="server" Value ='<%# Eval("DealUseId") %>'/>
                                <asp:ImageButton runat="server" ImageUrl="~/Images/edit.jpg" ID="btnEdit" CommandName="Ed" CommandArgument='<%# Eval("DealUseId") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                 <asp:HiddenField ID="hdnCanDel" runat="server" Value='<%# Eval("CanDelete") %>' />
                                <asp:ImageButton runat="server" ImageUrl="~/Images/delete.gif" OnClientClick='javascript:return confirm("Do You Want To Delete This Record");' ID="delRecord" CommandName="Del" CommandArgument='<%# Eval("DealUseId") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <ajax:ModalPopupExtender ID="mpeUpdateUse" TargetControlID="lbDummy" CancelControlID="imgbtnClose"
        PopupControlID="divPopUp" runat="server" BackgroundCssClass="modalBackground">
    </ajax:ModalPopupExtender>
    <asp:Label ID="lbDummy" runat="server"></asp:Label>
    <div id="divPopUp" class="PopupDivBodyStyle" style="width: 350px; height: 200px; overflow: auto; background-color: white; z-index: 111; position: absolute; display: block;">
        <table style="width: 100%">
            
            <tr style="background-color: gray; border-bottom: solid;width:100%;">
                <td colspan="2">
                    <table>
                        <tr>
                            <td style="text-align: left; width: 490px;">
                                <asp:Label ID="lblHeader" runat="server" Text="USE" ForeColor="Black" Style="padding-left: 10px; vertical-align: middle; font-weight: bold;"></asp:Label>
                            </td>
                            <td style="text-align: right;">
                                <asp:ImageButton ID="imgbtnClose" runat="server" ImageUrl="~/Images/Close.png" AlternateText="Close"
                                    ForeColor="Red" ImageAlign="Right" Style="margin-top: 4px" />
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="Paddingtd">
                    <asp:Label ID="lblErrorMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 20px;">Use Type</td>
                <td>
                    <asp:TextBox ID="txtUse" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
              <tr>
                <td style="padding-left: 20px;">IsActive</td>
                <td>
                   <asp:CheckBox ID="chkIsActive" runat ="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr><td></td>
                <td >
                    <asp:Button ID="btnUpdate" runat="server" Text="ADD" CssClass="SqureButton" Width ="100px" OnClick="btnUpdate_Click" OnClientClick="return Validate();"/>
                </td>
            </tr>
        </table>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentFooter" Runat="Server">
</asp:Content>

