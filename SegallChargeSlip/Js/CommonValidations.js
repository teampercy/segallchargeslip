﻿$(document).ready(function () {

    $(".onlyNumbers").keydown(function (event) {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey == true) || (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        }
        else {

            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

});

$(document).ready(function () {

    $(".onlyDecimal").keydown(function (event) {

        if (event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39)) {
            alert(event.keyCode);
            return;
        } else {

            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });
    //$(".EmailValidation").blur(function () {
    //    alert("test1");

    //    EmailValidation($(this).val());
    //});
});

//Function Check For Integer Only 
function AllowIntergers(CtrId) {
   
    var regExp = /^\s*(\+|-)?\d+\s*$/;
    var Input = document.getElementById(CtrId).value;
    if (!regExp.test(Input)) {
        document.getElementById(CtrId).value = "";
    }
    else {
        return true;
    }

    return false;
}

// Function Check For Decimal No only
function AllowDecimals(CtrId) {
   // alert('vijaysa');
   // alert(CtrId);
    var regExp = /^\d+(\.\d{0,9})?$/;
    var Input = document.getElementById(CtrId).value;
    if (!regExp.test(Input)) {
        document.getElementById(CtrId).value = "";
    }
    else {
        return true;
    }

    return false;
}
function AllowDecimalsNet(CtrId) {
    // alert('vijaysa');
    // alert(CtrId);
    var regExp = /^\d*\.?\d*$/
    var Input = document.getElementById(CtrId).value;
    if (!regExp.test(Input)) {
        document.getElementById(CtrId).value = "";
    }
    else {
        return true;
    }

    return false;
}

function SelectAllGridCheckboxes(headerchk, GridName) {
    debugger;
    var gvcheck = document.getElementById(GridName);
    var i;
    //Condition to check header checkbox selected or not if that is true checked all checkboxes
    if (headerchk.checked) {
        for (i = 0; i < gvcheck.rows.length; i++) {
            var inputs = gvcheck.rows[i].getElementsByTagName('input');
            inputs[0].checked = true;
        }
    }
        //if condition fails uncheck all checkboxes in gridview
    else {
        for (i = 0; i < gvcheck.rows.length; i++) {
            var inputs = gvcheck.rows[i].getElementsByTagName('input');
            inputs[0].checked = false;
        }
    }
}
function replaceAll(find, replace, str) {
    while (str.indexOf(find) > 0) {
        str = str.replace(find, replace);
    }
    return str;
}

function isDate(txtDate) {
    var currVal = $.trim(txtDate.value);
    if (currVal == '') {
        alert("Enter Date In Proper Format");
        txtDate.focus();
        return false;
    }
    //Declare Regex 
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    if (dtArray == null) {
        alert("Enter Date In Proper Format");
        txtDate.focus();
        return false;
    }
    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay = dtArray[3];
    dtYear = dtArray[5];
    if (dtMonth < 1 || dtMonth > 12) {
        alert("Enter Date In Proper Format");
        txtDate.focus();
        return false;
    }
    else if (dtDay < 1 || dtDay > 31) {
        alert("Enter Date In Proper Format");
        txtDate.focus();
        return false;
    }
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31) {
        alert("Enter Date In Proper Format");
        txtDate.focus();
        return false;
    }
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap)) {
            alert("Enter Date In Proper Format");
            txtDate.focus();
            return false;
        }
    }
    return true;

}

function IsValidEmailId(Input) {
    debugger;
    var regExp = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
    if (!regExp.test($.trim(Input.value))) {
        Input.value = "";
        Input.focus();
    }
    else {

        return true;
    }

    return false;
}
//Valdation for allowing only numbar space and dash like for zipcode
      function Validate(event) {
          var regex = new RegExp("^[0-9 \-]+$");
          var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
          if (!regex.test(key)) {
              event.preventDefault();
              return false;
          }
      }

//function EmailValidation() {
//    var Input = $('#ContentMain_CommCalculator_txtEmail').val();

//    var regExp = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;

//    if (!regExp.test(Input)) {
//        $('#ContentMain_CommCalculator_txtEmail').val('');
//    }
//    else {

//        return true;
//    }

//    return false;
//}
//function AllowNumbersOnly(CtrId) {
//    var regExpD = /^[0-9]*$/;
//    alert(regExpD.test(document.getElementById(CtrId)));
//    if (!regExpD.test(document.getElementById(CtrId))) {
//        document.getElementById(CtrId).value = "";
//        return false;
//    }
//    else {
//        return true;
//    }
//}


