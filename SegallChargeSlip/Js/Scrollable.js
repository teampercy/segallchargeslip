﻿(function ($) {
    $.fn.Scrollable = function (options) {
        var defaults = {
            ScrollHeight: 250,
            Width: 0 
        };
        var options = $.extend(defaults, options);
        return this.each(function () {
            var grid = $(this).get(0);
           // debugger;
         
         
            //--------------vv--------- marker : sk : 12/11/2013 : added to fix alignment in columns in past due and comming due sections 
            gridWidthTotal++;
            var gridWidth = grid.offsetWidth;
           

            if (gridWidthTotal == 1)
            {
                gridoffsetWidth_global = gridWidth;
               // alert("1 " + gridoffsetWidth_global);
            }
           
            if (gridWidthTotal == 3)
            {
                gridWidth = gridoffsetWidth_global;
               // alert("3 " + gridWidth);
            }

           
           // alert("grid.offsetWidth :  " + gridWidth + "  gridTotalWidth: " + gridWidthTotal + "  gridoffsetWidth_global: " + gridoffsetWidth_global);
            //--------------^^--------- marker : sk : 12/11/2013 : added to fix alignment in columns in past due and comming due sections 

            var headerCellWidths = new Array();
            for (var i = 0; i < grid.getElementsByTagName("TH").length; i++) {
                headerCellWidths[i] = grid.getElementsByTagName("TH")[i].offsetWidth;
            }

          

            //--------------vv--------- marker : sk : 12/16/2013 : added to fix alignment in columns in past due and comming due sections 

            if (gridWidthTotal == 1) {
                headerCellWidths_global = headerCellWidths;
            }
            
            if (gridWidthTotal == 3) {
                headerCellWidths = headerCellWidths_global;
            }
            //--------------^^--------- marker : sk : 12/16/2013 : added to fix alignment in columns in past due and comming due sections 

           // alert("headerCellWidths " + headerCellWidths);
            grid.parentNode.appendChild(document.createElement("div"));
            var parentDiv = grid.parentNode;

            var table = document.createElement("table");
            for (i = 0; i < grid.attributes.length; i++) {
                if (grid.attributes[i].specified && grid.attributes[i].name != "id") {

                    if (grid.attributes[i].name == "style") {
                        table.setAttribute("style", " height: 25px; font-weight: bold; border-bottom: 1px solid black; font-size: small; color: black; text-align: center; ");
                    }
                    else {
                        table.setAttribute(grid.attributes[i].name, grid.attributes[i].value);
                    }
                }
            }

            //table.style.cssText = grid.style.cssText;

            //alert(gridWidth + "px");

            table.style.width =  (gridWidth - 17)+ "px";
           // alert(table.style.width);


            table.appendChild(document.createElement("tbody"));
            table.getElementsByTagName("tbody")[0].appendChild(grid.getElementsByTagName("TR")[0]);
            var cells = table.getElementsByTagName("TH");

           


            var headerWidths = 0;
           // var gridRowWidth_test = 0;

            var gridRow = grid.getElementsByTagName("TR")[0];
            for (var i = 0; i < cells.length; i++) {
                var width;
                if (headerCellWidths[i] > gridRow.getElementsByTagName("TD")[i].offsetWidth) {
                    width = headerCellWidths[i];
                    headerWidths++;
                   // alert("headercell widths > tds offsetwidth = headerCellWidths["+i+"]" + headerCellWidths[i]);

                }
                else {

                    width = gridRow.getElementsByTagName("TD")[i].offsetWidth;
                    // gridRowWidth_test++;

                    //alert("tds offset width of "+ i +": " + headerCellWidths[i]);
                }

                //-----------------
              //  alert(headerCellWidths[i]);
                   // width = headerCellWidths[i];
                  //  headerWidths++;
               
                cells[i].style.width = parseInt(width - 3) + "px";
                gridRow.getElementsByTagName("TD")[i].style.width = parseInt(width - 3) + "px";
            }

            
         

            //   alert( "headerCellWidths : " + headerCellWidths + " gridRowWidth_test: " + gridRowWidth_test);


          parentDiv.removeChild(grid);

            var dummyHeader = document.createElement("div");
            dummyHeader.appendChild(table);
            parentDiv.appendChild(dummyHeader);
            parentDiv.appendChild(table);
            parentDiv.style = " background-color:#D3D3D3;"
            if (options.Width > 0) {
                gridWidth = options.Width;
            }
            var scrollableDiv = document.createElement("div");
            gridWidth = parseInt(gridWidth); //  + 17
            scrollableDiv.style.cssText = "background-color: #FFFFFF; overflow:scroll; overflow-x:hidden; height:" + options.ScrollHeight + "px;width:" + gridWidth + "px";
            scrollableDiv.appendChild(grid);
            parentDiv.appendChild(scrollableDiv);
             

    
   


            //var dummyHeader = document.createElement("div");
            //dummyHeader.appendChild(table);

            ////parentDiv.appendChild(dummyHeader);
            ////if (options.Width > 0) {
            ////    gridWidth = options.Width;
            ////}
            //var scrollableDiv = document.createElement("div");
            //gridWidth = parseInt(gridWidth + 17);
            //scrollableDiv.style.cssText = "overflow:auto;height:" + options.ScrollHeight + "px;width:" + gridWidth + "px";
            //scrollableDiv.appendChild(grid);

            //dummyHeader.appendChild(scrollableDiv);

            //parentDiv.appendChild(dummyHeader);
            ////parentDiv.style.width = gridWidth + "px";
            ////toc.style.width = '95%';
            //// parentDiv.style.width = '95%';
            ////J$("#divContentsp").style.width = '534px';
            ////parentDiv.removeChild(grid);



            //var itemtoReplaceContentOf = $('#divContentsp');
            //itemtoReplaceContentOf.html('');

            //itemtoReplaceContentOf.appendChild(dummyHeader);
            //dummyHeader.innerHTML.appendTo(itemtoReplaceContentOf);
        });
    };
    $.fn.Scrollable101 = function (options) {

    //970
        
        var defaults = {
            ScrollHeight: 368,
            Width: 0
        };
        var options = $.extend(defaults, options);
        return this.each(function () {
            var grid = $(this).get(0);
            debugger;
           // alert('scrollable');

            //--------------vv--------- marker : sk : 12/11/2013 : added to fix alignment in columns in past due and comming due sections 
            //gridWidthTotal++;
            var gridWidth = 970;//grid.offsetWidth;


            //if (gridWidthTotal == 1) {
            //    gridoffsetWidth_global = gridWidth;
            //    // alert("1 " + gridoffsetWidth_global);
            //}

            //if (gridWidthTotal == 3) {
            //    gridWidth = gridoffsetWidth_global;
            //    // alert("3 " + gridWidth);
            //}


            // alert("grid.offsetWidth :  " + gridWidth + "  gridTotalWidth: " + gridWidthTotal + "  gridoffsetWidth_global: " + gridoffsetWidth_global);
            //--------------^^--------- marker : sk : 12/11/2013 : added to fix alignment in columns in past due and comming due sections 

            var headerCellWidths = new Array();
           // alert(grid.getElementsByTagName("TH").length);
            if (grid.getElementsByTagName("TH").length == 12) {
               // alert('called');
                headerCellWidths[0] = 128;
                headerCellWidths[1] = 82;
                headerCellWidths[2] = 120;
                headerCellWidths[3] = 82;
                headerCellWidths[4] = 120;
                headerCellWidths[5] = 14;
                headerCellWidths[6] = 120;
                headerCellWidths[7] = 120;
                headerCellWidths[8] = 120;
                headerCellWidths[9] = 20;
                headerCellWidths[10] = 20;
                headerCellWidths[11] = 20;
            }
            else if (grid.getElementsByTagName("TH").length == 10) {
               // alert('called1');
                headerCellWidths[0] = 180;
                headerCellWidths[1] = 120;
                headerCellWidths[2] = 120;
                headerCellWidths[3] = 80;
                headerCellWidths[4] = 120;
                headerCellWidths[5] = 14;
                headerCellWidths[6] = 120;
                headerCellWidths[7] = 21;
                headerCellWidths[8] = 21;
                headerCellWidths[9] = 21;
            }
            else {
                for (var i = 0; i < grid.getElementsByTagName("TH").length; i++) {
                headerCellWidths[i] = grid.getElementsByTagName("TH")[i].offsetWidth;

                }
            }



            //--------------vv--------- marker : sk : 12/16/2013 : added to fix alignment in columns in past due and comming due sections 

            //if (gridWidthTotal == 1) {
            //    headerCellWidths_global = headerCellWidths;
            //}

            //if (gridWidthTotal == 3) {
            //    headerCellWidths = headerCellWidths_global;
            //}
            //--------------^^--------- marker : sk : 12/16/2013 : added to fix alignment in columns in past due and comming due sections 

            // alert("headerCellWidths " + headerCellWidths);
            grid.parentNode.appendChild(document.createElement("div"));
            var parentDiv = grid.parentNode;

            var table = document.createElement("table");
            for (i = 0; i < grid.attributes.length; i++) {
                if (grid.attributes[i].specified && grid.attributes[i].name != "id") {

                    if (grid.attributes[i].name == "style") {
                        table.setAttribute("style", "font-size:Small;width:"+ gridWidth+"px;border-collapse:collapse;color: black; border-bottom: 1px solid black; font-weight: bold;");
                            //height: 25px; font-weight: bold; border-bottom: 1px solid black; font-size: small; color: black; text-align: center; ");
                    }
                    else {
                        table.setAttribute(grid.attributes[i].name, grid.attributes[i].value);
                    }
                }
            }

            //table.style.cssText = grid.style.cssText;

            //alert(gridWidth + "px");

            //table.style.width = (gridWidth) + "px";
            // alert(table.style.width);


            table.appendChild(document.createElement("tbody"));
            table.getElementsByTagName("tbody")[0].appendChild(grid.getElementsByTagName("TR")[0]);
            var cells = table.getElementsByTagName("TH");




            var headerWidths = 0;
            // var gridRowWidth_test = 0;

            var gridRow = grid.getElementsByTagName("TR")[0];
            for (var i = 0; i < cells.length; i++) {
                var width;


                width = headerCellWidths[i] ;
                headerWidths++;


               //// gridRow.getElementsByTagName("TD")[i].offsetWidth=790;
               // if (headerCellWidths[i] > gridRow.getElementsByTagName("TD")[i].offsetWidth) {
               //     width = headerCellWidths[i];
               //     headerWidths++;
               //     // alert("headercell widths > tds offsetwidth = headerCellWidths["+i+"]" + headerCellWidths[i]);

               // }
               // else {

               //     width = gridRow.getElementsByTagName("TD")[i].offsetWidth;
               //     // gridRowWidth_test++;

               //     //alert("tds offset width of "+ i +": " + headerCellWidths[i]);
               // }

               // //-----------------
               // //  alert(headerCellWidths[i]);
               // // width = headerCellWidths[i];
               // //  headerWidths++;

                cells[i].style.width = parseInt(width ) + "px";
                gridRow.getElementsByTagName("TD")[i].style.width = parseInt(width) + "px";
            }




            //   alert( "headerCellWidths : " + headerCellWidths + " gridRowWidth_test: " + gridRowWidth_test);


            parentDiv.removeChild(grid);

            var dummyHeader = document.createElement("div");
            dummyHeader.appendChild(table);
            parentDiv.appendChild(dummyHeader);
            parentDiv.appendChild(table);
            parentDiv.style = " background-color:#D3D3D3;"
            if (options.Width > 0) {
                gridWidth = options.Width;
            }
            var scrollableDiv = document.createElement("div");
            gridWidth = parseInt(gridWidth) + 17;
            scrollableDiv.style.cssText = "background-color: #FFFFFF;  overflow-y:auto; height:" + options.ScrollHeight + "px;width:" + gridWidth + "px";
            scrollableDiv.appendChild(grid);
            parentDiv.appendChild(scrollableDiv);


            //var dummyHeader = document.createElement("div");
            //dummyHeader.appendChild(table);

            ////parentDiv.appendChild(dummyHeader);
            ////if (options.Width > 0) {
            ////    gridWidth = options.Width;
            ////}
            //var scrollableDiv = document.createElement("div");
            //gridWidth = parseInt(gridWidth + 17);
            //scrollableDiv.style.cssText = "overflow:auto;height:" + options.ScrollHeight + "px;width:" + gridWidth + "px";
            //scrollableDiv.appendChild(grid);

            //dummyHeader.appendChild(scrollableDiv);

            //parentDiv.appendChild(dummyHeader);
            ////parentDiv.style.width = gridWidth + "px";
            ////toc.style.width = '95%';
            //// parentDiv.style.width = '95%';
            ////J$("#divContentsp").style.width = '534px';
            ////parentDiv.removeChild(grid);



            //var itemtoReplaceContentOf = $('#divContentsp');
            //itemtoReplaceContentOf.html('');

            //itemtoReplaceContentOf.appendChild(dummyHeader);
            //dummyHeader.innerHTML.appendTo(itemtoReplaceContentOf);
        });
    };
})(jQuery);