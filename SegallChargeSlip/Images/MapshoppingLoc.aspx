﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MapshoppingLoc.aspx.cs" Inherits="Pages_MapshoppingLoc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&Place=true&libraries=places"></script>
    <script src="../Js/GoogleMap/infobox.js"></script>

    <title></title>

    <script type="text/javascript">
        var overlay;
        var mapgoogle;
        var markersArray = [];
        var strLocationDesc;

        //$(function () {

        //     loadShowMap();
        //});
        google.maps.event.addDomListener(window, 'load', loadShowMap);

        function loadShowMap() {
            debugger;
            //var query = location.search;
            //alert(query);
            //alert($.query.get('id'));
            var input = document.getElementById('txtSearch');
            var autocomplete = new google.maps.places.Autocomplete(input);
            var latlng = new google.maps.LatLng(41.4925374, -99.9018);
            var myOptions = {
                zoom:8,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            geocoder = new google.maps.Geocoder();
            mapgoogle = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            NewMapLoad(mapgoogle, geocoder, '');

            $('#btnMapSearch').click(function () {
                debugger;

                //var parentOffset = $('#map_canvas').parent().offset();
                //var point = new google.maps.Point(e.pageX - parentOffset.left, e.pageY - parentOffset.top);
                //var ll = overlay.getProjection().fromContainerPixelToLatLng(point);
                geocoder = new google.maps.Geocoder();
                var address = $('#txtSearch').val();
                deleteOverlays();
                if (address != '') {
                    // $('#hdnlocation').val(address);
                    NewMapLoad(mapgoogle, geocoder, $('#txtSearch').val());
                }
                else { alert('Please enter a Location to Search'); }
            });

            $('#btnCloneLocation').click(function () {
                debugger;

                //var parentOffset = $('#map_canvas').parent().offset();
                //var point = new google.maps.Point(e.pageX - parentOffset.left, e.pageY - parentOffset.top);
                //var ll = overlay.getProjection().fromContainerPixelToLatLng(point);
                geocoder = new google.maps.Geocoder();
                var address = $('#txtSearch').val();
                deleteOverlays();
                if (address != '') {
                    // $('#hdnlocation').val(address);
                    NewMapLoad(mapgoogle, geocoder, 'clone');
                }
                else { alert('Please enter a Location to Search'); }
            });


            $("#draggable").draggable({
                helper: 'clone',
                stop: function (e) {

                    var parentOffset = $('#map_canvas').parent().offset();
                    var point = new google.maps.Point(e.pageX - parentOffset.left, e.pageY - parentOffset.top);
                    var ll = overlay.getProjection().fromContainerPixelToLatLng(point);
                    var address;
                    // deleteOverlays();
                    geocoder = new google.maps.Geocoder();

                    geocoder.geocode({ 'address': "", 'latLng': ll, 'region': 'US' }, function (results, status) {

                        if (status == google.maps.GeocoderStatus.OK) {
                            $('#txtSearch').val("");
                            deleteOverlays();
                            address = results;
                            $('#hdnlocation').val(address[0].address_components[address[0].address_components.length - 1].long_name);
                            //alert($('#hdnlocation').val());
                            mapgoogle.setCenter(results[0].geometry.location);
                            var marker = new google.maps.Marker({
                                map: mapgoogle,
                                position: results[0].geometry.location,
                                title: "Rectified position",
                                draggable: true,
                                icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
                            });
                            markersArray.push(marker);
                            var infowindow = new google.maps.InfoWindow({
                                content: "Search Location",
                                maxWidth: 300,
                                maxHeight: 200
                            });


                            google.maps.event.addListener(marker, 'click', function () {
                                infowindow.open(mapgoogle, marker);
                            });
                            google.maps.event.addListener(mapgoogle, 'click', function () {
                                // alert('Info CLoase');
                                infowindow.close();
                            });
                            setSearchmarker(marker);

                            NewMapLoad(mapgoogle, geocoder, '');
                        } else {
                            alert("Geocode was not successful for the following reason: " + status+ "  Please Check Address ");
                        }
                    });
                }
            });
        }



        function NewMapLoad(Nmapgoogle, geocoder, searchplace) {
            debugger;
            //   AddMappoints("", "", "", strPopupDiv, geocoder, mapgoogle, PointColor, strAddr);
            var strAddr = "Hollywood, LA"
            var strPopupDiv = "<table cellPadding='0' cellSpacing='0' style='width:100%;'> <tr><td>" + "Initial Point" + "</td></tr></table> ";
            var strSearchDiv = "<table cellPadding='0' cellSpacing='0' style='width:100%;'> <tr><td>" + "Search Point" + "</td></tr></table> ";
            var PointColor = "RED";

            //var OrgLocation = document.getElementById('hdnactlocation');
            var OrglatLon = document.getElementById('hdnactlocation');
            var Orglat = "", OrgLon = "";

            if (OrglatLon != null && OrglatLon.value.split('|').length > 1) {
                Orglat = OrglatLon.value.split('|')[0];
                OrgLon = OrglatLon.value.split('|')[1];
            }
                //else if (OrgLocation != null && OrgLocation.value.trim() != "") {
            else if (OrglatLon != null && OrglatLon.value.trim() != "") {
                strAddr = OrglatLon.value;
            }

            AddMappointsNew(Orglat, OrgLon, strPopupDiv, geocoder, mapgoogle, PointColor, strAddr, "Initial Point");

            if (searchplace != '' && searchplace.toLowerCase() != 'clone') {
                AddMappointsNew('', '', strSearchDiv, geocoder, mapgoogle, "GREEN", searchplace, "Search Point");
            }
            if (searchplace.toLowerCase() == 'clone') {
                AddMappointsNew(orglat, orglon, strSearchDiv, geocoder, mapgoogle, "GREEN", strAddr, "Search Point");
            }

            overlay = new google.maps.OverlayView();
            overlay.draw = function () { };
            overlay.setMap(mapgoogle);
        }

        function AddMappointsNew(lat, lon, strpopup, geocoder, mapgoogle, pointcolor, address, title) {
            var point;
            var iconlnk = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
            // debugger;

            if (pointcolor != "" && pointcolor == "GREEN") {
                iconlnk = "http://maps.google.com/mapfiles/ms/icons/green-dot.png";
            }

            geocoder = new google.maps.Geocoder();
            //var addr = strpopup;
            var Latitude = lat;
            var longtitude = lon;
            var addressdiv = $('<div/>').html(strpopup).html();

            if (Latitude != '' && longtitude != '') {
                //add point using lat lan

                var marker = new google.maps.Marker({
                    map: mapgoogle,
                    icon: iconlnk,
                    position: new google.maps.LatLng(Latitude, longtitude),
                    title: title
                });

                mapgoogle.setCenter(new google.maps.LatLng(Latitude, longtitude));
                // marker.setDraggable(true);

                //google.maps.event.addListener(marker, "dragend", function (event) {
                //    var point = marker.getPosition();
                //    map.panTo(point);

                //});

                var infowindow = new google.maps.InfoWindow({
                    content: addressdiv,
                    maxWidth: 370,
                    maxHeight: 230
                });
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(mapgoogle, marker);
                });
                if (pointcolor != "" && pointcolor == "GREEN") {
                    setSearchmarker(marker);
                }

                markersArray.push(marker);
            }
            else {
                geocoder.geocode({ 'address': address }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        mapgoogle.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: mapgoogle,
                            icon: iconlnk,
                            position: results[0].geometry.location,
                            title: title
                        });
                        var infowindow = new google.maps.InfoWindow({
                            content: addressdiv,
                            maxWidth: 300,
                            maxHeight: 200
                        });
                        google.maps.event.addListener(marker, 'click', function () {
                            infowindow.open(mapgoogle, marker);
                        });
                        if (pointcolor != "" && pointcolor == "GREEN") {
                            setSearchmarker(marker);
                        }

                        markersArray.push(marker);
                    } else {
                        mapgoogle.setZoom(5);
                        alert("Geocode was not successful for the following reason: " + status);
                       
                    }
                });
                //add point using geo code
            }

        }



        function setSearchmarker(marker) {
            google.maps.event.addListener(marker, 'dragend', function () {
                document.getElementById('hdnNewlocationLon').value = marker.position.lng();
                document.getElementById('hdnNewlocationLat').value = marker.position.lat();
            });

            marker.setDraggable(true);
            document.getElementById('hdnNewlocationLon').value = marker.position.lng();
            document.getElementById('hdnNewlocationLat').value = marker.position.lat();
            //alert(document.getElementById('hdnNewlocationLon').value + " ____" + document.getElementById('hdnNewlocationLat').value);
            //alert(marker.position.lat() + "_________" + marker.position.lng());
        }


        // Deletes all markers in the array by removing references to them
        function deleteOverlays() {
            document.getElementById('hdnNewlocationLon').value = "";
            document.getElementById('hdnNewlocationLat').value = "";
            if (markersArray) {
                for (i in markersArray) {
                    markersArray[i].setMap(null);
                }
                markersArray.length = 0;
            }
        }


        function getLaton() {

            if (document.getElementById('hdnNewlocationLon').value.trim() != "" && document.getElementById('hdnNewlocationLat').value.trim() != "") {
                parent.callfromIfram(document.getElementById('hdnNewlocationLat').value, document.getElementById('hdnNewlocationLon').value.trim());
                //set values and //close pop up 
            }
            else {
                alert('Please select New location first');
                //alert(document.getElementById('hdnNewlocationLon').value.trim() + "____" + document.getElementById('hdnNewlocationLat').value.trim());
                return false;
            }
        }
    </script>
    <style type="text/css">
        #draggable {
            /*position: absolute;
        width: 30px;
        height: 30px;*/
            z-index: 1000000000;
        }
    </style>
</head>
<body>

    <table style="text-align: center; width: 98%; height: 95%">
        <tr>
            <td style="width: 70%;">
                <input id="txtSearch" type="text" placeholder="search address" style="width: 400px; height: 30px; float: left; padding-left: 10px;" />
                <input runat="server" type="hidden" id="hdnNewlocationLon" value="" />
                <input runat="server" type="hidden" id="hdnNewlocationLat" value="" />
                <input type="text" runat="server" id="lbl123" value="adsf" visible="false" />

                <input runat="server" type="hidden" id="hdnActLatLon" value="" />
                <input runat="server" type="hidden" value=" Hollywood,LA " id="hdnactlocation" />
            </td>
            <td>
                <a href="#" id="btnMapSearch" style="border: none; color: black; cursor: pointer;">
                    <img src="../Images/Map%20Search-Button.gif" title="Search" width="35" height="30" style="border: none; vertical-align: middle;" />
                </a>
            </td>
            <td>
                <a href="#" style="border: none; color: black; cursor: pointer;">
                    <img src="../Images/Map-Set-Location-Button.gif" id="draggable" title="Search" width="35" height="30" style="border: none; vertical-align: middle;" />
                </a>


            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="map_canvas" style="width: 95%; height: 525px; background-color: GrayText"></div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div style="width: 95%; ">
                    <a onclick="return deleteOverlays();" style="display: none">Clear all points</a>
                    <input id="btnnextMap" onclick="return getLaton();" class="SqureButton" style="background-color: #F68620; float: right;" title="NEXT" value="NEXT" />
                    <%-- <a onclick="return getLaton();" id="btnnextMap"  href="#"   class="SqureButton" style="cursor: hand; Textdecoration:none; " >NEXT </a>--%>

                    <a id="btnCloneLocation" style="display: none">Clone </a>
                </div>
            </td>
        </tr>
    </table>

</body>
</html>
