﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ApplicationPortal.aspx.cs" Inherits="ApplicationPortal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Js/jquery-1.9.1.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#dvGeoApp").click(function () {
                // alert("Charge Slip Click");
            });
            $("#dvChargeSlip").click(function () {
                window.location.href = "Login.aspx";
            });
            $("#imgPortalHome").click(function () {
                window.location.href = "ApplicationPortal.aspx";
            });
        });

    </script>
    <style type="text/css">
        body {
            font-family: Verdana;
            font-size: 11px;
            color: #727272;
            /*background-image: url('Images/white_carbonfiber.png');*/
            background-color: #22034C;
            margin: 0px;
        }

        .borderCSS {
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            width: 140px;
            height: 140px;
            line-height: 140px;
            text-align: center;
            cursor: pointer;
            /* vertical-align:middle;
            flex-align:center;
            display: inline-block;*/
        }



        span {
            display: inline-block;
            vertical-align: middle;
            line-height: normal;
            color: white;
            font-size: 22px;
            font-weight: 700;
        }

        .pointer {
            cursor: pointer;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table style="width: 100%; height: 100%">
            <tr style="height: 20%">
                <td>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr style="height: 30%">
                <td align="center">

                    <table style="background-color: white; width: 70%; height: 250px">
                        <tr>
                            <td valign="top" align="left">

                                <asp:Image ID="imgPortalHome" ImageUrl="MasterPage/Images/homeIconOrange.png" Width="22px" Height="22px" runat="server" CssClass="pointer" />
                            </td>
                            <td style="width: 50%; padding-left: 20%">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Image ID="Image1" ImageUrl="Images/Segall_logo.png" Width="160px" Height="160px" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-size: 18px; color: #22034C; font-weight: 700;" align="center">APPLICATION<br />
                                            PORTAL
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>

                                <div style="background-color:  #B8B8B8;" class="borderCSS" id="dvGeoApp">
                                    <span>Geo App </span>
                                </div>


                            </td>
                            <td align="left">
                                <div style="background-color: #F68620;" class="borderCSS" id="dvChargeSlip">
                                    <span>Charge<br />
                                        Slips </span>
                                </div>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr style="height: 30%">
                <td align="center" style="color: white; font-size: 15px">www.segallgroup.com</td>
            </tr>
        </table>

    </form>
</body>
</html>
