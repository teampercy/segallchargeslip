﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SegallChargeSlipBll;




//using EditableControls;

/// <summary>
/// Summary description for Utility
/// </summary>
public class Utility
{

    #region Variables


    #endregion


    public Utility()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    // Functions To Fill ASP:DropDownList And EditableDropDownList

    #region FillDropDownOverrideFun



    public static void FillDropDown(ref DropDownList ddl, DataTable dt, string txtField, string ValueFeild)
    {
        ddl.Items.Clear();
        //if (dt.Rows.Count > 0)
        //{
        ddl.DataSource = dt;
        ddl.DataTextField = txtField;
        ddl.DataValueField = ValueFeild;
        ddl.DataBind();
        ddl.Items.Insert(0, new ListItem("- Select -", ""));
        ddl.SelectedIndex = 0;
        //}
    }
    public static void FillDropDownBroker(ref DropDownList ddl, DataTable dt, string txtField, string ValueFeild)
    {
        ddl.Items.Clear();
        //if (dt.Rows.Count > 0)
        //{
        ddl.DataSource = dt;
        ddl.DataTextField = txtField;
        ddl.DataValueField = ValueFeild;
        ddl.DataBind();
        ddl.Items.Insert(0, new ListItem("- ALL -", ""));//Changed "" to "0"
        ddl.SelectedIndex = 0;
        //}
    }

    public static void FillDropDown(ref AjaxControlToolkit.ComboBox ddl, DataTable dt, string txtField, string ValueFeild)
    {
        ddl.Items.Clear();
        if (dt.Rows.Count > 0)
        {
            ddl.DataSource = dt;
            ddl.DataTextField = txtField;
            ddl.DataValueField = ValueFeild;
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("- Select -", ""));
            ddl.SelectedIndex = 0;
        }
    }



    public static void ConvertToSF(ref decimal output, decimal input)
    {
        if (!string.IsNullOrEmpty(input.ToString()))
        {
            output = input * 43560;
        }
    }

    public static void ClearSessionExceptUser()
    {
        int user;
        string usertype, Name, LoggedInUser;

        user = Convert.ToInt32(HttpContext.Current.Session[GlobleData.User]);
        usertype = HttpContext.Current.Session["UserType"].ToString();
        LoggedInUser = HttpContext.Current.Session["UserName"].ToString();

        Name = HttpContext.Current.Session["User"].ToString();
        HttpContext.Current.Session.Clear();
        HttpContext.Current.Session.RemoveAll();
        HttpContext.Current.Session[GlobleData.User] = user;
        HttpContext.Current.Session["UserType"] = usertype;
        HttpContext.Current.Session["User"] = Name;
        HttpContext.Current.Session["UserName"] = LoggedInUser;
      
    }

    public static void CheckSession()
    {
        if (HttpContext.Current.Session[GlobleData.User] == null)
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.RemoveAll();

            HttpContext.Current.Session[GlobleData.cns_PrevPage] = HttpContext.Current.Request.Url.AbsolutePath.ToString();
            HttpContext.Current.Response.Redirect("~/Login.aspx",false);

            // 
        }

    }
    public static bool IsValidSession()
    {
        if (HttpContext.Current.Session[GlobleData.User] != null)
        {
            return true;
            //valid session 
            //add ne other check for valid session
        }
        else
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.RemoveAll();
            HttpContext.Current.Session[GlobleData.cns_PrevPage] = HttpContext.Current.Request.Url.AbsolutePath.ToString();
            HttpContext.Current.Response.Redirect("~/Login.aspx",false);
            // 
            return false;
        }

    }

    #region TypeWiseFillCombo

    #endregion

    //public static void FillShoppingCenter(ref AjaxControlToolkit.ComboBox ddl,string txtField, string ValueFeild)
    //{
    //     CustomList objCL = new CustomList();
    //     FillDropDown(ref ddl, objCL.GetLocationList(), "ShoppingCenter", "LocationID");
    //}

    public static void FillStates(ref DropDownList ddlState)
    {
        CustomList objCL = new CustomList();
        FillDropDown(ref ddlState, objCL.GetStateList(), "StateName", "State");
    }

    public static void FillBrokerDropDownFromUserCompany(ref DropDownList ddlBroker)
    {
        Users objUser = new Users();
        objUser.UserID = Convert.ToInt32(HttpContext.Current.Session[GlobleData.User]);
        FillDropDown(ref ddlBroker, objUser.GetBrokerListByUsersCompany(), "BrokerName", "BrokerId");

    }

    public static void WriteErrorLog(string PageSource, string MethodSource, string ErrorMsg)
    {
        ErrorLog.WriteLog(PageSource, MethodSource, ErrorMsg);

        //HttpContext.Current.Response.Write("<script>alert('Error Occurred');return false;</script>");
    }



    #endregion


    //public static List<string> AutoFillTextBox(List<string> lst)
    //{
    //    List<string> rslt = new List<string>();
    //    if (lst.Count != 0)
    //    {




    //    }
    //    return rslt;
    //}
}