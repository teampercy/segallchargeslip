﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;

/// <summary>
/// Summary description for EmailUtility
/// </summary>
public class EmailUtility
{

    #region Variables
    //string EmailBody, EmailSub, EmailFrom, EmailTo, EmailCC;
    #endregion

    #region Properties

    public string EmailBody
    {
        get;
        set;
    }

    public string EmailFrom
    {
        get;
        set;
    }
    public string EmailTo
    {
        get;
        set;
    }

    public string EmailCC
    {
        get;
        set;
    }
    public string EmailSub
    {
        get;
        set;
    }

    // For sending PDF attachment -- read from memory steam
    public byte[] Memorybyte
    {
        get;
        set;
    }

    public string EmailAttachmentName
    {
        get;
        set;
    }
    #endregion


    public Boolean SendEmailWithPdfAttachment()
    {
        try
        {
            MailMessage oMsg = new MailMessage();
            SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPHOST"]);
            oMsg.From = (new MailAddress(EmailFrom));
            oMsg.To.Add(new MailAddress(EmailTo));
            oMsg.Subject = EmailSub;
            oMsg.Body = EmailBody;
            oMsg.Attachments.Add(new Attachment(new MemoryStream(Memorybyte), EmailAttachmentName));
            oMsg.IsBodyHtml = true;
            System.Net.NetworkCredential basicAuthentication = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["USER"], System.Configuration.ConfigurationManager.AppSettings["PASSWORD"]);
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = basicAuthentication;
            smtp.Send(oMsg);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public EmailUtility()
    {
        //
        // TODO: Add constructor logic here
        //


    }
}