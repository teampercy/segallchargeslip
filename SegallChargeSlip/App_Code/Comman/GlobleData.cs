﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GlobleData
/// </summary>
public class GlobleData
{
    public const string DealTransactionObj = "DealTransactionObj";
    public const string NewChargeSlipObj = "NewChargeSlipObj";
    public const string DealTransactionId = "DealTransactionId";
    public const string NewChargeSlipId = "NewChargeSlipId";
    public const string User = "UserId";
    public const string CommissionTb = "dtDueDtComm";
    public const string Attachments = "dtAttachment";
    public const string cns_dtBasicLeaseTerm = "dtBasicLeaseTerm";
    public const string cns_dtCommTerm = "dtCommTerm";
    public const int cns_TertType_BaseLease = 1;
    public const int cns_TertType_OptionTerm = 2;
    public const int cns_TertType_OptionTermPayable = 4; //this field not used so replace the value to cns_TertType_OptionTerm1
    public const int cns_TertType_ContingentTerm = 3;
    public const int cns_TertType_OptionTerm1 = 4;
    public const int cns_TertType_OptionTerm2 = 5;
    public const int cns_TertType_OptionTerm3 = 6;
    public const string cns_totBaseLeaseComm = "BaseLeaseCommTot";
    public const string cns_totOptComm = "OptCommTot";
    public const string cns_totContingentComm = "ContingentCommTot";
    public const string cns_totOptComm1 = "OptCommTot1";
    public const string cns_totOptComm2 = "OptCommTot2";
    public const string cns_totOptComm3 = "OptCommTot3";
    public const int Pagesize = 4; //----SK
    public const string TotalRent = "TotalRent";
    public const byte cns_Status_Complete = 1;
    public const byte cns_Status_Draft = 0;
    public const string cns_Is_Complete = "IsComplete";
    public const string cns_PrevPage = "PrevPage";
    public const string cns_DueDate = "tbDueDates";
    public const string cns_IsFullCommPaid = "IsFullCommPaid";
    public enum SnapShotDetailTable { 
    GrossReceivables_1=0,
    CompanyPostSplit_1=1,
    PAST_DUE =2,
    GrossReceivables_2 = 3,
    CompanyPostSplit_2 = 4,
    Base_Term_Current_Year= 5,
    GrossReceivables_3 = 6,
    CompanyPostSplitFuture = 7,
    POSTsplit_Receivables_for_future_due =8,
    GrossPaid_NetPaid =9,
    Payments_Current_year =10,
    Pre_Split_Options_Contingent_Receivables=11,
    OPT_Contingent_for_Currentyear_and_PastDue = 12,
    Total_Pre_Split_Options_Contingent_Receivables = 13,
    OPTION_COntin_NetPOSTsplit_Receivables_for_future_due= 14,
    Estimated_Post_Split_Commission_Due =15,
    CurrentYearChargeslip =16,
    PreviousYearChargeslip = 17,
    MainSummary_Table = 18,
    Gross_Commission_needed_to_Reach_Next_Tier =19
    };

}