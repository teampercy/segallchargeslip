﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using SegallChargeSlipBll;
using System.Data;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService {

    public WebService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public string[] SearchGetWhatList(string prefixText, int count)
    {
        List<string> lstSearchList = new List<string>();
        try
        {
            Locations objLocations = new Locations();
            objLocations.ShoppingCenter = prefixText;
            DataSet ds = objLocations.GetLocationSeachList();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dt in ds.Tables[0].Rows)
                {
                    //dt["ValueType"].ToString()
                    lstSearchList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dt["ShoppingCenter"].ToString(), "1" + "," + dt["LocationID"].ToString()));
                }
            }
        }
        catch (Exception ex)
        {

        }
        return lstSearchList.ToArray();
    }
    
}
