﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Js/jquery-1.9.1.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#imgPortalHome").click(function () {
                window.location.href = "ApplicationPortal.aspx";
            });
        });
    </script>
    <style type="text/css">
        .loginContent {
            font-size: 1.1em;
            color: #22034C;
        }

        .btn_submitLogin {
            font-size: 1em;
            font-weight: bold;
            color: white;
            background-color: #F68620;
            width: 90px;
        }

        .btn_GoToAdmin {
            color: gray;
            text-decoration: none;
        }
        .loginfont {
            font-size:20px;
            color:#22034C;
        }

        body {
            font-family: Verdana;
            font-size: 11px;
            color: #727272;
            /*background-image: url('Images/white_carbonfiber.png');*/
            background-color: #22034C;
            margin: 0px;
        }

        .pointer {
            cursor: pointer;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <ajax:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true" />
        <div id="mainContainer">
            <table style="width: 100%; height: 100%;">
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 100%; margin: auto; padding-top: 25px; padding-bottom: 25px;" align="center" class="loginContent">
                        <table style="width: 60%; margin: auto; background-color: white; margin-top: 20px; margin-bottom: 20px;">
                            <tr>
                                <td align="left">
                                    <asp:Image ID="imgPortalHome" ImageUrl="MasterPage/Images/homeIconOrange.png" Width="22px" Height="22px" runat="server" CssClass="pointer" />
                                </td>
                            </tr>
                            <tr style="">
                                <td style="width: 10%;"></td>
                                <td style="width: 30%;">
                                  

                                    <table><tr><td align="center">
                                         <asp:Image ID="Image1" ImageUrl="Images/Segall_logo.png" Width="160px" Height="160px" runat="server" />
                                               </td></tr>
                                        <tr><td align="center" style="font-size:34px; font-weight:900; color:#22034C;">CS <font color="#D66A2A">APP</font></td></tr>
                                    </table>
                                    <br />
<%--                                    <h1 style="text-align:left;"><span style="color: #22034C; font-size: 1.5em;">CS</span><span style="color: #D66A2A; font-size: 1.5em;">APP</span></h1>--%>
                                </td>
                                <td align="center">
                                    <table style="width: 270px; background-color: white; margin: auto;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"><strong>USER LOGIN</strong></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtUsername" Width="270px" />
                                                <ajax:TextBoxWatermarkExtender ID="WMtxtUsername" runat="server"
                                                    WatermarkText="EMAIL.." TargetControlID="txtUsername">
                                                </ajax:TextBoxWatermarkExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtPassword" Width="270px" TextMode="Password" />
                                                <ajax:TextBoxWatermarkExtender ID="WMtxtPassword" runat="server"
                                                    WatermarkText="PASSWORD.." TargetControlID="txtPassword">
                                                </ajax:TextBoxWatermarkExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right; Width: 270px">
                                                <asp:Button Text="LOGIN" ID="btnSubmit" runat="server" CssClass="btn_submitLogin" OnClick="btnLogin_Click" />
                                            </td>
                                        </tr>

                                    </table>
                                    <br />
                                    <%-- <table style="width:100%">
                                        <tr>
                                            <td align="right" style="width:100%; color:gray; text-decoration:none; padding-right:15px;" align="right">
                                                <asp:LinkButton Text="GO TO ADMIN" ID="lnkGoToAdmin" runat="server" class="btn_GoToAdmin" />                                                 
                                            </td>
                                        </tr>
                                    </table>--%>

                                </td>
                            </tr>
                       
                        </table>

                    </td>
                </tr>

                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
